Imports Dapper.Contrib.Extensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entity.ConnectionExtensions
Imports isr.Core.MSTest
Imports isr.Core.EnumExtensions
Imports isr.Dapper.Entities

''' <summary> An ohmni auditor. </summary>
''' <remarks>
''' David, 2/25/2020 <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class OhmniAuditor

    #Region " ATTRIBUTE TYPE ENTITY "

    ''' <summary> Assert the Attribute Type entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertAttributeTypeEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Dim id As Integer = 100
        Dim entity As New OhmniAttributeTypeEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = OhmniAttributeTypeBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            r = entity.ValidateNewEntity($"{NameOf(Ohmni.OhmniAttributeTypeEntity)} with '{NameOf(Ohmni.OhmniAttributeTypeEntity.AttributeTypeId)}' of  {id}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualFetched As Boolean = entity.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {id} should not exist")
            r = entity.ValidateNewEntity($"{NameOf(Ohmni.OhmniAttributeTypeEntity)} with '{NameOf(Ohmni.OhmniAttributeTypeEntity.AttributeTypeId)}' of  {id}")
            Assert.IsTrue(r.Success, r.Details)

            id = 1
            actualFetched = entity.FetchUsingKey(connection, id)
            Assert.IsTrue(actualFetched, $"key {id} should exist")
            r = entity.ValidateStoredEntity($"Stored {NameOf(Ohmni.OhmniAttributeTypeEntity)} with '{NameOf(Ohmni.OhmniAttributeTypeEntity.AttributeTypeId)}' of  {id}")
            Assert.IsTrue(r.Success, r.Details)

            Dim name As String = AttributeType.TrackingTolerance.ToString
            entity.Name = name
            r = entity.ValidateChangedEntity($"Changed {NameOf(Ohmni.OhmniAttributeTypeEntity)} '{NameOf(Ohmni.OhmniAttributeTypeEntity.Name)}' to {name}")
            Assert.IsTrue(r.Success, r.Details)

            actualFetched = entity.FetchUsingUniqueIndex(connection)
            Assert.IsTrue(actualFetched, $"Name {name} should exist")
            Assert.AreEqual(name, entity.Name, $"Name {name} should exist")
            r = entity.ValidateStoredEntity($"{NameOf(Ohmni.OhmniAttributeTypeEntity)} with '{NameOf(Ohmni.OhmniAttributeTypeEntity.Name)}' of {name}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = 2
            Dim actualCount As Integer = entity.FetchAllEntities(connection)
            Assert.IsTrue(actualCount >= expectedCount, $"Expected at least {expectedCount} found {actualCount} Attribute types")
        End Using
    End Sub

    #End Region

    #Region " LOT "

    ''' <summary> Assert adding lot entity. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> An OhmniLotEntity. </returns>
    Public Shared Function AssertAddingLotEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                 ByVal partNumber As String, ByVal lotNumber As String) As CincoLotEntity
        Dim lot As CincoLotEntity = New CincoLotEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = CincoLotBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim partEntity As CincoPartEntity = OhmniAuditor.AssertAddingPartEntity(site, provider, partNumber)

            lot = New CincoLotEntity With {.lotNumber = lotNumber, .PartAutoId = partEntity.PartAutoId, .partNumber = partEntity.PartNumber}
            Dim actualInserted As Boolean = lot.Obtain(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(CincoLotEntity)}  with '{NameOf(CincoLotEntity.LotNumber)}' of {lot.LotNumber} should have been inserted")
            r = lot.ValidateStoredEntity($"Insert {NameOf(CincoLotEntity)} with '{NameOf(CincoLotEntity.LotNumber)}' of {lotNumber}")
            Assert.IsTrue(r.Success, r.Details)
        End Using
        Return lot

    End Function

    ''' <summary> Asserts lot entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertLotEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Dim id As Integer = 3
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = CincoLotBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of CincoLotNub)()
            connection.DeleteAll(Of CincoPartNub)()
            Dim partNumber As String = "Part1"
            Dim partId As Integer = 1
            Dim part As New CincoPartEntity With {.partNumber = partNumber}
            part.Insert(connection)
            partId = part.PartAutoId
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = False

            Dim expectedCount As Integer = 0
            Dim lot As New CincoLotEntity
            actualFetched = lot.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(CincoLotEntity)} with '{NameOf(CincoLotEntity.LotAutoId)}' of {id} should not exist")
            r = lot.ValidateNewEntity($"New {NameOf(CincoLotEntity)} with '{NameOf(CincoLotEntity.LotAutoId)}' of {id}")
            Assert.IsTrue(r.Success, r.Details)
            lot.Clear()

            Dim lotNumber As String = "lot1"
            lot = New CincoLotEntity With {.lotNumber = lotNumber, .partAutoId = partId, .partNumber = partNumber}
            Dim actualInserted As Boolean = lot.Insert(connection)
            expectedCount += 1
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(CincoLotEntity)}  with '{NameOf(CincoLotEntity.LotNumber)}' of {lot.LotNumber} should have been inserted")
            r = lot.ValidateStoredEntity($"Insert {NameOf(CincoLotEntity)} with '{NameOf(CincoLotEntity.LotNumber)}' of {lotNumber}")
            Assert.IsTrue(r.Success, r.Details)

            lot.FetchUsingKey(connection)
            Dim expectedTimestamp As DateTime = DateTime.UtcNow
            Dim actualTimeStamp As DateTime = lot.Timestamp
            Asserts.Instance.AreEqual(expectedTimestamp, actualTimeStamp, TimeSpan.FromSeconds(10), "Expected timestamp")

            lot.FetchPartEntity(connection)
            Assert.AreEqual(part.PartNumber, lot.PartEntity.PartNumber, $"Fetch {NameOf(CincoPartEntity)} using '{NameOf(CincoLotEntity.PartAutoId)}' of {lot.PartAutoId} should match the {NameOf(CincoPartEntity)} '{NameOf(CincoPartEntity.PartNumber)} '")
            r = lot.PartEntity.ValidateStoredEntity($"Fetched {NameOf(CincoPartEntity)} from  {NameOf(CincoLotEntity)} '[{NameOf(CincoLotEntity.LotNumber)},{NameOf(CincoLotEntity.PartNumber)}]' of [{lotNumber},{partNumber}]")
            Assert.IsTrue(r.Success, r.Details)

            lotNumber = "Lot2"
            lot.LotNumber = lotNumber
            r = lot.ValidateChangedEntity($"Changed {NameOf(CincoLotEntity)} with '{NameOf(CincoLotEntity.LotNumber)}' of {lotNumber}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualUpdate As Boolean = lot.Update(connection, UpdateModes.Refetch)
            Dim expectedUpdate As Boolean = True
            Assert.AreEqual(expectedUpdate, actualUpdate, $"{NameOf(CincoLotEntity)} with '{NameOf(CincoLotEntity.LotNumber)}' of {lot.LotNumber} should be updated")
            r = lot.ValidateStoredEntity($"Updated {NameOf(CincoLotEntity)} with '{NameOf(CincoLotEntity.LotNumber)}' of {lotNumber}")
            Assert.IsTrue(r.Success, r.Details)

            actualUpdate = lot.Update(connection, UpdateModes.Refetch)
            expectedUpdate = False
            Assert.AreEqual(expectedUpdate, actualUpdate, $"Updated {NameOf(CincoLotEntity)} with unchanged '{NameOf(CincoLotEntity.LotNumber)}' of {lot.LotNumber} should be clean")
            r = lot.ValidateStoredEntity($"Updated {NameOf(CincoLotEntity)} with unchanged '{NameOf(CincoLotEntity.LotNumber)}' of {lot.LotNumber}")
            Assert.IsTrue(r.Success, r.Details)

            ' Insert must be done on a new lot; setting the lot number and inserting leaves the proxy 'dirty'. lot.LotNumber = "Lot1"
            lot = New CincoLotEntity With {.lotNumber = "Lot1", .partAutoId = partId, .partNumber = partNumber}
            actualInserted = lot.Insert(connection)
            expectedCount += 1
            Assert.AreEqual(expectedCount, CincoLotEntity.CountLots(connection, lot.PartNumber), $"{expectedCount} lots should be counted for {lot.PartNumber}")
            Assert.IsTrue(actualInserted, $"Lot {lot.LotNumber} should have been inserted")

            Assert.AreEqual(1, CincoLotEntity.CountEntities(connection, lot.LotNumber), $"Single lot should be counted for {lot.PartNumber}.{lot.LotNumber}")
            Assert.IsTrue(CincoLotEntity.IsExists(connection, lot.LotNumber),
                              $"Lot {lot.PartNumber}.{lot.LotNumber} should {NameOf(CincoLotEntity.IsExists)}")

            lotNumber = lot.LotNumber
            Dim partAutoId As Integer = lot.PartAutoId
            actualFetched = lot.Obtain(connection)
            expectedFetched = True
            Assert.AreEqual(expectedFetched, actualFetched, $"Lot {lot.PartNumber}.{lot.LotNumber} should have been obtained")
            Assert.AreEqual(partNumber, lot.PartNumber, $"Obtained Lot {lot.PartNumber}.{lot.LotNumber} should match part number")
            Assert.AreEqual(partAutoId, lot.PartAutoId, $"Obtained Part auto id {lot.PartAutoId} should match part auto id")

            connection.DeleteAll(Of CincoLotNub)()
            expectedCount = 10
            For i As Integer = 1 To expectedCount
                lot = New CincoLotEntity With {.lotNumber = $"Lot{i}", .partAutoId = part.PartAutoId, .partNumber = part.PartNumber}
                actualInserted = lot.Insert(connection)
                expectedInserted = True
                Assert.AreEqual(expectedInserted, actualInserted, $"Lot {lot.LotNumber} should have been inserted")
            Next
            Dim actualCount As Integer = lot.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple Lots should have been inserted")

            lotNumber = lot.LotNumber
            Dim actualDelete As Boolean = lot.Delete(connection)
            Dim expectedDelete As Boolean = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Lot {lotNumber} should be deleted")
            expectedCount -= 1

            Dim lot0 As ICincoLot = lot.Lots(0)
            actualDelete = CincoLotEntity.Delete(connection, lot0.LotAutoId)
            expectedDelete = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Lot {lot0.LotNumber} should be deleted")
            expectedCount -= 1
            part = New CincoPartEntity
            part.FetchUsingKey(connection, partId)
            actualCount = part.FetchLots(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple Lots should have been fetched for part {part.PartNumber}")

            Dim samePart As New CincoPartEntity
            samePart.FetchLots(connection, part.PartAutoId)
            actualCount = samePart.Lots.Count
            Assert.AreEqual(part.PartAutoId, samePart.PartAutoId, $"same part should be fetched")
            Assert.AreEqual(part.PartNumber, samePart.PartNumber, $"same part should be fetched")
            Assert.AreEqual(expectedCount, actualCount, $"multiple Lots should have been fetched")

        End Using
    End Sub

    #End Region

    #Region " PART "

    ''' <summary> Assert add part entity. </summary>
    ''' <remarks> David, 5/17/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <returns> An OhmniPartEntity. </returns>
    Public Shared Function AssertAddingPartEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal partNumber As String) As CincoPartEntity
        Dim part As New CincoPartEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            part.PartNumber = partNumber
            Dim actualInserted As Boolean = part.Obtain(connection)
            Assert.AreEqual(partNumber, part.PartNumber, True, $"{NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} of {partNumber} must equals the saved number {part.PartNumber}")
            r = part.ValidateStoredEntity($"Inserted {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} of {partNumber}")
            Assert.IsTrue(r.Success, r.Details)
        End Using
        Return part
    End Function

    ''' <summary> Asserts part entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertPartEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Dim id As Integer = 3
        Dim part As New CincoPartEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim expectedCount As Integer = 0
            Dim tableName As String = CincoPartBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of CincoPartNub)()
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = part.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.AutoId)} of {id} should not exist")
            r = part.ValidateNewEntity($"New {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.AutoId)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            Dim partNumber As String = "Part1"
            part.PartNumber = partNumber
            Dim actualInserted As Boolean = part.Insert(connection)
            Assert.AreEqual(partNumber, part.PartNumber, $"{NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} of {partNumber} must equals the saved number {part.PartNumber}")
            r = part.ValidateStoredEntity($"Inserted {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} of {partNumber}")
            Assert.IsTrue(r.Success, r.Details)

            part.FetchUsingKey(connection)
            Dim expectedTimestamp As DateTime = DateTime.Now
            Dim actualTimeStamp As DateTime = part.Timestamp.ToLocalTime
            Asserts.Instance.AreEqual(expectedTimestamp, actualTimeStamp, TimeSpan.FromSeconds(10), "Expected timestamp")

            expectedCount += 1
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Part {part.PartNumber} should have been inserted")

            partNumber = "Part2"
            part.PartNumber = partNumber
            r = part.ValidateChangedEntity($"Changed {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} to {partNumber}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualUpdated As Boolean = part.Update(connection, UpdateModes.Refetch)
            Dim expectedUpdated As Boolean = True
            Assert.AreEqual(expectedUpdated, actualUpdated, $"{NameOf(Entities.PartEntity)} with {part.PartNumber} update should match")

            actualUpdated = part.Update(connection, UpdateModes.Refetch)
            Assert.IsFalse(actualUpdated, $"Part {part.PartNumber} should not be updated")
            Dim actualCount As Integer = CincoPartEntity.CountEntities(connection, part.PartNumber)
            Assert.AreEqual(expectedCount, actualCount, $"Single part should be counted for {part.PartNumber}")
            Assert.IsTrue(CincoPartEntity.IsExists(connection, part.PartNumber), $"Part {part.PartNumber} should {NameOf(isr.Dapper.Entities.PartEntity.IsExists)}")

            ' set part proxy dirty
            actualUpdated = part.Update(connection, UpdateModes.Refetch Or UpdateModes.ForceUpdate)
            Assert.IsTrue(actualUpdated, $"Part {part.PartNumber} should be updated after setting proxy dirty")

            actualUpdated = part.Update(connection)
            Assert.IsTrue(actualUpdated, $"Part {part.PartNumber} should be clean after update")

            partNumber = part.PartNumber
            Dim AutoId As Integer = part.PartAutoId
            actualFetched = part.Obtain(connection)
            expectedFetched = True
            Assert.AreEqual(expectedFetched, actualFetched, $"Part {part.PartNumber} should have been obtained")
            Assert.AreEqual(partNumber, part.PartNumber, $"Obtained Part number {part.PartNumber} should match")
            Assert.AreEqual(AutoId, part.PartAutoId, $"Obtained Part auto id {part.PartAutoId} should match")


            connection.DeleteAll(Of CincoPartNub)()
            expectedCount = 10
            For i As Integer = 1 To expectedCount
                part = New CincoPartEntity With {.partNumber = $"Part{i}"}
                actualInserted = part.Insert(connection)
                expectedInserted = True
                Assert.AreEqual(expectedInserted, actualInserted, $"Part {part.PartNumber} should have been inserted")
            Next
            actualCount = part.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple parts should have been inserted")

            partNumber = part.PartNumber
            Dim actualDelete As Boolean = part.Delete(connection)
            Dim expectedDelete As Boolean = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Part {partNumber} should be deleted")

            Dim part0 As CincoPartEntity = part.Parts(0)
            actualDelete = CincoPartEntity.Delete(connection, part0.PartAutoId)
            expectedDelete = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Part {part0.PartNumber} should be deleted")

            Dim expectedUniquePartNumber As Boolean = True
            Dim actualUniqueLabel As Boolean = CincoPartBuilder.UsingUniqueLabel(connection)
            Assert.AreEqual(expectedUniquePartNumber, actualUniqueLabel, $"{NameOf(Ohmni.CincoPartEntity)} '{NameOf(CincoPartBuilder.UsingUniqueLabel)}' should match")

        End Using
    End Sub

    #End Region

    #Region " PART ATTRIBUTE RANGE "

    ''' <summary> Asserts part attribute range. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertPartAttributeRangeEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = OhmniPartAttributeRangeBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of CincoPartNub)
            connection.DeleteAll(Of OhmniPartAttributeRangeNub)

            Dim part As New CincoPartEntity With {.PartNumber = "Part1"}
            part.Insert(connection)

            Dim attributeType As AttributeType = attributeType.Tcr
            Dim partAttributeRange As New OhmniPartAttributeRangeEntity With {.PartAutoId = part.PartAutoId,
                                                .AttributeTypeId = attributeType, .Min = 0, .Max = 0.1}

            Dim actualInserted As Boolean = partAttributeRange.Insert(connection)
            Dim expectedCount As Integer = 0
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(OhmniPartAttributeRangeEntity)} should have been inserted")
            r = partAttributeRange.ValidateStoredEntity($"Inserted {NameOf(OhmniPartAttributeRangeEntity)} for part {part.PartNumber}")
            Assert.IsTrue(r.Success, r.Details)
            expectedCount += 1

            Dim count As Integer = part.FetchPartAttributeRanges(connection)
            Assert.AreEqual(expectedCount, count, $"Part entity should bring the correct number of {NameOf(OhmniPartAttributeRangeEntity)}")
            connection.DeleteAll(Of OhmniPartAttributeRangeNub)()
        End Using
    End Sub

    #End Region

    #Region " PART ATTRIBUTE REAL "

    ''' <summary> Asserts part attribute Value. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertPartAttributeValueEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = OhmniPartAttributeValueBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of CincoPartNub)
            connection.DeleteAll(Of OhmniPartAttributeValueNub)

            Dim part As New CincoPartEntity With {.PartNumber = "Part1"}
            part.Insert(connection)

            Dim attributeType As AttributeType = attributeType.NominalValue
            Dim partAttributeValue As New OhmniPartAttributeValueEntity With {.PartAutoId = part.PartAutoId, .AttributeTypeId = attributeType, .Value = 1}

            Dim actualInserted As Boolean = partAttributeValue.Insert(connection)
            Dim expectedCount As Integer = 0
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(OhmniPartAttributeValueEntity)} should have been inserted")
            r = partAttributeValue.ValidateStoredEntity($"Inserted {NameOf(OhmniPartAttributeValueEntity)} for part {part.PartNumber}")
            Assert.IsTrue(r.Success, r.Details)

            expectedCount += 1

            Dim count As Integer = part.FetchPartAttributeValues(connection)
            Assert.AreEqual(expectedCount, count, $"Part entity should bring the correct number of {NameOf(OhmniPartAttributeValueEntity)}")
            connection.DeleteAll(Of OhmniPartAttributeValueNub)()
        End Using
    End Sub

    #End Region

    #Region " RELEASE HISTORY "

    ''' <summary> Asserts release history entity. </summary>
    ''' <remarks>
    ''' Time zone issue with SQL Light.  Storing the SQL value in UTC returns back time in the
    ''' timezone of the 'server'.
    ''' </remarks>
    ''' <param name="site">                  The site. </param>
    ''' <param name="provider">              The provider. </param>
    ''' <param name="activeDatabaseVersion"> The active database version. </param>
    Public Shared Sub AssertReleaseHistoryEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal activeDatabaseVersion As String)
        Dim expectedActiveVersion As String = "1.0.0"
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = CincoReleaseHistoryBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = False
            Dim releaseHistory As New CincoReleaseHistoryEntity
            actualFetched = releaseHistory.FetchUsingKey(connection, expectedActiveVersion)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Ohmni.CincoReleaseHistoryEntity)} {NameOf(Ohmni.CincoReleaseHistoryEntity.VersionNumber)} Of {expectedActiveVersion} should Not exist")
            r = releaseHistory.ValidateNewEntity($"New {NameOf(Ohmni.CincoReleaseHistoryEntity)} With {NameOf(Ohmni.CincoReleaseHistoryEntity.VersionNumber)} Of {expectedActiveVersion}")
            Assert.IsTrue(r.Success, r.Details)
            releaseHistory.Clear()

            ' clear the table; the table maybe empty, in which case the action returns False.
            connection.DeleteAll(Of CincoReleaseHistoryNub)

            Dim expectedCount As Integer = 0
            Dim actualCount As Integer = CincoReleaseHistoryBuilder.UpdateReleaseHistory(connection, activeDatabaseVersion,
                                                                                    DateTime.Now, "Latest release")
            Assert.IsTrue(actualCount >= 0, $"Release {activeDatabaseVersion} should exist Or updated")
            expectedCount += 1

            expectedActiveVersion = activeDatabaseVersion
            actualFetched = releaseHistory.FetchUsingKey(connection, expectedActiveVersion)
            expectedFetched = True
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Ohmni.CincoReleaseHistoryEntity)} With {NameOf(Ohmni.CincoReleaseHistoryEntity.VersionNumber)} Of {expectedActiveVersion} should exist")
            r = releaseHistory.ValidateStoredEntity($"Existing {NameOf(Ohmni.CincoReleaseHistoryEntity)} With {NameOf(Ohmni.CincoReleaseHistoryEntity.VersionNumber)} Of {expectedActiveVersion}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedUpdate As Boolean = True
            Dim actualUpdate As Boolean = True
            actualUpdate = releaseHistory.Update(connection, UpdateModes.Refetch)
            Assert.IsFalse(actualUpdate, $"{NameOf(Ohmni.CincoReleaseHistoryEntity)} With {NameOf(Ohmni.CincoReleaseHistoryEntity.VersionNumber)} Of {expectedActiveVersion} should Not be updated")
            r = releaseHistory.ValidateStoredEntity($"Unchanged {NameOf(Ohmni.CincoReleaseHistoryEntity)} With {NameOf(Ohmni.CincoReleaseHistoryEntity.VersionNumber)} Of {expectedActiveVersion}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedInserted As Boolean = True
            Dim actualInserted As Boolean = False
            Dim expectedActive As Boolean = True
            Dim notActiveVersion As String = "0.0.1000"
            releaseHistory = New CincoReleaseHistoryEntity With {.VersionNumber = notActiveVersion, .IsActive = expectedActive, .Description = "alpha release",
                                                            .Timestamp = DateTime.Now.Subtract(TimeSpan.FromDays(expectedCount))}
            actualInserted = releaseHistory.Insert(connection)
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Ohmni.CincoReleaseHistoryEntity)} With {NameOf(Ohmni.CincoReleaseHistoryEntity.VersionNumber)} Of {releaseHistory.VersionNumber} should have been inserted")
            Assert.AreEqual(expectedActive, releaseHistory.IsActive, $"{NameOf(Ohmni.CincoReleaseHistoryEntity)} '{NameOf(releaseHistory.IsActive)}' should have been set true")
            r = releaseHistory.ValidateStoredEntity($"Added {NameOf(Ohmni.CincoReleaseHistoryEntity)} with {NameOf(Ohmni.CincoReleaseHistoryEntity.VersionNumber)} of {releaseHistory.VersionNumber} should be clean")
            Assert.IsTrue(r.Success, r.Details)

            expectedCount += 1

            actualFetched = releaseHistory.FetchUsingKey(connection)
            expectedFetched = True
            Assert.AreEqual(expectedFetched, actualFetched, $"release {notActiveVersion} should exist")

            actualUpdate = releaseHistory.Update(connection, UpdateModes.Refetch)
            Assert.IsFalse(actualUpdate, $"Release History {releaseHistory.VersionNumber} should not be updated")

            expectedActive = False
            releaseHistory.IsActive = expectedActive
            expectedUpdate = True
            actualUpdate = releaseHistory.Update(connection, UpdateModes.Refetch)
            Assert.AreEqual(expectedUpdate, actualUpdate, $"{NameOf(Ohmni.CincoReleaseHistoryEntity)} with {NameOf(Ohmni.CincoReleaseHistoryEntity.VersionNumber)} of {releaseHistory.VersionNumber} should be updated")
            r = releaseHistory.ValidateStoredEntity($"Updated {NameOf(Ohmni.CincoReleaseHistoryEntity)} with {NameOf(Ohmni.CincoReleaseHistoryEntity.VersionNumber)} of {releaseHistory.VersionNumber}")
            Assert.IsTrue(r.Success, r.Details)
            ' prevent unique timestamp exception
            Dim localTimestamp As Date = releaseHistory.Timestamp.ToLocalTime.Subtract(TimeSpan.FromSeconds(6))
            releaseHistory.UpdateLocalTimeStamp(connection, localTimestamp)
            ' SQL Server time stored with a precision scale of 2 for 6 bytes.
            Asserts.Get.AreEqual(localTimestamp, releaseHistory.Timestamp.ToLocalTime, TimeSpan.FromMilliseconds(11), $"{NameOf(Ohmni.CincoReleaseHistoryEntity)} with {NameOf(Ohmni.CincoReleaseHistoryEntity.Timestamp)} timestamp should match")

            notActiveVersion = "0.1.1000"
            expectedActive = False
            releaseHistory = New CincoReleaseHistoryEntity With {.VersionNumber = notActiveVersion, .IsActive = expectedActive, .Description = "beta release"}
            actualInserted = releaseHistory.Insert(connection)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Release History {releaseHistory.VersionNumber} should have been inserted")

            ' prevent unique timestamp exception
            localTimestamp = releaseHistory.Timestamp.ToLocalTime.Subtract(TimeSpan.FromSeconds(4))
            releaseHistory.UpdateLocalTimeStamp(connection, localTimestamp)
            expectedCount += 1

            releaseHistory = New CincoReleaseHistoryEntity With {.VersionNumber = "0.2.1000", .IsActive = False, .Description = "release candidate"}
            actualInserted = releaseHistory.Insert(connection)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Release History {releaseHistory.VersionNumber} should have been inserted")
            ' prevent unique timestamp exception
            localTimestamp = releaseHistory.Timestamp.ToLocalTime.Subtract(TimeSpan.FromSeconds(2))
            releaseHistory.UpdateLocalTimeStamp(connection, localTimestamp)
            expectedCount += 1

            actualCount = releaseHistory.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple ReleaseHistorys should have been inserted")

            Dim VersionNumber As String = releaseHistory.VersionNumber
            Dim actualDelete As Boolean = releaseHistory.Delete(connection)
            Dim expectedDelete As Boolean = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Release History {VersionNumber} should be deleted")
            expectedCount -= 1

            actualCount = releaseHistory.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple ReleaseHistorys should have been inserted")

            ' find inactive release history.
            Dim recordIndex As Integer = expectedCount - 1
            Dim releaseHistory1 As ICincoReleaseHistory = releaseHistory.ReleaseHistories(recordIndex)
            Do Until Not releaseHistory1.IsActive OrElse recordIndex = 0
                recordIndex -= 1
                releaseHistory1 = releaseHistory.ReleaseHistories(recordIndex)
            Loop
            actualDelete = CincoReleaseHistoryEntity.Delete(connection, releaseHistory1.VersionNumber)
            expectedDelete = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Release History {releaseHistory1.VersionNumber} should be deleted")
            expectedCount -= 1

            ' test active release
            Dim outcome As (Success As Boolean, Details As String) = CincoReleaseHistoryEntity.TryVerifyDatabaseVersion(connection, expectedActiveVersion)
            Assert.IsTrue(outcome.Success, $"version {expectedActiveVersion} should be active; {outcome.Details}")
            outcome = CincoReleaseHistoryEntity.TryVerifyDatabaseVersion(connection, notActiveVersion)
            Assert.IsFalse(outcome.Success, $"version {notActiveVersion} should not be active")

        End Using
    End Sub

    #End Region

    #Region " SESSION: OHMNI "

    ''' <summary> Assert session entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="testLevelId">   Identifier for the test level. </param>
    ''' <param name="testLevelName"> Name of the test level. </param>
    Public Shared Sub AssertSessionEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                          ByVal testLevelId As Integer, ByVal testLevelName As String)
        Dim id As Integer = 3
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = CincoSessionBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of CincoStationNub)
            connection.DeleteAll(Of CincoSessionNub)

            Dim expectedTimeZoneId As String = TimeZone.CurrentTimeZone.StandardName
            Dim expectedComputerName As String = isr.Dapper.Entities.Tests.Auditor.ServerComputer.Name
            Dim station As New CincoStationEntity With {.StationName = expectedComputerName, .ComputerName = expectedComputerName, .TimeZoneId = expectedTimeZoneId}
            Dim actualInserted As Boolean = station.Obtain(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Station {station.StationName } should have been inserted")
            Dim actualFetched As Boolean
            Dim expectedFetched As Boolean = False
            Dim session As New CincoSessionEntity
            actualFetched = session.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Ohmni.CincoSessionEntity)} with '{NameOf(Ohmni.CincoSessionEntity.SessionAutoId)}' of {id} should not exist")
            r = session.ValidateNewEntity($"New {NameOf(Ohmni.CincoSessionEntity)} with '{NameOf(Ohmni.CincoSessionEntity.SessionAutoId)}' of {id}")
            Assert.IsTrue(r.Success, r.Details)
            session.Clear()

            Dim expectedTestLevel As Integer = testLevelId
            session = New CincoSessionEntity With {.StationAutoId = station.StationAutoId, .StationName = station.StationName,
                                                        .testLevelId = expectedTestLevel, .Description = $"Session {testLevelName}"}
            actualInserted = session.Insert(connection)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Session {session.StationAutoId} should have been inserted")
            r = session.ValidateStoredEntity($"Inserted {NameOf(Ohmni.CincoSessionEntity)} for '{NameOf(Ohmni.CincoSessionEntity.StationName)}' of {station.StationName}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = 1
            Dim actualCount As Integer = session.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple Sessions should have been inserted")

        End Using
    End Sub

    ''' <summary> Assert session entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="site">      The site. </param>
    ''' <param name="provider">  The provider. </param>
    ''' <param name="testLevel"> The test level. </param>
    Public Shared Sub AssertSessionEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal testLevel As CincoTestLevel)
        OhmniAuditor.AssertSessionEntity(site, provider, CInt(testLevel), testLevel.Description)
    End Sub

    ''' <summary> Assert add session entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="stationAutoId"> Identifier for the station automatic. </param>
    ''' <param name="stationName">   Name of the station. </param>
    ''' <param name="testLevel">     The test level. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function AssertAddSessionEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                  ByVal stationAutoId As Integer, ByVal stationName As String, ByVal testLevel As Integer) As Integer
        Dim id As Integer = 3
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = CincoSessionBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of CincoSessionNub)

            Dim actualFetched As Boolean
            Dim session As New CincoSessionEntity
            actualFetched = session.FetchUsingKey(connection, id)
            Assert.IsFalse(actualFetched, $"{NameOf(Ohmni.CincoSessionEntity)} with '{NameOf(Ohmni.CincoSessionEntity.SessionAutoId)}' of {id} should not exist")
            r = session.ValidateNewEntity($"New {NameOf(Ohmni.CincoSessionEntity)} with '{NameOf(Ohmni.CincoSessionEntity.SessionAutoId)}' of {id}")
            Assert.IsTrue(r.Success, r.Details)
            session.Clear()

            session = New CincoSessionEntity With {.stationAutoId = stationAutoId, .stationName = stationName,
                                              .TestLevelId = testLevel, .Description = $"Session {testLevel}"}
            Dim actualInserted As Boolean = session.Insert(connection)
            Assert.IsTrue(actualInserted, $"{NameOf(Ohmni.CincoSessionEntity)} for '{NameOf(Ohmni.CincoSessionEntity.StationName)}' of {stationName} should have been inserted")
            r = session.ValidateStoredEntity($"Insert {NameOf(Ohmni.CincoSessionEntity)} for '{NameOf(Ohmni.CincoSessionEntity.StationName)}' of {stationName}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = 1
            Dim actualCount As Integer = session.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple Sessions should have been inserted")
            Return session.SessionAutoId
        End Using
    End Function

    #End Region

    #Region " STATION "

    ''' <summary> Asserts station entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">                        The site. </param>
    ''' <param name="provider">                    The provider. </param>
    ''' <param name="expectedUsingNativeTracking"> True to expected using native tracking. </param>
    Public Shared Sub AssertStationEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal expectedUsingNativeTracking As Boolean)
        Dim expectedComputerName As String = isr.Dapper.Entities.Tests.Auditor.ServerComputer.Name
        Dim expectedTimeZoneId As String = TimeZone.CurrentTimeZone.StandardName
        Dim id As Integer = 4
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = CincoStationBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of CincoStationNub)()
            Dim station As New CincoStationEntity With {.UsingNativeTracking = expectedUsingNativeTracking}

            ' connection.DeleteAll(Of StationNub)()
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = station.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {id} should not exist")
            r = station.ValidateNewEntity($"New {NameOf(Ohmni.CincoStationEntity)} with key {id}")
            Assert.IsTrue(r.Success, r.Details)
            station.Clear()

            Dim expectedInserted As Boolean = True
            Dim actualInserted As Boolean = False
            station = New CincoStationEntity With {.stationName = expectedComputerName, .ComputerName = expectedComputerName, .TimeZoneId = expectedTimeZoneId}
            station.UsingNativeTracking = expectedUsingNativeTracking
            actualInserted = station.Insert(connection)
            Assert.AreEqual(expectedInserted, actualInserted, $"Station {station.StationName} should have been inserted")
            Assert.AreEqual(expectedTimeZoneId, station.TimeZoneId, $"Station {NameOf(station.TimeZoneId)} should have been set true")
            r = station.ValidateStoredEntity($"Inserted {NameOf(Ohmni.CincoStationEntity)} with {NameOf(Ohmni.CincoStationEntity.ComputerName)} of {expectedComputerName}")
            Assert.IsTrue(r.Success, r.Details)

            station.FetchUsingKey(connection)
            Dim expectedLocalTimestamp As DateTime = DateTime.Now
            Dim actualLocalTimeStamp As DateTime = station.Timestamp.ToLocalTime
            Asserts.Instance.AreEqual(expectedLocalTimestamp, actualLocalTimeStamp, TimeSpan.FromSeconds(10), "Expected local time timestamp")

            Dim expectedLocaleTimeStamp As DateTimeOffset = actualLocalTimeStamp
            Dim actualLocaleTimeStamp As DateTimeOffset = station.LocaleTimestamp
            Asserts.Instance.AreEqual(expectedLocaleTimeStamp, actualLocaleTimeStamp, TimeSpan.FromSeconds(0), "Expected locale time timestamp")

            station.StationName = "New Name"
            r = station.ValidateChangedEntity($"Changed {NameOf(Ohmni.CincoStationEntity)} with {NameOf(Ohmni.CincoStationEntity.ComputerName)} of {station.ComputerName}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualUpdate As Boolean = station.Update(connection, UpdateModes.Refetch)
            Dim expectedUpdate As Boolean = True
            Assert.IsTrue(actualUpdate,
                          $"Changed {NameOf(Ohmni.CincoStationEntity)} with {NameOf(Ohmni.CincoStationEntity.ComputerName)} of {station.ComputerName} should be updated")
            r = station.ValidateStoredEntity($"Updated #1 {NameOf(Ohmni.CincoStationEntity)} with {NameOf(Ohmni.CincoStationEntity.ComputerName)} of {station.ComputerName}")
            Assert.IsTrue(r.Success, r.Details)

            actualUpdate = station.Update(connection, UpdateModes.Refetch)
            actualUpdate = station.Update(connection, UpdateModes.Refetch)
            Assert.IsFalse(actualUpdate, $"Station {station.ComputerName} should not be updated")
            r = station.ValidateStoredEntity($"Updated #2 {NameOf(Ohmni.CincoStationEntity)} with {NameOf(Ohmni.CincoStationEntity.ComputerName)} of {station.ComputerName}")
            Assert.IsTrue(r.Success, r.Details)

            station = New CincoStationEntity With {.stationName = "Station1", .ComputerName = "Computer1", .TimeZoneId = expectedTimeZoneId}
            station.UsingNativeTracking = expectedUsingNativeTracking
            actualInserted = station.Insert(connection)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted,
                            $"{NameOf(Ohmni.CincoStationEntity)} with {NameOf(Ohmni.CincoStationEntity.ComputerName)} of {station.ComputerName} should have been inserted")

            station = New CincoStationEntity With {.stationName = "Station2", .ComputerName = "Computer2", .TimeZoneId = expectedTimeZoneId}
            station.UsingNativeTracking = expectedUsingNativeTracking
            actualInserted = station.Insert(connection)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted,
                            $"{NameOf(Ohmni.CincoStationEntity)} with {NameOf(Ohmni.CincoStationEntity.ComputerName)} of {station.ComputerName} should have been inserted")

            Dim expectedCount As Integer = 3
            Dim actualCount As Integer = station.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple {NameOf(Ohmni.CincoStationEntity)}'s should have been inserted")

            Dim stationName As String = station.StationName
            Dim actualDelete As Boolean = station.Delete(connection)
            Dim expectedDelete As Boolean = True
            Assert.AreEqual(expectedDelete, actualDelete,
                            $"{NameOf(Ohmni.CincoStationEntity)} with {NameOf(Ohmni.CincoStationEntity.StationName)} of {station.StationName} should be deleted")
            station.Clear()

            ' get active station
            station.ComputerName = expectedComputerName
            station.Obtain(connection)
            Assert.AreEqual(expectedComputerName, station.ComputerName,
                            $"{NameOf(Ohmni.CincoStationEntity)} with {NameOf(Ohmni.CincoStationEntity.StationName)} of {station.StationName} should be the active station")

            stationName = station.StationName
            Dim newStationName As String = $"{stationName}.Changed"
            Dim expectedStationName As String = newStationName
            id = station.StationAutoId
            If connection.State = Data.ConnectionState.Closed Then connection.Open()
            Using transaction As Data.IDbTransaction = connection.BeginTransaction()
                Try
                    station.StationName = newStationName
                    r = station.ValidateChangedEntity($"Changed {NameOf(Ohmni.CincoStationEntity)} '{NameOf(Ohmni.CincoStationEntity.StationName)}' to {newStationName}")
                    Assert.IsTrue(r.Success, r.Details)
                    Dim transactedConnection As New Global.Dapper.TransactedConnection(connection, transaction)
                    expectedStationName = newStationName
                    actualUpdate = station.Update(transactedConnection, UpdateModes.Refetch)
                    Assert.IsTrue(actualUpdate, $"Changed {NameOf(Ohmni.CincoStationEntity)} '{NameOf(Ohmni.CincoStationEntity.StationName)}' to {newStationName} should be updated")
                    transaction.Commit()
                    station.FetchUsingKey(connection)
                    Assert.AreEqual(expectedStationName, station.StationName,
                                $"{NameOf(Ohmni.CincoStationEntity)} '{NameOf(Ohmni.CincoStationEntity.StationName)}' should be restored after transaction rollback")
                Catch
                    Throw
                End Try
            End Using
            Using transaction As Data.IDbTransaction = connection.BeginTransaction()
                Try
                    expectedStationName = station.StationName
                    station.StationName = stationName
                    r = station.ValidateChangedEntity($"Changed {NameOf(Ohmni.CincoStationEntity)} '{NameOf(Ohmni.CincoStationEntity.StationName)}' to {newStationName}")
                    Assert.IsTrue(r.Success, r.Details)
                    Dim transactedConnection As New Global.Dapper.TransactedConnection(connection, transaction)
                    actualUpdate = station.Update(transactedConnection, UpdateModes.Refetch)
                    Assert.IsTrue(actualUpdate, $"Changed {NameOf(Ohmni.CincoStationEntity)} '{NameOf(Ohmni.CincoStationEntity.StationName)}' to {newStationName} should be updated")
                    transaction.Rollback()
                    station.FetchUsingKey(connection)
                    Assert.AreEqual(expectedStationName, station.StationName,
                                    $"{NameOf(Ohmni.CincoStationEntity)} '{NameOf(Ohmni.CincoStationEntity.StationName)}' should be restored after transaction rollback")
                Catch
                    Throw
                End Try
            End Using

        End Using
    End Sub

    ''' <summary> Asserts adding a station entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <returns> A (AutoId As Integer, Name As String) </returns>
    Public Shared Function AssertAddStationEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase) As (AutoId As Integer, Name As String)
        Dim expectedComputerName As String = isr.Dapper.Entities.Tests.Auditor.ServerComputer.Name
        Dim expectedTimeZoneId As String = TimeZone.CurrentTimeZone.StandardName
        Dim id As Integer = 4
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = CincoStationBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of CincoStationNub)()
            Dim station As New CincoStationEntity

            ' connection.DeleteAll(Of StationNub)()
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = station.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {id} should not exist")
            r = station.ValidateNewEntity($"New {NameOf(Ohmni.CincoStationEntity)} with key {id}")
            Assert.IsTrue(r.Success, r.Details)
            station.Clear()

            Dim expectedInserted As Boolean = True
            Dim actualInserted As Boolean = False
            station = New CincoStationEntity With {.StationName = expectedComputerName, .ComputerName = expectedComputerName, .TimeZoneId = expectedTimeZoneId}
            actualInserted = station.Insert(connection)
            Assert.AreEqual(expectedInserted, actualInserted, $"Station {station.StationName } should have been inserted")
            Assert.AreEqual(expectedTimeZoneId, station.TimeZoneId, $"Station {NameOf(station.TimeZoneId)} should have been set")

            r = station.ValidateStoredEntity($"Inserted {NameOf(Ohmni.CincoStationEntity)} with {NameOf(Ohmni.CincoStationEntity.ComputerName)} of {station.ComputerName}")
            Assert.IsTrue(r.Success, r.Details)

            station.FetchUsingKey(connection)
            Dim expectedLocalTimestamp As DateTime = DateTime.Now
            Dim actualLocalTimeStamp As DateTime = station.Timestamp.ToLocalTime
            Asserts.Instance.AreEqual(expectedLocalTimestamp, actualLocalTimeStamp, TimeSpan.FromSeconds(10), "Expected local time timestamp")

            Dim expectedLocaleTimeStamp As DateTimeOffset = actualLocalTimeStamp
            Dim actualLocaleTimeStamp As DateTimeOffset = station.LocaleTimestamp
            Asserts.Instance.AreEqual(expectedLocaleTimeStamp, actualLocaleTimeStamp, TimeSpan.FromSeconds(0), "Expected locale time timestamp")
            Return (station.StationAutoId, station.StationName)
        End Using
    End Function

    #End Region

    #Region " TEST LEVEL "

    ''' <summary> Asserts the test level entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">                 The site. </param>
    ''' <param name="provider">             The provider. </param>
    ''' <param name="testLevelId">          Identifier for the test level. </param>
    ''' <param name="changedTestLevelName"> Name of the changed test level. </param>
    Public Shared Sub AssertTestLevelEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal testLevelId As Integer, ByVal changedTestLevelName As String)
        Dim id As Integer = 10
        Dim testLevel As New OhmniTestLevelEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = OhmniTestLevelBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            r = testLevel.ValidateNewEntity($"{NameOf(Ohmni.OhmniTestLevelEntity)} with {NameOf(Ohmni.OhmniTestLevelEntity.TestLevelId)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualFetched As Boolean = testLevel.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {id} should not exist")
            r = testLevel.ValidateNewEntity($"{NameOf(Ohmni.OhmniTestLevelEntity)} with {NameOf(Ohmni.OhmniTestLevelEntity.TestLevelId)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            id = testLevelId
            actualFetched = testLevel.FetchUsingKey(connection, id)
            Assert.IsTrue(actualFetched, $"key {id} should exist")
            r = testLevel.ValidateStoredEntity($"Existing {NameOf(Ohmni.OhmniTestLevelEntity)} with {NameOf(Ohmni.OhmniTestLevelEntity.TestLevelId)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            Dim existingName As String = testLevel.Name
            actualFetched = testLevel.FetchUsingUniqueIndex(connection, existingName)
            Assert.IsTrue(actualFetched, $"Name {existingName} should exist")
            r = testLevel.ValidateStoredEntity($"Existing {NameOf(Ohmni.OhmniTestLevelEntity)} with {NameOf(Ohmni.OhmniTestLevelEntity.Name)} equals {existingName}")
            Assert.IsTrue(r.Success, r.Details)

            Dim name As String = changedTestLevelName
            If Not String.Equals(testLevel.Name, changedTestLevelName) Then
                testLevel.Name = name
                r = testLevel.ValidateChangedEntity($"Changed {NameOf(Ohmni.OhmniTestLevelEntity)} with {NameOf(Ohmni.OhmniTestLevelEntity.TestLevelId)} to {id}")
                Assert.IsTrue(r.Success, r.Details)
                ' try to fetch using new name; 
                actualFetched = testLevel.FetchUsingUniqueIndex(connection)
                Assert.IsFalse(actualFetched, $"Name {name} should not exist")
            End If

            Dim expectedCount As Integer = 2
            Dim actualCount As Integer = testLevel.FetchAllEntities(connection)
            Assert.IsTrue(actualCount >= expectedCount, $"Expected at least {expectedCount} found {actualCount} {NameOf(Ohmni.OhmniTestLevelEntity)}s")
        End Using
    End Sub

    ''' <summary> Asserts the test level entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="site">        The site. </param>
    ''' <param name="provider">    The provider. </param>
    ''' <param name="testLevelId"> Identifier for the test level. </param>
    Public Shared Sub AssertTestLevelEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal testLevelId As TestLevel)
        OhmniAuditor.AssertTestLevelEntity(site, provider, CInt(testLevelId), $"{testLevelId}.Changed")
    End Sub

    ''' <summary> Asserts the test level entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="site">        The site. </param>
    ''' <param name="provider">    The provider. </param>
    ''' <param name="testLevelId"> Identifier for the test level. </param>
    Public Shared Sub AssertTestLevelEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal testLevelId As CincoTestLevel)
        OhmniAuditor.AssertTestLevelEntity(site, provider, CInt(testLevelId), $"{testLevelId}.Changed")
    End Sub

    #End Region

End Class

