Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Partial Friend NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Dapper Ohmni Tests"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Unit Tests for the Dapper Ohmni Entities Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Dapper.Ohmni.Tests"

    End Class

End Namespace

