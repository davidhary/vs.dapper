Imports isr.Dapper.Entities.Tests
Namespace Global.isr.Dapper.Ohmni.Tests.Cinco

    Partial Public MustInherit Class TestSuite

        ''' <summary> (Unit Test Method) tests ohmni attribute type entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub OhmniAttributeTypeEntityTest()
            OhmniAuditor.AssertAttributeTypeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests lot entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub LotEntityTest()
            OhmniAuditor.AssertLotEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests part entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub PartEntityTest()
            OhmniAuditor.AssertPartEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests part attribute range entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub PartAttributeRangeEntityTest()
            OhmniAuditor.AssertPartAttributeRangeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) releases the history test. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub ReleaseHistoryTest()
            OhmniAuditor.AssertReleaseHistoryEntity(TestSuite.TestInfo, Me.GetProvider, TestSuite.BuiltDatabaseVersion)
        End Sub

        ''' <summary> (Unit Test Method) tests station entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub StationEntityTest()
            OhmniAuditor.AssertStationEntity(TestSuite.TestInfo, Me.GetProvider, False)
        End Sub

        ''' <summary> (Unit Test Method) tests session entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub OhmniCincoSessionEntityTest()
            OhmniAuditor.AssertSessionEntity(TestSuite.TestInfo, Me.GetProvider, CincoTestLevel.Normal)
        End Sub

        ''' <summary> (Unit Test Method) tests ohmni test level entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub OhmniTestLevelEntityTest()
            OhmniAuditor.AssertTestLevelEntity(TestSuite.TestInfo, Me.GetProvider, CincoTestLevel.Normal)
        End Sub

    End Class

End Namespace

