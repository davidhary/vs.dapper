Imports Dapper

Imports isr.Dapper.Entity
#Disable Warning IDE1006 ' Naming Styles
Namespace Global.isr.Dapper.Ohmni.Tests.Cinco

    Partial Public Class TestSuite

        ''' <summary> Initializes this object. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="provider"> The provider. </param>
        Public Shared Sub Initialize(ByVal provider As ProviderBase)
            Console.WriteLine($".NET {Environment.Version}")
            Console.WriteLine($"Dapper {GetType(Global.Dapper.SqlMapper).AssemblyQualifiedName}")
            Console.WriteLine($"Connection string: {provider.ConnectionString}")
            TestSuite._Provider = provider
            ' test naked connection
            Assert.IsTrue(TestSuite.Provider.IsConnectionExists(), $"connection {TestSuite.Provider.GetConnection} does not exist")
            ' test connection with event handling
            Using connection As System.Data.IDbConnection = TestSuite.Provider.GetConnection()
                Assert.IsTrue(connection.Exists(), "connection does not exist")
            End Using
            If provider Is Nothing Then
                TestSuite._SchemaBuilder = New SchemaBuilder
            Else
                TestSuite._SchemaBuilder = New SchemaBuilder With {.Provider = provider}
                TestSuite.EnumerateSchemaObjects(provider.ProviderType)
            End If
            ' this prevents testing.
            TestSuite.BuildDatabase(provider.BuildMasterConnectionString(False))
            TestSuite.SchemaBuilder.AssertSchemaObjectsExist()
        End Sub

        ''' <summary> Cleanups this object. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Public Shared Sub Cleanup()
            If TableCategory.None <> TestSuiteSettings.Get.DeleteOption Then
                TestSuite.SchemaBuilder.DeleteAllRecords(TestSuiteSettings.Get.DeleteOption)
            End If
            If TableCategory.None <> TestSuiteSettings.Get.DropTablesOption Then
                TestSuite.SchemaBuilder.DropAllTables(TestSuiteSettings.Get.DropTablesOption, TestSuiteSettings.Get.IgnoreDropTableErrors)
            End If
        End Sub

        ''' <summary> Gets or sets the provider. </summary>
        ''' <value> The provider. </value>
        Public Shared ReadOnly Property Provider As ProviderBase

        ''' <summary> Gets or sets the schema builder. </summary>
        ''' <value> The schema builder. </value>
        Public Shared ReadOnly Property SchemaBuilder As SchemaBuilder

        ''' <summary> Enumerate schema objects. </summary>
        ''' <remarks> David, 2020-07-04. </remarks>
        ''' <param name="providerType"> Type of the provider. </param>
        Public Shared Sub EnumerateSchemaObjects(ByVal providerType As ProviderType)
            Try
                TestSuite.EnumerateSchemaObjectsThis(providerType)
            Catch
                TestSuite.SchemaBuilder.EnumeratedTables.Clear()
                Throw
            End Try
        End Sub

        ''' <summary> Enumerate schema objects this. </summary>
        ''' <remarks> David, 2020-07-04. </remarks>
        ''' <param name="providerType"> Type of the provider. </param>
        Private Shared Sub EnumerateSchemaObjectsThis(ByVal providerType As ProviderType)

            Dim providerFileLabel As String = If(providerType = ProviderType.SQLite, TestSuiteSettings.Get.SQLiteFileLabel, TestSuiteSettings.Get.SqlServerFileLabel)

            Dim schemaBuilder As SchemaBuilder = TestSuite.SchemaBuilder
            schemaBuilder.EnumeratedTables.Clear()
            Dim tb As New TableMaker

            ' ATTRIBUTE TYPE 
            tb = New TableMaker With {.TableName = OhmniAttributeTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf OhmniAttributeTypeBuilder.CreateTable,
                                      .InsertAction = AddressOf CincoPartEntity.InsertAttributeTypeValues}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' TEST LEVEL
            tb = New TableMaker With {.TableName = OhmniTestLevelBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf OhmniTestLevelBuilder.CreateTable,
                                      .InsertAction = AddressOf CincoSessionEntity.InsertTestLevelValues}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' STATION
            tb = New TableMaker With {.TableName = CincoStationBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf CincoStationBuilder.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' SESSION
            tb = New TableMaker With {.TableName = CincoSessionBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf CincoSessionBuilder.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PART 
            tb = New TableMaker With {.TableName = CincoPartBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf CincoPartBuilder.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PART ATTRIBUTE RANGE
            OhmniPartAttributeRangeBuilder.AttributeTypeTableName = OhmniAttributeTypeBuilder.TableName
            OhmniPartAttributeRangeBuilder.AttributeTypeForeignKeyName = NameOf(IOhmniAttributeType.AttributeTypeId)
            tb = New TableMaker With {.TableName = OhmniPartAttributeRangeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf OhmniPartAttributeRangeBuilder.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' LOT
            tb = New TableMaker With {.TableName = CincoLotBuilder.TableName, .Category = TableCategory.None,
                                     .CreateAction = AddressOf CincoLotBuilder.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' VERSION
            TestSuite.BuiltDatabaseVersion = (New isr.Core.MyAssemblyInfo(GetType(TestSuite).Assembly)).Version.ToString(3)
            tb = New TableMaker With {.TableName = CincoReleaseHistoryBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf CincoReleaseHistoryBuilder.CreateTable,
                                      .PostCreateAction = AddressOf CincoReleaseHistoryBuilder.UpsertRevision,
                                      .PostCreateValue = $"{TestSuite.BuiltDatabaseVersion},Initial Release"}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

        End Sub

        ''' <summary> Gets or sets the built database version. </summary>
        ''' <value> The built database version. </value>
        Public Shared Property BuiltDatabaseVersion As String

        ''' <summary> Creates data base. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        Public Shared Sub CreateDatabase(ByVal masterConnectionString As String)
            If TestSuiteSettings.Get.CreateDatabase Then
                If System.IO.File.Exists(TestSuite.Provider.FileName) Then System.IO.File.Delete(TestSuite.Provider.FileName)
                TestSuite.CreateDatabaseThis(masterConnectionString)
            ElseIf Not TestSuite.Provider.DatabaseExists Then
                TestSuite.CreateDatabaseThis(masterConnectionString)
            End If
        End Sub

        ''' <summary> Creates database. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Private Shared Function CreateDatabaseThis(ByVal masterConnectionString As String) As Boolean
            Dim result As Boolean = TestSuite.Provider.CreateDatabase(masterConnectionString, TestSuite.Provider.DatabaseName)
            Dim providerFileLabel As String = If(TestSuite.Provider.ProviderType = ProviderType.SQLite,
                                                    TestSuiteSettings.Get.SQLiteFileLabel,
                                                    TestSuiteSettings.Get.SqlServerFileLabel)
            Dim count As Integer
            Dim schemaFileName As String
            If result Then
                schemaFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                        String.Format(TestSuiteSettings.Get.SchemaFileNameFormat, providerFileLabel))
                count = TestSuite.Provider.ExecuteScript(schemaFileName)
                result = count > 0
            End If
            If result Then
                schemaFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                        String.Format(TestSuiteSettings.Get.SchemaDataFileNameFormat, providerFileLabel))
                count = TestSuite.Provider.ExecuteScript(schemaFileName)
                result = count > 0
            End If
            Return result
        End Function

        ''' <summary> Create or clear database and add all tables. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        Public Shared Sub BuildDatabase(ByVal masterConnectionString As String)
            TestSuite.CreateDatabase(masterConnectionString)
            If TestSuiteSettings.Get.ClearDatabase Then TestSuite.SchemaBuilder.DropAllTables(TestSuiteSettings.Get.IgnoreDropTableErrors)
            TestSuite.SchemaBuilder.BuildSchema()
        End Sub

    End Class

End Namespace
