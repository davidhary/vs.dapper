Imports isr.Dapper.Entity
Namespace Global.isr.Dapper.Ohmni.Tests.Cinco

    ''' <summary> An Ohmni Cinco Test Suite for SQL Server Dapper Entities. </summary>
    ''' <remarks>
    ''' The test suites here implement TestSuiteBase so that each provider runs the entire set of
    ''' tests without declarations per method If we want to support a new provider, they need only be
    ''' added here - not in multiple places. (c) 2018 Integrated Scientific Resources, Inc. All
    ''' rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 8/16/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class SqlServerTestSuite
        Inherits TestSuite

        #Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestSuite.BaseClassInitialize(testContext)
                TestSuite.Initialize(SqlServerTestSuite.GetSharedProvider)
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            TestSuite.Cleanup()
        End Sub

        #End Region

        #Region " PROVIDER "

        ''' <summary> Gets the provider. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The provider. </returns>
        Public Overrides Function GetProvider() As ProviderBase
            Return SqlServerTestSuite.GetSharedProvider
        End Function

        ''' <summary> The SQL server provider. </summary>
        Private Shared _SqlServerProvider As SqlServerProvider

        ''' <summary> Gets the shared provider. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The shared provider. </returns>
        Public Shared Function GetSharedProvider() As SqlServerProvider
            If SqlServerTestSuite._SqlServerProvider Is Nothing Then
                SqlServerTestSuite._SqlServerProvider = SqlServerProvider.Create(TestSuiteSettings.Get.SqlServerConnectionString)
            End If
            Return SqlServerTestSuite._SqlServerProvider
        End Function

        #End Region

    End Class

End Namespace
