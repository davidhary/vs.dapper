﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Dapper.EntityUI.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Dapper.EntityUI.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Dapper.EntityUI.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
