
#pragma warning disable IDE1006 // Naming Styles
namespace isr.Dapper.Entities.Probe.Tests
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> A test site class. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class TestSite : isr.Core.MSTest.TestSiteBase
    {
    }
}
