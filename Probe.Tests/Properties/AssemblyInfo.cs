using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Dapper.Entities.Probe.Tests.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Dapper.Entities.Probe.Tests.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Dapper.Entities.Probe.Tests.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
