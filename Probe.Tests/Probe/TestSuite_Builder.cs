using System;

using Dapper;

using isr.Dapper.Entity;

using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace isr.Dapper.Entities.Probe.Tests
{

    public partial class TestSuite
    {

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="provider"> The provider. </param>
        public static void Initialize( ProviderBase provider )
        {
            Console.WriteLine( $".NET {Environment.Version}" );
            Console.WriteLine( $"Dapper {typeof( SqlMapper ).AssemblyQualifiedName}" );
            Console.WriteLine( $"Connection string: {provider.ConnectionString}" );
            Provider = provider;
            // test naked connection
            Assert.IsTrue( Provider.IsConnectionExists(), $"connection {Provider.GetConnection()} does not exist" );
            // test connection with event handling
            using ( var connection = Provider.GetConnection() )
            {
                Assert.IsTrue( connection.Exists(), "connection does not exist" );
            }

            if ( provider is null )
            {
                SchemaBuilder = new SchemaBuilder();
            }
            else
            {
                SchemaBuilder = new SchemaBuilder() { Provider = provider };
                EnumerateSchemaObjects( provider.ProviderType );
            }
            // this prevents testing.
            BuildDatabase( provider.BuildMasterConnectionString( false ) );
            SchemaBuilder.AssertSchemaObjectsExist();
        }

        /// <summary> Cleanups this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public static void Cleanup()
        {
            if ( TableCategory.None != TestSuiteSettings.Get().DeleteOption )
            {
                SchemaBuilder.DeleteAllRecords( TestSuiteSettings.Get().DeleteOption );
            }

            if ( TableCategory.None != TestSuiteSettings.Get().DropTablesOption )
            {
                SchemaBuilder.DropAllTables( TestSuiteSettings.Get().DropTablesOption, TestSuiteSettings.Get().IgnoreDropTableErrors );
            }
        }

        /// <summary> Gets or sets the provider. </summary>
        /// <value> The provider. </value>
        public static ProviderBase Provider { get; private set; }

        /// <summary> Gets or sets the schema builder. </summary>
        /// <value> The schema builder. </value>
        public static SchemaBuilder SchemaBuilder { get; private set; }

        /// <summary> Enumerate schema objects. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="providerType"> Type of the provider. </param>
        public static void EnumerateSchemaObjects( ProviderType providerType )
        {
            try
            {
                EnumerateSchemaObjectsThis( providerType );
            }
            catch
            {
                SchemaBuilder.EnumeratedTables.Clear();
                throw;
            }
        }

        /// <summary> Enumerate schema objects this. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="providerType"> Type of the provider. </param>
        private static void EnumerateSchemaObjectsThis( ProviderType providerType )
        {
            string providerFileLabel = providerType == ProviderType.SQLite ? TestSuiteSettings.Get().SQLiteFileLabel : TestSuiteSettings.Get().SqlServerFileLabel;
            var schemaBuilder = SchemaBuilder;
            schemaBuilder.EnumeratedTables.Clear();
            var tb = new TableMaker();

            // BUCKET BIN 
            tb = new TableMaker() {
                TableName = BucketBinBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = BucketBinBuilder.Instance.CreateTable,
                InsertAction = BucketBinBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = BucketBinEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // READING BIN
            tb = new TableMaker() {
                TableName = ReadingBinBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ReadingBinBuilder.Instance.CreateTable,
                InsertAction = ReadingBinBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // READING TYPE
            tb = new TableMaker() {
                TableName = ReadingTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ReadingTypeBuilder.Instance.CreateTable,
                InsertAction = ReadingTypeBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // METER MODEL
            tb = new TableMaker() {
                TableName = MeterModelBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = MeterModelBuilder.Instance.CreateTable,
                InsertAction = MeterModelBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // TEST TYPE
            tb = new TableMaker() {
                TableName = TestTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = TestTypeBuilder.Instance.CreateTable,
                InsertAction = TestTypeBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // TOLERANCE
            tb = new TableMaker() {
                TableName = ToleranceBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ToleranceBuilder.CreateTable,
                InsertAction = ToleranceBuilder.InsertValues
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            MeterBuilder.Instance.ForeignTableName = MeterModelBuilder.TableName;
            isr.Dapper.Entities.MeterBuilder.Instance.ForeignTableKeyName = nameof( isr.Dapper.Entities.MeterModelNub.Id );

            tb = new TableMaker() {
                TableName = MeterBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = x => MeterBuilder.Instance.CreateTable( x, UniqueIndexOptions.None ),
                InsertAction = MeterBuilder.InsertValues
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            MeterGuardBandLookupBuilder.MeterTableName = MeterModelBuilder.TableName;
            isr.Dapper.Entities.MeterGuardBandLookupBuilder.MeterTableKeyName = nameof( isr.Dapper.Entities.MeterModelNub.Id );
            MeterGuardBandLookupBuilder.NomTypeTableName = NomTypeBuilder.TableName;
            isr.Dapper.Entities.MeterGuardBandLookupBuilder.NomTypeTableKeyName = nameof( isr.Dapper.Entities.NomTypeNub.Id );

            tb = new TableMaker() {
                TableName = MeterGuardBandLookupBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = MeterGuardBandLookupBuilder.CreateTable,
                InsertFileValuesAction = MeterGuardBandLookupBuilder.InsertFileValues,
                ValuesFileName = System.IO.Path.Combine( TestSuiteSettings.Get().SchemaFolderName, string.Format( TestSuiteSettings.Get().GuardBandLookupFileNameFormat, providerFileLabel ) )
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = OhmMeterSettingBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmMeterSettingBuilder.CreateTable,
                InsertAction = OhmMeterSettingBuilder.InsertValues
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            OhmMeterSettingLookupBuilder.MeterTableName = MeterModelBuilder.TableName;
            isr.Dapper.Entities.OhmMeterSettingLookupBuilder.MeterTableKeyName = nameof( isr.Dapper.Entities.MeterModelNub.Id );

            tb = new TableMaker() {
                TableName = OhmMeterSettingLookupBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmMeterSettingLookupBuilder.CreateTable,
                InsertFileValuesAction = OhmMeterSettingLookupBuilder.InsertFileValues,
                ValuesFileName = System.IO.Path.Combine( TestSuiteSettings.Get().SchemaFolderName, string.Format( TestSuiteSettings.Get().AddOhmMeterSettingLookupFileNameFormat, providerFileLabel ) )
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = OhmMeterSetupBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmMeterSetupBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // COMPUTER + PLATFORM + STATION + SESSION
            tb = new TableMaker() {
                TableName = ComputerBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => ComputerBuilder.Instance.CreateTable( x, true )
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = PlatformBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => PlatformBuilder.Instance.CreateTable( x, true )
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // if the station is part of a platform, make the station label non-unique to allow same name for different platforms.
            tb = new TableMaker() {
                TableName = StationBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => StationBuilder.Instance.CreateTable( x, true )
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = ComputerStationBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ComputerStationBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = PlatformStationBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = PlatformStationBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = SessionBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = SessionBuilder.Instance.CreateTableNonUniqueLabel
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = StationSessionBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = StationSessionBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = SessionOhmMeterSetupBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = SessionOhmMeterSetupBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART 
            tb = new TableMaker() {
                TableName = PartBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = PartBuilder.Instance.CreateTableNonUniqueLabel
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = PartNamingTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = x => {
                    var s = new PartNamingTypeBuilder();
                    return s.CreateTable( x );
                },
                InsertAction = x => PartNamingTypeBuilder.Instance.InsertIgnoreDefaultRecords( x )
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            PartNamingBuilder.Instance.PrimaryTableName = PartBuilder.TableName;
            PartNamingBuilder.Instance.PrimaryTableKeyName = nameof( PartNub.AutoId );
            PartNamingBuilder.Instance.SecondaryTableName = PartNamingTypeBuilder.TableName;
            PartNamingBuilder.Instance.SecondaryTableKeyName = nameof( PartNamingTypeNub.Id );

            tb = new TableMaker() {
                TableName = PartNamingBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = PartNamingBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // the lot uses a unique lot numbers assuming lot numbers are unique across all parts.
            tb = new TableMaker() {
                TableName = LotBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = LotBuilder.Instance.CreateTableUniqueLabel
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = PartLotBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = PartLotBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = LotSessionBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = LotSessionBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SUBSTRATE
            tb = new TableMaker() {
                TableName = SubstrateTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = SubstrateTypeBuilder.Instance.CreateTable,
                InsertAction = SubstrateTypeBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = SubstrateBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => SubstrateBuilder.Instance.CreateTable( x, UniqueIndexOptions.None )
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // STRUCTURES
            tb = new TableMaker() {
                TableName = StructureTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = StructureTypeBuilder.Instance.CreateTable,
                InsertAction = StructureTypeBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = StructureBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => StructureBuilder.Instance.CreateTable( x, UniqueIndexOptions.None )
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = new TableMaker() {
                TableName = SessionStructureBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = SessionStructureBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // REVISION
            CurrentDatabaseVersion = new Core.MyAssemblyInfo( typeof( TestSuite ).Assembly ).Version.ToString( 3 );
            tb = new TableMaker() {
                TableName = RevisionBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = RevisionBuilder.Instance.CreateTableUniqueLabel,
                PostCreateAction = RevisionBuilder.UpsertRevision,
                PostCreateValue = CurrentDatabaseVersion,
                ExistsUpdateAction = RevisionBuilder.UpsertRevision,
                ExistsUpdateValue = CurrentDatabaseVersion
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
        }

        /// <summary> Gets or sets the built database version. </summary>
        /// <value> The built database version. </value>
        public static string CurrentDatabaseVersion { get; set; }

        /// <summary> Creates data base. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        public static void CreateDatabase( string masterConnectionString )
        {
            if ( TestSuiteSettings.Get().CreateDatabase )
            {
                if ( System.IO.File.Exists( Provider.FileName ) )
                    System.IO.File.Delete( Provider.FileName );
                _ = CreateDatabaseThis( masterConnectionString );
            }
            else if ( !Provider.DatabaseExists() )
            {
                _ = CreateDatabaseThis( masterConnectionString );
            }
        }

        /// <summary> Creates database. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        private static bool CreateDatabaseThis( string masterConnectionString )
        {
            bool result = Provider.CreateDatabase( masterConnectionString, Provider.DatabaseName );
            string providerFileLabel = Provider.ProviderType == ProviderType.SQLite ? TestSuiteSettings.Get().SQLiteFileLabel : TestSuiteSettings.Get().SqlServerFileLabel;
            int count;
            string schemaFileName;
            if ( result )
            {
                schemaFileName = System.IO.Path.Combine( TestSuiteSettings.Get().SchemaFolderName, string.Format( TestSuiteSettings.Get().SchemaFileNameFormat, providerFileLabel ) );
                count = Provider.ExecuteScript( schemaFileName );
                result = count > 0;
            }

            if ( result )
            {
                schemaFileName = System.IO.Path.Combine( TestSuiteSettings.Get().SchemaFolderName, string.Format( TestSuiteSettings.Get().SchemaDataFileNameFormat, providerFileLabel ) );
                count = Provider.ExecuteScript( schemaFileName );
                result = count > 0;
            }

            return result;
        }

        /// <summary> Create or clear database and add all tables. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        public static void BuildDatabase( string masterConnectionString )
        {
            CreateDatabase( masterConnectionString );
            if ( TestSuiteSettings.Get().ClearDatabase )
                SchemaBuilder.DropAllTables( TestSuiteSettings.Get().IgnoreDropTableErrors );
            SchemaBuilder.BuildSchema();
        }

        /// <summary> Initializes the lookup entities. </summary>
        /// <remarks> David, 2020-07-11. </remarks>
        /// <param name="provider"> The provider. </param>
        public static void InitializeLookupEntities( ProviderBase provider )
        {
            if ( provider is null )
                return;
            SchemaBuilder = new SchemaBuilder() { Provider = provider };
            EnumerateSchemaObjects( provider.ProviderType );
            SchemaBuilder.FetchAll();
        }

        /// <summary> Initializes the lookup entities. </summary>
        /// <remarks> David, 2020-07-11. </remarks>
        public void InitializeLookupEntities()
        {
            InitializeLookupEntities( this.GetProvider() );
        }
    }
}
