﻿Option Infer On
Option Strict Off
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper

Namespace DapperTests
	Public Class TransactionTests
		Inherits TestBase

		<TestMethod> _
		Public Sub TestTransactionCommit()
			Try
				connection.Execute("create table #TransactionTest ([ID] int, [Value] varchar(32));")

				Using transaction = connection.BeginTransaction()
					connection.Execute("insert into #TransactionTest ([ID], [Value]) values (1, 'ABC');", transaction:= transaction)

					transaction.Commit()
				End Using

				Assert.AreEqual(1, connection.Query(Of Integer)("select count(*) from #TransactionTest;").Single())
			Finally
				connection.Execute("drop table #TransactionTest;")
			End Try
		End Sub

		<TestMethod> _
		Public Sub TestTransactionRollback()
			connection.Execute("create table #TransactionTest ([ID] int, [Value] varchar(32));")

			Try
				Using transaction = connection.BeginTransaction()
					connection.Execute("insert into #TransactionTest ([ID], [Value]) values (1, 'ABC');", transaction:= transaction)

					transaction.Rollback()
				End Using

				Assert.AreEqual(0, connection.Query(Of Integer)("select count(*) from #TransactionTest;").Single())
			Finally
				connection.Execute("drop table #TransactionTest;")
			End Try
		End Sub

		<TestMethod> _
		Public Sub TestCommandWithInheritedTransaction()
			connection.Execute("create table #TransactionTest ([ID] int, [Value] varchar(32));")

			Try
				Using transaction = connection.BeginTransaction()
					Dim transactedConnection = New TransactedConnection(connection, transaction)

					transactedConnection.Execute("insert into #TransactionTest ([ID], [Value]) values (1, 'ABC');")

					transaction.Rollback()
				End Using

				Assert.AreEqual(0, connection.Query(Of Integer)("select count(*) from #TransactionTest;").Single())
			Finally
				connection.Execute("drop table #TransactionTest;")
			End Try
		End Sub
	End Class
End Namespace
