﻿Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper
Namespace DapperTests
	Public Class LiteralTests
		Inherits TestBase

        <TestMethod>
        Public Sub LiteralReplacementBoolean()
            Dim row As Integer? = Me.Connection.Query(Of Integer?)("select 42 where 1 = {=val}", New With {Key .val = True}).SingleOrDefault()
            Assert.IsNotNull(row)
            Assert.AreEqual(42, row)
            row = Me.Connection.Query(Of Integer?)("select 42 where 1 = {=val}", New With {Key .val = False}).SingleOrDefault()
            Assert.IsNull(row)
        End Sub

        <TestMethod> _
		Public Sub LiteralReplacementWithIn()
            Dim data As IEnumerable(Of MyRow) = Me.Connection.Query(Of MyRow)("select @x where 1 in @ids and 1 ={=a}", New With {Key .x = 1, Key .ids = {1, 2, 3}, Key .a = 1}).ToList()
        End Sub

		Private Class MyRow
            Public Property X() As Integer
        End Class

		<TestMethod> _
		Public Sub LiteralIn()
			connection.Execute("create table #literalin(id int not null);")
			connection.Execute("insert #literalin (id) values (@id)", { New With {Key .id = 1}, New With {Key .id = 2}, New With {Key .id = 3} })
            Dim count As Integer = Me.Connection.Query(Of Integer)("select count(1) from #literalin where id in {=ids}", New With {Key .ids = {1, 3, 4}}).Single()
            Assert.AreEqual(2, count)
		End Sub

		<TestMethod> _
		Public Sub LiteralReplacement()
			connection.Execute("create table #literal1 (id int not null, foo int not null)")
			connection.Execute("insert #literal1 (id,foo) values ({=id}, @foo)", New With {Key .id = 123, Key .foo = 456})
            Dim rows As Object() = {New With {Key .id = 1, Key .foo = 2}, New With {Key .id = 3, Key .foo = 4}}
            Connection.Execute("insert #literal1 (id,foo) values ({=id}, @foo)", rows)
            Dim count As Integer = Me.Connection.Query(Of Integer)("select count(1) from #literal1 where id={=foo}", New With {Key .foo = 123}).Single()
            Assert.AreEqual(1, count)
			Dim sum As Integer = Me.Connection.Query(Of Integer)("select sum(id) + sum(foo) from #literal1").Single()
			Assert.AreEqual(sum, 123 + 456 + 1 + 2 + 3 + 4)
		End Sub

		<TestMethod> _
		Public Sub LiteralReplacementDynamic()
            Dim args As New DynamicParameters()
            args.Add("id", 123)
			connection.Execute("create table #literal2 (id int not null)")
			connection.Execute("insert #literal2 (id) values ({=id})", args)

			args = New DynamicParameters()
			args.Add("foo", 123)
            Dim count As Integer = Me.Connection.Query(Of Integer)("select count(1) from #literal2 where id={=foo}", args).Single()
            Assert.AreEqual(1, count)
		End Sub
	End Class
End Namespace
