﻿Imports System.Linq
Imports System.Data
Imports System.Diagnostics
Imports System
Imports System.Threading.Tasks
Imports System.Threading
Imports System.Data.SqlClient
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper

Namespace DapperTests
    Public Class AsyncTests
        Inherits TestBase

        Private _marsConnection As SqlConnection
        Private Function MarsConnection() As SqlConnection
            If _marsConnection Is Nothing Then
                _marsConnection = TestBase.GetOpenConnection(True)
            End If
            Return _marsConnection
        End Function

        <TestMethod>
        Public Async Function TestBasicStringUsageAsync() As Task
            Dim query As IEnumerable(Of String) = Await Me.Connection.QueryAsync(Of String)("select 'abc' as [Value] union all select @txt", New With {Key .txt = "def"}).ConfigureAwait(False)
            Dim arr As String() = query.ToArray()
            Assert.AreEqual({"abc", "def"}, arr)
        End Function

        <TestMethod>
        Public Async Function TestBasicStringUsageQueryFirstAsync() As Task
            Dim str As String = Await Me.Connection.QueryFirstAsync(Of String)(New CommandDefinition("select 'abc' as [Value] union all select @txt", New With {Key .txt = "def"})).ConfigureAwait(False)
            Assert.AreEqual("abc", str)
        End Function

        <TestMethod>
        Public Async Function TestBasicStringUsageQueryFirstAsyncDynamic() As Task
            Dim str As Object = Await Me.Connection.QueryFirstAsync("select 'abc' as [Value] union all select @txt", New With {Key .txt = "def"}).ConfigureAwait(False)
            Assert.AreEqual("abc", CType(str, String))
        End Function

        <TestMethod>
        Public Async Function TestBasicStringUsageQueryFirstOrDefaultAsync() As Task
            Dim str As String = Await Me.Connection.QueryFirstOrDefaultAsync(Of String)(New CommandDefinition("select null as [Value] union all select @txt", New With {Key .txt = "def"})).ConfigureAwait(False)
            Assert.IsNull(str)
        End Function

        <TestMethod>
        Public Async Function TestBasicStringUsageQueryFirstOrDefaultAsyncDynamic() As Task
            Dim str As Object = Await Me.Connection.QueryFirstOrDefaultAsync("select null as [Value] union all select @txt", New With {Key .txt = "def"}).ConfigureAwait(False)
            Assert.IsNull(str)
        End Function

        <TestMethod>
        Public Async Function TestBasicStringUsageQuerySingleAsyncDynamic() As Task
            Dim str As String = Await Me.Connection.QuerySingleAsync(Of String)(New CommandDefinition("select 'abc' as [Value]")).ConfigureAwait(False)
            Assert.AreEqual("abc", str)
        End Function

        <TestMethod>
        Public Async Function TestBasicStringUsageQuerySingleAsync() As Task
            Dim str As Object = Await Me.Connection.QuerySingleAsync("select 'abc' as [Value]").ConfigureAwait(False)
            Assert.AreEqual("abc", CType(str, String))
        End Function

        <TestMethod>
        Public Async Function TestBasicStringUsageQuerySingleOrDefaultAsync() As Task
            Dim str As String = Await Me.Connection.QuerySingleOrDefaultAsync(Of String)(New CommandDefinition("select null as [Value]")).ConfigureAwait(False)
            Assert.IsNull(str)
        End Function

        <TestMethod>
        Public Async Function TestBasicStringUsageQuerySingleOrDefaultAsyncDynamic() As Task
            Dim str As Object = Await Me.Connection.QuerySingleOrDefaultAsync("select null as [Value]").ConfigureAwait(False)
            Assert.IsNull(str)
        End Function

        <TestMethod>
        Public Async Function TestBasicStringUsageAsyncNonBuffered() As Task
            Dim query As IEnumerable(Of String) = Await Me.Connection.QueryAsync(Of String)(New CommandDefinition("select 'abc' as [Value] union all select @txt", New With {Key .txt = "def"}, flags:=CommandFlags.None)).ConfigureAwait(False)
            Dim arr As String() = query.ToArray()
            Assert.AreEqual({"abc", "def"}, arr)
        End Function

        <TestMethod>
        Public Sub TestLongOperationWithCancellation()
            Dim cancel As New CancellationTokenSource(TimeSpan.FromSeconds(5))
            Dim task As Task(Of IEnumerable(Of Integer)) = Me.Connection.QueryAsync(Of Integer)(New CommandDefinition("waitfor delay '00:00:10';select 1", cancellationToken:=cancel.Token))
            Try
                If Not task.Wait(TimeSpan.FromSeconds(7)) Then
                    Throw New TimeoutException() ' should have cancelled
                End If
            Catch agg As AggregateException
                Assert.IsTrue(TypeOf agg.InnerException Is SqlException)
            End Try
        End Sub

        <TestMethod>
        Public Async Function TestBasicStringUsageClosedAsync() As Task
            Dim query As IEnumerable(Of String) = Await Me.Connection.QueryAsync(Of String)("select 'abc' as [Value] union all select @txt", New With {Key .txt = "def"}).ConfigureAwait(False)
            Dim arr As String() = query.ToArray()
            Assert.AreEqual({"abc", "def"}, arr)
        End Function

        <TestMethod>
        Public Async Function TestQueryDynamicAsync() As Task
            Dim row As Object = (Await Me.Connection.QueryAsync("select 'abc' as [Value]").ConfigureAwait(False)).Single()
            Dim value As String = CType(row, String)
            Assert.AreEqual("abc", value)
        End Function

        <TestMethod>
        Public Async Function TestClassWithStringUsageAsync() As Task
            Dim query As IEnumerable(Of BasicType) = Await Me.Connection.QueryAsync(Of BasicType)("select 'abc' as [Value] union all select @txt", New With {Key .txt = "def"}).ConfigureAwait(False)
            Dim arr As BasicType() = query.ToArray()
            Assert.AreEqual({"abc", "def"}, arr.Select(Function(x) x.Value))
        End Function

        <TestMethod>
        Public Async Function TestExecuteAsync() As Task
            Dim val As Integer = Await Me.Connection.ExecuteAsync("declare @foo table(id int not null); insert @foo values(@id);", New With {Key .id = 1}).ConfigureAwait(False)
            Assert.AreEqual(1, val)
        End Function

        <TestMethod>
        Public Sub TestExecuteClosedConnAsyncInner()
            Dim query As Task(Of Integer) = Me.Connection.ExecuteAsync("declare @foo table(id int not null); insert @foo values(@id);", New With {Key .id = 1})
            Dim val As Integer = query.Result
            Assert.AreEqual(1, val)
        End Sub

        <TestMethod>
        Public Async Function TestMultiMapWithSplitAsync() As Task
            Const sql As String = "select 1 as id, 'abc' as name, 2 as id, 'def' as name"
            Dim productQuery As IEnumerable(Of Product) = Await Me.Connection.QueryAsync(Of Product, Category, Product)(sql, Function(prod, cat)
                                                                                                                                 prod.Category = cat
                                                                                                                                 Return prod
                                                                                                                             End Function).ConfigureAwait(False)
            Dim product As Product = productQuery.First()
            ' assertions
            Assert.AreEqual(1, product.Id)
            Assert.AreEqual("abc", product.Name)
            Assert.AreEqual(2, product.Category.Id)
            Assert.AreEqual("def", product.Category.Name)
        End Function

#If False Then
        <TestMethod>
        Public Async Function TestMultiMapArbitraryWithSplitAsync() As Task
            Const sql As String = "select 1 as id, 'abc' as name, 2 as id, 'def' as name"
            Dim productQuery As IEnumerable(Of Product) = Await Me.Connection.QueryAsync(Of Product)(sql, {GetType(Product), GetType(Category)}, Function(objects)
                                                                                                                                                  Dim prod = CType(objects(0), Product)
                                                                                                                                                  prod.Category = CType(objects(1), Category)
                                                                                                                                                  Return prod
                                                                                                                                              End Function).ConfigureAwait(False)

            Dim product As Product = productQuery.First()
            ' assertions
            Assert.AreEqual(1, product.Id)
            Assert.AreEqual("abc", product.Name)
            Assert.AreEqual(2, product.Category.Id)
            Assert.AreEqual("def", product.Category.Name)
        End Function

#End If
        <TestMethod>
        Public Async Function TestMultiMapWithSplitClosedConnAsync() As Task
            Const sql As String = "select 1 as id, 'abc' as name, 2 as id, 'def' as name"
            Using conn As IDbConnection = GetClosedConnection()
                Dim productQuery As IEnumerable(Of Product) = Await conn.QueryAsync(Of Product, Category, Product)(sql, Function(prod, cat)
                                                                                                                            prod.Category = cat
                                                                                                                            Return prod
                                                                                                                        End Function).ConfigureAwait(False)

                Dim product As Product = productQuery.First()
                ' assertions
                Assert.AreEqual(1, product.Id)
                Assert.AreEqual("abc", product.Name)
                Assert.AreEqual(2, product.Category.Id)
                Assert.AreEqual("def", product.Category.Name)
            End Using
        End Function

        <TestMethod>
        Public Async Function TestMultiAsync() As Task
            Using multi As SqlMapper.GridReader = Await Me.Connection.QueryMultipleAsync("select 1; select 2").ConfigureAwait(False)
                Assert.AreEqual(1, multi.ReadAsync(Of Integer)().Result.Single())
                Assert.AreEqual(2, multi.ReadAsync(Of Integer)().Result.Single())
            End Using
        End Function

        <TestMethod>
        Public Async Function TestMultiAsyncViaFirstOrDefault() As Task
            Using multi As SqlMapper.GridReader = Await Me.Connection.QueryMultipleAsync("select 1; select 2; select 3; select 4; select 5").ConfigureAwait(False)
                Assert.AreEqual(1, multi.ReadFirstOrDefaultAsync(Of Integer)().Result)
                Assert.AreEqual(2, multi.ReadAsync(Of Integer)().Result.Single())
                Assert.AreEqual(3, multi.ReadFirstOrDefaultAsync(Of Integer)().Result)
                Assert.AreEqual(4, multi.ReadAsync(Of Integer)().Result.Single())
                Assert.AreEqual(5, multi.ReadFirstOrDefaultAsync(Of Integer)().Result)
            End Using
        End Function

        <TestMethod>
        Public Async Function TestMultiClosedConnAsync() As Task
            Using multi As SqlMapper.GridReader = Await Me.Connection.QueryMultipleAsync("select 1; select 2").ConfigureAwait(False)
                Assert.AreEqual(1, multi.ReadAsync(Of Integer)().Result.Single())
                Assert.AreEqual(2, multi.ReadAsync(Of Integer)().Result.Single())
            End Using
        End Function

        <TestMethod>
        Public Async Function TestMultiClosedConnAsyncViaFirstOrDefault() As Task
            Using multi As SqlMapper.GridReader = Await Me.Connection.QueryMultipleAsync("select 1; select 2; select 3; select 4; select 5;").ConfigureAwait(False)
                Assert.AreEqual(1, multi.ReadFirstOrDefaultAsync(Of Integer)().Result)
                Assert.AreEqual(2, multi.ReadAsync(Of Integer)().Result.Single())
                Assert.AreEqual(3, multi.ReadFirstOrDefaultAsync(Of Integer)().Result)
                Assert.AreEqual(4, multi.ReadAsync(Of Integer)().Result.Single())
                Assert.AreEqual(5, multi.ReadFirstOrDefaultAsync(Of Integer)().Result)
            End Using
        End Function

        <TestMethod>
        Public Async Function ExecuteReaderOpenAsync() As Task
            Dim dt As New DataTable()
            dt.Load(Await Me.Connection.ExecuteReaderAsync("select 3 as [three], 4 as [four]").ConfigureAwait(False))
            Assert.AreEqual(2, dt.Columns.Count)
            Assert.AreEqual("three", dt.Columns(0).ColumnName)
            Assert.AreEqual("four", dt.Columns(1).ColumnName)
            Assert.AreEqual(1, dt.Rows.Count)
            Assert.AreEqual(3, CInt(Fix(dt.Rows(0)(0))))
            Assert.AreEqual(4, CInt(Fix(dt.Rows(0)(1))))
        End Function

        <TestMethod>
        Public Async Function ExecuteReaderClosedAsync() As Task
            Using conn As IDbConnection = GetClosedConnection()
                Dim dt As New DataTable()
                dt.Load(Await conn.ExecuteReaderAsync("select 3 as [three], 4 as [four]").ConfigureAwait(False))
                Assert.AreEqual(2, dt.Columns.Count)
                Assert.AreEqual("three", dt.Columns(0).ColumnName)
                Assert.AreEqual("four", dt.Columns(1).ColumnName)
                Assert.AreEqual(1, dt.Rows.Count)
                Assert.AreEqual(3, CInt(Fix(dt.Rows(0)(0))))
                Assert.AreEqual(4, CInt(Fix(dt.Rows(0)(1))))
            End Using
        End Function

        <TestMethod>
        Public Async Function LiteralReplacementOpen() As Task
            Await LiteralReplacement(Connection).ConfigureAwait(False)
        End Function

        <TestMethod>
        Public Async Function LiteralReplacementClosed() As Task
            Using conn As IDbConnection = GetClosedConnection()
                Await LiteralReplacement(conn).ConfigureAwait(False)
            End Using
        End Function

        Private Async Function LiteralReplacement(ByVal conn As IDbConnection) As Task
            Try
                Await conn.ExecuteAsync("drop table literal1").ConfigureAwait(False)
            Catch ' don't care
            End Try
            Await conn.ExecuteAsync("create table literal1 (id int not null, foo int not null)").ConfigureAwait(False)
            Await conn.ExecuteAsync("insert literal1 (id,foo) values ({=id}, @foo)", New With {Key .id = 123, Key .foo = 456}).ConfigureAwait(False)
            Dim rows As Object() = {New With {Key .id = 1, Key .foo = 2}, New With {Key .id = 3, Key .foo = 4}}
            Await conn.ExecuteAsync("insert literal1 (id,foo) values ({=id}, @foo)", rows).ConfigureAwait(False)
            Dim count As Integer = (Await conn.QueryAsync(Of Integer)("select count(1) from literal1 where id={=foo}", New With {Key .foo = 123}).ConfigureAwait(False)).Single()
            Assert.AreEqual(1, count)
            Dim sum As Integer = (Await conn.QueryAsync(Of Integer)("select sum(id) + sum(foo) from literal1").ConfigureAwait(False)).Single()
            Assert.AreEqual(sum, 123 + 456 + 1 + 2 + 3 + 4)
        End Function

        <TestMethod>
        Public Async Function LiteralReplacementDynamicOpen() As Task
            Await LiteralReplacementDynamic(Connection).ConfigureAwait(False)
        End Function

        <TestMethod>
        Public Async Function LiteralReplacementDynamicClosed() As Task
            Using conn As IDbConnection = GetClosedConnection()
                Await LiteralReplacementDynamic(conn).ConfigureAwait(False)
            End Using
        End Function

        Private Async Function LiteralReplacementDynamic(ByVal conn As IDbConnection) As Task
            Dim args As New DynamicParameters()
            args.Add("id", 123)
            Try
                Await conn.ExecuteAsync("drop table literal2").ConfigureAwait(False)
            Catch ' don't care
            End Try
            Await conn.ExecuteAsync("create table literal2 (id int not null)").ConfigureAwait(False)
            Await conn.ExecuteAsync("insert literal2 (id) values ({=id})", args).ConfigureAwait(False)

            args = New DynamicParameters()
            args.Add("foo", 123)
            Dim count As Integer = (Await conn.QueryAsync(Of Integer)("select count(1) from literal2 where id={=foo}", args).ConfigureAwait(False)).Single()
            Assert.AreEqual(1, count)
        End Function

        <TestMethod>
        Public Async Function LiteralInAsync() As Task
            Await Me.Connection.ExecuteAsync("create table #literalin(id int not null);").ConfigureAwait(False)
            Await Me.Connection.ExecuteAsync("insert #literalin (id) values (@id)", {New With {Key .id = 1}, New With {Key .id = 2}, New With {Key .id = 3}}).ConfigureAwait(False)
            Dim count As Integer = (Await Me.Connection.QueryAsync(Of Integer)("select count(1) from #literalin where id in {=ids}", New With {Key .ids = {1, 3, 4}}).ConfigureAwait(False)).Single()
            Assert.AreEqual(2, count)
        End Function

        ' <FactLongRunning>

        Public Async Function RunSequentialVersusParallelAsync() As Task
            Dim ids As IEnumerable(Of Object) = Enumerable.Range(1, 20000).Select(Function(id) New With {Key id}).ToArray()
            Await MarsConnection.ExecuteAsync(New CommandDefinition("select @id", ids.Take(5), flags:=CommandFlags.None)).ConfigureAwait(False)

            Dim watch As Stopwatch = Stopwatch.StartNew()
            Await MarsConnection.ExecuteAsync(New CommandDefinition("select @id", ids, flags:=CommandFlags.None)).ConfigureAwait(False)
            watch.Stop()
            Console.WriteLine("No pipeline: {0}ms", watch.ElapsedMilliseconds)

            watch = Stopwatch.StartNew()
            Await MarsConnection.ExecuteAsync(New CommandDefinition("select @id", ids, flags:=CommandFlags.Pipelined)).ConfigureAwait(False)
            watch.Stop()
            Console.WriteLine("Pipeline: {0}ms", watch.ElapsedMilliseconds)
        End Function

        ' <FactLongRunning>

        Public Sub RunSequentialVersusParallelSync()
            Dim ids As IEnumerable(Of Object) = Enumerable.Range(1, 20000).Select(Function(id) New With {Key id}).ToArray()
            MarsConnection.Execute(New CommandDefinition("select @id", ids.Take(5), flags:=CommandFlags.None))

            Dim watch As Stopwatch = Stopwatch.StartNew()
            MarsConnection.Execute(New CommandDefinition("select @id", ids, flags:=CommandFlags.None))
            watch.Stop()
            Console.WriteLine("No pipeline: {0}ms", watch.ElapsedMilliseconds)

            watch = Stopwatch.StartNew()
            MarsConnection.Execute(New CommandDefinition("select @id", ids, flags:=CommandFlags.Pipelined))
            watch.Stop()
            Console.WriteLine("Pipeline: {0}ms", watch.ElapsedMilliseconds)
        End Sub

        ' <Collection(NonParallelDefinition.Name)>

        Public Class AsyncQueryCacheTests
            Inherits TestBase

            Private _marsConnection As SqlConnection
            Private Function MarsConnection() As SqlConnection
                If _marsConnection Is Nothing Then
                    _marsConnection = TestBase.GetOpenConnection(True)
                End If
                Return _marsConnection
            End Function

            <TestMethod>
            Public Sub AssertNoCacheWorksForQueryMultiple()
                Const a As Integer = 123, b As Integer = 456
                Dim cmdDef As New CommandDefinition("select @a; select @b;", New With {Key a, Key b}, commandType:=CommandType.Text, flags:=CommandFlags.NoCache)

                Dim c, d As Integer
                SqlMapper.PurgeQueryCache()
                Dim before As Integer = SqlMapper.GetCachedSQLCount()
                Using multi As SqlMapper.GridReader = MarsConnection.QueryMultiple(cmdDef)
                    c = multi.Read(Of Integer)().Single()
                    d = multi.Read(Of Integer)().Single()
                End Using
                Dim after As Integer = SqlMapper.GetCachedSQLCount()
                Assert.AreEqual(0, before)
                Assert.AreEqual(0, after)
                Assert.AreEqual(123, c)
                Assert.AreEqual(456, d)
            End Sub
        End Class

        Private Class BasicType
            Public Property Value() As String
        End Class

        <TestMethod>
        Public Async Function TypeBasedViaTypeAsync() As Task
            Dim type As Type = Common.GetSomeType()

            Dim actual As Object = (Await MarsConnection.QueryAsync(type, "select @A as [A], @B as [B]", New With {Key .A = 123, Key .B = "abc"}).ConfigureAwait(False)).FirstOrDefault()
            Assert.AreEqual(CObj(actual).GetType(), type)
            Dim someType As SomeType = CType(actual, SomeType)
            Dim a As Integer = someType.A
            Dim b As String = someType.B
            Assert.AreEqual(123, a)
            Assert.AreEqual("abc", b)
        End Function

        <TestMethod>
        Public Async Function TypeBasedViaTypeAsyncFirstOrDefault() As Task
            Dim type As Type = Common.GetSomeType()

            Dim actual As Object = Await MarsConnection.QueryFirstOrDefaultAsync(type, "select @A as [A], @B as [B]", New With {Key .A = 123, Key .B = "abc"}).ConfigureAwait(False)
            Assert.AreEqual(CObj(actual).GetType(), type)
            Dim someType As SomeType = CType(actual, SomeType)
            Dim a As Integer = someType.A
            Dim b As String = someType.B
            Assert.AreEqual(123, a)
            Assert.AreEqual("abc", b)
        End Function

        <TestMethod>
        Public Async Function Issue22_ExecuteScalarAsync() As Task
            Dim i As Integer = Await Me.Connection.ExecuteScalarAsync(Of Integer)("select 123").ConfigureAwait(False)
            Assert.AreEqual(123, i)

            i = Await Me.Connection.ExecuteScalarAsync(Of Integer)("select cast(123 as bigint)").ConfigureAwait(False)
            Assert.AreEqual(123, i)

            Dim j As Long = Await Me.Connection.ExecuteScalarAsync(Of Long)("select 123").ConfigureAwait(False)
            Assert.AreEqual(123L, j)

            j = Await Me.Connection.ExecuteScalarAsync(Of Long)("select cast(123 as bigint)").ConfigureAwait(False)
            Assert.AreEqual(123L, j)

            Dim k? As Integer = Await Me.Connection.ExecuteScalarAsync(Of Integer?)("select @i", New With {Key .i = Nothing}).ConfigureAwait(False)
            Assert.IsNull(k)
        End Function

        <TestMethod>
        Public Async Function Issue346_QueryAsyncConvert() As Task
            Dim i As Integer = (Await Me.Connection.QueryAsync(Of Integer)("Select Cast(123 as bigint)").ConfigureAwait(False)).First()
            Assert.AreEqual(123, i)
        End Function

        <TestMethod>
        Public Async Function TestSupportForDynamicParametersOutputExpressionsAsync() As Task
            Dim bob As New Person With {.Name = "bob", .PersonId = 1, .Address = New Address With {.PersonId = 2}}

            Dim p As New DynamicParameters(bob)
            p.Output(bob, Function(b) b.PersonId)
            p.Output(bob, Function(b) b.Occupation)
            p.Output(bob, Function(b) b.NumberOfLegs)
            p.Output(bob, Function(b) b.Address.Name)
            p.Output(bob, Function(b) b.Address.PersonId)

            Await Me.Connection.ExecuteAsync("" & ControlChars.CrLf & "SET @Occupation = 'grillmaster' " & ControlChars.CrLf & "SET @PersonId = @PersonId + 1 " & ControlChars.CrLf & "SET @NumberOfLegs = @NumberOfLegs - 1" & ControlChars.CrLf & "SET @AddressName = 'bobs burgers'" & ControlChars.CrLf & "SET @AddressPersonId = @PersonId", p).ConfigureAwait(False)

            Assert.AreEqual("grillmaster", bob.Occupation)
            Assert.AreEqual(2, bob.PersonId)
            Assert.AreEqual(1, bob.NumberOfLegs)
            Assert.AreEqual("bobs burgers", bob.Address.Name)
            Assert.AreEqual(2, bob.Address.PersonId)
        End Function

        <TestMethod>
        Public Async Function TestSupportForDynamicParametersOutputExpressions_ScalarAsync() As Task
            Dim bob As New Person With {.Name = "bob", .PersonId = 1, .Address = New Address With {.PersonId = 2}}

            Dim p As New DynamicParameters(bob)
            p.Output(bob, Function(b) b.PersonId)
            p.Output(bob, Function(b) b.Occupation)
            p.Output(bob, Function(b) b.NumberOfLegs)
            p.Output(bob, Function(b) b.Address.Name)
            p.Output(bob, Function(b) b.Address.PersonId)

            Dim result As Integer = CInt(Fix(Await Me.Connection.ExecuteScalarAsync("" & ControlChars.CrLf & "SET @Occupation = 'grillmaster' " & ControlChars.CrLf & "SET @PersonId = @PersonId + 1 " & ControlChars.CrLf & "SET @NumberOfLegs = @NumberOfLegs - 1" & ControlChars.CrLf & "SET @AddressName = 'bobs burgers'" & ControlChars.CrLf & "SET @AddressPersonId = @PersonId" & ControlChars.CrLf & "select 42", p).ConfigureAwait(False)))

            Assert.AreEqual("grillmaster", bob.Occupation)
            Assert.AreEqual(2, bob.PersonId)
            Assert.AreEqual(1, bob.NumberOfLegs)
            Assert.AreEqual("bobs burgers", bob.Address.Name)
            Assert.AreEqual(2, bob.Address.PersonId)
            Assert.AreEqual(42, result)
        End Function

        <TestMethod>
        Public Async Function TestSupportForDynamicParametersOutputExpressions_Query_Default() As Task
            Dim bob As New Person With {.Name = "bob", .PersonId = 1, .Address = New Address With {.PersonId = 2}}

            Dim p As New DynamicParameters(bob)
            p.Output(bob, Function(b) b.PersonId)
            p.Output(bob, Function(b) b.Occupation)
            p.Output(bob, Function(b) b.NumberOfLegs)
            p.Output(bob, Function(b) b.Address.Name)
            p.Output(bob, Function(b) b.Address.PersonId)

            Dim result As Integer = (Await Me.Connection.QueryAsync(Of Integer)("" & ControlChars.CrLf & "SET @Occupation = 'grillmaster' " & ControlChars.CrLf & "SET @PersonId = @PersonId + 1 " & ControlChars.CrLf & "SET @NumberOfLegs = @NumberOfLegs - 1" & ControlChars.CrLf & "SET @AddressName = 'bobs burgers'" & ControlChars.CrLf & "SET @AddressPersonId = @PersonId" & ControlChars.CrLf & "select 42", p).ConfigureAwait(False)).Single()

            Assert.AreEqual("grillmaster", bob.Occupation)
            Assert.AreEqual(2, bob.PersonId)
            Assert.AreEqual(1, bob.NumberOfLegs)
            Assert.AreEqual("bobs burgers", bob.Address.Name)
            Assert.AreEqual(2, bob.Address.PersonId)
            Assert.AreEqual(42, result)
        End Function

        <TestMethod>
        Public Async Function TestSupportForDynamicParametersOutputExpressions_Query_BufferedAsync() As Task
            Dim bob As New Person With {.Name = "bob", .PersonId = 1, .Address = New Address With {.PersonId = 2}}

            Dim p As New DynamicParameters(bob)
            p.Output(bob, Function(b) b.PersonId)
            p.Output(bob, Function(b) b.Occupation)
            p.Output(bob, Function(b) b.NumberOfLegs)
            p.Output(bob, Function(b) b.Address.Name)
            p.Output(bob, Function(b) b.Address.PersonId)

            Dim result As Integer = (Await Me.Connection.QueryAsync(Of Integer)(New CommandDefinition("" & ControlChars.CrLf & "SET @Occupation = 'grillmaster' " & ControlChars.CrLf & "SET @PersonId = @PersonId + 1 " & ControlChars.CrLf & "SET @NumberOfLegs = @NumberOfLegs - 1" & ControlChars.CrLf & "SET @AddressName = 'bobs burgers'" & ControlChars.CrLf & "SET @AddressPersonId = @PersonId" & ControlChars.CrLf & "select 42", p, flags:=CommandFlags.Buffered)).ConfigureAwait(False)).Single()

            Assert.AreEqual("grillmaster", bob.Occupation)
            Assert.AreEqual(2, bob.PersonId)
            Assert.AreEqual(1, bob.NumberOfLegs)
            Assert.AreEqual("bobs burgers", bob.Address.Name)
            Assert.AreEqual(2, bob.Address.PersonId)
            Assert.AreEqual(42, result)
        End Function

        <TestMethod>
        Public Async Function TestSupportForDynamicParametersOutputExpressions_Query_NonBufferedAsync() As Task
            Dim bob As New Person With {.Name = "bob", .PersonId = 1, .Address = New Address With {.PersonId = 2}}

            Dim p As New DynamicParameters(bob)
            p.Output(bob, Function(b) b.PersonId)
            p.Output(bob, Function(b) b.Occupation)
            p.Output(bob, Function(b) b.NumberOfLegs)
            p.Output(bob, Function(b) b.Address.Name)
            p.Output(bob, Function(b) b.Address.PersonId)

            Dim result As Integer = (Await Me.Connection.QueryAsync(Of Integer)(New CommandDefinition("" & ControlChars.CrLf & "SET @Occupation = 'grillmaster' " & ControlChars.CrLf & "SET @PersonId = @PersonId + 1 " & ControlChars.CrLf & "SET @NumberOfLegs = @NumberOfLegs - 1" & ControlChars.CrLf & "SET @AddressName = 'bobs burgers'" & ControlChars.CrLf & "SET @AddressPersonId = @PersonId" & ControlChars.CrLf & "select 42", p, flags:=CommandFlags.None)).ConfigureAwait(False)).Single()

            Assert.AreEqual("grillmaster", bob.Occupation)
            Assert.AreEqual(2, bob.PersonId)
            Assert.AreEqual(1, bob.NumberOfLegs)
            Assert.AreEqual("bobs burgers", bob.Address.Name)
            Assert.AreEqual(2, bob.Address.PersonId)
            Assert.AreEqual(42, result)
        End Function

        <TestMethod>
        Public Async Function TestSupportForDynamicParametersOutputExpressions_QueryMultipleAsync() As Task
            Dim bob As New Person With {.Name = "bob", .PersonId = 1, .Address = New Address With {.PersonId = 2}}

            Dim p As New DynamicParameters(bob)
            p.Output(bob, Function(b) b.PersonId)
            p.Output(bob, Function(b) b.Occupation)
            p.Output(bob, Function(b) b.NumberOfLegs)
            p.Output(bob, Function(b) b.Address.Name)
            p.Output(bob, Function(b) b.Address.PersonId)

            Dim x, y As Integer
            Using multi As SqlMapper.GridReader = Await Me.Connection.QueryMultipleAsync("" & ControlChars.CrLf & "SET @Occupation = 'grillmaster' " & ControlChars.CrLf & "SET @PersonId = @PersonId + 1 " & ControlChars.CrLf & "SET @NumberOfLegs = @NumberOfLegs - 1" & ControlChars.CrLf & "SET @AddressName = 'bobs burgers'" & ControlChars.CrLf & "select 42" & ControlChars.CrLf & "select 17" & ControlChars.CrLf & "SET @AddressPersonId = @PersonId", p).ConfigureAwait(False)
                x = multi.ReadAsync(Of Integer)().Result.Single()
                y = multi.ReadAsync(Of Integer)().Result.Single()
            End Using

            Assert.AreEqual("grillmaster", bob.Occupation)
            Assert.AreEqual(2, bob.PersonId)
            Assert.AreEqual(1, bob.NumberOfLegs)
            Assert.AreEqual("bobs burgers", bob.Address.Name)
            Assert.AreEqual(2, bob.Address.PersonId)
            Assert.AreEqual(42, x)
            Assert.AreEqual(17, y)
        End Function

        <TestMethod>
        Public Async Function TestSubsequentQueriesSuccessAsync() As Task
            Dim data0 As IEnumerable(Of AsyncFoo0) = (Await Me.Connection.QueryAsync(Of AsyncFoo0)("select 1 as [Id] where 1 = 0").ConfigureAwait(False)).ToList()
            Assert.IsFalse(data0.Any)

            Dim data1 As IEnumerable(Of AsyncFoo1) = (Await Me.Connection.QueryAsync(Of AsyncFoo1)(New CommandDefinition("select 1 as [Id] where 1 = 0", flags:=CommandFlags.Buffered)).ConfigureAwait(False)).ToList()
            Assert.IsFalse(data1.Any)

            Dim data2 As IEnumerable(Of AsyncFoo2) = (Await Me.Connection.QueryAsync(Of AsyncFoo2)(New CommandDefinition("select 1 as [Id] where 1 = 0", flags:=CommandFlags.None)).ConfigureAwait(False)).ToList()
            Assert.IsFalse(data2.Any)

            data0 = (Await Me.Connection.QueryAsync(Of AsyncFoo0)("select 1 as [Id] where 1 = 0").ConfigureAwait(False)).ToList()
            Assert.IsFalse(data0.Any)

            data1 = (Await Me.Connection.QueryAsync(Of AsyncFoo1)(New CommandDefinition("select 1 as [Id] where 1 = 0", flags:=CommandFlags.Buffered)).ConfigureAwait(False)).ToList()
            Assert.IsFalse(data1.Any)

            data2 = (Await Me.Connection.QueryAsync(Of AsyncFoo2)(New CommandDefinition("select 1 as [Id] where 1 = 0", flags:=CommandFlags.None)).ConfigureAwait(False)).ToList()
            Assert.IsFalse(data2.Any)
        End Function

        Private Class AsyncFoo0
            Public Property Id() As Integer
        End Class

        Private Class AsyncFoo1
            Public Property Id() As Integer
        End Class

        Private Class AsyncFoo2
            Public Property Id() As Integer
        End Class

        <TestMethod>
        Public Async Function TestSchemaChangedViaFirstOrDefaultAsync() As Task
            Await Me.Connection.ExecuteAsync("create table #dog(Age int, Name nvarchar(max)) insert #dog values(1, 'Alf')").ConfigureAwait(False)
            Try
                Dim d As Dog = Await Me.Connection.QueryFirstOrDefaultAsync(Of Dog)("select * from #dog").ConfigureAwait(False)
                Assert.AreEqual("Alf", d.Name)
                Assert.AreEqual(1, d.Age)
                Connection.Execute("alter table #dog drop column Name")
                d = Await Me.Connection.QueryFirstOrDefaultAsync(Of Dog)("select * from #dog").ConfigureAwait(False)
                Assert.IsNull(d.Name)
                Assert.AreEqual(1, d.Age)
            Finally
            End Try
            Await Me.Connection.ExecuteAsync("drop table #dog").ConfigureAwait(False)
        End Function

        <TestMethod>
        Public Async Function TestMultiMapArbitraryMapsAsync() As Task
            ' please excuse the trite example, but it is easier to follow than a more real-world one
            Const createSql As String = String.Empty & ControlChars.CrLf & "                create table #ReviewBoards (Id int, Name varchar(20), User1Id int, User2Id int, User3Id int, User4Id int, User5Id int, User6Id int, User7Id int, User8Id int, User9Id int)" & ControlChars.CrLf & "                create table #Users (Id int, Name varchar(20))" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Users values(1, 'User 1')" & ControlChars.CrLf & "                insert #Users values(2, 'User 2')" & ControlChars.CrLf & "                insert #Users values(3, 'User 3')" & ControlChars.CrLf & "                insert #Users values(4, 'User 4')" & ControlChars.CrLf & "                insert #Users values(5, 'User 5')" & ControlChars.CrLf & "                insert #Users values(6, 'User 6')" & ControlChars.CrLf & "                insert #Users values(7, 'User 7')" & ControlChars.CrLf & "                insert #Users values(8, 'User 8')" & ControlChars.CrLf & "                insert #Users values(9, 'User 9')" & ControlChars.CrLf & ControlChars.CrLf & "                insert #ReviewBoards values(1, 'Review Board 1', 1, 2, 3, 4, 5, 6, 7, 8, 9)" & ControlChars.CrLf & ""
            Await Me.Connection.ExecuteAsync(createSql).ConfigureAwait(False)
            Try
                Const sql As String = String.Empty & ControlChars.CrLf & "                select " & ControlChars.CrLf & "                    rb.Id, rb.Name," & ControlChars.CrLf & "                    u1.*, u2.*, u3.*, u4.*, u5.*, u6.*, u7.*, u8.*, u9.*" & ControlChars.CrLf & "                from #ReviewBoards rb" & ControlChars.CrLf & "                    inner join #Users u1 on u1.Id = rb.User1Id" & ControlChars.CrLf & "                    inner join #Users u2 on u2.Id = rb.User2Id" & ControlChars.CrLf & "                    inner join #Users u3 on u3.Id = rb.User3Id" & ControlChars.CrLf & "                    inner join #Users u4 on u4.Id = rb.User4Id" & ControlChars.CrLf & "                    inner join #Users u5 on u5.Id = rb.User5Id" & ControlChars.CrLf & "                    inner join #Users u6 on u6.Id = rb.User6Id" & ControlChars.CrLf & "                    inner join #Users u7 on u7.Id = rb.User7Id" & ControlChars.CrLf & "                    inner join #Users u8 on u8.Id = rb.User8Id" & ControlChars.CrLf & "                    inner join #Users u9 on u9.Id = rb.User9Id" & ControlChars.CrLf & ""

                Dim types As Type() = {GetType(ReviewBoard), GetType(User), GetType(User), GetType(User), GetType(User), GetType(User), GetType(User), GetType(User), GetType(User), GetType(User)}

                Dim mapper As Func(Of Object(), ReviewBoard) = Function(objects)
                                                                   Dim board As ReviewBoard = CType(objects(0), ReviewBoard)
                                                                   board.User1 = CType(objects(1), User)
                                                                   board.User2 = CType(objects(2), User)
                                                                   board.User3 = CType(objects(3), User)
                                                                   board.User4 = CType(objects(4), User)
                                                                   board.User5 = CType(objects(5), User)
                                                                   board.User6 = CType(objects(6), User)
                                                                   board.User7 = CType(objects(7), User)
                                                                   board.User8 = CType(objects(8), User)
                                                                   board.User9 = CType(objects(9), User)
                                                                   Return board
                                                               End Function

                Dim data As IEnumerable(Of ReviewBoard) = (Await Me.Connection.QueryAsync(Of ReviewBoard)(sql, types, mapper).ConfigureAwait(False)).ToList()

                Dim p As ReviewBoard = data(0)
                Assert.AreEqual(1, p.Id)
                Assert.AreEqual("Review Board 1", p.Name)
                Assert.AreEqual(1, p.User1.Id)
                Assert.AreEqual(2, p.User2.Id)
                Assert.AreEqual(3, p.User3.Id)
                Assert.AreEqual(4, p.User4.Id)
                Assert.AreEqual(5, p.User5.Id)
                Assert.AreEqual(6, p.User6.Id)
                Assert.AreEqual(7, p.User7.Id)
                Assert.AreEqual(8, p.User8.Id)
                Assert.AreEqual(9, p.User9.Id)
                Assert.AreEqual("User 1", p.User1.Name)
                Assert.AreEqual("User 2", p.User2.Name)
                Assert.AreEqual("User 3", p.User3.Name)
                Assert.AreEqual("User 4", p.User4.Name)
                Assert.AreEqual("User 5", p.User5.Name)
                Assert.AreEqual("User 6", p.User6.Name)
                Assert.AreEqual("User 7", p.User7.Name)
                Assert.AreEqual("User 8", p.User8.Name)
                Assert.AreEqual("User 9", p.User9.Name)
            Finally
                Connection.Execute("drop table #Users drop table #ReviewBoards")
            End Try
        End Function

        <TestMethod>
        Public Async Function Issue157_ClosedReaderAsync() As Task
            Dim args As Object = New With {Key .x = 42}
            Const sql As String = "select 123 as [A], 'abc' as [B] where @x=42"
            Dim row As SomeType = (Await Me.Connection.QueryAsync(Of SomeType)(New CommandDefinition(sql, args, flags:=CommandFlags.None)).ConfigureAwait(False)).Single()
            Assert.IsNotNull(row)
            Assert.AreEqual(123, row.A)
            Assert.AreEqual("abc", row.B)

            args = New With {Key .x = 5}
            Assert.IsFalse((Await Me.Connection.QueryAsync(Of SomeType)(New CommandDefinition(sql, args, flags:=CommandFlags.None)).ConfigureAwait(False)).Any())
        End Function

        <TestMethod>
        Public Async Function TestAtEscaping() As Task
            Dim id As Integer = (Await Me.Connection.QueryAsync(Of Integer)("" & ControlChars.CrLf & "                declare @@Name int" & ControlChars.CrLf & "                select @@Name = @Id+1" & ControlChars.CrLf & "                select @@Name" & ControlChars.CrLf & "                ", New Product With {.Id = 1}).ConfigureAwait(False)).Single()
            Assert.AreEqual(2, id)
        End Function

        <TestMethod>
        Public Async Function Issue1281_DataReaderOutOfOrderAsync() As Task
            Using reader As IDataReader = Await Me.Connection.ExecuteReaderAsync("Select 0, 1, 2").ConfigureAwait(False)
                Assert.IsTrue(reader.Read())
                Assert.AreEqual(2, reader.GetInt32(2))
                Assert.AreEqual(0, reader.GetInt32(0))
                Assert.AreEqual(1, reader.GetInt32(1))
                Assert.IsFalse(reader.Read())
            End Using
        End Function

        <TestMethod>
        Public Async Function Issue563_QueryAsyncShouldThrowException() As Task
            Try
                Dim data As IEnumerable(Of Integer) = (Await Me.Connection.QueryAsync(Of Integer)("select 1 union all select 2; RAISERROR('after select', 16, 1);").ConfigureAwait(False)).ToList()
                Assert.IsTrue(False, "Expected Exception")
            Catch ex As SqlException When (ex.Message = "after select")
            End Try
        End Function
    End Class
End Namespace
