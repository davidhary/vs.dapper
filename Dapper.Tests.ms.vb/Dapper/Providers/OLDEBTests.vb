﻿#If OLEDB Then
Imports System
Imports System.Data.OleDb
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Namespace DapperTests
	Public Class OLDEBTests
		Inherits TestBase

		Public Shared OleDbConnectionString As [Function](String)If(IsAppVeyor, "Provider=SQLOLEDB;Data Source=(local)\SQL2016;Initial Catalog=tempdb;User Id=sa;Password=Password12!", "Provider=SQLOLEDB;Data Source=.;Initial Catalog=tempdb;Integrated Security=SSPI")

		Public Function GetOleDbConnection() As OleDbConnection
			Dim conn = New OleDbConnection(OleDbConnectionString)
			conn.Open()
			Return conn
		End Function

		' see https://stackoverflow.com/q/18847510/23354
		<TestMethod> _
		Public Sub TestOleDbParameters()
			Using conn = GetOleDbConnection()
				Dim row = conn.Query("select Id = ?, Age = ?", New With {Key .foo = 12, Key .bar = 23}).Single() ' these names DO NOT MATTER!!!
				Dim age As Integer = row.Age
				Dim id As Integer = row.Id
				Assert.AreEqual(23, age)
				Assert.AreEqual(12, id)
			End Using
		End Sub

		<TestMethod> _
		Public Sub PseudoPositionalParameters_Simple()
			Using connection = GetOleDbConnection()
				Dim value As Integer = connection.Query(Of Integer)("select ?x? + ?y_2? + ?z?", New With {Key .x = 1, Key .y_2 = 3, Key .z = 5, Key .z2 = 24}).Single()
				Assert.AreEqual(9, value)
			End Using
		End Sub

		<TestMethod> _
		Public Sub Issue601_InternationalParameterNamesWork_OleDb()
			' pseudo-positional
			Using connection = GetOleDbConnection()
				Dim value As Integer = connection.QuerySingle(Of Integer)("select ?æøå٦?", New With {Key .æøå٦ = 42})
			End Using
		End Sub

		<TestMethod> _
		Public Sub PseudoPositionalParameters_Dynamic()
			Using connection = GetOleDbConnection()
				Dim args = New DynamicParameters()
				args.Add("x", 1)
				args.Add("y_2", 3)
				args.Add("z", 5)
				args.Add("z2", 24)
				Dim value As Integer = connection.Query(Of Integer)("select ?x? + ?y_2? + ?z?", args).Single()
				Assert.AreEqual(9, value)
			End Using
		End Sub

		<TestMethod> _
		Public Sub PseudoPositionalParameters_ReusedParameter()
			Using connection = GetOleDbConnection()
				Dim ex = Assert.Throws(Of InvalidOperationException)(Function() connection.Query(Of Integer)("select ?x? + ?y_2? + ?x?", New With {Key .x = 1, Key .y_2 = 3}).Single())
				Assert.AreEqual("When passing parameters by position, each parameter can only be referenced once", ex.Message)
			End Using
		End Sub

		<TestMethod> _
		Public Sub Issue569_SO38527197_PseudoPositionalParameters_In()
			Using connection = GetOleDbConnection()
				Dim ids() As Integer = { 1, 2, 5, 7 }
				Dim list = connection.Query(Of Integer)("select * from string_split('1,2,3,4,5',',') where value in ?ids?", New With {Key ids}).AsList()
				list.Sort()
				Assert.AreEqual("1,2,5", String.Join(",", list))
			End Using
		End Sub

		<TestMethod> _
		Public Sub PseudoPositional_CanUseVariable()
			Using connection = GetOleDbConnection()
				Const id As Integer = 42
				Dim row = connection.QuerySingle("declare @id int = ?id?; select @id as [A], @id as [B];", New With {Key id})
				Dim a As Integer = CInt(Fix(row.A))
				Dim b As Integer = CInt(Fix(row.B))
				Assert.AreEqual(42, a)
				Assert.AreEqual(42, b)
			End Using
		End Sub

		<TestMethod> _
		Public Sub PseudoPositional_CannotUseParameterMultipleTimes()
			Using connection = GetOleDbConnection()
				Dim ex = Assert.Throws(Of InvalidOperationException)(Sub()
					Const id As Integer = 42
					connection.QuerySingle("select ?id? as [A], ?id? as [B];", New With {Key id})
				End Sub)
				Assert.AreEqual("When passing parameters by position, each parameter can only be referenced once", ex.Message)
			End Using
		End Sub

		<TestMethod> _
		Public Sub PseudoPositionalParameters_ExecSingle()
			Using connection = GetOleDbConnection()
				Dim data = New With {Key .x = 6}
				connection.Execute("create table #named_single(val int not null)")
				Dim count As Integer = connection.Execute("insert #named_single (val) values (?x?)", data)
				Dim sum As Integer = CInt(Fix(connection.ExecuteScalar("select sum(val) from #named_single")))
				Assert.AreEqual(1, count)
				Assert.AreEqual(6, sum)
			End Using
		End Sub

		<TestMethod> _
		Public Sub PseudoPositionalParameters_ExecMulti()
			Using connection = GetOleDbConnection()
				Dim data = { New With {Key .x = 1, Key .y = 1}, New With {Key .x = 3, Key .y = 1}, New With {Key .x = 6, Key .y = 1} }
				connection.Execute("create table #named_multi(val int not null)")
				Dim count As Integer = connection.Execute("insert #named_multi (val) values (?x?)", data)
				Dim sum As Integer = CInt(Fix(connection.ExecuteScalar("select sum(val) from #named_multi")))
				Assert.AreEqual(3, count)
				Assert.AreEqual(10, sum)
			End Using
		End Sub

		<TestMethod> _
		Public Sub Issue457_NullParameterValues()
			Const sql As String = String.Empty & ControlChars.CrLf & "DECLARE @since DATETIME, @customerCode nvarchar(10)" & ControlChars.CrLf & "SET @since = ? -- ODBC parameter" & ControlChars.CrLf & "SET @customerCode = ? -- ODBC parameter" & ControlChars.CrLf & ControlChars.CrLf & "SELECT @since as [Since], @customerCode as [Code]"

			Using connection = GetOleDbConnection()
				Dim since? As DateTime = Nothing ' DateTime.Now.Date;
				Const code As String = Nothing ' "abc";
				Dim row = connection.QuerySingle(sql, New With {Key since, Key .customerCode = code})
				Dim a = CType(row.Since, Date?)
				Dim b = CStr(row.Code)

				Assert.AreEqual(since, a)
				Assert.AreEqual(code, b)
			End Using
		End Sub

		<TestMethod> _
		Public Sub Issue457_NullParameterValues_Named()
			Const sql As String = String.Empty & ControlChars.CrLf & "DECLARE @since DATETIME, @customerCode nvarchar(10)" & ControlChars.CrLf & "SET @since = ?since? -- ODBC parameter" & ControlChars.CrLf & "SET @customerCode = ?customerCode? -- ODBC parameter" & ControlChars.CrLf & ControlChars.CrLf & "SELECT @since as [Since], @customerCode as [Code]"

			Using connection = GetOleDbConnection()
				Dim since? As DateTime = Nothing ' DateTime.Now.Date;
				Const code As String = Nothing ' "abc";
				Dim row = connection.QuerySingle(sql, New With {Key since, Key .customerCode = code})
				Dim a = CType(row.Since, Date?)
				Dim b = CStr(row.Code)

				Assert.AreEqual(since, a)
				Assert.AreEqual(code, b)
			End Using
		End Sub

		<TestMethod> _
		Public Async Sub Issue457_NullParameterValues_MultiAsync()
			Const sql As String = String.Empty & ControlChars.CrLf & "DECLARE @since DATETIME, @customerCode nvarchar(10)" & ControlChars.CrLf & "SET @since = ? -- ODBC parameter" & ControlChars.CrLf & "SET @customerCode = ? -- ODBC parameter" & ControlChars.CrLf & ControlChars.CrLf & "SELECT @since as [Since], @customerCode as [Code]"

			Using connection = GetOleDbConnection()
				Dim since? As DateTime = Nothing ' DateTime.Now.Date;
				Const code As String = Nothing ' "abc";
				Using multi = Await connection.QueryMultipleAsync(sql, New With {Key since, Key .customerCode = code}).ConfigureAwait(False)
					Dim row = Await multi.ReadSingleAsync().ConfigureAwait(False)
					Dim a = CType(row.Since, Date?)
					Dim b = CStr(row.Code)

					Assert.AreEqual(a, since)
					Assert.AreEqual(b, code)
				End Using
			End Using
		End Sub

		<TestMethod> _
		Public Async Sub Issue457_NullParameterValues_MultiAsync_Named()
			Const sql As String = String.Empty & ControlChars.CrLf & "DECLARE @since DATETIME, @customerCode nvarchar(10)" & ControlChars.CrLf & "SET @since = ?since? -- ODBC parameter" & ControlChars.CrLf & "SET @customerCode = ?customerCode? -- ODBC parameter" & ControlChars.CrLf & ControlChars.CrLf & "SELECT @since as [Since], @customerCode as [Code]"

			Using connection = GetOleDbConnection()
				Dim since? As DateTime = Nothing ' DateTime.Now.Date;
				Const code As String = Nothing ' "abc";
				Using multi = Await connection.QueryMultipleAsync(sql, New With {Key since, Key .customerCode = code}).ConfigureAwait(False)
					Dim row = Await multi.ReadSingleAsync().ConfigureAwait(False)
					Dim a = CType(row.Since, Date?)
					Dim b = CStr(row.Code)

					Assert.AreEqual(a, since)
					Assert.AreEqual(b, code)
				End Using
			End Using
		End Sub
	End Class
End Namespace
#End If
