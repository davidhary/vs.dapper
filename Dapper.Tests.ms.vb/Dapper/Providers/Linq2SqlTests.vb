﻿#If LINQ2SQL Then
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Namespace DapperTests
	Public Class Linq2SqlTests
		Inherits TestBase

		<TestMethod> _
		Public Sub TestLinqBinaryToClass()
			Dim orig(19) As Byte
			CType(New Random(123456), Random).NextBytes(orig)
			Dim input = New System.Data.Linq.Binary(orig)

			Dim output = connection.Query(Of WithBinary)("select @input as [Value]", New With {Key input}).First().Value

			Assert.AreEqual(orig, output.ToArray())
		End Sub

		<TestMethod> _
		Public Sub TestLinqBinaryRaw()
			Dim orig(19) As Byte
			CType(New Random(123456), Random).NextBytes(orig)
			Dim input = New System.Data.Linq.Binary(orig)

			Dim output = connection.Query(Of System.Data.Linq.Binary)("select @input as [Value]", New With {Key input}).First()

			Assert.AreEqual(orig, output.ToArray())
		End Sub

		Private Class WithBinary
			Public Property Value() As System.Data.Linq.Binary
		End Class

		Private Class NoDefaultConstructorWithBinary
			Public Property Value() As System.Data.Linq.Binary
			Public Property Ynt() As Integer
			Public Sub New(ByVal val As System.Data.Linq.Binary)
				Value = val
			End Sub
		End Class

		<TestMethod> _
		Public Sub TestNoDefaultConstructorBinary()
			Dim orig(19) As Byte
			CType(New Random(123456), Random).NextBytes(orig)
			Dim input = New System.Data.Linq.Binary(orig)
			Dim output = connection.Query(Of NoDefaultConstructorWithBinary)("select @input as val", New With {Key input}).First().Value
			Assert.AreEqual(orig, output.ToArray())
		End Sub
	End Class
End Namespace
#End If
