﻿Imports System
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Namespace DapperTests
	Public Class MySQLTests
		Inherits TestBase

		Private Shared Function GetMySqlConnection(Optional ByVal open As Boolean = True, Optional ByVal convertZeroDatetime As Boolean = False, Optional ByVal allowZeroDatetime As Boolean = False) As MySql.Data.MySqlClient.MySqlConnection
			Dim cs As String = If(IsAppVeyor, "Server=localhost;Database=test;Uid=root;Pwd=Password12!;", "Server=localhost;Database=tests;Uid=test;Pwd=pass;")
			Dim csb = New MySql.Data.MySqlClient.MySqlConnectionStringBuilder(cs) With {.AllowZeroDateTime = allowZeroDatetime, .ConvertZeroDateTime = convertZeroDatetime}
			Dim conn = New MySql.Data.MySqlClient.MySqlConnection(csb.ConnectionString)
			If open Then
				conn.Open()
			End If
			Return conn
		End Function

		<FactMySql> _
		Public Sub DapperEnumValue_Mysql()
			Using conn = GetMySqlConnection()
				Common.DapperEnumValue(conn)
			End Using
		End Sub

		<FactMySql(Skip := "See https://github.com/StackExchange/Dapper/issues/552, not resolved on the MySQL end.")> _
		Public Sub Issue552_SignedUnsignedBooleans()
			Using conn = GetMySqlConnection(True, False, False)
				conn.Execute("" & ControlChars.CrLf & "CREATE TEMPORARY TABLE IF NOT EXISTS `bar` (" & ControlChars.CrLf & "  `id` INT NOT NULL," & ControlChars.CrLf & "  `bool_val` BOOL NULL," & ControlChars.CrLf & "  PRIMARY KEY (`id`));" & ControlChars.CrLf & "  " & ControlChars.CrLf & "  truncate table bar;" & ControlChars.CrLf & "  insert bar (id, bool_val) values (1, null);" & ControlChars.CrLf & "  insert bar (id, bool_val) values (2, 0);" & ControlChars.CrLf & "  insert bar (id, bool_val) values (3, 1);" & ControlChars.CrLf & "  insert bar (id, bool_val) values (4, null);" & ControlChars.CrLf & "  insert bar (id, bool_val) values (5, 1);" & ControlChars.CrLf & "  insert bar (id, bool_val) values (6, 0);" & ControlChars.CrLf & "  insert bar (id, bool_val) values (7, null);" & ControlChars.CrLf & "  insert bar (id, bool_val) values (8, 1);")

				Dim rows = conn.Query(Of MySqlHasBool)("select * from bar;").ToDictionary(Function(x) x.Id)

				Assert.Null(rows(1).Bool_Val)
				Assert.IsFalse(rows(2).Bool_Val)
				Assert.IsTrue(rows(3).Bool_Val)
				Assert.Null(rows(4).Bool_Val)
				Assert.IsTrue(rows(5).Bool_Val)
				Assert.IsFalse(rows(6).Bool_Val)
				Assert.Null(rows(7).Bool_Val)
				Assert.IsTrue(rows(8).Bool_Val)
			End Using
		End Sub

		Private Class MySqlHasBool
			Public Property Id() As Integer
			Public Property Bool_Val() As Boolean?
		End Class

		<FactMySql> _
		Public Sub Issue295_NullableDateTime_MySql_Default()
			Using conn = GetMySqlConnection(True, False, False)
				Common.TestDateTime(conn)
			End Using
		End Sub

		<FactMySql> _
		Public Sub Issue295_NullableDateTime_MySql_ConvertZeroDatetime()
			Using conn = GetMySqlConnection(True, True, False)
				Common.TestDateTime(conn)
			End Using
		End Sub

		<FactMySql(Skip := "See https://github.com/StackExchange/Dapper/issues/295, AllowZeroDateTime=True is not supported")> _
		Public Sub Issue295_NullableDateTime_MySql_AllowZeroDatetime()
			Using conn = GetMySqlConnection(True, False, True)
				Common.TestDateTime(conn)
			End Using
		End Sub

		<FactMySql(Skip := "See https://github.com/StackExchange/Dapper/issues/295, AllowZeroDateTime=True is not supported")> _
		Public Sub Issue295_NullableDateTime_MySql_ConvertAllowZeroDatetime()
			Using conn = GetMySqlConnection(True, True, True)
				Common.TestDateTime(conn)
			End Using
		End Sub

		<FactMySql> _
		Public Sub Issue426_SO34439033_DateTimeGainsTicks()
			Using conn = GetMySqlConnection(True, True, True)
				Try ' don't care
					conn.Execute("drop table Issue426_Test")
				Catch
				End Try
				Try ' don't care
					conn.Execute("create table Issue426_Test (Id int not null, Time time not null)")
				Catch
				End Try
				Const ticks As Long = 553440000000
				Const Id As Integer = 426

				Dim localObj = New Issue426_Test With {.Id = Id, .Time = TimeSpan.FromTicks(ticks)}
				conn.Execute("replace into Issue426_Test values (@Id,@Time)", localObj)

				Dim dbObj = conn.Query(Of Issue426_Test)("select * from Issue426_Test where Id = @id", New With {Key .id = Id}).Single()
				Assert.AreEqual(Id, dbObj.Id)
				Assert.AreEqual(ticks, dbObj.Time.Value.Ticks)
			End Using
		End Sub

		<FactMySql> _
		Public Sub SO36303462_Tinyint_Bools()
			Using conn = GetMySqlConnection(True, True, True)
				Try ' don't care
					conn.Execute("drop table SO36303462_Test")
				Catch
				End Try
				conn.Execute("create table SO36303462_Test (Id int not null, IsBold tinyint not null);")
				conn.Execute("insert SO36303462_Test (Id, IsBold) values (1,1);")
				conn.Execute("insert SO36303462_Test (Id, IsBold) values (2,0);")
				conn.Execute("insert SO36303462_Test (Id, IsBold) values (3,1);")

				Dim rows = conn.Query(Of SO36303462)("select * from SO36303462_Test").ToDictionary(Function(x) x.Id)
				Assert.AreEqual(3, rows.Count)
				Assert.IsTrue(rows(1).IsBold)
				Assert.IsFalse(rows(2).IsBold)
				Assert.IsTrue(rows(3).IsBold)
			End Using
		End Sub

		Private Class SO36303462
			Public Property Id() As Integer
			Public Property IsBold() As Boolean
		End Class

		Public Class Issue426_Test
			Public Property Id() As Long
			Public Property Time() As TimeSpan?
		End Class

		<AttributeUsage(AttributeTargets.Method, AllowMultiple := False)> _
		Public Class FactMySqlAttribute
			Inherits TestMethodAttribute

			Public Overrides Property Skip() As String
				Get
					Return If(unavailable, MyBase.Skip)
				End Get
				Set(ByVal value As String)
					MyBase.Skip = value
				End Set
			End Property

			Private Shared ReadOnly unavailable As String

			Shared Sub New()
				Try
					Using GetMySqlConnection(True) ' just trying to see if it works
					End Using
				Catch ex As Exception
					unavailable = $"MySql is unavailable: {ex.Message}"
				End Try
			End Sub
		End Class
	End Class
End Namespace
