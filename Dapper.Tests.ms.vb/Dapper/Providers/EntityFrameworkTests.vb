﻿#If ENTITY_FRAMEWORK Then
Imports System.Data.Entity.Spatial
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Namespace DapperTests.Providers
	<Collection("TypeHandlerTests")> _
	Public Class EntityFrameworkTests
		Inherits TestBase

		Public Sub New()
			EntityFramework.Handlers.Register()
		End Sub

		<TestMethod> _
		Public Sub Issue570_DbGeo_HasValues()
			EntityFramework.Handlers.Register()
			Const redmond As String = "POINT (122.1215 47.6740)"
			Dim point As DbGeography = DbGeography.PointFromText(redmond, DbGeography.DefaultCoordinateSystemId)
			Dim orig As DbGeography = point.Buffer(20)

			Dim fromDb = connection.QuerySingle(Of DbGeography)("declare @geos table(geo geography); insert @geos(geo) values(@val); select * from @geos", New With {Key .val = orig})

			Assert.NotNull(fromDb.Area)
			Assert.AreEqual(orig.Area, fromDb.Area)
		End Sub

		<TestMethod> _
		Public Sub Issue22_ExecuteScalar_EntityFramework()
			Dim geo = DbGeography.LineFromText("LINESTRING(-122.360 47.656, -122.343 47.656 )", 4326)
			Dim geo2 = connection.ExecuteScalar(Of DbGeography)("select @geo", New With {Key geo})
			Assert.NotNull(geo2)
		End Sub
	End Class
End Namespace
#End If
