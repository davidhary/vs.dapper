Imports System.Data.SQLite

Imports Dapper
Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> An SQLite test suite. </summary>
    ''' <remarks>
    ''' The test suites here implement TestSuiteBase so that each provider runs the entire set of
    ''' tests without declarations per method If we want to support a new provider, they need only be
    ''' added here - not in multiple places. (c) 2018 Integrated Scientific Resources, Inc. All
    ''' rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 8/16/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class SQLiteTestSuite
        Inherits TestBase

        #Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                Console.WriteLine($".NET {Environment.Version}")
                Console.WriteLine($"Dapper {GetType(SqlMapper).AssemblyQualifiedName}")
                Console.WriteLine($"Connection string: {SQLiteTestSuite.ConnectionString}")
                SQLiteTestSuite.CreateDataTables()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        #End Region

        #Region " IMPLEMENTATION "

        ''' <summary> The connection string. </summary>
        Public Shared ReadOnly ConnectionString As String = ConfigReader.SQLiteMemoryConnectionString

        ''' <summary> Gets the connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The connection. </returns>
        Public Overrides Function GetConnection() As System.Data.IDbConnection
            Return SQLiteTestSuite.GetNewConnection()
        End Function

        ''' <summary> Gets new connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The new connection. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Shared Function GetNewConnection() As System.Data.IDbConnection
            Dim connection As System.Data.IDbConnection = Nothing
            Try
                connection = New SQLiteConnection(SQLiteTestSuite.ConnectionString)
            Catch ex As Exception
                Unavailable = $"is unavailable: {ex.Message}"
            End Try
            Return connection
        End Function

        ''' <summary> Creates data tables. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Shared Sub CreateDataTables()
            Using connection As System.Data.IDbConnection = SQLiteTestSuite.GetNewConnection()
                connection.Open()
            End Using
        End Sub

        #End Region

        #Region " CUSTOM TESTS "

        ''' <summary> (Unit Test Method) tests dapper enum value. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub DapperEnumValueTest()
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                CommonTests.DapperEnumValue(connection)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) issue 466: SQLite hates optimizations. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub SQLiteHatesOptimizations()
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                SqlMapper.ResetTypeHandlers()
                Dim row As HazNameId = connection.Query(Of HazNameId)("select 42 as Id").First
                Assert.AreEqual(42, row.Id)
                row = connection.Query(Of HazNameId)("select 42 as Id").First()
                Assert.AreEqual(42, row.Id)

                SqlMapper.ResetTypeHandlers()
                row = connection.QueryFirst(Of HazNameId)("select 42 as Id")
                Assert.AreEqual(42, row.Id)
                row = connection.QueryFirst(Of HazNameId)("select 42 as Id")
                Assert.AreEqual(42, row.Id)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) Issue 466: SQLite hates optimizations asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function SQLiteHatesOptimizationsAsync() As Task
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                SqlMapper.ResetTypeHandlers()
                Dim row As HazNameId = (Await connection.QueryAsync(Of HazNameId)("select 42 as Id").ConfigureAwait(False)).First()
                Assert.AreEqual(42, row.Id)
                row = (Await connection.QueryAsync(Of HazNameId)("select 42 as Id").ConfigureAwait(False)).First()
                Assert.AreEqual(42, row.Id)

                SqlMapper.ResetTypeHandlers()
                row = Await connection.QueryFirstAsync(Of HazNameId)("select 42 as Id").ConfigureAwait(False)
                Assert.AreEqual(42, row.Id)
                row = Await connection.QueryFirstAsync(Of HazNameId)("select 42 as Id").ConfigureAwait(False)
                Assert.AreEqual(42, row.Id)
            End Using
        End Function

        ''' <summary> (Unit Test Method) sq lite likes parameters with prefix. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub SQLiteLikesParametersWithPrefix()
            Me.SQLiteParameterNaming(True)
        End Sub

        ''' <summary> (Unit Test Method) sq lite likes parameters without prefix. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub SQLiteLikesParametersWithoutPrefix()
            Me.SQLiteParameterNaming(False)
        End Sub

        ''' <summary> Asserts SQLite parameter naming (Issue 467). </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="prefix"> True to prefix. </param>
        Private Sub SQLiteParameterNaming(ByVal prefix As Boolean)
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Dim cmd As System.Data.IDbCommand = connection.CreateCommand()
                cmd.CommandText = "select @foo"
                cmd.Parameters.Add(New SQLiteParameter(If(prefix, "@foo", "foo"), Data.SqlDbType.Int) With {.Value = 42})
                Dim i As Integer = Convert.ToInt32(cmd.ExecuteScalar())
                Assert.AreEqual(42, i)
            End Using
        End Sub

        #End Region

    End Class

End Namespace
