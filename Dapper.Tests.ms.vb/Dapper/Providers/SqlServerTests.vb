Imports Dapper
Namespace Global.isr.Dapper.Tests.DapperTests
    #Region " SQL SERVER "

    ''' <summary> An SQL server test suite. </summary>
    ''' <remarks>
    ''' The test suites here implement TestSuiteBase so that each provider runs the entire set of
    ''' tests without declarations per method If we want to support a new provider, they need only be
    ''' added here - not in multiple places. (c) 2018 Integrated Scientific Resources, Inc. All
    ''' rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 8/16/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class SqlServerTestSuite
        Inherits TestBase

        #Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                Console.WriteLine($".NET {Environment.Version}")
                Console.WriteLine($"Dapper {GetType(SqlMapper).AssemblyQualifiedName}")
                Console.WriteLine($"Connection string: {SqlServerTestSuite.ConnectionString}")
                SqlServerTestSuite.CreateDataTables()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        #End Region

        #Region " IMPLEMENTATION "

        ''' <summary> The connection string. </summary>
        Public Shared ReadOnly ConnectionString As String = ConfigReader.SqlServerConnectionString

        ''' <summary> Gets the connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The connection. </returns>
        Public Overrides Function GetConnection() As System.Data.IDbConnection
            Return SqlServerTestSuite.GetNewConnection()
        End Function

        ''' <summary> Gets new connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The new connection. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Shared Function GetNewConnection() As System.Data.IDbConnection
            Dim connection As System.Data.IDbConnection = Nothing
            Try
                connection = New System.Data.SqlClient.SqlConnection(SqlServerTestSuite.ConnectionString)
            Catch ex As Exception
                Unavailable = $"is unavailable: {ex.Message}"
            End Try
            Return connection
        End Function

        ''' <summary> Creates data tables. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Shared Sub CreateDataTables()
            Using connection As System.Data.IDbConnection = SqlServerTestSuite.GetNewConnection()
                connection.Open()
            End Using
        End Sub

        #End Region

    End Class

#End Region

End Namespace
