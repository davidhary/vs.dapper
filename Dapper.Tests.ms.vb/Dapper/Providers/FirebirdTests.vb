﻿Imports FirebirdSql.Data.FirebirdClient
Imports System.Data
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Namespace DapperTests.Providers
	Public Class FirebirdTests
		Inherits TestBase

		<Fact(Skip := "Bug in Firebird; a PR to fix it has been submitted")> _
		Public Sub Issue178_Firebird()
			Const cs As String = "initial catalog=localhost:database;user id=SYSDBA;password=masterkey"

			Using connection = New FbConnection(cs)
				connection.Open()
				Const sql As String = "select count(*) from Issue178"
				Try
					connection.Execute("drop table Issue178")
				Catch ' don't care
				End Try
				connection.Execute("create table Issue178(id int not null)")
				connection.Execute("insert into Issue178(id) values(42)")
				' raw ADO.net
				Using sqlCmd = New FbCommand(sql, connection)
				Using reader1 As IDataReader = sqlCmd.ExecuteReader()
					Assert.IsTrue(reader1.Read())
					Assert.AreEqual(1, reader1.GetInt32(0))
					Assert.IsFalse(reader1.Read())
					Assert.IsFalse(reader1.NextResult())
				End Using
				End Using

				' dapper
				Using reader2 = connection.ExecuteReader(sql)
					Assert.IsTrue(reader2.Read())
					Assert.AreEqual(1, reader2.GetInt32(0))
					Assert.IsFalse(reader2.Read())
					Assert.IsFalse(reader2.NextResult())
				End Using

				Dim count = connection.Query(Of Integer)(sql).Single()
				Assert.AreEqual(1, count)
			End Using
		End Sub
	End Class
End Namespace
