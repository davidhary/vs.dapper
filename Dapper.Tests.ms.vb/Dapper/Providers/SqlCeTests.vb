﻿Imports System.Data.SqlServerCe
Imports System.IO

Imports Dapper
Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> An SQL CE test suite. </summary>
    ''' <remarks>
    ''' The test suites here implement TestSuiteBase so that each provider runs the entire set of
    ''' tests without declarations per method If we want to support a new provider, they need only be
    ''' added here - not in multiple places. (c) 2018 Integrated Scientific Resources, Inc. All
    ''' rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 8/16/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class SqlCETests
        Inherits TestBase

        #Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                Console.WriteLine($".NET {Environment.Version}")
                Console.WriteLine($"Dapper {GetType(SqlMapper).AssemblyQualifiedName}")
                Console.WriteLine($"Connection string: {SqlCETests.ConnectionString}")
                SqlCETests.CreateDataTables()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        #End Region

        #Region " IMPLEMENTATION "

        ''' <summary> Filename of the file. </summary>
        Private Shared ReadOnly FileName As String = ConfigReader.SqlCeFileName

        ''' <summary> The connection string. </summary>
        Public Shared ReadOnly ConnectionString As String = ConfigReader.SqlCeConnectionString

        ''' <summary> Gets the connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The connection. </returns>
        Public Overrides Function GetConnection() As System.Data.IDbConnection
            Return SqlCETests.GetNewConnection()
        End Function

        ''' <summary> Gets new connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The new connection. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Shared Function GetNewConnection() As System.Data.IDbConnection
            Dim connection As System.Data.IDbConnection = Nothing
            Try
                connection = New SqlCeConnection(SqlCETests.ConnectionString)
            Catch ex As Exception
                Unavailable = $"is unavailable: {ex.Message}"
            End Try
            Return connection
        End Function

        ''' <summary> Creates data tables. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Shared Sub CreateDataTables()
            If File.Exists(FileName) Then
                File.Delete(FileName)
            End If
            Using engine As New SqlCeEngine(SqlCETests.ConnectionString)
                engine.CreateDatabase()
            End Using
            Using connection As System.Data.IDbConnection = SqlCETests.GetNewConnection
                connection.Open()
                connection.Execute("create table Posts (ID int, Title nvarchar(50), Body nvarchar(50), AuthorID int)")
                connection.Execute("create table Authors (ID int, Name nvarchar(50))")
                connection.Execute("insert Posts values (1,'title','body',1)")
                connection.Execute("insert Posts values(2,'title2','body2',null)")
                connection.Execute("insert Authors values(1,'sam')")
                Dim data As IEnumerable(Of PostCE) = connection.Query(Of PostCE, AuthorCE, PostCE)("select * from Posts p left join Authors a on a.ID = p.AuthorID",
                                                                                            Function(post, author)
                                                                                                post.Author = author
                                                                                                Return post
                                                                                            End Function).ToList()
                Dim firstPost As PostCE = data(0)
                Assert.AreEqual("title", firstPost.Title)
                Assert.AreEqual("sam", firstPost.Author.Name)
                Assert.IsNull(data(1).Author)
            End Using
            Console.WriteLine("Created database")
        End Sub

        ''' <summary> A post ce. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Class PostCE

            ''' <summary> Gets or sets the identifier. </summary>
            ''' <value> The identifier. </value>
            Public Property Id() As Integer

            ''' <summary> Gets or sets the title. </summary>
            ''' <value> The title. </value>
            Public Property Title() As String

            ''' <summary> Gets or sets the body. </summary>
            ''' <value> The body. </value>
            Public Property Body() As String

            ''' <summary> Gets or sets the author. </summary>
            ''' <value> The author. </value>
            Public Property Author() As AuthorCE
        End Class

        ''' <summary> An author ce. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Class AuthorCE

            ''' <summary> Gets or sets the identifier. </summary>
            ''' <value> The identifier. </value>
            Public Property Id() As Integer

            ''' <summary> Gets or sets the name. </summary>
            ''' <value> The name. </value>
            Public Property Name() As String
        End Class

#End Region


    End Class

End Namespace