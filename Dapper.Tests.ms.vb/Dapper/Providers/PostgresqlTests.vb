﻿Imports System
Imports System.Data
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Namespace DapperTests
	Public Class PostcresqlTests
		Inherits TestBase

		Private Shared Function GetOpenNpgsqlConnection() As Npgsql.NpgsqlConnection
			Dim cs As String = If(IsAppVeyor, "Server=localhost;Port=5432;User Id=postgres;Password=Password12!;Database=test", "Server=localhost;Port=5432;User Id=dappertest;Password=dapperpass;Database=dappertest") ' ;Encoding = UNICODE
			Dim conn = New Npgsql.NpgsqlConnection(cs)
			conn.Open()
			Return conn
		End Function

		Private Class Cat
			Public Property Id() As Integer
			Public Property Breed() As String
			Public Property Name() As String
		End Class

		Private ReadOnly Cats() As Cat = { New Cat() With {.Breed = "Abyssinian", .Name="KACTUS"}, New Cat() With {.Breed = "Aegean cat", .Name="KADAFFI"}, New Cat() With {.Breed = "American Bobtail", .Name="KANJI"}, New Cat() With {.Breed = "Balinese", .Name="MACARONI"}, New Cat() With {.Breed = "Bombay", .Name="MACAULAY"}, New Cat() With {.Breed = "Burmese", .Name="MACBETH"}, New Cat() With {.Breed = "Chartreux", .Name="MACGYVER"}, New Cat() With {.Breed = "German Rex", .Name="MACKENZIE"}, New Cat() With {.Breed = "Javanese", .Name="MADISON"}, New Cat() With {.Breed = "Persian", .Name="MAGNA"} }

		<FactPostgresql> _
		Public Sub TestPostgresqlArrayParameters()
			Using conn = GetOpenNpgsqlConnection()
				Dim transaction As IDbTransaction = conn.BeginTransaction()
				conn.Execute("create table tcat ( id serial not null, breed character varying(20) not null, name character varying (20) not null);")
				conn.Execute("insert into tcat(breed, name) values(:breed, :name) ", Cats)

				Dim r = conn.Query(Of Cat)("select * from tcat where id=any(:catids)", New With {Key .catids = { 1, 3, 5 }})
				Assert.AreEqual(3, r.Count())
				Assert.AreEqual(1, r.Count(Function(c) c.Id = 1))
				Assert.AreEqual(1, r.Count(Function(c) c.Id = 3))
				Assert.AreEqual(1, r.Count(Function(c) c.Id = 5))
				transaction.Rollback()
			End Using
		End Sub

		<AttributeUsage(AttributeTargets.Method, AllowMultiple := False)> _
		Public Class FactPostgresqlAttribute
			Inherits TestMethodAttribute

			Public Overrides Property Skip() As String
				Get
					Return If(unavailable, MyBase.Skip)
				End Get
				Set(ByVal value As String)
					MyBase.Skip = value
				End Set
			End Property

			Private Shared ReadOnly unavailable As String

			Shared Sub New()
				Try
					Using GetOpenNpgsqlConnection() ' just trying to see if it works
					End Using
				Catch ex As Exception
					unavailable = $"Postgresql is unavailable: {ex.Message}"
				End Try
			End Sub
		End Class
	End Class
End Namespace