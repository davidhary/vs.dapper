﻿Option Strict Off
Imports System
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper
Namespace DapperTests
	Public Class TupleTests
		Inherits TestBase

        <TestMethod>
        Public Sub TupleStructParameter_Fails_HelpfulMessage()
            Dim ex = MyAssert.Throws(Of NotSupportedException)(Function() Connection.QuerySingle(Of Integer)("select @id", New Tuple(Of Integer, String)(42, "Fred")))
            Assert.AreEqual("ValueTuple should not be used for parameters - the language-level names are not available to use as parameter names, and it adds unnecessary boxing", ex.Message)
        End Sub

        <TestMethod> _
		Public Sub TupleClassParameter_Works()
			Assert.AreEqual(42, connection.QuerySingle(Of Integer)("select @Item1", Tuple.Create(42, "Fred")))
		End Sub

		<TestMethod> _
		Public Sub TupleReturnValue_Works_ByPosition()
            Dim val = Connection.QuerySingle(Of Tuple(Of Integer, String))("select 42, 'Fred'")
            Assert.AreEqual(42, val.id)
			Assert.AreEqual("Fred", val.name)
		End Sub

		<TestMethod> _
		Public Sub TupleReturnValue_TooManyColumns_Ignored()
            Dim val = Connection.QuerySingle(Of Tuple(Of Integer, String))("select 42, 'Fred', 123")
            Assert.AreEqual(42, val.id)
			Assert.AreEqual("Fred", val.name)
		End Sub

		<TestMethod> _
		Public Sub TupleReturnValue_TooFewColumns_Unmapped()
            ' I'm very wary of making this throw, but I can also see some sense in pointing out the oddness
            Dim val = Connection.QuerySingle(Of Tuple(Of Integer, String, Integer))("select 42, 'Fred'")
            Assert.AreEqual(42, val.id)
			Assert.AreEqual("Fred", val.name)
			Assert.AreEqual(0, val.extra)
		End Sub

		<TestMethod> _
		Public Sub TupleReturnValue_Works_NamesIgnored()
            Dim val = Connection.QuerySingle(Of Tuple(Of Integer, String))("select 42 as [Item2], 'Fred' as [Item1]")
            Assert.AreEqual(42, val.id)
			Assert.AreEqual("Fred", val.name)
		End Sub
	End Class
End Namespace
