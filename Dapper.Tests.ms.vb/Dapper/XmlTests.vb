﻿Imports System.Xml
Imports System.Xml.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper

Namespace DapperTests
	Public Class XmlTests
		Inherits TestBase

		<TestMethod> _
		Public Sub CommonXmlTypesSupported()
            Dim xml As XmlDocument = New XmlDocument()
            xml.LoadXml("<abc/>")

            Dim foo As New Foo With {.A = xml, .B = XDocument.Parse("<def/>"), .C = XElement.Parse("<ghi/>")}
#Disable Warning IDE0037 ' Use inferred member name
            Dim bar As Foo = Me.Connection.QuerySingle(Of Foo)("select @a as [A], @b as [B], @c as [C]", New With {Key .a = foo.A, Key .b = foo.B, Key .c = foo.C})
#Enable Warning IDE0037 ' Use inferred member name
            Assert.AreEqual("abc", bar.A.DocumentElement.Name)
            Assert.AreEqual("def", bar.B.Root.Name.LocalName)
			Assert.AreEqual("ghi", bar.C.Name.LocalName)
		End Sub

		Public Class Foo
			Public Property A() As XmlDocument
			Public Property B() As XDocument
			Public Property C() As XElement
		End Class
	End Class
End Namespace
