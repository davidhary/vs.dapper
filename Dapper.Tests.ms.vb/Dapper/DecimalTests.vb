﻿Imports System
Imports System.Data
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper
Namespace DapperTests
    Public Class DecimalTests
        Inherits TestBase

        <TestMethod>
        Public Sub Issue261_Decimals()
            Dim parameters As New DynamicParameters()
            parameters.Add("c", dbType:=DbType.Decimal, direction:=ParameterDirection.Output, precision:=10, scale:=5)
            connection.Execute("create proc #Issue261 @c decimal(10,5) OUTPUT as begin set @c=11.884 end")
            connection.Execute("#Issue261", parameters, commandType:=CommandType.StoredProcedure)
            Dim c As Decimal = parameters.Get(Of Decimal)("c")
            Assert.AreEqual(11.884D, c)
        End Sub

        <TestMethod>
        Public Sub Issue261_Decimals_ADONET_SetViaBaseClassSub()
            Issue261_Decimals_ADONET(True)
        End Sub

        <TestMethod>
        Public Sub Issue261_Decimals_ADONET_SetViaConcreteClassSub()
            Issue261_Decimals_ADONET(False)
        End Sub

        Private Sub Issue261_Decimals_ADONET(setPrecisionScaleViaAbstractApi As Boolean)
            Try
                Using cmd As SqlClient.SqlCommand = Me.Connection.CreateCommand()
                    cmd.CommandText = "create proc #Issue261Direct @c decimal(10,5) OUTPUT as begin set @c=11.884 end"
                    cmd.ExecuteNonQuery()
                End Using
            Catch ' we don't care that it already exists
            End Try

            Using cmd As SqlClient.SqlCommand = Me.Connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "#Issue261Direct"
                Dim c As SqlClient.SqlParameter = cmd.CreateParameter()
                c.ParameterName = "c"
                c.Direction = ParameterDirection.Output
                c.Value = DBNull.Value
                c.DbType = DbType.Decimal

                If setPrecisionScaleViaAbstractApi Then
                    Dim baseParam As IDbDataParameter = c
                    baseParam.Precision = 10
                    baseParam.Scale = 5
                Else
                    c.Precision = 10
                    c.Scale = 5
                End If

                cmd.Parameters.Add(c)
                cmd.ExecuteNonQuery()
                Dim value As Decimal = CDec(c.Value)
                Assert.AreEqual(11.884D, value)
            End Using
        End Sub

        <TestMethod>
        Public Sub BasicDecimals()
            Dim c As Decimal = Me.Connection.Query(Of Decimal)("select @c", New With {Key .c = 11.884D}).Single()
            Assert.AreEqual(11.884D, c)
        End Sub

        <TestMethod>
        Public Sub TestDoubleDecimalConversions_SO18228523_RightWay()
            Dim row As HasDoubleDecimal = Me.Connection.Query(Of HasDoubleDecimal)("select cast(1 as float) as A, cast(2 as float) as B, cast(3 as decimal) as C, cast(4 as decimal) as D").Single()
            Assert.AreEqual(1.0, row.A)
            Assert.AreEqual(2.0, row.B)
            Assert.AreEqual(3D, row.C)
            Assert.AreEqual(4D, row.D)
        End Sub

        <TestMethod>
        Public Sub TestDoubleDecimalConversions_SO18228523_WrongWay()
            Dim row As HasDoubleDecimal = Me.Connection.Query(Of HasDoubleDecimal)("select cast(1 as decimal) as A, cast(2 as decimal) as B, cast(3 as float) as C, cast(4 as float) as D").Single()
            Assert.AreEqual(1.0, row.A)
            Assert.AreEqual(2.0, row.B)
            Assert.AreEqual(3D, row.C)
            Assert.AreEqual(4D, row.D)
        End Sub

        <TestMethod>
        Public Sub TestDoubleDecimalConversions_SO18228523_Nulls()
            Dim row As HasDoubleDecimal = Me.Connection.Query(Of HasDoubleDecimal)("select cast(null as decimal) as A, cast(null as decimal) as B, cast(null as float) as C, cast(null as float) as D").Single()
            Assert.AreEqual(0.0, row.A)
            Assert.IsNull(row.B)
            Assert.AreEqual(0.0D, row.C)
            Assert.IsNull(row.D)
        End Sub

        Private Class HasDoubleDecimal
            Public Property A As Double
            Public Property B As Double?
            Public Property C As Decimal
            Public Property D As Decimal?
        End Class
    End Class

End Namespace
