﻿Option Infer Off
Option Strict Off
Imports System
Imports System.Data
Imports System.Linq
Imports System.Threading.Tasks
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper

Namespace DapperTests
	Public Class ProcedureTests
		Inherits TestBase

		<TestMethod> _
		Public Sub TestProcWithOutParameter()
            Me.Connection.Execute("CREATE PROCEDURE #TestProcWithOutParameter" & ControlChars.CrLf & "                @ID int output," & ControlChars.CrLf & "                @Foo varchar(100)," & ControlChars.CrLf & "                @Bar int" & ControlChars.CrLf & "            AS" & ControlChars.CrLf & "                SET @ID = @Bar + LEN(@Foo)")
            Dim obj = New With {Key .ID = 0, Key .Foo = "abc", Key .Bar = 4}
			Dim args = New DynamicParameters(obj)
			args.Add("ID", 0, direction:= ParameterDirection.Output)
			connection.Execute("#TestProcWithOutParameter", args, commandType:= CommandType.StoredProcedure)
			Assert.AreEqual(7, args.Get(Of Integer)("ID"))
		End Sub

		<TestMethod> _
		Public Sub TestProcWithOutAndReturnParameter()
			connection.Execute("CREATE PROCEDURE #TestProcWithOutAndReturnParameter" & ControlChars.CrLf & "                @ID int output," & ControlChars.CrLf & "                @Foo varchar(100)," & ControlChars.CrLf & "                @Bar int" & ControlChars.CrLf & "            AS" & ControlChars.CrLf & "                SET @ID = @Bar + LEN(@Foo)" & ControlChars.CrLf & "                RETURN 42")
			Dim obj = New With {Key .ID = 0, Key .Foo = "abc", Key .Bar = 4}
			Dim args = New DynamicParameters(obj)
			args.Add("ID", 0, direction:= ParameterDirection.Output)
			args.Add("result", 0, direction:= ParameterDirection.ReturnValue)
			connection.Execute("#TestProcWithOutAndReturnParameter", args, commandType:= CommandType.StoredProcedure)
			Assert.AreEqual(7, args.Get(Of Integer)("ID"))
			Assert.AreEqual(42, args.Get(Of Integer)("result"))
		End Sub

		<TestMethod> _
		Public Sub TestIssue17648290()
			Dim p = New DynamicParameters()
			Const code As Integer = 1, getMessageControlId As Integer = 2
			p.Add("@Code", code)
			p.Add("@MessageControlID", getMessageControlId)
			p.Add("@SuccessCode", dbType:= DbType.Int32, direction:= ParameterDirection.Output)
			p.Add("@ErrorDescription", dbType:= DbType.String, direction:= ParameterDirection.Output, size:= 255)
			connection.Execute("CREATE PROCEDURE #up_MessageProcessed_get" & ControlChars.CrLf & "                @Code varchar(10)," & ControlChars.CrLf & "                @MessageControlID varchar(22)," & ControlChars.CrLf & "                @SuccessCode int OUTPUT," & ControlChars.CrLf & "                @ErrorDescription varchar(255) OUTPUT" & ControlChars.CrLf & "            AS" & ControlChars.CrLf & "            BEGIN" & ControlChars.CrLf & "                Select 2 as MessageProcessID, 38349348 as StartNum, 3874900 as EndNum, GETDATE() as StartDate, GETDATE() as EndDate" & ControlChars.CrLf & "                SET @SuccessCode = 0" & ControlChars.CrLf & "                SET @ErrorDescription = 'Completed successfully'" & ControlChars.CrLf & "            END")
			Dim result = connection.Query(sql:= "#up_MessageProcessed_get", param:= p, commandType:= CommandType.StoredProcedure)
			Dim row = result.Single()
			Assert.AreEqual(2, CInt(Fix(row.MessageProcessID)))
			Assert.AreEqual(38349348, CInt(Fix(row.StartNum)))
			Assert.AreEqual(3874900, CInt(Fix(row.EndNum)))
            Dim startDate As DateTime = row.StartDate, endDate As DateTime = row.EndDate
            Assert.AreEqual(0, p.Get(Of Integer)("SuccessCode"))
			Assert.AreEqual("Completed successfully", p.Get(Of String)("ErrorDescription"))
		End Sub

		<TestMethod> _
		Public Sub SO24605346_ProcsAndStrings()
			connection.Execute("create proc #GetPracticeRebateOrderByInvoiceNumber " & ControlChars.CrLf & "                @TaxInvoiceNumber nvarchar(20) " & ControlChars.CrLf & "            as" & ControlChars.CrLf & "                select @TaxInvoiceNumber as [fTaxInvoiceNumber]")
			Const InvoiceNumber As String = "INV0000000028PPN"
			Dim result = connection.Query(Of PracticeRebateOrders)("#GetPracticeRebateOrderByInvoiceNumber", New With {Key .TaxInvoiceNumber = InvoiceNumber}, commandType:= CommandType.StoredProcedure).FirstOrDefault()

			Assert.AreEqual("INV0000000028PPN", result.TaxInvoiceNumber)
		End Sub

		Private Class PracticeRebateOrders
			Public fTaxInvoiceNumber As String
#If Not NETCOREAPP1_0 Then
			<System.Xml.Serialization.XmlElement(Form := System.Xml.Schema.XmlSchemaForm.Unqualified)> _
			Public Property TaxInvoiceNumber() As String
#Else
			Public Property TaxInvoiceNumber() As String
#End If
				Get
					Return fTaxInvoiceNumber
				End Get
				Set(ByVal value As String)
					fTaxInvoiceNumber = value
				End Set
			End Property
		End Class

		<TestMethod> _
		Public Sub Issue327_ReadEmptyProcedureResults()
			' Actually testing for not erroring here on the mapping having no rows to map on in Read<T>();
			connection.Execute("" & ControlChars.CrLf & "            CREATE PROCEDURE #TestEmptyResults" & ControlChars.CrLf & "            AS" & ControlChars.CrLf & "                SELECT Top 0 1 Id, 'Bob' Name;" & ControlChars.CrLf & "                SELECT Top 0 'Billy Goat' Creature, 'Unicorn' SpiritAnimal, 'Rainbow' Location;")
			Dim query = connection.QueryMultiple("#TestEmptyResults", commandType:= CommandType.StoredProcedure)
			Dim result1 = query.Read(Of Issue327_Person)()
			Dim result2 = query.Read(Of Issue327_Magic)()
			Assert.IsFalse(result1.Any())
			Assert.IsFalse(result2.Any())
		End Sub

		Private Class Issue327_Person
			Public Property Id() As Integer
			Public Property Name() As String
		End Class

		Private Class Issue327_Magic
			Public Property Creature() As String
			Public Property SpiritAnimal() As String
			Public Property Location() As String
		End Class

		<TestMethod> _
		Public Sub TestProcSupport()
			Dim p = New DynamicParameters()
			p.Add("a", 11)
			p.Add("b", dbType:= DbType.Int32, direction:= ParameterDirection.Output)
			p.Add("c", dbType:= DbType.Int32, direction:= ParameterDirection.ReturnValue)

			connection.Execute("" & ControlChars.CrLf & "            create proc #TestProc " & ControlChars.CrLf & "	            @a int," & ControlChars.CrLf & "	            @b int output" & ControlChars.CrLf & "            as " & ControlChars.CrLf & "            begin" & ControlChars.CrLf & "	            set @b = 999" & ControlChars.CrLf & "	            select 1111" & ControlChars.CrLf & "	            return @a" & ControlChars.CrLf & "            end")
			Assert.AreEqual(1111, connection.Query(Of Integer)("#TestProc", p, commandType:= CommandType.StoredProcedure).First())

			Assert.AreEqual(11, p.Get(Of Integer)("c"))
			Assert.AreEqual(999, p.Get(Of Integer)("b"))
		End Sub

		' https://stackoverflow.com/q/8593871
		<TestMethod> _
		Public Sub TestListOfAnsiStrings()
			Dim results = connection.Query(Of String)("select * from (select 'a' str union select 'b' union select 'c') X where str in @strings", New With {Key .strings = { New DbString With {.IsAnsi = True, .Value = "a"}, New DbString With {.IsAnsi = True, .Value = "b"} }}).ToList()

			Assert.AreEqual(2, results.Count)
			results.Sort()
			Assert.AreEqual("a", results(0))
			Assert.AreEqual("b", results(1))
		End Sub

		<TestMethod> _
		Public Sub TestDateTime2PrecisionPreservedInDynamicParameters()
            Const tempSPName As String = "#" & NameOf(TestDateTime2PrecisionPreservedInDynamicParameters)

            Dim datetimeDefault As New Date(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc)
            Dim datetime2 As DateTime = datetimeDefault.AddTicks(1) ' Add 100 ns

            Assert.IsTrue(datetimeDefault < datetime2)

            Me.Connection.Execute($"create proc {tempSPName} " & ControlChars.CrLf & "	            @a datetime2," & ControlChars.CrLf & "	            @b datetime2 output" & ControlChars.CrLf & "            as " & ControlChars.CrLf & "            begin" & ControlChars.CrLf & "	            set @b = @a" & ControlChars.CrLf & "	            select DATEADD(ns, -100, @b)" & ControlChars.CrLf & "            end")

            Dim p = New DynamicParameters()
			' Note: parameters declared as DateTime2
			p.Add("a", datetime2, dbType:= DbType.DateTime2, direction:= ParameterDirection.Input)
			p.Add("b", dbType:= DbType.DateTime2, direction:= ParameterDirection.Output)

            Dim fromSelect As DateTime = connection.Query(Of Date)(tempSPName, p, commandType:=CommandType.StoredProcedure).First()

            Assert.AreEqual(datetimeDefault, fromSelect)

			Assert.AreEqual(datetime2, p.Get(Of Date)("b"))
		End Sub

        ' <Theory, InlineData(Nothing), InlineData(DbType.DateTime)>

        Public Sub TestDateTime2LosePrecisionInDynamicParameters(ByVal dbType? As DbType)
            Const tempSPName As String = "#" & NameOf(TestDateTime2LosePrecisionInDynamicParameters)

            Dim datetimeDefault As New Date(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc)
            Dim datetime2 As DateTime = datetimeDefault.AddTicks(1) ' Add 100 ns

            Assert.IsTrue(datetimeDefault < datetime2)

            Me.Connection.Execute($"create proc {tempSPName}" & ControlChars.CrLf & "	            @a datetime2," & ControlChars.CrLf & "	            @b datetime2 output" & ControlChars.CrLf & "            as " & ControlChars.CrLf & "            begin" & ControlChars.CrLf & "	            set @b = DATEADD(ns, 100, @a)" & ControlChars.CrLf & "	            select @b" & ControlChars.CrLf & "            end")

            Dim p = New DynamicParameters()
			' Note: input parameter declared as DateTime (or implicitly as this) but SP has DateTime2
			p.Add("a", datetime2, dbType:= dbType, direction:= ParameterDirection.Input)
            p.Add("b", dbType:=dbType, direction:=ParameterDirection.Output)

            Dim fromSelect As DateTime = connection.Query(Of Date)(tempSPName, p, commandType:=CommandType.StoredProcedure).First()

            ' @a truncates to datetimeDefault when passed into SP by DynamicParameters, add 100ns and it comes out as DateTime2
            Assert.AreEqual(datetime2, fromSelect)

			' @b gets set to datetime2 value but is truncated back to DbType.DateTime by DynamicParameter's Output declaration
			Assert.AreEqual(datetimeDefault, p.Get(Of Date)("b"))
		End Sub


		<TestMethod> _
		Public Async Function Issue591_NoResultsAsync() As Task
            Const tempSPName As String = "#" & NameOf(ProcedureTests.Issue591_NoResultsAsync)
            Dim result = Await Connection.QueryAsync($"create proc {tempSPName}" & ControlChars.CrLf & "            as " & ControlChars.CrLf & "            begin" & ControlChars.CrLf & "                -- basically a failed if statement, so the select is not happening and the stored proc return nothing" & ControlChars.CrLf & "                if 1=0" & ControlChars.CrLf & "                begin" & ControlChars.CrLf & "                    select 1 as Num" & ControlChars.CrLf & "                end" & ControlChars.CrLf & "            end" & ControlChars.CrLf & "            " & ControlChars.CrLf & "            exec {tempSPName}")
            Assert.IsTrue(result Is Nothing)
        End Function
	End Class
End Namespace
