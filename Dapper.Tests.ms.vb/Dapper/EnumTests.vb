﻿Imports System.Data
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper
Namespace DapperTests
    Public Class EnumTests
        Inherits TestBase

        <TestMethod>
        Public Sub TestEnumWeirdness()
            Assert.IsNull(Me.Connection.Query(Of TestEnumClass)("select null as [EnumEnum]").First().EnumEnum)
            Assert.AreEqual(TestEnum.Bla, Me.Connection.Query(Of TestEnumClass)("select cast(1 as tinyint) as [EnumEnum]").First().EnumEnum)
        End Sub

        <TestMethod>
        Public Sub TestEnumStrings()
            Assert.AreEqual(TestEnum.Bla, Me.Connection.Query(Of TestEnumClassNoNull)("select 'BLA' as [EnumEnum]").First().EnumEnum)
            Assert.AreEqual(TestEnum.Bla, Me.Connection.Query(Of TestEnumClassNoNull)("select 'bla' as [EnumEnum]").First().EnumEnum)

            Assert.AreEqual(TestEnum.Bla, Me.Connection.Query(Of TestEnumClass)("select 'BLA' as [EnumEnum]").First().EnumEnum)
            Assert.AreEqual(TestEnum.Bla, Me.Connection.Query(Of TestEnumClass)("select 'bla' as [EnumEnum]").First().EnumEnum)
        End Sub

        <TestMethod>
        Public Sub TestEnumParamsWithNullable()
            Const a As EnumParam = EnumParam.A
            Dim b? As EnumParam = EnumParam.B, c? As EnumParam = Nothing
            Dim obj As EnumParamObject = Me.Connection.Query(Of EnumParamObject)("select @a as A, @b as B, @c as C", New With {Key a, Key b, Key c}).Single()
            Assert.AreEqual(EnumParam.A, obj.A)
            Assert.AreEqual(EnumParam.B, obj.B)
            Assert.IsNull(obj.C)
        End Sub

        <TestMethod>
        Public Sub TestEnumParamsWithoutNullable()
            Const a As EnumParam = EnumParam.A
            Const b As EnumParam = EnumParam.B, c As EnumParam = 0
            Dim obj As EnumParamObjectNonNullable = Me.Connection.Query(Of EnumParamObjectNonNullable)("select @a as A, @b as B, @c as C", New With {Key a, Key b, Key c}).Single()
            Assert.AreEqual(EnumParam.A, obj.A)
            Assert.AreEqual(EnumParam.B, obj.B)
            Assert.AreEqual(obj.C, CType(0, EnumParam))
        End Sub

        Private Enum EnumParam As Short
            None = 0
            A = 1
            B = 2
        End Enum

        Private Class EnumParamObject
            Public Property A() As EnumParam
            Public Property B() As EnumParam?
            Public Property C() As EnumParam?
        End Class

        Private Class EnumParamObjectNonNullable
            Public Property A() As EnumParam
            Public Property B() As EnumParam?
            Public Property C() As EnumParam?
        End Class

        Private Enum TestEnum As Byte
            Bla = 1
        End Enum

        Private Class TestEnumClass
            Public Property EnumEnum() As TestEnum?
        End Class

        Private Class TestEnumClassNoNull
            Public Property EnumEnum() As TestEnum
        End Class

        <TestMethod>
        Public Sub AdoNetEnumValue()
            Using cmd As SqlClient.SqlCommand = Me.Connection.CreateCommand()
                cmd.CommandText = "select @foo"
                Dim p As SqlClient.SqlParameter = cmd.CreateParameter()
                p.ParameterName = "@foo"
                p.DbType = DbType.Int32 ' it turns out that this is the key piece; setting the DbType
                p.Value = AnEnum.B
                cmd.Parameters.Add(p)
                Dim value As Object = cmd.ExecuteScalar()
                Dim val As AnEnum = CType(value, AnEnum)
                Assert.AreEqual(AnEnum.B, val)
            End Using
        End Sub

        <TestMethod>
        Public Sub DapperEnumValue_SqlServerFunction()
            Common.DapperEnumValue(Connection)
        End Sub

        Private Enum SO27024806Enum
            Foo = 0
            Bar = 1
        End Enum

        <TestMethod>
        Public Sub SO27024806_TestVarcharEnumMemberWithExplicitConstructor()
            Dim foo As SO27024806Class = Me.Connection.Query(Of SO27024806Class)("SELECT 'Foo' AS myField").Single()
            Assert.AreEqual(SO27024806Enum.Foo, foo.MyField)
        End Sub

        Private Class SO27024806Class
            Public Sub New(myField As SO27024806Enum)
                myField = myField
            End Sub
            Public Property MyField As SO27024806Enum
        End Class

    End Class

End Namespace
