﻿Imports System
Imports System.Data
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper
Namespace DapperTests
	Public Class ConstructorTests
		Inherits TestBase

		<TestMethod> _
		Public Sub TestAbstractInheritance()
            Dim order As AbstractInheritance.ConcreteOrder = Me.Connection.Query(Of AbstractInheritance.ConcreteOrder)("select 1 Internal,2 Protected,3 [Public],4 Concrete").First()
            Assert.AreEqual(1, order.Internal)
            Assert.AreEqual(2, order.ProtectedVal)
			Assert.AreEqual(3, order.Public)
			Assert.AreEqual(4, order.Concrete)
		End Sub

		<TestMethod> _
		Public Sub TestMultipleConstructors()
            Dim mult As MultipleConstructors = Me.Connection.Query(Of MultipleConstructors)("select 0 A, 'Dapper' b").First()
            Assert.AreEqual(0, mult.A)
            Assert.AreEqual("Dapper", mult.B)
        End Sub

        <TestMethod>
        Public Sub TestConstructorsWithAccessModifiers()
            Dim value As ConstructorsWithAccessModifiers = Me.Connection.Query(Of ConstructorsWithAccessModifiers)("select 0 A, 'Dapper' b").First()
            Assert.AreEqual(1, value.A)
            Assert.AreEqual("Dapper!", value.B)
        End Sub

        <TestMethod>
        Public Sub TestNoDefaultConstructor()
            Dim guid As Guid = System.Guid.NewGuid()
            Dim nodef As NoDefaultConstructor = Me.Connection.Query(Of NoDefaultConstructor)("select CAST(NULL AS integer) A1,  CAST(NULL AS integer) b1, CAST(NULL AS real) f1, 'Dapper' s1, G1 = @id", New With {Key .id = guid}).First()
            Assert.AreEqual(0, nodef.A)
            Assert.IsNull(nodef.B)
            Assert.AreEqual(0, nodef.F)
            Assert.AreEqual("Dapper", nodef.S)
            Assert.AreEqual(nodef.G, guid)
        End Sub

        <TestMethod>
        Public Sub TestNoDefaultConstructorWithChar()
            Const c1 As Char = "ą"c
            Const c3 As Char = "ó"c
#Disable Warning IDE0037 ' Use inferred member name
            Dim nodef As NoDefaultConstructorWithChar = Me.Connection.Query(Of NoDefaultConstructorWithChar)("select @c1 c1, @c2 c2, @c3 c3", New With {Key .c1 = c1, Key .c2 = CType(Nothing, Char?), Key .c3 = c3}).First()
#Enable Warning IDE0037 ' Use inferred member name
            Assert.AreEqual(nodef.Char1, c1)
            Assert.IsNull(nodef.Char2)
            Assert.AreEqual(nodef.Char3, c3)
        End Sub

        <TestMethod>
        Public Sub TestNoDefaultConstructorWithEnum()
            Dim nodef As NoDefaultConstructorWithEnum = Me.Connection.Query(Of NoDefaultConstructorWithEnum)("select cast(2 as smallint) E1, cast(5 as smallint) n1, cast(null as smallint) n2").First()
            Assert.AreEqual(ShortEnum.Two, nodef.E)
            Assert.AreEqual(ShortEnum.Five, nodef.NE1)
            Assert.IsNull(nodef.NE2)
        End Sub

        <TestMethod>
        Public Sub ExplicitConstructors()
            Dim rows As IEnumerable(Of _ExplicitConstructors) = Me.Connection.Query(Of _ExplicitConstructors)("" & ControlChars.CrLf & "declare @ExplicitConstructors table (" & ControlChars.CrLf & "    Field INT NOT NULL PRIMARY KEY IDENTITY(1,1)," & ControlChars.CrLf & "    Field_1 INT NOT NULL);" & ControlChars.CrLf & "insert @ExplicitConstructors(Field_1) values (1);" & ControlChars.CrLf & "SELECT * FROM @ExplicitConstructors").ToList()
            Assert.AreEqual(1, rows.Count)
            Assert.AreEqual(1, rows(0).Field)
            Assert.AreEqual(1, rows(0).Field_1)
            Assert.IsTrue(rows(0).GetWentThroughProperConstructor())
        End Sub

        Private Class _ExplicitConstructors
            Public Property Field() As Integer
            Public Property Field_1() As Integer

            Private ReadOnly WentThroughProperConstructor As Boolean

            Public Sub New() ' yep
            End Sub

            <ExplicitConstructor>
            Public Sub New(ByVal foo As String, ByVal bar As Integer)
                WentThroughProperConstructor = True
            End Sub

            Public Function GetWentThroughProperConstructor() As Boolean
                Return WentThroughProperConstructor
            End Function
        End Class

        Public NotInheritable Class AbstractInheritance
            Public MustInherit Class Order
                Friend Property Internal() As Integer
                Protected Property [Protected]() As Integer
                Public Property [Public]() As Integer
                Public Function ProtectedVal() As Integer
                    Return [Protected]
                End Function
            End Class
            Private Sub New()
            End Sub
            Public Class ConcreteOrder
                Inherits Order
                Public Property Concrete() As Integer
            End Class
        End Class

        Private Class MultipleConstructors
            Public Sub New()
            End Sub

            Public Sub New(ByVal a As Integer, ByVal b As String)
                Me.A = a + 1
                Me.B = b & "!"
            End Sub

            Public Property A() As Integer
            Public Property B() As String
        End Class

        Private Class ConstructorsWithAccessModifiers
            Private Sub New()
            End Sub

            Public Sub New(ByVal a As Integer, ByVal b As String)
                Me.A = a + 1
                Me.B = b & "!"
            End Sub

            Public Property A() As Integer
            Public Property B() As String
        End Class

        Private Class NoDefaultConstructor
            Public Sub New(ByVal a1 As Integer, ByVal b1? As Integer, ByVal f1 As Single, ByVal s1 As String, ByVal G1 As Guid)
                A = a1
                B = b1
                F = f1
                S = s1
                G = G1
            End Sub

            Public Property A() As Integer
            Public Property B() As Integer?
            Public Property F() As Single
            Public Property S() As String
            Public Property G() As Guid
        End Class

        Private Class NoDefaultConstructorWithChar
            Public Sub New(ByVal c1 As Char, ByVal c2? As Char, ByVal c3? As Char)
                Char1 = c1
                Char2 = c2
                Char3 = c3
            End Sub

            Public Property Char1() As Char
            Public Property Char2() As Char?
            Public Property Char3() As Char?
        End Class

        Private Class NoDefaultConstructorWithEnum
            Public Sub New(ByVal e1 As ShortEnum, ByVal n1? As ShortEnum, ByVal n2? As ShortEnum)
                E = e1
                NE1 = n1
                NE2 = n2
            End Sub

            Public Property E() As ShortEnum
            Public Property NE1() As ShortEnum?
            Public Property NE2() As ShortEnum?
        End Class

        Private Class WithPrivateConstructor
            Public Property Foo() As Integer
            Private Sub New()
            End Sub
        End Class

        <TestMethod>
        Public Sub TestWithNonPublicConstructor()
            Dim output As WithPrivateConstructor = Me.Connection.Query(Of WithPrivateConstructor)("select 1 as Foo").First()
            Assert.AreEqual(1, output.Foo)
		End Sub
	End Class
End Namespace
