Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> some type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class SomeType

        ''' <summary> Gets or sets a. </summary>
        ''' <value> a. </value>
        Public Property A() As Integer

        ''' <summary> Gets or sets the b. </summary>
        ''' <value> The b. </value>
        Public Property B() As String
    End Class
End Namespace
