Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> A haz name identifier. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class HazNameId

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer
    End Class
End Namespace
