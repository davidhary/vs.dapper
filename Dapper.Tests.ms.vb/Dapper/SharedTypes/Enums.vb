﻿Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> Values that represent the enums. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Friend Enum AnEnum

        ''' <summary> An enum constant representing a option. </summary>
        A = 2

        ''' <summary> An enum constant representing the b option. </summary>
        B = 1
    End Enum

    ''' <summary> Values that represent another enums. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Friend Enum AnotherEnum As Byte

        ''' <summary> An enum constant representing a option. </summary>
        A = 2

        ''' <summary> An enum constant representing the b option. </summary>
        B = 1
    End Enum
End Namespace
