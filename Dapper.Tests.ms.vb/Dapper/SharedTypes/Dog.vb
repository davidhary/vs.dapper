﻿Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> A dog. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Dog

        ''' <summary> Gets or sets the age. </summary>
        ''' <value> The age. </value>
        Public Property Age() As Integer?

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Guid

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets or sets the weight. </summary>
        ''' <value> The weight. </value>
        Public Property Weight() As Single?

        ''' <summary> Ignored property. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> An Integer. </returns>
        <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
        Public Function IgnoredProperty() As Integer
            Return 1
        End Function
    End Class
End Namespace
