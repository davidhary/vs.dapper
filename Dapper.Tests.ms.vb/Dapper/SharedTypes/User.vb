﻿Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> An user. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class User

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String
    End Class
End Namespace
