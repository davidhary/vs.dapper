﻿Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> A post. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Post

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer

        ''' <summary> Gets or sets the owner. </summary>
        ''' <value> The owner. </value>
        Public Property Owner() As User

        ''' <summary> Gets or sets the content. </summary>
        ''' <value> The content. </value>
        Public Property Content() As String

        ''' <summary> Gets or sets the comment. </summary>
        ''' <value> The comment. </value>
        Public Property Comment() As Comment
    End Class
End Namespace
