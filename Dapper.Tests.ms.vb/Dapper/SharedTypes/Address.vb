﻿Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> An address. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Address

        ''' <summary> Gets or sets the identifier of the address. </summary>
        ''' <value> The identifier of the address. </value>
        Public Property AddressId() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets or sets the identifier of the person. </summary>
        ''' <value> The identifier of the person. </value>
        Public Property PersonId() As Integer
    End Class
End Namespace
