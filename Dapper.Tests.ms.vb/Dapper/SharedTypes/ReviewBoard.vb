﻿Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> A review board. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class ReviewBoard

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets or sets the user 1. </summary>
        ''' <value> The user 1. </value>
        Public Property User1() As User

        ''' <summary> Gets or sets the user 2. </summary>
        ''' <value> The user 2. </value>
        Public Property User2() As User

        ''' <summary> Gets or sets the user 3. </summary>
        ''' <value> The user 3. </value>
        Public Property User3() As User

        ''' <summary> Gets or sets the user 4. </summary>
        ''' <value> The user 4. </value>
        Public Property User4() As User

        ''' <summary> Gets or sets the user 5. </summary>
        ''' <value> The user 5. </value>
        Public Property User5() As User

        ''' <summary> Gets or sets the user 6. </summary>
        ''' <value> The user 6. </value>
        Public Property User6() As User

        ''' <summary> Gets or sets the user 7. </summary>
        ''' <value> The user 7. </value>
        Public Property User7() As User

        ''' <summary> Gets or sets the user 8. </summary>
        ''' <value> The user 8. </value>
        Public Property User8() As User

        ''' <summary> Gets or sets the user 9. </summary>
        ''' <value> The user 9. </value>
        Public Property User9() As User
    End Class
End Namespace
