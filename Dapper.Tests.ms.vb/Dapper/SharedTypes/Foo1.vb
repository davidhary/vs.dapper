Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> A foo 1. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Foo1

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id As Integer

        ''' <summary> Gets or sets the identifier of the bar. </summary>
        ''' <value> The identifier of the bar. </value>
        Public Property BarId() As Integer
    End Class
End Namespace
