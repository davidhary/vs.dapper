﻿Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> A product. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Product

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets or sets the category. </summary>
        ''' <value> The category. </value>
        Public Property Category() As Category
    End Class
End Namespace
