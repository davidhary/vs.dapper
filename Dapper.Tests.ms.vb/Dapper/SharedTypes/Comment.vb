﻿Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> A comment. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Comment

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer

        ''' <summary> Gets or sets information describing the comment. </summary>
        ''' <value> Information describing the comment. </value>
        Public Property CommentData() As String
    End Class
End Namespace
