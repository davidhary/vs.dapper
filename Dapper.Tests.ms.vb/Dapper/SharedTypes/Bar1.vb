Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> A bar 1. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Bar1

        ''' <summary> Gets or sets the identifier of the bar. </summary>
        ''' <value> The identifier of the bar. </value>
        Public Property BarId As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String
    End Class
End Namespace
