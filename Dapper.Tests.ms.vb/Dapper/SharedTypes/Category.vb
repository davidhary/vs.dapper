﻿Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> A category. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Category

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets or sets the description. </summary>
        ''' <value> The description. </value>
        Public Property Description() As String
    End Class
End Namespace
