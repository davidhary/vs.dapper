Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> A person. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Person

        ''' <summary> Gets or sets the identifier of the person. </summary>
        ''' <value> The identifier of the person. </value>
        Public Property PersonId() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets or sets the occupation. </summary>
        ''' <value> The occupation. </value>
        Public Property Occupation() As String

        ''' <summary> Gets or sets the number of legs. </summary>
        ''' <value> The total number of legs. </value>
        Public Property NumberOfLegs As Integer = 2

        ''' <summary> Gets or sets the address. </summary>
        ''' <value> The address. </value>
        Public Property Address() As Address
    End Class
End Namespace
