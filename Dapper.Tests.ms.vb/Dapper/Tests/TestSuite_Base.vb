''' <summary> The test suite base class for Dapper Contrib functions. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/8/2018 </para>
''' </remarks>
<TestClass()>
Partial Public MustInherit Class TestBase

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        If TestBase.Skip Then Assert.Inconclusive()
        Assert.IsTrue(ConfigReader.Exists, $"{TypeOf ConfigReader}.{NameOf(ConfigReader)} settings should exist")
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo?.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets or sets the test context which provides information about and functionality for the
    ''' current test run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> Gets or sets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite

    #End Region

    #Region " MUST INHERIT "

    ''' <summary> Gets the connection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The connection. </returns>
    Public MustOverride Function GetConnection() As System.Data.IDbConnection

    ''' <summary> Gets closed connection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The closed connection. </returns>
    Protected Function GetClosedConnection() As System.Data.IDbConnection
        Return Me.GetConnection()
    End Function

    ''' <summary> Gets open connection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The open connection. </returns>
    Protected Function GetOpenConnection() As System.Data.IDbConnection
        Dim connection As System.Data.IDbConnection = Me.GetClosedConnection()
        connection.Open()
        Return connection
    End Function

    ''' <summary> Gets or sets the skip. </summary>
    ''' <value> The skip. </value>
    Public Shared ReadOnly Property Skip() As Boolean

    ''' <summary> The unavailable. </summary>
    Private Shared _Unavailable As String

    ''' <summary> Gets or sets the unavailable. </summary>
    ''' <value> The unavailable. </value>
    Public Shared Property Unavailable As String
        Get
            Return _Unavailable
        End Get
        Set(value As String)
            _Unavailable = value
            _Skip = Not String.IsNullOrEmpty(value)
        End Set
    End Property

    #End Region

End Class


