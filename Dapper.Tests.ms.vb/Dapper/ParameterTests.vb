﻿Option Infer On
Option Strict Off
Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports System.Dynamic
Imports System.Linq
Imports Dapper
Imports System.Globalization
Imports System.Text.RegularExpressions
Imports System.Diagnostics

Namespace DapperTests
    Public Class ParameterTests
        Inherits TestBase

        Public Class DbParams
            Implements IEnumerable(Of IDbDataParameter), SqlMapper.IDynamicParameters

            Private ReadOnly parameters As List(Of IDbDataParameter) = New List(Of IDbDataParameter)()

            Public Sub Add(ByVal value As IDbDataParameter)
                parameters.Add(value)
            End Sub

            Private Sub AddParameters(ByVal command As IDbCommand, ByVal identity As SqlMapper.Identity) Implements SqlMapper.IDynamicParameters.AddParameters
                For Each parameter As IDbDataParameter In parameters
                    command.Parameters.Add(parameter)
                Next
            End Sub

            Private Function GetEnumerator() As IEnumerator(Of IDbDataParameter) Implements IEnumerable(Of IDbDataParameter).GetEnumerator
                Return parameters.GetEnumerator
            End Function

            Private Function IEnumerable_GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
                Return GetEnumerator()
            End Function
        End Class

        Private Shared Function CreateSqlDataRecordList(ByVal numbers As IEnumerable(Of Integer)) As List(Of Microsoft.SqlServer.Server.SqlDataRecord)
            Dim number_list As New List(Of Microsoft.SqlServer.Server.SqlDataRecord)()
            Dim tvp_definition As Microsoft.SqlServer.Server.SqlMetaData() = {New Microsoft.SqlServer.Server.SqlMetaData("n", SqlDbType.Int)}

            For Each n As Integer In numbers
                Dim rec As New Microsoft.SqlServer.Server.SqlDataRecord(tvp_definition)
                rec.SetInt32(0, n)
                number_list.Add(rec)
            Next
            Return number_list
        End Function

        Private Class IntDynamicParam
            Implements SqlMapper.IDynamicParameters

            Private ReadOnly numbers As IEnumerable(Of Integer)

            Public Sub New(ByVal numbers As IEnumerable(Of Integer))
                Me.numbers = numbers
            End Sub

            Public Sub AddParameters(ByVal command As IDbCommand, ByVal identity As SqlMapper.Identity) Implements SqlMapper.IDynamicParameters.AddParameters
                Dim sqlCommand As SqlCommand = CType(command, SqlCommand)
                sqlCommand.CommandType = CommandType.StoredProcedure
                Dim number_list As IEnumerable(Of Microsoft.SqlServer.Server.SqlDataRecord) = CreateSqlDataRecordList(numbers)
                Dim p As SqlParameter = sqlCommand.Parameters.Add("ints", SqlDbType.Structured)
                p.Direction = ParameterDirection.Input
                p.TypeName = "int_list_type"
                p.Value = number_list
            End Sub
        End Class

        Private Class IntCustomParam
            Implements SqlMapper.ICustomQueryParameter

            Private ReadOnly numbers As IEnumerable(Of Integer)

            Public Sub New(ByVal numbers As IEnumerable(Of Integer))
                Me.numbers = numbers
            End Sub

            Public Sub AddParameter(ByVal command As IDbCommand, ByVal name As String) Implements SqlMapper.ICustomQueryParameter.AddParameter
                Dim sqlCommand As SqlCommand = CType(command, SqlCommand)
                sqlCommand.CommandType = CommandType.StoredProcedure
                Dim number_list As IEnumerable(Of Microsoft.SqlServer.Server.SqlDataRecord) = CreateSqlDataRecordList(numbers)
                Dim p As SqlParameter = sqlCommand.Parameters.Add(name, SqlDbType.Structured)
                p.Direction = ParameterDirection.Input
                p.TypeName = "int_list_type"
                p.Value = number_list
            End Sub
        End Class

        <TestMethod>
        Public Sub TestDoubleParam()
            Assert.AreEqual(0.1R, Connection.Query(Of Double)("select @d", New With {Key .d = 0.1R}).First())
        End Sub

        <TestMethod>
        Public Sub TestBoolParam()
            Assert.IsFalse(Connection.Query(Of Boolean)("select @b", New With {Key .b = False}).First())
        End Sub

        <TestMethod>
        Public Sub TestTimeSpanParam()
            Assert.AreEqual(Connection.Query(Of TimeSpan)("select @ts", New With {Key .ts = TimeSpan.FromMinutes(42)}).First(), TimeSpan.FromMinutes(42))
        End Sub

        <TestMethod>
        Public Sub PassInIntArray()
            Assert.AreEqual({1, 2, 3}, Connection.Query(Of Integer)("select * from (select 1 as Id union all select 2 union all select 3) as X where Id in @Ids",
                                                                    New With {Key .Ids = New Integer() {1, 2, 3}.AsEnumerable()}))
        End Sub

        <TestMethod>
        Public Sub PassInEmptyIntArray()
            Assert.AreEqual(New Integer(-1) {}, Connection.Query(Of Integer)("select * from (select 1 as Id union all select 2 union all select 3) as X where Id in @Ids",
                                                                             New With {Key .Ids = New Integer(-1) {}}))
        End Sub

        <TestMethod>
        Public Sub TestExecuteCommandWithHybridParameters()
            Dim p As New DynamicParameters(New With {Key .a = 1, Key .b = 2})
            p.Add("c", dbType:=DbType.Int32, direction:=ParameterDirection.Output)
            Connection.Execute("set @c = @a + @b", p)
            Assert.AreEqual(3, p.[Get](Of Integer)("@c"))
        End Sub

        <TestMethod>
        Public Sub GuidIn_SO_24177902()
            Dim a As Guid = Guid.NewGuid(), b As Guid = Guid.NewGuid(), c As Guid = Guid.NewGuid(), d As Guid = Guid.NewGuid()
            Connection.Execute("create table #foo (i int, g uniqueidentifier)")
            Connection.Execute("insert #foo(i,g) values(@i,@g)", {New With {Key .i = 1, Key .g = a}, New With {Key .i = 2, Key .g = b},
                               New With {Key .i = 3, Key .g = c},
                               New With {Key .i = 4, Key .g = d}})
            Dim guids As IEnumerable(Of Guid) = Connection.Query(Of Guid)("select g from #foo where i in (2,3)").ToArray()
            Assert.AreEqual(2, guids.Count)
            Assert.IsFalse(guids.Contains(a))
            Assert.IsTrue(guids.Contains(b))
            Assert.IsTrue(guids.Contains(c))
            Assert.IsFalse(guids.Contains(d))
            Dim rows = Connection.Query("select * from #foo where g in @guids order by i", New With {guids}).[Select](Function(row) New With {Key .i = CInt(row.i),
                                                                                                                          Key .g = CType(row.g, Guid)}).ToArray()
            Assert.AreEqual(2, rows.Length)
            Assert.AreEqual(2, rows(0).i)
            Assert.AreEqual(b, rows(0).g)
            Assert.AreEqual(3, rows(1).i)
            Assert.AreEqual(c, rows(1).g)
        End Sub

        ' <FactUnlessCaseSensitiveDatabase>

        Public Sub TestParameterInclusionNotSensitiveToCurrentCulture()
            Dim current As CultureInfo = ActiveCulture
            Try
                ActiveCulture = New CultureInfo("tr-TR")
                Connection.Query(Of Integer)("select @pid", New With {Key .PId = 1}).Single()
            Finally
                ActiveCulture = current
            End Try
        End Sub

        <TestMethod>
        Public Sub TestMassiveStrings()
            Dim str = New String("X"c, 20000)
            Assert.AreEqual(Connection.Query(Of String)("select @a", New With {Key .a = str}).First(), str)
        End Sub

        <TestMethod>
        Public Sub TestTVPWithAnonymousObject()
            Try
                Connection.Execute("CREATE TYPE int_list_type AS TABLE (n int NOT NULL PRIMARY KEY)")
                Connection.Execute("CREATE PROC get_ints @integers int_list_type READONLY AS select * from @integers")
                Dim nums As IEnumerable(Of Integer) = Connection.Query(Of Integer)("get_ints",
                                                                                   New With {Key .integers = New IntCustomParam(New Integer() {1, 2, 3})},
                                                                                   commandType:=CommandType.StoredProcedure).ToList()
                Assert.AreEqual(1, nums(0))
                Assert.AreEqual(2, nums(1))
                Assert.AreEqual(3, nums(2))
                Assert.AreEqual(3, nums.Count)
            Finally

                Try
                    Connection.Execute("DROP PROC get_ints")
                Finally
                    Connection.Execute("DROP TYPE int_list_type")
                End Try
            End Try
        End Sub

        <TestMethod>
        Public Sub TestTVPWithAnonymousEmptyObject()
            Try
                Connection.Execute("CREATE TYPE int_list_type AS TABLE (n int NOT NULL PRIMARY KEY)")
                Connection.Execute("CREATE PROC get_ints @integers int_list_type READONLY AS select * from @integers")
                Dim nums As IEnumerable(Of Integer) = Connection.Query(Of Integer)("get_ints", New With {Key .integers = New IntCustomParam(New Integer() {})
                }, commandType:=CommandType.StoredProcedure).ToList()
                Assert.AreEqual(1, nums(0))
                Assert.AreEqual(2, nums(1))
                Assert.AreEqual(3, nums(2))
                Assert.AreEqual(3, nums.Count)
            Catch ex As ArgumentException
                Assert.IsTrue(String.Compare(ex.Message, "There are no records in the SqlDataRecord enumeration. To send a table-valued parameter with no rows, use a null reference for the value instead.") = 0)
            Finally

                Try
                    Connection.Execute("DROP PROC get_ints")
                Finally
                    Connection.Execute("DROP TYPE int_list_type")
                End Try
            End Try
        End Sub

        <TestMethod>
        Public Sub TestTVP()
            Try
                Connection.Execute("CREATE TYPE int_list_type AS TABLE (n int NOT NULL PRIMARY KEY)")
                Connection.Execute("CREATE PROC get_ints @ints int_list_type READONLY AS select * from @ints")
                Dim nums As IEnumerable(Of Integer) = Connection.Query(Of Integer)("get_ints", New IntDynamicParam(New Integer() {1, 2, 3})).ToList()
                Assert.AreEqual(1, nums(0))
                Assert.AreEqual(2, nums(1))
                Assert.AreEqual(3, nums(2))
                Assert.AreEqual(3, nums.Count)
            Finally

                Try
                    Connection.Execute("DROP PROC get_ints")
                Finally
                    Connection.Execute("DROP TYPE int_list_type")
                End Try
            End Try
        End Sub

        Private Class DynamicParameterWithIntTVP
            Inherits DynamicParameters
            Implements SqlMapper.IDynamicParameters

            Private ReadOnly numbers As IEnumerable(Of Integer)

            Public Sub New(ByVal numbers As IEnumerable(Of Integer))
                Me.numbers = numbers
            End Sub

            Public Overloads Sub AddParameters(ByVal command As IDbCommand, ByVal identity As SqlMapper.Identity)
                MyBase.AddParameters(command, identity)
                Dim sqlCommand As SqlCommand = CType(command, SqlCommand)
                sqlCommand.CommandType = CommandType.StoredProcedure
                Dim number_list = CreateSqlDataRecordList(numbers)
                Dim p = sqlCommand.Parameters.Add("ints", SqlDbType.Structured)
                p.Direction = ParameterDirection.Input
                p.TypeName = "int_list_type"
                p.Value = number_list
            End Sub
        End Class

        <TestMethod>
        Public Sub TestTVPWithAdditionalParams()
            Try
                Connection.Execute("CREATE TYPE int_list_type AS TABLE (n int NOT NULL PRIMARY KEY)")
                Connection.Execute("CREATE PROC get_values @ints int_list_type READONLY, @stringParam varchar(20), @dateParam datetime AS select i.*, @stringParam as stringParam, @dateParam as dateParam from @ints i")
                Dim dynamicParameters = New DynamicParameterWithIntTVP(New Integer() {1, 2, 3})
                dynamicParameters.AddDynamicParams(New With {Key .stringParam = "stringParam", Key .dateParam = New DateTime(2012, 1, 1)})
                Dim results = Connection.Query("get_values", dynamicParameters, commandType:=CommandType.StoredProcedure).ToList()
                Assert.AreEqual(3, results.Count)

                For i As Integer = 0 To results.Count - 1
                    Dim result = results(i)
                    Assert.AreEqual(i + 1, result.n)
                    Assert.AreEqual("stringParam", result.stringParam)
                    Assert.AreEqual(New DateTime(2012, 1, 1), result.dateParam)
                Next

            Finally

                Try
                    Connection.Execute("DROP PROC get_values")
                Finally
                    Connection.Execute("DROP TYPE int_list_type")
                End Try
            End Try
        End Sub

        <TestMethod>
        Public Sub TestSqlDataRecordListParametersWithAsTableValuedParameter()
            Try
                Connection.Execute("CREATE TYPE int_list_type AS TABLE (n int NOT NULL PRIMARY KEY)")
                Connection.Execute("CREATE PROC get_ints @integers int_list_type READONLY AS select * from @integers")
                Dim records = CreateSqlDataRecordList(New Integer() {1, 2, 3})
                Dim nums = Connection.Query(Of Integer)("get_ints", New With {Key .integers = records.AsTableValuedParameter()}, commandType:=CommandType.StoredProcedure).ToList()
                Assert.AreEqual(New Integer() {1, 2, 3}, nums)
                nums = Connection.Query(Of Integer)("select * from @integers", New With {Key .integers = records.AsTableValuedParameter("int_list_type")}).ToList()
                Assert.AreEqual(New Integer() {1, 2, 3}, nums)

                Try
                    Connection.Query(Of Integer)("select * from @integers", New With {Key .integers = records.AsTableValuedParameter()}).First()
                    Throw New InvalidOperationException()
                Catch ex As Exception
                    ex.Message.Equals("The table type parameter 'ids' must have a valid type name.")
                End Try

            Finally

                Try
                    Connection.Execute("DROP PROC get_ints")
                Finally
                    Connection.Execute("DROP TYPE int_list_type")
                End Try
            End Try
        End Sub

        <TestMethod>
        Public Sub TestEmptySqlDataRecordListParametersWithAsTableValuedParameter()
            Try
                Connection.Execute("CREATE TYPE int_list_type AS TABLE (n int NOT NULL PRIMARY KEY)")
                Connection.Execute("CREATE PROC get_ints @integers int_list_type READONLY AS select * from @integers")
                Dim emptyRecord = CreateSqlDataRecordList(Enumerable.Empty(Of Integer)())
                Dim nums = Connection.Query(Of Integer)("get_ints", New With {Key .integers = emptyRecord.AsTableValuedParameter()}, commandType:=CommandType.StoredProcedure).ToList()
                Assert.IsTrue(nums.Count = 0)
            Finally

                Try
                    Connection.Execute("DROP PROC get_ints")
                Finally
                    Connection.Execute("DROP TYPE int_list_type")
                End Try
            End Try
        End Sub

        <TestMethod>
        Public Sub TestSqlDataRecordListParametersWithTypeHandlers()
            Try
                Connection.Execute("CREATE TYPE int_list_type AS TABLE (n int NOT NULL PRIMARY KEY)")
                Connection.Execute("CREATE PROC get_ints @integers int_list_type READONLY AS select * from @integers")
                Dim records As IEnumerable(Of Microsoft.SqlServer.Server.SqlDataRecord) = CreateSqlDataRecordList(New Integer() {1, 2, 3})
                Dim nums = Connection.Query(Of Integer)("get_ints", New With {Key .integers = records}, commandType:=CommandType.StoredProcedure).ToList()
                Assert.AreEqual(New Integer() {1, 2, 3}, nums)

                Try
                    Connection.Query(Of Integer)("select * from @integers", New With {Key .integers = records}).First()
                    Throw New InvalidOperationException()
                Catch ex As Exception
                    ex.Message.Equals("The table type parameter 'ids' must have a valid type name.")
                End Try

            Finally

                Try
                    Connection.Execute("DROP PROC get_ints")
                Finally
                    Connection.Execute("DROP TYPE int_list_type")
                End Try
            End Try
        End Sub

        <TestMethod>
        Public Sub DataTableParameters()
            Try
                Connection.Execute("drop proc #DataTableParameters")
            Catch ' don't care
            End Try
            Try
                Connection.Execute("drop table #DataTableParameters")
            Catch ' don't care
            End Try
            Try
                Connection.Execute("drop type MyTVPType")
            Catch ' don't care
            End Try
            Connection.Execute("create type MyTVPType as table (id int)")
            Connection.Execute("create proc #DataTableParameters @ids MyTVPType readonly as select count(1) from @ids")

            Dim table As New DataTable ' With {.Columns From {{"id", GetType(Integer)}}, .Rows = {{1}, {2}, {3}}}
            table.Columns.Add("id", GetType(Integer))
            table.Rows.Add({1})
            table.Rows.Add({2})
            table.Rows.Add({3})

            Dim count As Integer = Connection.Query(Of Integer)("#DataTableParameters", New With {Key .ids = table.AsTableValuedParameter()}, commandType:=CommandType.StoredProcedure).First()
            Assert.AreEqual(3, count)

            count = Connection.Query(Of Integer)("select count(1) from @ids", New With {Key .ids = table.AsTableValuedParameter("MyTVPType")}).First()
            Assert.AreEqual(3, count)

            Try
                Connection.Query(Of Integer)("select count(1) from @ids", New With {Key .ids = table.AsTableValuedParameter()}).First()
                Throw New InvalidOperationException()
            Catch ex As Exception
                ex.Message.Equals("The table type parameter 'ids' must have a valid type name.")
            End Try
        End Sub

#If False Then
        <TestMethod>
        Public Sub SO29533765_DataTableParametersViaDynamicParameters()
            Try ' don't care
                Connection.Execute("drop proc #DataTableParameters")
            Catch
            End Try
            Try ' don't care
                Connection.Execute("drop table #DataTableParameters")
            Catch
            End Try
            Try ' don't care
                Connection.Execute("drop type MyTVPType")
            Catch
            End Try
            Connection.Execute("create type MyTVPType as table (id int)")
            Connection.Execute("create proc #DataTableParameters @ids MyTVPType readonly as select count(1) from @ids")

            Dim table As New DataTable With {.TableName = "MyTVPType"}
            table.Columns.Add("id", GetType(Integer))
            table.Rows.Add({1})
            table.Rows.Add({1})
            table.Rows.Add({1})

            table.SetTypeName(table.TableName) ' per SO29533765
            Dim args As IDictionary(Of String, Object) = New Dictionary(Of String, Object) With {  ("ids") = table}
            Dim count As Integer = Connection.Query(Of Integer)("#DataTableParameters", args, commandType:=CommandType.StoredProcedure).First()
            Assert.AreEqual(3, count)

            count = Connection.Query(Of Integer)("select count(1) from @ids", args).First()
            Assert.AreEqual(3, count)
        End Sub

#End If

        <TestMethod>
        Public Sub SO26468710_InWithTVPs()
            ' this is just to make it re-runnable; normally you only do this once
            Try
                Connection.Execute("drop type MyIdList")
            Catch ' don't care
            End Try
            Connection.Execute("create type MyIdList as table(id int);")

            ' Dim ids = New DataTable With {.Columns = {{"id", GetType(Integer)}}, .Rows = {{1}, {3}, {5}}}
            Dim ids As New DataTable
            ids.Columns.Add("id", GetType(Integer))
            ids.Rows.Add({1})
            ids.Rows.Add({3})
            ids.Rows.Add({5})

            ids.SetTypeName("MyIdList")
            Dim sum As Integer = Connection.Query(Of Integer)("" & ControlChars.CrLf & "            declare @tmp table(id int not null);" & ControlChars.CrLf & "            insert @tmp (id) values(1), (2), (3), (4), (5), (6), (7);" & ControlChars.CrLf & "            select * from @tmp t inner join @ids i on i.id = t.id", New With {Key ids}).Sum()
            Assert.AreEqual(9, sum)
        End Sub

        <TestMethod>
        Public Sub DataTableParametersWithExtendedProperty()
            Try
                Connection.Execute("drop proc #DataTableParameters")
            Catch ' don't care
            End Try
            Try
                Connection.Execute("drop table #DataTableParameters")
            Catch ' don't care
            End Try
            Try
                Connection.Execute("drop type MyTVPType")
            Catch ' don't care
            End Try
            Connection.Execute("create type MyTVPType as table (id int)")
            Connection.Execute("create proc #DataTableParameters @ids MyTVPType readonly as select count(1) from @ids")

            ' Dim table = New DataTable With {.Columns = {{"id", GetType(Integer)}}, .Rows = {{1}, {2}, {3}}}
            Dim table As New DataTable
            table.Columns.Add("id", GetType(Integer))
            table.Rows.Add({1})
            table.Rows.Add({2})
            table.Rows.Add({3})

            table.SetTypeName("MyTVPType") ' <== extended metadata
            Dim count As Integer = Connection.Query(Of Integer)("#DataTableParameters", New With {Key .ids = table}, commandType:=CommandType.StoredProcedure).First()
            Assert.AreEqual(3, count)

            count = Connection.Query(Of Integer)("select count(1) from @ids", New With {Key .ids = table}).First()
            Assert.AreEqual(3, count)

            Try
                Connection.Query(Of Integer)("select count(1) from @ids", New With {Key .ids = table}).First()
                Throw New InvalidOperationException()
            Catch ex As Exception
                ex.Message.Equals("The table type parameter 'ids' must have a valid type name.")
            End Try
        End Sub

        <TestMethod>
        Public Sub SupportInit()
            Dim obj = Connection.Query(Of WithInit)("select 'abc' as Value").Single()
            Assert.AreEqual("abc", obj.Value)
            Assert.AreEqual(31, obj.Flags)
        End Sub

        Public Class WithInit
            Implements ISupportInitialize

            Public Property Value As String
            Public Property Flags As Integer

            Private Sub BeginInit() Implements ISupportInitialize.BeginInit
                Math.Min(System.Threading.Interlocked.Increment(Flags), Flags - 1)
            End Sub

            Private Sub EndInit() Implements ISupportInitialize.EndInit
                Flags = 30
            End Sub

            Private Class CSharpImpl
                Shared Function __Assign(Of T)(ByRef target As T, value As T) As T
                    target = value
                    Return value
                End Function
            End Class
        End Class

        <TestMethod>
        Public Sub SO29596645_TvpProperty()
            Try
                Connection.Execute("CREATE TYPE SO29596645_ReminderRuleType AS TABLE (id int NOT NULL)")
            Catch
            End Try

            Connection.Execute("create proc #SO29596645_Proc (@Id int, @Rules SO29596645_ReminderRuleType READONLY)
                                as begin select @Id + ISNULL((select sum(id) from @Rules), 0); end")
            Dim obj = New SO29596645_OrganisationDTO()
            Dim val As Integer = Connection.Query(Of Integer)("#SO29596645_Proc", obj.Rules, commandType:=CommandType.StoredProcedure).Single()
            Assert.AreEqual(20, val)
        End Sub

        Private Class SO29596645_RuleTableValuedParameters
            Implements SqlMapper.IDynamicParameters

            Private ReadOnly parameterName As String

            Public Sub New(ByVal parameterName As String)
                Me.parameterName = parameterName
            End Sub

            Public Sub AddParameters(command As IDbCommand, identity As SqlMapper.Identity) Implements SqlMapper.IDynamicParameters.AddParameters
                Debug.WriteLine("> AddParameters")
                Dim lazy = CType(command, SqlCommand)
                lazy.Parameters.AddWithValue("Id", 7)
                'Dim table = New DataTable With {.Columns = {{"Id", GetType(Integer)}}, .Rows = {{4}, {9}}}
                Dim table As New DataTable
                table.Columns.Add("id", GetType(Integer))
                table.Rows.Add({4})
                table.Rows.Add({9})

                lazy.Parameters.AddWithValue("Rules", table)
                Debug.WriteLine("< AddParameters")
            End Sub

        End Class

        Private Class SO29596645_OrganisationDTO
            Public ReadOnly Property Rules As SO29596645_RuleTableValuedParameters
            Public Sub New()
                Rules = New SO29596645_RuleTableValuedParameters("@Rules")
            End Sub
        End Class

        <TestMethod>
        Public Sub TestCustomParameters()
            Dim args = New DbParams From {
                New SqlParameter("foo", 123),
                New SqlParameter("bar", "abc")
            }
            Dim result = Connection.Query("select Foo=@foo, Bar=@bar", args).Single()
            Dim foo As Integer = result.Foo
            Dim bar As String = result.Bar
            Assert.AreEqual(123, foo)
            Assert.AreEqual("abc", bar)
        End Sub

        <TestMethod>
        Public Sub TestDynamicParamNullSupport()
            Dim p = New DynamicParameters()
            p.Add("@b", dbType:=DbType.Int32, direction:=ParameterDirection.Output)
            Connection.Execute("select @b = null", p)
            Assert.IsNull(p.[Get](Of Integer?)("@b"))
        End Sub

        <TestMethod>
        Public Sub TestAppendingAnonClasses()
            Dim p = New DynamicParameters()
            p.AddDynamicParams(New With {Key .A = 1, Key .B = 2})
            p.AddDynamicParams(New With {Key .C = 3, Key .D = 4})
            Dim result = Connection.Query("select @A a,@B b,@C c,@D d", p).Single()
            Assert.AreEqual(1, CInt(result.a))
            Assert.AreEqual(2, CInt(result.b))
            Assert.AreEqual(3, CInt(result.c))
            Assert.AreEqual(4, CInt(result.d))
        End Sub

        <TestMethod>
        Public Sub TestAppendingADictionary()
            Dim dictionary As New Dictionary(Of String, Object) From {{"A", 1}, {"B", "two"}}

            Dim p = New DynamicParameters()
            p.AddDynamicParams(dictionary)

            Dim result = Connection.Query("select @A a, @B b", p).Single()

            Assert.AreEqual(1, CInt(Fix(result.a)))
            Assert.AreEqual("two", CStr(result.b))
        End Sub

        <TestMethod>
        Public Sub TestAppendingAnExpandoObject()
#Disable Warning IDE0017 ' Simplify object initialization
            Dim expando As Object = New ExpandoObject()
#Enable Warning IDE0017 ' Simplify object initialization
            expando.A = 1
            expando.B = "two"

            Dim p = New DynamicParameters()
            p.AddDynamicParams(expando)

            Dim result = Connection.Query("select @A a, @B b", p).Single()

            Assert.AreEqual(1, CInt(Fix(result.a)))
            Assert.AreEqual("two", CStr(result.b))
        End Sub

        <TestMethod>
        Public Sub TestAppendingAList()
            Dim p = New DynamicParameters()
            Dim list = New Integer() {1, 2, 3}
            p.AddDynamicParams(New With {list
            })
            Dim result = Connection.Query(Of Integer)("select * from (select 1 A union all select 2 union all select 3) X where A in @list", p).ToList()
            Assert.AreEqual(1, result(0))
            Assert.AreEqual(2, result(1))
            Assert.AreEqual(3, result(2))
        End Sub

        <TestMethod>
        Public Sub TestAppendingAListAsDictionary()
            Dim p = New DynamicParameters()
            Dim list = New Integer() {1, 2, 3}
            Dim args = New Dictionary(Of String, Object)
            For Each i As Integer In list
                args.Add(i.ToString, i)
            Next
            p.AddDynamicParams(args)

            Dim result = Connection.Query(Of Integer)("select * from (select 1 A union all select 2 union all select 3) X where A in @ids", p).ToList()

            Assert.AreEqual(1, result(0))
            Assert.AreEqual(2, result(1))
            Assert.AreEqual(3, result(2))
        End Sub

        <TestMethod>
        Public Sub TestAppendingAListByName()
            Dim p As DynamicParameters = New DynamicParameters()
            Dim list = New Integer() {1, 2, 3}
            p.Add("ids", list)
            Dim result = Connection.Query(Of Integer)("select * from (select 1 A union all select 2 union all select 3) X where A in @ids", p).ToList()
            Assert.AreEqual(1, result(0))
            Assert.AreEqual(2, result(1))
            Assert.AreEqual(3, result(2))
        End Sub

        <TestMethod>
        Public Sub ParameterizedInWithOptimizeHint()
            Const sql As String = "
select count(1)
from(
    select 1 as x
    union all select 2
    union all select 5) y
where y.x in @vals
option (optimize for (@vals unKnoWn))"
            Dim count As Integer = Connection.Query(Of Integer)(sql, New With {Key .vals = {1, 2, 3, 4}}).Single()
            Assert.AreEqual(2, count)
            count = Connection.Query(Of Integer)(sql, New With {Key .vals = {1}}).Single()
            Assert.AreEqual(1, count)
            count = Connection.Query(Of Integer)(sql, New With {Key .vals = New Integer(-1) {}}).Single()
            Assert.AreEqual(0, count)
        End Sub

        <TestMethod>
        Public Sub TestProcedureWithTimeParameter()
            Dim p = New DynamicParameters()
            p.Add("a", TimeSpan.FromHours(10), dbType:=DbType.Time)
            Connection.Execute("CREATE PROCEDURE #TestProcWithTimeParameter
    @a TIME
    AS 
    BEGIN
    SELECT @a
    END")
            Assert.AreEqual(Connection.Query(Of TimeSpan)("#TestProcWithTimeParameter", p, commandType:=CommandType.StoredProcedure).First(), New TimeSpan(10, 0, 0))
        End Sub

        <TestMethod>
        Public Sub TestUniqueIdentifier()
            Dim guid = System.Guid.NewGuid()
            Dim result = Connection.Query(Of Guid)("declare @foo uniqueidentifier set @foo = @guid select @foo", New With {guid}).Single()
            Assert.AreEqual(guid, result)
        End Sub

        <TestMethod>
        Public Sub TestNullableUniqueIdentifierNonNull()
            Dim guid As Guid? = System.Guid.NewGuid()
            Dim result = Connection.Query(Of Guid?)("declare @foo uniqueidentifier set @foo = @guid select @foo", New With {guid}).Single()
            Assert.AreEqual(guid, result)
        End Sub

        <TestMethod>
        Public Sub TestNullableUniqueIdentifierNull()
            Dim guid As Guid? = Nothing
            Dim result = Connection.Query(Of Guid?)("declare @foo uniqueidentifier set @foo = @guid select @foo", New With {guid}).Single()
            Assert.AreEqual(guid, result)
        End Sub

        <TestMethod>
        Public Sub TestSupportForDynamicParameters()
            Dim p = New DynamicParameters()
            p.Add("name", "bob")
            p.Add("age", dbType:=DbType.Int32, direction:=ParameterDirection.Output)
            Assert.AreEqual("bob", Connection.Query(Of String)("set @age = 11 select @name", p).First())
            Assert.AreEqual(11, p.[Get](Of Integer)("age"))
        End Sub

        <TestMethod>
        Public Sub TestSupportForDynamicParametersOutputExpressions()
            Dim bob = New Person With {
                .Name = "bob",
                .PersonId = 1,
                .Address = New Address With {
                    .PersonId = 2
                }
            }
            Dim p = New DynamicParameters(bob)
            p.Output(bob, Function(b) b.PersonId)
            p.Output(bob, Function(b) b.Occupation)
            p.Output(bob, Function(b) b.NumberOfLegs)
            p.Output(bob, Function(b) b.Address.Name)
            p.Output(bob, Function(b) b.Address.PersonId)
            Connection.Execute("
SET @Occupation = 'grillmaster' 
SET @PersonId = @PersonId + 1 
SET @NumberOfLegs = @NumberOfLegs - 1
SET @AddressName = 'bobs burgers'
SET @AddressPersonId = @PersonId", p)
            Assert.AreEqual("grillmaster", bob.Occupation)
            Assert.AreEqual(2, bob.PersonId)
            Assert.AreEqual(1, bob.NumberOfLegs)
            Assert.AreEqual("bobs burgers", bob.Address.Name)
            Assert.AreEqual(2, bob.Address.PersonId)
        End Sub

        <TestMethod>
        Public Sub TestSupportForDynamicParametersOutputExpressions_Scalar()
            Using connection = ParameterTests.GetOpenConnection()
                Dim bob = New Person With {
                    .Name = "bob",
                    .PersonId = 1,
                    .Address = New Address With {
                        .PersonId = 2
                    }
                }
                Dim p = New DynamicParameters(bob)
                p.Output(bob, Function(b) b.PersonId)
                p.Output(bob, Function(b) b.Occupation)
                p.Output(bob, Function(b) b.NumberOfLegs)
                p.Output(bob, Function(b) b.Address.Name)
                p.Output(bob, Function(b) b.Address.PersonId)
                Dim result = CInt(connection.ExecuteScalar("
SET @Occupation = 'grillmaster' 
SET @PersonId = @PersonId + 1 
SET @NumberOfLegs = @NumberOfLegs - 1
SET @AddressName = 'bobs burgers'
SET @AddressPersonId = @PersonId
select 42", p))
                Assert.AreEqual("grillmaster", bob.Occupation)
                Assert.AreEqual(2, bob.PersonId)
                Assert.AreEqual(1, bob.NumberOfLegs)
                Assert.AreEqual("bobs burgers", bob.Address.Name)
                Assert.AreEqual(2, bob.Address.PersonId)
                Assert.AreEqual(42, result)
            End Using
        End Sub

        <TestMethod>
        Public Sub TestSupportForDynamicParametersOutputExpressions_Query_Buffered()
            Using connection = TestBase.GetOpenConnection()
                Dim bob = New Person With {
                    .Name = "bob",
                    .PersonId = 1,
                    .Address = New Address With {
                        .PersonId = 2
                    }
                }
                Dim p = New DynamicParameters(bob)
                p.Output(bob, Function(b) b.PersonId)
                p.Output(bob, Function(b) b.Occupation)
                p.Output(bob, Function(b) b.NumberOfLegs)
                p.Output(bob, Function(b) b.Address.Name)
                p.Output(bob, Function(b) b.Address.PersonId)
                Dim result = connection.Query(Of Integer)("
SET @Occupation = 'grillmaster' 
SET @PersonId = @PersonId + 1 
SET @NumberOfLegs = @NumberOfLegs - 1
SET @AddressName = 'bobs burgers'
SET @AddressPersonId = @PersonId
select 42", p, buffered:=True).Single()
                Assert.AreEqual("grillmaster", bob.Occupation)
                Assert.AreEqual(2, bob.PersonId)
                Assert.AreEqual(1, bob.NumberOfLegs)
                Assert.AreEqual("bobs burgers", bob.Address.Name)
                Assert.AreEqual(2, bob.Address.PersonId)
                Assert.AreEqual(42, result)
            End Using
        End Sub

        <TestMethod>
        Public Sub TestSupportForDynamicParametersOutputExpressions_Query_NonBuffered()
            Using connection = TestBase.GetOpenConnection()
                Dim bob = New Person With {
                    .Name = "bob",
                    .PersonId = 1,
                    .Address = New Address With {
                        .PersonId = 2
                    }
                }
                Dim p = New DynamicParameters(bob)
                p.Output(bob, Function(b) b.PersonId)
                p.Output(bob, Function(b) b.Occupation)
                p.Output(bob, Function(b) b.NumberOfLegs)
                p.Output(bob, Function(b) b.Address.Name)
                p.Output(bob, Function(b) b.Address.PersonId)
                Dim result = connection.Query(Of Integer)("
SET @Occupation = 'grillmaster' 
SET @PersonId = @PersonId + 1 
SET @NumberOfLegs = @NumberOfLegs - 1
SET @AddressName = 'bobs burgers'
SET @AddressPersonId = @PersonId
select 42", p, buffered:=False).Single()
                Assert.AreEqual("grillmaster", bob.Occupation)
                Assert.AreEqual(2, bob.PersonId)
                Assert.AreEqual(1, bob.NumberOfLegs)
                Assert.AreEqual("bobs burgers", bob.Address.Name)
                Assert.AreEqual(2, bob.Address.PersonId)
                Assert.AreEqual(42, result)
            End Using
        End Sub

        <TestMethod>
        Public Sub TestSupportForDynamicParametersOutputExpressions_QueryMultiple()
            Using connection = TestBase.GetOpenConnection()
                Dim bob = New Person With {
                    .Name = "bob",
                    .PersonId = 1,
                    .Address = New Address With {
                        .PersonId = 2
                    }
                }
                Dim p = New DynamicParameters(bob)
                p.Output(bob, Function(b) b.PersonId)
                p.Output(bob, Function(b) b.Occupation)
                p.Output(bob, Function(b) b.NumberOfLegs)
                p.Output(bob, Function(b) b.Address.Name)
                p.Output(bob, Function(b) b.Address.PersonId)
                Dim x, y As Integer

                Using multi = connection.QueryMultiple("
SET @Occupation = 'grillmaster' 
SET @PersonId = @PersonId + 1 
SET @NumberOfLegs = @NumberOfLegs - 1
SET @AddressName = 'bobs burgers'
select 42
select 17
SET @AddressPersonId = @PersonId", p)
                    x = multi.Read(Of Integer)().Single()
                    y = multi.Read(Of Integer)().Single()
                End Using

                Assert.AreEqual("grillmaster", bob.Occupation)
                Assert.AreEqual(2, bob.PersonId)
                Assert.AreEqual(1, bob.NumberOfLegs)
                Assert.AreEqual("bobs burgers", bob.Address.Name)
                Assert.AreEqual(2, bob.Address.PersonId)
                Assert.AreEqual(42, x)
                Assert.AreEqual(17, y)
            End Using
        End Sub

        <TestMethod>
        Public Sub TestSupportForExpandoObjectParameters()
            Dim p As New Dyno() With {.Name = "bob"}
            Dim parameters As Object = p
            Dim result As String = Connection.Query(Of String)("select @name", parameters).First()
            Assert.AreEqual("bob", result)
        End Sub

        <TestMethod>
        Public Sub SO25069578_DynamicParams_Procs()
            Dim parameters = New DynamicParameters()
            parameters.Add("foo", "bar")

            Try
                Connection.Execute("drop proc SO25069578")
            Catch
            End Try

            Connection.Execute("create proc SO25069578 @foo nvarchar(max) as select @foo as [X]")
            Dim tran = Connection.BeginTransaction()
            Dim row = Connection.Query(Of HazX)("SO25069578", parameters, commandType:=CommandType.StoredProcedure, transaction:=tran).Single()
            tran.Rollback()
            Assert.AreEqual("bar", row.X)
        End Sub

        Public Class HazX
            Public Property X As String
        End Class

        Public Sub SO25297173_DynamicIn()
            Const query As String = "
declare @table table(value int not null);
insert @table values(1);
insert @table values(2);
insert @table values(3);
insert @table values(4);
insert @table values(5);
insert @table values(6);
insert @table values(7);
SELECT value FROM @table WHERE value IN @myIds"
            Dim queryParams = New Dictionary(Of String, Object) From {{"5", 5}, {"6", 6}}

            Dim dynamicParams = New DynamicParameters(queryParams)
            Dim result As List(Of Integer) = Connection.Query(Of Integer)(query, dynamicParams).ToList()
            Assert.AreEqual(2, result.Count)
            Assert.IsTrue(result.Contains(5))
            Assert.IsTrue(result.Contains(6))
        End Sub

        <TestMethod>
        Public Sub Test_AddDynamicParametersRepeatedShouldWork()
            Dim args = New DynamicParameters()
            args.AddDynamicParams(New With {Key .Foo = 123})
            args.AddDynamicParams(New With {Key .Foo = 123})
            Dim i As Integer = Connection.Query(Of Integer)("select @Foo", args).Single()
            Assert.AreEqual(123, i)
        End Sub

        <TestMethod>
        Public Sub Test_AddDynamicParametersRepeatedIfParamTypeIsDbStiringShouldWork()
            Dim foo = New DbString() With {.Value = "123"}
            Dim args = New DynamicParameters()
#Disable Warning IDE0037 ' Use inferred member name
            args.AddDynamicParams(New With {Key .Foo = foo})
            args.AddDynamicParams(New With {Key .Foo = foo})
#Enable Warning IDE0037 ' Use inferred member name
            Dim i As Integer = Connection.Query(Of Integer)("select @Foo", args).Single()
            Assert.AreEqual(123, i)
        End Sub

        <TestMethod>
        Public Sub AllowIDictionaryParameters()
            Dim parameters As New Dictionary(Of String, Object) From {{"param1", 0}}
            Connection.Query("SELECT @param1", parameters)
        End Sub

        <TestMethod>
        Public Sub TestParameterWithIndexer()
            Me.Connection.Execute("create proc #TestProcWithIndexer 
	@A int
as 
begin
	select @A
end")
            Dim item = Connection.Query(Of Integer)("#TestProcWithIndexer", New ParameterWithIndexer(), commandType:=CommandType.StoredProcedure).Single()
        End Sub

        Public Class ParameterWithIndexer
            Public Property A As Integer
            Default Public Overridable Property Item(ByVal columnName As String) As String
                Get
                    Return Nothing
                End Get
                Set(ByVal value As String)
                End Set
            End Property
        End Class

        <TestMethod>
        Public Sub TestMultipleParametersWithIndexer()
            Dim order = Connection.Query(Of MultipleParametersWithIndexer)("select 1 A,2 B").First()
            Assert.AreEqual(1, order.A)
            Assert.AreEqual(2, order.B)
        End Sub

        Public Class MultipleParametersWithIndexer
            Inherits MultipleParametersWithIndexerDeclaringType

            Public Property A As Integer

            Private Class CSharpImpl
                <Obsolete("Please refactor calling code to use normal Visual Basic assignment")>
                Shared Function __Assign(Of T)(ByRef target As T, value As T) As T
                    target = value
                    Return value
                End Function
            End Class
        End Class

        Public Class MultipleParametersWithIndexerDeclaringType
            Default Public Property Item(ByVal field As Object) As Object
                Get
                    Return Nothing
                End Get
                Set(ByVal value As Object)
                End Set
            End Property

            Default Public Property Item(ByVal field As Object, ByVal index As Integer) As Object
                Get
                    Return Nothing
                End Get
                Set(ByVal value As Object)
                End Set
            End Property

            Public Property B As Integer

            Private Class CSharpImpl
                Shared Function __Assign(Of T)(ByRef target As T, value As T) As T
                    target = value
                    Return value
                End Function
            End Class
        End Class

        <TestMethod>
        Public Sub Issue182_BindDynamicObjectParametersAndColumns()
            Connection.Execute("create table #Dyno ([Id] uniqueidentifier primary key, [Name] nvarchar(50) not null, [Foo] bigint not null);")
            Dim guid = System.Guid.NewGuid()
            Dim orig = New Dyno With {
                .Name = "T Rex",
                .Id = guid,
                .Foo = 123L
            }
            Dim result = Connection.Execute("insert into #Dyno ([Id], [Name], [Foo]) values (@Id, @Name, @Foo);", orig)
            Dim fromDb = Connection.Query(Of Dyno)("select * from #Dyno where Id=@Id", orig).Single()
            Assert.AreEqual(CType(fromDb.Id, Guid), guid)
            Assert.AreEqual("T Rex", fromDb.Name)
            Assert.AreEqual(123L, CLng(fromDb.Foo))
        End Sub

        Public Class Dyno
            Public Property Id As Object
            Public Property Name As String
            Public Property Foo As Object

            Private Class CSharpImpl
                Shared Function __Assign(Of T)(ByRef target As T, value As T) As T
                    target = value
                    Return value
                End Function
            End Class
        End Class

        <TestMethod>
        Public Sub Issue151_ExpandoObjectArgsQuery()
            Dim args As New Dyno() With {.Id = 123, .Name = "abc"}
            Dim row = Connection.Query("select @Id as [Id], @Name as [Name]", CObj(args)).Single()
            Assert.AreEqual(123, CInt(row.Id))
            Assert.AreEqual("abc", CStr(row.Name))
        End Sub

        <TestMethod>
        Public Sub Issue151_ExpandoObjectArgsExec()
            Dim args As New Dyno() With {.Id = 123, .Name = "abc"}
            Connection.Execute("create table #issue151 (Id int not null, Name nvarchar(20) not null)")
            Assert.AreEqual(1, Connection.Execute("insert #issue151 values(@Id, @Name)", CObj(args)))
            Dim row = Connection.Query("select Id, Name from #issue151").Single()
            Assert.AreEqual(123, CInt(row.Id))
            Assert.AreEqual("abc", CStr(row.Name))
        End Sub

        <TestMethod>
        Public Sub Issue192_InParameterWorksWithSimilarNames()
            Dim rows = Connection.Query("
declare @Issue192 table (
    Field INT NOT NULL PRIMARY KEY IDENTITY(1,1),
    Field_1 INT NOT NULL);
insert @Issue192(Field_1) values (1), (2), (3);
SELECT * FROM @Issue192 WHERE Field IN @Field AND Field_1 IN @Field_1", New With {Key .Field = {1, 2}, Key .Field_1 = {2, 3}
            }).Single()
            Assert.AreEqual(2, CInt(rows.Field))
            Assert.AreEqual(2, CInt(rows.Field_1))
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")>
        <TestMethod>
        Public Sub Issue192_InParameterWorksWithSimilarNamesWithUnicode()
            Dim rows = Connection.Query("
declare @Issue192 table (
    Field INT NOT NULL PRIMARY KEY IDENTITY(1,1),
    Field_1 INT NOT NULL);
insert @Issue192(Field_1) values (1), (2), (3);
SELECT * FROM @Issue192 WHERE Field IN @µ AND Field_1 IN @µµ", New With {Key .µ = {1, 2}, Key .µµ = {2, 3}}).Single()
            Assert.AreEqual(2, CInt(rows.Field))
            Assert.AreEqual(2, CInt(rows.Field_1))
        End Sub

        ' <FactUnlessCaseSensitiveDatabase>

        Public Sub Issue220_InParameterCanBeSpecifiedInAnyCase()
            Assert.AreEqual({1}, Connection.Query(Of Integer)("select * from (select 1 as Id) as X where Id in @ids", New With {Key .Ids = {1}}))
        End Sub

        <TestMethod>
        Public Sub SO30156367_DynamicParamsWithoutExec()
            Dim dbParams = New DynamicParameters()
            dbParams.Add("Field1", 1)
            Dim value = dbParams.[Get](Of Integer)("Field1")
            Assert.AreEqual(1, value)
        End Sub

        <TestMethod>
        Public Sub RunAllStringSplitTestsDisabled()
            RunAllStringSplitTests(-1, 1500)
        End Sub

        ' <FactRequiredCompatibilityLevel(FactRequiredCompatibilityLevelAttribute.SqlServer2016)>

        Public Sub RunAllStringSplitTestsEnabled()
            RunAllStringSplitTests(10, 4500)
        End Sub

        Private Sub RunAllStringSplitTests(ByVal stringSplit As Integer, ByVal Optional max As Integer = 150)
            Dim oldVal As Integer = SqlMapper.Settings.InListStringSplitCount

            Try
                SqlMapper.Settings.InListStringSplitCount = stringSplit

                Try
                    Connection.Execute("drop table #splits")
                Catch
                End Try

                Dim count As Integer = Connection.QuerySingle(Of Integer)("create table #splits (i int not null);" & String.Concat(Enumerable.Range(-max, max * 3).[Select](Function(i) $"insert #splits (i) values ({i});")) & "select count(1) from #splits")
                Assert.AreEqual(count, 3 * max)

                Dim j As Integer = 0
                While j < max
                    Try
                        Dim vals = Enumerable.Range(1, j)
                        Dim list = Connection.Query(Of Integer)("select i from #splits where i in @vals", New With {vals}).AsList()
                        Assert.AreEqual(list.Count, j)
                        Assert.AreEqual(list.Sum(), vals.Sum())
                    Catch ex As Exception
                        Throw New InvalidOperationException($"Error when j={j}: {ex.Message}", ex)
                    End Try
                    Incr(j)
                End While

            Finally
                SqlMapper.Settings.InListStringSplitCount = oldVal
            End Try
        End Sub

        Private Shared Sub Incr(ByRef i As Integer)
            If i <= 15 Then
                i += 1
            ElseIf i <= 80 Then
                i += 5
            ElseIf i <= 200 Then
                i += 10
            ElseIf i <= 1000 Then
                i += 50
            Else
                i += 100
            End If
        End Sub

        <TestMethod>
        Public Sub Issue601_InternationalParameterNamesWork()
            Dim result = Connection.QuerySingle(Of Integer)("select @æøå٦", New With {Key .æøå٦ = 42
            })
            Assert.AreEqual(42, result)
        End Sub

        <TestMethod>
        Public Sub TestListExpansionPadding_Enabled()
            TestListExpansionPadding(True)
        End Sub

        <TestMethod>
        Public Sub TestListExpansionPadding_Disabled()
            TestListExpansionPadding(False)
        End Sub

        Private Sub TestListExpansionPadding(ByVal enabled As Boolean)
            Dim oldVal As Boolean = SqlMapper.Settings.PadListExpansions

            Try
                SqlMapper.Settings.PadListExpansions = enabled
                Assert.AreEqual(4096, Connection.ExecuteScalar(Of Integer)("
create table #ListExpansion(id int not null identity(1,1), value int null);
insert #ListExpansion (value) values (null);
declare @loop int = 0;
while (@loop < 12)
begin -- double it
	insert #ListExpansion (value) select value from #ListExpansion;
	set @loop = @loop + 1;
end

select count(1) as [Count] from #ListExpansion"))
                Dim list = New List(Of Integer)()
                Dim batchCount As Integer, nextId As Integer = 1
                Dim rand = New Random(12345)
                Const SQL_SERVER_MAX_PARAMS As Integer = 2095
                TestListForExpansion(list, enabled)

                While list.Count < SQL_SERVER_MAX_PARAMS

                    Try

                        If list.Count <= 20 Then
                            batchCount = 1
                        ElseIf list.Count <= 200 Then
                            batchCount = rand.[Next](1, 40)
                        Else
                            batchCount = rand.[Next](1, 100)
                        End If

                        Dim j As Integer = 0

                        While j < batchCount AndAlso list.Count < SQL_SERVER_MAX_PARAMS
                            list.Add(Math.Min(System.Threading.Interlocked.Increment(nextId), nextId - 1))
                            j += 1
                        End While

                        TestListForExpansion(list, enabled)
                    Catch ex As Exception
                        Throw New InvalidOperationException($"Failure with {list.Count} items: {ex.Message}", ex)
                    End Try
                End While

            Finally
                SqlMapper.Settings.PadListExpansions = oldVal
            End Try
        End Sub

        Private Sub TestListForExpansion(ByVal list As List(Of Integer), ByVal enabled As Boolean)
            Dim row = Connection.QuerySingle("
declare @hits int, @misses int, @count int;
select @count = count(1) from #ListExpansion;
select @hits = count(1) from #ListExpansion where id in @ids ;
select @misses = count(1) from #ListExpansion where not id in @ids ;
declare @query nvarchar(max) = N' in @ids '; -- ok, I confess to being pleased with this hack ;p
select @hits as [Hits], (@count - @misses) as [Misses], @query as [Query];
", New With {Key .ids = list})
            Dim hits As Integer = row.Hits, misses As Integer = row.Misses
            Dim query As String = row.Query
            Dim argCount As Integer = Regex.Matches(query, "@ids[0-9]").Count
            Dim expectedCount As Integer = GetExpectedListExpansionCount(list.Count, enabled)
            Assert.AreEqual(hits, list.Count)
            Assert.AreEqual(misses, list.Count)
            Assert.AreEqual(argCount, expectedCount)
        End Sub

        Private Shared Function GetExpectedListExpansionCount(ByVal count As Integer, ByVal enabled As Boolean) As Integer
            If Not enabled Then Return count
            If count <= 5 OrElse count > 2070 Then Return count
            Dim padFactor As Integer

            If count <= 150 Then
                padFactor = 10
            ElseIf count <= 750 Then
                padFactor = 50
            ElseIf count <= 2000 Then
                padFactor = 100
            ElseIf count <= 2070 Then
                padFactor = 10
            Else
                padFactor = 200
            End If
            Dim blocks As Integer = CInt(count / padFactor), delta As Integer = count Mod padFactor
            If delta <> 0 Then blocks += 1
            Return blocks * padFactor
        End Function

        Private Class CSharpImpl
            Shared Function __Assign(Of T)(ByRef target As T, value As T) As T
                target = value
                Return value
            End Function
        End Class
    End Class
End Namespace
