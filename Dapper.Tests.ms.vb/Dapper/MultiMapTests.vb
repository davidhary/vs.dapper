﻿Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper
Namespace DapperTests
	Public Class MultiMapTests
		Inherits TestBase

		<TestMethod> _
		Public Sub ParentChildIdentityAssociations()
            Dim lookup As New Dictionary(Of Integer, Parent)()
            Dim found As Parent = Nothing
            Dim parents As Dictionary(Of Integer, Parent) = Me.Connection.Query(Of Parent, Child, Parent)("select 1 as [Id], 1 as [Id] union all select 1,2 union all select 2,3 union all select 1,4 union all select 3,5",
                                                                                               Function(parent, child)
                                                                                                   If Not lookup.TryGetValue(parent.Id, found) Then
                                                                                                       'INSTANT VB TODO TASK: Assignments within expressions are not supported in VB
                                                                                                       'ORIGINAL LINE: lookup.Add(parent.Id, found = parent);
                                                                                                       lookup.Add(parent.Id, found)
                                                                                                   End If
                                                                                                   found.Children.Add(child)
                                                                                                   Return found
                                                                                               End Function).Distinct().ToDictionary(Function(p) p.Id)
            Assert.AreEqual(3, parents.Count)
            Assert.IsTrue(parents(1).Children.Select(Function(c) c.Id).SequenceEqual({1, 2, 4}))
            Assert.IsTrue(parents(2).Children.Select(Function(c) c.Id).SequenceEqual({3}))
            Assert.IsTrue(parents(3).Children.Select(Function(c) c.Id).SequenceEqual({5}))
        End Sub

        Private Class Parent
            Public Property Id() As Integer
            Public ReadOnly Children As New List(Of Child)()
        End Class

        Private Class Child
            Public Property Id() As Integer
        End Class

        <TestMethod>
        Public Sub TestMultiMap()
            Const createSql As String = String.Empty & ControlChars.CrLf & "                create table #Users (Id int, Name varchar(20))" & ControlChars.CrLf & "                create table #Posts (Id int, OwnerId int, Content varchar(20))" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Users values(99, 'Sam')" & ControlChars.CrLf & "                insert #Users values(2, 'I am')" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Posts values(1, 99, 'Sams Post1')" & ControlChars.CrLf & "                insert #Posts values(2, 99, 'Sams Post2')" & ControlChars.CrLf & "                insert #Posts values(3, null, 'no ones post')" & ControlChars.CrLf & ""
            Connection.Execute(createSql)
            Try
                Const sql As String = "select * from #Posts p " & ControlChars.CrLf & "left join #Users u on u.Id = p.OwnerId " & ControlChars.CrLf & "Order by p.Id"

                Dim data As IEnumerable(Of Post) = Me.Connection.Query(Of Post, User, Post)(sql, Function(post, user)
                                                                                                     post.Owner = user
                                                                                                     Return post
                                                                                                 End Function).ToList()
                Dim p As Post = data(0)
                Assert.AreEqual("Sams Post1", p.Content)
                Assert.AreEqual(1, p.Id)
                Assert.AreEqual("Sam", p.Owner.Name)
                Assert.AreEqual(99, p.Owner.Id)
                Assert.IsNull(data(2).Owner)
            Finally
                Connection.Execute("drop table #Users drop table #Posts")
            End Try
        End Sub

        <TestMethod>
        Public Sub TestSchemaChangedMultiMap()
            Connection.Execute("create table #dog(Age int, Name nvarchar(max)) insert #dog values(1, 'Alf')")
            Try
                Dim tuple As Tuple(Of Dog, Dog) = Me.Connection.Query(Of Dog, Dog, Tuple(Of Dog, Dog))("select * from #dog d1 join #dog d2 on 1=1", AddressOf System.Tuple.Create, splitOn:="Age").Single()

                Assert.AreEqual("Alf", tuple.Item1.Name)
                Assert.AreEqual(1, tuple.Item1.Age)
                Assert.AreEqual("Alf", tuple.Item2.Name)
                Assert.AreEqual(1, tuple.Item2.Age)

                Connection.Execute("alter table #dog drop column Name")
                tuple = Me.Connection.Query(Of Dog, Dog, Tuple(Of Dog, Dog))("select * from #dog d1 join #dog d2 on 1=1", AddressOf System.Tuple.Create, splitOn:="Age").Single()
                Assert.IsNull(tuple.Item1.Name)
                Assert.AreEqual(1, tuple.Item1.Age)
                Assert.IsNull(tuple.Item2.Name)
                Assert.AreEqual(1, tuple.Item2.Age)
            Finally
                Connection.Execute("drop table #dog")
            End Try
        End Sub

        <TestMethod>
        Public Sub TestReadMultipleIntegersWithSplitOnAny()
            Assert.AreEqual({Tuple.Create(1, 2, 3), Tuple.Create(4, 5, 6)}, Connection.Query(Of Integer, Integer, Integer, Tuple(Of Integer, Integer, Integer))("select 1,2,3 union all select 4,5,6",
                                                                                                                                                                AddressOf Tuple.Create, splitOn:="*"))
        End Sub

        Private Class Multi1
            Public Property Id() As Integer
        End Class

        Private Class Multi2
            Public Property Id() As Integer
        End Class

        <TestMethod>
        Public Sub QueryMultimapFromClosed()
            Using conn As IDbConnection = Me.GetClosedConnection()
                Assert.AreEqual(ConnectionState.Closed, conn.State)
                Dim i As Integer = conn.Query(Of Multi1, Multi2, Integer)("select 2 as [Id], 3 as [Id]", Function(x, y) x.Id + y.Id).Single()
                Assert.AreEqual(ConnectionState.Closed, conn.State)
                Assert.AreEqual(5, i)
            End Using
        End Sub

        <TestMethod>
        Public Sub TestMultiMapThreeTypesWithGridReader()
            Const createSql As String = String.Empty & ControlChars.CrLf & "                create table #Users (Id int, Name varchar(20))" & ControlChars.CrLf & "                create table #Posts (Id int, OwnerId int, Content varchar(20))" & ControlChars.CrLf & "                create table #Comments (Id int, PostId int, CommentData varchar(20))" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Users values(99, 'Sam')" & ControlChars.CrLf & "                insert #Users values(2, 'I am')" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Posts values(1, 99, 'Sams Post1')" & ControlChars.CrLf & "                insert #Posts values(2, 99, 'Sams Post2')" & ControlChars.CrLf & "                insert #Posts values(3, null, 'no ones post')" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Comments values(1, 1, 'Comment 1')"
            Connection.Execute(createSql)
            Try
                Const sql As String = "SELECT p.* FROM #Posts p" & ControlChars.CrLf & ControlChars.CrLf & "select p.*, u.Id, u.Name + '0' Name, c.Id, c.CommentData from #Posts p " & ControlChars.CrLf & "left join #Users u on u.Id = p.OwnerId " & ControlChars.CrLf & "left join #Comments c on c.PostId = p.Id" & ControlChars.CrLf & "where p.Id = 1" & ControlChars.CrLf & "Order by p.Id"

                Dim grid As SqlMapper.GridReader = Me.Connection.QueryMultiple(sql)

                Dim post1 As IEnumerable(Of Post) = grid.Read(Of Post)().ToList()

                Dim post2 As Post = grid.Read(Of Post, User, Comment, Post)(Function(post, user, comment)
                                                                                post.Owner = user
                                                                                post.Comment = comment
                                                                                Return post
                                                                            End Function).SingleOrDefault()
                Assert.AreEqual(1, post2.Comment.Id)
                Assert.AreEqual(99, post2.Owner.Id)
            Finally
                Connection.Execute("drop table #Users drop table #Posts drop table #Comments")
            End Try
        End Sub

        <TestMethod>
        Public Sub TestMultiMapperIsNotConfusedWithUnorderedCols()
            Dim result As Tuple(Of Foo1, Bar1) = Me.Connection.Query(Of Foo1, Bar1, Tuple(Of Foo1, Bar1))("select 1 as Id, 2 as BarId, 3 as BarId, 'a' as Name",
                                                                                                       AddressOf Tuple.Create, splitOn:="BarId").First()
            Assert.AreEqual(1, result.Item1.Id)
            Assert.AreEqual(2, result.Item1.BarId)
            Assert.AreEqual(3, result.Item2.BarId)
            Assert.AreEqual("a", result.Item2.Name)
        End Sub

        <TestMethod>
        Public Sub TestMultiMapDynamic()
            Const createSql As String = String.Empty & ControlChars.CrLf & "                create table #Users (Id int, Name varchar(20))" & ControlChars.CrLf & "                create table #Posts (Id int, OwnerId int, Content varchar(20))" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Users values(99, 'Sam')" & ControlChars.CrLf & "                insert #Users values(2, 'I am')" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Posts values(1, 99, 'Sams Post1')" & ControlChars.CrLf & "                insert #Posts values(2, 99, 'Sams Post2')" & ControlChars.CrLf & "                insert #Posts values(3, null, 'no ones post')" & ControlChars.CrLf & ""
            Connection.Execute(createSql)

            Const sql As String = "select * from #Posts p " & ControlChars.CrLf & "left join #Users u on u.Id = p.OwnerId " & ControlChars.CrLf & "Order by p.Id"

            Dim data As IEnumerable(Of Post) = Me.Connection.Query(Of Post, User, Post)(sql, Function(post, user)
                                                                                                 post.Owner = user
                                                                                                 Return post
                                                                                             End Function).ToList()
            Dim p As Post = data(0)

            ' hairy extension method support for dynamics
            Assert.AreEqual("Sams Post1", CStr(p.Content))
            Assert.AreEqual(1, CInt(Fix(p.Id)))
            Assert.AreEqual("Sam", CStr(p.Owner.Name))
            Assert.AreEqual(99, CInt(Fix(p.Owner.Id)))

            Assert.IsNull(CObj(data(2).Owner))

            Connection.Execute("drop table #Users drop table #Posts")
        End Sub

        ''' <summary> (Unit Test Method) tests multi map with split. </summary>
        ''' <remarks>https://stackoverflow.com/q/6056778/23354</remarks>
        <TestMethod>
        Public Sub TestMultiMapWithSplit()
            Const sql As String = "select 1 as id, 'abc' as name, 2 as id, 'def' as name"
            Dim product As Product = Me.Connection.Query(Of Product, Category, Product)(sql, Function(prod, cat)
                                                                                                 prod.Category = cat
                                                                                                 Return prod
                                                                                             End Function).First()
            ' assertions
            Assert.AreEqual(1, product.Id)
            Assert.AreEqual("abc", product.Name)
            Assert.AreEqual(2, product.Category.Id)
            Assert.AreEqual("def", product.Category.Name)
        End Sub

        <TestMethod>
        Public Sub TestMultiMapWithSplitWithNullValue() ' https://stackoverflow.com/q/10744728/449906
            Const sql As String = "select 1 as id, 'abc' as name, NULL as description, 'def' as name"
            Dim product As Product = Me.Connection.Query(Of Product, Category, Product)(sql, Function(prod, cat)
                                                                                                 prod.Category = cat
                                                                                                 Return prod
                                                                                             End Function, splitOn:="description").First()
            ' assertions
            Assert.AreEqual(1, product.Id)
            Assert.AreEqual("abc", product.Name)
            Assert.IsNull(product.Category)
        End Sub

        <TestMethod>
        Public Sub TestMultiMapWithSplitWithNullValueAndSpoofColumn() ' https://stackoverflow.com/q/10744728/449906
            Const sql As String = "select 1 as id, 'abc' as name, 1 as spoof, NULL as description, 'def' as name"
            Dim product As Product = Me.Connection.Query(Of Product, Category, Product)(sql, Function(prod, cat)
                                                                                                 prod.Category = cat
                                                                                                 Return prod
                                                                                             End Function, splitOn:="spoof").First()
            ' assertions
            Assert.AreEqual(1, product.Id)
            Assert.AreEqual("abc", product.Name)
            Assert.IsNotNull(product.Category)
            Assert.AreEqual(0, product.Category.Id)
            Assert.AreEqual("def", product.Category.Name)
            Assert.IsNull(product.Category.Description)
        End Sub

#If False Then
        <TestMethod>
        Public Sub TestMultiMappingVariations()
            Const sql As String = "select 1 as Id, 'a' as Content, 2 as Id, 'b' as Content, 3 as Id, 'c' as Content, 4 as Id, 'd' as Content, 5 as Id, 'e' as Content"

            Dim order As Post = Me.Connection.Query(Of Product, Person, Person, Product)(sql, Function(o, owner, creator)
                                                                                                  o.Owner = owner
                                                                                                  o.Creator = creator
                                                                                                  Return o
                                                                                              End Function).First()

            Assert.AreEqual(order.Id, 1)
            Assert.AreEqual(order.Content, "a")
            Assert.AreEqual(order.Owner.Id, 2)
            Assert.AreEqual(order.Owner.Content, "b")
            Assert.AreEqual(order.Creator.Id, 3)
            Assert.AreEqual(order.Creator.Content, "c")

            order = Me.Connection.Query(Of Dynamic, Dynamic, Dynamic, Dynamic, Dynamic)(sql, Function(o, owner, creator, address)
                                                                                                 o.Owner = owner
                                                                                                 o.Creator = creator
                                                                                                 o.Owner.Address = address
                                                                                                 Return o
                                                                                             End Function).First()

            Assert.AreEqual(order.Id, 1)
            Assert.AreEqual(order.Content, "a")
            Assert.AreEqual(order.Owner.Id, 2)
            Assert.AreEqual(order.Owner.Content, "b")
            Assert.AreEqual(order.Creator.Id, 3)
            Assert.AreEqual(order.Creator.Content, "c")
            Assert.AreEqual(order.Owner.Address.Id, 4)
            Assert.AreEqual(order.Owner.Address.Content, "d")

            order = Me.Connection.Query(Of Dynamic, Dynamic, Dynamic, Dynamic, Dynamic, Dynamic)(sql, Function(a, b, c, d, e)
                                                                                                          a.B = b
                                                                                                          a.C = c
                                                                                                          a.C.D = d
                                                                                                          a.E = e
                                                                                                          Return a
                                                                                                      End Function).First()

            Assert.AreEqual(order.Id, 1)
            Assert.AreEqual(order.Content, "a")
            Assert.AreEqual(order.B.Id, 2)
            Assert.AreEqual(order.B.Content, "b")
            Assert.AreEqual(order.C.Id, 3)
            Assert.AreEqual(order.C.Content, "c")
            Assert.AreEqual(order.C.D.Id, 4)
            Assert.AreEqual(order.C.D.Content, "d")
            Assert.AreEqual(order.E.Id, 5)
            Assert.AreEqual(order.E.Content, "e")
        End Sub
#End If

        Private Class UserWithConstructor
            Public Sub New(ByVal id As Integer, ByVal name As String)
                Ident = id
                FullName = name
            End Sub

            Public Property Ident() As Integer
            Public Property FullName() As String
        End Class

        Private Class PostWithConstructor
            Public Sub New(ByVal id As Integer, ByVal ownerid As Integer, ByVal content As String)
                Ident = id
                FullContent = content
            End Sub
            Public Property Ident() As Integer
            Public Property Owner() As UserWithConstructor
            Public Property FullContent() As String
            Public Property Comment() As Comment
        End Class

        <TestMethod>
        Public Sub TestMultiMapWithConstructor()
            Const createSql As String = String.Empty & ControlChars.CrLf & "                create table #Users (Id int, Name varchar(20))" & ControlChars.CrLf & "                create table #Posts (Id int, OwnerId int, Content varchar(20))" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Users values(99, 'Sam')" & ControlChars.CrLf & "                insert #Users values(2, 'I am')" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Posts values(1, 99, 'Sams Post1')" & ControlChars.CrLf & "                insert #Posts values(2, 99, 'Sams Post2')" & ControlChars.CrLf & "                insert #Posts values(3, null, 'no ones post')"
            Connection.Execute(createSql)
            Try
                Const sql As String = "select * from #Posts p " & ControlChars.CrLf & "                           left join #Users u on u.Id = p.OwnerId " & ControlChars.CrLf & "                           Order by p.Id"
                Dim data() As PostWithConstructor = Me.Connection.Query(Of PostWithConstructor, UserWithConstructor, PostWithConstructor)(sql, Function(post, user)
                                                                                                                                                   post.Owner = user
                                                                                                                                                   Return post
                                                                                                                                               End Function).ToArray()
                Dim p As PostWithConstructor = data.First()
                Assert.AreEqual("Sams Post1", p.FullContent)
                Assert.AreEqual(1, p.Ident)
                Assert.AreEqual("Sam", p.Owner.FullName)
                Assert.AreEqual(99, p.Owner.Ident)
                Assert.IsNull(data(2).Owner)
            Finally
                Connection.Execute("drop table #Users drop table #Posts")
            End Try
        End Sub

        <TestMethod>
        Public Sub TestMultiMapArbitraryMaps()
            ' please excuse the trite example, but it is easier to follow than a more real-world one
            Const createSql As String = String.Empty & ControlChars.CrLf & "                create table #ReviewBoards (Id int, Name varchar(20), User1Id int, User2Id int, User3Id int, User4Id int, User5Id int, User6Id int, User7Id int, User8Id int, User9Id int)" & ControlChars.CrLf & "                create table #Users (Id int, Name varchar(20))" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Users values(1, 'User 1')" & ControlChars.CrLf & "                insert #Users values(2, 'User 2')" & ControlChars.CrLf & "                insert #Users values(3, 'User 3')" & ControlChars.CrLf & "                insert #Users values(4, 'User 4')" & ControlChars.CrLf & "                insert #Users values(5, 'User 5')" & ControlChars.CrLf & "                insert #Users values(6, 'User 6')" & ControlChars.CrLf & "                insert #Users values(7, 'User 7')" & ControlChars.CrLf & "                insert #Users values(8, 'User 8')" & ControlChars.CrLf & "                insert #Users values(9, 'User 9')" & ControlChars.CrLf & ControlChars.CrLf & "                insert #ReviewBoards values(1, 'Review Board 1', 1, 2, 3, 4, 5, 6, 7, 8, 9)" & ControlChars.CrLf & ""
            Connection.Execute(createSql)
            Try
                Const sql As String = String.Empty & ControlChars.CrLf & "                    select " & ControlChars.CrLf & "                        rb.Id, rb.Name," & ControlChars.CrLf & "                        u1.*, u2.*, u3.*, u4.*, u5.*, u6.*, u7.*, u8.*, u9.*" & ControlChars.CrLf & "                    from #ReviewBoards rb" & ControlChars.CrLf & "                        inner join #Users u1 on u1.Id = rb.User1Id" & ControlChars.CrLf & "                        inner join #Users u2 on u2.Id = rb.User2Id" & ControlChars.CrLf & "                        inner join #Users u3 on u3.Id = rb.User3Id" & ControlChars.CrLf & "                        inner join #Users u4 on u4.Id = rb.User4Id" & ControlChars.CrLf & "                        inner join #Users u5 on u5.Id = rb.User5Id" & ControlChars.CrLf & "                        inner join #Users u6 on u6.Id = rb.User6Id" & ControlChars.CrLf & "                        inner join #Users u7 on u7.Id = rb.User7Id" & ControlChars.CrLf & "                        inner join #Users u8 on u8.Id = rb.User8Id" & ControlChars.CrLf & "                        inner join #Users u9 on u9.Id = rb.User9Id" & ControlChars.CrLf & ""

                Dim types As Type() = {GetType(ReviewBoard), GetType(User), GetType(User), GetType(User), GetType(User), GetType(User), GetType(User), GetType(User), GetType(User), GetType(User)}

                Dim mapper As Func(Of Object(), ReviewBoard) = Function(objects)
                                                                   Dim board As ReviewBoard = CType(objects(0), ReviewBoard)
                                                                   board.User1 = CType(objects(1), User)
                                                                   board.User2 = CType(objects(2), User)
                                                                   board.User3 = CType(objects(3), User)
                                                                   board.User4 = CType(objects(4), User)
                                                                   board.User5 = CType(objects(5), User)
                                                                   board.User6 = CType(objects(6), User)
                                                                   board.User7 = CType(objects(7), User)
                                                                   board.User8 = CType(objects(8), User)
                                                                   board.User9 = CType(objects(9), User)
                                                                   Return board
                                                               End Function

                Dim data As IEnumerable(Of ReviewBoard) = Me.Connection.Query(Of ReviewBoard)(sql, types, mapper).ToList()

                Dim p As ReviewBoard = data(0)
                Assert.AreEqual(1, p.Id)
                Assert.AreEqual("Review Board 1", p.Name)
                Assert.AreEqual(1, p.User1.Id)
                Assert.AreEqual(2, p.User2.Id)
                Assert.AreEqual(3, p.User3.Id)
                Assert.AreEqual(4, p.User4.Id)
                Assert.AreEqual(5, p.User5.Id)
                Assert.AreEqual(6, p.User6.Id)
                Assert.AreEqual(7, p.User7.Id)
                Assert.AreEqual(8, p.User8.Id)
                Assert.AreEqual(9, p.User9.Id)
                Assert.AreEqual("User 1", p.User1.Name)
                Assert.AreEqual("User 2", p.User2.Name)
                Assert.AreEqual("User 3", p.User3.Name)
                Assert.AreEqual("User 4", p.User4.Name)
                Assert.AreEqual("User 5", p.User5.Name)
                Assert.AreEqual("User 6", p.User6.Name)
                Assert.AreEqual("User 7", p.User7.Name)
                Assert.AreEqual("User 8", p.User8.Name)
                Assert.AreEqual("User 9", p.User9.Name)
            Finally
                Connection.Execute("drop table #Users drop table #ReviewBoards")
            End Try
        End Sub

        <TestMethod>
        Public Sub TestMultiMapGridReader()
            Const createSql As String = String.Empty & ControlChars.CrLf & "                create table #Users (Id int, Name varchar(20))" & ControlChars.CrLf & "                create table #Posts (Id int, OwnerId int, Content varchar(20))" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Users values(99, 'Sam')" & ControlChars.CrLf & "                insert #Users values(2, 'I am')" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Posts values(1, 99, 'Sams Post1')" & ControlChars.CrLf & "                insert #Posts values(2, 99, 'Sams Post2')" & ControlChars.CrLf & "                insert #Posts values(3, null, 'no ones post')" & ControlChars.CrLf & ""
            Connection.Execute(createSql)

            Const sql As String = "select p.*, u.Id, u.Name + '0' Name from #Posts p " & ControlChars.CrLf & "left join #Users u on u.Id = p.OwnerId " & ControlChars.CrLf & "Order by p.Id" & ControlChars.CrLf & ControlChars.CrLf & "select p.*, u.Id, u.Name + '1' Name from #Posts p " & ControlChars.CrLf & "left join #Users u on u.Id = p.OwnerId " & ControlChars.CrLf & "Order by p.Id" & ControlChars.CrLf & ""

            Dim grid As SqlMapper.GridReader = Me.Connection.QueryMultiple(sql)

            For i As Integer = 0 To 1
                Dim data As IEnumerable(Of Post) = grid.Read(Of Post, User, Post)(Function(post, user)
                                                                                      post.Owner = user
                                                                                      Return post
                                                                                  End Function).ToList()
                Dim p As Post = data(0)
                Assert.AreEqual("Sams Post1", p.Content)
                Assert.AreEqual(1, p.Id)
                Assert.AreEqual(p.Owner.Name, "Sam" & i)
                Assert.AreEqual(99, p.Owner.Id)
                Assert.IsNull(data(2).Owner)
            Next i

            Connection.Execute("drop table #Users drop table #Posts")
        End Sub

        <TestMethod>
        Public Sub TestFlexibleMultiMapping()
            Const sql As String = "select " & ControlChars.CrLf & "    1 as PersonId, 'bob' as Name, " & ControlChars.CrLf & "    2 as AddressId, 'abc street' as Name, 1 as PersonId," & ControlChars.CrLf & "    3 as Id, 'fred' as Name" & ControlChars.CrLf & "    "
            Dim personWithAddress As Tuple(Of Person, Address, Extra) = Me.Connection.Query(Of Person, Address, Extra, Tuple(Of Person, Address, Extra))(sql, AddressOf Tuple.Create, splitOn:="AddressId,Id").First()

            Assert.AreEqual(1, personWithAddress.Item1.PersonId)
            Assert.AreEqual("bob", personWithAddress.Item1.Name)
            Assert.AreEqual(2, personWithAddress.Item2.AddressId)
            Assert.AreEqual("abc street", personWithAddress.Item2.Name)
            Assert.AreEqual(1, personWithAddress.Item2.PersonId)
            Assert.AreEqual(3, personWithAddress.Item3.Id)
            Assert.AreEqual("fred", personWithAddress.Item3.Name)
        End Sub

        <TestMethod>
        Public Sub TestMultiMappingWithSplitOnSpaceBetweenCommas()
            Const sql As String = "select " & ControlChars.CrLf & "                        1 as PersonId, 'bob' as Name, " & ControlChars.CrLf & "                        2 as AddressId, 'abc street' as Name, 1 as PersonId," & ControlChars.CrLf & "                        3 as Id, 'fred' as Name" & ControlChars.CrLf & "                        "
            Dim personWithAddress As Tuple(Of Person, Address, Extra) = Me.Connection.Query(Of Person, Address, Extra, Tuple(Of Person, Address, Extra))(sql, AddressOf Tuple.Create, splitOn:="AddressId, Id").First()

            Assert.AreEqual(1, personWithAddress.Item1.PersonId)
            Assert.AreEqual("bob", personWithAddress.Item1.Name)
            Assert.AreEqual(2, personWithAddress.Item2.AddressId)
            Assert.AreEqual("abc street", personWithAddress.Item2.Name)
            Assert.AreEqual(1, personWithAddress.Item2.PersonId)
            Assert.AreEqual(3, personWithAddress.Item3.Id)
            Assert.AreEqual("fred", personWithAddress.Item3.Name)
        End Sub

        Private Class Extra
            Public Property Id() As Integer
            Public Property Name() As String
        End Class

        <TestMethod>
        Public Sub TestMultiMappingWithNonReturnedProperty()
            Const sql As String = "select " & ControlChars.CrLf & "                            1 as PostId, 'Title' as Title," & ControlChars.CrLf & "                            2 as BlogId, 'Blog' as Title"
            Dim postWithBlog As Post_DupeProp = Me.Connection.Query(Of Post_DupeProp, Blog_DupeProp, Post_DupeProp)(sql, Function(p, b)
                                                                                                                             p.Blog = b
                                                                                                                             Return p
                                                                                                                         End Function, splitOn:="BlogId").First()

            Assert.AreEqual(1, postWithBlog.PostId)
            Assert.AreEqual("Title", postWithBlog.Title)
            Assert.AreEqual(2, postWithBlog.Blog.BlogId)
            Assert.AreEqual("Blog", postWithBlog.Blog.Title)
        End Sub

        Private Class Post_DupeProp
            Public Property PostId() As Integer
            Public Property Title() As String
            Public Property BlogId() As Integer
            Public Property Blog() As Blog_DupeProp
        End Class

        Private Class Blog_DupeProp
            Public Property BlogId() As Integer
            Public Property Title() As String
        End Class


        ''' <summary> (Unit Test Method) tests split with missing members. </summary>
        ''' <remarks>https://stackoverflow.com/questions/16955357/issue-about-dapper</remarks>
        <TestMethod>
        Public Sub TestSplitWithMissingMembers()
            Dim result As Topic = Me.Connection.Query(Of Topic, Profile, Topic)("select 123 as ID, 'abc' as Title," & ControlChars.CrLf & "                     cast('01 Feb 2013' as datetime) as CreateDate," & ControlChars.CrLf & "                     'ghi' as Name, 'def' as Phone",
                                                                                Function(T, P)
                                                                                    T.Author = P
                                                                                    Return T
                                                                                End Function, Nothing, Nothing, True, "ID,Name").Single()

            Assert.AreEqual(123, result.ID)
            Assert.AreEqual("abc", result.Title)
            Assert.AreEqual(New Date(2013, 2, 1), result.CreateDate)
            Assert.IsNull(result.Name)
            Assert.IsNull(result.Content)

            Assert.AreEqual("def", result.Author.Phone)
            Assert.AreEqual("ghi", result.Author.Name)
            Assert.AreEqual(0, result.Author.ID)
            Assert.IsNull(result.Author.Address)
        End Sub

        Public Class Profile
			Public Property ID() As Integer
			Public Property Name() As String
			Public Property Phone() As String
			Public Property Address() As String
			'public ExtraInfo Extra { get; set; }
		End Class

		Public Class Topic
			Public Property ID() As Integer
			Public Property Title() As String
            Public Property CreateDate() As DateTime
            Public Property Content() As String
			Public Property UID() As Integer
			Public Property TestColum() As Integer
			Public Property Name() As String
			Public Property Author() As Profile
			'public Attachment Attach { get; set; }
		End Class

		<TestMethod> _
		Public Sub TestInvalidSplitCausesNiceError()
			Try
				connection.Query(Of User, User, User)("select 1 A, 2 B, 3 C", Function(x, y) x)
			Catch e1 As ArgumentException
				' expecting an app exception due to multi mapping being bodged 
			End Try
            Try
                Connection.Query(Of User, User, User)("select 1 A, 2 B, 3 C", Function(x, y) x)
            Catch e2 As ArgumentException
                ' expecting an app exception due to multi mapping being bodged 
            End Try
		End Sub
	End Class
End Namespace
