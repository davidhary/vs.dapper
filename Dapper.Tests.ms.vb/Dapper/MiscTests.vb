﻿Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper

Namespace DapperTests
	Public Class MiscTests
		Inherits TestBase

		<TestMethod> _
		Public Sub TestNullableGuidSupport()
            Dim guid As Guid? = Me.Connection.Query(Of Guid?)("select null").First()
            Assert.IsNull(guid)

            guid = System.Guid.NewGuid()
            Dim guid2 As Guid? = Me.Connection.Query(Of Guid?)("select @guid", New With {Key guid}).First()
            Assert.AreEqual(guid, guid2)
        End Sub

        <TestMethod>
        Public Sub TestNonNullableGuidSupport()
            Dim guid As Guid = System.Guid.NewGuid()
            Dim guid2 As Guid? = Me.Connection.Query(Of Guid?)("select @guid", New With {Key guid}).First()
            Assert.IsTrue(Nullable.Equals(guid, guid2))
        End Sub

        Private Structure Car
            Public Enum TrapEnum As Integer
                A = 1
                B = 2
            End Enum
            Public Property Name As String
            Public Property Age() As Integer
            Public Property Trap() As TrapEnum
        End Structure

        Private Structure CarWithAllProps
            Public Property Name() As String
            Public Property Age() As Integer
            Public Property Trap() As Car.TrapEnum
        End Structure

        <TestMethod>
        Public Sub TestStructs()
            Dim car As Car = Me.Connection.Query(Of Car)("select 'Ford' Name, 21 Age, 2 Trap").First()
            Assert.AreEqual(21, car.Age)
            Assert.AreEqual("Ford", car.Name)
            Assert.AreEqual(2, CInt(Fix(car.Trap)))
        End Sub

        <TestMethod>
        Public Sub TestStructAsParam()
            Dim car1 As New CarWithAllProps With {.Name = "Ford", .Age = 21, .Trap = Car.TrapEnum.B}
            ' note Car has Name as a field; parameters only respect properties at the moment
            Dim car2 As CarWithAllProps = Me.Connection.Query(Of CarWithAllProps)("select @Name Name, @Age Age, @Trap Trap", car1).First()
            Assert.AreEqual(car2.Name, car1.Name)
            Assert.AreEqual(car2.Age, car1.Age)
            Assert.AreEqual(car2.Trap, car1.Trap)
        End Sub

        <TestMethod>
        Public Sub SelectListInt()
            Assert.AreEqual({1, 2, 3}, Connection.Query(Of Integer)("select 1 union all select 2 union all select 3"))
        End Sub

        <TestMethod>
        Public Sub SelectBinary()
            Connection.Query(Of Byte())("select cast(1 as varbinary(4))").First().SequenceEqual(New Byte() {1})
        End Sub

        <TestMethod>
        Public Sub TestSchemaChanged()
            Me.Connection.Execute("create table #dog(Age int, Name nvarchar(max)) insert #dog values(1, 'Alf')")
            Try
                Dim d As Dog = Me.Connection.Query(Of Dog)("select * from #dog").Single()
                Assert.AreEqual("Alf", d.Name)
                Assert.AreEqual(1, d.Age)
                Me.Connection.Execute("alter table #dog drop column Name")
                d = Me.Connection.Query(Of Dog)("select * from #dog").Single()
                Assert.IsNull(d.Name)
                Assert.AreEqual(1, d.Age)
            Finally
                Me.Connection.Execute("drop table #dog")
            End Try
        End Sub

        <TestMethod>
        Public Sub TestSchemaChangedViaFirstOrDefault()
            Connection.Execute("create table #dog(Age int, Name nvarchar(max)) insert #dog values(1, 'Alf')")
            Try
                Dim d As Dog = Me.Connection.QueryFirstOrDefault(Of Dog)("select * from #dog")
                Assert.AreEqual("Alf", d.Name)
                Assert.AreEqual(1, d.Age)
                Connection.Execute("alter table #dog drop column Name")
                d = Me.Connection.QueryFirstOrDefault(Of Dog)("select * from #dog")
                Assert.IsNull(d.Name)
                Assert.AreEqual(1, d.Age)
            Finally
                Connection.Execute("drop table #dog")
            End Try
        End Sub

        <TestMethod>
        Public Sub Test_Single_First_Default()
            Dim sql As String = "select 0 where 1 = 0;" ' no rows

            Dim ex As Exception = MyAssert.Throws(Of InvalidOperationException)(Function() Connection.QueryFirst(Of Integer)(sql))
            Assert.AreEqual("Sequence contains no elements", ex.Message)

            ex = MyAssert.Throws(Of InvalidOperationException)(Function() Connection.QuerySingle(Of Integer)(sql))
            Assert.AreEqual("Sequence contains no elements", ex.Message)

            Assert.AreEqual(0, Connection.QueryFirstOrDefault(Of Integer)(sql))
            Assert.AreEqual(0, Connection.QuerySingleOrDefault(Of Integer)(sql))

            sql = "select 1;" ' one row
            Assert.AreEqual(1, Connection.QueryFirst(Of Integer)(sql))
            Assert.AreEqual(1, Connection.QuerySingle(Of Integer)(sql))
            Assert.AreEqual(1, Connection.QueryFirstOrDefault(Of Integer)(sql))
            Assert.AreEqual(1, Connection.QuerySingleOrDefault(Of Integer)(sql))

            sql = "select 2 union select 3 order by 1;" ' two rows
            Assert.AreEqual(2, Connection.QueryFirst(Of Integer)(sql))

            ex = MyAssert.Throws(Of InvalidOperationException)(Function() Connection.QuerySingle(Of Integer)(sql))
            Assert.AreEqual("Sequence contains more than one element", ex.Message)

            Assert.AreEqual(2, Connection.QueryFirstOrDefault(Of Integer)(sql))

            ex = MyAssert.Throws(Of InvalidOperationException)(Function() Connection.QuerySingleOrDefault(Of Integer)(sql))
            Assert.AreEqual("Sequence contains more than one element", ex.Message)
        End Sub

        <TestMethod>
        Public Sub TestStrings()
            Assert.AreEqual({"a", "b"}, Connection.Query(Of String)("select 'a' a union select 'b'"))
        End Sub

        ''' <summary> (Unit Test Method) check complex concatenate. </summary>
        ''' <remarks>
        ''' https://stackoverflow.com/questions/16726709/string-format-with-sql-wildcard-causing-dapper-query-to-break.
        ''' </remarks>
        <TestMethod>
        Public Sub CheckComplexConcat()
            Const end_wildcard As String = String.Empty & ControlChars.CrLf & "SELECT * FROM #users16726709" & ControlChars.CrLf & "WHERE (first_name LIKE CONCAT(@search_term, '%') OR last_name LIKE CONCAT(@search_term, '%'));"

            Const both_wildcards As String = String.Empty & ControlChars.CrLf & "SELECT * FROM #users16726709" & ControlChars.CrLf & "WHERE (first_name LIKE CONCAT('%', @search_term, '%') OR last_name LIKE CONCAT('%', @search_term, '%'));"

            Const formatted As String = String.Empty & ControlChars.CrLf & "SELECT * FROM #users16726709" & ControlChars.CrLf & "WHERE (first_name LIKE {0} OR last_name LIKE {0});"

            Const use_end_only As String = "CONCAT(@search_term, '%')"
            Const use_both As String = "CONCAT('%', @search_term, '%')"

            ' if true, slower query due to not being able to use indices, but will allow searching inside strings 
            Const allow_start_wildcards As Boolean = False

            Dim query As String = String.Format(formatted, If(allow_start_wildcards, use_both, use_end_only))
            Const term As String = "F" ' the term the user searched for

            Connection.Execute("create table #users16726709 (first_name varchar(200), last_name varchar(200))" & ControlChars.CrLf & "insert #users16726709 values ('Fred','Bloggs') insert #users16726709 values ('Tony','Farcus') insert #users16726709 values ('Albert','TenoF')")

            ' Using Dapper
            Assert.AreEqual(2, Connection.Query(end_wildcard, New With {Key .search_term = term}).Count())
            Assert.AreEqual(3, Connection.Query(both_wildcards, New With {Key .search_term = term}).Count())
            Assert.AreEqual(2, Connection.Query(query, New With {Key .search_term = term}).Count())
        End Sub

        <TestMethod>
        Public Sub TestExtraFields()
            Dim guid As Guid = System.Guid.NewGuid()
            Dim dogs As IEnumerable(Of Dog) = Me.Connection.Query(Of Dog)("select '' as Extra, 1 as Age, 0.1 as Name1 , Id = @id", New With {Key .id = guid})
            MyAssert.IsSingle(dogs)
            Assert.AreEqual(1, dogs.First().Age)
            Assert.AreEqual(dogs.First().Id, guid)
        End Sub

        <TestMethod>
        Public Sub TestStrongType()
            Dim guid As Guid = System.Guid.NewGuid()
            Dim dogs As IEnumerable(Of Dog) = Me.Connection.Query(Of Dog)("select Age = @Age, Id = @Id", New With {Key .Age = CType(Nothing, Integer?), Key .Id = guid})
            MyAssert.IsSingle(dogs)
            Assert.IsNull(dogs.First().Age)
            Assert.AreEqual(dogs.First().Id, guid)
        End Sub

        <TestMethod>
        Public Sub TestSimpleNull()
            Assert.IsNull(Connection.Query(Of Date?)("select null").First())
        End Sub

        <TestMethod>
        Public Sub TestExpando()
            Dim rows As IEnumerable(Of Object) = Me.Connection.Query("select 1 A, 2 B union all select 3, 4").ToList()

            Assert.AreEqual(1, CInt(Fix(CType(rows(0), SomeType).A)))
            Assert.AreEqual(2, CInt(Fix(CType(rows(0), SomeType).B)))
            Assert.AreEqual(3, CInt(Fix(CType(rows(1), SomeType).A)))
            Assert.AreEqual(4, CInt(Fix(CType(rows(1), SomeType).B)))
        End Sub

        <TestMethod>
        Public Sub TestStringList()
            Assert.AreEqual({"a", "b", "c"}, Connection.Query(Of String)("select * from (select 'a' as x union all select 'b' union all select 'c') as T where x in @strings", New With {Key .strings = {"a", "b", "c"}}))
            Assert.AreEqual(New String() {}, Connection.Query(Of String)("select * from (select 'a' as x union all select 'b' union all select 'c') as T where x in @strings", New With {Key .strings = New String() {}}))
        End Sub

        <TestMethod>
        Public Sub TestExecuteCommand()
            Assert.AreEqual(2, Connection.Execute("" & ControlChars.CrLf & "    set nocount on " & ControlChars.CrLf & "    create table #t(i int) " & ControlChars.CrLf & "    set nocount off " & ControlChars.CrLf & "    insert #t " & ControlChars.CrLf & "    select @a a union all select @b " & ControlChars.CrLf & "    set nocount on " & ControlChars.CrLf & "    drop table #t", New With {Key .a = 1, Key .b = 2}))
        End Sub

        <TestMethod>
        Public Sub TestExecuteMultipleCommand()
            Connection.Execute("create table #t(i int)")
            Try
                Dim tally As Integer = Me.Connection.Execute("insert #t (i) values(@a)", {New With {Key .a = 1}, New With {Key .a = 2}, New With {Key .a = 3}, New With {Key .a = 4}})
                Dim sum As Integer = Me.Connection.Query(Of Integer)("select sum(i) from #t").First()
                Assert.AreEqual(4, tally)
                Assert.AreEqual(10, sum)
            Finally
                Connection.Execute("drop table #t")
            End Try
        End Sub

        Private Class Student
            Public Property Name() As String
            Public Property Age() As Integer
        End Class

        <TestMethod>
        Public Sub TestExecuteMultipleCommandStrongType()
            Connection.Execute("create table #t(Name nvarchar(max), Age int)")
            Try
                Dim tally As Integer = Me.Connection.Execute("insert #t (Name,Age) values(@Name, @Age)", New List(Of Student) From {
                    New Student With {.Age = 1, .Name = "sam"},
                    New Student With {.Age = 2, .Name = "bob"}})
                Dim sum As Integer = Me.Connection.Query(Of Integer)("select sum(Age) from #t").First()
                Assert.AreEqual(2, tally)
                Assert.AreEqual(3, sum)
            Finally
                Connection.Execute("drop table #t")
            End Try
        End Sub

        <TestMethod>
        Public Sub TestExecuteMultipleCommandObjectArray()
            Connection.Execute("create table #t(i int)")
            Dim tally As Integer = Me.Connection.Execute("insert #t (i) values(@a)", New Object() {New With {Key .a = 1}, New With {Key .a = 2}, New With {Key .a = 3}, New With {Key .a = 4}})
            Dim sum As Integer = Me.Connection.Query(Of Integer)("select sum(i) from #t drop table #t").First()
            Assert.AreEqual(4, tally)
            Assert.AreEqual(10, sum)
        End Sub

        Private Class TestObj
            Public _internal As Integer
            Friend WriteOnly Property Internal() As Integer
                Set(ByVal value As Integer)
                    _internal = value
                End Set
            End Property

            Public _priv As Integer
            Private WriteOnly Property Priv() As Integer
                Set(ByVal value As Integer)
                    _priv = value
                End Set
            End Property
            Private Function PrivGet() As Integer
                Return _priv
            End Function
        End Class

        <TestMethod>
        Public Sub TestSetInternal()
            Assert.AreEqual(10, Connection.Query(Of TestObj)("select 10 as [Internal]").First()._internal)
        End Sub

        <TestMethod>
        Public Sub TestSetPrivate()
            Assert.AreEqual(10, Connection.Query(Of TestObj)("select 10 as [Priv]").First()._priv)
        End Sub

        <TestMethod>
        Public Sub TestExpandWithNullableFields()
            Dim row As Object = Me.Connection.Query("select null A, 2 B").Single()
            Assert.IsNull(CType(CType(row, SomeType).A, Integer?))
            Assert.AreEqual(2, CType(row, SomeType).B)
        End Sub

        <TestMethod>
        Public Sub TestEnumeration()
            Dim en As IEnumerable(Of Integer) = Me.Connection.Query(Of Integer)("select 1 as one union all select 2 as one", buffered:=False)
            Dim i As IEnumerator(Of Integer) = en.GetEnumerator()
            i.MoveNext()

            Dim gotException As Boolean = False
            Try
                Dim x As Integer = Me.Connection.Query(Of Integer)("select 1 as one", buffered:=False).First()
            Catch e1 As Exception
                gotException = True
            End Try

            Do While i.MoveNext()
            Loop

            ' should not exception, since enumerated
            en = Me.Connection.Query(Of Integer)("select 1 as one", buffered:=False)

            Assert.IsTrue(gotException)
        End Sub

        <TestMethod>
        Public Sub TestEnumerationDynamic()
            Dim en As IEnumerable(Of Object) = Me.Connection.Query("select 1 as one union all select 2 as one", buffered:=False)
            Dim i As IEnumerator(Of Object) = en.GetEnumerator()
            i.MoveNext()

            Dim gotException As Boolean = False
            Try
                Dim x As Object = Me.Connection.Query("select 1 as one", buffered:=False).First()
            Catch e1 As Exception
                gotException = True
            End Try

            Do While i.MoveNext()
            Loop

            ' should not exception, since enumerated
            en = Me.Connection.Query("select 1 as one", buffered:=False)

            Assert.IsTrue(gotException)
        End Sub

        <TestMethod>
        Public Sub TestNakedBigInt()
            Const foo As Long = 12345
            Dim result As Long = Me.Connection.Query(Of Long)("select @foo", New With {Key foo}).Single()
            Assert.AreEqual(foo, result)
        End Sub

        <TestMethod>
        Public Sub TestBigIntMember()
            Const foo As Long = 12345
            Dim result As WithBigInt = Me.Connection.Query(Of WithBigInt)("" & ControlChars.CrLf & "declare @bar table(Value bigint)" & ControlChars.CrLf & "insert @bar values (@foo)" & ControlChars.CrLf & "select * from @bar", New With {Key foo}).Single()
            Assert.AreEqual(result.Value, foo)
        End Sub

        Private Class WithBigInt
            Public Property Value() As Long
        End Class

        <TestMethod>
        Public Sub TestFieldsAndPrivates()
            Dim data As TestFieldCaseAndPrivatesEntity = Me.Connection.Query(Of TestFieldCaseAndPrivatesEntity)("select a=1,b=2,c=3,d=4,f='5'").Single()
            Assert.AreEqual(1, data.A)
            Assert.AreEqual(2, data.GetB())
            Assert.AreEqual(3, data.C)
            Assert.AreEqual(4, data.GetD())
            Assert.AreEqual(5, data.E)
        End Sub

        Private Class TestFieldCaseAndPrivatesEntity
            Public Property A() As Integer
            Private Property B() As Integer
            Public Function GetB() As Integer
                Return B
            End Function
            Public C As Integer = 0
            Private ReadOnly D As Integer = 0
            Public Function GetD() As Integer
                Return Me.D
            End Function
            Public Property E() As Integer
            Private Property F() As String
                Get
                    Return E.ToString()
                End Get
                Set(ByVal value As String)
                    Me.E = Integer.Parse(value)
                End Set
            End Property
        End Class

        Private Class InheritanceTest1
            Public Property Base1() As String
            Private privateBase2 As String
            Public Property Base2() As String
                Get
                    Return privateBase2
                End Get
                Private Set(ByVal value As String)
                    privateBase2 = value
                End Set
            End Property
        End Class

        Private Class InheritanceTest2
            Inherits InheritanceTest1

            Public Property Derived1() As String
            Private privateDerived2 As String
            Public Property Derived2() As String
                Get
                    Return privateDerived2
                End Get
                Private Set(ByVal value As String)
                    privateDerived2 = value
                End Set
            End Property
        End Class

        <TestMethod>
        Public Sub TestInheritance()
            ' Test that inheritance works.
            Dim list As IEnumerable(Of InheritanceTest2) = Me.Connection.Query(Of InheritanceTest2)("select 'One' as Derived1, 'Two' as Derived2, 'Three' as Base1, 'Four' as Base2")
            Assert.AreEqual("One", list.First().Derived1)
            Assert.AreEqual("Two", list.First().Derived2)
            Assert.AreEqual("Three", list.First().Base1)
            Assert.AreEqual("Four", list.First().Base2)
        End Sub

        <TestMethod>
        Public Sub ExecuteReader()
            Dim dt As New DataTable()
            dt.Load(Connection.ExecuteReader("select 3 as [three], 4 as [four]"))
            Assert.AreEqual(2, dt.Columns.Count)
            Assert.AreEqual("three", dt.Columns(0).ColumnName)
            Assert.AreEqual("four", dt.Columns(1).ColumnName)
            Assert.AreEqual(1, dt.Rows.Count)
            Assert.AreEqual(3, CInt(Fix(dt.Rows(0)(0))))
            Assert.AreEqual(4, CInt(Fix(dt.Rows(0)(1))))
        End Sub

        <TestMethod>
        Public Sub TestDefaultDbStringDbType()
            Dim origDefaultStringDbType As Boolean = DbString.IsAnsiDefault
            Try
                DbString.IsAnsiDefault = True
                Dim a As New DbString With {.Value = "abcde"}
                Dim b As New DbString With {.Value = "abcde", .IsAnsi = False}
                Assert.IsTrue(a.IsAnsi)
                Assert.IsFalse(b.IsAnsi)
            Finally
                DbString.IsAnsiDefault = origDefaultStringDbType
            End Try
        End Sub

        <TestMethod>
        Public Sub TestFastExpandoSupportsIDictionary()
            Dim row As IDictionary(Of String, Object) = TryCast(Connection.Query("select 1 A, 'two' B").First(), IDictionary(Of String, Object))
            Assert.AreEqual(1, row("A"))
            Assert.AreEqual("two", row("B"))
        End Sub

        <TestMethod>
        Public Sub TestDapperSetsPrivates()
            Assert.AreEqual(1, Connection.Query(Of PrivateDan)("select 'one' ShadowInDB").First().Shadow)

            Assert.AreEqual(1, Connection.QueryFirstOrDefault(Of PrivateDan)("select 'one' ShadowInDB").Shadow)
        End Sub

        Private Class PrivateDan
            Public Property Shadow() As Integer
            Private WriteOnly Property ShadowInDB() As String
                Set(ByVal value As String)
                    Shadow = If(value = "one", 1, 0)
                End Set
            End Property
        End Class

        <TestMethod>
        Public Sub TestUnexpectedDataMessage()
            Dim msg As String = Nothing
            Try
                Connection.Query(Of Integer)("select count(1) where 1 = @Foo", New WithBizarreData With {.Foo = New GenericUriParser(GenericUriParserOptions.Default), .Bar = 23}).First()
            Catch ex As Exception
                msg = ex.Message
            End Try
            Assert.AreEqual("The member Foo of type System.GenericUriParser cannot be used as a parameter value", msg)
        End Sub

        <TestMethod>
        Public Sub TestUnexpectedButFilteredDataMessage()
            Dim i As Integer = Me.Connection.Query(Of Integer)("select @Bar", New WithBizarreData With {.Foo = New GenericUriParser(GenericUriParserOptions.Default), .Bar = 23}).Single()

            Assert.AreEqual(23, i)
        End Sub

        Private Class WithBizarreData
            Public Property Foo() As GenericUriParser
            Public Property Bar() As Integer
        End Class

        Private Class WithCharValue
            Public Property Value() As Char
            Public Property ValueNullable() As Char?
        End Class

        <TestMethod>
        Public Sub TestCharInputAndOutput()
            Const test As Char = "〠"c
            Dim c As Char = Me.Connection.Query(Of Char)("select @c", New With {Key .c = test}).Single()
            Assert.AreEqual(c, test)
            Dim obj As WithCharValue = Me.Connection.Query(Of WithCharValue)("select @Value as Value", New WithCharValue With {.Value = c}).Single()
            Assert.AreEqual(obj.Value, test)
        End Sub

        <TestMethod>
        Public Sub TestNullableCharInputAndOutputNonNull()
            Dim test? As Char = "〠"c
            Dim c? As Char = Me.Connection.Query(Of Char?)("select @c", New With {Key .c = test}).Single()
            Assert.AreEqual(c, test)
            Dim obj As WithCharValue = Me.Connection.Query(Of WithCharValue)("select @ValueNullable as ValueNullable", New WithCharValue With {.ValueNullable = c}).Single()
            Assert.AreEqual(obj.ValueNullable, test)
        End Sub

        <TestMethod>
        Public Sub TestNullableCharInputAndOutputNull()
            Dim test? As Char = Nothing
            Dim c? As Char = Me.Connection.Query(Of Char?)("select @c", New With {Key .c = test}).Single()
            Assert.AreEqual(c, test)
            Dim obj As WithCharValue = Me.Connection.Query(Of WithCharValue)("select @ValueNullable as ValueNullable", New WithCharValue With {.ValueNullable = c}).Single()
            Assert.AreEqual(obj.ValueNullable, test)
        End Sub

        <TestMethod>
        Public Sub WorkDespiteHavingWrongStructColumnTypes()
            Dim hazInt As CanHazInt = Me.Connection.Query(Of CanHazInt)("select cast(1 as bigint) Value").Single()
            Assert.AreEqual(1, hazInt.Value)
        End Sub

        Private Structure CanHazInt
            Public Property Value() As Integer
        End Structure

        <TestMethod>
        Public Sub TestInt16Usage()
            Assert.AreEqual(Connection.Query(Of Short)("select cast(42 as smallint)").Single(), CShort(42))
            Assert.AreEqual(Connection.Query(Of Short?)("select cast(42 as smallint)").Single(), CType(42, Short?))
            Assert.AreEqual(Connection.Query(Of Short?)("select cast(null as smallint)").Single(), CType(Nothing, Short?))

            Assert.AreEqual(Connection.Query(Of ShortEnum)("select cast(42 as smallint)").Single(), CType(42, ShortEnum))
            Assert.AreEqual(Connection.Query(Of ShortEnum?)("select cast(42 as smallint)").Single(), CType(42, ShortEnum?))
            Assert.AreEqual(Connection.Query(Of ShortEnum?)("select cast(null as smallint)").Single(), CType(Nothing, ShortEnum?))

            Dim row As WithInt16Values = Me.Connection.Query(Of WithInt16Values)("select cast(1 as smallint) as NonNullableInt16, cast(2 as smallint) as NullableInt16, cast(3 as smallint) as NonNullableInt16Enum, cast(4 as smallint) as NullableInt16Enum").Single()
            Assert.AreEqual(row.NonNullableInt16, CShort(1))
            Assert.AreEqual(row.NullableInt16, CShort(2))
            Assert.AreEqual(ShortEnum.Three, row.NonNullableInt16Enum)
            Assert.AreEqual(ShortEnum.Four, row.NullableInt16Enum)

            row = Me.Connection.Query(Of WithInt16Values)("select cast(5 as smallint) as NonNullableInt16, cast(null as smallint) as NullableInt16, cast(6 as smallint) as NonNullableInt16Enum, cast(null as smallint) as NullableInt16Enum").Single()
            Assert.AreEqual(row.NonNullableInt16, CShort(5))
            Assert.AreEqual(row.NullableInt16, CType(Nothing, Short?))
            Assert.AreEqual(ShortEnum.Six, row.NonNullableInt16Enum)
            Assert.AreEqual(row.NullableInt16Enum, CType(Nothing, ShortEnum?))
        End Sub

        <TestMethod>
        Public Sub TestInt32Usage()
            Assert.AreEqual(Connection.Query(Of Integer)("select cast(42 as int)").Single(), CInt(42))
            Assert.AreEqual(Connection.Query(Of Integer?)("select cast(42 as int)").Single(), CType(42, Integer?))
            Assert.AreEqual(Connection.Query(Of Integer?)("select cast(null as int)").Single(), CType(Nothing, Integer?))

            Assert.AreEqual(Connection.Query(Of IntEnum)("select cast(42 as int)").Single(), CType(42, IntEnum))
            Assert.AreEqual(Connection.Query(Of IntEnum?)("select cast(42 as int)").Single(), CType(42, IntEnum?))
            Assert.AreEqual(Connection.Query(Of IntEnum?)("select cast(null as int)").Single(), CType(Nothing, IntEnum?))

            Dim row As WithInt32Values = Me.Connection.Query(Of WithInt32Values)("select cast(1 as int) as NonNullableInt32, cast(2 as int) as NullableInt32, cast(3 as int) as NonNullableInt32Enum, cast(4 as int) as NullableInt32Enum").Single()
            Assert.AreEqual(row.NonNullableInt32, CInt(1))
            Assert.AreEqual(row.NullableInt32, CInt(2))
            Assert.AreEqual(IntEnum.Three, row.NonNullableInt32Enum)
            Assert.AreEqual(IntEnum.Four, row.NullableInt32Enum)

            row = Me.Connection.Query(Of WithInt32Values)("select cast(5 as int) as NonNullableInt32, cast(null as int) as NullableInt32, cast(6 as int) as NonNullableInt32Enum, cast(null as int) as NullableInt32Enum").Single()
            Assert.AreEqual(row.NonNullableInt32, CInt(5))
            Assert.AreEqual(row.NullableInt32, CType(Nothing, Integer?))
            Assert.AreEqual(IntEnum.Six, row.NonNullableInt32Enum)
            Assert.AreEqual(row.NullableInt32Enum, CType(Nothing, IntEnum?))
        End Sub

        Public Class WithInt16Values
            Public Property NonNullableInt16() As Short
            Public Property NullableInt16() As Short?
            Public Property NonNullableInt16Enum() As ShortEnum
            Public Property NullableInt16Enum() As ShortEnum?
        End Class

        Public Class WithInt32Values
            Public Property NonNullableInt32() As Integer
            Public Property NullableInt32() As Integer?
            Public Property NonNullableInt32Enum() As IntEnum
            Public Property NullableInt32Enum() As IntEnum?
        End Class

        Public Enum IntEnum As Integer
            Zero = 0
            One = 1
            Two = 2
            Three = 3
            Four = 4
            Five = 5
            Six = 6
        End Enum

        <TestMethod>
        Public Sub Issue_40_AutomaticBoolConversion()
            Dim user As Issue40_User = Me.Connection.Query(Of Issue40_User)("select UserId=1,Email='abc',Password='changeme',Active=cast(1 as tinyint)").Single()
            Assert.IsTrue(user.Active)
            Assert.AreEqual(1, user.UserID)
            Assert.AreEqual("abc", user.Email)
            Assert.AreEqual("changeme", user.Password)
        End Sub

        Public Class Issue40_User
            Public Sub New()
                Password = String.Empty
                Email = Password
            End Sub

            Public Property UserID() As Integer
            Public Property Email() As String
            Public Property Password() As String
            Public Property Active() As Boolean
        End Class

        <TestMethod>
        Public Sub ExecuteFromClosed()
            Using conn As IDbConnection = GetClosedConnection()
                conn.Execute("-- nop")
                Assert.AreEqual(ConnectionState.Closed, conn.State)
            End Using
        End Sub

        <TestMethod>
        Public Sub ExecuteInvalidFromClosed()
            Using conn As IDbConnection = GetClosedConnection()
                Dim ex As Exception = MyAssert.Throws(Of Exception)(Function() conn.Execute("nop"))
                Assert.AreEqual(ConnectionState.Closed, conn.State)
            End Using
        End Sub

        <TestMethod>
        Public Sub QueryFromClosed()
            Using conn As IDbConnection = GetClosedConnection()
                Dim i As Integer = conn.Query(Of Integer)("select 1").Single()
                Assert.AreEqual(ConnectionState.Closed, conn.State)
                Assert.AreEqual(1, i)
            End Using
        End Sub

        <TestMethod>
        Public Sub QueryInvalidFromClosed()
            Using conn As IDbConnection = GetClosedConnection()
                MyAssert.Throws(Of Exception)(Function() conn.Query(Of Integer)("select gibberish").Single())
                Assert.AreEqual(ConnectionState.Closed, conn.State)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) tests null from int no rows. </summary>
        ''' <remarks>
        ''' https://stackoverflow.com/questions/13127886/dapper-returns-null-for-singleordefaultdatediff.
        ''' </remarks>
        <TestMethod>
        Public Sub TestNullFromInt_NoRows()
            Dim result As Integer = Me.Connection.Query(Of Integer)("select DATEDIFF(day, GETUTCDATE(), @date)", New With {Key .date = Date.UtcNow.AddDays(20)}).SingleOrDefault() ' case with rows
            Assert.AreEqual(20, result)
            result = Me.Connection.Query(Of Integer)("select DATEDIFF(day, GETUTCDATE(), @date) where 1 = 0", New With {Key .date = Date.UtcNow.AddDays(20)}).SingleOrDefault() ' case without rows
            Assert.AreEqual(0, result) ' zero rows; default of int over zero rows is zero
        End Sub

        <TestMethod>
        Public Sub DbStringAnsi()
            Dim a As Integer = Me.Connection.Query(Of Integer)("select datalength(@x)", New With {Key .x = New DbString With {.Value = "abc", .IsAnsi = True}}).Single()
            Dim b As Integer = Me.Connection.Query(Of Integer)("select datalength(@x)", New With {Key .x = New DbString With {.Value = "abc", .IsAnsi = False}}).Single()
            Assert.AreEqual(3, a)
            Assert.AreEqual(6, b)
        End Sub

        Private Class HasInt32
            Public Property Value() As Integer
        End Class

        ''' <summary> (Unit Test Method) downward integer conversion. </summary>
        ''' <remarks> https://stackoverflow.com/q/23696254/23354. </remarks>
        <TestMethod>
        Public Sub DownwardIntegerConversion()
            Const sql As String = "select cast(42 as bigint) as Value"
            Dim i As Integer = Me.Connection.Query(Of HasInt32)(sql).Single().Value
            Assert.AreEqual(42, i)

            i = Me.Connection.Query(Of Integer)(sql).Single()
            Assert.AreEqual(42, i)
        End Sub

        <TestMethod>
        Public Sub TypeBasedViaDynamic()
            Dim type As Type = Common.GetSomeType()
            Dim template As Object = Activator.CreateInstance(type)
            Dim actual As Object = CheetViaDynamic(template, "select @A as [A], @B as [B]", New With {Key .A = 123, Key .B = "abc"})
            Assert.AreEqual(CObj(actual).GetType(), type)
            Dim someType As SomeType = TryCast(actual, SomeType)
            Dim a As Integer = someType.A
            Dim b As String = someType.B
            Assert.AreEqual(123, a)
            Assert.AreEqual("abc", b)
        End Sub

        <TestMethod>
        Public Sub TypeBasedViaType()
            Dim type As Type = Common.GetSomeType()
            Dim actual As Object = Me.Connection.Query(type, "select @A as [A], @B as [B]", New With {Key .A = 123, Key .B = "abc"}).FirstOrDefault()
            Assert.AreEqual(CObj(actual).GetType(), type)
            Dim someType As SomeType = TryCast(actual, SomeType)
            Dim a As Integer = someType.A
            Dim b As String = someType.B
            Assert.AreEqual(123, a)
            Assert.AreEqual("abc", b)
        End Sub

        Private Function CheetViaDynamic(Of T)(ByVal template As T, ByVal query As String, ByVal args As Object) As T
            Return Connection.Query(Of T)(query, args).SingleOrDefault()
        End Function

        <TestMethod>
        Public Sub Issue22_ExecuteScalar()
            Dim i As Integer = Me.Connection.ExecuteScalar(Of Integer)("select 123")
            Assert.AreEqual(123, i)

            i = Me.Connection.ExecuteScalar(Of Integer)("select cast(123 as bigint)")
            Assert.AreEqual(123, i)

            Dim j As Long = Me.Connection.ExecuteScalar(Of Long)("select 123")
            Assert.AreEqual(123L, j)

            j = Me.Connection.ExecuteScalar(Of Long)("select cast(123 as bigint)")
            Assert.AreEqual(123L, j)

            Dim k? As Integer = Me.Connection.ExecuteScalar(Of Integer?)("select @i", New With {Key .i = Nothing})
            Assert.IsNull(k)
        End Sub

        <TestMethod>
        Public Sub Issue142_FailsNamedStatus()
            Dim row1 As Issue142_Status = Me.Connection.Query(Of Issue142_Status)("select @Status as [Status]", New With {Key .Status = StatusType.Started}).Single()
            Assert.AreEqual(StatusType.Started, row1.Status)

            Dim row2 As Issue142_StatusType = Me.Connection.Query(Of Issue142_StatusType)("select @Status as [Status]", New With {Key .Status = Status.Started}).Single()
            Assert.AreEqual(Status.Started, row2.Status)
        End Sub

        Public Class Issue142_Status
            Public Property Status() As StatusType
        End Class

        Public Class Issue142_StatusType
            Public Property Status() As Status
        End Class

        Public Enum StatusType As Byte
            NotStarted = 1
            Started = 2
            Finished = 3
        End Enum

        Public Enum Status As Byte
            NotStarted = 1
            Started = 2
            Finished = 3
        End Enum

        <TestMethod>
        Public Sub Issue178_SqlServer()
            Const sql As String = "select count(*) from Issue178"
            Try
                Connection.Execute("drop table Issue178")
            Catch ' don't care
            End Try
            Try
                Connection.Execute("create table Issue178(id int not null)")
            Catch ' don't care
            End Try
            ' raw ADO.net
            Dim sqlCmd As SqlCommand = New SqlCommand(sql, Connection)
            Using reader1 As IDataReader = sqlCmd.ExecuteReader()
                Assert.IsTrue(reader1.Read())
                Assert.AreEqual(0, reader1.GetInt32(0))
                Assert.IsFalse(reader1.Read())
                Assert.IsFalse(reader1.NextResult())
            End Using

            ' dapper
            Using reader2 As IDataReader = Me.Connection.ExecuteReader(sql)
                Assert.IsTrue(reader2.Read())
                Assert.AreEqual(0, reader2.GetInt32(0))
                Assert.IsFalse(reader2.Read())
                Assert.IsFalse(reader2.NextResult())
            End Using
        End Sub

        <TestMethod>
        Public Sub QueryBasicWithoutQuery()
            Dim i? As Integer = Me.Connection.Query(Of Integer?)("print 'not a query'").FirstOrDefault()
            Assert.IsNull(i)
        End Sub

        <TestMethod>
        Public Sub QueryComplexWithoutQuery()
            Dim obj As Object = Me.Connection.Query(Of Foo1)("print 'not a query'").FirstOrDefault()
            Assert.IsNull(obj)
        End Sub

        ' <FactLongRunning>

        Public Sub Issue263_Timeout()
            Dim watch As Stopwatch = Stopwatch.StartNew()
            Dim i As Integer = Me.Connection.Query(Of Integer)("waitfor delay '00:01:00'; select 42;", commandTimeout:=300, buffered:=False).Single()
            watch.Stop()
            Assert.AreEqual(42, i)
            Dim minutes As Double = watch.ElapsedMilliseconds \ 1000 \ 60
            Assert.IsTrue(minutes >= 0.95 AndAlso minutes <= 1.05)
        End Sub

        <TestMethod>
        Public Async Sub SO35470588_WrongValuePidValue()
            ' nuke, rebuild, and populate the table
            Try ' don't care
                Connection.Execute("drop table TPTable")
            Catch
            End Try
            Connection.Execute("" & ControlChars.CrLf & "create table TPTable (Pid int not null primary key identity(1,1), Value int not null);" & ControlChars.CrLf & "insert TPTable (Value) values (2), (568)")

            ' fetch the data using the query in the question, then force to a dictionary
            Dim rows As IDictionary(Of Integer, TPTable) = (Await Connection.QueryAsync(Of TPTable)("select * from TPTable").ConfigureAwait(False)).ToDictionary(Function(x) x.Pid)

            ' check the number of rows
            Assert.AreEqual(2, rows.Count)

            ' check row 1
            Assert.AreEqual(1, rows(1).Pid)
            Assert.AreEqual(2, rows(1).Value)

            ' check row 2
            Assert.AreEqual(2, rows(2).Pid)
            Assert.AreEqual(568, rows(2).Value)
        End Sub

        Public Class TPTable
            Public Property Pid() As Integer
            Public Property Value() As Integer
        End Class

        <TestMethod>
        Public Sub GetOnlyProperties()
            Dim obj As HazGetOnly = Me.Connection.QuerySingle(Of HazGetOnly)("select 42 as [Id], 'def' as [Name];")
            Assert.AreEqual(42, obj.Id)
			Assert.AreEqual("def", obj.Name)
		End Sub

		Private Class HazGetOnly
			Public ReadOnly Property Id() As Integer
            Public ReadOnly Property Name() As String = "abc"
        End Class
	End Class
End Namespace
