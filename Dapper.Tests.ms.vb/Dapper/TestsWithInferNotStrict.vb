﻿Option Infer On
Option Strict Off
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper

Namespace DapperTests

    Public Class LiteralTestsWithInfer
        Inherits TestBase

        <TestMethod>
        Public Sub LiteralReplacementEnumAndString()
            Dim args As Object = New With {Key .x = AnEnum.B, Key .y = 123.45D, Key .z = AnotherEnum.A}
            Dim row As Object = Me.Connection.Query("select {=x} as x,{=y} as y,cast({=z} as tinyint) as z", args).Single()
            Dim x As AnEnum = CType(CInt(Fix(row.x)), AnEnum)
            Dim y As Decimal = row.y
            Dim z As AnotherEnum = CType(CByte(row.z), AnotherEnum)
            Assert.AreEqual(AnEnum.B, x)
            Assert.AreEqual(123.45D, y)
            Assert.AreEqual(AnotherEnum.A, z)
        End Sub

        <TestMethod>
        Public Sub LiteralReplacementDynamicEnumAndString()
            Dim args As New DynamicParameters()
            args.Add("x", AnEnum.B)
            args.Add("y", 123.45D)
            args.Add("z", AnotherEnum.A)
            Dim row As Object = Me.Connection.Query("select {=x} as x,{=y} as y,cast({=z} as tinyint) as z", args).Single()
            Dim x As AnEnum = CType(CInt(Fix(row.x)), AnEnum)
            Dim y As Decimal = row.y
            Dim z As AnotherEnum = CType(CByte(row.z), AnotherEnum)
            Assert.AreEqual(AnEnum.B, x)
            Assert.AreEqual(123.45D, y)
            Assert.AreEqual(AnotherEnum.A, z)
        End Sub

    End Class

    Public Class MiscTestsWithInfer
        Inherits TestBase

        <TestMethod>
        Public Sub TestDbString()
            Dim obj As Object = Me.Connection.Query("select datalength(@a) as a, datalength(@b) as b, datalength(@c) as c, datalength(@d) as d, datalength(@e) as e, datalength(@f) as f", New With {Key .a = New DbString With {.Value = "abcde", .IsFixedLength = True, .Length = 10, .IsAnsi = True}, Key .b = New DbString With {.Value = "abcde", .IsFixedLength = True, .Length = 10, .IsAnsi = False}, Key .c = New DbString With {.Value = "abcde", .IsFixedLength = False, .Length = 10, .IsAnsi = True}, Key .d = New DbString With {.Value = "abcde", .IsFixedLength = False, .Length = 10, .IsAnsi = False}, Key .e = New DbString With {.Value = "abcde", .IsAnsi = True}, Key .f = New DbString With {.Value = "abcde", .IsAnsi = False}}).First()
            Assert.AreEqual(10, CInt(Fix(obj.a)))
            Assert.AreEqual(20, CInt(Fix(obj.b)))
            Assert.AreEqual(5, CInt(Fix(obj.c)))
            Assert.AreEqual(10, CInt(Fix(obj.d)))
            Assert.AreEqual(5, CInt(Fix(obj.e)))
            Assert.AreEqual(10, CInt(Fix(obj.f)))
        End Sub

        <TestMethod>
        Public Sub TestDynamicMutation()
            Dim obj As Object = Me.Connection.Query("select 1 as [a], 2 as [b], 3 as [c]").Single()
            Assert.AreEqual(1, CInt(Fix(obj.a)))
            Dim dict As IDictionary(Of String, Object) = obj
            Assert.AreEqual(3, dict.Count)
            Assert.IsTrue(dict.Remove("a"))
            Assert.IsFalse(dict.Remove("d"))
            Assert.AreEqual(2, dict.Count)
            dict.Add("d", 4)
            Assert.AreEqual(3, dict.Count)
            Assert.AreEqual("b,c,d", String.Join(",", dict.Keys.OrderBy(Function(x) x)))
            Assert.AreEqual("2,3,4", String.Join(",", dict.OrderBy(Function(x) x.Key).Select(Function(x) x.Value)))

            Assert.AreEqual(2, CInt(Fix(obj.b)))
            Assert.AreEqual(3, CInt(Fix(obj.c)))
            Assert.AreEqual(4, CInt(Fix(obj.d)))
            Try
                Assert.AreEqual(1, CInt(Fix(obj.a)))
                Throw New InvalidOperationException("should have thrown")
            Catch e1 As InvalidCastException
                ' pass
            End Try
        End Sub

        <TestMethod>
        Public Sub SO30435185_InvalidTypeOwner()
            Dim ex As Exception = MyAssert.Throws(Of InvalidOperationException)(Sub()
                                                                                    Const sql As String = " INSERT INTO #XXX" & ControlChars.CrLf & "                        (XXXId, AnotherId, ThirdId, Value, Comment)" & ControlChars.CrLf & "                        VALUES" & ControlChars.CrLf & "                        (@XXXId, @AnotherId, @ThirdId, @Value, @Comment); select @@rowcount as [Foo]"
                                                                                    Dim command As Object = New With {Key .MyModels = {New With {Key .XXXId = 1, Key .AnotherId = 2, Key .ThirdId = 3, Key .Value = "abc", Key .Comment = "def"}}}
#Disable Warning IDE0037 ' Use inferred member name
                                                                                    Dim parameters As IEnumerable(Of Object) = command.MyModels.Select(Function(model) New With {Key .XXXId = model.XXXId, Key .AnotherId = model.AnotherId, Key .ThirdId = model.ThirdId, Key .Value = model.Value, Key .Comment = model.Comment}).ToArray()
#Enable Warning IDE0037 ' Use inferred member name
                                                                                    Dim rowcount As Integer = CInt(Fix(Connection.Query(sql, parameters).Single().Foo))
                                                                                    Assert.AreEqual(1, rowcount)
                                                                                End Sub)
            Assert.AreEqual("An enumerable sequence of parameters (arrays, lists, etc) is not allowed in this context", ex.Message)
        End Sub

#If False Then
        <TestMethod>
        Public Sub TestIssue131()
            Dim results As IEnumerable(Of Dynamic) = Me.Connection.Query(Of Dynamic, Integer, Dynamic)("SELECT 1 Id, 'Mr' Title, 'John' Surname, 4 AddressCount", Function(person, addressCount, person), splitOn:="AddressCount").FirstOrDefault()
            Dim asDict = CType(results, IDictionary(Of String, Object))

            Assert.IsTrue(asDict.ContainsKey("Id"))
            Assert.IsTrue(asDict.ContainsKey("Title"))
            Assert.IsTrue(asDict.ContainsKey("Surname"))
            Assert.IsFalse(asDict.ContainsKey("AddressCount"))
        End Sub

        <TestMethod>
        Public Sub TestDapperTableMetadataRetrieval()
            ' Test for a bug found in CS 51509960 where the following sequence would result in an InvalidOperationException being
            ' thrown due to an attempt to access a disposed of DataReader:
            '
            ' - Perform a dynamic query that yields no results
            ' - Add data to the source of that query
            ' - Perform a the same query again
            Connection.Execute("CREATE TABLE #sut (value varchar(10) NOT NULL PRIMARY KEY)")
            Assert.AreEqual(Enumerable.Empty(Of Dynamic)(), Connection.Query("SELECT value FROM #sut"))
            Assert.AreEqual(1, Connection.Execute("INSERT INTO #sut (value) VALUES ('test')"))
            Dim result As Object = Me.Connection.Query("SELECT value FROM #sut")
            Dim first = result.First()
            Assert.AreEqual("test", CStr(first.value))
        End Sub
#End If

    End Class
End Namespace
