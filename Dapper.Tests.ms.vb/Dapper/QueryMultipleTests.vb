﻿Option Infer On
Option Strict Off
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper

Namespace DapperTests
	Public Class QueryMultipleTests
		Inherits TestBase

		<TestMethod> _
		Public Sub TestQueryMultipleBuffered()
			Using grid = connection.QueryMultiple("select 1; select 2; select @x; select 4", New With {Key .x = 3})
				Dim a = grid.Read(Of Integer)()
				Dim b = grid.Read(Of Integer)()
				Dim c = grid.Read(Of Integer)()
				Dim d = grid.Read(Of Integer)()

				Assert.AreEqual(1, a.Single())
				Assert.AreEqual(2, b.Single())
				Assert.AreEqual(3, c.Single())
				Assert.AreEqual(4, d.Single())
			End Using
		End Sub

		<TestMethod> _
		Public Sub TestQueryMultipleNonBufferedIncorrectOrder()
			Using grid = connection.QueryMultiple("select 1; select 2; select @x; select 4", New With {Key .x = 3})
				Dim a = grid.Read(Of Integer)(False)
				Try
					Dim b = grid.Read(Of Integer)(False)
					Throw New InvalidOperationException() ' should have thrown
				Catch e1 As InvalidOperationException
					' that's expected
				End Try
			End Using
		End Sub

		<TestMethod> _
		Public Sub TestQueryMultipleNonBufferedCorrectOrder()
			Using grid = connection.QueryMultiple("select 1; select 2; select @x; select 4", New With {Key .x = 3})
				Dim a = grid.Read(Of Integer)(False).Single()
				Dim b = grid.Read(Of Integer)(False).Single()
				Dim c = grid.Read(Of Integer)(False).Single()
				Dim d = grid.Read(Of Integer)(False).Single()

				Assert.AreEqual(1, a)
				Assert.AreEqual(2, b)
				Assert.AreEqual(3, c)
				Assert.AreEqual(4, d)
			End Using
		End Sub

		<TestMethod> _
		Public Sub TestMultiReaderBasic()
			Const sql As String = "select 1 as Id union all select 2 as Id     select 'abc' as name   select 1 as Id union all select 2 as Id"
			Dim i, j As Integer
			Dim s As String
			Using multi = connection.QueryMultiple(sql)
				i = multi.Read(Of Integer)().First()
				s = multi.Read(Of String)().Single()
				j = multi.Read(Of Integer)().Sum()
			End Using
			Assert.AreEqual(1, i)
			Assert.AreEqual("abc", s)
			Assert.AreEqual(3, j)
		End Sub

		<TestMethod> _
		Public Sub TestReadDynamicWithGridReader()
			Const createSql As String = String.Empty & ControlChars.CrLf & "                create table #Users (Id int, Name varchar(20))" & ControlChars.CrLf & "                create table #Posts (Id int, OwnerId int, Content varchar(20))" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Users values(99, 'Sam')" & ControlChars.CrLf & "                insert #Users values(2, 'I am')" & ControlChars.CrLf & ControlChars.CrLf & "                insert #Posts values(1, 99, 'Sams Post1')" & ControlChars.CrLf & "                insert #Posts values(2, 99, 'Sams Post2')" & ControlChars.CrLf & "                insert #Posts values(3, null, 'no ones post')"
			Try
				connection.Execute(createSql)

				Const sql As String = "SELECT * FROM #Users ORDER BY Id" & ControlChars.CrLf & "                        SELECT * FROM #Posts ORDER BY Id DESC"

				Dim grid = connection.QueryMultiple(sql)

				Dim users = grid.Read().ToList()
				Dim posts = grid.Read().ToList()

				Assert.AreEqual(2, users.Count)
				Assert.AreEqual(3, posts.Count)

				Assert.AreEqual(2, CInt(Fix(users(0).Id)))
				Assert.AreEqual(3, CInt(Fix(posts(0).Id)))
			Finally
				connection.Execute("drop table #Users drop table #Posts")
			End Try
		End Sub

		<TestMethod> _
		Public Sub Issue268_ReturnQueryMultiple()
			connection.Execute("create proc #TestProc268 (@a int, @b int, @c int)as " & ControlChars.CrLf & "begin" & ControlChars.CrLf & "select @a;" & ControlChars.CrLf & ControlChars.CrLf & "select @b" & ControlChars.CrLf & ControlChars.CrLf & "return @c; " & ControlChars.CrLf & "end")
			Dim p = New DynamicParameters(New With {Key .a = 1, Key .b = 2, Key .c = 3})
			p.Add("RetVal", dbType:= DbType.Int32, direction:= ParameterDirection.ReturnValue)

			Using reader = connection.QueryMultiple("#TestProc268", p, commandType:= CommandType.StoredProcedure)
				reader.Read()
			End Using
			Dim retVal = p.Get(Of Integer)("RetVal")
			Assert.AreEqual(3, retVal)
		End Sub

		<TestMethod> _
		Public Sub Issue524_QueryMultiple_Cast()
			' aka: Read<int> should work even if the data is a <long>
			' using regular API
			Assert.AreEqual(42, connection.Query(Of Integer)("select cast(42 as bigint)").Single())
			Assert.AreEqual(42, connection.QuerySingle(Of Integer)("select cast(42 as bigint)"))

			' using multi-reader API
			Using reader = connection.QueryMultiple("select cast(42 as bigint); select cast(42 as bigint)")
				Assert.AreEqual(42, reader.Read(Of Integer)().Single())
				Assert.AreEqual(42, reader.ReadSingle(Of Integer)())
			End Using
		End Sub

		<TestMethod> _
		Public Sub QueryMultipleFromClosed()
			Using conn = GetClosedConnection()
				Using multi = conn.QueryMultiple("select 1; select 'abc';")
					Assert.AreEqual(1, multi.Read(Of Integer)().Single())
					Assert.AreEqual("abc", multi.Read(Of String)().Single())
				End Using
				Assert.AreEqual(ConnectionState.Closed, conn.State)
			End Using
		End Sub

		<TestMethod> _
		Public Sub QueryMultiple2FromClosed()
			Using conn = GetClosedConnection()
				Assert.AreEqual(ConnectionState.Closed, conn.State)
				Using multi = conn.QueryMultiple("select 1 select 2 select 3")
					Assert.AreEqual(1, multi.Read(Of Integer)().Single())
					Assert.AreEqual(2, multi.Read(Of Integer)().Single())
					' not reading 3 is intentional here
				End Using
				Assert.AreEqual(ConnectionState.Closed, conn.State)
			End Using
		End Sub

		<TestMethod> _
		Public Sub SO35554284_QueryMultipleUntilConsumed()
			Using reader = connection.QueryMultiple("select 1 as Id; select 2 as Id; select 3 as Id;")
				Dim items = New List(Of HazNameId)()
				Do While Not reader.IsConsumed
					items.AddRange(reader.Read(Of HazNameId)())
				Loop
				Assert.AreEqual(3, items.Count)
				Assert.AreEqual(1, items(0).Id)
				Assert.AreEqual(2, items(1).Id)
				Assert.AreEqual(3, items(2).Id)
			End Using
		End Sub

		<TestMethod> _
		Public Sub QueryMultipleInvalidFromClosed()
			Using conn = GetClosedConnection()
                MyAssert.Throws(Of Exception)(Function() conn.QueryMultiple("select gibberish"))
                Assert.AreEqual(ConnectionState.Closed, conn.State)
			End Using
		End Sub

        <TestMethod>
        Public Sub TestMultiSelectWithSomeEmptyGridsUnbufferedSub()
            TestMultiSelectWithSomeEmptyGrids(False)
        End Sub

        <TestMethod>
        Public Sub TestMultiSelectWithSomeEmptyGridsBufferedSub()
            TestMultiSelectWithSomeEmptyGrids(True)
        End Sub

        Private Sub TestMultiSelectWithSomeEmptyGrids(buffered As Boolean)
            Using reader = Connection.QueryMultiple("select 1; select 2 where 1 = 0; select 3 where 1 = 0; select 4;")
                Dim one = reader.Read(Of Integer)(buffered:=buffered).ToArray()
                Dim two = reader.Read(Of Integer)(buffered:=buffered).ToArray()
                Dim three = reader.Read(Of Integer)(buffered:=buffered).ToArray()
                Dim four = reader.Read(Of Integer)(buffered:=buffered).ToArray()
                Try
                    reader.Read(Of Integer)(buffered:=buffered)
                    Throw New InvalidOperationException("this should not have worked!")
                Catch ex As ObjectDisposedException
                    Assert.AreEqual("The reader has been disposed; this can happen after all data has been consumed" & vbCrLf & "Object name: 'Dapper.SqlMapper+GridReader'.", ex.Message)
                End Try

                Assert.AreEqual(1, one.Count)
                Assert.AreEqual(1, one(0))
                Assert.IsFalse(two.Any)
                Assert.IsFalse(three.Any)
                Assert.AreEqual(1, four.Count)
                Assert.AreEqual(4, four(0))
            End Using
        End Sub

        <TestMethod>
        Public Sub TypeBasedViaTypeMulti()
            Dim type As Type = Common.GetSomeType()

            Dim first, second As Object
            Using multi = connection.QueryMultiple("select @A as [A], @B as [B]; select @C as [A], @D as [B]", New With {Key .A = 123, Key .B = "abc", Key .C = 456, Key .D = "def"})
                first = multi.Read(type).Single()
                second = multi.Read(type).Single()
            End Using
            Assert.AreEqual(CObj(first).GetType(), type)
            Dim a As Integer = first.A
            Dim b As String = first.B
            Assert.AreEqual(123, a)
            Assert.AreEqual("abc", b)

            Assert.AreEqual(CObj(second).GetType(), type)
            a = second.A
            b = second.B
            Assert.AreEqual(456, a)
            Assert.AreEqual("def", b)
        End Sub

    End Class
End Namespace
