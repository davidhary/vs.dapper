﻿Imports System.Collections.Generic
Imports System.Linq
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper
Namespace DapperTests
	Public Class DataReaderTests
		Inherits TestBase

		<TestMethod> _
		Public Sub GetSameReaderForSameShape()
            Dim origReader As IDataReader = Me.Connection.ExecuteReader("select 'abc' as Name, 123 as Id")
            Dim origParser As Func(Of IDataReader, Object) = origReader.GetRowParser(GetType(HazNameId))

            Dim typedParser As Func(Of IDataReader, HazNameId) = origReader.GetRowParser(Of HazNameId)()

            Assert.IsTrue(ReferenceEquals(origParser, typedParser))

            Dim list As IEnumerable(Of HazNameId) = origReader.Parse(Of HazNameId)().ToList()
            Assert.AreEqual(1, list.Count)
            Assert.AreEqual("abc", list(0).Name)
            Assert.AreEqual(123, list(0).Id)
            origReader.Dispose()

            Dim secondReader As IDataReader = Me.Connection.ExecuteReader("select 'abc' as Name, 123 as Id")
            Dim secondParser As Func(Of IDataReader, Object) = secondReader.GetRowParser(GetType(HazNameId))
            Dim thirdParser As Func(Of IDataReader, Object) = secondReader.GetRowParser(GetType(HazNameId), 1)

            list = secondReader.Parse(Of HazNameId)().ToList()
            Assert.AreEqual(1, list.Count)
            Assert.AreEqual("abc", list(0).Name)
            Assert.AreEqual(123, list(0).Id)
            secondReader.Dispose()

            ' now: should be different readers, but same parser
            Assert.IsFalse(ReferenceEquals(origReader, secondReader))
            Assert.IsTrue(ReferenceEquals(origParser, secondParser))
            Assert.IsFalse(ReferenceEquals(secondParser, thirdParser))
        End Sub

        <TestMethod>
        Public Sub DiscriminatedUnion()
            Dim result As New List(Of Discriminated_BaseType)()
            Using reader As IDataReader = Me.Connection.ExecuteReader("" & ControlChars.CrLf & "select 'abc' as Name, 1 as Type, 3.0 as Value" & ControlChars.CrLf & "union all" & ControlChars.CrLf & "select 'def' as Name, 2 as Type, 4.0 as Value")
                If reader.Read() Then
                    Dim toFoo As Func(Of IDataReader, Discriminated_BaseType) = reader.GetRowParser(Of Discriminated_BaseType)(GetType(Discriminated_Foo))
                    Dim toBar As Func(Of IDataReader, Discriminated_BaseType) = reader.GetRowParser(Of Discriminated_BaseType)(GetType(Discriminated_Bar))
                    Dim col As Integer = reader.GetOrdinal("Type")
                    Do
                        Select Case reader.GetInt32(col)
                            Case 1
                                result.Add(toFoo(reader))
                            Case 2
                                result.Add(toBar(reader))
                        End Select
                    Loop While reader.Read()
                End If
            End Using

            Assert.AreEqual(2, result.Count)
            Assert.AreEqual(1, result(0).Type)
            Assert.AreEqual(2, result(1).Type)
            Dim foo As Discriminated_Foo = CType(result(0), Discriminated_Foo)
            Assert.AreEqual("abc", foo.Name)
            Dim bar As Discriminated_Bar = CType(result(1), Discriminated_Bar)
            Assert.AreEqual(bar.Value, CSng(4.0))
        End Sub

        <TestMethod>
        Public Sub DiscriminatedUnionWithMultiMapping()
            Dim result As New List(Of DiscriminatedWithMultiMapping_BaseType)()
            Using reader As IDataReader = Me.Connection.ExecuteReader("" & ControlChars.CrLf & "select 'abc' as Name, 1 as Type, 3.0 as Value, 1 as Id, 'zxc' as Name" & ControlChars.CrLf & "union all" & ControlChars.CrLf & "select 'def' as Name, 2 as Type, 4.0 as Value, 2 as Id, 'qwe' as Name")
                If reader.Read() Then
                    Dim col As Integer = reader.GetOrdinal("Type")
                    Dim splitOn As Integer = reader.GetOrdinal("Id")

                    Dim toFoo As Func(Of IDataReader, DiscriminatedWithMultiMapping_BaseType) = reader.GetRowParser(Of DiscriminatedWithMultiMapping_BaseType)(GetType(DiscriminatedWithMultiMapping_Foo), 0, splitOn)
                    Dim toBar As Func(Of IDataReader, DiscriminatedWithMultiMapping_BaseType) = reader.GetRowParser(Of DiscriminatedWithMultiMapping_BaseType)(GetType(DiscriminatedWithMultiMapping_Bar), 0, splitOn)
                    Dim toHaz As Func(Of IDataReader, HazNameId) = reader.GetRowParser(Of HazNameId)(GetType(HazNameId), splitOn, reader.FieldCount - splitOn)

                    Do
                        Dim obj As DiscriminatedWithMultiMapping_BaseType = Nothing
                        Select Case reader.GetInt32(col)
                            Case 1
                                obj = toFoo(reader)
                            Case 2
                                obj = toBar(reader)
                        End Select

                        Assert.IsNotNull(obj)
                        obj.HazNameIdObject = toHaz(reader)
                        result.Add(obj)

                    Loop While reader.Read()
                End If
            End Using

            Assert.AreEqual(2, result.Count)
			Assert.AreEqual(1, result(0).Type)
			Assert.AreEqual(2, result(1).Type)
            Dim foo As DiscriminatedWithMultiMapping_Foo = CType(result(0), DiscriminatedWithMultiMapping_Foo)
            Assert.AreEqual("abc", foo.Name)
			Assert.AreEqual(1, foo.HazNameIdObject.Id)
			Assert.AreEqual("zxc", foo.HazNameIdObject.Name)
            Dim bar As DiscriminatedWithMultiMapping_Bar = CType(result(1), DiscriminatedWithMultiMapping_Bar)
            Assert.AreEqual(bar.Value, CSng(4.0))
			Assert.AreEqual(2, bar.HazNameIdObject.Id)
			Assert.AreEqual("qwe", bar.HazNameIdObject.Name)
		End Sub

		Private MustInherit Class Discriminated_BaseType
			Public MustOverride ReadOnly Property Type() As Integer
		End Class

        Private Class Discriminated_Foo
            Inherits Discriminated_BaseType
            Public Property Name() As String
            Public Overrides ReadOnly Property [Type] As Integer
        End Class

        Private Class Discriminated_Bar
			Inherits Discriminated_BaseType
            Public Property Value() As Single
            Public Overrides ReadOnly Property [Type] As Integer
        End Class

        Private MustInherit Class DiscriminatedWithMultiMapping_BaseType
			Inherits Discriminated_BaseType

			Public MustOverride Property HazNameIdObject() As HazNameId
		End Class

		Private Class DiscriminatedWithMultiMapping_Foo
			Inherits DiscriminatedWithMultiMapping_BaseType
            Public Overrides Property HazNameIdObject() As HazNameId
            Public Property Name() As String
            Public Overrides ReadOnly Property [Type] As Integer
        End Class

        Private Class DiscriminatedWithMultiMapping_Bar
			Inherits DiscriminatedWithMultiMapping_BaseType
            Public Overrides Property HazNameIdObject() As HazNameId
            Public Property Value() As Single
            Public Overrides ReadOnly Property [Type] As Integer
        End Class
    End Class
End Namespace
