﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Linq
Imports Dapper
Namespace DapperTests

    ' <Collection(NonParallelDefinition.Name)>

    Public Class NullTests
        Inherits TestBase

        <TestMethod>
        Public Sub TestNullableDefault()
            TestNullable(False)
        End Sub

        <TestMethod>
        Public Sub TestNullableApplyNulls()
            TestNullable(True)
        End Sub

        Private Sub TestNullable(ByVal applyNulls As Boolean)
            Dim oldSetting As Boolean = SqlMapper.Settings.ApplyNullValues
            Try
                SqlMapper.Settings.ApplyNullValues = applyNulls
                SqlMapper.PurgeQueryCache()

                Dim data As IDictionary(Of Integer, NullTestClass) = Connection.Query(Of NullTestClass)("" & ControlChars.CrLf & "declare @data table(Id int not null, A int null, B int null, C varchar(20), D int null, E int null)" & ControlChars.CrLf & "insert @data (Id, A, B, C, D, E) values " & ControlChars.CrLf & "	(1,null,null,null,null,null)," & ControlChars.CrLf & "	(2,42,42,'abc',2,2)" & ControlChars.CrLf & "select * from @data").ToDictionary(Function(p) p.Id)

                Dim obj As NullTestClass = data(2)

                Assert.AreEqual(2, obj.Id)
                Assert.AreEqual(42, obj.A)
                Assert.AreEqual(42, obj.B)
                Assert.AreEqual("abc", obj.C)
                Assert.AreEqual(AnEnum.A, obj.D)
                Assert.AreEqual(AnEnum.A, obj.E)

                obj = data(1)
                Assert.AreEqual(1, obj.Id)
                If applyNulls Then
                    Assert.AreEqual(2, obj.A) ' cannot be null
                    Assert.IsNull(obj.B)
                    Assert.IsNull(obj.C)
                    Assert.AreEqual(AnEnum.B, obj.D)
                    Assert.IsNull(obj.E)
                Else
                    Assert.AreEqual(2, obj.A)
                    Assert.AreEqual(2, obj.B)
                    Assert.AreEqual("def", obj.C)
                    Assert.AreEqual(AnEnum.B, obj.D)
                    Assert.AreEqual(AnEnum.B, obj.E)
                End If
            Finally
                SqlMapper.Settings.ApplyNullValues = oldSetting
            End Try
        End Sub

        Private Class NullTestClass
            Public Property Id() As Integer
            Public Property A() As Integer
            Public Property B() As Integer?
            Public Property C() As String
            Public Property D() As AnEnum
            Public Property E() As AnEnum?

            Public Sub New()
                A = 2
                B = 2
                C = "def"
                D = AnEnum.B
                E = AnEnum.B
            End Sub
        End Class
    End Class
End Namespace
