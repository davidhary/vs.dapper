﻿Imports System.Data.Common

Imports Dapper
Namespace Global.isr.Dapper.Tests.DapperTests

    ''' <summary> A common. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 9/11/2018 </para>
    ''' </remarks>
    Public NotInheritable Class CommonTests

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Sub New()
        End Sub

        ''' <summary> Gets some type. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> some type. </returns>
        Public Shared Function GetSomeType() As Type
            Return GetType(SomeType)
        End Function

        ''' <summary> Dapper enum value. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="connection"> The connection. </param>
        Public Shared Sub DapperEnumValue(connection As System.Data.IDbConnection)

            ' test passing as AsEnum, reading as int
            Dim v As AnEnum = CType(connection.QuerySingle(Of Integer)("select @v, @y, @z", New With {Key .v = AnEnum.B, Key .y = CType(AnEnum.B, AnEnum?), Key .z = CType(Nothing, AnEnum?)}), AnEnum)
            Assert.AreEqual(AnEnum.B, v)

            Dim args As New DynamicParameters()
            args.Add("v", AnEnum.B)
            args.Add("y", AnEnum.B)
            args.Add("z", Nothing)
            v = CType(connection.QuerySingle(Of Integer)("select @v, @y, @z", args), AnEnum)
            Assert.AreEqual(AnEnum.B, v)

            ' test passing as int, reading as AnEnum
            Dim k As Integer = CInt(Fix(connection.QuerySingle(Of AnEnum)("select @v, @y, @z", New With {Key .v = CInt(Fix(AnEnum.B)), Key .y = CType(CInt(Fix(AnEnum.B)), Integer?), Key .z = CType(Nothing, Integer?)})))
            Assert.AreEqual(k, CInt(Fix(AnEnum.B)))

            args = New DynamicParameters()
            args.Add("v", CInt(Fix(AnEnum.B)))
            args.Add("y", CInt(Fix(AnEnum.B)))
            args.Add("z", Nothing)
            k = CInt(Fix(connection.QuerySingle(Of AnEnum)("select @v, @y, @z", args)))
            Assert.AreEqual(k, CInt(Fix(AnEnum.B)))

        End Sub

        ''' <summary> Asserts date time. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="connection"> The connection. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Public Shared Sub TestDatetime(connection As DbConnection)
            Dim now? As DateTime = DateTimeOffset.UtcNow.DateTime
            Try
                connection.Execute("DROP TABLE Persons")
            Catch
            End Try
            connection.Execute("CREATE TABLE Persons (id int not null, dob datetime null)")
            connection.Execute("INSERT Persons (id, dob) values (@id, @dob)", New With {Key .id = 7, Key .dob = CType(Nothing, DateTime?)})
            connection.Execute("INSERT Persons (id, dob) values (@id, @dob)", New With {Key .id = 42, Key .dob = now})
            Dim row As NullableDatePerson = connection.QueryFirstOrDefault(Of NullableDatePerson)("SELECT id, dob, dob as dob2 FROM Persons WHERE id=@id", New With {Key .id = 7})
            Assert.IsNotNull(row)
            Assert.AreEqual(7, row.Id)
            Assert.IsNull(row.DoB)
            Assert.IsNull(row.DoB2)
            row = connection.QueryFirstOrDefault(Of NullableDatePerson)("SELECT id, dob FROM Persons WHERE id=@id", New With {Key .id = 42})
            Assert.IsNotNull(row)
            Assert.AreEqual(42, row.Id)
            row.DoB.Equals(now)
            row.DoB2.Equals(now)
        End Sub

        ''' <summary> A nullable date person. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Class NullableDatePerson

            ''' <summary> Gets or sets the identifier. </summary>
            ''' <value> The identifier. </value>
            Public Property Id As Integer

            ''' <summary> Gets or sets the do b. </summary>
            ''' <value> The do b. </value>
            Public Property DoB As DateTime?

            ''' <summary> Gets or sets the do b 2. </summary>
            ''' <value> The do b 2. </value>
            Public Property DoB2 As DateTime?
        End Class

    End Class
End Namespace
