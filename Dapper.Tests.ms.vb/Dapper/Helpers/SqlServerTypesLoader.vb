﻿Imports System
Imports System.IO
Imports System.Runtime.InteropServices

Namespace DapperTests
	''' <summary>
	''' Utility methods related to CLR Types for SQL Server 
	''' </summary>
	Friend NotInheritable Class SqlServerTypesLoader

		Private Sub New()
		End Sub

		Private Shared ReadOnly _nativeLoadLock As New Object()
		Private Shared _nativeAssembliesLoaded As Boolean

		<DllImport("kernel32.dll", CharSet := CharSet.Unicode, SetLastError := True)> _
		Private Shared Function LoadLibrary(ByVal libname As String) As IntPtr
		End Function

		''' <summary>
		''' Loads the required native assemblies for the current architecture (x86 or x64)
		''' </summary>
		''' <param name="rootApplicationPath">
		''' Root path of the current application. Use Server.MapPath(".") for ASP.NET applications
		''' and AppDomain.CurrentDomain.BaseDirectory for desktop applications.
		''' </param>
		Public Shared Sub LoadNativeAssemblies(ByVal rootApplicationPath As String)
			If _nativeAssembliesLoaded Then
				Return
			End If
			SyncLock _nativeLoadLock
				If Not _nativeAssembliesLoaded Then
                    Dim nativeBinaryPath As String = If(IntPtr.Size > 4, Path.Combine(rootApplicationPath, "x64\"), Path.Combine(rootApplicationPath, "x86\"))
                    Console.Write("(from: " & nativeBinaryPath & ")...")
					LoadNativeAssembly(nativeBinaryPath, "msvcr120.dll")
					LoadNativeAssembly(nativeBinaryPath, "SqlServerSpatial140.dll")
					_nativeAssembliesLoaded = True
				End If
			End SyncLock
		End Sub

		Private Shared Sub LoadNativeAssembly(ByVal nativeBinaryPath As String, ByVal assemblyName As String)
            Dim path As String = System.IO.Path.Combine(nativeBinaryPath, assemblyName)
            Dim ptr As IntPtr = LoadLibrary(path)
            If ptr = IntPtr.Zero Then
                Throw New Exception(String.Format("Error loading {0} (ErrorCode: {1})", assemblyName, Marshal.GetLastWin32Error()))
            End If
        End Sub
	End Class
End Namespace
