﻿Option Infer On
Option Strict Off
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Linq
Imports System.Reflection
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Dapper

Namespace DapperTests

    Public Class TypeHandlerTests
        Inherits TestBase

        <TestMethod>
        Public Sub TestChangingDefaultStringTypeMappingToAnsiString()
            Const sql As String = "SELECT SQL_VARIANT_PROPERTY(CONVERT(sql_variant, @testParam),'BaseType') AS BaseType"
            Dim param = New With {Key .testParam = "TestString"}

            Dim result01 = connection.Query(Of String)(sql, param).FirstOrDefault()
            Assert.AreEqual("nvarchar", result01)

            SqlMapper.PurgeQueryCache()

            SqlMapper.AddTypeMap(GetType(String), DbType.AnsiString) ' Change Default String Handling to AnsiString
            Dim result02 = connection.Query(Of String)(sql, param).FirstOrDefault()
            Assert.AreEqual("varchar", result02)

            SqlMapper.PurgeQueryCache()
            SqlMapper.AddTypeMap(GetType(String), DbType.String) ' Restore Default to Unicode String
        End Sub

        <TestMethod>
        Public Sub TestChangingDefaultStringTypeMappingToAnsiStringFirstOrDefault()
            Const sql As String = "SELECT SQL_VARIANT_PROPERTY(CONVERT(sql_variant, @testParam),'BaseType') AS BaseType"
            Dim param = New With {Key .testParam = "TestString"}

            Dim result01 = connection.QueryFirstOrDefault(Of String)(sql, param)
            Assert.AreEqual("nvarchar", result01)

            SqlMapper.PurgeQueryCache()

            SqlMapper.AddTypeMap(GetType(String), DbType.AnsiString) ' Change Default String Handling to AnsiString
            Dim result02 = connection.QueryFirstOrDefault(Of String)(sql, param)
            Assert.AreEqual("varchar", result02)

            SqlMapper.PurgeQueryCache()
            SqlMapper.AddTypeMap(GetType(String), DbType.String) ' Restore Default to Unicode String
        End Sub

        <TestMethod>
        Public Sub TestCustomTypeMap()
            ' default mapping
            Dim item = connection.Query(Of TypeWithMapping)("Select 'AVal' as A, 'BVal' as B").Single()
            Assert.AreEqual("AVal", item.A)
            Assert.AreEqual("BVal", item.B)

            ' custom mapping
            Dim map = New CustomPropertyTypeMap(GetType(TypeWithMapping), Function(type, columnName) type.GetProperties().FirstOrDefault(Function(prop) GetDescriptionFromAttribute(prop) = columnName))
            SqlMapper.SetTypeMap(GetType(TypeWithMapping), map)

            item = connection.Query(Of TypeWithMapping)("Select 'AVal' as A, 'BVal' as B").Single()
            Assert.AreEqual("BVal", item.A)
            Assert.AreEqual("AVal", item.B)

            ' reset to default
            SqlMapper.SetTypeMap(GetType(TypeWithMapping), Nothing)
            item = connection.Query(Of TypeWithMapping)("Select 'AVal' as A, 'BVal' as B").Single()
            Assert.AreEqual("AVal", item.A)
            Assert.AreEqual("BVal", item.B)
        End Sub

        Private Shared Function GetDescriptionFromAttribute(ByVal member As MemberInfo) As String
            If member Is Nothing Then
                Return Nothing
            End If
            Dim attrib = CType(Attribute.GetCustomAttribute(member, GetType(ComponentModel.DescriptionAttribute), False), ComponentModel.DescriptionAttribute)
            Return attrib?.Description
        End Function

        Public Class TypeWithMapping
            <ComponentModel.Description("B")>
            Public Property A() As String

            <ComponentModel.Description("A")>
            Public Property B() As String
        End Class

        <TestMethod>
        Public Sub Issue136_ValueTypeHandlers()
            SqlMapper.ResetTypeHandlers()
            SqlMapper.AddTypeHandler(GetType(LocalDate), LocalDateHandler.Default)
            Dim param = New LocalDateResult With {.NotNullable = New LocalDate With {.Year = 2014, .Month = 7, .Day = 25}, .NullableNotNull = New LocalDate With {.Year = 2014, .Month = 7, .Day = 26}, .NullableIsNull = Nothing}

            Dim result = connection.Query(Of LocalDateResult)("SELECT @NotNullable AS NotNullable, @NullableNotNull AS NullableNotNull, @NullableIsNull AS NullableIsNull", param).Single()

            SqlMapper.ResetTypeHandlers()
            SqlMapper.AddTypeHandler(GetType(LocalDate?), LocalDateHandler.Default)
            result = connection.Query(Of LocalDateResult)("SELECT @NotNullable AS NotNullable, @NullableNotNull AS NullableNotNull, @NullableIsNull AS NullableIsNull", param).Single()
        End Sub

        <CLSCompliant(False)>
        Public Class LocalDateHandler
            Inherits SqlMapper.TypeHandler(Of LocalDate)

            Private Sub New() ' private constructor
            End Sub

            ' Make the field type ITypeHandler to ensure it cannot be used with SqlMapper.AddTypeHandler<T>(TypeHandler<T>)
            ' by mistake.
            Public Shared ReadOnly [Default] As SqlMapper.ITypeHandler = New LocalDateHandler()

            Public Overrides Function Parse(ByVal value As Object) As LocalDate
                Dim [date] = CDate(value)
                Return New LocalDate With {.Year = [date].Year, .Month = [date].Month, .Day = [date].Day}
            End Function

            Public Overrides Sub SetValue(ByVal parameter As IDbDataParameter, ByVal value As LocalDate)
                parameter.DbType = DbType.DateTime
                parameter.Value = New Date(value.Year, value.Month, value.Day)
            End Sub
        End Class

        Public Structure LocalDate
            Public Property Year() As Integer
            Public Property Month() As Integer
            Public Property Day() As Integer
        End Structure

        Public Class LocalDateResult
            Public Property NotNullable() As LocalDate
            Public Property NullableNotNull() As LocalDate?
            Public Property NullableIsNull() As LocalDate?
        End Class

        <CLSCompliant(False)>
        Public Class LotsOfNumerics
            Public Enum E_Byte As Byte
                A = 0
                B = 1
            End Enum
            <CLSCompliant(False)>
            Public Enum E_SByte As SByte
                A = 0
                B = 1
            End Enum
            Public Enum E_Short As Short
                A = 0
                B = 1
            End Enum
            <CLSCompliant(False)>
            Public Enum E_UShort As UShort
                A = 0
                B = 1
            End Enum
            Public Enum E_Int As Integer
                A = 0
                B = 1
            End Enum
            <CLSCompliant(False)>
            Public Enum E_UInt As UInteger
                A = 0
                B = 1
            End Enum
            Public Enum E_Long As Long
                A = 0
                B = 1
            End Enum
            <CLSCompliant(False)>
            Public Enum E_ULong As ULong
                A = 0
                B = 1
            End Enum

            Public Property P_Byte() As E_Byte
            <CLSCompliant(False)>
            Public Property P_SByte() As E_SByte
            Public Property P_Short() As E_Short
            <CLSCompliant(False)>
            Public Property P_UShort() As E_UShort
            Public Property P_Int() As E_Int
            <CLSCompliant(False)>
            Public Property P_UInt() As E_UInt
            Public Property P_Long() As E_Long
            <CLSCompliant(False)>
            Public Property P_ULong() As E_ULong

            Public Property N_Bool() As Boolean
            Public Property N_Byte() As Byte
            <CLSCompliant(False)>
            Public Property N_SByte() As SByte
            Public Property N_Short() As Short
            <CLSCompliant(False)>
            Public Property N_UShort() As UShort
            Public Property N_Int() As Integer
            Public Property N_UInt() As UInteger
            Public Property N_Long() As Long
            Public Property N_ULong() As ULong

            Public Property N_Float() As Single
            Public Property N_Double() As Double
            Public Property N_Decimal() As Decimal

            Public Property N_P_Byte() As E_Byte?
            Public Property N_P_SByte() As E_SByte?
            Public Property N_P_Short() As E_Short?
            Public Property N_P_UShort() As E_UShort?
            Public Property N_P_Int() As E_Int?
            Public Property N_P_UInt() As E_UInt?
            Public Property N_P_Long() As E_Long?
            Public Property N_P_ULong() As E_ULong?

            Public Property N_N_Bool() As Boolean?
            Public Property N_N_Byte() As Byte?
            Public Property N_N_SByte() As SByte?
            Public Property N_N_Short() As Short?
            Public Property N_N_UShort() As UShort?
            Public Property N_N_Int() As Integer?
            Public Property N_N_UInt() As UInteger?
            Public Property N_N_Long() As Long?
            Public Property N_N_ULong() As ULong?

            Public Property N_N_Float() As Single?
            Public Property N_N_Double() As Double?
            Public Property N_N_Decimal() As Decimal?
        End Class

        <TestMethod>
        Public Sub TestBigIntForEverythingWorks()
            TestBigIntForEverythingWorks_ByDataType(Of Long)("bigint")
            TestBigIntForEverythingWorks_ByDataType(Of Integer)("int")
            TestBigIntForEverythingWorks_ByDataType(Of Byte)("tinyint")
            TestBigIntForEverythingWorks_ByDataType(Of Short)("smallint")
            TestBigIntForEverythingWorks_ByDataType(Of Boolean)("bit")
            TestBigIntForEverythingWorks_ByDataType(Of Single)("float(24)")
            TestBigIntForEverythingWorks_ByDataType(Of Double)("float(53)")
        End Sub

        Private Sub TestBigIntForEverythingWorks_ByDataType(Of T)(ByVal dbType As String)
            Using reader = connection.ExecuteReader("select cast(1 as " & dbType & ")")
                Assert.IsTrue(reader.Read())
                reader.GetFieldType(0).Equals(GetType(T))
                Assert.IsFalse(reader.Read())
                Assert.IsFalse(reader.NextResult())
            End Using

            Dim sql As String = "select " & String.Join(",", GetType(LotsOfNumerics).GetProperties().Select(Function(x) "cast (1 as " & dbType & ") as [" & x.Name & "]"))
            Dim row = connection.Query(Of LotsOfNumerics)(sql).Single()

            Assert.IsTrue(row.N_Bool)
            Assert.AreEqual(row.N_SByte, CSByte(1))
            Assert.AreEqual(row.N_Byte, CByte(1))
            Assert.AreEqual(row.N_Int, CInt(1))
            Assert.AreEqual(row.N_UInt, CUInt(1))
            Assert.AreEqual(row.N_Short, CShort(1))
            Assert.AreEqual(row.N_UShort, CUShort(1))
            Assert.AreEqual(row.N_Long, CLng(1))
            Assert.AreEqual(row.N_ULong, CULng(1))
            Assert.AreEqual(row.N_Float, CSng(1))
            Assert.AreEqual(row.N_Double, CDbl(1))
            Assert.AreEqual(row.N_Decimal, CDec(1))

            Assert.AreEqual(LotsOfNumerics.E_Byte.B, row.P_Byte)
            Assert.AreEqual(LotsOfNumerics.E_SByte.B, row.P_SByte)
            Assert.AreEqual(LotsOfNumerics.E_Short.B, row.P_Short)
            Assert.AreEqual(LotsOfNumerics.E_UShort.B, row.P_UShort)
            Assert.AreEqual(LotsOfNumerics.E_Int.B, row.P_Int)
            Assert.AreEqual(LotsOfNumerics.E_UInt.B, row.P_UInt)
            Assert.AreEqual(LotsOfNumerics.E_Long.B, row.P_Long)
            Assert.AreEqual(LotsOfNumerics.E_ULong.B, row.P_ULong)

            Assert.IsTrue(row.N_N_Bool.Value)
            Assert.AreEqual(row.N_N_SByte.Value, CSByte(1))
            Assert.AreEqual(row.N_N_Byte.Value, CByte(1))
            Assert.AreEqual(row.N_N_Int.Value, CInt(1))
            Assert.AreEqual(row.N_N_UInt.Value, CUInt(1))
            Assert.AreEqual(row.N_N_Short.Value, CShort(1))
            Assert.AreEqual(row.N_N_UShort.Value, CUShort(1))
            Assert.AreEqual(row.N_N_Long.Value, CLng(1))
            Assert.AreEqual(row.N_N_ULong.Value, CULng(1))
            Assert.AreEqual(row.N_N_Float.Value, CSng(1))
            Assert.AreEqual(row.N_N_Double.Value, CDbl(1))
            Assert.AreEqual(row.N_N_Decimal, CDec(1))

            Assert.AreEqual(LotsOfNumerics.E_Byte.B, row.N_P_Byte.Value)
            Assert.AreEqual(LotsOfNumerics.E_SByte.B, row.N_P_SByte.Value)
            Assert.AreEqual(LotsOfNumerics.E_Short.B, row.N_P_Short.Value)
            Assert.AreEqual(LotsOfNumerics.E_UShort.B, row.N_P_UShort.Value)
            Assert.AreEqual(LotsOfNumerics.E_Int.B, row.N_P_Int.Value)
            Assert.AreEqual(LotsOfNumerics.E_UInt.B, row.N_P_UInt.Value)
            Assert.AreEqual(LotsOfNumerics.E_Long.B, row.N_P_Long.Value)
            Assert.AreEqual(LotsOfNumerics.E_ULong.B, row.N_P_ULong.Value)

            TestBigIntForEverythingWorksGeneric(Of Boolean)(True, dbType)
            TestBigIntForEverythingWorksGeneric(Of SByte)(CSByte(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Byte)(CByte(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Integer)(CInt(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of UInteger)(CUInt(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Short)(CShort(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of UShort)(CUShort(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Long)(CLng(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of ULong)(CULng(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Single)(CSng(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Double)(CDbl(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Decimal)(CDec(1), dbType)

            TestBigIntForEverythingWorksGeneric(LotsOfNumerics.E_Byte.B, dbType)
            TestBigIntForEverythingWorksGeneric(LotsOfNumerics.E_SByte.B, dbType)
            TestBigIntForEverythingWorksGeneric(LotsOfNumerics.E_Int.B, dbType)
            TestBigIntForEverythingWorksGeneric(LotsOfNumerics.E_UInt.B, dbType)
            TestBigIntForEverythingWorksGeneric(LotsOfNumerics.E_Short.B, dbType)
            TestBigIntForEverythingWorksGeneric(LotsOfNumerics.E_UShort.B, dbType)
            TestBigIntForEverythingWorksGeneric(LotsOfNumerics.E_Long.B, dbType)
            TestBigIntForEverythingWorksGeneric(LotsOfNumerics.E_ULong.B, dbType)

            TestBigIntForEverythingWorksGeneric(Of Boolean?)(True, dbType)
            TestBigIntForEverythingWorksGeneric(Of SByte?)(CSByte(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Byte?)(CByte(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Integer?)(CInt(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of UInteger?)(CUInt(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Short?)(CShort(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of UShort?)(CUShort(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Long?)(CLng(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of ULong?)(CULng(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Single?)(CSng(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Double?)(CDbl(1), dbType)
            TestBigIntForEverythingWorksGeneric(Of Decimal?)(CDec(1), dbType)

            TestBigIntForEverythingWorksGeneric(Of LotsOfNumerics.E_Byte?)(LotsOfNumerics.E_Byte.B, dbType)
            TestBigIntForEverythingWorksGeneric(Of LotsOfNumerics.E_SByte?)(LotsOfNumerics.E_SByte.B, dbType)
            TestBigIntForEverythingWorksGeneric(Of LotsOfNumerics.E_Int?)(LotsOfNumerics.E_Int.B, dbType)
            TestBigIntForEverythingWorksGeneric(Of LotsOfNumerics.E_UInt?)(LotsOfNumerics.E_UInt.B, dbType)
            TestBigIntForEverythingWorksGeneric(Of LotsOfNumerics.E_Short?)(LotsOfNumerics.E_Short.B, dbType)
            TestBigIntForEverythingWorksGeneric(Of LotsOfNumerics.E_UShort?)(LotsOfNumerics.E_UShort.B, dbType)
            TestBigIntForEverythingWorksGeneric(Of LotsOfNumerics.E_Long?)(LotsOfNumerics.E_Long.B, dbType)
            TestBigIntForEverythingWorksGeneric(Of LotsOfNumerics.E_ULong?)(LotsOfNumerics.E_ULong.B, dbType)
        End Sub

        Private Sub TestBigIntForEverythingWorksGeneric(Of T)(ByVal expected As T, ByVal dbType As String)
            Dim query = connection.Query(Of T)("select cast(1 as " & dbType & ")").Single()
            Assert.AreEqual(query, expected)

            Dim scalar = connection.ExecuteScalar(Of T)("select cast(1 as " & dbType & ")")
            Assert.AreEqual(scalar, expected)
        End Sub

        <TestMethod>
        Public Sub TestSubsequentQueriesSuccess()
            Dim data0 = connection.Query(Of Fooz0)("select 1 as [Id] where 1 = 0").ToList()
            MyAssert.IsEmpty(data0)

            Dim data1 = connection.Query(Of Fooz1)(New CommandDefinition("select 1 as [Id] where 1 = 0", flags:=CommandFlags.Buffered)).ToList()
            MyAssert.IsEmpty(data1)

            Dim data2 = connection.Query(Of Fooz2)(New CommandDefinition("select 1 as [Id] where 1 = 0", flags:=CommandFlags.None)).ToList()
            MyAssert.IsEmpty(data2)

            data0 = connection.Query(Of Fooz0)("select 1 as [Id] where 1 = 0").ToList()
            MyAssert.IsEmpty(data0)

            data1 = connection.Query(Of Fooz1)(New CommandDefinition("select 1 as [Id] where 1 = 0", flags:=CommandFlags.Buffered)).ToList()
            MyAssert.IsEmpty(data1)

            data2 = connection.Query(Of Fooz2)(New CommandDefinition("select 1 as [Id] where 1 = 0", flags:=CommandFlags.None)).ToList()
            MyAssert.IsEmpty(data2)
        End Sub

        Private Class Fooz0
            Public Property Id() As Integer
        End Class

        Private Class Fooz1
            Public Property Id() As Integer
        End Class

        Private Class Fooz2
            Public Property Id() As Integer
        End Class

        <CLSCompliant(False)>
        Public Class RatingValueHandler
            Inherits SqlMapper.TypeHandler(Of RatingValue)

            Private Sub New()
            End Sub

            Public Shared ReadOnly [Default] As New RatingValueHandler()

            Public Overrides Function Parse(ByVal value As Object) As RatingValue
                If TypeOf value Is Integer Then
                    Return New RatingValue() With {.Value = CInt(Fix(value))}
                End If

                Throw New FormatException("Invalid conversion to RatingValue")
            End Function

            Public Overrides Sub SetValue(ByVal parameter As IDbDataParameter, ByVal value As RatingValue)
                ' ... null, range checks etc ...
                parameter.DbType = System.Data.DbType.Int32
                parameter.Value = value.Value
            End Sub
        End Class

        Public Class RatingValue
            Public Property Value() As Integer
            ' ... some other properties etc ...
        End Class

        Public Class MyResult
            Public Property CategoryName() As String
            Public Property CategoryRating() As RatingValue
        End Class

        <TestMethod>
        Public Sub SO24740733_TestCustomValueHandler()
            SqlMapper.AddTypeHandler(RatingValueHandler.Default)
            Dim foo = connection.Query(Of MyResult)("SELECT 'Foo' AS CategoryName, 200 AS CategoryRating").Single()

            Assert.AreEqual("Foo", foo.CategoryName)
            Assert.AreEqual(200, foo.CategoryRating.Value)
        End Sub

        <TestMethod>
        Public Sub SO24740733_TestCustomValueSingleColumn()
            SqlMapper.AddTypeHandler(RatingValueHandler.Default)
            Dim foo = connection.Query(Of RatingValue)("SELECT 200 AS CategoryRating").Single()

            Assert.AreEqual(200, foo.Value)
        End Sub

        <CLSCompliant(False)>
        Public Class StringListTypeHandler
            Inherits SqlMapper.TypeHandler(Of List(Of String))

            Private Sub New()
            End Sub

            Public Shared ReadOnly [Default] As New StringListTypeHandler()
            'Just a simple List<string> type handler implementation
            Public Overrides Sub SetValue(ByVal parameter As IDbDataParameter, ByVal value As List(Of String))
                parameter.Value = String.Join(",", value)
            End Sub

            Public Overrides Function Parse(ByVal value As Object) As List(Of String)
                Return (If((TryCast(value, String)), "")).Split(","c).ToList()
            End Function
        End Class

        Public Class MyObjectWithStringList
            Public Property Names() As List(Of String)
        End Class

        <TestMethod>
        Public Sub Issue253_TestIEnumerableTypeHandlerParsing()
            SqlMapper.ResetTypeHandlers()
            SqlMapper.AddTypeHandler(StringListTypeHandler.Default)
            Dim foo = connection.Query(Of MyObjectWithStringList)("SELECT 'Sam,Kyro' AS Names").Single()
            Assert.AreEqual({"Sam", "Kyro"}, foo.Names)
        End Sub

        <TestMethod>
        Public Sub Issue253_TestIEnumerableTypeHandlerSetParameterValue()
            SqlMapper.ResetTypeHandlers()
            SqlMapper.AddTypeHandler(StringListTypeHandler.Default)

            connection.Execute("CREATE TABLE #Issue253 (Names VARCHAR(50) NOT NULL);")
            Try
                Const names As String = "Sam,Kyro"
                Dim names_list As List(Of String) = names.Split(","c).ToList()
                Dim foo = connection.Query(Of String)("INSERT INTO #Issue253 (Names) VALUES (@Names); SELECT Names FROM #Issue253;", New With {Key .Names = names_list}).Single()
                Assert.AreEqual(foo, names)
            Finally
                connection.Execute("DROP TABLE #Issue253;")
            End Try
        End Sub

        <CLSCompliant(False)>
        Public Class RecordingTypeHandler(Of T)
            Inherits SqlMapper.TypeHandler(Of T)

            Public Overrides Sub SetValue(ByVal parameter As IDbDataParameter, ByVal value As T)
                SetValueWasCalled = True
                parameter.Value = value
            End Sub

            Public Overrides Function Parse(ByVal value As Object) As T
                ParseWasCalled = True
                Return CType(value, T)
            End Function

            Public Property SetValueWasCalled() As Boolean
            Public Property ParseWasCalled() As Boolean
        End Class

        <TestMethod>
        Public Sub Test_RemoveTypeMap()
            SqlMapper.ResetTypeHandlers()
            SqlMapper.RemoveTypeMap(GetType(Date))

            Dim dateTimeHandler = New RecordingTypeHandler(Of Date)()
            SqlMapper.AddTypeHandler(dateTimeHandler)

            connection.Execute("CREATE TABLE #Test_RemoveTypeMap (x datetime NOT NULL);")

            Try
                connection.Execute("INSERT INTO #Test_RemoveTypeMap VALUES (@Now)", New With {Key DateTime.Now})
                connection.Query(Of Date)("SELECT * FROM #Test_RemoveTypeMap")

                Assert.IsTrue(dateTimeHandler.ParseWasCalled)
                Assert.IsTrue(dateTimeHandler.SetValueWasCalled)
            Finally

                connection.Execute("DROP TABLE #Test_RemoveTypeMap")
                SqlMapper.AddTypeMap(GetType(Date), DbType.DateTime) ' or an option to reset type map?
            End Try
        End Sub

        <TestMethod>
        Public Sub TestReaderWhenResultsChange()
            Try
                connection.Execute("create table #ResultsChange (X int);create table #ResultsChange2 (Y int);insert #ResultsChange (X) values(1);insert #ResultsChange2 (Y) values(1);")

                Dim obj1 = connection.Query(Of ResultsChangeType)("select * from #ResultsChange").Single()
                Assert.AreEqual(1, obj1.X)
                Assert.AreEqual(0, obj1.Y)
                Assert.AreEqual(0, obj1.Z)

                Dim obj2 = connection.Query(Of ResultsChangeType)("select * from #ResultsChange rc inner join #ResultsChange2 rc2 on rc2.Y=rc.X").Single()
                Assert.AreEqual(1, obj2.X)
                Assert.AreEqual(1, obj2.Y)
                Assert.AreEqual(0, obj2.Z)

                connection.Execute("alter table #ResultsChange add Z int null")
                connection.Execute("update #ResultsChange set Z = 2")

                Dim obj3 = connection.Query(Of ResultsChangeType)("select * from #ResultsChange").Single()
                Assert.AreEqual(1, obj3.X)
                Assert.AreEqual(0, obj3.Y)
                Assert.AreEqual(2, obj3.Z)

                Dim obj4 = connection.Query(Of ResultsChangeType)("select * from #ResultsChange rc inner join #ResultsChange2 rc2 on rc2.Y=rc.X").Single()
                Assert.AreEqual(1, obj4.X)
                Assert.AreEqual(1, obj4.Y)
                Assert.AreEqual(2, obj4.Z)
            Finally
                connection.Execute("drop table #ResultsChange;drop table #ResultsChange2;")
            End Try
        End Sub

        Private Class ResultsChangeType
            Public Property X() As Integer
            Public Property Y() As Integer
            Public Property Z() As Integer
        End Class

        Public Class WrongTypes
            Public Property A() As Integer
            Public Property B() As Double
            Public Property C() As Long
            Public Property D() As Boolean
        End Class

        <TestMethod>
        Public Sub TestWrongTypes_WithRightTypes()
            Dim item = connection.Query(Of WrongTypes)("select 1 as A, cast(2.0 as float) as B, cast(3 as bigint) as C, cast(1 as bit) as D").Single()
            Assert.AreEqual(1, item.A)
            Assert.AreEqual(2.0, item.B)
            Assert.AreEqual(3L, item.C)
            Assert.IsTrue(item.D)
        End Sub

        <TestMethod>
        Public Sub TestWrongTypes_WithWrongTypes()
            Dim item = connection.Query(Of WrongTypes)("select cast(1.0 as float) as A, 2 as B, 3 as C, cast(1 as bigint) as D").Single()
            Assert.AreEqual(1, item.A)
            Assert.AreEqual(2.0, item.B)
            Assert.AreEqual(3L, item.C)
            Assert.IsTrue(item.D)
        End Sub

        <TestMethod>
        Public Sub SO24607639_NullableBools()
            Dim obj = connection.Query(Of HazBools)("declare @vals table (A bit null, B bit null, C bit null);" & ControlChars.CrLf & "                insert @vals (A,B,C) values (1,0,null);" & ControlChars.CrLf & "                select * from @vals").Single()
            Assert.IsNotNull(obj)
            Assert.IsTrue(obj.A.Value)
            Assert.IsFalse(obj.B.Value)
            Assert.IsNull(obj.C)
        End Sub

        Private Class HazBools
            Public Property A() As Boolean?
            Public Property B() As Boolean?
            Public Property C() As Boolean?
        End Class

        <TestMethod>
        Public Sub Issue130_IConvertible()
            Dim row As Object = connection.Query("select 1 as [a], '2' as [b]").Single()
            Dim a As Integer = row.a
            Dim b As String = row.b
            Assert.AreEqual(1, a)
            Assert.AreEqual("2", b)

            row = Connection.Query(Of Object)("select 3 as [a], '4' as [b]").Single()
            a = row.a
            b = row.b
            Assert.AreEqual(3, a)
            Assert.AreEqual("4", b)
        End Sub

        <TestMethod>
        Public Sub Issue149_TypeMismatch_SequentialAccess()
            Dim guid As Guid = System.Guid.Parse("cf0ef7ac-b6fe-4e24-aeda-a2b45bb5654e")
            Dim ex = MyAssert.Throws(Of Exception)(Function() Connection.Query(Of Issue149_Person)("select @guid as Id", New With {Key guid}).First())
            Assert.AreEqual("Error parsing column 0 (Id=cf0ef7ac-b6fe-4e24-aeda-a2b45bb5654e - Object)", ex.Message)
        End Sub

        Public Class Issue149_Person
            Public Property Id() As String
        End Class

        <TestMethod>
        Public Sub Issue295_NullableDateTime_SqlServerFunction()
            Common.TestDateTime(Connection)
        End Sub

        <TestMethod>
        Public Sub SO29343103_UtcDates()
            Const sql As String = "select @date"
            Dim [date] = Date.UtcNow
            Dim returned = connection.Query(Of Date)(sql, New With {Key [date]}).Single()
            Dim delta = returned - [date]
            Assert.IsTrue(delta.TotalMilliseconds >= -10 AndAlso delta.TotalMilliseconds <= 10)
        End Sub

        <TestMethod>
        Public Sub Issue461_TypeHandlerWorksInConstructor()
            SqlMapper.AddTypeHandler(New Issue461_BlargHandler())

            connection.Execute("CREATE TABLE #Issue461 (" & ControlChars.CrLf & "                                      Id                int not null IDENTITY(1,1)," & ControlChars.CrLf & "                                      SomeValue         nvarchar(50)," & ControlChars.CrLf & "                                      SomeBlargValue    nvarchar(200)," & ControlChars.CrLf & "                                    )")
            Const Expected As String = "abc123def"
            Dim blarg = New Blarg(Expected)
            connection.Execute("INSERT INTO #Issue461 (SomeValue, SomeBlargValue) VALUES (@value, @blarg)", New With {Key .value = "what up?", Key blarg})

            ' test: without constructor
            Dim parameterlessWorks = connection.QuerySingle(Of Issue461_ParameterlessTypeConstructor)("SELECT * FROM #Issue461")
            Assert.AreEqual(1, parameterlessWorks.Id)
            Assert.AreEqual("what up?", parameterlessWorks.SomeValue)
            Assert.AreEqual(parameterlessWorks.SomeBlargValue.Value, Expected)

            ' test: via constructor
            Dim parameterDoesNot = connection.QuerySingle(Of Issue461_ParameterisedTypeConstructor)("SELECT * FROM #Issue461")
            Assert.AreEqual(1, parameterDoesNot.Id)
            Assert.AreEqual("what up?", parameterDoesNot.SomeValue)
            Assert.AreEqual(parameterDoesNot.SomeBlargValue.Value, Expected)
        End Sub

        ' I would usually expect this to be a struct; using a class
        ' so that we can't pass unexpectedly due to forcing an unsafe cast - want
        ' to see an InvalidCastException if it is wrong
        Private Class Blarg
            Public Sub New(value As String)
                value = value
            End Sub
            Public Property Value As String
            Public Overrides Function ToString() As String
                Return Value
            End Function
        End Class

        Private Class Issue461_BlargHandler
            Inherits SqlMapper.TypeHandler(Of Blarg)
            Public Overrides Sub SetValue(parameter As IDbDataParameter, value As Blarg)
                parameter.Value = If((CType(value.Value, Object)), DBNull.Value)
            End Sub

            Public Overrides Function Parse(value As Object) As Blarg
                Dim s As String = If((value = Nothing OrElse TypeOf value Is DBNull), Nothing, Convert.ToString(value))
                Return New Blarg(s)
            End Function
        End Class


        Private Class Issue461_ParameterlessTypeConstructor
            Public Property Id() As Integer

            Public Property SomeValue() As String
            Public Property SomeBlargValue() As Blarg
        End Class

        Private Class Issue461_ParameterisedTypeConstructor
            Public Sub New(id As Integer, someValue As String, someBlargValue As Blarg)
                id = id
                someValue = someValue
                someBlargValue = someBlargValue
            End Sub

            Public ReadOnly Property Id() As Integer

            Public ReadOnly Property SomeValue() As String
            Public ReadOnly Property SomeBlargValue() As Blarg
        End Class
    End Class

End Namespace
