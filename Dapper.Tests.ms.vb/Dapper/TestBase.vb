﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Threading
Imports Dapper

Namespace DapperTests
	Public MustInherit Class TestBase
		Implements IDisposable

		Protected Shared ReadOnly IsAppVeyor As Boolean = Environment.GetEnvironmentVariable("Appveyor")?.ToUpperInvariant() = "TRUE"

        Public Shared Function ConnectionString() As String
            Return If(IsAppVeyor, "Server=(local)\SQL2016;Database=tempdb;User ID=sa;Password=Password12!", "Data Source=.;Initial Catalog=tempdb;Integrated Security=True")
        End Function

        <CLSCompliant(False)>
        Protected _connection As SqlConnection
        Protected Function Connection() As SqlConnection
            If _connection Is Nothing Then
                _connection = TestBase.GetOpenConnection()
            End If
            Return _connection
        End Function

        Public Shared Function GetOpenConnection(Optional ByVal mars As Boolean = False) As SqlConnection
            Dim cs As String = ConnectionString()
            If mars Then
                Dim scsb As New SqlConnectionStringBuilder(cs) With {.MultipleActiveResultSets = True}
                cs = scsb.ConnectionString
            End If
            Dim connection As SqlConnection = New SqlConnection(cs)
            connection.Open()
            Return connection
        End Function

		Public Function GetClosedConnection() As SqlConnection
            Dim conn As New SqlConnection(ConnectionString)
            If conn.State <> ConnectionState.Closed Then
				Throw New InvalidOperationException("should be closed!")
			End If
			Return conn
		End Function

        Protected Shared Property ActiveCulture() As CultureInfo
            Get
                Return Thread.CurrentThread.CurrentCulture
            End Get
            Set(ByVal value As CultureInfo)
                Thread.CurrentThread.CurrentCulture = value
            End Set
        End Property

        Shared Sub New()
            Console.WriteLine("Dapper: " & GetType(SqlMapper).AssemblyQualifiedName)
            Console.WriteLine("Using Connection string: {0}", TestBase.ConnectionString)
            Console.WriteLine($".NET: {Environment.Version}")
            Console.Write("Loading native assemblies for SQL types...")
            Try
                SqlServerTypesLoader.LoadNativeAssemblies(AppDomain.CurrentDomain.BaseDirectory)
                Console.WriteLine("done.")
            Catch ex As Exception
                Console.WriteLine("failed.")
                Console.Error.WriteLine(ex.Message)
            End Try
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
			_connection?.Dispose()
		End Sub
	End Class

    ' <CollectionDefinition(Name, DisableParallelization := True)> 
    Public Class NonParallelDefinition
        Inherits TestBase
        Public Const Name As String = "NonParallel"
    End Class

End Namespace
