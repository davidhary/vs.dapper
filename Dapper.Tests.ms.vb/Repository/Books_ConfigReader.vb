Namespace Global.isr.Dapper.Tests.RepositoryTests

    ''' <summary> An information class for the Pith tests. </summary>
    ''' <remarks>
    ''' David, 4/13/2018 <para>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    ''' Licensed under The MIT License.</para>
    ''' </remarks>
    Partial Friend Class ConfigReader

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Sub New()
            MyBase.New
        End Sub

        ''' <summary> Gets the reader. </summary>
        ''' <value> The reader. </value>
        Private Shared ReadOnly Property Reader As isr.Core.AppSettingsReader = isr.Core.AppSettingsReader.Get()

        ''' <summary> Gets the exists. </summary>
        ''' <value> The exists. </value>
        Public Shared ReadOnly Property Exists As Boolean
            Get
                Return ConfigReader.Reader.AppSettingBoolean()
            End Get
        End Property

        ''' <summary> Gets the verbose. </summary>
        ''' <value> The verbose. </value>
        Public Shared ReadOnly Property Verbose As Boolean
            Get
                Return ConfigReader.Reader.AppSettingBoolean()
            End Get
        End Property

        ''' <summary> Gets the connection string. </summary>
        ''' <value> The connection string. </value>
        Public Shared ReadOnly Property ConnectionString() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the 'insert' command. </summary>
        ''' <value> The 'insert' command. </value>
        Public Shared ReadOnly Property InsertCommand() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the insert command return identity. </summary>
        ''' <value> The insert command return identity. </value>
        Public Shared ReadOnly Property InsertCommandReturnIdentity() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the 'read all' command. </summary>
        ''' <value> The 'read all' command. </value>
        Public Shared ReadOnly Property ReadAllCommand() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the 'read one' command. </summary>
        ''' <value> The 'read one' command. </value>
        Public Shared ReadOnly Property ReadOneCommand() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the 'update' command. </summary>
        ''' <value> The 'update' command. </value>
        Public Shared ReadOnly Property UpdateCommand() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the 'delete' command. </summary>
        ''' <value> The 'delete' command. </value>
        Public Shared ReadOnly Property DeleteCommand() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the book name 1. </summary>
        ''' <value> The book name 1. </value>
        Public Shared ReadOnly Property BookName1() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the book name 2. </summary>
        ''' <value> The book name 2. </value>
        Public Shared ReadOnly Property BookName2() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the book isbn 1. </summary>
        ''' <value> The book isbn 1. </value>
        Public Shared ReadOnly Property BookISBN1() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the book isbn 2. </summary>
        ''' <value> The book isbn 2. </value>
        Public Shared ReadOnly Property BookISBN2() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

    End Class
End Namespace

