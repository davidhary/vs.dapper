Namespace Global.isr.Dapper.Tests.RepositoryTests

    ''' <summary> Asserts for using Dapper with a Books Repository. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 1/26/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class BooksRepositoryTests

        #Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _BooksRepository = New BooksRepository()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            If _TestInfo IsNot Nothing Then _TestInfo.Dispose() : _TestInfo = Nothing
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(ConfigReader.Exists, $"{GetType(ConfigReader)}.{NameOf(ConfigReader)} settings should exist")
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo?.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets or sets the test context which provides information about and functionality for the
        ''' current test run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext

        ''' <summary> Gets or sets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite

        #End Region

        #Region " BOOK TETS "

        ''' <summary> The books repository. </summary>
        Private Shared _BooksRepository As IRepository(Of Book) = Nothing

        Public Shared Function [Get]() As IEnumerable(Of Book)
            Return _BooksRepository.GetAll()
        End Function

        Public Shared Function [Get](ByVal id As Integer) As Book
            Return _BooksRepository.GetById(id)
        End Function

        ''' <summary> Adds book. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="book"> The book. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Shared Function Add(ByVal book As Book) As Boolean
            Dim result As Boolean = _BooksRepository.Add(book)
            Assert.IsTrue(result, $"Failed adding {book}")
            Return result
        End Function

        ''' <summary> Adds an entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="book"> The book. </param>
        Public Shared Sub AddEntity(ByVal book As Book)
            Dim result As Book = _BooksRepository.AddEntity(book)
            Assert.IsTrue(result.Id > 0, $"Failed adding {book}")
        End Sub

        ''' <summary> Updates this object. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="id">   The Identifier to delete. </param>
        ''' <param name="book"> The book. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Shared Function Update(ByVal id As Integer, ByVal book As Book) As Boolean
            book.Id = id
            Return _BooksRepository.Update(book)
        End Function

        ''' <summary> Deletes the given ID. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        '''                                      illegal values. </exception>
        ''' <param name="id"> The Identifier to delete. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Shared Function Delete(ByVal id As Integer) As Boolean
            If id <= 0 Then Throw New ArgumentException($"invalid id {id}", NameOf(id))
            Return _BooksRepository.Delete(id)
        End Function

        ''' <summary> (Unit Test Method) tests full blown exception. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod()>
        Public Sub BooksRepositoryAddTest()
            Try
                Dim book As New Book With {.BookName = ConfigReader.BookName1, .ISBN = ConfigReader.BookISBN1}
                BooksRepositoryTests.AddEntity(book)
                BooksRepositoryTests.Delete(book.Id)
            Catch
                Throw
            Finally
            End Try
        End Sub

        ''' <summary> (Unit Test Method) tests books repository delete. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod()>
        Public Sub BooksRepositoryDeleteTest()
            Try
                BooksRepositoryTests.Delete(2)
            Catch
                Throw
            Finally
            End Try
        End Sub

        #End Region

    End Class
End Namespace
