Namespace Global.isr.Dapper.Tests.RepositoryTests

    ''' <summary> A repository interface. </summary>
    ''' <remarks>
    ''' David, 8/6/2018, https://www.codeproject.com/script/Membership/View.aspx?mid=3402606. <para>
    ''' (c) 2018 Raul Rajat Singh. All rights reserved.</para><para>
    ''' Licensed under The MIT License.</para>
    ''' </remarks>
    Public Interface IRepository(Of T As Class)

        ''' <summary> Gets all. </summary>
        ''' <returns> all. </returns>
        Function GetAll() As List(Of T)

        ''' <summary> Adds entity. </summary>
        ''' <param name="entity"> The entity. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Function Add(ByVal entity As T) As Boolean

        ''' <summary> Adds entity. </summary>
        ''' <param name="entity"> The entity. </param>
        ''' <returns> The entity with the identity assigned. </returns>
        Function AddEntity(ByVal entity As T) As T

        ''' <summary> Gets by identifier. </summary>
        ''' <param name="id"> The Identifier to delete. </param>
        ''' <returns> The by identifier. </returns>
        Function GetById(ByVal id As Integer) As T

        ''' <summary> Updates the given entity. </summary>
        ''' <param name="entity"> The entity. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Function Update(ByVal entity As T) As Boolean

        ''' <summary> Deletes the given ID. </summary>
        ''' <param name="id"> The Identifier to delete. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Function Delete(ByVal id As Integer) As Boolean
    End Interface
End Namespace

