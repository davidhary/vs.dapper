Imports Dapper
Namespace Global.isr.Dapper.Tests.RepositoryTests

    ''' <summary> The books repository. </summary>
    ''' <remarks>
    ''' (c) 2018 Raul Rajat Singh. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 8/6/2018 </para>
    ''' </remarks>
    Public Class BooksRepository
        Implements IRepository(Of Book)

        ''' <summary> The database connection. </summary>
        Private ReadOnly _Dbconnection As System.Data.IDbConnection = Nothing

        ''' <summary> Default constructor. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Public Sub New()
            Me._Dbconnection = New System.Data.SqlClient.SqlConnection(ConfigReader.ConnectionString)
        End Sub

        ''' <summary> Adds entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="book"> The entity. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Function Add(ByVal book As Book) As Boolean Implements IRepository(Of Book).Add
            Dim sql As String = ConfigReader.InsertCommand
            Dim count As Integer = Me._Dbconnection.Execute(sql, book)
            Return count > 0
        End Function

        ''' <summary> Adds entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="book"> The book. </param>
        ''' <returns> The entity with the identity assigned. </returns>
        Public Function AddEntity(ByVal book As Book) As Book Implements IRepository(Of Book).AddEntity
            Dim sql As String = ConfigReader.InsertCommandReturnIdentity
            book.Id = Me._Dbconnection.Query(Of Int32)(sql, book).Single
            Return book
        End Function

        ''' <summary> Deletes the given ID. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="id"> The Identifier to delete. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
        Public Function Delete(ByVal id As Integer) As Boolean Implements IRepository(Of Book).Delete
            Dim sql As String = ConfigReader.DeleteCommand
            Dim count As Integer = Me._Dbconnection.Execute(sql, New With {Key .Id = id})
            Return count > 0
        End Function

        ''' <summary> Gets all. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> all. </returns>
        Public Function GetAll() As List(Of Book) Implements IRepository(Of Book).GetAll
            Dim sql As String = ConfigReader.ReadAllCommand
            Dim queryResult As IEnumerable(Of Book) = Me._Dbconnection.Query(Of Book)(sql)
            Return queryResult.ToList()
        End Function

        ''' <summary> Gets by identifier. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="id"> The Identifier to delete. </param>
        ''' <returns> The by identifier. </returns>
        Public Function GetById(ByVal id As Integer) As Book Implements IRepository(Of Book).GetById
            Dim book As Book = Nothing
            Dim sql As String = ConfigReader.ReadOneCommand
            #Disable Warning IDE0037 ' Use inferred member name
            Dim queryResult As IEnumerable(Of Book) = Me._Dbconnection.Query(Of Book)(sql, New With {Key .Id = id})
            #Enable Warning IDE0037 ' Use inferred member name

            If queryResult IsNot Nothing Then
                book = queryResult.FirstOrDefault()
            End If
            Return book
        End Function

        ''' <summary> Updates the given entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="book"> The entity. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Function Update(ByVal book As Book) As Boolean Implements IRepository(Of Book).Update
            Dim sql As String = ConfigReader.UpdateCommand
            Dim count As Integer = Me._Dbconnection.Execute(sql, book)
            Return count > 0
        End Function
    End Class

End Namespace
