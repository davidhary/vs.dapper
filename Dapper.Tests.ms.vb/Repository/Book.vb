Namespace Global.isr.Dapper.Tests.RepositoryTests

    ''' <summary> A book. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Book

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer

        ''' <summary> Gets or sets the name of the book. </summary>
        ''' <value> The name of the book. </value>
        Public Property BookName() As String

        ''' <summary> Gets or sets the isbn. </summary>
        ''' <value> The isbn. </value>
        Public Property ISBN() As String

        ''' <summary> Returns a string that represents the current object. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A string that represents the current object. </returns>
        Public Overrides Function ToString() As String
            Return $"{Me.Id}.{Me.BookName}.{Me.ISBN}"
        End Function

    End Class

End Namespace
