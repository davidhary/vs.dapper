Imports Dapper.Contrib.Extensions
#Disable Warning IDE1006 ' Naming Styles
Namespace Global.isr.Dapper.Tests.ContribTests
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Interface for object zz. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Interface IObjectZZ

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        <ExplicitKey>
        Property Id() As String

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Property Name() As String

        ''' <summary> Gets or sets the is active. </summary>
        ''' <value> The is active. </value>
        Property IsActive() As Boolean

        ''' <summary> Gets or sets the timestamp. </summary>
        ''' <value> The timestamp. </value>
        Property Timestamp() As DateTime
    End Interface

    ''' <summary> An object zz. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    <Table("ObjectZZ")>
    Public Class ObjectZZ
        Implements IObjectZZ

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        <ExplicitKey>
        Public Property Id() As String Implements IObjectZZ.Id

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String Implements IObjectZZ.Name

        ''' <summary> Gets or sets the is active. </summary>
        ''' <value> The is active. </value>
        Public Property IsActive() As Boolean Implements IObjectZZ.IsActive

        ''' <summary> Gets or sets the timestamp. </summary>
        ''' <value> The timestamp. </value>
        Public Property Timestamp() As DateTime Implements IObjectZZ.Timestamp
    End Class

    ''' <summary> Interface for object x coordinate. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Interface IObjectX

        ''' <summary> Gets or sets the identifier of the object x coordinate. </summary>
        ''' <value> The identifier of the object x coordinate. </value>
        <ExplicitKey>
        Property ObjectXId() As String

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Property Name() As String
    End Interface

    ''' <summary> An object x coordinate. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    <Table("ObjectX")>
    Public Class ObjectX
        Implements IObjectX

        ''' <summary> Gets or sets the identifier of the object x coordinate. </summary>
        ''' <value> The identifier of the object x coordinate. </value>
        <ExplicitKey>
        Public Property ObjectXId() As String Implements IObjectX.ObjectXId

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String Implements IObjectX.Name
    End Class

    ''' <summary> An object y coordinate. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    <Table("ObjectY")>
    Public Class ObjectY

        ''' <summary> Gets or sets the identifier of the object y coordinate. </summary>
        ''' <value> The identifier of the object y coordinate. </value>
        <ExplicitKey>
        Public Property ObjectYId() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String
    End Class

    ''' <summary> An object z coordinate. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    <Table("ObjectZ")>
    Public Class ObjectZ

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        <ExplicitKey>
        Public Property Id() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String
    End Class

    ''' <summary> Interface for users. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Interface IUsers

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        <Key>
        Property Id() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Property Name() As String

        ''' <summary> Gets or sets the age. </summary>
        ''' <value> The age. </value>
        Property Age() As Integer
    End Interface

    ''' <summary> An users. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Users
        Implements IUsers

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer Implements IUsers.Id

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String Implements IUsers.Name

        ''' <summary> Gets or sets the age. </summary>
        ''' <value> The age. </value>
        Public Property Age() As Integer Implements IUsers.Age
    End Class

    ''' <summary> Interface for nullable date. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Interface INullableDate

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        <Key>
        Property Id() As Integer

        ''' <summary> Gets or sets the date value. </summary>
        ''' <value> The date value. </value>
        Property DateValue() As DateTime?
    End Interface

    ''' <summary> A nullable date. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class NullableDate
        Implements INullableDate

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer Implements INullableDate.Id

        ''' <summary> Gets or sets the date value. </summary>
        ''' <value> The date value. </value>
        Public Property DateValue() As DateTime? Implements INullableDate.DateValue
    End Class

    ''' <summary> A person. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Class Person

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String
    End Class

    ''' <summary> A stuff. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    <Table("Stuff")>
    Public Class Stuff

        ''' <summary> Gets or sets the identifier of the. </summary>
        ''' <value> The identifier of the. </value>
        <Key>
        Public Property TheId() As Short

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets or sets the created. </summary>
        ''' <value> The created. </value>
        Public Property Created() As DateTime?
    End Class

    ''' <summary> A car. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    <Table("Automobile")>
    Public Class Car

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets or sets the computed. </summary>
        ''' <value> The computed. </value>
        <Computed>
        Public Property Computed() As String
    End Class

    ''' <summary> A result. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    <Table("Result")>
    Public Class Result

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id() As Integer

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets or sets the order. </summary>
        ''' <value> The order. </value>
        Public Property Order() As Integer
    End Class

    ''' <summary> A t. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    <Table("GenericType")>
    Public Class GenericType(Of T)

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        <ExplicitKey>
        Public Property Id() As String

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String
    End Class

End Namespace
