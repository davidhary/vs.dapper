Imports System.Data.SqlServerCe
Imports System.Transactions

Imports Dapper
Imports Dapper.Contrib.Extensions
#Disable Warning IDE1006 ' Naming Styles
Namespace Global.isr.Dapper.Tests.ContribTests
    #Enable Warning IDE1006 ' Naming Styles

    ''' <summary> The test suite base class for Dapper Contrib functions. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 9/8/2018 </para>
    ''' </remarks>
    <TestClass()>
    Partial Public MustInherit Class TestSuite

        #Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestInitialize()>
        Public Sub MyTestInitialize()
            Assert.IsTrue(ConfigReader.Exists, $"{GetType(ConfigReader)}.{NameOf(ConfigReader)} settings should exist")
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo?.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets or sets the test context which provides information about and functionality for the
        ''' current test run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext

        ''' <summary> Gets or sets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite

        #End Region

        #Region " MUST INHERIT "

        ''' <summary> Gets the connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The connection. </returns>
        Public MustOverride Function GetConnection() As System.Data.IDbConnection

        ''' <summary> Gets open connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The open connection. </returns>
        Private Function GetOpenConnection() As System.Data.IDbConnection
            Dim connection As System.Data.IDbConnection = Me.GetConnection()
            connection.Open()
            Return connection
        End Function

        #End Region

    End Class

End Namespace
