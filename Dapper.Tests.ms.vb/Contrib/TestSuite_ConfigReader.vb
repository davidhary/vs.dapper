Imports isr.Core
#Disable Warning IDE1006 ' Naming Styles
Namespace Global.isr.Dapper.Tests.ContribTests
    #Enable Warning IDE1006 ' Naming Styles

    ''' <summary> An information class for the Pith tests. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 4/13/2018 </para>
    ''' </remarks>
    Partial Friend Class ConfigReader

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Sub New()
            MyBase.New
        End Sub

        ''' <summary> Gets the reader. </summary>
        ''' <value> The reader. </value>
        Private Shared ReadOnly Property Reader As isr.Core.AppSettingsReader = isr.Core.AppSettingsReader.Get()

        ''' <summary> Gets the exists. </summary>
        ''' <value> The exists. </value>
        Public Shared ReadOnly Property Exists As Boolean
            Get
                Return ConfigReader.Reader.AppSettingBoolean()
            End Get
        End Property

        ''' <summary> Gets the SQL server connection string. </summary>
        ''' <value> The SQL server connection string. </value>
        Public Shared ReadOnly Property SqlServerConnectionString() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the sq lite memory connection string. </summary>
        ''' <value> The sq lite memory connection string. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Shared ReadOnly Property SQLiteMemoryConnectionString() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the sq lite connection string. </summary>
        ''' <value> The sq lite connection string. </value>
        Public Shared ReadOnly Property SQLiteConnectionString() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the SQL ce connection string. </summary>
        ''' <value> The SQL ce connection string. </value>
        Public Shared ReadOnly Property SqlCeConnectionString() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

        ''' <summary> Gets the filename of the SQL ce file. </summary>
        ''' <value> The filename of the SQL ce file. </value>
        Public Shared ReadOnly Property SqlCeFileName() As String
            Get
                Return ConfigReader.Reader.AppSettingValue()
            End Get
        End Property

    End Class
End Namespace
