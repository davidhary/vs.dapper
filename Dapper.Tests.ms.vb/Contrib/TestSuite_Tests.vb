Imports System.Data.SqlServerCe
Imports System.Transactions

Imports Dapper
Imports Dapper.Contrib.Extensions
Namespace Global.isr.Dapper.Tests.ContribTests

    Partial Public MustInherit Class TestSuite

        #Region " CONNECTION "

        ''' <summary> (Unit Test Method) tests closed connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub TestClosedConnection()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Assert.IsTrue(connection.Insert(New Users With {.Name = "Adama", .Age = 10}) > 0)
                Dim users As IEnumerable(Of Users) = connection.GetAll(Of Users)()
                Assert.IsTrue(users.Any)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) queries if a given connection exists. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub ConnectionExists()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Assert.IsTrue(connection.Exists(), "connection does not exist")
            End Using
        End Sub

        #End Region

        #Region " DATA TABLES "

        ''' <summary> (Unit Test Method) queries if a given table exists. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub TableExists()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                ' need to use provider bases tests here.
                'Assert.IsTrue(connection.TableExists("User"), "table users does not exist")
                'Assert.IsFalse(connection.TableExists("User1"), "table user1 exists")
            End Using
        End Sub

        ''' <summary> (Unit Test Method) test cars against Automobiles table. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub TableName()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                ' tests against "Automobiles" table (Table attribute)
                Dim id As Long = connection.Insert(New Car With {.Name = "Volvo"})
                Dim car As Car = connection.Get(Of Car)(id)
                Assert.IsNotNull(car)
                Assert.AreEqual("Volvo", car.Name)
                Assert.AreEqual("Volvo", connection.Get(Of Car)(id).Name)
                Assert.IsTrue(connection.Update(New Car With {.Id = CInt(Fix(id)), .Name = "Saab"}))
                Assert.AreEqual("Saab", connection.Get(Of Car)(id).Name)
                Assert.IsTrue(connection.Delete(New Car With {.Id = CInt(Fix(id))}))
                Assert.IsNull(connection.Get(Of Car)(id))
            End Using
        End Sub

        ''' <summary> (Unit Test Method) asynchronous test cars against Automobiles table. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function TableNameAsync() As Task
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                ' tests against "Automobiles" table (Table attribute)
                Dim id As Integer = Await connection.InsertAsync(New Car With {.Name = "VolvoAsync"}).ConfigureAwait(False)
                Dim car As Car = Await connection.GetAsync(Of Car)(id).ConfigureAwait(False)
                Assert.IsNotNull(car)
                Assert.AreEqual("VolvoAsync", car.Name)
                Assert.IsTrue(Await connection.UpdateAsync(New Car With {.Id = id, .Name = "SaabAsync"}).ConfigureAwait(False))
                Assert.AreEqual("SaabAsync", (Await connection.GetAsync(Of Car)(id).ConfigureAwait(False)).Name)
                Assert.IsTrue(Await connection.DeleteAsync(New Car With {.Id = id}).ConfigureAwait(False))
                Assert.IsNull(Await connection.GetAsync(Of Car)(id).ConfigureAwait(False))
            End Using
        End Function

        #End Region

        #Region " GET "

        ''' <summary> (Unit Test Method) tests simple get using the User table. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub TestSimpleGet()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Dim id As Long = connection.Insert(New Users With {.Name = "Adama", .Age = 10})
                Dim user As Users = connection.Get(Of Users)(id)
                Assert.AreEqual(user.Id, CInt(Fix(id)))
                Assert.AreEqual("Adama", user.Name)
                connection.Delete(user)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) tests simple get asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function TestSimpleGetAsync() As Task
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Dim id As Integer = Await connection.InsertAsync(New Users With {.Name = "Adama", .Age = 10}).ConfigureAwait(False)
                Dim user As Users = Await connection.GetAsync(Of Users)(id).ConfigureAwait(False)
                Assert.AreEqual(id, user.Id)
                Assert.AreEqual("Adama", user.Name)
                Await connection.DeleteAsync(user).ConfigureAwait(False)
            End Using
        End Function

        ''' <summary> (Unit Test Method) gets all with explicit key. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub GetAllWithExplicitKey()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.DeleteAll(Of ObjectX)
                Dim guid As String = System.Guid.NewGuid().ToString()
                Dim o1 As ObjectX = New ObjectX With {.ObjectXId = guid, .Name = "Foo1"}
                connection.Insert(o1)
                o1 = New ObjectX With {.ObjectXId = System.Guid.NewGuid().ToString(), .Name = "Foo2"}
                connection.Insert(o1)
                Dim objectXs As IEnumerable(Of ObjectX) = connection.GetAll(Of ObjectX)().ToList()
                Assert.IsTrue(objectXs.Count >= 2)
                Assert.AreEqual(1, objectXs.Count(Function(x) x.ObjectXId = guid))
            End Using
        End Sub

        ''' <summary> (Unit Test Method) gets all explicit key. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub GetAllExplicitKey()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.DeleteAll(Of ObjectZZ)
                Dim guid As String = System.Guid.NewGuid().ToString()
                ' default values not working!
                Dim o1 As IObjectZZ = New ObjectZZ With {.Id = guid, .Name = "Foo1", .IsActive = True, .Timestamp = DateTime.UtcNow}
                connection.Insert(o1)
                o1 = connection.Get(Of ObjectZZ)(o1.Id)
                Assert.IsTrue(o1.IsActive, $"{NameOf(IObjectZZ.IsActive)} should default to True")
                ' default values not working!
                o1 = New ObjectZZ With {.Id = System.Guid.NewGuid().ToString(), .Name = "Foo2", .IsActive = True, .Timestamp = DateTime.UtcNow}
                connection.Insert(o1)
                Dim objectXs As IEnumerable(Of ObjectZZ) = connection.GetAll(Of ObjectZZ)().ToList()
                Assert.IsTrue(objectXs.Count >= 2)
                Assert.AreEqual(1, objectXs.Count(Function(x) x.Id = guid))
            End Using
        End Sub

        ''' <summary> (Unit Test Method) gets all. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub GetAll()
            Const numberOfEntities As Integer = 10

            Dim users As List(Of Users) = New List(Of Users)()
            For i As Integer = 0 To numberOfEntities - 1
                users.Add(New Users With {.Name = "User " & i, .Age = i})
            Next i

            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.DeleteAll(Of Users)()

                Dim total As Long = connection.Insert(users)
                Assert.AreEqual(CInt(total), numberOfEntities)
                users = connection.GetAll(Of Users)().ToList()
                Assert.AreEqual(users.Count, numberOfEntities)
                Dim iusers As IEnumerable(Of IUsers) = connection.GetAll(Of IUsers)().ToList()
                Assert.AreEqual(iusers.Count, numberOfEntities)
                For i As Integer = 0 To numberOfEntities - 1
                    Assert.AreEqual(iusers(i).Age, i)
                Next i
            End Using
        End Sub

        ''' <summary> (Unit Test Method) gets all asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> all asynchronous. </returns>
        <TestMethod>
        Public Async Function GetAllAsync() As Task
            Const numberOfEntities As Integer = 10

            Dim users As New List(Of Users)()
            For i As Integer = 0 To numberOfEntities - 1
                users.Add(New Users With {.Name = "User " & i, .Age = i})
            Next i

            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Await connection.DeleteAllAsync(Of Users)().ConfigureAwait(False)

                Dim total As Integer = Await connection.InsertAsync(users).ConfigureAwait(False)
                Assert.AreEqual(total, numberOfEntities)
                users = CType(Await connection.GetAllAsync(Of Users)().ConfigureAwait(False), List(Of Users))
                Assert.AreEqual(users.Count, numberOfEntities)
                Dim iusers As IEnumerable(Of IUsers) = Await connection.GetAllAsync(Of IUsers)().ConfigureAwait(False)
                Assert.AreEqual(iusers.ToList().Count, numberOfEntities)
            End Using
        End Function

        ''' <summary> (Unit Test Method) gets and get all with nullable values. Issue 933. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub GetAndGetAllWithNullableValues()
            ' Test for issue #933
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Dim id1 As Long = connection.Insert(New NullableDate With {.DateValue = New Date(2011, 7, 14)})
                Dim id2 As Long = connection.Insert(New NullableDate With {.DateValue = Nothing})
                Dim value1 As INullableDate = connection.Get(Of INullableDate)(id1)
                Assert.AreEqual(New Date(2011, 7, 14), value1.DateValue.Value)
                Dim value2 As INullableDate = connection.Get(Of INullableDate)(id2)
                Assert.IsTrue(value2.DateValue Is Nothing)
                Dim value3 As List(Of INullableDate) = connection.GetAll(Of INullableDate)().ToList()
                Assert.AreEqual(New Date(2011, 7, 14), value3(0).DateValue.Value)
                Assert.IsTrue(value3(1).DateValue Is Nothing)
            End Using
        End Sub

        ''' <summary>
        ''' (Unit Test Method) gets asynchronous and get all asynchronous with nullable values.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The asynchronous and get all asynchronous with nullable values. </returns>
        <TestMethod>
        Public Async Function GetAsyncAndGetAllAsyncWithNullableValues() As Task
            ' Test for issue #933
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Dim id1 As Long = connection.Insert(New NullableDate With {.DateValue = New Date(2011, 7, 14)})
                Dim id2 As Long = connection.Insert(New NullableDate With {.DateValue = Nothing})
                Dim value1 As INullableDate = Await connection.GetAsync(Of INullableDate)(id1).ConfigureAwait(False)
                Assert.AreEqual(New Date(2011, 7, 14), value1.DateValue.Value)
                Dim value2 As INullableDate = Await connection.GetAsync(Of INullableDate)(id2).ConfigureAwait(False)
                Assert.IsTrue(value2.DateValue Is Nothing)
                Dim value3 As IEnumerable(Of INullableDate) = Await connection.GetAllAsync(Of INullableDate)().ConfigureAwait(False)
                Dim valuesList As IEnumerable(Of INullableDate) = value3.ToList()
                Assert.AreEqual(New Date(2011, 7, 14), valuesList(0).DateValue.Value)
                Assert.IsTrue(valuesList(1).DateValue Is Nothing)
            End Using
        End Function

        #End Region

        #Region " INSERT "

        ''' <summary> (Unit Test Method) inserts a check key. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub InsertCheckKey()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Assert.IsNull(connection.Get(Of IUsers)(3))
                Dim user As Users = New Users With {.Name = "Adamb", .Age = 10}
                Dim id As Integer = CInt(Fix(connection.Insert(user)))
                Assert.AreEqual(user.Id, id)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) inserts a check key asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function InsertCheckKeyAsync() As Task
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Await connection.DeleteAllAsync(Of Users)().ConfigureAwait(False)

                Assert.IsNull(Await connection.GetAsync(Of IUsers)(3).ConfigureAwait(False))
                Dim user As Users = New Users With {.Name = "Adamb", .Age = 10}
                Dim id As Integer = Await connection.InsertAsync(user).ConfigureAwait(False)
                Assert.AreEqual(user.Id, id)
            End Using
        End Function

        ''' <summary> (Unit Test Method) short identity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub ShortIdentity()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Const name As String = "First item"
                Dim id As Long = connection.Insert(New Stuff With {.Name = name})
                Assert.IsTrue(id > 0) ' 1-n are valid here, due to parallel tests
                Dim item As Stuff = connection.Get(Of Stuff)(id)
                Assert.AreEqual(item.TheId, CShort(Fix(id)))
                Assert.AreEqual(item.Name, name)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) null date time. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub NullDatetime()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.Insert(New Stuff With {.Name = "First item"})
                connection.Insert(New Stuff With {.Name = "Second item", .Created = DateTime.Now})
                Dim stuff As IEnumerable(Of Stuff) = connection.Query(Of Stuff)("select * from Stuff").ToList()
                Assert.IsNull(stuff(0).Created)
                Assert.IsNotNull(stuff.Last().Created)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) inserts a field with reserved name. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub InsertFieldWithReservedName()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.DeleteAll(Of Users)()
                Dim id As Long = connection.Insert(New Result() With {.Name = "Adam", .Order = 1})
                Dim result As Result = connection.Get(Of Result)(id)
                Assert.AreEqual(1, result.Order)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) inserts a field with reserved name asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function InsertFieldWithReservedNameAsync() As Task
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Await connection.DeleteAllAsync(Of Users)().ConfigureAwait(False)
                Dim id As Long = Await connection.InsertAsync(New Result With {.Name = "Adam", .Order = 1}).ConfigureAwait(False)

                Dim result As Result = Await connection.GetAsync(Of Result)(id).ConfigureAwait(False)
                Assert.AreEqual(1, result.Order)
            End Using
        End Function

        ''' <summary> (Unit Test Method) inserts an enumerable. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub InsertEnumerable()
            Me.InsertHelper(Function(src) src.AsEnumerable())
        End Sub

        ''' <summary> (Unit Test Method) inserts an array. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub InsertArray()
            Me.InsertHelper(Function(src) src.ToArray())
        End Sub

        ''' <summary> (Unit Test Method) inserts a list. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub InsertList()
            Me.InsertHelper(Function(src) src.ToList())
        End Sub

        ''' <summary> Helper method that insert. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="helper"> The helper. </param>
        Private Sub InsertHelper(Of T As Class)(ByVal helper As Func(Of IEnumerable(Of Users), T))
            Const numberOfEntities As Integer = 10

            Dim users As List(Of Users) = New List(Of Users)()
            For i As Integer = 0 To numberOfEntities - 1
                users.Add(New Users With {.Name = "User " & i, .Age = i})
            Next i

            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.DeleteAll(Of Users)()
                Dim total As Long = connection.Insert(helper(users))
                Assert.AreEqual(CInt(total), numberOfEntities)
                users = connection.Query(Of Users)("select * from Users").ToList()
                Assert.AreEqual(users.Count, numberOfEntities)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) inserts an enumerable asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function InsertEnumerableAsync() As Task
            Await Me.InsertHelperAsync(Function(src) src.AsEnumerable()).ConfigureAwait(False)
        End Function

        ''' <summary> (Unit Test Method) inserts an array asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function InsertArrayAsync() As Task
            Await Me.InsertHelperAsync(Function(src) src.ToArray()).ConfigureAwait(False)
        End Function

        ''' <summary> (Unit Test Method) inserts a list asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function InsertListAsync() As Task
            Await Me.InsertHelperAsync(Function(src) src.ToList()).ConfigureAwait(False)
        End Function

        ''' <summary> Inserts a helper asynchronous described by helper. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="helper"> The helper. </param>
        ''' <returns> A Task. </returns>
        Private Async Function InsertHelperAsync(Of T As Class)(ByVal helper As Func(Of IEnumerable(Of Users), T)) As Task
            Const numberOfEntities As Integer = 10

            Dim users As New List(Of Users)()
            For i As Integer = 0 To numberOfEntities - 1
                users.Add(New Users With {.Name = "User " & i, .Age = i})
            Next i

            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Await connection.DeleteAllAsync(Of Users)().ConfigureAwait(False)
                Dim total As Long = Await connection.InsertAsync(helper(users)).ConfigureAwait(False)
                Assert.AreEqual(CInt(total), numberOfEntities)
                users = connection.Query(Of Users)("select * from Users").ToList()
                Assert.AreEqual(users.Count, numberOfEntities)
            End Using
        End Function

        ''' <summary>
        ''' (This method is obsolete) (Unit Test Method) inserts a with custom database type.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        <TestMethod, Ignore(), Obsolete("Not parallel friendly - thinking about how to test this")>
        Public Sub InsertWithCustomDbType()
            SqlMapperExtensions.GetDatabaseType = Function(conn) "SQLiteConnection"

            Dim sqliteCodeCalled As Boolean = False
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.DeleteAll(Of Users)()
                Assert.IsNull(connection.Get(Of Users)(3))
                Try
                    connection.Insert(New Users With {.Name = "Adam", .Age = 10})
                Catch ex As SqlCeException
                    sqliteCodeCalled = ex.Message.IndexOf("There was an error parsing the query", StringComparison.OrdinalIgnoreCase) >= 0
                End Try
            End Using
            SqlMapperExtensions.GetDatabaseType = Nothing

            If Not sqliteCodeCalled Then
                Throw New InvalidOperationException("Was expecting SQLite code to be called")
            End If
        End Sub

        ''' <summary> (Unit Test Method) inserts a with custom table name mapper. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub InsertWithCustomTableNameMapper()
            SqlMapperExtensions.TableNameMapper = Function(type)
                                                      Select Case type.Name()
                                                          Case "Person"
                                                              Return "People"
                                                          Case Else
                                                              Dim tableattr As TableAttribute = TryCast(type.GetCustomAttributes(False).SingleOrDefault(Function(attr) attr.GetType().Name = "TableAttribute"), TableAttribute)
                                                              If tableattr IsNot Nothing Then
                                                                  Return tableattr.Name
                                                              End If
                                                              Dim name As String = type.Name & "s"
                                                              Return If(type.IsInterface() AndAlso name.StartsWith("I", StringComparison.OrdinalIgnoreCase),
                                                                        name.Substring(1), name)
                                                      End Select
                                                  End Function

            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Dim id As Long = connection.Insert(New Person With {.Name = "Mr Mapper"})
                Assert.AreEqual(1, CInt(id))
                connection.GetAll(Of Person)()
            End Using
        End Sub

        #End Region

        #Region " DELETE "

        ''' <summary> (Unit Test Method) deletes all. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub DeleteAll()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Dim id1 As Long = connection.Insert(New Users With {.Name = "Alice", .Age = 32})
                Dim id2 As Long = connection.Insert(New Users With {.Name = "Bob", .Age = 33})
                Assert.IsTrue(connection.DeleteAll(Of Users)())
                Assert.IsNull(connection.Get(Of Users)(id1))
                Assert.IsNull(connection.Get(Of Users)(id2))
            End Using
        End Sub

        ''' <summary> (Unit Test Method) deletes all asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function DeleteAllAsync() As Task
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Await connection.DeleteAllAsync(Of Users)().ConfigureAwait(False)

                Dim id1 As Long = Await connection.InsertAsync(New Users With {.Name = "Alice", .Age = 32}).ConfigureAwait(False)
                Dim id2 As Long = Await connection.InsertAsync(New Users With {.Name = "Bob", .Age = 33}).ConfigureAwait(False)
                Await connection.DeleteAllAsync(Of Users)().ConfigureAwait(False)
                Assert.IsNull(Await connection.GetAsync(Of Users)(id1).ConfigureAwait(False))
                Assert.IsNull(Await connection.GetAsync(Of Users)(id2).ConfigureAwait(False))
            End Using
        End Function

        ''' <summary> (Unit Test Method) deletes the enumerable. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub DeleteEnumerable()
            Me.DeleteHelper(Function(src) src.AsEnumerable())
        End Sub

        ''' <summary> (Unit Test Method) deletes the array. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub DeleteArray()
            Me.DeleteHelper(Function(src) src.ToArray())
        End Sub

        ''' <summary> (Unit Test Method) deletes the list. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub DeleteList()
            Me.DeleteHelper(Function(src) src.ToList())
        End Sub

        ''' <summary> Helper method that delete. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="helper"> The helper. </param>
        Private Sub DeleteHelper(Of T As Class)(ByVal helper As Func(Of IEnumerable(Of Users), T))
            Const numberOfEntities As Integer = 10

            Dim users As List(Of Users) = New List(Of Users)()
            For i As Integer = 0 To numberOfEntities - 1
                users.Add(New Users With {.Name = "User " & i, .Age = i})
            Next i

            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.DeleteAll(Of Users)()
                Dim total As Long = connection.Insert(helper(users))
                Assert.AreEqual(CInt(total), numberOfEntities)
                users = connection.Query(Of Users)("select * from Users").ToList()
                Assert.AreEqual(users.Count, numberOfEntities)

                Dim usersToDelete As IEnumerable(Of Users) = users.Take(10).ToList()
                connection.Delete(helper(usersToDelete))
                users = connection.Query(Of Users)("select * from Users").ToList()
                Assert.AreEqual(users.Count, numberOfEntities - 10)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) deletes the enumerable asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function DeleteEnumerableAsync() As Task
            Await Me.DeleteHelperAsync(Function(src) src.AsEnumerable()).ConfigureAwait(False)
        End Function

        ''' <summary> (Unit Test Method) deletes the array asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function DeleteArrayAsync() As Task
            Await Me.DeleteHelperAsync(Function(src) src.ToArray()).ConfigureAwait(False)
        End Function

        ''' <summary> (Unit Test Method) deletes the list asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function DeleteListAsync() As Task
            Await Me.DeleteHelperAsync(Function(src) src.ToList()).ConfigureAwait(False)
        End Function

        ''' <summary> Deletes the helper asynchronous described by helper. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="helper"> The helper. </param>
        ''' <returns> A Task. </returns>
        Private Async Function DeleteHelperAsync(Of T As Class)(ByVal helper As Func(Of IEnumerable(Of Users), T)) As Task
            Const numberOfEntities As Integer = 10

            Dim users As New List(Of Users)()
            For i As Integer = 0 To numberOfEntities - 1
                users.Add(New Users With {.Name = "User " & i, .Age = i})
            Next i

            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Await connection.DeleteAllAsync(Of Users)().ConfigureAwait(False)

                Dim total As Long = Await connection.InsertAsync(helper(users)).ConfigureAwait(False)
                Assert.AreEqual(CInt(total), numberOfEntities)
                users = connection.Query(Of Users)("select * from Users").ToList()
                Assert.AreEqual(users.Count, numberOfEntities)
                Dim usersToDelete As List(Of Users) = users.Take(10).ToList()
                Await connection.DeleteAsync(helper(usersToDelete)).ConfigureAwait(False)
                users = connection.Query(Of Users)("select * from Users").ToList()
                Assert.AreEqual(users.Count, numberOfEntities - 10)
            End Using
        End Function

        #End Region

        #Region " UPDATE "

        ''' <summary> (Unit Test Method) tests dual update. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub DualUpdateTest()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.DeleteAll(Of Users)()
                Assert.IsNull(connection.Get(Of Users)(3))
                'insert with computed attribute that should be ignored
                connection.Insert(New Car With {.Name = "Volvo", .Computed = "this property should be ignored"})
                Dim userName As String = "Adam"
                Dim id As Long = connection.Insert(New Users With {.Name = userName, .Age = 10})
                'get a user with "isdirty" tracking
                Dim user As IUsers = connection.Get(Of IUsers)(id)
                Assert.AreEqual(userName, user.Name)
                Dim previousUserName As String = user.Name
                Assert.IsFalse(connection.Update(Of IUsers)(user), $"Updated even though previous user name did not change {previousUserName}={user.Name}")

                previousUserName = user.Name
                userName = "Bob"
                user.Name = userName
                Assert.IsTrue(connection.Update(user), $"Did not update even though previous user name changed {previousUserName}<>{user.Name}")
                previousUserName = user.Name
                ' this test fails unless the user is fetched; change tracking works only once.
                user = connection.Get(Of IUsers)(id)
                Assert.IsFalse(connection.Update(user), $"Updated even though previous user name did not change {previousUserName}={user.Name}")

                previousUserName = user.Name
                userName = "James"
                user.Name = userName
                Assert.IsTrue(connection.Update(user), $"Did not update even though previous user name changed {previousUserName}<>{user.Name}")
                previousUserName = user.Name
                ' this test fails unless the user is fetched; change tracking works only once.
                user = connection.Get(Of IUsers)(id)
                Assert.IsFalse(connection.Update(user), $"Updated even though previous user name did not change {previousUserName}={user.Name}")
            End Using
        End Sub

        ''' <summary> (Unit Test Method) inserts a get update. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub InsertGetUpdate()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.DeleteAll(Of Users)()
                Assert.IsNull(connection.Get(Of Users)(3))
                'insert with computed attribute that should be ignored
                connection.Insert(New Car With {.Name = "Volvo", .Computed = "this property should be ignored"})
                Dim id As Long = connection.Insert(New Users With {.Name = "Adam", .Age = 10})
                'get a user with "isdirty" tracking
                Dim user As IUsers = connection.Get(Of IUsers)(id)
                Assert.AreEqual("Adam", user.Name)

                Dim IProxyUser As SqlMapperExtensions.IProxy = TryCast(user, Global.Dapper.Contrib.Extensions.SqlMapperExtensions.IProxy)
                Assert.IsNotNull(IProxyUser, "IUser cannot be cast as IProxy")
                Assert.IsFalse(IProxyUser.IsDirty, "IUser should not be dirty after first fetch from the database")

                Assert.IsFalse(connection.Update(user)) ' returns false if not updated, based on tracking
                user.Name = "Bob"

                Assert.IsTrue(IProxyUser.IsDirty, "IUser should be dirty after setting a new value for the user name")

                Assert.IsTrue(connection.Update(user))  ' returns true if updated, based on tracking
                user = connection.Get(Of IUsers)(id)

                IProxyUser = TryCast(user, Global.Dapper.Contrib.Extensions.SqlMapperExtensions.IProxy)
                Assert.IsFalse(IProxyUser.IsDirty, "IUser should not be dirty after second fetch from the database")

                Assert.AreEqual("Bob", user.Name)

                Assert.IsFalse(connection.Update(user), "IUser updated event through not dirty")

                'get a user with no tracking
                Dim notrackedUser As Users = connection.Get(Of Users)(id)

                IProxyUser = TryCast(notrackedUser, Global.Dapper.Contrib.Extensions.SqlMapperExtensions.IProxy)
                Assert.IsNull(IProxyUser, "User was cast as IProxy--only IUser should")

                Assert.AreEqual("Bob", notrackedUser.Name)
                Assert.IsTrue(connection.Update(notrackedUser)) 'returns true, even though user was not changed
                notrackedUser.Name = "Cecil"
                Assert.IsTrue(connection.Update(notrackedUser))
                Assert.AreEqual("Cecil", connection.Get(Of Users)(id).Name)

                Assert.AreEqual(1, connection.Query(Of Users)("select * from Users").Count)
                Assert.IsTrue(connection.Delete(user))
                Assert.IsFalse(connection.Query(Of Users)("select * from Users").Any)

                Assert.IsFalse(connection.Update(notrackedUser)) 'returns false, user not found
            End Using
        End Sub

        ''' <summary> (Unit Test Method) inserts a get update asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function InsertGetUpdateAsync() As Task
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Assert.IsNull(Await connection.GetAsync(Of Users)(30).ConfigureAwait(False))

                Dim originalCount As Integer = (Await connection.QueryAsync(Of Integer)("select Count(*) from Users").ConfigureAwait(False)).First()
                ' DH: consider this change
                originalCount = (Await connection.ExecuteScalarAsync(Of Integer)("select Count(*) from Users").ConfigureAwait(False))

                Dim id As Integer = Await connection.InsertAsync(New Users With {.Name = "Adam", .Age = 10}).ConfigureAwait(False)

                'get a user with "isdirty" tracking
                Dim user As IUsers = Await connection.GetAsync(Of IUsers)(id).ConfigureAwait(False)
                Assert.AreEqual("Adam", user.Name)
                Assert.IsFalse(Await connection.UpdateAsync(user).ConfigureAwait(False)) 'returns false if not updated, based on tracking
                user.Name = "Bob"
                Assert.IsTrue(Await connection.UpdateAsync(user).ConfigureAwait(False)) 'returns true if updated, based on tracking
                user = Await connection.GetAsync(Of IUsers)(id).ConfigureAwait(False)
                Assert.AreEqual("Bob", user.Name)

                'get a user with no tracking
                Dim notrackedUser As Users = Await connection.GetAsync(Of Users)(id).ConfigureAwait(False)
                Assert.AreEqual("Bob", notrackedUser.Name)
                Assert.IsTrue(Await connection.UpdateAsync(notrackedUser).ConfigureAwait(False))
                'returns true, even though user was not changed
                notrackedUser.Name = "Cecil"
                Assert.IsTrue(Await connection.UpdateAsync(notrackedUser).ConfigureAwait(False))
                Assert.AreEqual("Cecil", (Await connection.GetAsync(Of Users)(id).ConfigureAwait(False)).Name)

                Assert.AreEqual((Await connection.QueryAsync(Of Users)("select * from Users").ConfigureAwait(False)).Count(), originalCount + 1)
                Assert.IsTrue(Await connection.DeleteAsync(user).ConfigureAwait(False))
                Assert.AreEqual((Await connection.QueryAsync(Of Users)("select * from Users").ConfigureAwait(False)).Count(), originalCount)

                Assert.IsFalse(Await connection.UpdateAsync(notrackedUser).ConfigureAwait(False)) 'returns false, user not found

                Assert.IsTrue(Await connection.InsertAsync(New Users With {.Name = "Adam", .Age = 10}).ConfigureAwait(False) > originalCount + 1)
            End Using
        End Function

        ''' <summary> (Unit Test Method) issue 418. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub Issue418()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                'update first (will fail) then insert
                'added for bug #418
                Dim updateObject As ObjectX = New ObjectX With {.ObjectXId = Guid.NewGuid().ToString(), .Name = "Someone"}
                Dim updates As Boolean = connection.Update(updateObject)
                Assert.IsFalse(updates)
                connection.DeleteAll(Of ObjectX)()
                Dim objectXId As String = Guid.NewGuid().ToString()
                Dim insertObject As ObjectX = New ObjectX With {.ObjectXId = objectXId, .Name = "Someone else"}
                connection.Insert(insertObject)
                Dim list As IEnumerable(Of ObjectX) = connection.GetAll(Of ObjectX)()
                Assert.AreEqual(1, list.Count)
            End Using
        End Sub

        ''' <summary>
        ''' (Unit Test Method) inserts a get update delete with explicit key. issue #351.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        <CodeAnalysis.SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification:="<Pending>")>
        Public Sub InsertGetUpdateDeleteWithExplicitKey()
            ' Tests for issue #351
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Dim guid As String = System.Guid.NewGuid().ToString()
                Dim o1 As ObjectX = New ObjectX With {.ObjectXId = guid, .Name = "Foo"}
                Dim originalxCount As Integer = connection.Query(Of Integer)("Select Count(*) From ObjectX").First()

                ' DH: consider this change:
                originalxCount = connection.ExecuteScalar(Of Integer)("Select Count(*) From ObjectX")

                connection.Insert(o1)
                Dim list1 As IEnumerable(Of ObjectX) = connection.Query(Of ObjectX)("select * from ObjectX").ToList()
                Assert.AreEqual(list1.Count, originalxCount + 1)
                o1 = connection.Get(Of ObjectX)(guid)
                Assert.AreEqual(o1.ObjectXId, guid)
                o1.Name = "Bar"
                connection.Update(o1)
                o1 = connection.Get(Of ObjectX)(guid)
                Assert.AreEqual("Bar", o1.Name)
                connection.Delete(o1)
                o1 = connection.Get(Of ObjectX)(guid)
                Assert.IsNull(o1)

                Const id As Integer = 42
                Dim o2 As ObjectY = New ObjectY With {.ObjectYId = id, .Name = "Foo"}
                Dim originalyCount As Integer = connection.Query(Of Integer)("Select Count(*) From ObjectY").First()

                ' DH: consider this change:
                originalyCount = connection.ExecuteScalar(Of Integer)("Select Count(*) From ObjectY")

                connection.Insert(o2)
                Dim list2 As IEnumerable(Of ObjectY) = connection.Query(Of ObjectY)("select * from ObjectY").ToList()
                Assert.AreEqual(list2.Count, originalyCount + 1)
                o2 = connection.Get(Of ObjectY)(id)
                Assert.AreEqual(o2.ObjectYId, id)
                o2.Name = "Bar"
                connection.Update(o2)
                o2 = connection.Get(Of ObjectY)(id)
                Assert.AreEqual("Bar", o2.Name)
                connection.Delete(o2)
                o2 = connection.Get(Of ObjectY)(id)
                Assert.IsNull(o2)
            End Using
        End Sub

        ''' <summary>
        ''' (Unit Test Method) inserts a get update delete with explicit key asynchronous. issue #351.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function InsertGetUpdateDeleteWithExplicitKeyAsync() As Task
            ' Tests for issue #351
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Dim guid As String = System.Guid.NewGuid().ToString()
                Dim o1 As New ObjectX With {.ObjectXId = guid, .Name = "Foo"}
                Dim originalxCount As Integer = (Await connection.ExecuteScalarAsync(Of Integer)("Select Count(*) From ObjectX").ConfigureAwait(False))

                Await connection.InsertAsync(o1).ConfigureAwait(False)
                Dim list1 As List(Of ObjectX) = (Await connection.QueryAsync(Of ObjectX)("select * from ObjectX").ConfigureAwait(False)).ToList()
                Assert.AreEqual(list1.Count, originalxCount + 1)
                o1 = Await connection.GetAsync(Of ObjectX)(guid).ConfigureAwait(False)
                Assert.AreEqual(o1.ObjectXId, guid)
                o1.Name = "Bar"
                Await connection.UpdateAsync(o1).ConfigureAwait(False)
                o1 = Await connection.GetAsync(Of ObjectX)(guid).ConfigureAwait(False)
                Assert.AreEqual("Bar", o1.Name)
                Await connection.DeleteAsync(o1).ConfigureAwait(False)
                o1 = Await connection.GetAsync(Of ObjectX)(guid).ConfigureAwait(False)
                Assert.IsNull(o1)

                Const id As Integer = 42
                Dim o2 As New ObjectY With {.ObjectYId = id, .Name = "Foo"}
                Dim unused As Integer = connection.Query(Of Integer)("Select Count(*) From ObjectY").First()
                Dim originalyCount As Integer = connection.ExecuteScalar(Of Integer)("Select Count(*) From ObjectY")

                Await connection.InsertAsync(o2).ConfigureAwait(False)
                Dim list2 As List(Of ObjectY) = (Await connection.QueryAsync(Of ObjectY)("select * from ObjectY").ConfigureAwait(False)).ToList()
                Assert.AreEqual(list2.Count, originalyCount + 1)
                o2 = Await connection.GetAsync(Of ObjectY)(id).ConfigureAwait(False)
                Assert.AreEqual(o2.ObjectYId, id)
                o2.Name = "Bar"
                Await connection.UpdateAsync(o2).ConfigureAwait(False)
                o2 = Await connection.GetAsync(Of ObjectY)(id).ConfigureAwait(False)
                Assert.AreEqual("Bar", o2.Name)
                Await connection.DeleteAsync(o2).ConfigureAwait(False)
                o2 = Await connection.GetAsync(Of ObjectY)(id).ConfigureAwait(False)
                Assert.IsNull(o2)
            End Using
        End Function

        ''' <summary>
        ''' (Unit Test Method) inserts a get update delete with explicit key named identifier.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub InsertGetUpdateDeleteWithExplicitKeyNamedId()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Const id As Integer = 42
                Dim o2 As ObjectZ = New ObjectZ With {.Id = id, .Name = "Foo"}
                connection.Insert(o2)
                Dim list2 As IEnumerable(Of ObjectZ) = connection.Query(Of ObjectZ)("select * from ObjectZ").ToList()
                Assert.AreEqual(1, list2.Count)
                o2 = connection.Get(Of ObjectZ)(id)
                Assert.AreEqual(o2.Id, id)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) updates the enumerable. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub UpdateEnumerable()
            Me.UpdateHelper(Function(src) src.AsEnumerable())
        End Sub

        ''' <summary> (Unit Test Method) updates the array. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub UpdateArray()
            Me.UpdateHelper(Function(src) src.ToArray())
        End Sub

        ''' <summary> (Unit Test Method) updates the list. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub UpdateList()
            Me.UpdateHelper(Function(src) src.ToList())
        End Sub

        ''' <summary> Helper method that update. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="helper"> The helper. </param>
        Private Sub UpdateHelper(Of T As Class)(ByVal helper As Func(Of IEnumerable(Of Users), T))
            Const numberOfEntities As Integer = 10

            Dim users As List(Of Users) = New List(Of Users)()
            For i As Integer = 0 To numberOfEntities - 1
                users.Add(New Users With {.Name = "User " & i, .Age = i})
            Next i

            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.DeleteAll(Of Users)()
                Dim total As Long = connection.Insert(helper(users))
                Assert.AreEqual(CInt(total), numberOfEntities)
                users = connection.Query(Of Users)("select * from Users").ToList()
                Assert.AreEqual(users.Count, numberOfEntities)
                For Each user As Users In users
                    user.Name &= " updated"
                Next user
                connection.Update(helper(users))
                Dim name As String = connection.Query(Of Users)("select * from Users").First().Name
                Assert.IsTrue(name.Contains("updated"))
            End Using
        End Sub

        ''' <summary> (Unit Test Method) updates the enumerable asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function UpdateEnumerableAsync() As Task
            Await Me.UpdateHelperAsync(Function(src) src.AsEnumerable()).ConfigureAwait(False)
        End Function

        ''' <summary> (Unit Test Method) updates the array asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function UpdateArrayAsync() As Task
            Await Me.UpdateHelperAsync(Function(src) src.ToArray()).ConfigureAwait(False)
        End Function

        ''' <summary> (Unit Test Method) updates the list asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function UpdateListAsync() As Task
            Await Me.UpdateHelperAsync(Function(src) src.ToList()).ConfigureAwait(False)
        End Function

        ''' <summary> Updates the helper asynchronous described by helper. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="helper"> The helper. </param>
        ''' <returns> A Task. </returns>
        Private Async Function UpdateHelperAsync(Of T As Class)(ByVal helper As Func(Of IEnumerable(Of Users), T)) As Task
            Const numberOfEntities As Integer = 10

            Dim users As New List(Of Users)()
            For i As Integer = 0 To numberOfEntities - 1
                users.Add(New Users With {.Name = "User " & i, .Age = i})
            Next i

            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Await connection.DeleteAllAsync(Of Users)().ConfigureAwait(False)

                Dim total As Long = Await connection.InsertAsync(helper(users)).ConfigureAwait(False)
                Assert.AreEqual(CInt(total), numberOfEntities)
                users = connection.Query(Of Users)("select * from Users").ToList()
                Assert.AreEqual(users.Count, numberOfEntities)
                For Each User As Users In users
                    User.Name &= " updated"
                Next User
                Await connection.UpdateAsync(helper(users)).ConfigureAwait(False)
                Dim name As String = connection.Query(Of Users)("select * from Users").First().Name
                Assert.IsTrue(name.Contains("updated"))
            End Using
        End Function

        #End Region

        #Region " TRANSACTIONS "

        ''' <summary> (Unit Test Method) transactions this object. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub Transactions()
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Dim id As Long = connection.Insert(New Car With {.Name = "one car"}) 'insert outside transaction
                Dim tran As System.Data.IDbTransaction = connection.BeginTransaction()
                Dim car As Car = connection.Get(Of Car)(id, tran)
                Dim orgName As String = car.Name
                car.Name = "Another car"
                connection.Update(car, tran)
                tran.Rollback()
                car = connection.Get(Of Car)(id) 'updates should have been rolled back
                Assert.AreEqual(car.Name, orgName)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) transaction scope. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub TransactionScope()
            Using txscope As TransactionScope = New TransactionScope()
                Using connection As System.Data.IDbConnection = Me.GetConnection()
                    Dim id As Long = connection.Insert(New Car With {.Name = "one car"}) 'insert car within transaction
                    txscope.Dispose() 'rollback
                    Assert.IsNull(connection.Get(Of Car)(id)) 'returns null - car with that id should not exist
                End Using
            End Using
        End Sub

        #End Region

        #Region " TYPE WITH GENERIC PARAMETER "

        ''' <summary> (Unit Test Method) type with generic parameter can be inserted. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub TypeWithGenericParameterCanBeInserted()
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                connection.DeleteAll(Of GenericType(Of String))()
                Dim objectToInsert As GenericType(Of String) = New GenericType(Of String) With {.Id = Guid.NewGuid().ToString(), .Name = "something"}
                connection.Insert(objectToInsert)

                Assert.AreEqual(1, connection.GetAll(Of GenericType(Of String))().Count)

                Dim objectsToInsert As List(Of GenericType(Of String)) = New List(Of GenericType(Of String)) From {New GenericType(Of String) With {.Id = Guid.NewGuid().ToString(), .Name = "1"},
                                                                                                                   New GenericType(Of String) With {.Id = Guid.NewGuid().ToString(), .Name = "2"}}

                connection.Insert(objectsToInsert)
                Dim list As IEnumerable(Of GenericType(Of String)) = connection.GetAll(Of GenericType(Of String))()
                Assert.AreEqual(3, list.Count())
            End Using
        End Sub

        ''' <summary> (Unit Test Method) type with generic parameter can be updated. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub TypeWithGenericParameterCanBeUpdated()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Dim objectToInsert As GenericType(Of String) = New GenericType(Of String) With {.Id = Guid.NewGuid().ToString(), .Name = "something"}
                connection.Insert(objectToInsert)
                objectToInsert.Name = "somethingelse"
                connection.Update(objectToInsert)
                Dim updatedObject As GenericType(Of String) = connection.Get(Of GenericType(Of String))(objectToInsert.Id)
                Assert.AreEqual(objectToInsert.Name, updatedObject.Name)
            End Using
        End Sub

        ''' <summary> (Unit Test Method) type with generic parameter can be deleted. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub TypeWithGenericParameterCanBeDeleted()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Dim objectToInsert As GenericType(Of String) = New GenericType(Of String) With {.Id = Guid.NewGuid().ToString(), .Name = "something"}
                connection.Insert(objectToInsert)
                Dim deleted As Boolean = connection.Delete(objectToInsert)
                Assert.IsTrue(deleted)
            End Using
        End Sub

        ''' <summary>
        ''' (Unit Test Method) type with generic parameter can be inserted asynchronous.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function TypeWithGenericParameterCanBeInsertedAsync() As Task
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Await connection.DeleteAllAsync(Of GenericType(Of String))()
                Dim objectToInsert As New GenericType(Of String) With {.Id = Guid.NewGuid().ToString(), .Name = "something"}
                Await connection.InsertAsync(objectToInsert)
                Assert.AreEqual(1, connection.GetAll(Of GenericType(Of String)).Count)
                Dim objectsToInsert As New List(Of GenericType(Of String)) From {New GenericType(Of String) With {.Id = Guid.NewGuid().ToString(), .Name = "1"},
                                                                                 New GenericType(Of String) With {.Id = Guid.NewGuid().ToString(), .Name = "2"}}
                Await connection.InsertAsync(objectsToInsert)
                Dim list As IEnumerable(Of GenericType(Of String)) = connection.GetAll(Of GenericType(Of String))()
                Assert.AreEqual(3, list.Count())
            End Using
        End Function

        ''' <summary>
        ''' (Unit Test Method) type with generic parameter can be updated asynchronous.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function TypeWithGenericParameterCanBeUpdatedAsync() As Task
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Dim objectToInsert As New GenericType(Of String) With {.Id = Guid.NewGuid().ToString(), .Name = "something"}
                Await connection.InsertAsync(objectToInsert)
                objectToInsert.Name = "somethingelse"
                Await connection.UpdateAsync(objectToInsert)
                Dim updatedObject As GenericType(Of String) = connection.Get(Of GenericType(Of String))(objectToInsert.Id)
                Assert.AreEqual(objectToInsert.Name, updatedObject.Name)
            End Using
        End Function

        ''' <summary>
        ''' (Unit Test Method) type with generic parameter can be deleted asynchronous.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function TypeWithGenericParameterCanBeDeletedAsync() As Task
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Dim objectToInsert As New GenericType(Of String) With {.Id = Guid.NewGuid().ToString(), .Name = "something"}
                Await connection.InsertAsync(objectToInsert)
                Dim deleted As Boolean = Await connection.DeleteAsync(objectToInsert)
                Assert.IsTrue(deleted)
            End Using
        End Function

        #End Region

        #Region " BUILDERS "

        ''' <summary> (Unit Test Method) builder select clause. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        <TestMethod>
        Public Sub BuilderSelectClause()
            Using connection As System.Data.IDbConnection = Me.GetConnection()
                Dim rand As Random = New Random(8675309)
                Dim data As List(Of Users) = New List(Of Users)()
                For i As Integer = 0 To 99
                    Dim nU As Users = New Users With {.Age = rand.Next(70), .Id = i, .Name = Guid.NewGuid().ToString()}
                    data.Add(nU)
                    nU.Id = CInt(Fix(connection.Insert(nU)))
                Next i

                Dim builder As SqlBuilder = New SqlBuilder()
                Dim justId As SqlBuilder.Template = builder.AddTemplate("SELECT /**select**/ from Users")
                Dim all As SqlBuilder.Template = builder.AddTemplate("SELECT Name, /**select**/, Age from Users")

                builder.Select("Id")

                Dim ids As IEnumerable(Of Integer) = connection.Query(Of Integer)(justId.RawSql, justId.Parameters)
                Dim users As IEnumerable(Of Users) = connection.Query(Of Users)(all.RawSql, all.Parameters)

                For Each u As Users In data
                    If Not ids.Any(Function(i) u.Id = i) Then
                        Throw New InvalidOperationException("Missing ids in select")
                    End If
                    If Not users.Any(Function(a) a.Id = u.Id AndAlso a.Name = u.Name AndAlso a.Age = u.Age) Then
                        Throw New InvalidOperationException("Missing users in select")
                    End If
                Next u
            End Using
        End Sub

        ''' <summary> (Unit Test Method) builder template without composition. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        <TestMethod>
        Public Sub BuilderTemplateWithoutComposition()
            Dim builder As SqlBuilder = New SqlBuilder()
            Dim template As SqlBuilder.Template = builder.AddTemplate("SELECT COUNT(*) from Users WHERE Age = @age", New With {Key .age = 5})
            ' Dim template As SqlBuilder.Template = builder.AddTemplate("SELECT COUNT(*) from Users WHERE (Age = @age and Name = @name)", New With {Key .age = 5, .name = "a"})

            If template.RawSql Is Nothing Then
                Throw New InvalidOperationException("RawSql null")
            End If
            If template.Parameters Is Nothing Then
                Throw New InvalidOperationException("Parameters null")
            End If

            Using connection As System.Data.IDbConnection = Me.GetConnection()
                connection.DeleteAll(Of Users)()
                connection.Insert(New Users With {.Age = 5, .Name = "Testy McTestington"})

                If connection.Query(Of Integer)(template.RawSql, template.Parameters).Single() <> 1 Then
                    Throw New InvalidOperationException("Query failed")
                End If
                If connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters) <> 1 Then
                    Throw New InvalidOperationException("Query failed")
                End If
            End Using
        End Sub

        ''' <summary> (Unit Test Method) builder select clause asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function BuilderSelectClauseAsync() As Task
            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Await connection.DeleteAllAsync(Of Users)().ConfigureAwait(False)

                Dim rand As New Random(8675309)
                Dim data As New List(Of Users)()
                For i As Integer = 0 To 99
                    Dim nU As New Users With {.Age = rand.Next(70), .Id = i, .Name = Guid.NewGuid().ToString()}
                    data.Add(nU)
                    nU.Id = Await connection.InsertAsync(nU).ConfigureAwait(False)
                Next i

                Dim builder As New SqlBuilder()
                Dim justId As SqlBuilder.Template = builder.AddTemplate("SELECT /**select**/ from Users")
                Dim all As SqlBuilder.Template = builder.AddTemplate("SELECT Name, /**select**/, Age from Users")

                builder.Select("Id")

                Dim ids As IEnumerable(Of Integer) = Await connection.QueryAsync(Of Integer)(justId.RawSql, justId.Parameters).ConfigureAwait(False)
                Dim users As IEnumerable(Of Users) = Await connection.QueryAsync(Of Users)(all.RawSql, all.Parameters).ConfigureAwait(False)

                For Each u As Users In data
                    If Not ids.Any(Function(i) u.Id = i) Then
                        Throw New Exception("Missing ids in select")
                    End If
                    If Not users.Any(Function(a) a.Id = u.Id AndAlso a.Name = u.Name AndAlso a.Age = u.Age) Then
                        Throw New Exception("Missing users in select")
                    End If
                Next u
            End Using
        End Function

        ''' <summary> (Unit Test Method) builder template without composition asynchronous. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        ''' <returns> A Task. </returns>
        <TestMethod>
        Public Async Function BuilderTemplateWithoutCompositionAsync() As Task
            Dim builder As New SqlBuilder()
            Dim template As SqlBuilder.Template = builder.AddTemplate("SELECT COUNT(*) from Users WHERE Age = @age", New With {Key .age = 5})

            If template.RawSql Is Nothing Then
                Throw New Exception("RawSql null")
            End If
            If template.Parameters Is Nothing Then
                Throw New Exception("Parameters null")
            End If

            Using connection As System.Data.IDbConnection = Me.GetOpenConnection()
                Await connection.DeleteAllAsync(Of Users)().ConfigureAwait(False)

                Await connection.InsertAsync(New Users With {.Age = 5, .Name = "Testy McTestington"}).ConfigureAwait(False)

                If (Await connection.QueryAsync(Of Integer)(template.RawSql, template.Parameters).ConfigureAwait(False)).Single() <> 1 Then
                    Throw New Exception("Query failed")
                End If
                If (Await connection.ExecuteScalarAsync(Of Integer)(template.RawSql, template.Parameters).ConfigureAwait(False)) <> 1 Then
                    Throw New Exception("Query failed")
                End If
            End Using
        End Function

        #End Region

    End Class

End Namespace
