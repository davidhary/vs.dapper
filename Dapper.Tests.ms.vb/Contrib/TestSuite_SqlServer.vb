Imports Dapper
Namespace Global.isr.Dapper.Tests.ContribTests

    ''' <summary> An SQL server test suite. </summary>
    ''' <remarks>
    ''' The test suites here implement TestSuiteBase so that each provider runs the entire set of
    ''' tests without declarations per method If we want to support a new provider, they need only be
    ''' added here - not in multiple places. (c) 2018 Integrated Scientific Resources, Inc. All
    ''' rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 8/16/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class SqlServerTestSuite
        Inherits TestSuite

        #Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                Console.WriteLine($".NET {Environment.Version}")
                Console.WriteLine($"Dapper {GetType(SqlMapper).AssemblyQualifiedName}")
                Console.WriteLine($"Connection string: {SqlServerTestSuite.ConnectionString}")
                SqlServerTestSuite.CreateDataTables()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        #End Region

        #Region " IMPLEMENTATION "

        ''' <summary> The connection string. </summary>
        Public Shared ReadOnly ConnectionString As String = Global.isr.Dapper.Tests.ContribTests.ConfigReader.SqlServerConnectionString

        ''' <summary> Gets the connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The connection. </returns>
        Public Overrides Function GetConnection() As System.Data.IDbConnection
            Return Global.isr.Dapper.Tests.ContribTests.SqlServerTestSuite.GetNewConnection()
        End Function

        ''' <summary> Gets new connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The new connection. </returns>
        Friend Shared Function GetNewConnection() As System.Data.IDbConnection
            Return New System.Data.SqlClient.SqlConnection(SqlServerTestSuite.ConnectionString)
        End Function

        ''' <summary> Creates data tables. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Shared Sub CreateDataTables()
            Using connection As System.Data.IDbConnection = SqlServerTestSuite.GetNewConnection()
                ' ReSharper disable once AccessToDisposedClosure
                Dim dropTable As Action(Of String) = Sub(name) connection.Execute($"IF OBJECT_ID('{name}', 'U') IS NOT NULL DROP TABLE [{name}]; ")
                connection.Open()
                dropTable("Stuff")
                connection.Execute("CREATE TABLE Stuff (TheId int IDENTITY(1,1) not null, Name nvarchar(100) not null, Created DateTime null);")
                dropTable("People")
                connection.Execute("CREATE TABLE People (Id int IDENTITY(1,1) not null, Name nvarchar(100) not null);")
                dropTable("Users")
                connection.Execute("CREATE TABLE Users (Id int IDENTITY(1,1) not null, Name nvarchar(100) not null, Age int not null);")
                dropTable("Automobile")
                connection.Execute("CREATE TABLE Automobile (Id int IDENTITY(1,1) not null, Name nvarchar(100) not null);")
                dropTable("Result")
                connection.Execute("CREATE TABLE Result (Id int IDENTITY(1,1) not null, Name nvarchar(100) not null, [Order] int not null);")
                dropTable("ObjectX")
                connection.Execute("CREATE TABLE ObjectX (ObjectXId nvarchar(100) not null, Name nvarchar(100) not null);")
                dropTable("ObjectY")
                connection.Execute("CREATE TABLE ObjectY (ObjectYId int not null, Name nvarchar(100) not null);")
                dropTable("ObjectZ")
                connection.Execute("CREATE TABLE ObjectZ (Id int not null, Name nvarchar(100) not null);")
                dropTable("GenericType")
                connection.Execute("CREATE TABLE GenericType (Id nvarchar(100) not null, Name nvarchar(100) not null);")
                dropTable("NullableDate")
                connection.Execute("CREATE TABLE NullableDate (Id int IDENTITY(1,1) not null, DateValue DateTime null);")
                dropTable("ObjectZZ")
                connection.Execute("CREATE TABLE ObjectZZ (Id nvarchar(100) not null primary key, Name nvarchar(100) not null, IsActive bit not null DEFAULT 1, Timestamp DateTime NOT NULL DEFAULT (GETUTCDATE())) ")
            End Using
        End Sub

        #End Region

    End Class


End Namespace
