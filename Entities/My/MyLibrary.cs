
namespace isr.Dapper.Entities.My
{

    /// <summary>   Provides assembly information for the class library. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        private MyLibrary() : base()
        {
        }

        // ''' <summary> Gets Identifies the trace source. </summary>
        /// <summary>   Identifies the trace event. </summary>
        public const int TraceEventId = ( int ) Entity.My.ProjectTraceEventId.DapperEntities;
        /// <summary>   The assembly title. </summary>
        public const string AssemblyTitle = "Dapper Entities Library";
        /// <summary>   Information describing the assembly. </summary>
        public const string AssemblyDescription = "Dapper Entities Library";
        /// <summary>   The assembly product. </summary>
        public const string AssemblyProduct = "Dapper.Entities";

        /// <summary>   The test assembly strong name. </summary>
        public const string TestAssemblyStrongName = "isr.Dapper.Entities.Tests,PublicKey=" + global::Dapper.My.SolutionInfo.PublicKey;
    }
}
