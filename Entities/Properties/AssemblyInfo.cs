﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Dapper.Entities.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Dapper.Entities.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Dapper.Entities.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
