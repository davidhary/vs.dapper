using System;

namespace isr.Dapper.Entities.RandomExtensions
{
    /// <summary> Includes extensions for <see cref="Random">random number generation</see>. </summary>
    /// <remarks> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-11-14, 2.0.5066. </para></remarks>
    public static class RandomExtensionMethods
    {

        #region " RANDOM UNIFORM "

        /// <summary> Generates the next random uniformly-distributed number. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="random"> The pseudo random number generator. </param>
        /// <param name="min">    The minimum of the solution value. </param>
        /// <param name="max">    The maximum of the solution value. </param>
        /// <returns> A random value between the minimum and maximum. </returns>
        public static double NextUniform( this Random random, double min, double max )
        {
            return random is null ? throw new ArgumentNullException( nameof( random ) ) : (max - min) * random.NextDouble() + min;
        }

        /// <summary> Generates an array of random numbers-uniformly distributed. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="random"> The pseudo random number generator. </param>
        /// <param name="length"> The length. </param>
        /// <param name="min">    The minimum of the solution value. </param>
        /// <param name="max">    The maximum of the solution value. </param>
        /// <returns> An array of random numbers. </returns>
        public static double[] Uniform( this Random random, int length, double min, double max )
        {
            if ( random is null )
            {
                throw new ArgumentNullException( nameof( random ) );
            }

            if ( length <= 0 )
            {
                throw new ArgumentException( "Length must be positive", nameof( length ) );
            }

            var values = new double[length];
            for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
            {
                values[i] = random.NextUniform( min, max );
            }

            return values;
        }

        /// <summary> Generates an array of random uniformly-distributed number. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="random"> The pseudo random number generator. </param>
        /// <param name="min">    The minimum. </param>
        /// <param name="max">    The maximum. </param>
        /// <returns> An array of random numbers. </returns>
        public static double[] Uniform( this Random random, double[] min, double[] max )
        {
            if ( random is null )
            {
                throw new ArgumentNullException( nameof( random ) );
            }

            if ( min is null )
            {
                throw new ArgumentNullException( nameof( min ) );
            }
            else if ( max is null )
            {
                throw new ArgumentNullException( nameof( max ) );
            }
            else if ( min.Length == 0 )
            {
                throw new ArgumentException( "Length must be positive", nameof( min ) );
            }
            else if ( max.Length == 0 )
            {
                throw new ArgumentException( "Length must be positive", nameof( max ) );
            }
            else if ( min.Length != max.Length )
            {
                throw new ArgumentException( "Length of inputs must be the same", nameof( min ) );
            }

            var values = new double[min.Length];
            for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
            {
                values[i] = random.NextUniform( min[i], max[i] );
            }

            return values;
        }

        #endregion

        #region " RANDOM NORMAL "

        /// <summary>
        /// Generates the next random normally-distributed mean zero unity standard deviation number.
        /// </summary>
        /// <remarks>   Uses the Box-Mueller method. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="random">   The pseudo random number generator. </param>
        /// <returns>   A Double. </returns>
        public static double NextNormal( this Random random )
        {
            if ( random is null )
            {
                throw new ArgumentNullException( nameof( random ) );
            }

            double x, y, r2;
            do
            {
                x = 2.0d * random.NextDouble() - 1.0d;
                y = 2.0d * random.NextDouble() - 1.0d;
                r2 = x * x + y * y;
            }
            while ( r2 >= 1.0d || r2 == 0.0d );
            return x * Math.Sqrt( -2.0d * Math.Log( r2 ) / r2 );
        }

        /// <summary>
        /// Generates an array of random normally-distributed mean zero unity standard deviation numbers.
        /// </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <exception cref="ArgumentException">        Thrown when one or more arguments have
        ///                                             unsupported or illegal values. </exception>
        /// <param name="random">   The pseudo random number generator. </param>
        /// <param name="length">   The length. </param>
        /// <returns>   A Double() </returns>
        public static double[] Normal( this Random random, int length )
        {
            if ( random is null )
            {
                throw new ArgumentNullException( nameof( random ) );
            }

            if ( length <= 0 )
            {
                throw new ArgumentException( "Length must be positive", nameof( length ) );
            }

            var values = new double[length];
            for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
            {
                values[i] = random.NextNormal();
            }

            return values;
        }


        /// <summary> Generates the next random normally-distributed number. </summary>
        /// <remarks> Uses the Box-Mueller method. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="random"> The pseudo random number generator. </param>
        /// <param name="mean">   The mean. </param>
        /// <param name="sigma">  The sigma. </param>
        /// <returns> A Double. </returns>
        public static double NextNormal( this Random random, double mean, double sigma )
        {
            return random is null ? throw new ArgumentNullException( nameof( random ) ) : sigma * random.NextNormal() + mean;
        }

        /// <summary> Generates an array of random normally-distributed numbers. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="random"> The pseudo random number generator. </param>
        /// <param name="length"> The length. </param>
        /// <param name="mean">   The mean. </param>
        /// <param name="sigma">  The sigma. </param>
        /// <returns> A Double() </returns>
        public static double[] Normal( this Random random, int length, double mean, double sigma )
        {
            if ( random is null )
            {
                throw new ArgumentNullException( nameof( random ) );
            }

            if ( length <= 0 )
            {
                throw new ArgumentException( "Length must be positive", nameof( length ) );
            }

            var values = new double[length];
            for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
            {
                values[i] = random.NextNormal( mean, sigma );
            }

            return values;
        }

        #endregion

    }
}
