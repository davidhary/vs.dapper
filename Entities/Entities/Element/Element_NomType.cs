﻿

namespace isr.Dapper.Entities
{
    /// <summary>   An element entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class ElementEntity
    {

        /// <summary>   Gets or sets the <see cref="ElementNomTypeEntity"/>'s. </summary>
        /// <value> The <see cref="ElementNomTypeEntity"/>'s. </value>
        public ElementUniqueNomTypeEntityCollection NomTypeEntities { get; private set; }

        /// <summary>   Fetches <see cref="ElementNomTypeEntity"/>'s. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The <see cref="ElementNomTypeEntity"/>'s. </returns>
        public ElementUniqueNomTypeEntityCollection FetchElementNomTypeEntities( System.Data.IDbConnection connection )
        {
            this.NomTypeEntities = new ElementUniqueNomTypeEntityCollection( this.AutoId );
            this.NomTypeEntities.Populate( ElementNomTypeEntity.FetchEntities( connection, this.AutoId ) );
            return this.NomTypeEntities;
        }

        /// <summary>   Gets or sets <see cref="NomTypeEntityCollection"/>. </summary>
        /// <value> <see cref="NomTypeEntityCollection"/>. </value>
        public NomTypeEntityCollection NomTypes { get; private set; }

        /// <summary>   Fetches <see cref="NomTypeEntityCollection"/> </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The <see cref="NomTypeEntityCollection"/>. </returns>
        public NomTypeEntityCollection FetchNomTypes( System.Data.IDbConnection connection )
        {
            var elementNomType = new ElementNomTypeEntity() { ElementAutoId = AutoId };
            this.NomTypes = elementNomType.FetchNomTypes( connection );
            return this.NomTypes;
        }
    }
}