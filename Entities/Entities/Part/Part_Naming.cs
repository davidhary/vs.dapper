﻿
using System.Linq;

namespace isr.Dapper.Entities
{
    /// <summary>   A part entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class PartEntity
    {

        /// <summary>
        /// Gets or sets the <see cref=" PartUniqueNamingEntityCollection">Part Naming values</see>.
        /// </summary>
        /// <value> The Part Naming trait values. </value>
        public PartUniqueNamingEntityCollection Namings { get; private set; }

        /// <summary>
        /// Fetches <see cref=" PartUniqueNamingEntityCollection">Part Naming values</see>.
        /// </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of specifications. </returns>
        public int FetchNamings( System.Data.IDbConnection connection )
        {
            this.Namings = FetchNamings( connection, this.AutoId );
            return this.Namings?.Any() == true ? this.Namings.Count : 0;
        }

        /// <summary>
        /// Fetches <see cref="PartUniqueNamingEntityCollection">Part Naming values</see>.
        /// </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The specifications. </returns>
        public static PartUniqueNamingEntityCollection FetchNamings( System.Data.IDbConnection connection, int partAutoId )
        {
            if ( !PartNamingTypeEntity.IsEnumerated() )
                _ = PartNamingTypeEntity.TryFetchAll( connection );
            var namings = new PartUniqueNamingEntityCollection( partAutoId );
            namings.Populate( PartNamingEntity.FetchEntities( connection, partAutoId ) );
            return namings;
        }
    }
}