
using System.Collections.Generic;
using System.Linq;

namespace isr.Dapper.Entities
{
    /// <summary>   A part entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class PartEntity
    {

        /// <summary>   Gets or sets the <see cref="ProductPartEntity"/>. </summary>
        /// <value> The <see cref="ProductPartEntity"/> </value>
        public ProductPartEntity ProductPart { get; private set; }

        /// <summary>
        /// Gets or sets the <see cref="ProductUniqueElementEntityCollection">part elements</see>.
        /// </summary>
        /// <value> The Part elements entities. </value>
        public ProductUniqueElementEntityCollection Elements { get; private set; }

        /// <summary>
        /// Fetches the <see cref="ProductUniqueElementEntityCollection">part elements</see>.
        /// </summary>
        /// <remarks>   David, 2021-05-20. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeters">   The platform in meters. </param>
        /// <returns>   The number of Elements. </returns>
        public int FetchElements( System.Data.IDbConnection connection, IEnumerable<PlatformMeter> platformMeters )
        {
            int count = this.FetchElementsOnly( connection );
            if ( count > 0 )
                _ = this.FetchNomTraits( connection, platformMeters );
            return count;
        }

        /// <summary>   Fetches elements only. </summary>
        /// <remarks>   David, 2020-06-17. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The elements only. </returns>
        public int FetchElementsOnly( System.Data.IDbConnection connection )
        {
            if ( !(this.ProductPart?.IsClean()).GetValueOrDefault( false ) )
            {
                this.ProductPart = ProductPartEntity.FetchEntitiesByPart( connection, this.AutoId ).FirstOrDefault();
            }

            if ( (this.ProductPart?.IsClean()).GetValueOrDefault( false ) )
            {
                // a part has elements only if a product is associated with the part.
                this.Elements = FetchElements( connection, this.ProductPart.ProductAutoId );
            }

            _ = this.FetchElementEquations( connection );
            return this.Elements?.Any() == true ? this.Elements.Count : 0;
        }

        /// <summary>
        /// Fetches the <see cref="ProductUniqueElementEntityCollection">part elements</see>.
        /// </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The Elements. </returns>
        public static ProductUniqueElementEntityCollection FetchElements( System.Data.IDbConnection connection, int productAutoId )
        {
            if ( !NomTraitTypeEntity.IsEnumerated() )
                _ = NomTraitTypeEntity.TryFetchAll( connection );
            if ( !ElementTypeEntity.IsEnumerated() )
                _ = ElementTypeEntity.TryFetchAll( connection );
            var elements = new ProductUniqueElementEntityCollection( productAutoId );
            elements.Populate( ProductElementEntity.FetchOrderedElements( connection, productAutoId ) );
            _ = elements.FetchNomTypes( connection );
            return elements;
        }

        /// <summary>   Gets or sets the element equations. </summary>
        /// <value> The element equations. </value>
        public PartUniqueElementEquationEntityCollection ElementEquations { get; private set; }

        /// <summary>   Fetches element equations. </summary>
        /// <remarks>   David, 2020-07-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The element equations. </returns>
        public int FetchElementEquations( System.Data.IDbConnection connection )
        {
            this.ElementEquations = FetchElementEquations( connection, this );
            return this.ElementEquations?.Any() == true ? this.ElementEquations.Count : 0;
        }

        /// <summary>   Fetches element equations. </summary>
        /// <remarks>   David, 2020-07-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="part">         The <see cref="PartEntity"/>. </param>
        /// <returns>   The element equations. </returns>
        public static PartUniqueElementEquationEntityCollection FetchElementEquations( System.Data.IDbConnection connection, PartEntity part )
        {
            var elementEquations = new PartUniqueElementEquationEntityCollection( part );
            elementEquations.Populate( PartElementEquationEntity.FetchEntities( connection, part.AutoId ) );
            return elementEquations;
        }
    }
}
