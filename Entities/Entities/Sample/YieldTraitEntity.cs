using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Yield Trait builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class YieldTraitBuilder : KeyForeignRealBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( YieldTraitNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the first foreign reference table. </summary>
        /// <value> The name of the first foreign reference table. </value>
        public override string ForeignTableName { get; set; } = YieldTraitTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the first foreign reference table key. </summary>
        /// <value> The name of the first foreign reference table key. </value>
        public override string ForeignTableKeyName { get; set; } = nameof( YieldTraitTypeNub.Id );

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-06-13. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The new table. </returns>
        public string CreateTable( System.Data.IDbConnection connection )
        {
            // the same Element ID and Yield Type would repeat for the NUT associated with the next UUT.
            return this.CreateTable( connection, UniqueIndexOptions.None );
        }

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public override string AutoIdFieldName { get; set; } = nameof( YieldTraitEntity.AutoId );

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public override string ForeignIdFieldName { get; set; } = nameof( YieldTraitEntity.YieldTraitTypeId );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( YieldTraitEntity.Amount );

        #region " SINGLETON "

        private static readonly Lazy<YieldTraitBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static YieldTraitBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="YieldTraitEntity"/> <see cref="IKeyForeignReal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "YieldTrait" )]
    public class YieldTraitNub : KeyForeignRealNub, IKeyForeignReal
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public YieldTraitNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignReal CreateNew()
        {
            return new YieldTraitNub();
        }
    }

    /// <summary>
    /// The <see cref="YieldTraitEntity"/> storing Yield Traits.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-22 </para>
    /// </remarks>
    public class YieldTraitEntity : EntityBase<IKeyForeignReal, YieldTraitNub>, IKeyForeignReal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public YieldTraitEntity() : this( new YieldTraitNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public YieldTraitEntity( IKeyForeignReal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public YieldTraitEntity( IKeyForeignReal cache, IKeyForeignReal store ) : base( new YieldTraitNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public YieldTraitEntity( YieldTraitEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.YieldTraitBuilder.TableName, nameof( IKeyForeignReal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignReal CreateNew()
        {
            return new YieldTraitNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyForeignReal CreateCopy()
        {
            var destination = this.CreateNew();
            KeyForeignRealNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="YieldTraitEntity"/> interface value. </param>
        public override void CopyFrom( IKeyForeignReal value )
        {
            KeyForeignRealNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.ElementEntity"/>Value interface. </param>
        public override void UpdateCache( IKeyForeignReal value )
        {
            // first make the copy to notify of any property change.
            KeyForeignRealNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The <see cref="YieldTraitEntity"/> primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IKeyForeignReal>( key ) : connection.Get<YieldTraitNub>( key ) );
        }

        /// <summary>   Refetch; Fetches using the primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-15. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="YieldTraitEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int autoId )
        {
            return this.FetchUsingKey( connection, autoId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IKeyForeignReal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.AutoId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="YieldTraitEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int autoId )
        {
            return connection.Delete( new YieldTraitNub() { AutoId = autoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets or <see cref="YieldTraitEntity"/>'s. </summary>
        /// <value> The <see cref="YieldTraitEntity"/>'s. </value>
        public IEnumerable<YieldTraitEntity> YieldTraits { get; private set; }

        /// <summary>   Fetches all records into <see cref="YieldTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<YieldTraitEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IKeyForeignReal>() ) : Populate( connection.GetAll<YieldTraitNub>() );
        }

        /// <summary>   Fetches all records into <see cref="YieldTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.YieldTraits = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( YieldTraitEntity.YieldTraits ) );
            return this.YieldTraits?.Any() == true ? this.YieldTraits.Count() : 0;
        }

        /// <summary>   Populates a list of <see cref="YieldTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.ElementEntity"/>Value nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<YieldTraitEntity> Populate( IEnumerable<YieldTraitNub> nubs )
        {
            var l = new List<YieldTraitEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( YieldTraitNub nub in nubs )
                    l.Add( new YieldTraitEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of <see cref="YieldTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.ElementEntity"/>Value interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<YieldTraitEntity> Populate( IEnumerable<IKeyForeignReal> interfaces )
        {
            var l = new List<YieldTraitEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new YieldTraitNub();
                foreach ( IKeyForeignReal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new YieldTraitEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count element Yield Traits; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="YieldTraitEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int autoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{YieldTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( YieldTraitNub.AutoId )} = @AutoId", new { AutoId = autoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches element Yield Traits by Element number and element Yield Trait element Yield Trait
        /// Real Value type;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="YieldTraitEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<YieldTraitNub> FetchEntities( System.Data.IDbConnection connection, int autoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{YieldTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( YieldTraitNub.AutoId )} = @AutoId", new { AutoId = autoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<YieldTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches element Yield Traits by <see cref="YieldTraitEntity"/> id; expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="YieldTraitEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<YieldTraitNub> FetchNubs( System.Data.IDbConnection connection, int autoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{YieldTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( YieldTraitEntity.AutoId )} = @AutoId", new { AutoId = autoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<YieldTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.ElementEntity"/> Value exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="YieldTraitEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int autoId )
        {
            return 1 == CountEntities( connection, autoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>
        /// Fetches yield traits for the primary referenced entity identified by the <see cref="AutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<YieldTraitNub> FetchYieldTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{YieldTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( YieldTraitNub.AutoId )} = @AutoId", new { this.AutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<YieldTraitNub>( selector.RawSql, selector.Parameters );
        }

        #endregion

        #region " RELATIONS: YIELD TRAIT TYPE "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.YieldTraitTypeEntity"/> . </summary>
        /// <value> the <see cref="Dapper.Entities.YieldTraitTypeEntity"/> . </value>
        public YieldTraitTypeEntity YieldTraitTypeEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.YieldTraitTypeEntity"/>. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchYieldTraitTypeEntity( System.Data.IDbConnection connection )
        {
            this.YieldTraitTypeEntity = new YieldTraitTypeEntity();
            return this.YieldTraitTypeEntity.FetchUsingKey( connection, this.YieldTraitTypeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the first foreign key. </summary>
        /// <value> Identifies the first foreign key. </value>
        public int ForeignId
        {
            get => this.ICache.ForeignId;

            set {
                if ( !object.Equals( ( object ) this.ForeignId, ( object ) value ) )
                {
                    this.ICache.ForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( YieldTraitEntity.YieldTraitTypeId ) );
                }
            }
        }

        /// <summary>   Gets or sets the <see cref="YieldTraitEntity"/> Id. </summary>
        /// <value> The identifier of the yield trait type. </value>
        public int YieldTraitTypeId
        {
            get => this.ForeignId;

            set => this.ForeignId = value;
        }

        /// <summary>
        /// Gets or sets the yield value assigned to the <see cref="YieldTraitEntity"/> for the
        /// specific <see cref="Dapper.Entities.YieldTraitTypeEntity"/>.
        /// </summary>
        /// <value>
        /// The yield value assigned to the <see cref="YieldTraitEntity"/> for the specific
        /// <see cref="Dapper.Entities.YieldTraitTypeEntity"/>.
        /// </value>
        public double Amount
        {
            get => this.ICache.Amount;

            set {
                if ( !object.Equals( this.Amount, value ) )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The entity selector. </value>
        public DualKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of <see cref="YieldTraitEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class YieldTraitEntityCollection : EntityKeyedCollection<DualKeySelector, IKeyForeignReal, YieldTraitNub, YieldTraitEntity>
    {
        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override DualKeySelector GetKeyForItem( YieldTraitEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        public YieldTraitEntityCollection() : base()
        {
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( YieldTraitEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<YieldTraitEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( YieldTraitEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return YieldTraitBuilder.Instance.Upsert( connection, this );
        }
    }
}
