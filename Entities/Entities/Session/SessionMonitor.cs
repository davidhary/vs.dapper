using System.ComponentModel;
using System.Threading;
using System;

namespace isr.Dapper.Entities
{

    /// <summary>   A product test monitor. </summary>
    /// <remarks>
    /// David, 2020-05-29. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class SessionMonitor : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        public SessionMonitor() : base()
        {
            this.Context = SynchronizationContext.Current;
            this.UsingAsyncNotification = false;
        }

        #endregion

        #region " SINGLETON "

        private static readonly Lazy<SessionMonitor> LazySessionMonitor = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static SessionMonitor Instance => LazySessionMonitor.Value;

        #endregion

        #region " SYNCHRONIZATION CONTEXT "

        /// <summary> Gets or sets the synchronization context. </summary>
        /// <value> The context. </value>
        private SynchronizationContext Context { get; set; }


        /// <summary> Returns the current synchronization context. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
        /// null. </exception>
        /// <returns> A Threading.SynchronizationContext. </returns>
        private static SynchronizationContext CurrentSyncContext()
        {
            if ( SynchronizationContext.Current is null )
            {
                SynchronizationContext.SetSynchronizationContext( new SynchronizationContext() );
            }

            return SynchronizationContext.Current is null
                ? throw new InvalidOperationException( "Current Synchronization Context not set;. Must be set before starting the thread." )
                : SynchronizationContext.Current;
        }

        private bool _UsingAsyncNotification;
        /// <summary>   Gets or sets the notification level. </summary>
        /// <value> The notification level. </value>
        public bool UsingAsyncNotification
        {
            get => this._UsingAsyncNotification;
            set {
                if ( !bool.Equals( value, this.UsingAsyncNotification ) )
                {
                    this._UsingAsyncNotification = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a context for the active. </summary>
        /// <value> The active context. </value>
        protected SynchronizationContext ActiveContext => this.Context ?? CurrentSyncContext();

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            if ( this.UsingAsyncNotification )
                this.AsyncNotifyPropertyChanged( propertyName );
            else
                this.SyncNotifyPropertyChanged( propertyName );
        }

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void SyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            var handler = PropertyChanged;
            if ( handler != null )
                handler( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Asynchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void AsyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            var evt = this.PropertyChanged;
            if ( evt is object )
                this.ActiveContext.Post( ( object state ) => evt( this, new PropertyChangedEventArgs( propertyName ) ), null );
        }

        #endregion

        /// <summary>   Gets or sets the part. </summary>
        /// <value> The part. </value>
        public PartEntity Part { get; set; }

        /// <summary>   Gets or sets the lot. </summary>
        /// <value> The lot. </value>
        public LotEntity Lot { get; set; }

        /// <summary>   Gets or sets the session. </summary>
        /// <value> The session. </value>
        public SessionEntity Session { get; set; }

        /// <summary>   Gets or sets the product. </summary>
        /// <value> The product. </value>
        public ProductEntity Product { get; set; }

        /// <summary>   Gets or sets the Uut monitor. </summary>
        /// <value> The Uut monitor. </value>
        public UutMonitor UutMonitor { get; set; }

        /// <summary>   Gets or sets the elements. </summary>
        /// <value> The elements. </value>
        public ProductUniqueElementEntityCollection Elements { get; set; }

        /// <summary>   Gets or sets the uuts. </summary>
        /// <value> The uuts. </value>
        public SessionUniqueUutEntityCollection Uuts { get; set; }

        /// <summary>   Gets or sets the sorts. </summary>
        /// <value> The sorts. </value>
        public ProductUniqueProductSortEntityCollection Sorts { get; set; }

        /// <summary>   Gets or sets the Binning Entities. </summary>
        /// <value> The Binning Entities. </value>
        public ProductUniqueBinningEntityCollection Binnings { get; set; }
    }
}
