using System;
using System.ComponentModel;
using System.Threading;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Dapper.Entities
{

    /// <summary>   The Session Attribute class holing the Session trait values. </summary>
    /// <remarks>   David, 2020-05-29. </remarks>
    public class SessionTrait : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSestter">    The getter setter. </param>
        public SessionTrait( Core.Primitives.IGetterSetter<int> getterSestter ) : base()
        {
            this.GetterSetter = getterSestter;
            this.Context = SynchronizationContext.Current;
            this.UsingAsyncNotification = false;
        }

        #endregion

        #region " SYNCHRONIZATION CONTEXT "

        /// <summary> Gets or sets the synchronization context. </summary>
        /// <value> The context. </value>
        private SynchronizationContext Context { get; set; }


        /// <summary> Returns the current synchronization context. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
        /// null. </exception>
        /// <returns> A Threading.SynchronizationContext. </returns>
        private static SynchronizationContext CurrentSyncContext()
        {
            if ( SynchronizationContext.Current is null )
            {
                SynchronizationContext.SetSynchronizationContext( new SynchronizationContext() );
            }

            return SynchronizationContext.Current is null
                ? throw new InvalidOperationException( "Current Synchronization Context not set;. Must be set before starting the thread." )
                : SynchronizationContext.Current;
        }

        private bool _UsingAsyncNotification;
        /// <summary>   Gets or sets the notification level. </summary>
        /// <value> The notification level. </value>
        public bool UsingAsyncNotification
        {
            get => this._UsingAsyncNotification;
            set {
                if ( !bool.Equals( value, this.UsingAsyncNotification ) )
                {
                    this._UsingAsyncNotification = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a context for the active. </summary>
        /// <value> The active context. </value>
        protected SynchronizationContext ActiveContext => this.Context ?? CurrentSyncContext();

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            if ( this.UsingAsyncNotification )
                this.AsyncNotifyPropertyChanged( propertyName );
            else
                this.SyncNotifyPropertyChanged( propertyName );
        }

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void SyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            var handler = PropertyChanged;
            if ( handler != null )
                handler( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Asynchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void AsyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            var evt = this.PropertyChanged;
            if ( evt is object )
                this.ActiveContext.Post( ( object state ) => evt( this, new PropertyChangedEventArgs( propertyName ) ), null );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Core.Primitives.IGetterSetter<int> GetterSetter { get; set; }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) The name. </param>
        /// <returns>   The trait value. </returns>
        protected int? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void Setter( int? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }
        }

        #endregion

        #region " TRAITS "

        /// <summary>   Gets or sets the type of the session. </summary>
        /// <value> The type of the session. </value>
        public int SessionType
        {
            get => Conversions.ToInteger( this.Getter() );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the session number. </summary>
        /// <value> The session number. </value>
        public int SessionNumber
        {
            get => Conversions.ToInteger( this.Getter() );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets a value indicating whether the using guard. </summary>
        /// <value> True if using guard, false if not. </value>
        public bool UsingGuard
        {
            get => Conversions.ToBoolean( this.Getter() );

            set => this.Setter( value ? 1 : 0 );
        }

        /// <summary>   Gets or sets the first serial number. </summary>
        /// <value> The first serial number. </value>
        public int FirstSerialNumber
        {
            get => Conversions.ToInteger( this.Getter() );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the last serial number. </summary>
        /// <value> The last serial number. </value>
        public int LastSerialNumber
        {
            get => Conversions.ToInteger( this.Getter() );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the board number. </summary>
        /// <value> The board number. </value>
        public int BoardNumber
        {
            get => Conversions.ToInteger( this.Getter() );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the board size. </summary>
        /// <value> The size of the board. </value>
        public int BoardSize
        {
            get => Conversions.ToInteger( this.Getter() );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the binning mode. </summary>
        /// <value> The binning mode. </value>
        public int BinningMode
        {
            get => Conversions.ToInteger( this.Getter() );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the serialization mode. </summary>
        /// <value> The serialization mode. </value>
        public int SerializationMode
        {
            get => Conversions.ToInteger( this.Getter() );

            set => this.Setter( value );
        }

        #endregion

    }
}
