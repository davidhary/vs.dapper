
namespace isr.Dapper.Entities
{
    /// <summary>   A session entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class SessionEntity
    {

        /// <summary>   Gets or sets the <see cref="SessionTraitEntity"/> collection. </summary>
        /// <value> The traits. </value>
        public SessionUniqueTraitEntityCollection Traits { get; private set; }

        /// <summary>
        /// Fetches the <see cref="SessionUniqueTraitEntityCollection"/> Natural (Integer) value trait
        /// entity collection.
        /// </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of traits. </returns>
        public int FetchTraits( System.Data.IDbConnection connection )
        {
            if ( !SessionTraitTypeEntity.IsEnumerated() )
                _ = SessionTraitTypeEntity.TryFetchAll( connection );
            this.Traits = new SessionUniqueTraitEntityCollection( this.AutoId );
            this.Traits.Populate( SessionTraitEntity.FetchEntities( connection, this.AutoId ) );
            return this.Traits.Count;
        }
    }
}
