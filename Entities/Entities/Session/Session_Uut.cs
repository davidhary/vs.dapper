
using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;

namespace isr.Dapper.Entities
{
    /// <summary>   A session entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class SessionEntity
    {

        /// <summary>   Gets or sets the <see cref="SessionUniqueUutEntityCollection">Session Uuts</see>. </summary>
        /// <value> The Session Uuts entities. </value>
        public SessionUniqueUutEntityCollection Uuts { get; private set; }

        /// <summary>   Fetches the <see cref="SessionUniqueUutEntityCollection">Session Uuts</see>. </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of Uuts. </returns>
        public int FetchUuts( System.Data.IDbConnection connection )
        {
            this.Uuts = FetchUuts( connection, this.AutoId );
            foreach ( UutEntity uut in this.Uuts )
                _ = uut.FetchTraits( connection );
            return this.Uuts?.Any() == true ? this.Uuts.Count : 0;
        }

        /// <summary>   Fetches the <see cref="SessionUniqueUutEntityCollection">Session Uuts</see>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>   The Uuts. </returns>
        public static SessionUniqueUutEntityCollection FetchUuts( System.Data.IDbConnection connection, int sessionAutoId )
        {
            SessionUniqueUutEntityCollection result;
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        result = FetchUuts( new TransactedConnection( connection, transaction ), sessionAutoId );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                result = FetchUuts( transactedConnection, sessionAutoId );
            }

            return result;
        }

        /// <summary>   Fetches the <see cref="SessionUniqueUutEntityCollection">Session Uuts</see>. </summary>
        /// <remarks>   David, 2020-07-18. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>   The Uuts. </returns>
        [CLSCompliant( false )]
        public static SessionUniqueUutEntityCollection FetchUuts( TransactedConnection connection, int sessionAutoId )
        {
            if ( !UutTypeEntity.IsEnumerated() )
                _ = UutTypeEntity.TryFetchAll( connection );
#if false
            var lot = LotSessionEntity.FetchLots( connection, sessionAutoId).First();
            var part = PartLotEntity.FetchParts( connection, lot.AutoId ).First();
            _ = part.FetchElementsOnly( connection );
#endif
            var uuts = new SessionUniqueUutEntityCollection( sessionAutoId );
            uuts.Populate( SessionUutEntity.FetchOrderedUuts( connection, sessionAutoId ) );
            uuts.FetchNuts( connection );
            return uuts;
        }
    }

    /// <summary>   Collection of uut entities uniquely associated with a Session. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class SessionUniqueUutEntityCollection : UutEntityCollection
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public SessionUniqueUutEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<int, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, int>();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        public SessionUniqueUutEntityCollection( int sessionAutoId ) : this()
        {
            this.SessionAutoId = sessionAutoId;
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<int, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, int> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( UutEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.Amount, entity.AutoId );
            this._UniqueIndexDictionary.Add( entity.AutoId, entity.Amount );
        }

        /// <summary>
        /// Removes all uuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Selects an uut from the collection. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="uutNumber">    The uut number. </param>
        /// <returns>   An UutEntity. </returns>
        public UutEntity Uut( int uutNumber )
        {
            int id = this._PrimaryKeyDictionary[uutNumber];
            return this.Contains( id ) ? this[id] : new UutEntity();
        }

        /// <summary>   Uut number. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   The UUT number or -1 if not found. </returns>
        public int UutNumber( int uutAutoId )
        {
            return this._UniqueIndexDictionary.ContainsKey( uutAutoId ) ? this._UniqueIndexDictionary[uutAutoId] : -1;
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.UutEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.UutEntity"/>. </value>
        public int SessionAutoId { get; private set; }
    }


}
