using System.Collections.Generic;
using System.Linq;

using Dapper;

namespace isr.Dapper.Entities
{
    /// <summary>   A product sort entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class ProductSortEntity
    {

        /// <summary>
        /// Gets or sets the <see cref="IEnumerable{ProductSortTraitEntity}">Product Sort
        /// Trait entities. </see>.
        /// </summary>
        /// <value> The product Sort Trait entities. </value>
        public IEnumerable<ProductSortTraitEntity> TraitEntities { get; private set; }

        /// <summary>
        /// Fetches the <see cref="IEnumerable{ProductSortTraitEntity}">Product Sort Natural(Integer)-
        /// value Trait entities</see>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The entities count. </returns>
        public int FetchTraitEntities( System.Data.IDbConnection connection )
        {
            if ( !ProductSortTraitTypeEntity.IsEnumerated() )
                _ = ProductSortTraitTypeEntity.TryFetchAll( connection );
            return this.Populate( ProductSortTraitEntity.FetchEntities( connection, this.AutoId ) );
        }

        /// <summary>
        /// Fetches the <see cref="IEnumerable{ProductSortTraitEntity}">Product Sort Natural(Integer)-
        /// value Trait entities</see>.
        /// </summary>
        /// <remarks>
        /// https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
        /// https://dapper-tutorial.net/querymultiple.
        /// </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>   The entities count. </returns>
        public int FetchTraitEntities( System.Data.IDbConnection connection, int productSortAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"select * from [{ProductSortBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $" where {nameof( ProductSortNub.AutoId )} = @Id; " );
            _ = queryBuilder.AppendLine( $"select * from [{ProductSortTraitBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $" where {nameof( ProductSortTraitNub.PrimaryId )} = @Id; " );
            var gridReader = connection.QueryMultiple( queryBuilder.ToString(), new { Id = productSortAutoId } );
            var entity = gridReader.ReadSingle<ProductSortNub>();
            if ( entity is null )
            {
                return this.Populate( new List<ProductSortTraitEntity>() );
            }
            else
            {
                if ( !KeyForeignLabelNaturalNub.AreEqual( entity, this.ICache ) )
                {
                    this.UpdateCache( entity );
                    _ = this.UpdateStore();
                }

                if ( !ProductSortTraitTypeEntity.IsEnumerated() )
                    _ = ProductSortTraitTypeEntity.TryFetchAll( connection );
                return this.Populate( ProductSortTraitEntity.Populate( gridReader.Read<ProductSortTraitNub>() ) );
            }
        }

        /// <summary>   Gets or sets the <see cref="ProductUniqueSortTraitEntityCollection"/>. </summary>
        /// <value> The <see cref="ProductUniqueSortTraitEntityCollection"/>. </value>
        public ProductUniqueSortTraitEntityCollection ProductSortTraits { get; private set; }

        /// <summary>   Fetches <see cref="ProductUniqueSortTraitEntityCollection"/>. </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The <see cref="ProductUniqueSortTraitEntityCollection"/>. </returns>
        public int FetchTraits( System.Data.IDbConnection connection )
        {
            if ( !ProductSortTraitTypeEntity.IsEnumerated() )
                _ = ProductSortTraitTypeEntity.TryFetchAll( connection );
            return this.Populate( ProductSortTraitEntity.FetchEntities( connection, this.AutoId ) );
        }

        /// <summary>   Fetches <see cref="ProductUniqueSortTraitEntityCollection"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>   The <see cref="ProductUniqueSortTraitEntityCollection"/>. </returns>
        public static ProductUniqueSortTraitEntityCollection FetchTraits( System.Data.IDbConnection connection, int productSortAutoId )
        {
            if ( !ProductSortTraitTypeEntity.IsEnumerated() )
                _ = ProductSortTraitTypeEntity.TryFetchAll( connection );
            var productSortTraits = new ProductUniqueSortTraitEntityCollection( productSortAutoId );
            productSortTraits.Populate( ProductSortTraitEntity.FetchEntities( connection, productSortAutoId ) );
            return productSortTraits;
        }

        /// <summary>   Populates a list of <see cref="ProductSortTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-24. </remarks>
        /// <param name="entities"> The <see cref="ProductSortTraitEntity"/>'s. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        private int Populate( IEnumerable<ProductSortTraitEntity> entities )
        {
            this.TraitEntities = entities;
            if ( this.ProductSortTraits is null )
            {
                this.ProductSortTraits = new ProductUniqueSortTraitEntityCollection( this.AutoId );
            }
            else
            {
                this.ProductSortTraits.Clear();
            }

            this.ProductSortTraits.Populate( entities );
            this.NotifyPropertyChanged( nameof( ProductSortEntity.TraitEntities ) );
            this.NotifyPropertyChanged( nameof( ProductSortEntity.ProductSortTraits ) );
            return entities?.Any() == true ? entities.Count() : 0;
        }
    }
}
