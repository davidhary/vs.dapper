
using isr.Dapper.Entities.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Entities.Taper.Tests
{
    public abstract partial class TestSuite
    {

        #region " DATABASE BUILDER "


        #endregion

        #region " LOOKUP "

        /// <summary> (Unit Test Method) tests the meter entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void MeterEntityTest()
        {
            Auditor.AssertMeterEntity( TestInfo, this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests Meter Model entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void MeterModelEntityTest()
        {
            Auditor.AssertMeterModelEntity( TestInfo, this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests reading type entity. </summary>
        /// <remarks> David, 2020-06-02. </remarks>
        [TestMethod]
        public void ReadingTypeEntityTest()
        {
            Auditor.AssertReadingTypeEntity( TestInfo, this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests reading bin entity. </summary>
        /// <remarks> David, 2020-06-02. </remarks>
        [TestMethod]
        public void ReadingBinEntityTest()
        {
            Auditor.AssertReadingBinEntity( TestInfo, this.GetProvider(), ReadingBin.Good );
        }

        /// <summary> (Unit Test Method) tests tolerance entity. </summary>
        /// <remarks> David, 2020-06-02. </remarks>
        [TestMethod]
        public void ToleranceEntityTest()
        {
            Auditor.AssertToleranceEntity( TestInfo, this.GetProvider() );
        }

        #endregion

        #region " ELEMENT + ELEMENT ATTRIBUTES "

        /// <summary> (Unit Test Method) tests element entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void ElementEntityTest()
        {
            string productNumber = "D1206LF";
            var product = Auditor.AssertAddingProductEntity( TestInfo, this.GetProvider(), productNumber );
            string partNumber = "PFC-D1206LF-02-1001-3301-FB";
            var part = Auditor.AssertAddingPartEntity( TestInfo, this.GetProvider(), partNumber );
            _ = Auditor.AssertAddingProductPartEntity( TestInfo, this.GetProvider(), product.AutoId, part.AutoId );

            using var connection = this.GetProvider().GetConnection();
            // Use 2002 meter to allow selection of equations.
            int meterNumber = 1;
            int meterModel = ( int ) MeterModel.K2002;
            if ( MeterEntity.Meters is not object ) _ = MeterEntity.TryFetchAll( connection );
            PlatformMeter platformMeter = new( MeterEntity.SelectMeterEntity( meterModel, meterNumber ) );
            PlatformMeter[] platformMeters = new PlatformMeter[] { platformMeter };
            // int meterId = platformMeter.MeterId;

            _ = Auditor.AssertAddingProductElementEntities( TestInfo, this.GetProvider(), product, part, platformMeter );
        }

        #endregion

        #region " LOT "

        /// <summary> (Unit Test Method) tests lot entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void LotEntityTest()
        {
            // PFC-D1206LF-03-6122-2442-FB-2728  1725356 1770169
            // PFC-W0402LF-03-1000-B-4013  1749153  1749156 1725105
            // PFC-D1206LF-03-3202-1002-DB-2728 1733251 1733247  1733248  1733249
            string partNumber = "PFC-D1206LF-03-6122-2442-FB-2728";
            var lotNumbers = new string[] { "1725356", "1770169" };
            Auditor.AssertLotEntity( TestInfo, this.GetProvider(), partNumber, lotNumbers );
        }

        #endregion

        #region " NUT + UUT "

        /// <summary> (Unit Test Method) tests nut entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void NutEntityTest()
        {
            // Assert.Inconclusive( "Traits needs to be set for the modified platform meter design" );
            // tests adding Nut to a UUT with readings and bin.
            string productNumber = "D1206LF";
            string partNumber = "PFC-D1206LF-03-3202-1002-DB-2728";
            var lotNumbers = new string[] { "1733251", "1733247" };
            Auditor.AssertNutEntity( TestInfo, this.GetProvider(), productNumber, partNumber, new string[] { $"{lotNumbers[1]}:1", $"{lotNumbers[1]}:2" } );
        }

        /// <summary> (Unit Test Method) tests uut entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void UutEntityTest()
        {
            var lotNumbers = new string[] { "1725356", "1770169" };
            var session = Auditor.AssertAddingSessionEntity( TestInfo, this.GetProvider(), $"{lotNumbers[1]}:1" );
            // tests adding a UUT to the Lot and sorting using a bin number
            Auditor.AssertUutEntity( TestInfo, this.GetProvider(), new string[] {session.SessionLabel} );
        }

#endregion

#region " PART "

        /// <summary> (Unit Test Method) tests part entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void PartEntityTest()
        {
            var partNumbers = new string[] { "PFC-W0402LF-03-1000-B-4013", "PFC-W0402LF-03-1001-B-4013", "PFC-D1206LF-03-3202-1002-DB-2728", "PFC-D1206LF-03-6122-2442-FB-2728", "WIN-T0603LF-03-3091-B", "WIN-T0603LF-03-2002-B", "WIN-T0603LF-03-1002-B" };
            Auditor.AssertPartEntity( TestInfo, this.GetProvider(), partNumbers );
        }

        /// <summary> (Unit Test Method) tests part specification type entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void PartSpecificationTypeEntityTest()
        {
            Auditor.AssertPartSpecificationTypeEntity( TestInfo, this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests part specification entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void PartSpecificationEntityTest()
        {
            // tests adding specifications to the part.
            Auditor.AssertPartSpecifications( TestInfo, this.GetProvider() );
        }

#endregion

#region " PLATFORM "

        /// <summary> (Unit Test Method) tests the release history entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void RevisionEntityTest()
        {
            // Auditor.AssertUpdateRevisionEntity(TestSuite.TestInfo, Me.GetProvider)
            Auditor.AssertRevisionEntity( TestInfo, this.GetProvider(), CurrentDatabaseVersion );
        }

        /// <summary> (Unit Test Method) tests station entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void StationEntityTest()
        {
            Auditor.AssertStationEntity( TestInfo, this.GetProvider(), true );
        }

#endregion

#region " PRODUCT "

        /// <summary> (Unit Test Method) tests product entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void ProductEntityTest()
        {
            string productNumber = "D1206LF";
            string partNumber = "PFC-D1206LF-03-6122-2442-FB-2728";
            var lotNumbers = new string[] { "1725356", "1770169" };
            string lotNumber = lotNumbers[0];
            int sessionNumber = 1;
            Auditor.AssertProductEntity( TestInfo, this.GetProvider(), productNumber, partNumber, lotNumber, sessionNumber );
        }

#endregion

#region " SESSION "

        /// <summary> (Unit Test Method) tests the Meter Guard Band Lookup entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void MeterGuardBandLookupEntityTest()
        {
            Auditor.AssertMeterGuardBandLookupEntity( TestInfo, this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests ohm meter setup entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void OhmMeterSetupEntityTest()
        {
            Auditor.AssertOhmMeterSetupEntity( TestInfo, this.GetProvider() );
        }

        /// <summary>   (Unit Test Method) tests lot ohm meter setup entity. </summary>
        /// <remarks>   David, 2020-06-15. </remarks>
        [TestMethod]
        public void SessionOhmMeterSetupEntityTest()
        {
            var lotNumbers = new string[] { "1725356", "1770169" };
            var session = Auditor.AssertAddingSessionEntity( TestInfo, this.GetProvider(), $"{lotNumbers[1]}:1" );
            Auditor.AssertSessionOhmMeterSetupEntity( TestInfo, this.GetProvider(), session.AutoId );
        }

        /// <summary> (Unit Test Method) tests session entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void SessionEntityTest()
        {
            var lotNumbers = new string[] { "1725356", "1770169" };
            _ = Auditor.AssertAddingSessionEntity( TestInfo, this.GetProvider(), $"{lotNumbers[1]}:1" );
        }

        /// <summary>   (Unit Test Method) tests session type entity. </summary>
        /// <remarks>   David, 2021-06-03. </remarks>
        [TestMethod]
        public void SessionTypeEntityTest()
        {
            Auditor.AssertSessionTypeEntity( TestInfo, this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests Session Trait Type entity. </summary>
        /// <remarks> David, 2020-06-17. </remarks>
        [TestMethod]
        public void SessionTraitTypeEntityTest()
        {
            Auditor.AssertSessionTraitTypeEntity( TestInfo, this.GetProvider() );
        }

        /// <summary> Tests session trait entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public void SessionTraitEntityTest()
        {
            var lotNumbers = new string[] { "1725356", "1770169" };
            Auditor.AssertAddingSessionTraits( TestInfo, this.GetProvider(), $"{lotNumbers[0]}:1" );
        }

        /// <summary> Tests lot session entity. </summary>
        /// <remarks> David, 2020-05-05. </remarks>
        [TestMethod]
        public void LotSessionEntityTest()
        {
            string partNumber = "PFC-D1206LF-03-6122-2442-FB-2728";
            var lotNumbers = new string[] { "1725356", "1770169" };
            string lotNumber = lotNumbers[0];
            Auditor.AssertAddingLotSessionEntities( TestInfo, this.GetProvider(), partNumber, lotNumber, 2 );
        }

#endregion

    }
}
