using isr.Dapper.Entity;
using isr.Dapper.Entities.Tests;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Entities.Taper.Tests
{

    /// <summary> An Ohmni Taper Test Suite for Dapper Entities. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-26 </para>
    /// </remarks>
    [TestClass()]
    public abstract partial class TestSuite
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Base class initialize. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void BaseClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    BaseClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        public static void BaseClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            Assert.IsTrue( TestSuiteSettings.Get().Exists, $"{typeof( TestSuiteSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo?.AssertMessageQueue();
        }

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets or sets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " PROVIDER "

        /// <summary> Gets the provider. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The provider. </returns>
        public abstract ProviderBase GetProvider();


        /// <summary> Gets the connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private System.Data.IDbConnection GetConnection()
        {
            return this.GetProvider().GetConnection();
        }

        /// <summary> (Unit Test Method) queries if a given connection exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void ConnectionExists()
        {
            Auditor.AssertConnectionExists( this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests tables exist. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public virtual void TablesExistTest()
        {
            Auditor.AssertTablesExist( this.GetProvider(), SchemaBuilder.TableNames );
        }

        #endregion

    }
}
