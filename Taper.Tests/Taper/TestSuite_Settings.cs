using isr.Dapper.Entity;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Dapper.Entities.Taper.Tests
{

    /// <summary> An Ohmni Taper Test Suite Settings for Dapper Entities. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-26 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode( "Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0" )]
    [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Advanced )]
    public class TestSuiteSettings : Core.ApplicationSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        /// class to its default state.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private TestSuiteSettings() : base()
        {
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static TestSuiteSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static TestSuiteSettings Get()
        {
            if ( _Instance is null )
            {
                lock ( _SyncLocker )
                    _Instance = ( TestSuiteSettings ) Synchronized( new TestSuiteSettings() );
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get {
                lock ( _SyncLocker )
                    return _Instance is object;
            }
        }

        #endregion

        #region " CONFIGURATION SETTINGS "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public bool Exists
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public bool Verbose
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public bool Enabled
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public bool All
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        #endregion

        #region " SQLite SETTINGS "

        /// <summary> Gets or sets the sq lite connection string. </summary>
        /// <value> The sq lite connection string. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( @"Data Source=r:\Ohmni\Taper\TestDb\OhmniTaper.sqlite;Mode=ReadWriteCreate;datetimeformat=CurrentCulture" )]
        public string SQLiteConnectionString
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        #endregion

        #region " SqlServer SETTINGS "

        /// <summary> Gets or sets the connection string. </summary>
        /// <value> The connection string. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( @"Data Source=localhost\sqlx2019;Initial Catalog=tempdb;Integrated Security=True" )]
        public string SqlServerConnectionString
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        #endregion

        #region " BUILD SETTINGS "

        /// <summary>
        /// Gets or sets the delete option determining which, if any, tables types are deleted.
        /// </summary>
        /// <value> The delete option. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "0" )]
        public TableCategory DeleteOption
        {
            get {
                return ( TableCategory ) Conversions.ToInteger( AppSettingGetter( 0 ) );
            }

            set {
                AppSettingSetter( ( int ) value );
            }
        }

        /// <summary> Gets or sets the drop tables option. </summary>
        /// <value> The drop tables option. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "0" )]
        public TableCategory DropTablesOption
        {
            get {
                return ( TableCategory ) Conversions.ToInteger( AppSettingGetter( 0 ) );
            }

            set {
                AppSettingSetter( ( int ) value );
            }
        }

        /// <summary>
        /// Gets or sets the clear database sentinel determining if the database tables are dropped
        /// before new tables are created.
        /// </summary>
        /// <value> The clear database sentinel. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public bool ClearDatabase
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the add tables. </summary>
        /// <value> The add tables. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public bool AddTables
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the ignore drop table errors. </summary>
        /// <value> The ignore drop table errors. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public bool IgnoreDropTableErrors
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the create database sentinel. </summary>
        /// <value> The create database. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public bool CreateDatabase
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the pathname of the schema folder. </summary>
        /// <value> The pathname of the schema folder. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( @"r:\Ohmni\Taper\Schema" )]
        public string SchemaFolderName
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the SQL server file label. </summary>
        /// <value> The SQL server file label. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "SqlServer" )]
        public string SqlServerFileLabel
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the sq lite file label. </summary>
        /// <value> The sq lite file label. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "SQLite" )]
        public string SQLiteFileLabel
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the guard band lookup file name format. </summary>
        /// <value> The guard band lookup file name format. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "AddGuardBandLookupValues{0}.sql" )]
        public string GuardBandLookupFileNameFormat
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the add import file records file name format. </summary>
        /// <value> The add import file records file name format. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "AddImportFileRecords{0}.sql" )]
        public string AddImportFileRecordsFileNameFormat
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the add ohm meter setting lookup file name format. </summary>
        /// <value> The add ohm meter setting lookup file name format. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "AddOhmMeterSettingLookupValues{0}.sql" )]
        public string AddOhmMeterSettingLookupFileNameFormat
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the schema file name format. </summary>
        /// <value> The schema file name format. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "AddSchema{0}.sql" )]
        public string SchemaFileNameFormat
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the schema data file name format. </summary>
        /// <value> The schema data file name format. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "AddData{0}.sql" )]
        public string SchemaDataFileNameFormat
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the clear data file name format. </summary>
        /// <value> The clear data file name format. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "ClearData{0}.sql" )]
        public string ClearDataFileNameFormat
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the clear data upon initialize. </summary>
        /// <value> The clear data upon initialize. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public bool ClearDataUponInitialize
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the database revision. </summary>
        /// <value> The database revision. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "1.0.7502" )]
        public string DatabaseRevision
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the pathname of the files folder. </summary>
        /// <value> The pathname of the files folder. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( @"C:\Users\Public\Documents\Ohmni\Taper\Files" )]
        public string FilesFolderName
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the filename of the import file. </summary>
        /// <value> The filename of the import file. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "OhmniChipDb.csv" )]
        public string ImportFileName
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        #endregion

    }
}
