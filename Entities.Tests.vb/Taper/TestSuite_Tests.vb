
Imports System.Runtime.CompilerServices

Namespace Global.isr.Dapper.Entities.Tests.Ohmni.Taper

    Partial Public MustInherit Class TestSuite

        #Region " DATABASE BUILDER "


        #End Region

        #Region " LOOKUP "

        ''' <summary> (Unit Test Method) tests the multimeter entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub MultimeterEntityTest()
            Entities.Tests.Auditor.AssertMultimeterEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests Meter Model entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub MultimeterModelEntityTest()
            Entities.Tests.Auditor.AssertMultimeterModelEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests Nominal reading type entity. </summary>
        ''' <remarks> David, 6/2/2020. </remarks>
        <TestMethod>
        Public Sub NomReadingTypeEntityTest()
            Entities.Tests.Auditor.AssertNomReadingTypeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests reading bin entity. </summary>
        ''' <remarks> David, 6/2/2020. </remarks>
        <TestMethod>
        Public Sub ReadingBinEntityTest()
            Entities.Tests.Auditor.AssertReadingBinEntity(TestSuite.TestInfo, Me.GetProvider, isr.Dapper.Entities.ReadingBin.Good)
        End Sub

        ''' <summary> (Unit Test Method) tests tolerance entity. </summary>
        ''' <remarks> David, 6/2/2020. </remarks>
        <TestMethod>
        Public Sub ToleranceEntityTest()
            Entities.Tests.Auditor.AssertToleranceEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        #End Region

        #Region " ELEMENT + ELEMENT ATTRIBUTES "

        ''' <summary> (Unit Test Method) tests element entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub ElementEntityTest()
            Dim productNumber As String = "D1206LF"
            Dim product As ProductEntity = Auditor.AssertAddingProductEntity(TestSuite.TestInfo, Me.GetProvider, productNumber)
            Dim partNumber As String = "PFC-D1206LF-02-1001-3301-FB"
            Dim part As PartEntity = Auditor.AssertAddingPartEntity(TestSuite.TestInfo, Me.GetProvider, partNumber)
            Dim productPart As Entities.ProductPartEntity = Entities.Tests.Auditor.AssertAddingProductPartEntity(TestSuite.TestInfo, Me.GetProvider, product.AutoId, part.AutoId)
            Auditor.AssertAddingProductElementEntities(TestSuite.TestInfo, Me.GetProvider, product, part)
        End Sub

        #End Region

        #Region " LOT "

        ''' <summary> (Unit Test Method) tests lot entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub LotEntityTest()
            ' PFC-D1206LF-03-6122-2442-FB-2728  1725356 1770169
            ' PFC-W0402LF-03-1000-B-4013  1749153  1749156 1725105
            ' PFC-D1206LF-03-3202-1002-DB-2728 1733251 1733247  1733248  1733249
            Dim partNumber As String = "PFC-D1206LF-03-6122-2442-FB-2728"
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Auditor.AssertLotEntity(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumbers)
        End Sub

        ''' <summary> (Unit Test Method) tests lot ohm meter setup entity. </summary>
        ''' <remarks> David, 6/15/2020. </remarks>
        <TestMethod>
        Public Sub LotOhmMeterSetupEntityTest()
            Dim partNumber As String = "PFC-D1206LF-03-3202-1002-DB-2728"
            Dim lotNumbers As String() = New String() {"1733251", "1733247"}
            Dim lots As IEnumerable(Of LotEntity) = Auditor.AssertAddingLots(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumbers)
            For Each lot As LotEntity In lots
                Entities.Tests.Auditor.AssertLotOhmMeterSetupEntity(TestSuite.TestInfo, Me.GetProvider, lot.AutoId)
            Next
        End Sub

        #End Region

        #Region " NUT + UUT "

        ''' <summary> (Unit Test Method) tests nut entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub NutEntityTest()
            ' tests adding Nut to a UUT with readings and bin.
            Dim productNumber As String = "D1206LF"
            Dim partNumber As String = "PFC-D1206LF-03-3202-1002-DB-2728"
            Dim lotNumbers As String() = New String() {"1733251", "1733247"}
            Auditor.AssertNutEntity(TestSuite.TestInfo, Me.GetProvider, productNumber, partNumber, lotNumbers)
        End Sub

        ''' <summary> (Unit Test Method) tests uut entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub UutEntityTest()
            Dim partNumber As String = "PFC-D1206LF-03-3202-1002-DB-2728"
            Dim lotNumbers As String() = New String() {"1733251", "1733247"}
            Dim lots As IEnumerable(Of LotEntity) = Auditor.AssertAddingLots(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumbers)
            ' tests adding a UUT to the Lot and sorting using a bin number
            Auditor.AssertUutEntity(TestSuite.TestInfo, Me.GetProvider, lotNumbers)
        End Sub

        #End Region

        #Region " PART "

        ''' <summary> (Unit Test Method) tests part entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub PartEntityTest()
            Dim partNumbers As String() = New String() {"PFC-W0402LF-03-1000-B-4013",
                                                        "PFC-W0402LF-03-1001-B-4013",
                                                        "PFC-D1206LF-03-3202-1002-DB-2728",
                                                        "PFC-D1206LF-03-6122-2442-FB-2728",
                                                        "WIN-T0603LF-03-3091-B",
                                                        "WIN-T0603LF-03-2002-B",
                                                        "WIN-T0603LF-03-1002-B"}
            Auditor.AssertPartEntity(TestSuite.TestInfo, Me.GetProvider, partNumbers)
        End Sub

        ''' <summary> (Unit Test Method) tests part specification type entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub PartSpecificationTypeEntityTest()
            Entities.Tests.Auditor.AssertPartSpecificationTypeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests part specification entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub PartSpecificationEntityTest()
            ' tests adding specifications to the part.
            Auditor.AssertPartSpecifications(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        #End Region

        #Region " PLATFORM "

        ''' <summary> (Unit Test Method) tests the release history entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub RevisionEntityTest()
            ' Auditor.AssertUpdateRevisionEntity(TestSuite.TestInfo, Me.GetProvider)
            Auditor.AssertRevisionEntity(TestSuite.TestInfo, Me.GetProvider, TestSuite.CurrentDatabaseVersion)
        End Sub

        ''' <summary> (Unit Test Method) tests station entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub StationEntityTest()
            Auditor.AssertStationEntity(TestSuite.TestInfo, Me.GetProvider, True)
        End Sub

        #End Region

        #Region " PRODUCT "

        ''' <summary> (Unit Test Method) tests product entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub ProductEntityTest()
            Dim productNumber As String = "D1206LF"
            Dim partNumber As String = "PFC-D1206LF-03-6122-2442-FB-2728"
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Dim lotNumber As String = lotNumbers(0)
            Auditor.AssertProductEntity(TestSuite.TestInfo, Me.GetProvider, productNumber, partNumber, lotNumber)
        End Sub

        #End Region

        #Region " SESSION "

        ''' <summary> (Unit Test Method) tests the Meter Guard Band Lookup entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub MeterGuardBandLookupEntityTest()
            Entities.Tests.Auditor.AssertMeterGuardBandLookupEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests ohm meter setup entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub OhmMeterSetupEntityTest()
            Entities.Tests.Auditor.AssertOhmMeterSetupEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests session entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub SessionEntityTest()
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Auditor.AssertAddingSessionEntity(TestSuite.TestInfo, Me.GetProvider, $"{lotNumbers(1)}:1")
        End Sub

        ''' <summary> (Unit Test Method) tests Session Trait Type entity. </summary>
        ''' <remarks> David, 6/17/2020. </remarks>
        <TestMethod>
        Public Sub SessionAttributeTypeEntityTest()
            Auditor.AssertSessionTraitTypeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> Tests session attribute entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Public Sub SessionAttributeEntityTest()
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Auditor.AssertAddingSessionTraits(TestSuite.TestInfo, Me.GetProvider, $"{lotNumbers(0)}:1")
        End Sub

        ''' <summary> Tests lot session entity. </summary>
        ''' <remarks> David, 2020-05-05. </remarks>
        <TestMethod>
        Public Sub LotSessionEntityTest()
            Dim partNumber As String = "PFC-D1206LF-03-6122-2442-FB-2728"
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Dim lotNumber As String = lotNumbers(0)
            Auditor.AssertAddingLotSessionEntities(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumber, 2)
        End Sub

        #End Region

    End Class

End Namespace


