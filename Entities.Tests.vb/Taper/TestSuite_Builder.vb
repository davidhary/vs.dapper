Imports Dapper
Imports isr.Dapper.Entity
Namespace Global.isr.Dapper.Entities.Tests.Ohmni.Taper

    Partial Public Class TestSuite

        ''' <summary> Initializes this object. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="provider"> The provider. </param>
        Public Shared Sub Initialize(ByVal provider As ProviderBase)
            Console.WriteLine($".NET {Environment.Version}")
            Console.WriteLine($"Dapper {GetType(Global.Dapper.SqlMapper).AssemblyQualifiedName}")
            Console.WriteLine($"Connection string: {provider.ConnectionString}")
            TestSuite._Provider = provider
            ' test naked connection
            Assert.IsTrue(TestSuite.Provider.IsConnectionExists(), $"connection {TestSuite.Provider.GetConnection} does not exist")
            ' test connection with event handling
            Using connection As System.Data.IDbConnection = TestSuite.Provider.GetConnection()
                Assert.IsTrue(connection.Exists(), "connection does not exist")
            End Using
            If provider Is Nothing Then
                TestSuite._SchemaBuilder = New SchemaBuilder
            Else
                TestSuite._SchemaBuilder = New SchemaBuilder With {.Provider = provider}
                TestSuite.EnumerateSchemaObjects(provider.ProviderType)
            End If
            ' this prevents testing.
            TestSuite.BuildDatabase(provider.BuildMasterConnectionString(False))
            TestSuite.SchemaBuilder.AssertSchemaObjectsExist()

            If TestSuiteSettings.Get.ClearDataUponInitialize Then
                TestSuite.AssertClearData(provider)
            End If

            TestSuite.InitializeLookupEntities(provider)

        End Sub

        ''' <summary> Try clear data. </summary>
        ''' <remarks> David, 6/13/2020. </remarks>
        ''' <param name="provider"> The provider. </param>
        ''' <returns> The (Success As Boolean, Details As String) </returns>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        Friend Shared Function TryClearData(ByVal provider As isr.Dapper.Entity.ProviderBase) As (Success As Boolean, Details As String)
            Dim providerFileLabel As String = If(provider.ProviderType = isr.Dapper.Entity.ProviderType.SQLite,
                                                    TestSuiteSettings.Get.SQLiteFileLabel, TestSuiteSettings.Get.SqlServerFileLabel)
            Dim schemaFileName As String = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                                  String.Format(TestSuiteSettings.Get.ClearDataFileNameFormat, providerFileLabel))
            Dim count As Integer
            count = provider.ExecuteScript(schemaFileName)
            Return If(count >= 0, (True, String.Empty), (False, $"Failed clearing '{provider.DatabaseName}' data from {schemaFileName}"))
        End Function

        ''' <summary> Assert clear data. </summary>
        ''' <remarks> David, 3/25/2020. </remarks>
        ''' <param name="provider"> The provider. </param>
        Friend Shared Sub AssertClearData(ByVal provider As ProviderBase)
            Dim r As (Success As Boolean, Details As String)
            r = TestSuite.TryClearData(provider)
            Assert.IsTrue(r.Success, $"database could not be created because {r.Details}")
        End Sub

        ''' <summary> Cleanups this object. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Public Shared Sub Cleanup()
            If TableCategory.None <> TestSuiteSettings.Get.DeleteOption Then
                TestSuite.SchemaBuilder.DeleteAllRecords(TestSuiteSettings.Get.DeleteOption)
            End If
            If TableCategory.None <> TestSuiteSettings.Get.DropTablesOption Then
                TestSuite.SchemaBuilder.DropAllTables(TestSuiteSettings.Get.DropTablesOption, TestSuiteSettings.Get.IgnoreDropTableErrors)
            End If
        End Sub

        ''' <summary> Gets or sets the provider. </summary>
        ''' <value> The provider. </value>
        Public Shared ReadOnly Property Provider As ProviderBase

        ''' <summary> The Schema builder. </summary>
        ''' <value> The schema builder. </value>
        Public Shared ReadOnly Property SchemaBuilder As SchemaBuilder

        ''' <summary> Enumerate tables and view. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="providerType"> Type of the provider. </param>
        Public Shared Sub EnumerateSchemaObjects(ByVal providerType As ProviderType)
            Try
                TestSuite.EnumerateSchemaObjectsThis(providerType)
            Catch
                TestSuite.SchemaBuilder.EnumeratedTables.Clear()
                Throw
            End Try
        End Sub

        ''' <summary> Enumerate schema objects this. </summary>
        ''' <remarks> David, 2020-07-04. </remarks>
        ''' <param name="providerType"> Type of the provider. </param>
        Private Shared Sub EnumerateSchemaObjectsThis(ByVal providerType As ProviderType)

            Dim providerFileLabel As String = If(providerType = ProviderType.SQLite, TestSuiteSettings.Get.SQLiteFileLabel, TestSuiteSettings.Get.SqlServerFileLabel)

            Dim schemaBuilder As SchemaBuilder = TestSuite.SchemaBuilder
            schemaBuilder.EnumeratedTables.Clear()
            Dim tb As New TableMaker

            ' BUCKET BIN 
            tb = New TableMaker With {.TableName = Entities.BucketBinBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.BucketBinBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.BucketBinBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.BucketBinEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' LOT TRAIT TYPE 
            tb = New TableMaker With {.TableName = Entities.LotTraitTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.LotTraitTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.LotTraitTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.LotTraitTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' MULTIMETER MODEL
            tb = New TableMaker With {.TableName = Entities.MultimeterModelBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.MultimeterModelBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.MultimeterModelBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.MultimeterModelEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' NOMINAL TYPE 
            tb = New TableMaker With {.TableName = Entities.NomTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.NomTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.NomTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.NomTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' NOMINAL TRAIT TYPE 
            tb = New TableMaker With {.TableName = Entities.NomTraitTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.NomTraitTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.NomTraitTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.NomTraitTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PRODUCT SORT TRAIT TYPE
            tb = New TableMaker With {.TableName = Entities.ProductSortTraitTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.ProductSortTraitTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.ProductSortTraitTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.ProductSortTraitTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PRODUCT TRAIT TYPE
            tb = New TableMaker With {.TableName = Entities.ProductTraitTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.ProductTraitTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.ProductTraitTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.ProductTraitTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' READING BIN
            tb = New TableMaker With {.TableName = Entities.ReadingBinBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.ReadingBinBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.ReadingBinBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.ReadingBinEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' NOMINAL READING TYPE
            tb = New TableMaker With {.TableName = Entities.NomReadingTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.NomReadingTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.NomReadingTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.NomReadingTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' SAMPLE TRAIT TYPE
            tb = New TableMaker With {.TableName = Entities.SampleTraitTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.SampleTraitTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.SampleTraitTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.SampleTraitTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' SESSION TRAIT TYPE 
            tb = New TableMaker With {.TableName = Entities.SessionTraitTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.SessionTraitTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.SessionTraitTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.SessionTraitTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' TOLERANCE
            tb = New TableMaker With {.TableName = Entities.ToleranceBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.ToleranceBuilder.CreateTable,
                                      .InsertAction = AddressOf Entities.ToleranceBuilder.InsertValues,
                                      .FetchAllAction = AddressOf Entities.ToleranceEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' YIELD TRAIT TYPE
            tb = New TableMaker With {.TableName = Entities.YieldTraitTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.YieldTraitTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.YieldTraitTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.YieldTraitTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' MULTIMETER 
            Entities.MultimeterBuilder.Get.ForeignTableName = Entities.MultimeterModelBuilder.TableName
            Entities.MultimeterBuilder.Get.ForeignTableKeyName = NameOf(Entities.MultimeterModelNub.Id)
            tb = New TableMaker With {.TableName = Entities.MultimeterBuilder.Get.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.MultimeterBuilder
                                                          Return s.CreateTable(x, Entities.UniqueIndexOptions.ForeignIdAmount)
                                                      End Function,
                                      .InsertAction = AddressOf Entities.MultimeterBuilder.Get.InsertValues,
                                      .FetchAllAction = AddressOf Entities.MultimeterEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' ELEMENT TYPE
            tb = New TableMaker With {.TableName = Entities.ElementTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.ElementTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.ElementTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.ElementTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' ELEMENT
            ' the element has a non-unique label. the element label is unique for the part, which is enforced in code.
            tb = New TableMaker With {.TableName = Entities.ElementBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.ElementBuilder
                                                          Return s.CreateTable(x, Entities.UniqueIndexOptions.None)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' ELEMENT NOMINAL TYPE
            tb = New TableMaker With {.TableName = Entities.ElementNomTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.ElementNomTypeBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' METER GUARD BAND LOOKUP
            Entities.MeterGuardBandLookupBuilder.MultimeterTableName = Entities.MultimeterBuilder.TableName
            Entities.MeterGuardBandLookupBuilder.MultimeterTableKeyName = NameOf(Entities.MultimeterNub.Id)
            Entities.MeterGuardBandLookupBuilder.NomTypeTableName = Entities.NomTypeBuilder.TableName
            Entities.MeterGuardBandLookupBuilder.NomTypeTableKeyName = NameOf(Entities.NomTypeNub.Id)
            tb = New TableMaker With {.TableName = Entities.MeterGuardBandLookupBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.MeterGuardBandLookupBuilder.CreateTable,
                                      .InsertFileValuesAction = AddressOf Entities.MeterGuardBandLookupBuilder.InsertFileValues,
                                      .ValuesFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                                               String.Format(TestSuiteSettings.Get.GuardBandLookupFileNameFormat, providerFileLabel)),
                                      .FetchAllAction = AddressOf Entities.MeterGuardBandLookupEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' OHM METER SETTINGS
            tb = New TableMaker With {.TableName = Entities.OhmMeterSettingBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.OhmMeterSettingBuilder.CreateTable,
                                      .InsertAction = AddressOf Entities.OhmMeterSettingBuilder.InsertValues,
                                      .FetchAllAction = AddressOf Entities.OhmMeterSettingEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' OHM METER SETTINGS LOOKUP
            Entities.OhmMeterSettingLookupBuilder.MultimeterTableName = Entities.MultimeterBuilder.TableName
            Entities.OhmMeterSettingLookupBuilder.MultimeterTableKeyName = NameOf(Entities.MultimeterNub.Id)
            tb = New TableMaker With {.TableName = Entities.OhmMeterSettingLookupBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.OhmMeterSettingLookupBuilder.CreateTable,
                                      .InsertFileValuesAction = AddressOf Entities.OhmMeterSettingLookupBuilder.InsertFileValues,
                                      .ValuesFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                                              String.Format(TestSuiteSettings.Get.AddOhmMeterSettingLookupFileNameFormat, providerFileLabel)),
                                      .FetchAllAction = AddressOf Entities.OhmMeterSettingLookupEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' OHM METER SETUP
            tb = New TableMaker With {.TableName = Entities.OhmMeterSetupBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.OhmMeterSetupBuilder.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' COMPUTER
            tb = New TableMaker With {.TableName = Entities.ComputerBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.ComputerBuilder
                                                          Return s.CreateTable(x, True)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' STATION
            tb = New TableMaker With {.TableName = Entities.StationBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.StationBuilder
                                                          Return s.CreateTable(x, True)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' COMPUTER STATION
            Entities.ComputerStationBuilder.Get.PrimaryTableName = Entities.ComputerBuilder.TableName
            Entities.ComputerStationBuilder.Get.PrimaryTableKeyName = NameOf(Entities.ComputerNub.AutoId)
            Entities.ComputerStationBuilder.Get.SecondaryTableName = Entities.StationBuilder.TableName
            Entities.ComputerStationBuilder.Get.SecondaryTableKeyName = NameOf(Entities.StationNub.AutoId)
            tb = New TableMaker With {.TableName = Entities.ComputerStationBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.ComputerStationBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' SESSION
            tb = New TableMaker With {.TableName = Entities.SessionBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.SessionBuilder
                                                          Return s.CreateTable(x, False)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' STATION SESSION
            tb = New TableMaker With {.TableName = Entities.StationSessionBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.StationSessionBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' SESSION TRAIT
            Entities.SessionTraitBuilder.Get.PrimaryTableName = Entities.SessionBuilder.TableName
            Entities.SessionTraitBuilder.Get.PrimaryTableKeyName = NameOf(Entities.SessionNub.AutoId)
            Entities.SessionTraitBuilder.Get.SecondaryTableName = Entities.SessionTraitTypeBuilder.TableName
            Entities.SessionTraitBuilder.Get.SecondaryTableKeyName = NameOf(Entities.SessionTraitTypeNub.Id)
            tb = New TableMaker With {.TableName = Entities.SessionTraitBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.SessionTraitBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PART
            tb = New TableMaker With {.TableName = Entities.PartBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.PartBuilder
                                                          Return s.CreateTable(x, True)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PART ELEMENT EQUATION
            tb = New TableMaker With {.TableName = Entities.PartElementEquationBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.PartElementEquationBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PART NAMING TYPE
            tb = New TableMaker With {.TableName = Entities.PartNamingTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.PartNamingTypeBuilder
                                                          Return s.CreateTable(x)
                                                      End Function,
                                      .InsertAction = Function(x)
                                                          Return Entities.PartNamingTypeBuilder.Get.InsertIgnoreDefaultRecords(x)
                                                      End Function,
                                      .FetchAllAction = AddressOf Entities.PartNamingTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PART NAMING
            Entities.PartNamingBuilder.Get.PrimaryTableName = Entities.PartBuilder.TableName
            Entities.PartNamingBuilder.Get.PrimaryTableKeyName = NameOf(Entities.PartNub.AutoId)
            Entities.PartNamingBuilder.Get.SecondaryTableName = Entities.PartNamingTypeBuilder.TableName
            Entities.PartNamingBuilder.Get.SecondaryTableKeyName = NameOf(Entities.PartNamingTypeNub.Id)
            tb = New TableMaker With {.TableName = Entities.PartNamingBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.PartNamingBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PRODUCT 
            tb = New TableMaker With {.TableName = Entities.ProductBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.ProductBuilder.Get.CreateTableUniqueLabel}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PRODUCT TEXT TYPE
            tb = New TableMaker With {.TableName = Entities.ProductTextTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.ProductTextTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.ProductTextTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.ProductTextTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PRODUCT TEXT 
            tb = New TableMaker With {.TableName = Entities.ProductTextBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.ProductTextBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PRODUCT TRAIT
            Entities.ProductTraitBuilder.Get.PrimaryTableName = Entities.ProductBuilder.TableName
            Entities.ProductTraitBuilder.Get.PrimaryTableKeyName = NameOf(Entities.ProductNub.AutoId)
            Entities.ProductTraitBuilder.Get.SecondaryTableName = Entities.ProductTraitTypeBuilder.TableName
            Entities.ProductTraitBuilder.Get.SecondaryTableKeyName = NameOf(Entities.ProductTraitTypeNub.Id)
            tb = New TableMaker With {.TableName = Entities.ProductTraitBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.ProductTraitBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PRODUCT PART
            Entities.ProductPartBuilder.Get.PrimaryTableName = Entities.ProductBuilder.TableName
            Entities.ProductPartBuilder.Get.PrimaryTableKeyName = NameOf(Entities.ProductNub.AutoId)
            Entities.ProductPartBuilder.Get.SecondaryTableName = Entities.PartBuilder.TableName
            Entities.ProductPartBuilder.Get.SecondaryTableKeyName = NameOf(Entities.PartNub.AutoId)
            tb = New TableMaker With {.TableName = Entities.ProductPartBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.ProductPartBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PRODUCT SORT
            Entities.ProductSortBuilder.Get.ForeignTableName = Entities.ProductBuilder.TableName
            Entities.ProductSortBuilder.Get.ForeignTableKeyName = NameOf(Entities.ProductNub.AutoId)
            tb = New TableMaker With {.TableName = Entities.ProductSortBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.ProductSortBuilder
                                                          Return s.CreateTable(x, Entities.UniqueIndexOptions.ForeignIdLabel)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PRODUCT SORT TRAIT
            Entities.ProductSortTraitBuilder.Get.PrimaryTableName = Entities.ProductSortBuilder.TableName
            Entities.ProductSortTraitBuilder.Get.PrimaryTableKeyName = NameOf(Entities.ProductSortNub.AutoId)
            Entities.ProductSortTraitBuilder.Get.SecondaryTableName = Entities.ProductSortTraitTypeBuilder.TableName
            Entities.ProductSortTraitBuilder.Get.SecondaryTableKeyName = NameOf(Entities.ProductSortTraitTypeNub.Id)
            tb = New TableMaker With {.TableName = Entities.ProductSortTraitBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.ProductSortTraitBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PRODUCT ELEMENT
            Entities.ProductElementBuilder.Get.PrimaryTableName = Entities.ProductBuilder.TableName
            Entities.ProductElementBuilder.Get.PrimaryTableKeyName = NameOf(Entities.ProductNub.AutoId)
            Entities.ProductElementBuilder.Get.SecondaryTableName = Entities.ElementBuilder.TableName
            Entities.ProductElementBuilder.Get.SecondaryTableKeyName = NameOf(Entities.ElementNub.AutoId)
            tb = New TableMaker With {.TableName = Entities.ProductElementBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.ProductElementBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' LOT
            tb = New TableMaker With {.TableName = Entities.LotBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.LotBuilder
                                                          Return s.CreateTable(x, True)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PART LOT
            Entities.PartLotBuilder.Get.PrimaryTableName = Entities.PartBuilder.TableName
            Entities.PartLotBuilder.Get.PrimaryTableKeyName = NameOf(Entities.PartNub.AutoId)
            Entities.PartLotBuilder.Get.SecondaryTableName = Entities.LotBuilder.TableName
            Entities.PartLotBuilder.Get.SecondaryTableKeyName = NameOf(Entities.LotNub.AutoId)
            tb = New TableMaker With {.TableName = Entities.PartLotBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.PartLotBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' LOT TRAIT
            Entities.LotTraitBuilder.Get.PrimaryTableName = Entities.LotBuilder.TableName
            Entities.LotTraitBuilder.Get.PrimaryTableKeyName = NameOf(Entities.LotNub.AutoId)
            Entities.LotTraitBuilder.Get.SecondaryTableName = Entities.LotTraitTypeBuilder.TableName
            Entities.LotTraitBuilder.Get.SecondaryTableKeyName = NameOf(Entities.LotTraitTypeNub.Id)
            tb = New TableMaker With {.TableName = Entities.LotTraitBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.LotTraitBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' LOT OHM METER SETUP
            Entities.LotOhmMeterSetupBuilder.Get.PrimaryTableName = Entities.LotBuilder.TableName
            Entities.LotOhmMeterSetupBuilder.Get.PrimaryTableKeyName = NameOf(Entities.LotNub.AutoId)
            Entities.LotOhmMeterSetupBuilder.Get.SecondaryTableName = Entities.MultimeterBuilder.TableName
            Entities.LotOhmMeterSetupBuilder.Get.SecondaryTableKeyName = NameOf(Entities.MultimeterNub.Id)
            Entities.LotOhmMeterSetupBuilder.Get.ForeignIdTableName = Entities.OhmMeterSetupBuilder.TableName
            Entities.LotOhmMeterSetupBuilder.Get.ForeignIdTableKeyName = NameOf(Entities.OhmMeterSetupNub.AutoId)
            tb = New TableMaker With {.TableName = Entities.LotOhmMeterSetupBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.LotOhmMeterSetupBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' SAMPLE TRAIT
            tb = New TableMaker With {.TableName = Entities.SampleTraitBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.SampleTraitBuilder
                                                          Return s.CreateTable(x)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' LOT METER ELEMENT SAMPLE
            tb = New TableMaker With {.TableName = Entities.LotSampleBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.LotSampleBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' YIELD TRAIT
            tb = New TableMaker With {.TableName = Entities.YieldTraitBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.YieldTraitBuilder
                                                          Return s.CreateTable(x)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' LOT METER YIELD
            tb = New TableMaker With {.TableName = Entities.LotYieldBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.LotYieldBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' LOT SESSION
            Entities.LotSessionBuilder.Get.PrimaryTableName = Entities.LotBuilder.TableName
            Entities.LotSessionBuilder.Get.PrimaryTableKeyName = NameOf(Entities.LotNub.AutoId)
            Entities.LotSessionBuilder.Get.SecondaryTableName = Entities.SessionBuilder.TableName
            Entities.LotSessionBuilder.Get.SecondaryTableKeyName = NameOf(Entities.SessionNub.AutoId)
            tb = New TableMaker With {.TableName = Entities.LotSessionBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.LotSessionBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' NOMINAL TRAIT
            tb = New TableMaker With {.TableName = Entities.NomTraitBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Dim s As New Entities.NomTraitBuilder
                                                          Return s.CreateTable(x)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PART METER ELEMENT NOMINAL
            Entities.PartNomBuilder.Get.PrimaryTableName = Entities.PartBuilder.TableName
            Entities.PartNomBuilder.Get.PrimaryTableKeyName = NameOf(Entities.PartNub.AutoId)
            Entities.PartNomBuilder.Get.SecondaryTableName = Entities.MultimeterBuilder.TableName
            Entities.PartNomBuilder.Get.SecondaryTableKeyName = NameOf(Entities.MultimeterNub.Id)
            Entities.PartNomBuilder.Get.TernaryTableName = Entities.ElementBuilder.TableName
            Entities.PartNomBuilder.Get.TernaryTableKeyName = NameOf(Entities.ElementNub.AutoId)
            Entities.PartNomBuilder.Get.QuaternaryTableName = Entities.NomTraitBuilder.TableName
            Entities.PartNomBuilder.Get.QuaternaryTableKeyName = NameOf(Entities.NomTraitNub.AutoId)
            tb = New TableMaker With {.TableName = Entities.PartNomBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.PartNomBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PRODUCT BINNING
            tb = New TableMaker With {.TableName = Entities.ProductBinningBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.ProductBinningBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' NUT
            tb = New TableMaker With {.TableName = Entities.NutBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.NutBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' UUT TYPE
            tb = New TableMaker With {.TableName = Entities.UutTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.UutTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.UutTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.UutTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' UUT TRAIT TYPE
            tb = New TableMaker With {.TableName = Entities.UutTraitTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.UutTraitTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.UutTraitTypeBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.UutTraitTypeEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' UUT
            ' the uut has a non-unique number. the UUT number is unique for the lot, which is enforced in code.
            Entities.UutBuilder.Get.ForeignTableName = Entities.UutTypeBuilder.TableName
            Entities.UutBuilder.Get.ForeignTableKeyName = NameOf(Entities.UutTypeNub.Id)
            tb = New TableMaker With {.TableName = Entities.UutBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x As System.Data.IDbConnection)
                                                          Dim s As New Entities.UutBuilder
                                                          Return s.CreateTable(x, Entities.UniqueIndexOptions.None)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' UUT TRAIT
            tb = New TableMaker With {.TableName = Entities.UutTraitBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.UutTraitBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' UUT NUT
            Entities.UutNutBuilder.Get.PrimaryTableName = Entities.UutBuilder.TableName '"Dut"
            Entities.UutNutBuilder.Get.PrimaryTableKeyName = NameOf(Entities.UutNub.AutoId) ' "DutAutoId"
            Entities.UutNutBuilder.Get.SecondaryTableName = Entities.NutBuilder.TableName
            Entities.UutNutBuilder.Get.SecondaryTableKeyName = NameOf(Entities.NutNub.AutoId)
            tb = New TableMaker With {.TableName = Entities.UutNutBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.UutNutBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' NUT NOMINAL READING 
            Entities.NutNomReadingBuilder.Get.PrimaryTableName = Entities.NutBuilder.TableName
            Entities.NutNomReadingBuilder.Get.PrimaryTableKeyName = NameOf(Entities.NutNub.AutoId)
            Entities.NutNomReadingBuilder.Get.SecondaryTableName = Entities.NomReadingTypeBuilder.TableName
            Entities.NutNomReadingBuilder.Get.SecondaryTableKeyName = NameOf(Entities.NomReadingTypeNub.Id)
            tb = New TableMaker With {.TableName = Entities.NutNomReadingBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.NutNomReadingBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' NUT READING BIN
            Entities.NutReadingBinBuilder.Get.PrimaryTableName = Entities.NutBuilder.TableName
            Entities.NutReadingBinBuilder.Get.PrimaryTableKeyName = NameOf(Entities.NutNub.AutoId)
            Entities.NutReadingBinBuilder.Get.SecondaryTableName = Entities.ReadingBinBuilder.TableName
            Entities.NutReadingBinBuilder.Get.SecondaryTableKeyName = NameOf(Entities.ReadingBinNub.Id)
            tb = New TableMaker With {.TableName = Entities.NutReadingBinBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.NutReadingBinBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' UUT PRODUCT SORT
            Entities.UutProductSortBuilder.Get.PrimaryTableName = Entities.UutBuilder.TableName
            Entities.UutProductSortBuilder.Get.PrimaryTableKeyName = NameOf(Entities.UutNub.AutoId)
            Entities.UutProductSortBuilder.Get.SecondaryTableName = Entities.ProductSortBuilder.TableName
            Entities.UutProductSortBuilder.Get.SecondaryTableKeyName = NameOf(Entities.ProductSortNub.AutoId)
            tb = New TableMaker With {.TableName = Entities.UutProductSortBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.UutProductSortBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' LOT UUT
            Entities.LotUutBuilder.Get.PrimaryTableName = Entities.LotBuilder.TableName
            Entities.LotUutBuilder.Get.PrimaryTableKeyName = NameOf(Entities.LotNub.AutoId)
            Entities.LotUutBuilder.Get.SecondaryTableName = Entities.UutBuilder.TableName
            Entities.LotUutBuilder.Get.SecondaryTableKeyName = NameOf(Entities.UutNub.AutoId)
            tb = New TableMaker With {.TableName = Entities.LotUutBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.LotUutBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' REVISION
            TestSuite.CurrentDatabaseVersion = TestSuiteSettings.Get.DatabaseRevision
            tb = New TableMaker With {.TableName = Entities.RevisionBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.RevisionBuilder.Get.CreateTableUniqueLabel,
                                      .PostCreateAction = AddressOf Entities.RevisionBuilder.UpsertRevision,
                                      .PostCreateValue = TestSuite.CurrentDatabaseVersion,
                                      .ExistsUpdateAction = AddressOf Entities.RevisionBuilder.UpsertRevision,
                                      .ExistsUpdateValue = TestSuite.CurrentDatabaseVersion}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' VIEW MAKER
            Dim vm As New ViewMaker

            ' COMPUTER VIEW
            vm = New ViewMaker With {.ViewName = Entities.ComputerBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ComputerBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' COMPUTER STATION VIEW
            vm = New ViewMaker With {.ViewName = Entities.ComputerStationBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ComputerStationBuilder
                                                         Return s.CreateView(x, True)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' ELEMENT VIEW
            vm = New ViewMaker With {.ViewName = Entities.ElementBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ElementBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' ELEMENT NOM TYPE VIEW
            vm = New ViewMaker With {.ViewName = Entities.ElementNomTypeBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ElementNomTypeBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' LOT VIEW
            vm = New ViewMaker With {.ViewName = Entities.LotBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.LotBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' LOT OHM METER SETUP VIEW
            vm = New ViewMaker With {.ViewName = Entities.LotOhmMeterSetupBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.LotOhmMeterSetupBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' LOT SAMPLE VIEW
            vm = New ViewMaker With {.ViewName = Entities.LotSampleBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.LotSampleBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' LOT SESSION VIEW
            vm = New ViewMaker With {.ViewName = Entities.LotSessionBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.LotSessionBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' LOT TRAIT VIEW
            vm = New ViewMaker With {.ViewName = Entities.LotTraitBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.LotTraitBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' LOT UUT VIEW
            vm = New ViewMaker With {.ViewName = Entities.LotUutBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.LotUutBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' LOT YIELD VIEW
            vm = New ViewMaker With {.ViewName = Entities.LotYieldBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.LotYieldBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' MULTIMETER VIEW
            vm = New ViewMaker With {.ViewName = Entities.MultimeterBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.MultimeterBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' NOM TRAIT VIEW
            vm = New ViewMaker With {.ViewName = Entities.NomTraitBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.NomTraitBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' NUT VIEW
            vm = New ViewMaker With {.ViewName = Entities.NutBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.NutBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' NUT NOM READING VIEW
            vm = New ViewMaker With {.ViewName = Entities.NutNomReadingBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.NutNomReadingBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' NUT READING BIN VIEW
            vm = New ViewMaker With {.ViewName = Entities.NutReadingBinBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.NutReadingBinBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PART VIEW
            vm = New ViewMaker With {.ViewName = Entities.PartBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.PartBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PART ELEMENT EQUATION VIEW
            vm = New ViewMaker With {.ViewName = Entities.PartElementEquationBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.PartElementEquationBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PART LOT VIEW
            vm = New ViewMaker With {.ViewName = Entities.PartLotBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.PartLotBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PART NAMING VIEW
            vm = New ViewMaker With {.ViewName = Entities.PartNamingBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.PartNamingBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PART NOM VIEW
            vm = New ViewMaker With {.ViewName = Entities.PartNomBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.PartNomBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PRODUCT VIEW
            vm = New ViewMaker With {.ViewName = Entities.ProductBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ProductBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PRODUCT BINNING VIEW
            vm = New ViewMaker With {.ViewName = Entities.ProductBinningBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ProductBinningBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PRODUCT ELEMENT VIEW
            vm = New ViewMaker With {.ViewName = Entities.ProductElementBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ProductElementBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PRODUCT PART VIEW
            vm = New ViewMaker With {.ViewName = Entities.ProductPartBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ProductPartBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PRODUCT SORT VIEW
            vm = New ViewMaker With {.ViewName = Entities.ProductSortBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ProductSortBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PRODUCT SORT TRAIT VIEW
            vm = New ViewMaker With {.ViewName = Entities.ProductSortTraitBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ProductSortTraitBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PRODUCT TEXT VIEW
            vm = New ViewMaker With {.ViewName = Entities.ProductTextBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ProductTextBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' PRODUCT TRAIT VIEW
            vm = New ViewMaker With {.ViewName = Entities.ProductTraitBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.ProductTraitBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' REVISION VIEW
            vm = New ViewMaker With {.ViewName = Entities.RevisionBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.RevisionBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' SESSION VIEW
            vm = New ViewMaker With {.ViewName = Entities.SessionBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.SessionBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' SESSION TRAIT VIEW
            vm = New ViewMaker With {.ViewName = Entities.SessionTraitBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.SessionTraitBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' STATION VIEW
            vm = New ViewMaker With {.ViewName = Entities.StationBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.StationBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' STATION SESSION VIEW
            vm = New ViewMaker With {.ViewName = Entities.StationSessionBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.StationSessionBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' UUT VIEW
            vm = New ViewMaker With {.ViewName = Entities.UutBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.UutBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' UUT NUT VIEW
            vm = New ViewMaker With {.ViewName = Entities.UutNutBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.UutNutBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' UUT PRODUCT SORT VIEW
            vm = New ViewMaker With {.ViewName = Entities.UutProductSortBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.UutProductSortBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' UUT TRAIT VIEW
            vm = New ViewMaker With {.ViewName = Entities.UutTraitBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.UutTraitBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

            ' YIELD TRAIT VIEW
            vm = New ViewMaker With {.ViewName = Entities.YieldTraitBuilder.Get.ViewName,
                                     .CreateAction = Function(x As System.Data.IDbConnection)
                                                         Dim s As New Entities.YieldTraitBuilder
                                                         Return s.CreateView(x, False)
                                                     End Function}
            schemaBuilder.EnumeratedViews.Add(vm.ViewName, vm)

        End Sub

        ''' <summary> Gets or sets the built database version. </summary>
        ''' <value> The built database version. </value>
        Public Shared Property CurrentDatabaseVersion As String

        ''' <summary> Creates data base. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        Public Shared Sub CreateDatabase(ByVal masterConnectionString As String)
            If TestSuiteSettings.Get.CreateDatabase Then
                If System.IO.File.Exists(TestSuite.Provider.FileName) Then System.IO.File.Delete(TestSuite.Provider.FileName)
                TestSuite.CreateDatabaseThis(masterConnectionString)
            ElseIf Not TestSuite.Provider.DatabaseExists Then
                TestSuite.CreateDatabaseThis(masterConnectionString)
            End If
        End Sub

        ''' <summary> Creates database. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Private Shared Function CreateDatabaseThis(ByVal masterConnectionString As String) As Boolean
            Dim result As Boolean = TestSuite.Provider.CreateDatabase(masterConnectionString, TestSuite.Provider.DatabaseName)
            Dim providerFileLabel As String = If(TestSuite.Provider.ProviderType = ProviderType.SQLite,
                                                    TestSuiteSettings.Get.SQLiteFileLabel,
                                                    TestSuiteSettings.Get.SqlServerFileLabel)
            Dim count As Integer
            Dim schemaFileName As String
            If result Then
                schemaFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                        String.Format(TestSuiteSettings.Get.SchemaFileNameFormat, providerFileLabel))
                count = TestSuite.Provider.ExecuteScript(schemaFileName)
                result = count > 0
            End If
            If result Then
                schemaFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                        String.Format(TestSuiteSettings.Get.SchemaDataFileNameFormat, providerFileLabel))
                count = TestSuite.Provider.ExecuteScript(schemaFileName)
                result = count > 0
            End If
            Return result
        End Function

        ''' <summary> Create or clear database and add all tables. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        Public Shared Sub BuildDatabase(ByVal masterConnectionString As String)
            TestSuite.CreateDatabase(masterConnectionString)
            If TestSuiteSettings.Get.ClearDatabase Then TestSuite.SchemaBuilder.DropAllTables(TestSuiteSettings.Get.IgnoreDropTableErrors)
            TestSuite.SchemaBuilder.BuildSchema()
        End Sub

        ''' <summary> Initializes the lookup entities. </summary>
        ''' <remarks> David, 7/11/2020. </remarks>
        ''' <param name="provider"> The provider. </param>
        Public Shared Sub InitializeLookupEntities(ByVal provider As ProviderBase)
            If provider Is Nothing Then Return
            TestSuite._SchemaBuilder = New SchemaBuilder With {.Provider = provider}
            TestSuite.EnumerateSchemaObjects(provider.ProviderType)
            TestSuite.SchemaBuilder.FetchAll()
        End Sub

        ''' <summary> Initializes the lookup entities. </summary>
        ''' <remarks> David, 7/11/2020. </remarks>
        Public Sub InitializeLookupEntities()
            TestSuite.InitializeLookupEntities(Me.GetProvider)
        End Sub

    End Class

End Namespace
