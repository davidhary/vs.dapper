Imports isr.Dapper.Entity
Namespace Global.isr.Dapper.Entities.Tests.Ohmni.Probe

    ''' <summary> An Ohmni Probe Test Suite Settings for Dapper Entities. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 8/26/2019 </para></remarks>
    <Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
     Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
     Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
    Public Class TestSuiteSettings
        Inherits isr.Core.ApplicationSettingsBase

        #Region " SINGLETON "

        ''' <summary>
        ''' Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        ''' class to its default state.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Sub New()
            MyBase.New
        End Sub

        ''' <summary> Opens the settings editor. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Public Shared Sub OpenSettingsEditor()
            isr.Core.WindowsForms.EditConfiguration($"{GetType(TestSuiteSettings)} Editor", TestSuiteSettings.Get)
        End Sub

        ''' <summary>
        ''' Gets the locking object to enforce thread safety when creating the singleton instance.
        ''' </summary>
        ''' <value> The sync locker. </value>
        Private Shared Property _SyncLocker As New Object

        ''' <summary> Gets the instance. </summary>
        ''' <value> The instance. </value>
        Private Shared Property _Instance As TestSuiteSettings

        ''' <summary> Instantiates the class. </summary>
        ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
        ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        ''' <returns> A new or existing instance of the class. </returns>
        Public Shared Function [Get]() As TestSuiteSettings
            If _Instance Is Nothing Then
                SyncLock _SyncLocker
                    _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New TestSuiteSettings()), TestSuiteSettings)
                End SyncLock
            End If
            Return _Instance
        End Function

        ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
        ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        Public Shared ReadOnly Property Instantiated() As Boolean
            Get
                SyncLock _SyncLocker
                    Return _Instance IsNot Nothing
                End SyncLock
            End Get
        End Property

        #End Region

        #Region " CONFIGURATION SETTINGS "

        ''' <summary> Returns true if test settings exist. </summary>
        ''' <value> <c>True</c> if testing settings exit. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
        Public Property Exists As Boolean
            Get
                Return Me.AppSettingGetter(False)
            End Get
            Set(value As Boolean)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Returns true to output test messages at the verbose level. </summary>
        ''' <value> The verbose messaging level. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
        Public Property Verbose As Boolean
            Get
                Return Me.AppSettingGetter(False)
            End Get
            Set(value As Boolean)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Returns true to enable this device. </summary>
        ''' <value> The device enable option. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
        Public Property Enabled As Boolean
            Get
                Return Me.AppSettingGetter(False)
            End Get
            Set(value As Boolean)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets all. </summary>
        ''' <value> all. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
        Public Property All As Boolean
            Get
                Return Me.AppSettingGetter(False)
            End Get
            Set(value As Boolean)
                Me.AppSettingSetter(value)
            End Set
        End Property

        #End Region

        #Region " SQLite SETTINGS "

        ''' <summary> Gets or sets the connection string. </summary>
        ''' <value> The connection string. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(),
            Global.System.Configuration.DefaultSettingValueAttribute("Data Source=C:\Users\Public\Documents\Ohmni\Probe\TestDb\OhmniProbe.sqlite;Mode=ReadWriteCreate;datetimeformat=CurrentCulture")>
        Public Property SQLiteConnectionString As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        #End Region

        #Region " SqlServer SETTINGS "

        ''' <summary> Gets or sets the connection string. </summary>
        ''' <value> The connection string. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(),
            Global.System.Configuration.DefaultSettingValueAttribute("Data Source=localhost\sqlx2019;Initial Catalog=tempdb;Integrated Security=True")>
        Public Property SqlServerConnectionString As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        #End Region

        #Region " BUILD SETTINGS "

        ''' <summary>
        ''' Gets or sets the delete option determining which, if any, tables types are deleted.
        ''' </summary>
        ''' <value> The delete option. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0")>
        Public Property DeleteOption As TableCategory
            Get
                Return CType(Me.AppSettingGetter(0), TableCategory)
            End Get
            Set(value As TableCategory)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the drop tables option. </summary>
        ''' <value> The drop tables option. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0")>
        Public Property DropTablesOption As TableCategory
            Get
                Return CType(Me.AppSettingGetter(0), TableCategory)
            End Get
            Set(value As TableCategory)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary>
        ''' Gets or sets the clear database sentinel determining if the database tables are dropped
        ''' before new tables are created.
        ''' </summary>
        ''' <value> The clear database sentinel. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
        Public Property ClearDatabase As Boolean
            Get
                Return Me.AppSettingGetter(False)
            End Get
            Set(value As Boolean)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the add tables. </summary>
        ''' <value> The add tables. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
        Public Property AddTables As Boolean
            Get
                Return Me.AppSettingGetter(False)
            End Get
            Set(value As Boolean)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the ignore drop table errors. </summary>
        ''' <value> The ignore drop table errors. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
        Public Property IgnoreDropTableErrors As Boolean
            Get
                Return Me.AppSettingGetter(False)
            End Get
            Set(value As Boolean)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the create database sentinel. </summary>
        ''' <value> The create database. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
        Public Property CreateDatabase As Boolean
            Get
                Return Me.AppSettingGetter(False)
            End Get
            Set(value As Boolean)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the pathname of the schema folder. </summary>
        ''' <value> The pathname of the schema folder. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("C:\Users\Public\Documents\Ohmni\Probe")>
        Public Property SchemaFolderName As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the SQL server file label. </summary>
        ''' <value> The SQL server file label. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("SqlServer")>
        Public Property SqlServerFileLabel As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the sq lite file label. </summary>
        ''' <value> The sq lite file label. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("SQLite")>
        Public Property SQLiteFileLabel As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the guard band lookup file name format. </summary>
        ''' <value> The guard band lookup file name format. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(),
            Global.System.Configuration.DefaultSettingValueAttribute("AddGuardBandLookupValues{0}.sql")>
        Public Property GuardBandLookupFileNameFormat As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the add ohm meter setting lookup file name format. </summary>
        ''' <value> The add ohm meter setting lookup file name format. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(),
            Global.System.Configuration.DefaultSettingValueAttribute("AddOhmMeterSettingLookupValues{0}.sql")>
        Public Property AddOhmMeterSettingLookupFileNameFormat As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the schema file name format. </summary>
        ''' <value> The schema file name format. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("AddSchema{0}.sql")>
        Public Property SchemaFileNameFormat As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the schema data file name format. </summary>
        ''' <value> The schema data file name format. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("AddData{0}.sql")>
        Public Property SchemaDataFileNameFormat As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the clear data file name format. </summary>
        ''' <value> The clear data file name format. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("ClearData{0}.sql")>
        Public Property ClearDataFileNameFormat As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the clear data upon initialize. </summary>
        ''' <value> The clear data upon initialize. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
        Public Property ClearDataUponInitialize As Boolean
            Get
                Return Me.AppSettingGetter(False)
            End Get
            Set(value As Boolean)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the database revision. </summary>
        ''' <value> The database revision. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("3.0.7472")>
        Public Property DatabaseRevision As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the pathname of the files folder. </summary>
        ''' <value> The pathname of the files folder. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(),
            Global.System.Configuration.DefaultSettingValueAttribute("C:\Users\Public\Documents\Ohmni\Probe\Files")>
        Public Property FilesFolderName As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        ''' <summary> Gets or sets the filename of the import file. </summary>
        ''' <value> The filename of the import file. </value>
        <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Import.csv")>
        Public Property ImportFileName As String
            Get
                Return Me.AppSettingGetter(String.Empty)
            End Get
            Set(value As String)
                Me.AppSettingSetter(value)
            End Set
        End Property

        #End Region

    End Class
End Namespace

