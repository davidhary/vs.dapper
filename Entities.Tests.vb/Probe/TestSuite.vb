Imports isr.Dapper.Entity
Namespace Global.isr.Dapper.Entities.Tests.Ohmni.Probe

    ''' <summary> An Ohmni Probe Test Suite for Dapper Entities. </summary>
    ''' <remarks>
    ''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 8/26/2019 </para>
    ''' </remarks>
    <TestClass()>
    Partial Public MustInherit Class TestSuite

        #Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Base class initialize. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        Public Shared Sub BaseClassInitialize(ByVal testContext As TestContext)
            Try
                _TestInfo = New TestSite
                TestInfo.AddTraceMessagesQueue(TestInfo.TraceMessagesQueueListener)
                TestInfo.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    BaseClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        Public Shared Sub BaseClassCleanup()
            If _TestInfo IsNot Nothing Then _TestInfo.Dispose() : _TestInfo = Nothing
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Assert.IsTrue(TestSuiteSettings.Get.Exists, $"{GetType(TestSuiteSettings)} settings should exist")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo?.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets or sets the test context which provides information about and functionality for the
        ''' current test run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext

        ''' <summary> Gets or sets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite

        #End Region

        #Region " PROVIDER "

        ''' <summary> Gets the provider. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The provider. </returns>
        Public MustOverride Function GetProvider() As ProviderBase

        ''' <summary> Gets the connection. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <returns> The connection. </returns>
        Private Function GetConnection() As System.Data.IDbConnection
            Return Me.GetProvider.GetConnection()
        End Function

#If False Then

        ''' <summary> (Unit Test Method) queries if a given connection exists. </summary>
        <TestMethod>
        Public Sub ConnectionExists()
            Auditor.AssertConnectionExists(Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests tables exist. </summary>
        <TestMethod>
        Public Overridable Sub TablesExistTest()
            Auditor.AssertTablesExist(Me.GetProvider, TestSuite.SchemaBuilder.TableNames)
        End Sub

#End If
        #End Region

    End Class
End Namespace

