Imports System.Runtime.CompilerServices
Imports Dapper
Imports isr.Dapper.Entity
#Disable Warning IDE1006 ' Naming Styles
Namespace Global.isr.Dapper.Entities.Tests.Ohmni.Probe
    #Enable Warning IDE1006 ' Naming Styles

    Partial Public Class TestSuite

        ''' <summary> Initializes this object. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="provider"> The provider. </param>
        Public Shared Sub Initialize(ByVal provider As ProviderBase)
            Console.WriteLine($".NET {Environment.Version}")
            Console.WriteLine($"Dapper {GetType(Global.Dapper.SqlMapper).AssemblyQualifiedName}")
            Console.WriteLine($"Connection string: {provider.ConnectionString}")
            TestSuite._Provider = provider
            ' test naked connection
            Assert.IsTrue(TestSuite.Provider.IsConnectionExists(), $"connection {TestSuite.Provider.GetConnection} does not exist")
            ' test connection with event handling
            Using connection As System.Data.IDbConnection = TestSuite.Provider.GetConnection()
                Assert.IsTrue(connection.Exists(), "connection does not exist")
            End Using
            If provider Is Nothing Then
                TestSuite._SchemaBuilder = New SchemaBuilder
            Else
                TestSuite._SchemaBuilder = New SchemaBuilder With {.Provider = provider}
                TestSuite.EnumerateSchemaObjects(provider.ProviderType)
            End If
            ' this prevents testing.
            TestSuite.BuildDatabase(provider.BuildMasterConnectionString(False))
            TestSuite.SchemaBuilder.AssertSchemaObjectsExist()
        End Sub

        ''' <summary> Cleanups this object. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Public Shared Sub Cleanup()
            If TableCategory.None <> TestSuiteSettings.Get.DeleteOption Then
                TestSuite.SchemaBuilder.DeleteAllRecords(TestSuiteSettings.Get.DeleteOption)
            End If
            If TableCategory.None <> TestSuiteSettings.Get.DropTablesOption Then
                TestSuite.SchemaBuilder.DropAllTables(TestSuiteSettings.Get.DropTablesOption, TestSuiteSettings.Get.IgnoreDropTableErrors)
            End If
        End Sub

        ''' <summary> Gets or sets the provider. </summary>
        ''' <value> The provider. </value>
        Public Shared ReadOnly Property Provider As ProviderBase

        ''' <summary> Gets or sets the schema builder. </summary>
        ''' <value> The schema builder. </value>
        Public Shared ReadOnly Property SchemaBuilder As SchemaBuilder

        ''' <summary> Enumerate schema objects. </summary>
        ''' <remarks> David, 2020-07-04. </remarks>
        ''' <param name="providerType"> Type of the provider. </param>
        Public Shared Sub EnumerateSchemaObjects(ByVal providerType As ProviderType)
            Try
                TestSuite.EnumerateSchemaObjectsThis(providerType)
            Catch
                TestSuite.SchemaBuilder.EnumeratedTables.Clear()
                Throw
            End Try
        End Sub

        ''' <summary> Enumerate schema objects this. </summary>
        ''' <remarks> David, 2020-07-04. </remarks>
        ''' <param name="providerType"> Type of the provider. </param>
        Private Shared Sub EnumerateSchemaObjectsThis(ByVal providerType As ProviderType)

            Dim providerFileLabel As String = If(providerType = ProviderType.SQLite, TestSuiteSettings.Get.SQLiteFileLabel, TestSuiteSettings.Get.SqlServerFileLabel)

            Dim schemaBuilder As SchemaBuilder = TestSuite.SchemaBuilder
            schemaBuilder.EnumeratedTables.Clear()
            Dim tb As New TableMaker

            ' BUCKET BIN 
            tb = New TableMaker With {.TableName = Entities.BucketBinBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf Entities.BucketBinBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf Entities.BucketBinBuilder.Get.InsertIgnoreDefaultRecords,
                                      .FetchAllAction = AddressOf Entities.BucketBinEntity.FetchAll}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' READING BIN
            tb = New TableMaker With {.TableName = ReadingBinBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf ReadingBinBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf ReadingBinBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' NOMINAL READING TYPE
            tb = New TableMaker With {.TableName = NomReadingTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf NomReadingTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf NomReadingTypeBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' MULTIMETER MODEL
            tb = New TableMaker With {.TableName = MultimeterModelBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf MultimeterModelBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf MultimeterModelBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' TEST TYPE
            tb = New TableMaker With {.TableName = TestTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf TestTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf TestTypeBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' TOLERANCE
            tb = New TableMaker With {.TableName = ToleranceBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf ToleranceBuilder.CreateTable,
                                      .InsertAction = AddressOf ToleranceBuilder.InsertValues}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            MultimeterBuilder.Get.ForeignTableName = MultimeterModelBuilder.TableName
            MultimeterBuilder.Get.ForeignTableKeyName = NameOf(MultimeterModelNub.Id)
            tb = New TableMaker With {.TableName = MultimeterBuilder.Get.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = Function(x)
                                                          Return MultimeterBuilder.Get.CreateTable(x, Entities.UniqueIndexOptions.None)
                                                      End Function,
                                      .InsertAction = AddressOf MultimeterBuilder.Get.InsertValues}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            MeterGuardBandLookupBuilder.MultimeterTableName = MultimeterModelBuilder.TableName
            MeterGuardBandLookupBuilder.MultimeterTableKeyName = NameOf(MultimeterModelNub.Id)
            MeterGuardBandLookupBuilder.NomTypeTableName = NomTypeBuilder.TableName
            MeterGuardBandLookupBuilder.NomTypeTableKeyName = NameOf(NomTypeNub.Id)
            tb = New TableMaker With {.TableName = MeterGuardBandLookupBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf MeterGuardBandLookupBuilder.CreateTable,
                                      .InsertFileValuesAction = AddressOf MeterGuardBandLookupBuilder.InsertFileValues,
                                      .ValuesFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                                              String.Format(TestSuiteSettings.Get.GuardBandLookupFileNameFormat, providerFileLabel))}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = OhmMeterSettingBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf OhmMeterSettingBuilder.CreateTable,
                                      .InsertAction = AddressOf OhmMeterSettingBuilder.InsertValues}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            OhmMeterSettingLookupBuilder.MultimeterTableName = MultimeterModelBuilder.TableName
            OhmMeterSettingLookupBuilder.MultimeterTableKeyName = NameOf(MultimeterModelNub.Id)
            tb = New TableMaker With {.TableName = OhmMeterSettingLookupBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf OhmMeterSettingLookupBuilder.CreateTable,
                                      .InsertFileValuesAction = AddressOf OhmMeterSettingLookupBuilder.InsertFileValues,
                                      .ValuesFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                                              String.Format(TestSuiteSettings.Get.AddOhmMeterSettingLookupFileNameFormat, providerFileLabel))}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = OhmMeterSetupBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf OhmMeterSetupBuilder.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' COMPUTER + PLATFORM + STATION + SESSION
            tb = New TableMaker With {.TableName = ComputerBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Return ComputerBuilder.Get.CreateTable(x, True)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = PlatformBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Return PlatformBuilder.Get.CreateTable(x, True)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' if the station is part of a platform, make the station label non-unique to allow same name for different platforms.
            tb = New TableMaker With {.TableName = StationBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Return StationBuilder.Get.CreateTable(x, True)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = ComputerStationBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf ComputerStationBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = PlatformStationBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf PlatformStationBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = SessionBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf SessionBuilder.Get.CreateTableNonUniqueLabel}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = StationSessionBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf StationSessionBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = LotOhmMeterSetupBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf LotOhmMeterSetupBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PART 
            tb = New TableMaker With {.TableName = PartBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf PartBuilder.Get.CreateTableNonUniqueLabel}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = PartNamingTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = Function(x)
                                                          Dim s As New PartNamingTypeBuilder
                                                          Return s.CreateTable(x)
                                                      End Function,
                                      .InsertAction = Function(x)
                                                          Return PartNamingTypeBuilder.Get.InsertIgnoreDefaultRecords(x)
                                                      End Function}

            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            Entities.PartNamingBuilder.Get.PrimaryTableName = Entities.PartBuilder.TableName
            Entities.PartNamingBuilder.Get.PrimaryTableKeyName = NameOf(Entities.PartNub.AutoId)
            Entities.PartNamingBuilder.Get.SecondaryTableName = Entities.PartNamingTypeBuilder.TableName
            Entities.PartNamingBuilder.Get.SecondaryTableKeyName = NameOf(Entities.PartNamingTypeNub.Id)
            tb = New TableMaker With {.TableName = Entities.PartNamingBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf Entities.PartNamingBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' the lot uses a unique lot numbers assuming lot numbers are unique across all parts.
            tb = New TableMaker With {.TableName = LotBuilder.TableName, .Category = TableCategory.None,
                                     .CreateAction = AddressOf LotBuilder.Get.CreateTableUniqueLabel}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = PartLotBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf PartLotBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = LotSessionBuilder.TableName, .Category = TableCategory.None,
                                     .CreateAction = AddressOf LotSessionBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' SUBSTRATE
            tb = New TableMaker With {.TableName = SubstrateTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf SubstrateTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf SubstrateTypeBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = SubstrateBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Return SubstrateBuilder.Get.CreateTable(x, Entities.UniqueIndexOptions.None)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' STRUCTURES
            tb = New TableMaker With {.TableName = StructureTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf StructureTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf StructureTypeBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = StructureBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Return StructureBuilder.Get.CreateTable(x, Entities.UniqueIndexOptions.None)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = SessionStructureBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf SessionStructureBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' REVISION
            TestSuite.CurrentDatabaseVersion = (New isr.Core.MyAssemblyInfo(GetType(TestSuite).Assembly)).Version.ToString(3)
            tb = New TableMaker With {.TableName = RevisionBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf RevisionBuilder.Get.CreateTableUniqueLabel,
                                      .PostCreateAction = AddressOf RevisionBuilder.UpsertRevision,
                                      .PostCreateValue = TestSuite.CurrentDatabaseVersion,
                                      .ExistsUpdateAction = AddressOf Entities.RevisionBuilder.UpsertRevision,
                                      .ExistsUpdateValue = TestSuite.CurrentDatabaseVersion}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

        End Sub

        ''' <summary> Gets or sets the built database version. </summary>
        ''' <value> The built database version. </value>
        Public Shared Property CurrentDatabaseVersion As String

        ''' <summary> Creates data base. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        Public Shared Sub CreateDatabase(ByVal masterConnectionString As String)
            If TestSuiteSettings.Get.CreateDatabase Then
                If System.IO.File.Exists(TestSuite.Provider.FileName) Then System.IO.File.Delete(TestSuite.Provider.FileName)
                TestSuite.CreateDatabaseThis(masterConnectionString)
            ElseIf Not TestSuite.Provider.DatabaseExists Then
                TestSuite.CreateDatabaseThis(masterConnectionString)
            End If
        End Sub

        ''' <summary> Creates database. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Private Shared Function CreateDatabaseThis(ByVal masterConnectionString As String) As Boolean
            Dim result As Boolean = TestSuite.Provider.CreateDatabase(masterConnectionString, TestSuite.Provider.DatabaseName)
            Dim providerFileLabel As String = If(TestSuite.Provider.ProviderType = ProviderType.SQLite,
                                                    TestSuiteSettings.Get.SQLiteFileLabel,
                                                    TestSuiteSettings.Get.SqlServerFileLabel)
            Dim count As Integer
            Dim schemaFileName As String
            If result Then
                schemaFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                        String.Format(TestSuiteSettings.Get.SchemaFileNameFormat, providerFileLabel))
                count = TestSuite.Provider.ExecuteScript(schemaFileName)
                result = count > 0
            End If
            If result Then
                schemaFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                        String.Format(TestSuiteSettings.Get.SchemaDataFileNameFormat, providerFileLabel))
                count = TestSuite.Provider.ExecuteScript(schemaFileName)
                result = count > 0
            End If
            Return result
        End Function

        ''' <summary> Create or clear database and add all tables. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        Public Shared Sub BuildDatabase(ByVal masterConnectionString As String)
            TestSuite.CreateDatabase(masterConnectionString)
            If TestSuiteSettings.Get.ClearDatabase Then TestSuite.SchemaBuilder.DropAllTables(TestSuiteSettings.Get.IgnoreDropTableErrors)
            TestSuite.SchemaBuilder.BuildSchema()
        End Sub

        ''' <summary> Initializes the lookup entities. </summary>
        ''' <remarks> David, 7/11/2020. </remarks>
        ''' <param name="provider"> The provider. </param>
        Public Shared Sub InitializeLookupEntities(ByVal provider As ProviderBase)
            If provider Is Nothing Then Return
            TestSuite._SchemaBuilder = New SchemaBuilder With {.Provider = provider}
            TestSuite.EnumerateSchemaObjects(provider.ProviderType)
            TestSuite.SchemaBuilder.FetchAll()
        End Sub

        ''' <summary> Initializes the lookup entities. </summary>
        ''' <remarks> David, 7/11/2020. </remarks>
        Public Sub InitializeLookupEntities()
            TestSuite.InitializeLookupEntities(Me.GetProvider)
        End Sub

    End Class

End Namespace
