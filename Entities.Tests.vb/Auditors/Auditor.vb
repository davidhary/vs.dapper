Imports System.IO
Imports System.Security.Policy

Imports Dapper.Contrib.Extensions

Imports isr.Core.Constructs
Imports isr.Core.EnumExtensions
Imports isr.Core.MSTest
Imports isr.Dapper.Entity
Imports isr.Dapper.Entity.ConnectionExtensions

''' <summary> An auditor: Owner of the actual test code. </summary>
''' <remarks> David, 3/26/2020. </remarks>
Partial Public NotInheritable Class Auditor

    #Region " PROVIDER TESTS "

    ''' <summary> Asserts connection exists. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertConnectionExists(ByVal provider As ProviderBase)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Assert.IsTrue(provider.ConnectionExists(), $"connection {connection.ConnectionString} should exist")
        End Using
    End Sub

    ''' <summary> Asserts tables exist. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="tableNames"> List of names of the tables. </param>
    Public Shared Sub AssertTablesExist(ByVal provider As ProviderBase, ByVal tableNames As IEnumerable(Of String))
        Using connection As System.Data.IDbConnection = provider.GetConnection
            For Each name As String In tableNames
                Assert.IsTrue(provider.TableExists(connection, name), $"table {name} should exist")
            Next
        End Using
    End Sub

    ''' <summary> Assert tables exist. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="tableNames"> List of names of the tables. </param>
    Public Shared Sub AssertTablesExist(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal tableNames As IEnumerable(Of String))
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            For Each tableName As String In tableNames
                site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
                Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Next
        End Using
    End Sub

    ''' <summary> Assert table exist. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="site">      The site. </param>
    ''' <param name="provider">  The provider. </param>
    ''' <param name="tableName"> Name of the table. </param>
    Public Shared Sub AssertTableExist(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal tableName As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
        End Using
    End Sub

    #End Region

    #Region " DAPPER FRAMEWORK TESTS "

    ''' <summary> Assert inserted identity key. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertInsertedIdentityKey(ByVal provider As ProviderBase)
        Using connection As System.Data.IDbConnection = provider.GetConnection
            connection.DeleteAll(Of PartNub)()
            Assert.IsNull(connection.Get(Of PartNub)(3))
            Dim part As PartNub = New PartNub With {.Label = "PFC-D1206LF-02-1001-3301-FB"}
            Dim id As Integer = CInt(connection.Insert(part))
            Assert.AreEqual(part.AutoId, id)
            Dim partFetched As PartNub = connection.Get(Of PartNub)(id)
            Assert.AreEqual(part.AutoId, partFetched.AutoId, "Fetched part has incorrect key")
        End Using
    End Sub

    #End Region

    #Region " ELEMENT "

    ''' <summary> Gets the element labels. </summary>
    ''' <value> The element labels. </value>
    Public Shared ReadOnly Property ElementLabels As IEnumerable(Of String)
        Get
            Return New String() {"R1", "R2", "D1", "M1"}
        End Get
    End Property

    ''' <summary> Select element nominal value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="label"> The label. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function SelectElementNominalValue(ByVal label As String) As Double
        Dim r1 As Double = 100
        Dim r2 As Double = 200
        Select Case label
            Case "R1"
                Return r1
            Case "R2"
                Return r2
            Case "D1"
                Return r1 + r2
            Case "M1"
                Return r1 / r2
            Case Else
                Throw New ArgumentException($"Unhandled [{NameOf(ElementEntity)}].[{NameOf(ElementEntity.Label)}] of {label}")
        End Select
    End Function

    ''' <summary> Select element tolerance. </summary>
    ''' <remarks> David, 6/1/2020. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="label"> The label. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function SelectElementTolerance(ByVal label As String) As Double
        Select Case label
            Case "R1"
                Return 0.01
            Case "R2"
                Return 0.01
            Case "D1"
                Return 0.01
            Case "M1"
                Return 0.01
            Case Else
                Throw New ArgumentException($"Unhandled [{NameOf(ElementEntity)}].[{NameOf(ElementEntity.Label)}] of {label}")
        End Select
    End Function

    ''' <summary> Select element equation. </summary>
    ''' <remarks> David, 7/16/2020. </remarks>
    ''' <param name="label">        The label. </param>
    ''' <param name="nominalValue"> The nominal value. </param>
    ''' <param name="tolerance">    The tolerance. </param>
    ''' <returns> A String such as |R1|R2|/|==|0.33333|0.001|. </returns>
    Public Shared Function SelectElementEquation(ByVal label As String, ByVal nominalValue As Double, ByVal tolerance As Double) As String
        Dim equationFormat As String = "|R1|R2|/|==|{0:G5}|{1}|"
        Return If(String.Equals(label, "M1"), String.Format(equationFormat, nominalValue, tolerance), String.Empty)
    End Function

    ''' <summary> Select element current source. </summary>
    ''' <remarks> David, 6/1/2020. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="label"> The label. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function SelectElementCurrentSource(ByVal label As String) As Double
        Select Case label
            Case "R1"
                Return 0.001
            Case "R2"
                Return 0.001
            Case "D1"
                Return 0.001
            Case "M1"
                Return 0.0
            Case Else
                Throw New ArgumentException($"Unhandled [{NameOf(ElementEntity)}].[{NameOf(ElementEntity.Label)}] of {label}")
        End Select
    End Function

    ''' <summary> Select element type. </summary>
    ''' <remarks> David, 6/1/2020. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="label"> The label. </param>
    ''' <returns> An ElementType. </returns>
    Public Shared Function SelectElementType(ByVal label As String) As ElementType
        Select Case label
            Case "R1"
                Return ElementType.Resistor
            Case "R2"
                Return ElementType.Resistor
            Case "D1"
                Return ElementType.CompoundResistor
            Case "M1"
                Return ElementType.Equation
            Case Else
                Throw New ArgumentException($"Unhandled [{NameOf(ElementEntity)}].[{NameOf(ElementEntity.Label)}] of {label}")
        End Select
    End Function

    ''' <summary> Assert adding element Traits. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="part">     The part. </param>
    ''' <param name="element">  The element. </param>
    Public Shared Sub AssertAddingElementNomTraits(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                   ByVal part As PartEntity, ByVal element As ElementEntity)

        Dim nomTraitEntity As NomTraitEntity
        Dim sigmaLevelNomTrait As NomTraitEntity
        Dim toleranceNomTrait As NomTraitEntity
        Dim guardBandFactorNomTrait As NomTraitEntity
        Dim lowerGuardedSpecificationLimitNomTrait As NomTraitEntity
        Dim upperGuardedSpecificationLimitNomTrait As NomTraitEntity
        Dim multimeterId As Integer = 1
        Dim multimeterIdentities As Integer() = New Integer() {multimeterId}
        Dim nomType As NomType = element.NomTypes.First.NomType
        Dim nomTraitSelector As New MeterElementNomTypeSelector(multimeterId, element.AutoId, nomType)
        Dim partElementEquation As PartElementEquationEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tolerance As Double = Auditor.SelectElementTolerance(element.Label)
            Dim nominalValue As Double = Auditor.SelectElementNominalValue(element.Label)
            Dim lowerOverflowLimit As Double = 0.1 * nominalValue
            Dim upperOverflowLimit As Double = 10 * nominalValue
            Dim equation As String = Auditor.SelectElementEquation(element.Label, nominalValue, tolerance)

            If Not String.IsNullOrEmpty(equation) Then
                partElementEquation = New PartElementEquationEntity With {.PartAutoId = part.AutoId, .ElementAutoId = element.AutoId, .Equation = equation}
                partElementEquation.Obtain(connection)
                r = partElementEquation.ValidateStoredEntity($"{NameOf(Entities.PartElementEquationEntity)} with [{NameOf(Entities.PartElementEquationEntity.PartAutoId)},{NameOf(Entities.PartElementEquationEntity.ElementAutoId)},{NameOf(Entities.PartElementEquationEntity.Equation)}] of [{part.AutoId},{element.AutoId},{equation}]")
                Assert.IsTrue(r.Success, r.Details)
                part.FetchElementEquations(connection)
                Assert.AreEqual(equation, part.ElementEquations.Equation(element.AutoId), $"{NameOf(PartUniqueElementEquationEntityCollection)}.{NameOf(PartUniqueElementEquationEntityCollection.Equation)} should match")
            End If

            Dim rnd As New Random(Date.Now.Millisecond)
            Dim sigmaLevel As Integer = 6
            Dim sigmaPerformanceLevel As Double = 6
            Dim guardBandFactor As Double = 0.1 * CInt(10 * (1 + rnd.NextDouble))
            Dim expectedSpecificationRange As RangeR = New RangeR(nominalValue * (1 - tolerance), nominalValue * (1 + tolerance))
            Dim expectedGuardedSpecificationLimit As Double = tolerance * (1 - guardBandFactor / sigmaPerformanceLevel)
            Dim expectedGuardedSpecificationRange As RangeR = New RangeR(nominalValue * (1 - expectedGuardedSpecificationLimit), nominalValue * (1 + expectedGuardedSpecificationLimit))

            Dim expectedTraitCount As Integer = 0
            Dim expectedElementMeterReadingRealCount As Integer = 0

            Dim elementMeterNomTraits As PartNomEntityCollection = part.NomTraits(nomTraitSelector)
            Dim nomTrait As NomTrait = elementMeterNomTraits.NomTrait

            nomTrait.SigmaPerformanceLevel = sigmaPerformanceLevel
            sigmaLevelNomTrait = elementMeterNomTraits.Entity(NomTraitType.SigmaPerformanceLevel)
            expectedTraitCount += 1

            nomTrait.LowerOverflowLimit = lowerOverflowLimit
            expectedTraitCount += 1

            nomTrait.UpperOverflowLimit = upperOverflowLimit
            expectedTraitCount += 1

            nomTrait.NominalValue = nominalValue
            nomTraitEntity = elementMeterNomTraits.Entity(NomTraitType.NominalValue)
            expectedTraitCount += 1

            nomTrait.Tolerance = tolerance
            toleranceNomTrait = elementMeterNomTraits.Entity(NomTraitType.Tolerance)
            expectedTraitCount += 1

            nomTrait.GuardBandFactor = guardBandFactor
            guardBandFactorNomTrait = elementMeterNomTraits.Entity(NomTraitType.GuardBandFactor)
            expectedTraitCount += 1

            nomTrait.LowerGuardedSpecificationLimit = expectedGuardedSpecificationRange.Min
            lowerGuardedSpecificationLimitNomTrait = elementMeterNomTraits.Entity(NomTraitType.LowerGuardedSpecificationLimit)
            expectedTraitCount += 1

            nomTrait.UpperGuardedSpecificationLimit = expectedGuardedSpecificationRange.Max
            upperGuardedSpecificationLimitNomTrait = elementMeterNomTraits.Entity(NomTraitType.UpperGuardedSpecificationLimit)
            expectedTraitCount += 1

            Assert.AreEqual(expectedTraitCount, part.NomTraits(nomTraitSelector).Count,
                            $"{NameOf(Entities.PartNomEntityCollection)} element count should match")

            part.NomTraits.Upsert(connection)

            Assert.AreEqual(expectedTraitCount, part.NomTraits(nomTraitSelector).Count,
                            $"{NameOf(Entities.PartNomEntityCollection)} element count should match after upsert")
            r = sigmaLevelNomTrait.ValidateStoredEntity($"{NameOf(Entities.NomTraitEntity)} Meter-specific {NameOf(nomTrait.SigmaPerformanceLevel)} should be clean")
            Assert.IsTrue(r.Success, r.Details)

            part.FetchNomTraits(connection, multimeterIdentities)
            Assert.AreEqual(expectedTraitCount, part.NomTraits(nomTraitSelector).Count,
                            $"{NameOf(Entities.PartNomEntityCollection)} element count should match after fetch")

            Assert.IsTrue(nomTrait.NominalValue.HasValue,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.NominalValue)} should have a value")
            Assert.AreEqual(nominalValue, nomTrait.NominalValue.Value,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.NominalValue)} value should match")
            Assert.IsTrue(nomTrait.Tolerance.HasValue,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.Tolerance)} should have a value")
            Assert.AreEqual(tolerance, nomTrait.Tolerance.Value,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.Tolerance)} value should match")
            Assert.IsTrue(nomTrait.SigmaPerformanceLevel.HasValue,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.SigmaPerformanceLevel)} should have a value")
            Assert.AreEqual(sigmaPerformanceLevel, nomTrait.SigmaPerformanceLevel.Value,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.SigmaPerformanceLevel)} value should match")
            Assert.IsTrue(nomTrait.GuardBandFactor.HasValue,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.GuardBandFactor)} should have a value")
            Assert.AreEqual(guardBandFactor, nomTrait.GuardBandFactor.Value,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.GuardBandFactor)} value should match")

            Assert.AreEqual(expectedGuardedSpecificationLimit, nomTrait.EstimatedGuardedSpecificationLimit,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.EstimatedGuardedSpecificationLimit)} value should match")
            Assert.AreEqual(expectedSpecificationRange.Min, nomTrait.EstimatedSpecificationRange.Min,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.EstimatedSpecificationRange)} minimum value should match")
            Assert.AreEqual(expectedSpecificationRange.Max, nomTrait.EstimatedSpecificationRange.Max,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.EstimatedSpecificationRange)} maximum value should match")

            Assert.AreEqual(expectedGuardedSpecificationRange.Min, nomTrait.EstimatedGuardedSpecificationRange.Min,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.EstimatedGuardedSpecificationRange)} minimum value should match")
            Assert.AreEqual(expectedGuardedSpecificationRange.Max, nomTrait.EstimatedGuardedSpecificationRange.Max,
                          $"{NameOf(Entities.PartNomEntityCollection)} {NameOf(Entities.NomTrait.EstimatedGuardedSpecificationRange)} maximum value should match")

            element.FetchNoms(connection, part, multimeterIdentities)
            Assert.IsTrue((element.NomTraitsCollection?.Any).GetValueOrDefault(False),
                          $"{NameOf(ElementEntity)} should have {NameOf(ElementEntity.NomTraitsCollection)}")

        End Using
    End Sub

    ''' <summary> Assert adding element texts. </summary>
    ''' <remarks> David, 7/6/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="element">  The element. </param>
    Public Shared Sub AssertAddingElementTexts(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                               ByVal element As ElementEntity)
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {ElementTextTypeBuilder.TableName, ElementNomTextBuilder.TableName})
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            element.FetchNomTexts(connection)
            If CType(element.ElementTypeId, ElementType) = ElementType.CompoundResistor OrElse CType(element.ElementTypeId, ElementType) = ElementType.Resistor Then
                element.NomTexts(NomType.Resistance).ElementText.ScanList = "@(1)"
                Assert.IsTrue((element.NomTexts?.Any).GetValueOrDefault(False),
                          $"{NameOf(ElementEntity)} should have {NameOf(ElementEntity.NomTexts)}")

                element.FetchNomTexts(connection)
                Assert.IsTrue((element.NomTexts?.Any).GetValueOrDefault(False),
                          $"{NameOf(ElementEntity)} should have {NameOf(ElementEntity.NomTexts)}")
            End If
        End Using
    End Sub

    #End Region

    #Region " GRADE "

    ''' <summary> Asserts <see cref="GradeEntity"/> test conditions. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="site">        The site. </param>
    ''' <param name="provider">    The provider. </param>
    ''' <param name="gradeNumber"> The grade number. </param>
    ''' <param name="gradeLabel">  The grade label. </param>
    ''' <param name="gradeCount">  Number of grades. </param>
    Public Shared Sub AssertGradeEntity(ByVal site As TestSiteBase, ByVal provider As ProviderBase, ByVal gradeNumber As Integer, ByVal gradeLabel As String, ByVal gradeCount As Integer)

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {GradeBuilder.TableName})
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim SubstrateTanTcrGrade As New GradeEntity
            SubstrateTanTcrGrade.FetchUsingKey(connection, gradeNumber)

            Dim expectedLabel As String = gradeLabel
            Assert.AreEqual(expectedLabel, SubstrateTanTcrGrade.Description, $"{NameOf(GradeEntity)}.{NameOf(GradeEntity.Label)}")

            Dim expectedGradeNumber As Integer = gradeNumber
            expectedLabel = gradeLabel
            SubstrateTanTcrGrade.FetchUsingUniqueIndex(connection, expectedLabel)
            Assert.AreEqual(expectedGradeNumber, SubstrateTanTcrGrade.Id, $"{NameOf(GradeEntity)}.{NameOf(GradeEntity.Id)}")

            Dim expectedCount As Integer = gradeCount
            Dim count As Integer = SubstrateTanTcrGrade.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, count, $"{NameOf(GradeEntity)} entity correct number of {NameOf(GradeEntity)}")
        End Using
    End Sub

    ''' <summary> Asserts <see cref="GradeEntity"/> test conditions. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Friend Shared Sub AssertGradeEntity(ByVal site As TestSite, ByVal provider As ProviderBase)
        Auditor.AssertGradeEntity(site, provider, CInt(isr.Dapper.Entities.Grade.Three), isr.Dapper.Entities.Grade.Three.Description, 8)
        Auditor.AssertGradeEntity(site, provider, CInt(isr.Dapper.Entities.Grade.Four), isr.Dapper.Entities.Grade.Four.Description, 8)
    End Sub

    #End Region

    #Region " FILE IMPORT STATE "

    ''' <summary> Asserts the file import state entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">                The site. </param>
    ''' <param name="provider">            The provider. </param>
    ''' <param name="expectedRecordCount"> Number of expected records. </param>
    Public Shared Sub AssertFileImportStateEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal expectedRecordCount As Integer)
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {FileImportStateBuilder.TableName})
        Dim id As Integer = 10
        Dim fileImportState As New FileImportStateEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = fileImportState.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.FileImportState)} with {NameOf(Entities.FileImportStateNub.Id)} of {id} should not exist")
            r = fileImportState.ValidateNewEntity($"New {NameOf(Entities.FileImportState)} with {NameOf(Entities.FileImportStateNub.Id)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            actualFetched = fileImportState.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.FileImportState)} with {NameOf(Entities.FileImportStateNub.Id)} of {id} should not exist")
            r = fileImportState.ValidateNewEntity($"New {NameOf(Entities.FileImportState)} with {NameOf(Entities.FileImportStateNub.Id)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            id = 1
            expectedFetched = True
            actualFetched = fileImportState.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.FileImportState)} with {NameOf(Entities.FileImportStateNub.Id)} of {id} should exist")
            r = fileImportState.ValidateStoredEntity($"Fetched {NameOf(Entities.FileImportState)} with {NameOf(Entities.FileImportStateNub.Id)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            Dim label As String = isr.Dapper.Entities.FileImportState.Imported.ToString
            fileImportState.Label = label
            r = fileImportState.ValidateChangedEntity($"Changed {NameOf(Entities.FileImportState)} with {NameOf(Entities.FileImportStateNub.Label)} to {label}")
            Assert.IsTrue(r.Success, r.Details)

            actualFetched = fileImportState.FetchUsingUniqueIndex(connection)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.FileImportState)} with {NameOf(Entities.FileImportStateNub.Label)} with {label} should exist")
            Assert.AreEqual(label, fileImportState.Label, $"{NameOf(Entities.FileImportState)} {NameOf(Entities.FileImportStateNub.Label)} should match")
            r = fileImportState.ValidateStoredEntity($"Fetched {NameOf(Entities.FileImportState)} with {NameOf(Entities.FileImportStateNub.Label)} of {label}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualCount As Integer = fileImportState.FetchAllEntities(connection)
            Assert.AreEqual(expectedRecordCount, actualCount, $"{NameOf(Entities.FileImportState)} record count should match")
        End Using
    End Sub

    #End Region

    #Region " IMPORT FILE "

    ''' <summary> Asserts ImportFile entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">             The site. </param>
    ''' <param name="provider">         The provider. </param>
    ''' <param name="importFileFolder"> Pathname of the import file folder. </param>
    ''' <param name="importFileName">   Filename of the import file. </param>
    Public Shared Sub AssertImportFileEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                             ByVal importFileFolder As String, ByVal importFileName As String)
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {FileImportStateBuilder.TableName,
                                                            ImportFileBuilder.TableName})
        Dim id As Integer = 3
        Dim importFile As New ImportFileEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedCount As Integer = 0
            connection.DeleteAll(Of ImportFileNub)()
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = importFile.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.Id)} of {id} should not exist")
            Dim r As (Success As Boolean, Details As String)
            r = importFile.ValidateNewEntity($"New {NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.Id)} of {id}")
            Assert.IsTrue(r.Success, r.Details)
            importFile.Clear()

            id = 1
            Dim fileName As String = Path.Combine(importFileFolder, importFileName)
            importFile = New ImportFileEntity With {.Id = id, .FileName = fileName, .FileImportStateId = FileImportState.Ready,
                                                    .Details = String.Empty, .FileTimestamp = DateTime.UtcNow, .ImportTimestamp = DateTime.UtcNow}
            Dim actualInserted As Boolean = importFile.Insert(connection)
            expectedCount += 1
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.FileName)} of {fileName} should have been inserted")
            r = importFile.ValidateStoredEntity($"Insert {NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.FileName)} of {fileName}")
            Assert.IsTrue(r.Success, r.Details)

            fileName = "Filename2"
            importFile.FileName = fileName
            r = importFile.ValidateChangedEntity($"Changed {NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.FileName)} to {fileName}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualUpdate As Boolean = importFile.Update(connection, UpdateModes.ForceUpdate Or UpdateModes.Refetch)
            Dim expectedUpdate As Boolean = True
            Assert.AreEqual(expectedUpdate, actualUpdate, $"{NameOf(Entities.ImportFileEntity)} with changed {NameOf(Entities.ImportFileEntity.FileName)} of {fileName} should be updated")
            r = importFile.ValidateStoredEntity($"Updated {NameOf(Entities.ImportFileEntity)} with changed {NameOf(Entities.ImportFileEntity.FileName)} of {fileName}")
            Assert.IsTrue(r.Success, r.Details)

            actualUpdate = importFile.Update(connection, UpdateModes.UpdateOnly)
            expectedUpdate = False
            Assert.AreEqual(expectedUpdate, actualUpdate, $"Unchanged {NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.FileName)} of {fileName} should be clear--not updated")
            r = importFile.ValidateStoredEntity($"Unchanged {NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.FileName)} of {fileName}")
            Assert.IsTrue(r.Success, r.Details)

            Assert.AreEqual(1, ImportFileEntity.CountEntities(connection, importFile.FileName), $"Single {NameOf(Entities.ImportFileEntity)} should be counted for {NameOf(Entities.ImportFileEntity.FileName)} of {fileName}")
            Assert.IsTrue(ImportFileEntity.IsExists(connection, importFile.FileName), $"{NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.FileName)} of {fileName} should {NameOf(ImportFileEntity.IsExists)}")

            fileName = importFile.FileName
            Dim ImportFileAutoId As Integer = importFile.Id
            actualFetched = importFile.Obtain(connection)
            expectedFetched = True
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.FileName)} of {fileName} should have been obtained")
            Assert.AreEqual(fileName, importFile.FileName, $"Obtained {NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.FileName)} should match")
            Assert.AreEqual(ImportFileAutoId, importFile.Id, $"Obtained {NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.Id)} of {fileName} should match")


            connection.DeleteAll(Of ImportFileNub)()
            expectedCount = 10
            For i As Integer = 1 To expectedCount
                importFile.FileName = $"FileName{i}"
                id += 1
                importFile.Id = id
                actualInserted = importFile.Insert(connection)
                expectedInserted = True
                Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.FileName)} of {fileName} should have been inserted")
            Next
            Dim actualCount As Integer = importFile.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple {NameOf(Entities.ImportFileEntity)}'s should have been inserted")

            fileName = importFile.FileName
            Dim actualDelete As Boolean = importFile.Delete(connection)
            Dim expectedDelete As Boolean = True
            Assert.AreEqual(expectedDelete, actualDelete, $"{NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.FileName)} of {fileName} should be deleted")

            Dim ImportFile0 As IImportFile = importFile.ImportFiles(0)
            actualDelete = ImportFileEntity.Delete(connection, ImportFile0.Id)
            expectedDelete = True
            Assert.AreEqual(expectedDelete, actualDelete, $"{NameOf(Entities.ImportFileEntity)} with {NameOf(Entities.ImportFileEntity.FileName)} of {fileName} should be deleted")
        End Using
    End Sub

    #End Region

    #Region " LOT "

    ''' <summary> Asserts <see cref="LotTraitTypeEntity"/> test conditions. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertLotTraitTypeEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {LotTraitTypeBuilder.TableName})
        Dim id As Integer = isr.Dapper.Entities.LotTraitType.None
        Dim lotTraitType As New LotTraitTypeEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = lotTraitType.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.LotTraitTypeEntity)} With {NameOf(Entities.LotTraitTypeEntity.Id)} Of {id} should Not exist")
            Dim r As (Success As Boolean, Details As String)
            r = lotTraitType.ValidateNewEntity($"New {NameOf(Entities.LotTraitTypeEntity)} With {NameOf(Entities.LotTraitTypeEntity.Id)} Of {id}")
            Assert.IsTrue(r.Success, r.Details)

            lotTraitType.Clear()
            id = isr.Dapper.Entities.LotTraitType.CertifiedQuantity
            expectedFetched = True
            actualFetched = lotTraitType.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.LotTraitTypeEntity)} With {NameOf(Entities.LotTraitTypeEntity.Id)} Of {id}should exist")
            r = lotTraitType.ValidateStoredEntity($"Fetched {NameOf(Entities.LotTraitTypeEntity)} With {NameOf(Entities.LotTraitTypeEntity.Id)} Of {id}")
            Assert.IsTrue(r.Success, r.Details)

            lotTraitType.Label = isr.Dapper.Entities.LotTraitType.CertifiedQuantity.ToString
            actualFetched = lotTraitType.FetchUsingUniqueIndex(connection)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.LotTraitTypeEntity)} by {NameOf(Entities.LotTraitTypeEntity.Label)} Of {isr.Dapper.Entities.LotTraitType.CertifiedQuantity} should exist")
            r = lotTraitType.ValidateStoredEntity($"Fetched {NameOf(Entities.LotTraitTypeEntity)} by {NameOf(Entities.LotTraitTypeEntity.Label)} Of {isr.Dapper.Entities.LotTraitType.CertifiedQuantity}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = [Enum].GetValues(GetType(isr.Dapper.Entities.LotTraitType)).Length - 1
            Dim actualCount As Integer = lotTraitType.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"Expected {expectedCount} {NameOf(Entities.LotTraitTypeEntity)}s")
        End Using
    End Sub

    ''' <summary> Assert adding lot entity. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="partAutoId"> Identifier for the part entity. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> A LotEntity. </returns>
    Public Shared Function AssertAddingLotEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                 ByVal partAutoId As Integer, ByVal lotNumber As String) As LotEntity
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {LotBuilder.TableName})
        Dim lot As New LotEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            lot = New LotEntity With {.LotNumber = lotNumber}
            Dim actualObtained As Boolean = lot.Obtain(connection)
            Dim expectedObtained As Boolean = True
            Assert.AreEqual(expectedObtained, actualObtained, $"{NameOf(Entities.LotEntity)}  with '{NameOf(Entities.LotEntity.LotNumber)}' of {lot.LotNumber} should have been obtained")
            Dim r As (Success As Boolean, Details As String)
            r = lot.ValidateStoredEntity($"Obtain {NameOf(Entities.LotEntity)} with '{NameOf(Entities.LotEntity.LotNumber)}' of {lotNumber}")
            Assert.IsTrue(r.Success, r.Details)

            Dim partLot As New PartLotEntity With {.LotAutoId = lot.AutoId, .PartAutoId = partAutoId}
            partLot.Obtain(connection)
            r = partLot.ValidateStoredEntity($"Inserted {NameOf(Entities.PartLotEntity)} with [{NameOf(Entities.PartLotEntity.LotAutoId)},{NameOf(Entities.PartLotEntity.PartAutoId)}] of {lot.AutoId},{partAutoId}]")
            Assert.IsTrue(r.Success, r.Details)

            Dim part As PartEntity = partLot.FetchPartEntity(connection)
            r = part.ValidateStoredEntity($"Fetched {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartLotEntity.PartAutoId)} of {partLot.PartAutoId}")
            Assert.IsTrue(r.Success, r.Details)

            Assert.AreEqual(part.PartNumber, PartLotEntity.FetchParts(connection, lot.AutoId).First.PartNumber, $"Fetch {NameOf(PartLotEntity)}.{NameOf(PartLotEntity.PartEntity)} should match")
            Assert.IsTrue(PartLotEntity.FetchLots(connection, partAutoId).Contains(lot), $"Fetched {NameOf(PartLotEntity)}.{NameOf(PartLotEntity.LotEntity)}'s should contain lot")
        End Using
        Return lot
    End Function

    ''' <summary> Assert adding lots. </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <param name="lotNumbers"> The lot numbers. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process assert adding lots in this collection.
    ''' </returns>
    Public Shared Function AssertAddingLots(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                          ByVal partNumber As String, ByVal lotNumbers As String()) As IEnumerable(Of LotEntity)
        Dim part As PartEntity = Auditor.AssertAddingPartEntity(site, provider, partNumber)
        Dim lots As New List(Of LotEntity)
        For Each lotNumber As String In lotNumbers
            lots.Add(Auditor.AssertAddingLotEntity(site, provider, part.AutoId, lotNumber))
        Next
        Return lots
    End Function

    ''' <summary> Asserts lot entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <param name="lotNumbers"> The lot numbers. </param>
    Public Shared Sub AssertLotEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                      ByVal partNumber As String, ByVal lotNumbers As IEnumerable(Of String))

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {LotBuilder.TableName})
        Dim id As Integer = 3
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            connection.DeleteAll(Of LotNub)()
            connection.DeleteAll(Of PartNub)()
            Dim partId As Integer = 1
            Dim part As New PartEntity With {.PartNumber = partNumber}
            part.Insert(connection)
            partId = part.AutoId
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = False

            Dim expectedCount As Integer = 0
            Dim lot As New LotEntity
            actualFetched = lot.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.LotEntity)} with '{NameOf(Entities.LotEntity.AutoId)}' of {id} should not exist")
            Dim r As (Success As Boolean, Details As String)
            r = lot.ValidateNewEntity($"New {NameOf(Entities.LotEntity)} with '{NameOf(Entities.LotEntity.AutoId)}' of {id}")
            Assert.IsTrue(r.Success, r.Details)
            lot.Clear()

            Assert.IsTrue(LotBuilder.Get.UsingUniqueLabel(connection), $"{NameOf(Entities.LotEntity)} should be using a unique {NameOf(Entities.LotEntity.LotNumber)} ")
            Dim lotNumber As String = lotNumbers(0)
            lot = New LotEntity With {.LotNumber = lotNumber}
            Dim actualInserted As Boolean = lot.Insert(connection)
            expectedCount += 1
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.LotEntity)}  with '{NameOf(Entities.LotEntity.LotNumber)}' of {lot.LotNumber} should have been inserted")
            r = lot.ValidateStoredEntity($"Insert {NameOf(Entities.LotEntity)} with '{NameOf(Entities.LotEntity.LotNumber)}' of {lotNumber}")
            Assert.IsTrue(r.Success, r.Details)

            lot.FetchUsingKey(connection)
            Dim expectedTimestamp As DateTime = DateTime.UtcNow
            Dim actualTimeStamp As DateTime = lot.Timestamp
            Asserts.Instance.AreEqual(expectedTimestamp, actualTimeStamp, TimeSpan.FromSeconds(10), "Expected timestamp")

            Dim partLot As New PartLotEntity With {.LotAutoId = lot.AutoId, .PartAutoId = part.AutoId}
            partLot.Insert(connection)
            r = partLot.ValidateStoredEntity($"Inserted {NameOf(Entities.PartLotEntity)} with [{NameOf(Entities.LotEntity.LotNumber)},{NameOf(Entities.PartEntity.PartNumber)}] of {lotNumber},{partNumber}]")
            Assert.IsTrue(r.Success, r.Details)

            partLot.FetchPartEntity(connection)
            Assert.AreEqual(part.PartNumber, partLot.PartEntity.PartNumber, $"Fetched {NameOf(Entities.PartLotEntity)} using '{NameOf(Entities.PartLotEntity.PrimaryId)}' of {lot.AutoId} should match the {NameOf(Entities.PartEntity)} '{NameOf(Entities.PartEntity.PartNumber)} '")
            r = partLot.PartEntity.ValidateStoredEntity($"Fetched {NameOf(Entities.PartLotEntity)} using '{NameOf(Entities.PartLotEntity.PrimaryId)}' of {lot.AutoId} should match the {NameOf(Entities.PartEntity)} '{NameOf(Entities.PartEntity.PartNumber)} of [{lotNumber},{partNumber}]")
            Assert.IsTrue(r.Success, r.Details)

            lotNumber = lotNumbers(1)
            lot.LotNumber = lotNumber
            r = lot.ValidateChangedEntity($"Changed {NameOf(Entities.LotEntity)} with '{NameOf(Entities.LotEntity.LotNumber)}' of {lotNumber}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualUpdate As Boolean = lot.Update(connection, UpdateModes.Refetch)
            Dim expectedUpdate As Boolean = True
            Assert.AreEqual(expectedUpdate, actualUpdate, $"{NameOf(Entities.LotEntity)} with '{NameOf(Entities.LotEntity.LotNumber)}' of {lot.LotNumber} should be updated")
            r = lot.ValidateStoredEntity($"Updated {NameOf(Entities.LotEntity)} with '{NameOf(Entities.LotEntity.LotNumber)}' of {lotNumber}")
            Assert.IsTrue(r.Success, r.Details)

            actualUpdate = lot.Update(connection, UpdateModes.Refetch)
            expectedUpdate = False
            Assert.AreEqual(expectedUpdate, actualUpdate, $"Updated {NameOf(Entities.LotEntity)} with unchanged '{NameOf(Entities.LotEntity.LotNumber)}' of {lot.LotNumber} should be clean")
            r = lot.ValidateStoredEntity($"Updated {NameOf(Entities.LotEntity)} with unchanged '{NameOf(Entities.LotEntity.LotNumber)}' of {lot.LotNumber}")
            Assert.IsTrue(r.Success, r.Details)

            ' Insert must be done on a new lot; setting the lot number and inserting leaves the proxy 'dirty'. lot.LotNumber = lotNumbers(0)
            lot = New LotEntity With {.LotNumber = lotNumbers(0), .AutoId = partId}
            actualInserted = lot.Insert(connection)
            partLot = New PartLotEntity With {.LotAutoId = lot.AutoId, .PartAutoId = part.AutoId}
            partLot.Insert(connection)
            r = partLot.ValidateStoredEntity($"Inserted {NameOf(Entities.PartLotEntity)} with [{NameOf(Entities.LotEntity.LotNumber)},{NameOf(Entities.PartEntity.PartNumber)}] of {lotNumber},{partNumber}]")
            Assert.IsTrue(r.Success, r.Details)

            expectedCount += 1
            Assert.AreEqual(expectedCount, PartLotEntity.CountLots(connection, part.AutoId), $"{expectedCount} lots should be counted for {part.PartNumber}")

            Assert.AreEqual(1, PartLotEntity.CountParts(connection, lot.AutoId), $"Single part should be counted for lot #{lot.LotNumber}")
            Assert.IsTrue(PartLotEntity.IsExists(connection, part.AutoId, lot.AutoId),
                          $"{NameOf(Entities.PartLotEntity)} with [{NameOf(Entities.PartLotEntity.PrimaryId)},{NameOf(Entities.PartLotEntity.SecondaryId)}] of [{part.AutoId}.{lot.AutoId} should exist")


            lotNumber = lot.LotNumber
            Dim AutoId As Integer = lot.AutoId
            actualFetched = lot.Obtain(connection)
            expectedFetched = True
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.LotNumber)} {lot.LotNumber} should have been obtained")
            Assert.AreEqual(lotNumber, lot.LotNumber, $"Obtained {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.LotNumber)} of {lot.LotNumber} should match lot number")
            Assert.AreEqual(AutoId, lot.AutoId, $"Obtained Part auto id {lot.AutoId} should match part auto id")

            connection.DeleteAll(Of LotNub)()
            connection.DeleteAll(Of PartLotNub)()
            expectedCount = 10
            For i As Integer = 1 To expectedCount
                lotNumber = $"Lot{i}"
                partLot = New PartLotEntity
                actualInserted = partLot.Obtain(connection, part.AutoId, lotNumber)
                Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.PartLotEntity)} for {lot.LotNumber} should have been inserted")
            Next
            Dim actualCount As Integer = lot.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"Lot count should match after inserting {expectedCount} lots")

            Dim lot0 As LotEntity = lot.Lots.First
            Assert.IsTrue(lot0.IsClean, $"Fetched entity {lot0.LotNumber} should be clean")
            lotNumber = lot0.LotNumber
            Dim actualDelete As Boolean = lot0.Delete(connection)
            Dim expectedDelete As Boolean = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Lot {lotNumber} should be deleted")
            expectedCount -= 1

            actualCount = lot.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"Lot count should match after deletion of one lot")

            lot0 = lot.Lots.First
            Assert.IsTrue(lot0.IsClean, $"Fetched entity {lot0.LotNumber} should be clean")
            actualDelete = LotEntity.Delete(connection, lot0.AutoId)
            expectedDelete = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Lot {lot0.LotNumber} should be deleted")
            expectedCount -= 1
            part = New PartEntity
            part.FetchUsingKey(connection, partId)
            actualCount = Entities.PartLotEntity.CountLots(connection, part.AutoId)
            Assert.AreEqual(expectedCount, actualCount, $"multiple Lots should have been fetched for part {part.PartNumber}")

        End Using
    End Sub

    #End Region

    #Region " LOT TRAIT "

    ''' <summary> Assert adding Lot Traits. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">      The site. </param>
    ''' <param name="provider">  The provider. </param>
    ''' <param name="lotAutoId"> Identifier for the <see cref="LotEntity"/> identity. </param>
    ''' <returns> A LotEntity. </returns>
    Public Shared Function AssertAddingLotTraits(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal lotAutoId As Integer) As LotEntity

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {LotBuilder.TableName, LotTraitBuilder.TableName,
                                                            LotTraitTypeBuilder.TableName})
        Dim Lot As New LotEntity With {.AutoId = lotAutoId}
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedTraitCount As Integer = 0
            Dim certifiedQuantity As Integer = 100
            Dim LotTraitEntity As New LotTraitEntity With {.LotAutoId = lotAutoId, .LotTraitTypeId = LotTraitType.CertifiedQuantity, .Amount = certifiedQuantity}
            LotTraitEntity.Insert(connection)
            expectedTraitCount += 1

            Lot.FetchUsingKey(connection)
            Dim r As (Success As Boolean, Details As String)
            r = Lot.ValidateStoredEntity($"Fetch {NameOf(Entities.LotEntity)} with '{NameOf(Entities.LotEntity.AutoId)}' of {lotAutoId}")
            Assert.IsTrue(r.Success, r.Details)
            Assert.AreEqual(expectedTraitCount, Lot.FetchTraits(connection), $"{NameOf(Entities.LotEntity)} {NameOf(Entities.LotEntity.Traits)} count should match")
            Assert.AreEqual(certifiedQuantity, Lot.Traits.LotTrait.CertifiedQuantity, $"{NameOf(Entities.LotEntity)} {NameOf(Entities.LotEntity.Traits)} {NameOf(Entities.LotTrait.CertifiedQuantity)} should match")

            Dim modifiedCertifiedQuantity As Integer = certifiedQuantity + 1
            Lot.Traits.LotTrait.CertifiedQuantity = modifiedCertifiedQuantity
            Assert.IsTrue(Lot.Upsert(connection), $"{NameOf(Entities.LotEntity)} {NameOf(Entities.LotEntity.Traits)} {NameOf(Entities.LotTrait.CertifiedQuantity)} should be updated")
            Assert.AreEqual(expectedTraitCount, Lot.FetchTraits(connection), $"{NameOf(Entities.LotEntity)} {NameOf(Entities.LotEntity.Traits)} count should match after second fetch")
            Assert.AreEqual(modifiedCertifiedQuantity, Lot.Traits.LotTrait.CertifiedQuantity, $"{NameOf(Entities.LotEntity)} {NameOf(Entities.LotEntity.Traits)} {NameOf(Entities.LotTrait.CertifiedQuantity)} should match after change")

        End Using
        Return Lot
    End Function

    ''' <summary> Assert adding Lot Traits. </summary>
    ''' <remarks> David, 6/17/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    Public Shared Sub AssertAddingLotTraits(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                            ByVal partNumber As String, ByVal lotNumber As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            connection.DeleteAll(Of LotNub)
            connection.DeleteAll(Of LotTraitNub)
            Dim part As PartEntity = Auditor.AssertAddingPartEntity(site, provider, partNumber)
            Dim Lot As LotEntity = Auditor.AssertAddingLotEntity(site, provider, part.AutoId, lotNumber)
            Lot = Auditor.AssertAddingLotTraits(site, provider, Lot.AutoId)
        End Using

    End Sub

    #End Region

    #Region " METER GUARD BAND LOOKUP ENTITY "

    ''' <summary> Asserts <see cref="MeterGuardBandLookupEntity"/> test conditions. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertMeterGuardBandLookupEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {MeterGuardBandLookupBuilder.TableName})
        Dim guardBandLookupId As Integer = 0
        Dim MeterGuardBandLookup As New MeterGuardBandLookupEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = MeterGuardBandLookup.FetchUsingKey(connection, guardBandLookupId)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {guardBandLookupId} should not exist")
            Dim r As (Success As Boolean, Details As String)
            r = MeterGuardBandLookup.ValidateNewEntity($"New {NameOf(IMeterGuardBandLookup)} with {NameOf(IMeterGuardBandLookup.Id)} of {guardBandLookupId}")
            Assert.IsTrue(r.Success, r.Details)
            guardBandLookupId = 1
            expectedFetched = True
            actualFetched = MeterGuardBandLookup.FetchUsingKey(connection, guardBandLookupId)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {guardBandLookupId} should exist")
            r = MeterGuardBandLookup.ValidateStoredEntity($"Stored {NameOf(IMeterGuardBandLookup)} with {NameOf(IMeterGuardBandLookup.Id)} of {guardBandLookupId}")
            Assert.IsTrue(r.Success, r.Details)

            MeterGuardBandLookup.Clear()
            MeterGuardBandLookupEntity.TryFetchAll(connection)
            ' Dim actualCount As Integer = MeterGuardBandLookupEntity.MeterGuardBandLookups.Count
            Assert.IsTrue(MeterGuardBandLookupEntity.MeterGuardBandLookups.Any, $"{NameOf(IMeterGuardBandLookup)} should have entities")
            Dim value As Double
            Dim expectedExists As Boolean = True
            Dim actualExists As Boolean
            Dim fetchedNub As MeterGuardBandLookupNub
            For Each nub As MeterGuardBandLookupEntity In MeterGuardBandLookupEntity.MeterGuardBandLookups
                If nub.MinimumInclusiveValue Is Nothing Then
                    value = nub.MaximumExclusiveValue.Value - 1
                ElseIf nub.MaximumExclusiveValue Is Nothing Then
                    value = nub.MinimumInclusiveValue.Value + 1
                Else
                    value = 0.5 * (nub.MaximumExclusiveValue.Value + nub.MinimumInclusiveValue.Value)
                End If
                actualExists = MeterGuardBandLookupEntity.IsExists(connection, nub.ToleranceCode, nub.MultimeterId, nub.NomTypeId, value)
                Assert.IsTrue(actualExists, $"Record for {nub.Id} should exist")
                fetchedNub = MeterGuardBandLookupEntity.FetchNubs(connection, nub.ToleranceCode, nub.MultimeterId, nub.NomTypeId, value).First
                Assert.IsNotNull(fetchedNub, $"Fetched entity for {nub.Id} should not be null")
                Assert.AreEqual(nub.Id, fetchedNub.Id, $"Fetched entity id values should match {nub.ToleranceCode} {fetchedNub.ToleranceCode}")
                Assert.IsTrue(MeterGuardBandLookupNub.AreEqual(nub, fetchedNub), $"Fetched entity with id {fetchedNub.Id} should equal the origin entity with id {nub.Id} ")
            Next
        End Using
    End Sub

    #End Region

    #Region " MULTIMETER ENTITY "

    ''' <summary> Asserts <see cref="MultimeterEntity"/> test conditions. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertMultimeterEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {MultimeterBuilder.TableName})
        Dim MultimeterModelId As Integer = MultimeterModel.None
        Dim Multimeter As New MultimeterEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = Multimeter.FetchUsingKey(connection, MultimeterModelId)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {MultimeterModelId} should not exist")
            Dim r As (Success As Boolean, Details As String)
            r = Multimeter.ValidateNewEntity($"New {NameOf(MultimeterEntity)} with {NameOf(MultimeterEntity.Id)} of {MultimeterModelId}")
            Assert.IsTrue(r.Success, r.Details)

            Multimeter.Clear()
            Dim expectedCount As Integer = 0
            For MultimeterNumber As Integer = 1 To 2
                For Each MultimeterModelId In New Integer() {isr.Dapper.Entities.MultimeterModel.K2002, isr.Dapper.Entities.MultimeterModel.K7510}
                    expectedFetched = True
                    actualFetched = Multimeter.FetchUsingUniqueIndex(connection, MultimeterNumber, MultimeterModelId)
                    Assert.AreEqual(expectedFetched, actualFetched, $"key {MultimeterModelId} should exist")
                    r = Multimeter.ValidateStoredEntity($"Fetched {NameOf(MultimeterEntity)} with [{NameOf(MultimeterEntity.MultimeterNumber)},{NameOf(MultimeterEntity.MultimeterModelId)}] of [{MultimeterNumber},{MultimeterModelId}]")
                    Assert.IsTrue(r.Success, r.Details)
                    expectedCount += 1
                Next
            Next
            Dim actualCount As Integer = Multimeter.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"Expected {expectedCount} Multimeters")
        End Using
    End Sub

    #End Region

    #Region " METER MODEL ENTITY "

    ''' <summary> Asserts <see cref="MultimeterModelEntity"/> test conditions. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertMultimeterModelEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {MultimeterModelBuilder.TableName})
        Dim id As Integer = isr.Dapper.Entities.MultimeterModel.None
        Dim meterModel As New MultimeterModelEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = meterModel.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(MultimeterModelEntity)} with {NameOf(MultimeterModelEntity.Id)} of {id} should not exist")
            Dim r As (Success As Boolean, Details As String)
            r = meterModel.ValidateNewEntity($"New {NameOf(MultimeterModelEntity)} with {NameOf(MultimeterModelEntity.Id)} of {id}")
            Assert.IsTrue(r.Success, r.Details)
            meterModel.Clear()
            id = isr.Dapper.Entities.MultimeterModel.K2002
            expectedFetched = True
            actualFetched = meterModel.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(MultimeterModelEntity)} with {NameOf(MultimeterModelEntity.Id)} of {id} should exist")
            r = meterModel.ValidateStoredEntity($"Fetched  {NameOf(MultimeterModelEntity)} with {NameOf(MultimeterModelEntity.Id)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            meterModel.Label = isr.Dapper.Entities.MultimeterModel.K2002.ToString
            actualFetched = meterModel.FetchUsingUniqueIndex(connection)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(MultimeterModelEntity)} with {NameOf(MultimeterModelEntity.Label)} of {isr.Dapper.Entities.MultimeterModel.K2002} should exist")
            r = meterModel.ValidateStoredEntity($"Fetched  {NameOf(MultimeterModelEntity)} by {NameOf(MultimeterModelEntity.Label)} equals {isr.Dapper.Entities.MultimeterModel.K2002}")
            Assert.IsTrue(r.Success, r.Details)

            id = isr.Dapper.Entities.MultimeterModel.K7510
            expectedFetched = True
            actualFetched = meterModel.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(MultimeterModelEntity)} with {NameOf(MultimeterModelEntity.Id)} of {id} should exist")
            r = meterModel.ValidateStoredEntity($"Fetched  {NameOf(MultimeterModelEntity)} with {NameOf(MultimeterModelEntity.Id)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            meterModel.Label = isr.Dapper.Entities.MultimeterModel.K7510.ToString
            actualFetched = meterModel.FetchUsingUniqueIndex(connection)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(MultimeterModelEntity)} by name {NameOf(MultimeterModelEntity.Label)} of {isr.Dapper.Entities.MultimeterModel.K7510} should exist")
            r = meterModel.ValidateStoredEntity($"Fetched {NameOf(MultimeterModelEntity)} by name {NameOf(MultimeterModelEntity.Label)} equals {isr.Dapper.Entities.MultimeterModel.K7510}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = [Enum].GetValues(GetType(isr.Dapper.Entities.MultimeterModel)).Length - 1
            Dim actualCount As Integer = meterModel.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"Expected {expectedCount} meter models")
        End Using
    End Sub

    #End Region

    #Region " NOMINAL READING TYPE ENTITY "

    ''' <summary> Asserts <see cref="NomReadingTypeEntity"/> test conditions. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertNomReadingTypeEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Dim id As Integer = isr.Dapper.Entities.NomReadingType.None
        Dim NomReadingType As New NomReadingTypeEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = NomReadingTypeBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = NomReadingType.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.NomReadingTypeEntity)} With {NameOf(Entities.NomReadingTypeEntity.Id)} Of {id} should Not exist")
            Dim r As (Success As Boolean, Details As String)
            r = NomReadingType.ValidateNewEntity($"New {NameOf(Entities.NomReadingTypeEntity)} With {NameOf(Entities.NomReadingTypeEntity.Id)} Of {id}")
            Assert.IsTrue(r.Success, r.Details)
            NomReadingType.Clear()
            id = isr.Dapper.Entities.NomReadingType.Reading
            expectedFetched = True
            actualFetched = NomReadingType.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.NomReadingTypeEntity)} With {NameOf(Entities.NomReadingTypeEntity.Id)} Of {id}should exist")
            r = NomReadingType.ValidateStoredEntity($"Fetched {NameOf(Entities.NomReadingTypeEntity)} With {NameOf(Entities.NomReadingTypeEntity.Id)} Of {id}")
            Assert.IsTrue(r.Success, r.Details)

            NomReadingType.Label = isr.Dapper.Entities.NomReadingType.Reading.ToString
            actualFetched = NomReadingType.FetchUsingUniqueIndex(connection)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.NomReadingTypeEntity)} by {NameOf(Entities.NomReadingTypeEntity.Label)} Of {isr.Dapper.Entities.NomReadingType.Reading} should exist")
            r = NomReadingType.ValidateStoredEntity($"Fetched {NameOf(Entities.NomReadingTypeEntity)} by {NameOf(Entities.NomReadingTypeEntity.Label)} Of {isr.Dapper.Entities.NomReadingType.Reading}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = [Enum].GetValues(GetType(isr.Dapper.Entities.NomReadingType)).Length - 1
            Dim actualCount As Integer = NomReadingType.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"Expected {expectedCount} {NameOf(Entities.NomReadingTypeEntity)}s")
        End Using
    End Sub

    #End Region

    #Region " NUT "

    ''' <summary> Assert adding Nut entity. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">      The site. </param>
    ''' <param name="provider">  The provider. </param>
    ''' <param name="uutAutoId"> Identifier for the uut automatic. </param>
    ''' <param name="elementId"> Identifier for the element. </param>
    ''' <param name="nomTypeId"> Identifier for the <see cref="NomTypeEntity"/>. </param>
    ''' <returns> A <see cref="Dapper.Entities.NutEntity"/>. </returns>
    Public Shared Function AssertAddingNutEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                 ByVal uutAutoId As Integer, ByVal elementId As Integer, ByVal nomTypeId As Integer) As NutEntity
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {NutBuilder.TableName, UutNutBuilder.TableName})

        Dim id As Integer = 3
        Dim nut As NutEntity
        Dim UutNut As UutNutEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            UutNut = New UutNutEntity()
            r = UutNut.TryObtain(connection, uutAutoId, elementId, nomTypeId)
            Assert.IsTrue(r.Success, $"{NameOf(Entities.UutNutEntity)} with '{NameOf(Entities.UutNutEntity.UutAutoId)}' of {uutAutoId} should have been inserted; {r.Details}")

            r = UutNut.ValidateStoredEntity($"Insert {NameOf(Entities.UutNutEntity)} with '{NameOf(Entities.UutNutEntity.UutAutoId)}' of {uutAutoId}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualFetched As Boolean = False
            nut = UutNut.FetchNutEntity(connection)
            r = nut.ValidateStoredEntity($"Fetch {NameOf(Entities.NutEntity)} with '{NameOf(Entities.NutEntity.ElementAutoId)}' of {elementId}")
            Assert.IsTrue(r.Success, r.Details)

        End Using
        Return nut
    End Function

    ''' <summary> Assert adding Nut readings. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="element">  The element. </param>
    ''' <param name="uut">      The uut. </param>
    ''' <param name="nutId">    Identifier of the <see cref="Dapper.Entities.NutEntity"/>. </param>
    ''' <returns> A NutEntity. </returns>
    Public Shared Function AssertAddingNutReadings(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                   ByVal element As ElementEntity, ByVal uut As UutEntity, ByVal nutId As Integer) As NutEntity

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {NutBuilder.TableName,
                                                                                          NutNomReadingBuilder.TableName,
                                                                                          NomReadingTypeBuilder.TableName})
        Dim r As (Success As Boolean, Details As String)
        Dim nut As New NutEntity With {.AutoId = nutId}
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            nut.FetchUsingKey(connection)
            r = nut.ValidateStoredEntity($"Fetch {NameOf(Entities.NutEntity)} with '{NameOf(Entities.NutEntity.AutoId)}' of {nutId}")
            Assert.IsTrue(r.Success, r.Details)

            Dim nomTrait As NomTrait = element.NomTraitsCollection(New MeterNomTypeSelector(1, nut.NomTypeId)).NomTrait

            Dim nominalValue As Double = nomTrait.NominalValue.Value
            Dim guardedSpecificationRange As RangeR = nomTrait.GuardedSpecificationRange
            Dim overflowRange As RangeR = nomTrait.OverflowRange


            Dim expectedNomReadingCount As Integer = 0
            Dim nominalValueNomReadingType As NomReadingType = NomReadingType.Reading
            Dim reading As Double = nominalValue + 1
            Dim nutNomReadingEntity As New NutNomReadingEntity With {.NutAutoId = nutId, .NomReadingTypeId = nominalValueNomReadingType, .Amount = reading}
            nutNomReadingEntity.Insert(connection)
            r = nutNomReadingEntity.ValidateStoredEntity($"Insert {NameOf(Entities.NutNomReadingEntity)} with '{NameOf(Entities.NutNomReadingEntity.NomReadingTypeId)}' of {nominalValueNomReadingType}")
            Assert.IsTrue(r.Success, r.Details)
            expectedNomReadingCount += 1

            Dim statusNomReadingType As NomReadingType = NomReadingType.Status
            Dim status As Integer = 101
            nutNomReadingEntity = New NutNomReadingEntity With {.NutAutoId = nutId, .NomReadingTypeId = statusNomReadingType, .Amount = status}
            nutNomReadingEntity.Insert(connection)
            r = nutNomReadingEntity.ValidateStoredEntity($"Insert {NameOf(Entities.NutNomReadingEntity)} with '{NameOf(Entities.NutNomReadingEntity.NomReadingTypeId)}' of {statusNomReadingType}")
            Assert.IsTrue(r.Success, r.Details)
            expectedNomReadingCount += 1

            Dim binNumberNomReadingType As NomReadingType = NomReadingType.BinNumber
            Dim binNumber As Integer = 11
            nutNomReadingEntity = New NutNomReadingEntity With {.NutAutoId = nutId, .NomReadingTypeId = binNumberNomReadingType, .Amount = binNumber}
            nutNomReadingEntity.Insert(connection)
            r = nutNomReadingEntity.ValidateStoredEntity($"Insert {NameOf(Entities.NutNomReadingEntity)} with '{NameOf(Entities.NutNomReadingEntity.NomReadingTypeId)}' of {binNumberNomReadingType}")
            Assert.IsTrue(r.Success, r.Details)
            expectedNomReadingCount += 1

            uut.FetchTraits(connection)

            nut.InitializeNomReadings(uut, element)
            nut.FetchNomReadings(connection)
            Assert.AreEqual(expectedNomReadingCount, nut.NomReadings.Count, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} count should match")
            Assert.IsTrue(nut.NomReadings.NomReading.Reading.HasValue, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} {NameOf(Entities.NomReading.Reading)} should have a value")
            Assert.AreEqual(reading, nut.NomReadings.NomReading.Reading.Value, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} {NameOf(Entities.NomReading.Reading)} should match")
            Assert.IsTrue(nut.NomReadings.NomReading.Status.HasValue, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} {NameOf(Entities.NomReading.Status)} should have a value")
            Assert.AreEqual(status, nut.NomReadings.NomReading.Status, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} {NameOf(Entities.NomReading.Status)} should match")
            Assert.IsTrue(nut.NomReadings.NomReading.BinNumber.HasValue, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} {NameOf(Entities.NomReading.BinNumber)} should have a value")
            Assert.AreEqual(binNumber, nut.NomReadings.NomReading.BinNumber, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} {NameOf(Entities.NomReading.BinNumber)} should match")
        End Using
        Return nut
    End Function

    ''' <summary> Reading getter. </summary>
    ''' <remarks> David, 7/16/2020. </remarks>
    ''' <param name="nut">          The nut. </param>
    ''' <param name="element">      The element. </param>
    ''' <param name="multimeterId"> Identifier for the multimeter. </param>
    ''' <param name="bin">          The bin. </param>
    ''' <returns> A (Reading As Double, Status As Integer) </returns>
    Public Shared Function ReadingGetter(ByVal nut As NutEntity, ByVal element As Entities.ElementEntity,
                                         ByVal multimeterId As Integer, ByVal bin As ReadingBin) As (Reading As Double, Status As Integer)
        Dim rand As New Random(DateTime.Now.Millisecond)
        Dim nominalValue As Double
        Dim nomReadingValue As Double
        Dim nomReadingStatus As Integer = 0
        Dim guardedToleranceBand As RangeR
        Dim overflowRange As RangeR
        Dim nomTypeId As Integer = nut.NomTypeId
        Dim nomTrait As NomTrait = element.NomTraitsCollection(New MeterNomTypeSelector(multimeterId, nomTypeId)).NomTrait
        nominalValue = nomTrait.NominalValue.Value
        guardedToleranceBand = nomTrait.GuardedSpecificationRange
        overflowRange = nomTrait.OverflowRange
        Select Case bin
            Case ReadingBin.Good
                nomReadingStatus = 0
                nomReadingValue = guardedToleranceBand.Min + rand.NextDouble * guardedToleranceBand.Span
            Case ReadingBin.High
                nomReadingStatus = 1
                nomReadingValue = guardedToleranceBand.Max + rand.NextDouble * guardedToleranceBand.Span
            Case ReadingBin.Low
                nomReadingStatus = 4
                nomReadingValue = guardedToleranceBand.Min - rand.NextDouble * guardedToleranceBand.Span
            Case ReadingBin.Overflow
                nomReadingStatus = 8
                nomReadingValue = overflowRange.Min * (1 - 0.1 * rand.NextDouble)
        End Select
        Return (nomReadingValue, nomReadingStatus)
    End Function

    ''' <summary> Assert adding nut nominal readings. </summary>
    ''' <remarks> David, 2020-06-27. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="nut">      The nut. </param>
    ''' <param name="element">  The element. </param>
    ''' <param name="bin">      The bin. </param>
    ''' <returns> A NutEntity. </returns>
    Public Shared Function AssertAddingNutNomReadings(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                      ByVal nut As NutEntity, ByVal element As Entities.ElementEntity, ByVal bin As ReadingBin) As NutEntity
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {NutBuilder.TableName, NutNomReadingBuilder.TableName})
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim multimeterId As Integer = 1
            Dim expectedNomReadingCount As Integer = 0
            Dim nomReadingType As NomReadingType = NomReadingType.Reading
            Dim nomReadingValue As Double
            Dim nomReadingStatus As Integer = 0

            Dim reading As (Reading As Double, Status As Integer) = Auditor.ReadingGetter(nut, element, multimeterId, bin)
            nomReadingStatus = reading.Status
            nomReadingValue = reading.Reading

            nomReadingType = NomReadingType.Reading
            Dim nutNomReadingEntity As New NutNomReadingEntity With {.NutAutoId = nut.AutoId, .NomReadingTypeId = nomReadingType, .Amount = nomReadingValue}
            nutNomReadingEntity.Insert(connection)
            r = nutNomReadingEntity.ValidateStoredEntity($"Insert {NameOf(Entities.NutNomReadingEntity)} with '{NameOf(Entities.NutNomReadingEntity.NomReadingTypeId)}' of {nomReadingType}")
            Assert.IsTrue(r.Success, r.Details)
            If nut.NomReadings Is Nothing Then nut.FetchNomReadings(connection)
            nut.NomReadings.Add(nutNomReadingEntity)
            expectedNomReadingCount += 1

            nomReadingType = NomReadingType.Status
            nutNomReadingEntity = New NutNomReadingEntity With {.NutAutoId = nut.AutoId, .NomReadingTypeId = nomReadingType, .Amount = nomReadingStatus}
            nutNomReadingEntity.Insert(connection)
            r = nutNomReadingEntity.ValidateStoredEntity($"Insert {NameOf(Entities.NutNomReadingEntity)} with '{NameOf(Entities.NutNomReadingEntity.NomReadingTypeId)}' of {nomReadingType}")
            Assert.IsTrue(r.Success, r.Details)
            nut.NomReadings.Add(nutNomReadingEntity)
            expectedNomReadingCount += 1

            nomReadingType = NomReadingType.BinNumber
            nutNomReadingEntity = New NutNomReadingEntity With {.NutAutoId = nut.AutoId, .NomReadingTypeId = nomReadingType, .Amount = bin}
            nutNomReadingEntity.Insert(connection)
            r = nutNomReadingEntity.ValidateStoredEntity($"Insert {NameOf(Entities.NutNomReadingEntity)} with '{NameOf(Entities.NutNomReadingEntity.NomReadingTypeId)}' of {nomReadingType}")
            Assert.IsTrue(r.Success, r.Details)
            nut.NomReadings.Add(nutNomReadingEntity)
            expectedNomReadingCount += 1

            nut.FetchNomReadings(connection)
            Assert.AreEqual(expectedNomReadingCount, nut.NomReadings.Count, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} count should match")
            Assert.AreEqual(nomReadingValue, nut.NomReadings.NomReading.Reading, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} {NameOf(Entities.NomReading.Reading)} should match")
            Assert.IsTrue(nut.NomReadings.NomReading.Status.HasValue, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} {NameOf(Entities.NomReading.Status)} should have a value")
            Assert.AreEqual(nomReadingStatus, CInt(nut.NomReadings.NomReading.Status.Value), $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} {NameOf(Entities.NomReading.Status)} should match")
            Assert.IsTrue(nut.NomReadings.NomReading.BinNumber.HasValue, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} {NameOf(Entities.NomReading.BinNumber)} should have a value")
            Assert.AreEqual(CInt(bin), nut.NomReadings.NomReading.BinNumber.Value, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.NomReadings)} {NameOf(Entities.NomReading.BinNumber)} should match")
        End Using
        Return nut
    End Function

    ''' <summary> Assert storing Nut bin. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="nutId">    Identifier of the <see cref="Dapper.Entities.NutEntity"/>. </param>
    ''' <param name="binId">    Identifier for the bin. </param>
    ''' <returns> An NutReadingBinEntity. </returns>
    Public Shared Function AssertStoringNutBin(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                               ByVal nutId As Integer, ByVal binId As Integer) As NutReadingBinEntity
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {NutBuilder.TableName, NutReadingBinBuilder.TableName})
        Dim nutReadingBin As New NutReadingBinEntity With {.NutAutoId = nutId, .ReadingBinId = binId}
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            r = nutReadingBin.TryObtain(connection)
            Assert.IsTrue(r.Success, r.Details)
        End Using
        Return nutReadingBin
    End Function

    ''' <summary> Assert adding Nut Bins. </summary>
    ''' <remarks> David, 5/30/2020. </remarks>
    ''' <param name="site">        The site. </param>
    ''' <param name="provider">    The provider. </param>
    ''' <param name="nut">         The <see cref="Dapper.Entities.NutEntity"/>. </param>
    ''' <param name="element">     The element. </param>
    ''' <param name="expectedBin"> The expected bin. </param>
    ''' <returns> A <see cref="Dapper.Entities.NutEntity"/>. </returns>
    Public Shared Function AssertAddingNutBin(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                              ByVal nut As NutEntity, ByVal element As Entities.ElementEntity, ByVal expectedBin As ReadingBin) As NutReadingBinEntity
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {NutBuilder.TableName, NutNomReadingBuilder.TableName})

        Dim meterId As Integer = 1
        Dim reading As Double? = nut.NomReadings.NomReading.Reading
        Dim bin As ReadingBin
        Dim nomTrait As NomTrait = element.NomTraitsCollection(New MeterNomTypeSelector(meterId, nut.NomTypeId)).NomTrait
        bin = nomTrait.DoBin(reading)
        Assert.AreEqual(expectedBin, bin, $"[{NameOf(NutEntity)}] bin number of {element.Label} should match")
        Return Auditor.AssertStoringNutBin(site, provider, nut.AutoId, bin)
    End Function

    ''' <summary> Assert Nut entity. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="productNumber"> The product number. </param>
    ''' <param name="partNumber">    The part number. </param>
    ''' <param name="lotNumbers">    The lot numbers. </param>
    Public Shared Sub AssertNutEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                      ByVal productNumber As String, ByVal partNumber As String, ByVal lotNumbers As IEnumerable(Of String))

        Dim nutReadingBin As NutReadingBinEntity
        Dim nut As NutEntity
        Dim element As ElementEntity
        Dim uut As UutEntity
        Dim multimeterId As Integer = 1
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            connection.DeleteAll(Of PartNub)
            connection.DeleteAll(Of LotNub)
            connection.DeleteAll(Of UutNub)
            connection.DeleteAll(Of LotUutNub)
            connection.DeleteAll(Of UutProductSortNub)
            connection.DeleteAll(Of NutNub)
            connection.DeleteAll(Of ElementNub)

            Dim expectedElementCount As Integer = 4
            Dim product As ProductEntity = Auditor.AssertAddingProductEntity(site, provider, productNumber)
            Dim productElement As New ProductElementEntity With {.ProductAutoId = product.AutoId}
            productElement.AddElements(connection, productNumber)
            Dim actualCount As Integer = product.FetchElements(connection)
            Assert.AreEqual(expectedElementCount, actualCount, $"inserted {NameOf(Entities.ProductEntity)} {productNumber} {NameOf(Entities.ProductEntity.Elements)} count should match")

            Dim part As PartEntity = Auditor.AssertAddingPartEntity(site, provider, partNumber)

            Dim productPart As New ProductPartEntity With {.ProductAutoId = product.AutoId, .PartAutoId = part.AutoId}
            productPart.Obtain(connection)
            r = productPart.ValidateStoredEntity($"Obtained {NameOf(ProductPartEntity)} With {NameOf(ProductPartEntity.ProductAutoId)} of {product.AutoId} and {NameOf(ProductPartEntity.PartAutoId)} of {part.AutoId}")
            Assert.IsTrue(r.Success, r.Details)

            Auditor.AssertAddingElementTraits(site, provider, product, part)

            Dim lastUutNumber As Integer = 0
            Dim lotNumber As String = lotNumbers(0)
            uut = Auditor.AssertAddingUutEntity(site, provider, multimeterId, lotNumber, lastUutNumber)
            lastUutNumber = uut.UutNumber

            MultimeterEntity.TryFetchAll(connection)
            For Each elementInfo As ElementInfo In Entities.ProductElementEntity.SelectElementInfo(productNumber)
                element = product.Elements.Element(elementInfo.Label)
                expectedElementCount += 1
                nut = Auditor.AssertAddingNutEntity(site, provider, uut.AutoId, element.AutoId, elementInfo.NomTypeId)
                nut.InitializeNomReadings(uut, element)
                nut.FetchNomReadings(connection)
                nutReadingBin = Auditor.AssertStoringNutBin(site, provider, nut.AutoId, ReadingBin.Good)
                nut = Auditor.AssertAddingNutReadings(site, provider, element, uut, nut.AutoId)
            Next

            lotNumber = lotNumbers(1)
            lastUutNumber = 0
            uut = Auditor.AssertAddingUutEntity(site, provider, multimeterId, lotNumber, lastUutNumber)
            lastUutNumber = uut.UutNumber

            For Each elementInfo As ElementInfo In Entities.ProductElementEntity.SelectElementInfo(productNumber)
                element = product.Elements.Element(elementInfo.Label)
                expectedElementCount += 1
                nut = Auditor.AssertAddingNutEntity(site, provider, uut.AutoId, element.AutoId, elementInfo.NomTypeId)
                nut.InitializeNomReadings(uut, element)
                nut.FetchNomReadings(connection)
                nutReadingBin = Auditor.AssertStoringNutBin(site, provider, nut.AutoId, ReadingBin.Good)
                nut = Auditor.AssertAddingNutReadings(site, provider, element, uut, nut.AutoId)
            Next

            uut = Auditor.AssertAddingUutEntity(site, provider, multimeterId, lotNumber, lastUutNumber)
            lastUutNumber = uut.UutNumber

            For Each elementInfo As ElementInfo In Entities.ProductElementEntity.SelectElementInfo(productNumber)
                element = product.Elements.Element(elementInfo.Label)
                expectedElementCount += 1
                nut = Auditor.AssertAddingNutEntity(site, provider, uut.AutoId, element.AutoId, elementInfo.NomTypeId)
                nut.InitializeNomReadings(uut, element)
                nut.FetchNomReadings(connection)
                nutReadingBin = Auditor.AssertStoringNutBin(site, provider, nut.AutoId, ReadingBin.Good)
                nut = Auditor.AssertAddingNutReadings(site, provider, element, uut, nut.AutoId)
            Next

        End Using
    End Sub

    #End Region

    #Region " OHM METER SETUP TESTS "

    ''' <summary> Asserts Ohm Meter Setting lookup and setup. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertOhmMeterSetupEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {OhmMeterSettingLookupBuilder.TableName,
                                                            OhmMeterSetupBuilder.TableName, OhmMeterSettingBuilder.TableName})
        Dim ohmMeterSettingLookupId As Integer = 0
        Dim ohmMeterSettingsLookupEntity As New OhmMeterSettingLookupEntity
        Dim ohmMeterSettingEntity As New OhmMeterSettingEntity
        Dim ohmMeterSetupEntity As New OhmMeterSetupEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = ohmMeterSettingsLookupEntity.FetchUsingKey(connection, ohmMeterSettingLookupId)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {ohmMeterSettingLookupId} should not exist")
            r = ohmMeterSettingsLookupEntity.ValidateNewEntity($"New {NameOf(IOhmMeterSettingLookup)} with {NameOf(IOhmMeterSettingLookup.OhmMeterSettingId)} equals {ohmMeterSettingLookupId}")
            Assert.IsTrue(r.Success, r.Details)
            ohmMeterSettingsLookupEntity.Clear()
            Dim meterEntities As IEnumerable(Of MultimeterEntity) = MultimeterEntity.FetchAllEntities(connection, False)
            Dim expectedCount As Integer = 4
            Assert.AreEqual(expectedCount, meterEntities.Count, $"There should be enough {NameOf(MultimeterEntity)}s")
            Dim expectedToleranceCode As String = "A"
            Dim resistance As Double = 399
            Dim expectedExists As Boolean = True
            Dim actualExists As Boolean
            Dim actualCount As Integer

            ' clear the table
            If provider.TableExists(connection, LotOhmMeterSetupBuilder.TableName) Then connection.DeleteAll(Of LotOhmMeterSetupNub)()
            connection.DeleteAll(Of OhmMeterSetupNub)()

            Dim id As Integer = 1
            ohmMeterSettingEntity = New OhmMeterSettingEntity With {.Id = id}
            actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex(connection)
            Assert.IsTrue(actualExists, $"{NameOf(isr.Dapper.Entities.OhmMeterSettingEntity)} should exist for auto id = {id}")
            actualCount = OhmMeterSettingEntity.CountEntities(connection)
            expectedCount = 6 ' at least 6
            Assert.IsTrue(actualCount >= expectedCount, $"{NameOf(isr.Dapper.Entities.OhmMeterSettingEntity)} should have at least {expectedCount} records")

            connection.DeleteAll(Of OhmMeterSetupNub)()

            OhmMeterSettingLookupEntity.TryFetchAll(connection)
            For Each MultimeterEntity As MultimeterEntity In meterEntities
                ohmMeterSettingsLookupEntity = OhmMeterSettingLookupEntity.SelectOhmMeterSettingLookupEntity(expectedToleranceCode, MultimeterEntity.Id, resistance)
                ohmMeterSettingEntity = New OhmMeterSettingEntity With {.Id = ohmMeterSettingsLookupEntity.OhmMeterSettingId}
                actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex(connection)
                Assert.IsTrue(actualExists, $"{NameOf(ohmMeterSettingEntity)} with '{NameOf(ohmMeterSettingEntity.Id)}' equals {ohmMeterSettingEntity.Id} should exist")

                ohmMeterSetupEntity = New OhmMeterSetupEntity
                ohmMeterSetupEntity.Copy(ohmMeterSettingEntity)

                actualCount = OhmMeterSetupEntity.CountEntities(connection)
                expectedCount = actualCount
                expectedCount += 1
                actualExists = ohmMeterSetupEntity.Insert(connection)
                Assert.IsTrue(actualExists, $"A new {NameOf(Entities.OhmMeterSetupEntity)} should be stored for meter ['{NameOf(Entities.OhmMeterSettingLookupEntity.MultimeterId)}'] of [{MultimeterEntity.Id}]")
                actualCount = OhmMeterSetupEntity.CountEntities(connection)
                Assert.IsTrue(actualCount >= expectedCount, $"Resistance test condition should have at least {expectedCount} records")
            Next

        End Using
    End Sub

    ''' <summary> Assert Lot ohm meter setup entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">      The site. </param>
    ''' <param name="provider">  The provider. </param>
    ''' <param name="lotAutoId"> Identifier for the <see cref="LotEntity"/> identity. </param>
    Public Shared Sub AssertLotOhmMeterSetupEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal lotAutoId As Integer)

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {OhmMeterSettingLookupBuilder.TableName,
                                                            OhmMeterSetupBuilder.TableName, OhmMeterSettingBuilder.TableName, LotOhmMeterSetupBuilder.TableName})
        Dim ohmMeterSettingLookupId As Integer = 0
        Dim ohmMeterSettingLookup As New OhmMeterSettingLookupEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = ohmMeterSettingLookup.FetchUsingKey(connection, ohmMeterSettingLookupId)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {ohmMeterSettingLookupId} should not exist")
            r = ohmMeterSettingLookup.ValidateNewEntity($"New {NameOf(IOhmMeterSettingLookup)} with {NameOf(IOhmMeterSettingLookup.OhmMeterSettingId)} equals {ohmMeterSettingLookupId}")
            Assert.IsTrue(r.Success, r.Details)
            ohmMeterSettingLookup.Clear()
            Dim meterEntities As IEnumerable(Of MultimeterEntity) = MultimeterEntity.FetchAllEntities(connection, False)
            Dim expectedCount As Integer = 4
            Assert.AreEqual(expectedCount, meterEntities.Count, $"There should be enough meter entities")
            Dim expectedToleranceCode As String = "A"
            Dim resistance As Double = 399
            Dim ohmMeterSettingLookupNub As OhmMeterSettingLookupNub
            Dim expectedExists As Boolean = True
            Dim actualExists As Boolean
            Dim ohmMeterSettingEntity As New OhmMeterSettingEntity
            Dim ohmMeterSetupEntity As New OhmMeterSetupEntity
            Dim actualCount As Integer

            ' clear the table
            connection.DeleteAll(Of LotOhmMeterSetupNub)()
            connection.DeleteAll(Of OhmMeterSetupNub)()
            Dim id As Integer = 1
            ohmMeterSettingEntity = New OhmMeterSettingEntity With {.Id = id}
            actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex(connection)
            Assert.IsTrue(actualExists, $"{NameOf(isr.Dapper.Entities.OhmMeterSettingEntity)} should exist for auto id = {id}")
            actualCount = OhmMeterSettingEntity.CountEntities(connection)
            expectedCount = 6 ' at least 6
            Assert.IsTrue(actualCount >= expectedCount, $"{NameOf(isr.Dapper.Entities.OhmMeterSettingEntity)} should have at least {expectedCount} records")

            Dim LotOhmMeterSetupEntity As New LotOhmMeterSetupEntity
            expectedCount = 0
            actualCount = connection.CountEntities(LotOhmMeterSetupBuilder.TableName)
            Assert.AreEqual(expectedCount, actualCount, $"Lot ohm meter setup nub record count should match")

            For Each MultimeterEntity As MultimeterEntity In meterEntities
                ohmMeterSettingLookupNub = OhmMeterSettingLookupEntity.FetchNubs(connection, expectedToleranceCode, MultimeterEntity.Id, resistance).First
                ohmMeterSettingEntity = New OhmMeterSettingEntity With {.Id = ohmMeterSettingLookupNub.OhmMeterSettingId}
                actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex(connection)
                Assert.IsTrue(actualExists, $"ohm meter setting condition should exist for id = {ohmMeterSettingEntity.Id}")

                ohmMeterSetupEntity = New OhmMeterSetupEntity
                ohmMeterSetupEntity.Copy(ohmMeterSettingEntity)

                actualCount = OhmMeterSetupEntity.CountEntities(connection)
                expectedCount = actualCount
                expectedCount += 1
                actualExists = ohmMeterSetupEntity.Insert(connection)
                Assert.IsTrue(actualExists, $"A new resistance ohm meter setup condition should be stored for meter {MultimeterEntity.MultimeterModelId} number {MultimeterEntity.MultimeterNumber}")
                actualCount = OhmMeterSetupEntity.CountEntities(connection)
                Assert.IsTrue(actualCount >= expectedCount, $"Resistance ohm meter setup condition should have at least {expectedCount} records")

                LotOhmMeterSetupEntity = New LotOhmMeterSetupEntity With {.LotAutoId = lotAutoId,
                                                                                  .OhmMeterSetupAutoId = ohmMeterSetupEntity.AutoId,
                                                                                  .MultimeterId = MultimeterEntity.Id}
                actualExists = LotOhmMeterSetupEntity.Insert(connection)
                Assert.IsTrue(actualExists,
                              $"A new Lot ohm meter setup condition should be stored for meter {MultimeterEntity.MultimeterModelId} and Lot id {lotAutoId}")
                actualCount = LotOhmMeterSetupEntity.CountEntities(connection, lotAutoId)
                Assert.IsTrue(actualCount >= expectedCount, $"Lot ohm meter setup condition should have at least {expectedCount} records")

            Next
        End Using
    End Sub


    #End Region

    #Region " PART "

    ''' <summary> Assert adding part entity. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <returns> A PartEntity. </returns>
    Public Shared Function AssertAddingPartEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal partNumber As String) As PartEntity

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {PartBuilder.TableName})
        Dim part As New PartEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            part.PartNumber = partNumber
            part.Obtain(connection)
            Assert.AreEqual(partNumber, part.PartNumber, $"{NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} of {partNumber} must equals the saved number {part.PartNumber}")
            r = part.ValidateStoredEntity($"Obtained {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} of {partNumber}")
            Assert.IsTrue(r.Success, r.Details)
            part.FetchNamings(connection)
        End Using
        Return part
    End Function

    ''' <summary> Asserts part entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">        The site. </param>
    ''' <param name="provider">    The provider. </param>
    ''' <param name="partNumbers"> The part numbers. </param>
    Public Shared Sub AssertPartEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                       ByVal partNumbers As IEnumerable(Of String))

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {PartBuilder.TableName})
        Dim id As Integer = 3
        Dim part As New PartEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim expectedCount As Integer = 0
            connection.DeleteAll(Of PartNub)()
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = part.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.AutoId)} of {id} should not exist")
            r = part.ValidateNewEntity($"New {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.AutoId)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedUniqueLabel As Boolean = True
            Dim actualUniqueLabel As Boolean = PartBuilder.Get.UsingUniqueLabel(connection)
            Assert.AreEqual(expectedUniqueLabel, actualUniqueLabel, $"{NameOf(Entities.PartEntity)} '{NameOf(PartBuilder.UsingUniqueLabel)}' should match")

            Dim partNumber As String = partNumbers(0)
            part.PartNumber = partNumber
            Dim actualInserted As Boolean = part.Insert(connection)
            Assert.AreEqual(partNumber, part.PartNumber, $"{NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} of {partNumber} must equals the saved number {part.PartNumber}")
            r = part.ValidateStoredEntity($"Inserted {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} of {partNumber}")
            Assert.IsTrue(r.Success, r.Details)

            part.FetchUsingKey(connection)
            Dim expectedTimestamp As DateTime = DateTime.Now
            Dim actualTimeStamp As DateTime = part.Timestamp.ToLocalTime
            Asserts.Instance.AreEqual(expectedTimestamp, actualTimeStamp, TimeSpan.FromSeconds(10), "Expected timestamp")

            expectedCount += 1
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Part {part.PartNumber} should have been inserted")

            partNumber = partNumbers(1)
            part.PartNumber = partNumber
            r = part.ValidateChangedEntity($"Changed {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} to {partNumber}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualUpdated As Boolean = part.Update(connection, UpdateModes.Refetch)
            Assert.IsTrue(actualUpdated, $"Part {part.PartNumber} should be updated")

            actualUpdated = part.Update(connection, UpdateModes.Refetch)
            Assert.IsFalse(actualUpdated, $"Part {part.PartNumber} should not be updated")
            Assert.AreEqual(1, PartEntity.CountEntities(connection, part.PartNumber), $"Single part should be counted for {part.PartNumber}")
            Assert.IsTrue(PartEntity.IsExists(connection, part.PartNumber), $"Part {part.PartNumber} should {NameOf(isr.Dapper.Entities.PartEntity.IsExists)}")

            ' set part proxy dirty
            actualUpdated = part.Update(connection, UpdateModes.Refetch Or UpdateModes.ForceUpdate)
            Assert.IsTrue(actualUpdated, $"Part {part.PartNumber} should be updated after setting proxy dirty")

            actualUpdated = part.Update(connection)
            Assert.IsTrue(actualUpdated, $"Part {part.PartNumber} should be clean after update")

            partNumber = part.PartNumber
            Dim AutoId As Integer = part.AutoId
            actualFetched = part.Obtain(connection)
            expectedFetched = True
            Assert.AreEqual(expectedFetched, actualFetched, $"Part {part.PartNumber} should have been obtained")
            Assert.AreEqual(partNumber, part.PartNumber, $"Obtained Part number {part.PartNumber} should match")
            Assert.AreEqual(AutoId, part.AutoId, $"Obtained Part auto id {part.AutoId} should match")


            connection.DeleteAll(Of PartNub)()
            expectedCount = partNumbers.Count
            For i As Integer = 1 To expectedCount
                part = New PartEntity With {.PartNumber = partNumbers(i - 1)}
                actualInserted = part.Insert(connection)
                expectedInserted = True
                Assert.AreEqual(expectedInserted, actualInserted, $"Part {part.PartNumber} should have been inserted")
            Next
            Dim actualCount As Integer = part.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple parts should have been inserted")

            Dim part0 As PartEntity = part.Parts.First
            Assert.IsTrue(part0.IsClean, $"fetched part should be clean")
            partNumber = part0.PartNumber
            Dim actualDelete As Boolean = part0.Delete(connection)
            Dim expectedDelete As Boolean = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Part {partNumber} should be deleted")
            expectedCount -= 1
            actualCount = part.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"Count should match after deleting one part")

            part0 = part.Parts.First
            Assert.IsTrue(part0.IsClean, $"fetched part should be clean")
            actualDelete = PartEntity.Delete(connection, part0.AutoId)
            expectedDelete = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Part {part0.PartNumber} should be deleted")
            expectedCount -= 1
            actualCount = part.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"Count should match after deleting another part")
        End Using
    End Sub

    #End Region

    #Region " PART SPECIFICATION TYPE "

    ''' <summary> Assert part specification type entity. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertPartSpecificationTypeEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {PartNamingTypeBuilder.TableName})

        Dim id As Integer = 100
        Dim entity As New PartNamingTypeEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedFetched As Boolean = False
            r = entity.ValidateNewEntity($"{NameOf(Entities.PartNamingTypeEntity)} with '{NameOf(Entities.PartNamingTypeEntity.Id)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualFetched As Boolean = entity.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.PartNamingTypeEntity)} with '{NameOf(Entities.PartNamingTypeEntity.Id)}' of '{id}' should not exist")
            r = entity.ValidateNewEntity($"{NameOf(Entities.PartNamingTypeEntity)} with '{NameOf(Entities.PartNamingTypeEntity.Id)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)

            id = 1
            actualFetched = entity.FetchUsingKey(connection, id)
            Assert.IsTrue(actualFetched, $"{GetType(PartNamingTypeEntity).Name}.{NameOf(PartNamingTypeEntity.Id)}={id} should exist")
            r = entity.ValidateStoredEntity($"Existing {NameOf(Entities.PartNamingTypeEntity)} with '{NameOf(Entities.PartNamingTypeEntity.Id)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)

            Dim label As String = PartNamingType.TrackingCode.ToString
            entity.Label = label
            r = entity.ValidateChangedEntity($"Changed {NameOf(Entities.PartNamingTypeEntity)} with '{NameOf(Entities.PartNamingTypeEntity.Label)}' to '{label}'")
            Assert.IsTrue(r.Success, r.Details)

            actualFetched = entity.FetchUsingUniqueIndex(connection)
            Assert.IsTrue(actualFetched, $"{GetType(PartNamingTypeEntity).Name}.{NameOf(PartNamingTypeEntity.Label)}={label} should exist")
            Assert.AreEqual(label, entity.Label, $"{GetType(PartNamingTypeEntity).Name}.{NameOf(PartNamingTypeEntity.Label)} should match")
            r = entity.ValidateStoredEntity($"Fetched {NameOf(Entities.PartNamingTypeEntity)} with '{NameOf(Entities.PartNamingTypeEntity.Label)}' of '{label}'")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = 2
            Dim actualCount As Integer = entity.FetchAllEntities(connection)
            Assert.IsTrue(actualCount >= expectedCount, $"Expected at least {expectedCount} found {actualCount} {GetType(PartNamingTypeEntity).Name} records")
        End Using
    End Sub

    #End Region

    #Region " PART SPECIFICATION "

    ''' <summary> Assert adding part specifications. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="partAutoId"> Identifier for the part entity. </param>
    ''' <returns> A PartEntity. </returns>
    Public Shared Function AssertAddingPartSpecifications(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal partAutoId As Integer) As PartEntity

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {PartBuilder.TableName, PartNamingBuilder.TableName,
                                                            PartNamingTypeBuilder.TableName})
        Dim part As New PartEntity With {.AutoId = partAutoId}
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedElementCount As Integer = 0
            Dim familyCode As String = "PFC"
            Dim partSpecificationEntity As New PartNamingEntity With {.PartAutoId = partAutoId, .PartNamingTypeId = PartNamingType.FamilyCode, .Label = familyCode}
            partSpecificationEntity.Insert(connection)
            expectedElementCount += 1

            Dim productNumber As String = "D1206"
            partSpecificationEntity = New PartNamingEntity With {.PartAutoId = partAutoId, .PartNamingTypeId = PartNamingType.ProductNumber, .Label = productNumber}
            partSpecificationEntity.Insert(connection)
            expectedElementCount += 1

            part.FetchUsingKey(connection)
            r = part.ValidateStoredEntity($"Fetch {NameOf(Entities.PartEntity)} with '{NameOf(Entities.PartEntity.AutoId)}' of {partAutoId}")
            Assert.IsTrue(r.Success, r.Details)
            Assert.AreEqual(expectedElementCount, part.FetchNamings(connection), $"{NameOf(Entities.PartEntity)} {NameOf(Entities.PartEntity.Namings)} count should match")
            Assert.AreEqual(familyCode, part.Namings.PartNaming.FamilyCode, $"{NameOf(Entities.PartEntity)} {NameOf(Entities.PartEntity.Namings)} {NameOf(Entities.PartNaming.FamilyCode)} should match")
            Assert.AreEqual(productNumber, part.Namings.PartNaming.ProductNumber, $"{NameOf(Entities.PartEntity)} {NameOf(Entities.PartEntity.Namings)} {NameOf(Entities.PartNaming.ProductNumber)} should match")

            Dim modifiedFamilyCode As String = "SPC"
            part.Namings.PartNaming.FamilyCode = modifiedFamilyCode
            Assert.IsTrue(part.Namings.Upsert(connection), $"{NameOf(Entities.PartEntity)} {NameOf(Entities.PartEntity.Namings)} {NameOf(Entities.PartNaming.FamilyCode)} should be updated")
            Assert.AreEqual(expectedElementCount, part.FetchNamings(connection), $"{NameOf(Entities.PartEntity)} {NameOf(Entities.PartEntity.Namings)} count should match after second fetch")
            Assert.AreEqual(modifiedFamilyCode, part.Namings.PartNaming.FamilyCode, $"{NameOf(Entities.PartEntity)} {NameOf(Entities.PartEntity.Namings)} {NameOf(Entities.PartNaming.FamilyCode)} should match")

            Dim toleranceCode As String = "F"
            part.Namings.PartNaming.ToleranceCode = toleranceCode
            Assert.IsTrue(part.Namings.Upsert(connection), $"{NameOf(Entities.PartEntity)} {NameOf(Entities.PartEntity.Namings)} {NameOf(Entities.PartNaming.ToleranceCode)} should be updated")
            expectedElementCount += 1
            Assert.AreEqual(expectedElementCount, part.FetchNamings(connection), $"{NameOf(Entities.PartEntity)} {NameOf(Entities.PartEntity.Namings)} count should match after third fetch")
            Assert.AreEqual(toleranceCode, part.Namings.PartNaming.ToleranceCode, $"{NameOf(Entities.PartEntity)} {NameOf(Entities.PartEntity.Namings)} {NameOf(Entities.PartNaming.ToleranceCode)} should match")

        End Using
        Return part
    End Function

    ''' <summary> Assert part specifications. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertPartSpecifications(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            connection.DeleteAll(Of PartNub)
            connection.DeleteAll(Of PartNamingNub)
            Dim partNumber As String = "PFC-D1206LF-02-1001-3301-FB"
            Dim part As PartEntity = Auditor.AssertAddingPartEntity(site, provider, partNumber)
            part = Auditor.AssertAddingPartSpecifications(site, provider, part.AutoId)
            partNumber = "part2"
            part = Auditor.AssertAddingPartEntity(site, provider, partNumber)
            part = Auditor.AssertAddingPartSpecifications(site, provider, part.AutoId)
        End Using

    End Sub

    #End Region

    #Region " POLARITY "

    ''' <summary> Asserts <see cref="PolarityEntity"/> test conditions. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertPolarityEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Dim id As Integer = 3
        Dim polarity As New PolarityEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = PolarityBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = polarity.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.PolarityEntity)} With '{NameOf(Entities.PolarityEntity.Id)}' of '{id}' should not exist")
            r = polarity.ValidateNewEntity($"{NameOf(Entities.PolarityEntity)} with '{NameOf(Entities.PolarityEntity.Id)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)

            polarity.Clear()
            id = 0
            expectedFetched = True
            actualFetched = polarity.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.PolarityEntity)} with '{NameOf(Entities.PolarityEntity.Id)}' of '{id}' should exist")
            r = polarity.ValidateStoredEntity($"Fetched {NameOf(Entities.PolarityEntity)} with '{NameOf(Entities.PolarityEntity.Id)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)

            polarity.Label = isr.Dapper.Entities.Polarity.Negative.ToString
            actualFetched = polarity.FetchUsingUniqueIndex(connection)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.PolarityEntity)} with '{NameOf(Entities.PolarityEntity.Label)}' of {isr.Dapper.Entities.Polarity.Negative} should exist")
            r = polarity.ValidateStoredEntity($"Fetched {NameOf(Entities.PolarityEntity)} with '{NameOf(Entities.PolarityEntity.Label)}' of '{polarity.Label}'")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = 2
            Dim actualCount As Integer = polarity.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"Expected {expectedCount} Polarities")
        End Using
    End Sub

    #End Region

    #Region " PLATFORM "

    ''' <summary> Asserts Platform entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertPlatformEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Dim expectedPlatformLabel As String = Auditor.ServerComputer.Name
        Dim expectedTimeZoneName As String = TimeZone.CurrentTimeZone.StandardName
        Dim id As Integer = 4
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = PlatformBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of PlatformNub)()
            Dim platform As New PlatformEntity

            ' connection.DeleteAll(Of PlatformNub)()
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = platform.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched,
                            $"{NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.AutoId)} of {id} should not exist")
            r = platform.ValidateNewEntity($"{NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.AutoId)} of {id}")
            Assert.IsTrue(r.Success, r.Details)
            platform.Clear()

            Dim expectedInserted As Boolean = True
            Dim actualInserted As Boolean = False
            platform = New PlatformEntity With {.Label = expectedPlatformLabel, .TimezoneId = expectedTimeZoneName}
            actualInserted = platform.Insert(connection)
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} of {expectedPlatformLabel} should have been inserted")
            Assert.AreEqual(expectedTimeZoneName, platform.TimezoneId, $"{NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.TimezoneId)} should match")
            r = platform.ValidateStoredEntity($"Inserted {NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} of {expectedPlatformLabel}")
            Assert.IsTrue(r.Success, r.Details)

            platform.FetchUsingKey(connection)
            Dim expectedLocalTimestamp As DateTime = DateTime.Now
            Dim actualLocalTimeStamp As DateTime = platform.Timestamp.ToLocalTime
            Asserts.Instance.AreEqual(expectedLocalTimestamp, actualLocalTimeStamp, TimeSpan.FromSeconds(10), "Expected local time timestamp")

            Dim expectedLocaleTimeStamp As DateTimeOffset = actualLocalTimeStamp
            Dim actualLocaleTimeStamp As DateTimeOffset = platform.LocaleTimestamp
            Asserts.Instance.AreEqual(expectedLocaleTimeStamp, actualLocaleTimeStamp, TimeSpan.FromSeconds(0), "Expected locale time timestamp")

            Dim newLabel As String = "new label"
            platform.Label = newLabel
            r = platform.ValidateChangedEntity($"Inserted {NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} changed to {newLabel}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualUpdate As Boolean = platform.Update(connection, UpdateModes.Refetch)
            Dim expectedUpdate As Boolean = True
            Assert.AreEqual(expectedUpdate, actualUpdate,
                            $"Changed {NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} changed to {newLabel} should be updated")
            r = platform.ValidateStoredEntity($"Updated {NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} changed to {newLabel}")
            Assert.IsTrue(r.Success, r.Details)

            expectedUpdate = False
            actualUpdate = platform.Update(connection, UpdateModes.Refetch)
            Assert.AreEqual(expectedUpdate, actualUpdate,
                            $"Unchanged {NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} of {newLabel} should not be updated")
            r = platform.ValidateStoredEntity($"Stored {NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} of {newLabel}")
            Assert.IsTrue(r.Success, r.Details)

            platform.Label = expectedPlatformLabel
            actualUpdate = platform.Update(connection, UpdateModes.Refetch)
            Assert.AreEqual(expectedUpdate, actualUpdate,
                            $"Changed {NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} changed to {expectedPlatformLabel} should be updated")
            r = platform.ValidateStoredEntity($"Updated {NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} changed to {expectedPlatformLabel}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = 1
            For i As Integer = 1 To 2
                newLabel = $"Platform{i}"
                platform = New PlatformEntity With {.Label = newLabel, .TimezoneId = expectedTimeZoneName}
                actualInserted = platform.Insert(connection)
                expectedInserted = True
                Assert.AreEqual(expectedInserted, actualInserted, $"New {NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} of {newLabel} should have been inserted")
                expectedCount += 1
            Next

            Dim actualCount As Integer = platform.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple {NameOf(Entities.PlatformEntity)}'s should have been inserted")

            Dim platformLabel As String = platform.Label
            Dim actualDelete As Boolean = platform.Delete(connection)
            Dim expectedDelete As Boolean = True
            Assert.AreEqual(expectedDelete, actualDelete,
                            $"{NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} of {platformLabel} should be deleted")
            platform.Clear()

            ' get active Platform
            platform.Label = expectedPlatformLabel
            platform.Obtain(connection)
            Assert.AreEqual(expectedPlatformLabel, platform.Label,
                            $"{NameOf(Entities.PlatformEntity)} with {NameOf(Entities.PlatformEntity.Label)} of {expectedPlatformLabel}  should be the active Platform")
        End Using
    End Sub

    #End Region

    #Region " PRODUCT BINNING "

    ''' <summary> Assert adding product binning entities. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="product">  The product entity. </param>
    ''' <param name="elements"> The elements. </param>
    ''' <param name="sorts">    The sorts. </param>
    ''' <returns> A ProductUniqueBinningEntityCollection. </returns>
    Public Shared Function AssertAddingProductBinningEntities(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                              ByVal product As ProductEntity,
                                                              ByVal elements As ProductUniqueElementEntityCollection,
                                                              ByVal sorts As ProductUniqueProductSortEntityCollection) As ProductUniqueBinningEntityCollection
        Auditor.AssertTableExist(site, provider, ProductPartBuilder.TableName)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            product.FetchProductBinning(connection)
            product.ProductBinnings.Populate(connection, elements, sorts)
            Dim expectedCount As Integer = product.ProductBinnings.Count
            product.ProductBinnings.Clear()
            product.FetchProductBinning(connection)
            product.ProductBinnings.Populate(connection, elements, sorts)
            If expectedCount <> product.ProductBinnings.Count Then
                Throw New isr.Core.OperationFailedException($"{NameOf(Dapper.Entities.ProductUniqueBinningEntityCollection)} count {product.ProductBinnings.Count} should be {expectedCount} after upsert")
            End If
            Return product.ProductBinnings
        End Using
    End Function

    #End Region

    #Region " PRODUCT SORTS "

    ''' <summary> Assert adding product sort entities. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="product">  The product entity. </param>
    ''' <param name="type">     The type. </param>
    ''' <returns> A ProductUniqueProductSortEntityCollection. </returns>
    Public Shared Function AssertAddingProductSortEntities(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                           ByVal product As ProductEntity, ByVal type As Type) As ProductUniqueProductSortEntityCollection
        Auditor.AssertTableExist(site, provider, ProductPartBuilder.TableName)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            ' insert sort labels for this product
            ProductSortBuilder.Get.InsertSortLabels(connection, product.AutoId, type)
            Dim expectedCount As Integer = [Enum].GetValues(type).Length

            ' fetch the product sort and their traits if any.
            Dim actualCount As Integer = product.FetchProductSorts(connection)
            Assert.AreEqual(expectedCount, actualCount, $"[{NameOf(Entities.ProductEntity)}].[{NameOf(Entities.ProductEntity.ProductSorts)}] count should match")

            actualCount = connection.CountEntities(ProductSortBuilder.TableName)
            Assert.AreEqual(expectedCount, product.ProductSorts.Count, $"[{NameOf(ProductSortBuilder)}].[{ProductSortBuilder.TableName}] record count should match")

            expectedCount = product.ProductSorts.Count
            product.ProductSorts.Upsert(connection)
            actualCount = connection.CountEntities(ProductSortBuilder.TableName)
            Assert.AreEqual(expectedCount, actualCount, $"[{NameOf(ProductUniqueProductSortEntityCollection)}] count should match after second Upsert")

            expectedCount = product.ProductSorts.Count
            ProductSortBuilder.Get.InsertSortLabels(connection, product.AutoId, type)
            actualCount = connection.CountEntities(ProductSortBuilder.TableName)
            Assert.AreEqual(expectedCount, actualCount, $"{NameOf(ProductUniqueProductSortEntityCollection)} count should match after second insert of sort labels")

            Return product.ProductSorts
        End Using
    End Function

    ''' <summary> Assert adding product sort entities. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="site">                      The site. </param>
    ''' <param name="provider">                  The provider. </param>
    ''' <param name="product">                   The product entity. </param>
    ''' <param name="overflowFailureCountLimit"> The overflow failure count limit. </param>
    ''' <returns> A ProductUniqueProductSortEntityCollection. </returns>
    Public Shared Function AssertAddingProductSortEntities(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                           ByVal product As ProductEntity, overflowFailureCountLimit As Integer) As ProductUniqueProductSortEntityCollection
        Dim sorts As ProductUniqueProductSortEntityCollection
        Dim productSort As ProductSortEntity
        Dim sortLabel As String = Entities.ProductSort.Good.Description
        Auditor.AssertTableExist(site, provider, ProductPartBuilder.TableName)
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            sorts = Entities.Tests.Auditor.AssertAddingProductSortEntities(site, provider, product, GetType(Entities.ProductSort))
            product.AssignProductSortBuckets()

            ' validate the bucket traits
            sortLabel = Entities.ProductSort.Good.Description
            Dim digitalOutput As Integer = 0
            Dim ContinuousFailureCountLimit As Integer = 0
            Dim bucketBin As BucketBin
            ' validate
            For Each sortLabel In GetType(Entities.ProductSort).Descriptions
                productSort = sorts.ProductSort(sortLabel)
                r = productSort.ValidateStoredEntity($"{NameOf(ProductSortEntity)} with {NameOf(ProductSortEntity.Label)} of {sortLabel}")
                Assert.IsTrue(r.Success, r.Details)
                Assert.IsNotNull(productSort.Traits, $"[{NameOf(productSort.Traits)}] for {sortLabel} should not be null")

                bucketBin = CType(productSort.Traits.ProductSortTrait.BucketBin, BucketBin)
                digitalOutput = BucketBinEntity.DigitalOutputMapper(bucketBin)
                Assert.IsTrue(productSort.Traits.ProductSortTrait.DigitalOutput.HasValue, $"{NameOf(Dapper.Entities.ProductSortTrait.DigitalOutput)} for {sortLabel} should have a value")
                Assert.AreEqual(digitalOutput, productSort.Traits.ProductSortTrait.DigitalOutput.Value, $"{NameOf(Dapper.Entities.ProductSortTrait.DigitalOutput)} for {sortLabel} should match")

                ContinuousFailureCountLimit = BucketBinEntity.ContinuousFailureCountLimitMapper(bucketBin)

                Assert.IsTrue(productSort.Traits.ProductSortTrait.ContinuousFailureCountLimit.HasValue, $"{NameOf(Dapper.Entities.ProductSortTrait.ContinuousFailureCountLimit)} for {sortLabel} should have a value")
                Assert.AreEqual(ContinuousFailureCountLimit, productSort.Traits.ProductSortTrait.ContinuousFailureCountLimit.Value, $"{NameOf(Dapper.Entities.ProductSortTrait.ContinuousFailureCountLimit)} for {sortLabel} should match")
            Next

        End Using
        Return sorts
    End Function

    #End Region

    #Region " PRODUCT PART "

    ''' <summary> Assert adding product part entity. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="productAutoId"> Identifier for the product automatic. </param>
    ''' <param name="partAutoId">    Identifier for the part entity. </param>
    ''' <returns> A PartEntity. </returns>
    Public Shared Function AssertAddingProductPartEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                         ByVal productAutoId As Integer, ByVal partAutoId As Integer) As ProductPartEntity
        Auditor.AssertTableExist(site, provider, ProductPartBuilder.TableName)
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim productPart As New ProductPartEntity With {.PartAutoId = partAutoId, .ProductAutoId = productAutoId}
            productPart.Insert(connection)
            r = productPart.ValidateStoredEntity($"Inserted {NameOf(Entities.ProductPartEntity)} With [{NameOf(Entities.ProductPartEntity.PartAutoId)},{NameOf(Entities.ProductPartEntity.ProductAutoId)}] Of [{partAutoId},{productAutoId}]")
            Assert.IsTrue(r.Success, r.Details)
            Return productPart
        End Using
    End Function

    ''' <summary> Assert adding product part entity. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="productAutoId"> Identifier for the product automatic. </param>
    ''' <param name="partNumber">    The part number. </param>
    ''' <returns> A PartEntity. </returns>
    Public Shared Function AssertAddingProductPartEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                         ByVal productAutoId As Integer, ByVal partNumber As String) As ProductPartEntity
        Auditor.AssertTableExist(site, provider, PartBuilder.TableName)
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim part As New PartEntity With {.PartNumber = partNumber}
            Dim actualInserted As Boolean = part.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.PartEntity)}  With '{NameOf(Entities.PartEntity.PartNumber)}' of {part.PartNumber} should have been inserted")
            r = part.ValidateStoredEntity($"Insert {NameOf(Entities.PartEntity)} with '{NameOf(Entities.PartEntity.PartNumber)}' of {partNumber}")
            Assert.IsTrue(r.Success, r.Details)
            Dim productPart As New ProductPartEntity With {.PartAutoId = part.AutoId, .ProductAutoId = productAutoId}
            productPart.Insert(connection)
            r = productPart.ValidateStoredEntity($"Inserted {NameOf(Entities.ProductPartEntity)} with [{NameOf(Entities.ProductPartEntity.PartAutoId)},{NameOf(Entities.ProductPartEntity.ProductAutoId)}] of {part.AutoId},{productAutoId}]")
            Assert.IsTrue(r.Success, r.Details)
            Return productPart
        End Using
    End Function

    #End Region

    #Region " PRODUCT PRODUCT "

    ''' <summary> Assert adding product entity. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="productNumber"> The product number. </param>
    ''' <returns> A ProductEntity. </returns>
    Public Shared Function AssertAddingProductEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                     ByVal productNumber As String) As ProductEntity
        Auditor.AssertTableExist(site, provider, ProductBuilder.TableName)
        Dim product As New ProductEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            product = New ProductEntity With {.ProductNumber = productNumber}
            product.Obtain(connection)
            r = product.ValidateStoredEntity($"Insert {NameOf(Entities.ProductEntity)} with '{NameOf(Entities.ProductEntity.ProductNumber)}' of {productNumber}")
            Assert.IsTrue(r.Success, r.Details)
            product.FetchTraits(connection)
            product.FetchProductSorts(connection)
            product.FetchTexts(connection)
            Dim scanList As String = ProductEntity.SelectScanList(productNumber)
            If Not String.IsNullOrEmpty(scanList) Then
                product.Texts.ProductText.ScanList = scanList
                product.Texts.Upsert(connection)
            End If

            Dim expectedCount As Integer = 3
            Dim minimumCp As Double = 1.31
            Dim minimumCpk As Double = 1.32
            Dim minimumCpm As Double = 1.33
            product.Traits.ProductTrait.MinimumCp = minimumCp
            product.Traits.ProductTrait.MinimumCpk = minimumCpk
            product.Traits.ProductTrait.MinimumCpm = minimumCpm
            Assert.AreEqual(expectedCount, product.Traits.Count, $"[{NameOf(ProductEntity)}].[{NameOf(ProductEntity.Traits)}].[Count] should match")
            product.Traits.Upsert(connection)
            Assert.AreEqual(expectedCount, product.Traits.Count, $"[{NameOf(ProductEntity)}].[{NameOf(ProductEntity.Traits)}].[Count] should match after upsert")
            product.FetchTraits(connection)
            Assert.AreEqual(expectedCount, product.Traits.Count, $"[{NameOf(ProductEntity)}].[{NameOf(ProductEntity.Traits)}].[Count] should match after fetch")
            Assert.AreEqual(minimumCp, product.Traits.ProductTrait.MinimumCp, $"[{NameOf(ProductEntity)}].[{NameOf(ProductEntity.Traits)}].[{NameOf(ProductTrait.MinimumCp)}] should match after fetch")
            Assert.AreEqual(minimumCpk, product.Traits.ProductTrait.MinimumCpk, $"[{NameOf(ProductEntity)}].[{NameOf(ProductEntity.Traits)}].[{NameOf(ProductTrait.MinimumCpk)}] should match after fetch")
            Assert.AreEqual(minimumCpm, product.Traits.ProductTrait.MinimumCpm, $"[{NameOf(ProductEntity)}].[{NameOf(ProductEntity.Traits)}].[{NameOf(ProductTrait.MinimumCpm)}] should match after fetch")
        End Using
        Return product
    End Function

    #End Region

    #Region " PRODUCT UUT NUT "

    ''' <summary> Assert adding uut nut entities. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="elements"> The elements. </param>
    ''' <param name="uut">      The uut. </param>
    ''' <returns> An UutEntity. </returns>
    Public Shared Function AssertAddingUutNutEntities(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                      ByVal elements As ProductUniqueElementEntityCollection, ByVal uut As UutEntity) As UutEntity
        Dim nut As NutEntity
        Dim nomTypeid As Integer = NomTrait.SelectDefaultNomType(ElementType.Equation)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim expectedNutCount As Integer = 0
            Dim element As ElementEntity
            For Each element In elements
                For Each nomTypeEntity As NomTypeEntity In element.NomTypes
                    nut = Entities.Tests.Auditor.AssertAddingNutEntity(site, provider, uut.AutoId, element.AutoId, nomTypeEntity.Id)
                    expectedNutCount += 1
                Next
            Next
            Assert.AreEqual(expectedNutCount, uut.FetchNuts(connection), $"{NameOf(UutEntity.Nuts)} count should match")
            uut.InitializeNomReadings(elements)
            uut.FetchNomReadings(connection)
            For Each nut In uut.Nuts
                Assert.IsNotNull(nut.NomReadings, $"{NameOf(NutEntity.NomReadings)} should not be null")
            Next
            uut.UutState = If((uut.Nuts?.Any).GetValueOrDefault(False), UutState.Ready, UutState.Undefined)
        End Using
        Return uut
    End Function

    ''' <summary> Assert adding uut nut readings. </summary>
    ''' <remarks> David, 7/9/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="elements"> The elements. </param>
    ''' <param name="uut">      The uut. </param>
    ''' <param name="bins">     The bins. </param>
    ''' <returns> An UutEntity. </returns>
    Public Shared Function AssertAddingUutNutReadings(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                      ByVal elements As ProductUniqueElementEntityCollection, ByVal uut As UutEntity,
                                                      ByVal bins As IEnumerable(Of Entities.ReadingBin)) As UutEntity
        Dim nut As NutEntity
        Dim element As ElementEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Assert.AreEqual(bins.Count, uut.FetchNuts(connection), $"{NameOf(UutEntity.Nuts)} count should match")
            uut.InitializeNomReadings(elements)
            uut.FetchNomReadings(connection)
            Dim elementLables As IEnumerable(Of String) = Auditor.ElementLabels
            For elementIndex As Integer = 0 To bins.Count - 1
                element = elements.Element(elementLables(elementIndex))
                nut = uut.Nuts.Nut(element.AutoId, element.NomTypes.First.NomType)
                nut = Entities.Tests.Auditor.AssertAddingNutNomReadings(site, provider, nut, element, bins(elementIndex))
            Next
        End Using
        Return uut
    End Function

    ''' <summary> Assert adding uut nut bins. </summary>
    ''' <remarks> David, 7/9/2020. </remarks>
    ''' <param name="site">             The site. </param>
    ''' <param name="provider">         The provider. </param>
    ''' <param name="elements">         The elements. </param>
    ''' <param name="uut">              The uut. </param>
    ''' <param name="expectedNutCount"> Number of expected nuts. </param>
    ''' <param name="bins">             The bins. </param>
    ''' <returns> An UutEntity. </returns>
    Public Shared Function AssertAddingUutNutBins(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                  ByVal elements As ProductUniqueElementEntityCollection, ByVal uut As UutEntity, ByVal expectedNutCount As Integer,
                                                  bins() As Entities.ReadingBin) As UutEntity
        Dim nut As NutEntity
        Dim element As ElementEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim binIndex As Integer = 0
            Assert.AreEqual(expectedNutCount, uut.FetchNuts(connection), $"{NameOf(UutEntity.Nuts)} count should match")
            uut.InitializeNomReadings(elements)
            uut.FetchNomReadings(connection)
            Dim elementLables As IEnumerable(Of String) = Auditor.ElementLabels
            Dim productNumber As String = "D1206LF"
            For Each elementInfo As ElementInfo In Entities.ProductElementEntity.SelectElementInfo(productNumber)
                element = elements.Element(elementInfo.Label)
                nut = uut.Nuts.Nut(element.AutoId, elementInfo.NomTypeId)
                Dim binId As Integer = Entities.Tests.Auditor.AssertAddingNutBin(site, provider, nut, element, bins(binIndex)).ReadingBinId
                ' uut.Nuts.UpaddReadingBin(nut, ReadingBinEntity.ReadingBins(bins(binIndex)))
                uut.Nuts.UpaddReadingBin(nut, ReadingBinEntity.EntityLookupDictionary(binId))
                binIndex += 1
            Next
        End Using
        uut.UutState = UutState.Measured
        Return uut
    End Function

    #End Region

    #Region " PRODUCT ELEMENTS "

    ''' <summary> Assert adding product element entities and element traits. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="product">  The product entity. </param>
    ''' <param name="part">     The part. </param>
    ''' <returns> An ElementEntity. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Shared Function AssertAddingProductElementEntities(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                              ByVal product As ProductEntity, ByVal part As PartEntity) As Integer
        Dim element As ElementEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim productNumber As String = product.ProductNumber
            Dim productElement As New ProductElementEntity With {.ProductAutoId = product.AutoId}
            productElement.AddElements(connection, productNumber)
            product.FetchElements(connection)
            part.FetchElements(connection, New Integer() {1})
            For Each element In product.Elements
                isr.Dapper.Entities.Tests.Auditor.AssertAddingElementNomTraits(site, provider, part, element)
            Next
            Return product.Elements.Count
        End Using

    End Function

    ''' <summary> Assert adding element traits. </summary>
    ''' <remarks> David, 6/30/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="product">  The product entity. </param>
    ''' <param name="part">     The part. </param>
    ''' <returns> An Integer. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Shared Function AssertAddingElementTraits(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                         ByVal product As ProductEntity, ByVal part As PartEntity) As Integer
        Dim element As ElementEntity
        Dim multimeterIdentities As Integer() = New Integer() {1}
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            product.FetchElements(connection)
            part.FetchElements(connection, multimeterIdentities)
            For Each element In product.Elements
                isr.Dapper.Entities.Tests.Auditor.AssertAddingElementNomTraits(site, provider, part, element)
            Next
            Return product.Elements.Count
        End Using

    End Function

    #End Region

    #Region " PRODUCT UUT SORT "

    ''' <summary> Asset insert uut sort. </summary>
    ''' <remarks> David, 5/28/2020. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="uut">           The <see cref="UutEntity"/>. </param>
    ''' <param name="productSortId"> Identifier for the product sort. </param>
    ''' <returns> An UutProductSortEntity. </returns>
    Public Shared Function AssetInsertUutSort(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                              ByVal uut As UutEntity, ByVal productSortId As Integer) As UutProductSortEntity
        Dim uutSort As UutProductSortEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            uutSort = New UutProductSortEntity() With {.UutAutoId = uut.AutoId, .ProductSortAutoId = productSortId}
            uutSort.Insert(connection)
            r = uutSort.ValidateStoredEntity($"{NameOf(UutProductSortEntity)}")
            Assert.IsTrue(r.Success, r.Details)
        End Using
        uut.UutState = Dapper.Entities.UutState.Complete
        Return uutSort
    End Function

    #End Region

    #Region " PRODUCT  "

    ''' <summary> Assert product entity. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="site">               The site. </param>
    ''' <param name="provider">           The provider. </param>
    ''' <param name="productTestMonitor"> The product test monitor. </param>
    Public Shared Sub AssertProductEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                          ByVal productTestMonitor As SessionMonitor)

        Dim elements As Entities.ProductUniqueElementEntityCollection
        Dim sorts As ProductUniqueProductSortEntityCollection
        Dim uut As UutEntity
        Dim binnings As Entities.ProductUniqueBinningEntityCollection
        Dim productSort As ProductSortEntity
        Dim uutSort As UutProductSortEntity
        Dim uutMonitor As UutMonitor = productTestMonitor.UutMonitor
        Dim multimeterId As Integer = 1
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim expectedCount As Integer = 0
            Dim lotAutoId As Integer = productTestMonitor.Lot.AutoId
            elements = productTestMonitor.Elements
            sorts = productTestMonitor.Sorts
            binnings = productTestMonitor.Binnings

            Dim expectedLastUutNumber As Integer = 0
            uutMonitor.PrepareNextUut(connection, elements, lotAutoId, UutType.Chip)
            Assert.AreEqual(expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match")
            uut = uutMonitor.ActiveUUTEntity
            Assert.AreEqual(UutState.Ready, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedLastUutNumber = uut.UutNumber

            ' ALL GOOD
            Dim bins As Entities.ReadingBin() = New Entities.ReadingBin() {Entities.ReadingBin.Good, Entities.ReadingBin.Good,
                                                                           Entities.ReadingBin.Good, Entities.ReadingBin.Good}
            uut = Entities.Tests.Auditor.AssertAddingUutNutReadings(site, provider, elements, uut, bins)
            uut = Entities.Tests.Auditor.AssertAddingUutNutBins(site, provider, elements, uut, uut.Nuts.Count, bins)

            Dim expectedSort As ProductSort = Entities.ProductSort.Good
            Dim expectedSortLabel As String = expectedSort.Description
            Dim expectedSortEntity As ProductSortEntity = sorts.ProductSort(expectedSortLabel)
            productSort = uutMonitor.SortUut(binnings, sorts)
            Assert.AreEqual(expectedSortLabel, productSort.Label, $"{NameOf(Entities.ProductSortEntity)} {NameOf(Entities.ProductSortEntity.Label)} should match")
            Assert.AreEqual(UutState.Present, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            uutSort = Auditor.AssetInsertUutSort(site, provider, uut, productSort.AutoId)
            Assert.AreEqual(UutState.Complete, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")

            Dim expectedContinuousFailureAlertCount As Integer = 0
            Dim expectedFailureCountAlert As Boolean = False
            Dim expectedFailureCount As Integer = 0
            Assert.AreEqual(expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.LastSortEntity)}].[{NameOf(ProductSortEntity.AutoId)}] should match")
            Assert.AreEqual(expectedFailureCount, uutMonitor.ContinuousFailureCount,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.ContinuousFailureCount)}] should match")
            Assert.AreEqual(expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailureAlert)}] should match")
            Assert.AreEqual(expectedContinuousFailureAlertCount, uutMonitor.ContinuousFailuresStack.Count,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailuresStack)}].Count should match")

            ' R1 OVERFLOW 
            uutMonitor.PrepareNextUut(connection, elements, lotAutoId, UutType.Chip)
            Assert.AreEqual(expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match")
            uut = uutMonitor.ActiveUUTEntity
            Assert.AreEqual(UutState.Ready, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedLastUutNumber = uut.UutNumber
            bins = New Entities.ReadingBin() {Entities.ReadingBin.Overflow, Entities.ReadingBin.Good, Entities.ReadingBin.Good, Entities.ReadingBin.Good}
            uut = Entities.Tests.Auditor.AssertAddingUutNutReadings(site, provider, elements, uut, bins)
            uut = Entities.Tests.Auditor.AssertAddingUutNutBins(site, provider, elements, uut, uut.Nuts.Count, bins)
            expectedSort = Entities.ProductSort.ReadingOverflow
            expectedSortLabel = expectedSort.Description
            expectedSortEntity = sorts.ProductSort(expectedSortLabel)

            ' override Failure count limit for testing
            expectedSortEntity.Traits.ProductSortTrait.ContinuousFailureCountLimit = 2

            productSort = uutMonitor.SortUut(binnings, sorts)
            Assert.AreEqual(expectedSortLabel, productSort.Label, $"{NameOf(Entities.ProductSortEntity)} {NameOf(Entities.ProductSortEntity.Label)} should match")
            Assert.AreEqual(UutState.Absent, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            uutSort = Auditor.AssetInsertUutSort(site, provider, uut, productSort.AutoId)
            Assert.AreEqual(UutState.Complete, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedFailureCountAlert = False
            expectedFailureCount += 1
            Assert.AreEqual(expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.LastSortEntity)}].[{NameOf(ProductSortEntity.AutoId)}] should match")
            Assert.AreEqual(expectedFailureCount, uutMonitor.ContinuousFailureCount,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.ContinuousFailureCount)}] should match")
            Assert.AreEqual(expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailureAlert)}] should match")
            Assert.AreEqual(expectedContinuousFailureAlertCount, uutMonitor.ContinuousFailuresStack.Count,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailuresStack)}].Count should match")

            ' R2 OVERFLOW 
            uutMonitor.PrepareNextUut(connection, elements, lotAutoId, UutType.Chip)
            Assert.AreEqual(expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match")
            uut = uutMonitor.ActiveUUTEntity
            Assert.AreEqual(UutState.Ready, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedLastUutNumber = uut.UutNumber
            bins = New Entities.ReadingBin() {Entities.ReadingBin.Good, Entities.ReadingBin.Overflow, Entities.ReadingBin.Good, Entities.ReadingBin.Good}
            uut = Entities.Tests.Auditor.AssertAddingUutNutReadings(site, provider, elements, uut, bins)
            uut = Entities.Tests.Auditor.AssertAddingUutNutBins(site, provider, elements, uut, uut.Nuts.Count, bins)
            expectedSort = Entities.ProductSort.ReadingOverflow
            expectedSortLabel = expectedSort.Description
            expectedSortEntity = sorts.ProductSort(expectedSortLabel)
            productSort = uutMonitor.SortUut(binnings, sorts)
            Assert.AreEqual(UutState.Absent, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            Assert.AreEqual(expectedSortLabel, productSort.Label, $"{NameOf(Entities.ProductSortEntity)} {NameOf(Entities.ProductSortEntity.Label)} should match")
            uutSort = Auditor.AssetInsertUutSort(site, provider, uut, productSort.AutoId)
            Assert.AreEqual(UutState.Complete, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedFailureCountAlert = True
            expectedFailureCount += 1
            expectedContinuousFailureAlertCount += 1
            Assert.AreEqual(expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.LastSortEntity)}].[{NameOf(ProductSortEntity.AutoId)}] should match")
            Assert.AreEqual(expectedFailureCount, uutMonitor.ContinuousFailureCount,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.ContinuousFailureCount)}] should match")
            Assert.AreEqual(expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailureAlert)}] should match")
            Assert.AreEqual(expectedContinuousFailureAlertCount, uutMonitor.ContinuousFailuresStack.Count,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailuresStack)}].Count should match")

            ' R2 LOW
            uutMonitor.PrepareNextUut(connection, elements, lotAutoId, UutType.Chip)
            Assert.AreEqual(expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match")
            uut = uutMonitor.ActiveUUTEntity
            Assert.AreEqual(UutState.Ready, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedLastUutNumber = uut.UutNumber
            bins = New Entities.ReadingBin() {Entities.ReadingBin.Good, Entities.ReadingBin.Low, Entities.ReadingBin.Good, Entities.ReadingBin.Good}
            uut = Entities.Tests.Auditor.AssertAddingUutNutReadings(site, provider, elements, uut, bins)
            uut = Entities.Tests.Auditor.AssertAddingUutNutBins(site, provider, elements, uut, uut.Nuts.Count, bins)
            expectedSort = Entities.ProductSort.ResistorGuardFailed
            expectedSortLabel = expectedSort.Description
            expectedSortEntity = sorts.ProductSort(expectedSortLabel)
            productSort = uutMonitor.SortUut(binnings, sorts)
            Assert.AreEqual(UutState.Present, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            Assert.AreEqual(expectedSortLabel, productSort.Label, $"{NameOf(Entities.ProductSortEntity)} {NameOf(Entities.ProductSortEntity.Label)} should match")
            uutSort = Auditor.AssetInsertUutSort(site, provider, uut, productSort.AutoId)
            Assert.AreEqual(UutState.Complete, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedFailureCountAlert = False
            expectedFailureCount = 1
            Assert.AreEqual(expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.LastSortEntity)}].[{NameOf(ProductSortEntity.AutoId)}] should match")
            Assert.AreEqual(expectedFailureCount, uutMonitor.ContinuousFailureCount,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.ContinuousFailureCount)}] should match")
            Assert.AreEqual(expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailureAlert)}] should match")
            Assert.AreEqual(expectedContinuousFailureAlertCount, uutMonitor.ContinuousFailuresStack.Count,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailuresStack)}].Count should match")

            ' D1 HIGH
            uutMonitor.PrepareNextUut(connection, elements, lotAutoId, UutType.Chip)
            Assert.AreEqual(expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match")
            uut = uutMonitor.ActiveUUTEntity
            Assert.AreEqual(UutState.Ready, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedLastUutNumber = uut.UutNumber
            bins = New Entities.ReadingBin() {Entities.ReadingBin.Good, Entities.ReadingBin.Good, Entities.ReadingBin.High, Entities.ReadingBin.Good}
            uut = Entities.Tests.Auditor.AssertAddingUutNutReadings(site, provider, elements, uut, bins)
            uut = Entities.Tests.Auditor.AssertAddingUutNutBins(site, provider, elements, uut, uut.Nuts.Count, bins)
            expectedSort = Entities.ProductSort.CompoundResistorGuardFailed
            expectedSortLabel = expectedSort.Description
            expectedSortEntity = sorts.ProductSort(expectedSortLabel)
            productSort = uutMonitor.SortUut(binnings, sorts)
            Assert.AreEqual(UutState.Present, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            Assert.AreEqual(expectedSortLabel, productSort.Label, $"{NameOf(Entities.ProductSortEntity)} {NameOf(Entities.ProductSortEntity.Label)} should match")
            uutSort = Auditor.AssetInsertUutSort(site, provider, uut, productSort.AutoId)
            Assert.AreEqual(UutState.Complete, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedFailureCountAlert = False
            expectedFailureCount = 1
            Assert.AreEqual(expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.LastSortEntity)}].[{NameOf(ProductSortEntity.AutoId)}] should match")
            Assert.AreEqual(expectedFailureCount, uutMonitor.ContinuousFailureCount,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.ContinuousFailureCount)}] should match")
            Assert.AreEqual(expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailureAlert)}] should match")
            Assert.AreEqual(expectedContinuousFailureAlertCount, uutMonitor.ContinuousFailuresStack.Count,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailuresStack)}].Count should match")

            ' M1 LOW
            uutMonitor.PrepareNextUut(connection, elements, lotAutoId, UutType.Chip)
            Assert.AreEqual(expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match")
            uut = uutMonitor.ActiveUUTEntity
            Assert.AreEqual(UutState.Ready, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedLastUutNumber = uut.UutNumber
            bins = New Entities.ReadingBin() {Entities.ReadingBin.Good, Entities.ReadingBin.Good, Entities.ReadingBin.Good, Entities.ReadingBin.Low}
            uut = Entities.Tests.Auditor.AssertAddingUutNutReadings(site, provider, elements, uut, bins)
            uut = Entities.Tests.Auditor.AssertAddingUutNutBins(site, provider, elements, uut, uut.Nuts.Count, bins)
            expectedSort = Entities.ProductSort.ComputedValueGuardFailed
            expectedSortLabel = expectedSort.Description
            expectedSortEntity = sorts.ProductSort(expectedSortLabel)
            productSort = uutMonitor.SortUut(binnings, sorts)
            Assert.AreEqual(UutState.Present, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            Assert.AreEqual(expectedSortLabel, productSort.Label, $"{NameOf(Entities.ProductSortEntity)} {NameOf(Entities.ProductSortEntity.Label)} should match")
            uutSort = Auditor.AssetInsertUutSort(site, provider, uut, productSort.AutoId)
            Assert.AreEqual(UutState.Complete, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedFailureCountAlert = False
            expectedFailureCount = 1
            Assert.AreEqual(expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.LastSortEntity)}].[{NameOf(ProductSortEntity.AutoId)}] should match")
            Assert.AreEqual(expectedFailureCount, uutMonitor.ContinuousFailureCount,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.ContinuousFailureCount)}] should match")
            Assert.AreEqual(expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailureAlert)}] should match")
            Assert.AreEqual(expectedContinuousFailureAlertCount, uutMonitor.ContinuousFailuresStack.Count,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailuresStack)}].Count should match")

            ' R1 HIGH M1 LOW
            uutMonitor.PrepareNextUut(connection, elements, lotAutoId, UutType.Chip)
            Assert.AreEqual(expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match")
            uut = uutMonitor.ActiveUUTEntity
            Assert.AreEqual(UutState.Ready, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedLastUutNumber = uut.UutNumber
            bins = New Entities.ReadingBin() {Entities.ReadingBin.High, Entities.ReadingBin.Good, Entities.ReadingBin.Good, Entities.ReadingBin.Low}
            uut = Entities.Tests.Auditor.AssertAddingUutNutReadings(site, provider, elements, uut, bins)
            uut = Entities.Tests.Auditor.AssertAddingUutNutBins(site, provider, elements, uut, uut.Nuts.Count, bins)
            expectedSort = Entities.ProductSort.ResistorGuardFailed
            expectedSortLabel = expectedSort.Description
            expectedSortEntity = sorts.ProductSort(expectedSortLabel)
            productSort = uutMonitor.SortUut(binnings, sorts)
            Assert.AreEqual(UutState.Present, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            Assert.AreEqual(expectedSortLabel, productSort.Label, $"{NameOf(Entities.ProductSortEntity)} {NameOf(Entities.ProductSortEntity.Label)} should match")
            uutSort = Auditor.AssetInsertUutSort(site, provider, uut, productSort.AutoId)
            Assert.AreEqual(UutState.Complete, uut.UutState, $"{NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutState)} should match")
            expectedFailureCountAlert = False
            expectedFailureCount = 1
            Assert.AreEqual(expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.LastSortEntity)}].[{NameOf(ProductSortEntity.AutoId)}] should match")
            Assert.AreEqual(expectedFailureCount, uutMonitor.ContinuousFailureCount,
                            $"[{NameOf(LotEntity)}].[{NameOf(Entities.UutMonitor.ContinuousFailureCount)}] should match")
            Assert.AreEqual(expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailureAlert)}] should match")
            Assert.AreEqual(expectedContinuousFailureAlertCount, uutMonitor.ContinuousFailuresStack.Count,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailuresStack)}].Count should match")

            expectedContinuousFailureAlertCount = 0
            uutMonitor.ClearContinuousFailures()
            Assert.AreEqual(expectedContinuousFailureAlertCount, uutMonitor.ContinuousFailuresStack.Count,
                            $"[{NameOf(LotEntity)}][{NameOf(Entities.UutMonitor.ContinuousFailuresStack)}].Count should match after clear")

            Dim expectedDeleteCount As Integer = 0
            Dim actualDeleteCount As Integer = UutProductSortEntity.DeleteUnsortedUuts(connection, productTestMonitor.Lot)
            Assert.AreEqual(expectedDeleteCount, actualDeleteCount,
                            $"Unsorted [{NameOf(UutEntity)}] count should match after clear")

        End Using
    End Sub

    ''' <summary> Assert product entity. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="productNumber"> The product number. </param>
    ''' <param name="partNumber">    The part number. </param>
    ''' <param name="lotNumber">     The lot number. </param>
    Public Shared Sub AssertProductEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                          ByVal productNumber As String, ByVal partNumber As String, ByVal lotNumber As String)

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider,
                   New String() {PartBuilder.TableName, Entities.ProductBuilder.TableName, Entities.ProductPartBuilder.TableName, Entities.LotBuilder.TableName})
        Dim productTestMonitor As SessionMonitor
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim expectedCount As Integer = 0
            connection.DeleteAll(Of PartNub)()
            connection.ReseedTableIdentity(PartBuilder.TableName)
            connection.DeleteAll(Of ProductNub)()
            connection.ReseedTableIdentity(ProductBuilder.TableName)
            connection.DeleteAll(Of ProductPartNub)()
            connection.DeleteAll(Of LotNub)()
            connection.ReseedTableIdentity(LotBuilder.TableName)
            Dim meterNUmber As Integer = 1
            Dim multimeterId As Integer = 1
            productTestMonitor = Auditor.AssetAddingSessionMonitor(site, provider, productNumber, partNumber, lotNumber, multimeterId, meterNUmber)
            Auditor.AssertProductEntity(site, provider, productTestMonitor)
        End Using
    End Sub

    #End Region

    #Region " READING BIN ENTITY "

    ''' <summary> Assert Reading bin entity. </summary>
    ''' <remarks> David, 3/30/2020. </remarks>
    ''' <param name="site">               The site. </param>
    ''' <param name="provider">           The provider. </param>
    ''' <param name="goodBinNumber">      The good bin number. </param>
    ''' <param name="goodBinNumberLabel"> The good bin number label. </param>
    ''' <param name="binCount">           Number of bins. </param>
    Public Shared Sub AssertReadingBinEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                             ByVal goodBinNumber As Integer, ByVal goodBinNumberLabel As String, ByVal binCount As Integer)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = ReadingBinBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")

            Dim binEntity As New ReadingBinEntity
            binEntity.FetchUsingKey(connection, goodBinNumber)

            Dim expectedLabel As String = goodBinNumberLabel
            Assert.AreEqual(expectedLabel, binEntity.Label, $"{NameOf(ReadingBinEntity)}.{NameOf(ReadingBinEntity.Label)}")

            Dim expectedCount As Integer = binCount
            Dim count As Integer = binEntity.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, count, $"{NameOf(ReadingBinNub)} entity correct number Of {NameOf(ReadingBinNub)}")
        End Using
    End Sub

    ''' <summary> Assert Reading bin entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="goodBinNumber"> The good bin number. </param>
    Public Shared Sub AssertReadingBinEntity(ByVal site As TestSiteBase, ByVal provider As ProviderBase, ByVal goodBinNumber As ReadingBin)
        Auditor.AssertReadingBinEntity(site, provider, CInt(goodBinNumber), goodBinNumber.ToString, [Enum].GetValues(GetType(ReadingBin)).Length)
    End Sub

    ''' <summary> Assert Reading bin entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="goodBinNumber"> The good bin number. </param>
    Public Shared Sub AssertReadingBinEntity(ByVal site As TestSiteBase, ByVal provider As ProviderBase, ByVal goodBinNumber As QualityReadingBin)
        Auditor.AssertReadingBinEntity(site, provider, CInt(goodBinNumber), goodBinNumber.ToString, [Enum].GetValues(GetType(QualityReadingBin)).Length)
    End Sub

    #End Region

    #Region " REVISION "

    ''' <summary> Assert update revision entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertUpdateRevisionEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {RevisionBuilder.TableName})

        Dim timestamp As DateTime
        ' timestamp = DateTime.Parse("2020-06-22 03:22:00.700")
        timestamp = DateTime.Parse("2020-06-22 14:43:01.100")
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Entities.RevisionBuilder.Get.UpdateTimestamp(connection, 2, timestamp)
        End Using
    End Sub

    ''' <summary> Asserts revision entity. </summary>
    ''' <remarks>
    ''' Time zone issue with SQL Light.  Storing the SQL value in UTC returns back time in the
    ''' timezone of the 'server'.
    ''' </remarks>
    ''' <param name="site">                  The site. </param>
    ''' <param name="provider">              The provider. </param>
    ''' <param name="activeDatabaseVersion"> The active database version. </param>
    Public Shared Sub AssertRevisionEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal activeDatabaseVersion As String)
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {RevisionBuilder.TableName})
        Dim expectedActiveVersion As String = "1.0.0"
        Dim newActiveVersion As String = "0.0.1000"
        Dim revisionEntity As New RevisionEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            ' clear previous if exists and reset identity
            Dim exists As Boolean = Entities.RevisionEntity.IsExists(connection, newActiveVersion)

            revisionEntity = New RevisionEntity
            revisionEntity.FetchUsingUniqueIndex(connection, activeDatabaseVersion)
            r = revisionEntity.ValidateStoredEntity($"Existing {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Revision)} Of {newActiveVersion}")
            Assert.IsTrue(r.Success, r.Details)

            ' delete the entity
            revisionEntity.Delete(connection)

            ' reseed the entity
            connection.ReseedTableIdentity(RevisionBuilder.TableName)

            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = False
            revisionEntity = New RevisionEntity
            actualFetched = revisionEntity.FetchUsingUniqueIndex(connection, expectedActiveVersion)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.RevisionEntity)} {NameOf(Entities.RevisionEntity.Revision)} Of {expectedActiveVersion} should Not exist")
            r = revisionEntity.ValidateNewEntity($"New {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Revision)} Of {expectedActiveVersion}")
            Assert.IsTrue(r.Success, r.Details)
            revisionEntity.Clear()

            ' clear the table; the table maybe empty, in which case the action returns False.
            connection.DeleteAll(Of RevisionNub)
            connection.ReseedTableIdentity(RevisionBuilder.TableName)

            Dim expectedCount As Integer = 0
            Dim actualInserted As Boolean = RevisionBuilder.UpsertRevision(connection, activeDatabaseVersion)
            Assert.IsTrue(actualInserted, $"Release {activeDatabaseVersion} should exist Or updated")
            expectedCount += 1

            expectedActiveVersion = activeDatabaseVersion
            actualFetched = revisionEntity.FetchUsingUniqueIndex(connection, expectedActiveVersion)
            expectedFetched = True
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Revision)} Of {expectedActiveVersion} should exist")
            r = revisionEntity.ValidateStoredEntity($"Existing {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Revision)} Of {expectedActiveVersion}")
            Assert.IsTrue(r.Success, r.Details)

            Dim previousActiveTimestamp As DateTime = revisionEntity.Timestamp
            Dim previousActiveVersion As String = revisionEntity.Revision
            Dim expectedUpdate As Boolean = True
            Dim actualUpdate As Boolean = True
            actualUpdate = revisionEntity.Update(connection, UpdateModes.Refetch)
            Assert.IsFalse(actualUpdate, $"{NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Revision)} Of {expectedActiveVersion} should Not be updated")
            r = revisionEntity.ValidateStoredEntity($"Unchanged {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Revision)} Of {expectedActiveVersion}")
            Assert.IsTrue(r.Success, r.Details)
            revisionEntity.FetchActive(connection)
            Assert.AreEqual(previousActiveTimestamp, revisionEntity.Timestamp, $"Active {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Timestamp)} should match previous value")
            Assert.AreEqual(previousActiveVersion, revisionEntity.Revision, $"Active {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Revision)} should match previous value")

            Dim endTime As DateTime = DateTime.Now.Add(TimeSpan.FromSeconds(1))
            Threading.SpinWait.SpinUntil(Function()
                                             Return DateTime.Now > endTime
                                         End Function)

            Dim expectedInserted As Boolean = True
            Dim expectedActive As Boolean = True
            expectedActiveVersion = newActiveVersion
            revisionEntity = New RevisionEntity With {.Revision = newActiveVersion}
            actualInserted = revisionEntity.Insert(connection)
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Revision)} Of {revisionEntity.Revision} should have been inserted")
            Dim newTimestamp As DateTime = revisionEntity.Timestamp
            revisionEntity.FetchActive(connection)
            Assert.AreEqual(newActiveVersion, revisionEntity.Revision, $"New active {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Revision)} should match previous value")
            Assert.AreEqual(newTimestamp, revisionEntity.Timestamp, $"New active {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Timestamp)} should match previous value")

            ' timestamp is in universal time.
            ' set timestamp of the current version to precede the previous version by one second.
            Entities.RevisionBuilder.Get.UpdateTimestamp(connection, revisionEntity.AutoId, previousActiveTimestamp.Subtract(TimeSpan.FromSeconds(1)))
            ' refetch the active revision; it should switch
            revisionEntity = New RevisionEntity
            revisionEntity.FetchActive(connection)
            Assert.AreEqual(previousActiveVersion, revisionEntity.Revision, $"Reactivated {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Revision)} should match previous value")
            Assert.AreEqual(previousActiveTimestamp, revisionEntity.Timestamp, $"Reactivated {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Timestamp)} should match previous value")

            newTimestamp = previousActiveTimestamp.Add(TimeSpan.FromSeconds(2))
            Entities.RevisionBuilder.Get.UpdateTimestamp(connection, newActiveVersion, newTimestamp)
            revisionEntity.FetchActive(connection)
            Assert.AreEqual(newActiveVersion, revisionEntity.Revision, $"Restored new active {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Revision)} should match previous value")
            Assert.AreEqual(newTimestamp, revisionEntity.Timestamp, $"Restored new active {NameOf(Entities.RevisionEntity)} With {NameOf(Entities.RevisionEntity.Timestamp)} should match previous value")

        End Using
    End Sub

    #End Region

    #Region " SESSION "

    ''' <summary> Asserts <see cref="SessionTraitTypeEntity"/> test conditions. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertSessionTraitTypeEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {SessionTraitTypeBuilder.TableName})
        Dim id As Integer = isr.Dapper.Entities.SessionTraitType.None
        Dim sessionTraitType As New SessionTraitTypeEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = sessionTraitType.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.SessionTraitTypeEntity)} With {NameOf(Entities.SessionTraitTypeEntity.Id)} Of {id} should Not exist")
            r = sessionTraitType.ValidateNewEntity($"New {NameOf(Entities.SessionTraitTypeEntity)} With {NameOf(Entities.SessionTraitTypeEntity.Id)} Of {id}")
            Assert.IsTrue(r.Success, r.Details)
            sessionTraitType.Clear()
            id = isr.Dapper.Entities.SessionTraitType.SessionNumber
            expectedFetched = True
            actualFetched = sessionTraitType.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.SessionTraitTypeEntity)} With {NameOf(Entities.SessionTraitTypeEntity.Id)} Of {id}should exist")
            r = sessionTraitType.ValidateStoredEntity($"Fetched {NameOf(Entities.SessionTraitTypeEntity)} With {NameOf(Entities.SessionTraitTypeEntity.Id)} Of {id}")
            Assert.IsTrue(r.Success, r.Details)

            sessionTraitType.Label = isr.Dapper.Entities.SessionTraitType.SessionNumber.ToString
            actualFetched = sessionTraitType.FetchUsingUniqueIndex(connection)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.SessionTraitTypeEntity)} by {NameOf(Entities.SessionTraitTypeEntity.Label)} Of {isr.Dapper.Entities.SessionTraitType.SessionNumber} should exist")
            r = sessionTraitType.ValidateStoredEntity($"Fetched {NameOf(Entities.SessionTraitTypeEntity)} by {NameOf(Entities.SessionTraitTypeEntity.Label)} Of {isr.Dapper.Entities.SessionTraitType.SessionNumber}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = [Enum].GetValues(GetType(isr.Dapper.Entities.SessionTraitType)).Length - 1
            Dim actualCount As Integer = sessionTraitType.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"Expected {expectedCount} {NameOf(Entities.SessionTraitTypeEntity)}s")
        End Using
    End Sub

    ''' <summary> Assert adding session entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">         The site. </param>
    ''' <param name="provider">     The provider. </param>
    ''' <param name="sessionLabel"> The session label. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function AssertAddingSessionEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal sessionLabel As String) As SessionEntity
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {SessionBuilder.TableName})
        Dim id As Integer = 3
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            connection.DeleteAll(Of SessionNub)
            Dim actualFetched As Boolean
            Dim session As New SessionEntity
            actualFetched = session.FetchUsingKey(connection, id)
            Assert.IsFalse(actualFetched, $"{NameOf(Entities.SessionEntity)} with {NameOf(Entities.SessionEntity.AutoId)} of {id} should not exist")
            r = session.ValidateNewEntity($"{NameOf(Entities.SessionEntity)} with {NameOf(Entities.SessionEntity.AutoId)} of {id}")
            Assert.IsTrue(r.Success, r.Details)
            session.Clear()

            session = New SessionEntity With {.Label = sessionLabel}
            Dim actualInserted As Boolean = session.Insert(connection)
            Assert.IsTrue(actualInserted, $"{NameOf(Entities.SessionEntity)} with {NameOf(Entities.SessionEntity.Label)} of {sessionLabel} should have been inserted")
            r = session.ValidateStoredEntity($"Inserted {NameOf(Entities.SessionEntity)} with {NameOf(Entities.SessionEntity.Label)} of {sessionLabel}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = 1
            Dim actualCount As Integer = session.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple {NameOf(Entities.SessionEntity)}'s should have been inserted")
            Return session
        End Using
    End Function

    ''' <summary> Assert adding lot session entities. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="site">         The site. </param>
    ''' <param name="provider">     The provider. </param>
    ''' <param name="partNumber">   The part number. </param>
    ''' <param name="lotNumber">    The lot number. </param>
    ''' <param name="sessionCount"> Number of sessions. </param>
    Public Shared Sub AssertAddingLotSessionEntities(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                     ByVal partNumber As String, ByVal lotNumber As String, ByVal sessionCount As Integer)
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {SessionBuilder.TableName,
                                                            LotBuilder.TableName, LotSessionBuilder.TableName, PartBuilder.TableName})
        Dim id As Integer = 3
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            connection.DeleteAll(Of SessionNub)()
            connection.DeleteAll(Of LotNub)()
            connection.DeleteAll(Of PartNub)()
            Dim partId As Integer = 1
            Dim part As New PartEntity With {.PartNumber = partNumber}
            part.Insert(connection)
            partId = part.AutoId
            r = part.ValidateStoredEntity($"Inserted {NameOf(Entities.PartEntity)} with '{NameOf(Entities.PartEntity.PartNumber)}' of {partNumber}")
            Assert.IsTrue(r.Success, r.Details)

            Dim lot As New LotEntity With {.LotNumber = lotNumber, .AutoId = partId}
            Dim actualInserted As Boolean = lot.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.LotEntity)}  with '{NameOf(Entities.LotEntity.LotNumber)}' of {lot.LotNumber} should have been inserted")
            r = lot.ValidateStoredEntity($"Insert {NameOf(Entities.LotEntity)} with '{NameOf(Entities.LotEntity.LotNumber)}' of {lotNumber}")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = False
            Dim expectedCount As Integer = 0
            Dim session As SessionEntity
            Dim lotSession As LotSessionEntity
            Dim label As String
            expectedInserted = True
            For i As Integer = 1 To 2
                label = $"{lot.LotNumber}:{expectedCount + 1}"
                session = New SessionEntity With {.Label = label}
                actualInserted = session.Insert(connection)
                Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.SessionEntity)} with {NameOf(Entities.SessionEntity.Label)} of {label} should have been inserted")
                r = session.ValidateStoredEntity($"{NameOf(Entities.SessionEntity)} with {NameOf(Entities.SessionEntity.Label)} of {label}")
                Assert.IsTrue(r.Success, r.Details)
                lotSession = New LotSessionEntity With {.LotAutoId = lot.AutoId, .SessionAutoId = session.AutoId}
                actualInserted = lotSession.Insert(connection)
                Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.LotSessionEntity)} with [{NameOf(Entities.LotSessionEntity.LotAutoId)},{NameOf(Entities.LotSessionEntity.SessionAutoId)}] of [{lot.AutoId},{session.AutoId}] should have been inserted")
                r = lotSession.ValidateStoredEntity($"Inserted {NameOf(Entities.LotSessionEntity)} with [{NameOf(Entities.LotSessionEntity.LotAutoId)},{NameOf(Entities.LotSessionEntity.SessionAutoId)}] of [{lot.AutoId},{session.AutoId}]")
                Assert.IsTrue(r.Success, r.Details)
                expectedCount += 1

                lotSession.FetchUsingKey(connection, lot.AutoId, session.AutoId)
                Assert.AreEqual(lot.AutoId, lotSession.LotAutoId, $"{NameOf(Entities.LotSessionEntity)} [{NameOf(Entities.LotSessionEntity.LotAutoId)}] should match")
                Assert.AreEqual(session.AutoId, lotSession.SessionAutoId, $"{NameOf(Entities.LotSessionEntity)} [{NameOf(Entities.LotSessionEntity.SessionAutoId)}] should match")
            Next
            Dim actualCount As Integer = LotSessionEntity.CountEntities(connection, lot.AutoId)
            Assert.AreEqual(expectedCount, actualCount, $"{NameOf(Entities.LotSessionEntity)}'s count should match")
        End Using
    End Sub

    #End Region

    #Region " SESSION MONITOR "

    ''' <summary> Asset adding Session Monitor. </summary>
    ''' <remarks> David, 5/28/2020. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="productNumber"> The product number. </param>
    ''' <param name="partNumber">    The part number. </param>
    ''' <param name="lotNumber">     The lot number. </param>
    ''' <param name="multimeterId">  Identifier for the multimeter. </param>
    ''' <param name="meterNumber">   The meter number. </param>
    ''' <returns> An Entities.ProductTestMonitor. </returns>
    Public Shared Function AssetAddingSessionMonitor(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                         ByVal productNumber As String, ByVal partNumber As String,
                                                         ByVal lotNumber As String, ByVal multimeterId As Integer,
                                                         ByVal meterNumber As Integer) As Entities.SessionMonitor
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider,
                   New String() {Entities.ProductBuilder.TableName, PartBuilder.TableName, Entities.ProductPartBuilder.TableName, Entities.LotBuilder.TableName})
        Dim productTestMonitor As SessionMonitor = Entities.SessionMonitor.Get
        Dim part As PartEntity
        ' Dim element As ElementEntity
        Dim productPart As Entities.ProductPartEntity
        Dim product As Entities.ProductEntity
        Dim lot As LotEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            product = Entities.Tests.Auditor.AssertAddingProductEntity(site, provider, productNumber)

            part = Auditor.AssertAddingPartEntity(site, provider, partNumber)
            productTestMonitor.Part = part

            productPart = Entities.Tests.Auditor.AssertAddingProductPartEntity(site, provider, product.AutoId, part.AutoId)

            Dim expectedElementCount As Integer = Entities.Tests.Auditor.AssertAddingProductElementEntities(site, provider, product, part)

            Assert.AreEqual(expectedElementCount, part.FetchElements(connection, New Integer() {multimeterId}),
                            $"{NameOf(PartEntity)}.{NameOf(PartEntity.Elements)} count should match")
            productTestMonitor.Elements = part.Elements

            Dim overflowContinuousFailureCountLimit As Integer = 2
            productTestMonitor.Sorts = Entities.Tests.Auditor.AssertAddingProductSortEntities(site, provider, product, overflowContinuousFailureCountLimit)
            productTestMonitor.Binnings = Entities.Tests.Auditor.AssertAddingProductBinningEntities(site, provider, product,
                                                                                                    productTestMonitor.Elements, productTestMonitor.Sorts)

            If Not MultimeterEntity.IsEnumerated Then MultimeterEntity.TryFetchAll(connection)
            lot = Auditor.AssertAddingLotEntity(site, provider, part.AutoId, lotNumber)
            lot.FetchUuts(connection)
            productTestMonitor.Lot = lot
            productTestMonitor.UutMonitor = New UutMonitor(MultimeterEntity.EntityLookupDictionary(multimeterId))

            productTestMonitor.Uuts = lot.Uuts

        End Using
        Return productTestMonitor
    End Function

    #End Region

    #Region " SESSION OHM METER "

    ''' <summary> Assert session ohm meter setup entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="sessionAutoId"> Identifier for the session automatic identity. </param>
    Public Shared Sub AssertSessionOhmMeterSetupEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal sessionAutoId As Integer)

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {OhmMeterSettingLookupBuilder.TableName,
                                                            OhmMeterSetupBuilder.TableName, OhmMeterSettingBuilder.TableName, SessionOhmMeterSetupBuilder.TableName})
        Dim ohmMeterSettingLookupId As Integer = 0
        Dim ohmMeterSettingLookup As New OhmMeterSettingLookupEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = ohmMeterSettingLookup.FetchUsingKey(connection, ohmMeterSettingLookupId)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {ohmMeterSettingLookupId} should not exist")
            r = ohmMeterSettingLookup.ValidateNewEntity($"New {NameOf(IOhmMeterSettingLookup)} with {NameOf(IOhmMeterSettingLookup.OhmMeterSettingId)} equals {ohmMeterSettingLookupId}")
            Assert.IsTrue(r.Success, r.Details)
            ohmMeterSettingLookup.Clear()
            Dim meterEntities As IEnumerable(Of MultimeterEntity) = MultimeterEntity.FetchAllEntities(connection, False)
            Dim expectedCount As Integer = 4
            Assert.AreEqual(expectedCount, meterEntities.Count, $"There should be enough meter entities")
            Dim expectedToleranceCode As String = "A"
            Dim resistance As Double = 399
            Dim ohmMeterSettingLookupNub As OhmMeterSettingLookupNub
            Dim expectedExists As Boolean = True
            Dim actualExists As Boolean
            Dim ohmMeterSettingEntity As New OhmMeterSettingEntity
            Dim ohmMeterSetupEntity As New OhmMeterSetupEntity
            Dim actualCount As Integer

            ' clear the table
            connection.DeleteAll(Of OhmMeterSetupNub)()
            Dim id As Integer = 1
            ohmMeterSettingEntity = New OhmMeterSettingEntity With {.Id = id}
            actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex(connection)
            Assert.IsTrue(actualExists, $"{NameOf(isr.Dapper.Entities.OhmMeterSettingEntity)} should exist for auto id = {id}")
            actualCount = OhmMeterSettingEntity.CountEntities(connection)
            expectedCount = 6 ' at least 6
            Assert.IsTrue(actualCount >= expectedCount, $"{NameOf(isr.Dapper.Entities.OhmMeterSettingEntity)} should have at least {expectedCount} records")

            Dim sessionOhmMeterSetupEntity As New SessionOhmMeterSetupEntity
            expectedCount = 0
            actualCount = connection.CountEntities(SessionOhmMeterSetupBuilder.TableName)
            Assert.AreEqual(expectedCount, actualCount, $"Session ohm meter setup nub record count should match")

            For Each MultimeterEntity As MultimeterEntity In meterEntities
                ohmMeterSettingLookupNub = OhmMeterSettingLookupEntity.FetchNubs(connection, expectedToleranceCode, MultimeterEntity.Id, resistance).First
                ohmMeterSettingEntity = New OhmMeterSettingEntity With {.Id = ohmMeterSettingLookupNub.OhmMeterSettingId}
                actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex(connection)
                Assert.IsTrue(actualExists, $"ohm meter setting condition should exist for id = {ohmMeterSettingEntity.Id}")

                ohmMeterSetupEntity = New OhmMeterSetupEntity
                ohmMeterSetupEntity.Copy(ohmMeterSettingEntity)

                actualCount = OhmMeterSetupEntity.CountEntities(connection)
                expectedCount = actualCount
                expectedCount += 1
                actualExists = ohmMeterSetupEntity.Insert(connection)
                Assert.IsTrue(actualExists, $"A new resistance ohm meter setup condition should be stored for meter {MultimeterEntity.MultimeterModelId} number {MultimeterEntity.MultimeterNumber}")
                actualCount = OhmMeterSetupEntity.CountEntities(connection)
                Assert.IsTrue(actualCount >= expectedCount, $"Resistance ohm meter setup condition should have at least {expectedCount} records")

                sessionOhmMeterSetupEntity = New SessionOhmMeterSetupEntity With {.SessionAutoId = sessionAutoId,
                                                                                  .OhmMeterSetupAutoId = ohmMeterSetupEntity.AutoId,
                                                                                  .MultimeterId = MultimeterEntity.Id}
                actualExists = sessionOhmMeterSetupEntity.Insert(connection)
                Assert.IsTrue(actualExists,
                              $"A new session ohm meter setup condition should be stored for meter {MultimeterEntity.MultimeterModelId} and session id {sessionAutoId}")
                actualCount = SessionOhmMeterSetupEntity.CountEntities(connection, sessionAutoId)
                Assert.IsTrue(actualCount >= expectedCount, $"Session ohm meter setup condition should have at least {expectedCount} records")

            Next
        End Using
    End Sub

    ''' <summary> Asserts station session ohm meter setup entity. </summary>
    ''' <remarks> David, 6/21/2020. </remarks>
    ''' <param name="site">         The site. </param>
    ''' <param name="provider">     The provider. </param>
    ''' <param name="sessionLabel"> The session label. </param>
    Public Shared Sub AssertSessionOhmMeterSetupEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal sessionLabel As String)
        Dim session As SessionEntity = Auditor.AssertAddingSessionEntity(site, provider, sessionLabel)
        Auditor.AssertSessionOhmMeterSetupEntity(site, provider, session.AutoId)
    End Sub

    #End Region

    #Region " SESSION TRAIT "

    ''' <summary> Assert adding Session Traits. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="sessionAutoId"> Identifier for the session automatic identity. </param>
    ''' <returns> A SessionEntity. </returns>
    Public Shared Function AssertAddingSessionTraits(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal sessionAutoId As Integer) As SessionEntity

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {SessionBuilder.TableName, SessionTraitBuilder.TableName,
                                                            SessionTraitTypeBuilder.TableName})
        Dim session As New SessionEntity With {.AutoId = sessionAutoId}
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedTraitCount As Integer = 0
            Dim sessionNumber As Integer = 1
            Dim sessionTraitEntity As New SessionTraitEntity With {.SessionAutoId = sessionAutoId, .SessionTraitTypeId = SessionTraitType.SessionNumber, .Amount = sessionNumber}
            sessionTraitEntity.Insert(connection)
            expectedTraitCount += 1

            Dim testTypeId As Integer = TestType.Final
            sessionTraitEntity = New SessionTraitEntity With {.SessionAutoId = sessionAutoId, .SessionTraitTypeId = SessionTraitType.TestType, .Amount = testTypeId}
            sessionTraitEntity.Insert(connection)
            expectedTraitCount += 1

            session.FetchUsingKey(connection)
            r = session.ValidateStoredEntity($"Fetch {NameOf(Entities.SessionEntity)} with '{NameOf(Entities.SessionEntity.AutoId)}' of {sessionAutoId}")
            Assert.IsTrue(r.Success, r.Details)
            Assert.AreEqual(expectedTraitCount, session.FetchTraits(connection), $"{NameOf(Entities.SessionEntity)} {NameOf(Entities.SessionEntity.Traits)} count should match")
            Assert.AreEqual(sessionNumber, session.Traits.SessionTrait.SessionNumber, $"{NameOf(Entities.SessionEntity)} {NameOf(Entities.SessionEntity.Traits)} {NameOf(Entities.SessionTrait.SessionNumber)} should match")
            Assert.AreEqual(testTypeId, session.Traits.SessionTrait.TestType, $"{NameOf(Entities.SessionEntity)} {NameOf(Entities.SessionEntity.Traits)} {NameOf(Entities.SessionTrait.TestType)} should match")

            Dim modifiedSessionNumber As Integer = sessionNumber + 1
            session.Traits.SessionTrait.SessionNumber = modifiedSessionNumber
            Assert.IsTrue(session.Upsert(connection), $"{NameOf(Entities.SessionEntity)} {NameOf(Entities.SessionEntity.Traits)} {NameOf(Entities.SessionTrait.SessionNumber)} should be updated")
            Assert.AreEqual(expectedTraitCount, session.FetchTraits(connection), $"{NameOf(Entities.SessionEntity)} {NameOf(Entities.SessionEntity.Traits)} count should match after second fetch")
            Assert.AreEqual(modifiedSessionNumber, session.Traits.SessionTrait.SessionNumber, $"{NameOf(Entities.SessionEntity)} {NameOf(Entities.SessionEntity.Traits)} {NameOf(Entities.SessionTrait.SessionNumber)} should match after change")

        End Using
        Return session
    End Function

    ''' <summary> Assert adding Session Traits. </summary>
    ''' <remarks> David, 6/17/2020. </remarks>
    ''' <param name="site">         The site. </param>
    ''' <param name="provider">     The provider. </param>
    ''' <param name="sessionLabel"> The session label. </param>
    Public Shared Sub AssertAddingSessionTraits(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal sessionLabel As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            connection.DeleteAll(Of SessionNub)
            connection.DeleteAll(Of SessionTraitNub)
            Dim Session As SessionEntity = Auditor.AssertAddingSessionEntity(site, provider, sessionLabel)
            Session = Auditor.AssertAddingSessionTraits(site, provider, Session.AutoId)
        End Using

    End Sub

    #End Region

    #Region " STATION "

    ''' <summary> The server computer. </summary>
    Private Shared _ServerComputer As Microsoft.VisualBasic.Devices.ServerComputer

    ''' <summary> Gets the server computer. </summary>
    ''' <value> The server computer. </value>
    Public Shared ReadOnly Property ServerComputer As Microsoft.VisualBasic.Devices.ServerComputer
        Get
            If Auditor._ServerComputer Is Nothing Then
                Auditor._ServerComputer = New Microsoft.VisualBasic.Devices.ServerComputer
            End If
            Return Auditor._ServerComputer
        End Get
    End Property

    ''' <summary> Asserts station entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">                        The site. </param>
    ''' <param name="provider">                    The provider. </param>
    ''' <param name="expectedUsingNativeTracking"> True to expected using native tracking. </param>
    Public Shared Sub AssertStationEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal expectedUsingNativeTracking As Boolean)

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {ComputerBuilder.TableName, StationBuilder.TableName})

        Dim expectedComputerName As String = Auditor.ServerComputer.Name
        Dim expectedTimeZone As String = TimeZone.CurrentTimeZone.StandardName
        Dim id As Integer = 4
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim computerStationEntity As New ComputerStationEntity
            r = computerStationEntity.TryObtain(connection, expectedComputerName, expectedComputerName, False)
            Assert.IsTrue(r.Success, r.Details)

            Dim station As StationEntity = computerStationEntity.FetchStationEntity(connection)
            r = station.ValidateStoredEntity($"{NameOf(Entities.StationEntity)}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualUpdate As Boolean
            Dim stationName As String = station.StationName
            Dim newStationName As String = $"{stationName}.Changed"
            Dim expectedStationName As String = newStationName
            id = station.AutoId
            If connection.State = Data.ConnectionState.Closed Then connection.Open()
            Using transaction As Data.IDbTransaction = connection.BeginTransaction()
                Try
                    station.StationName = newStationName
                    r = station.ValidateChangedEntity($"Changed {NameOf(Entities.StationEntity)} '{NameOf(Entities.StationEntity.StationName)}' to {newStationName}")
                    Assert.IsTrue(r.Success, r.Details)
                    Dim transactedConnection As New Global.Dapper.TransactedConnection(connection, transaction)
                    expectedStationName = newStationName
                    actualUpdate = station.Update(transactedConnection, UpdateModes.Refetch)
                    Assert.IsTrue(actualUpdate, $"Changed {NameOf(Entities.StationEntity)} '{NameOf(Entities.StationEntity.StationName)}' to {newStationName} should be updated")
                    transaction.Commit()
                    station.FetchUsingKey(connection)
                    Assert.AreEqual(expectedStationName, station.StationName,
                                $"{NameOf(Entities.StationEntity)} '{NameOf(Entities.StationEntity.StationName)}' should be restored after transaction rollback")
                Catch
                    Throw
                End Try
            End Using
            Using transaction As Data.IDbTransaction = connection.BeginTransaction()
                Try
                    expectedStationName = station.StationName
                    station.StationName = stationName
                    r = station.ValidateChangedEntity($"Changed {NameOf(Entities.StationEntity)} '{NameOf(Entities.StationEntity.StationName)}' to {newStationName}")
                    Assert.IsTrue(r.Success, r.Details)
                    Dim transactedConnection As New Global.Dapper.TransactedConnection(connection, transaction)
                    actualUpdate = station.Update(transactedConnection, UpdateModes.Refetch)
                    Assert.IsTrue(actualUpdate, $"Changed {NameOf(Entities.StationEntity)} '{NameOf(Entities.StationEntity.StationName)}' to {newStationName} should be updated")
                    transaction.Rollback()
                    station.FetchUsingKey(connection)
                    Assert.AreEqual(expectedStationName, station.StationName,
                                    $"{NameOf(Entities.StationEntity)} '{NameOf(Entities.StationEntity.StationName)}' should be restored after transaction rollback")
                Catch
                    Throw
                End Try
            End Using

        End Using
    End Sub

    ''' <summary> Asserts adding a station entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <returns> A (AutoId As Integer, Name As String) </returns>
    Public Shared Function AssertAddingStationEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase) As (AutoId As Integer, Name As String)
        Dim expectedComputerName As String = Auditor.ServerComputer.Name
        Dim expectedTimeZone As String = TimeZone.CurrentTimeZone.StandardName
        Dim id As Integer = 4
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = StationBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of StationNub)()
            Dim computerStationEntity As New ComputerStationEntity
            r = computerStationEntity.TryObtain(connection, expectedComputerName, expectedComputerName, False)
            Assert.IsTrue(r.Success, r.Details)

            Dim station As StationEntity = computerStationEntity.FetchStationEntity(connection)
            r = station.ValidateStoredEntity($"{NameOf(Entities.StationEntity)}")
            Assert.IsTrue(r.Success, r.Details)

            Return (station.AutoId, station.StationName)
        End Using
    End Function

    #End Region

    #Region " STATION SESSION "

    ''' <summary> Assert station session entity. </summary>
    ''' <remarks> David, 7/6/2020. </remarks>
    ''' <param name="site">         The site. </param>
    ''' <param name="provider">     The provider. </param>
    ''' <param name="sessionLabel"> The session label. </param>
    Public Shared Sub AssertStationSessionEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal sessionLabel As String)
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {StationSessionBuilder.TableName})
        Dim stationInfo As (StationAutoId As Integer, StationName As String) = Auditor.AssertAddingStationEntity(site, provider)
        Dim session As SessionEntity = Auditor.AssertAddingSessionEntity(site, provider, sessionLabel)
        Dim stationSession As StationSessionEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            stationSession = New StationSessionEntity With {.StationAutoId = stationInfo.StationAutoId, .SessionAutoId = session.AutoId}
            stationSession.Obtain(connection)
            r = stationSession.ValidateStoredEntity($"{NameOf(Entities.StationSessionEntity)} with [{NameOf(Entities.StationSessionEntity.StationAutoId)},{NameOf(Entities.StationSessionEntity.SessionAutoId)}] or [{stationInfo.StationAutoId},{session.AutoId}] ")
            Assert.IsTrue(r.Success, r.Details)
        End Using
    End Sub

    #End Region

    #Region " STRUCTURE TYPE "

    ''' <summary> Asserts the Structure Type entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertStructureTypeEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Dim id As Integer = 10
        Dim structureType As New StructureTypeEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = StructureTypeBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = structureType.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.StructureTypeEntity)} with '{NameOf(Entities.StructureTypeEntity.Id)}' of '{id}' should not exist")
            r = structureType.ValidateNewEntity($"{NameOf(Entities.StructureTypeEntity)} with '{NameOf(Entities.StructureTypeEntity.Id)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)

            id = isr.Dapper.Entities.StructureType.SplitCross
            actualFetched = structureType.FetchUsingKey(connection, id)
            Assert.IsTrue(actualFetched, $"{NameOf(Entities.StructureTypeEntity)} with '{NameOf(Entities.StructureTypeEntity.Id)}' of '{id}' should exist")
            r = structureType.ValidateStoredEntity($"Fetched {NameOf(Entities.StructureTypeEntity)} with '{NameOf(Entities.StructureTypeEntity.Id)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)

            Dim label As String = isr.Dapper.Entities.StructureType.GreekCross.ToString
            structureType.Label = label
            r = structureType.ValidateChangedEntity($"Changed {NameOf(Entities.StructureTypeEntity)} with '{NameOf(Entities.StructureTypeEntity.Label)}' of '{label}'")
            Assert.IsTrue(r.Success, r.Details)

            actualFetched = structureType.FetchUsingUniqueIndex(connection)
            Assert.IsTrue(actualFetched, $"{NameOf(Entities.StructureTypeEntity)} with '{NameOf(Entities.StructureTypeEntity.Label)}' of '{label}' be fetched")
            Assert.AreEqual(label, structureType.Label, $"{NameOf(Entities.StructureTypeEntity)} with '{NameOf(Entities.StructureTypeEntity.Label)}' of '{label}' should exist")
            r = structureType.ValidateStoredEntity($"Fetched {NameOf(Entities.StructureTypeEntity)} with '{NameOf(Entities.StructureTypeEntity.Label)}' of '{label}'")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = 3
            Dim actualCount As Integer = structureType.FetchAllEntities(connection)
            Assert.IsTrue(actualCount >= expectedCount, $"Expected at least {expectedCount} found {actualCount} PC Structure types")

        End Using
    End Sub

    #End Region

    #Region " STRUCTURE "

    ''' <summary> Asserts Structure entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    Public Shared Sub AssertStructureEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                            ByVal partNumber As String, ByVal lotNumber As String)
        Dim id As Integer = 3
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = StructureBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of PartNub)()
            connection.DeleteAll(Of LotNub)()
            connection.DeleteAll(Of SubstrateNub)()
            connection.DeleteAll(Of StructureNub)()
            Dim part As New PartEntity With {.PartNumber = partNumber}
            part.Insert(connection)

            Dim lot As New LotEntity With {.LotNumber = lotNumber, .AutoId = part.AutoId}
            lot.Insert(connection)

            Dim substrate As New SubstrateEntity With {.Amount = 1, .SubstrateTypeId = SubstrateType.Wafer}
            substrate.Insert(connection)

            Dim expectedFetched As Boolean = False
            Dim structureEntity As New StructureEntity

            Dim actualFetched As Boolean = structureEntity.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.AutoId)} of {id} should not exist")
            r = structureEntity.ValidateNewEntity($"{NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.AutoId)} of {id}")
            Assert.IsTrue(r.Success, r.Details)
            structureEntity.Clear()

            Dim structureType As StructureType = StructureType.GreekCross
            Dim expectedCount As Integer = 3
            Dim ordinalNumber As Integer = 1
            structureEntity = New StructureEntity With {.Amount = ordinalNumber, .ForeignId = structureType}
            Dim actualInserted As Boolean = structureEntity.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.Amount)} of {ordinalNumber} should have been inserted")
            Assert.AreEqual(ordinalNumber, structureEntity.Amount, $"{NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.Amount)} should match")
            r = structureEntity.ValidateStoredEntity($"Inserted {NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.Amount)} of {ordinalNumber}")
            Assert.IsTrue(r.Success, r.Details)

            ordinalNumber += 1
            structureEntity = New StructureEntity With {.Amount = ordinalNumber, .ForeignId = structureType}
            actualInserted = structureEntity.Insert(connection)
            Assert.AreEqual(expectedInserted, actualInserted, $"Structure {structureEntity.Amount} should have been inserted")
            Assert.AreEqual(ordinalNumber, structureEntity.Amount, $"Structure Number should match")
            r = structureEntity.ValidateStoredEntity($"Inserted {NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.Amount)} of {ordinalNumber}")
            Assert.IsTrue(r.Success, r.Details)

            ordinalNumber += 1
            structureEntity.Amount = ordinalNumber
            r = structureEntity.ValidateChangedEntity($"{NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.Amount)} to {ordinalNumber}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualUpdate As Boolean = structureEntity.Update(connection, UpdateModes.Refetch)
            Dim expectedUpdate As Boolean = True
            Assert.AreEqual(expectedUpdate, actualUpdate, $"{NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.Amount)} of {ordinalNumber} should be updated")
            r = structureEntity.ValidateStoredEntity($"Updated {NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.Amount)} of {ordinalNumber}")
            Assert.IsTrue(r.Success, r.Details)

            actualUpdate = structureEntity.Update(connection, UpdateModes.Refetch)
            Assert.IsFalse(actualUpdate, $"Unchanged {NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.Amount)} of {ordinalNumber} should not be updated")
            r = structureEntity.ValidateStoredEntity($"Unchanged {NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.Amount)} of {ordinalNumber}")
            Assert.IsTrue(r.Success, r.Details)

            ordinalNumber += 1
            structureEntity.Amount = ordinalNumber
            expectedUpdate = True
            actualUpdate = structureEntity.Update(connection, UpdateModes.Refetch)
            Assert.AreEqual(expectedUpdate, actualUpdate, $"Structure {structureEntity.Amount} should be updated")
            actualUpdate = structureEntity.Update(connection, UpdateModes.Refetch)
            Assert.IsFalse(actualUpdate, $"Structure {structureEntity.Amount} should not be updated; using cache")

            connection.DeleteAll(Of StructureNub)()
            For i As Integer = 1 To expectedCount
                ' this won't work; proxy is left dirty after the first insert; structureEntity.OrdinalNumber = i
                structureEntity = New StructureEntity With {.Amount = i, .ForeignId = structureType}
                actualInserted = structureEntity.Insert(connection)
                Assert.IsTrue(actualInserted, $"Structure {structureEntity.Amount} should have been inserted")
            Next
            Dim actualCount As Integer = structureEntity.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple Structures should have been inserted")

            Dim actualDelete As Boolean = structureEntity.Delete(connection)
            Dim expectedDelete As Boolean = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Structure Entity {ordinalNumber} should be deleted")
            expectedCount -= 1

            structureEntity = structureEntity.Structures(0)
            Dim deletectedOrdinalNumber As Integer = structureEntity.Amount
            actualDelete = StructureEntity.Delete(connection, structureEntity.AutoId)
            expectedDelete = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Structure {deletectedOrdinalNumber} should be deleted")
            expectedCount -= 1
            actualCount = structureEntity.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple Structures should have been inserted")

        End Using
    End Sub

    #End Region

    #Region " STRUCTURE SESSION "

    ''' <summary> Asserts session Structure. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">         The site. </param>
    ''' <param name="provider">     The provider. </param>
    ''' <param name="partNumber">   The part number. </param>
    ''' <param name="lotNumber">    The lot number. </param>
    ''' <param name="sessionLabel"> The session label. </param>
    Public Shared Sub AssertSessionStructure(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                             ByVal partNumber As String, ByVal lotNumber As String, ByVal sessionLabel As String)

        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = SessionStructureBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of PartNub)
            connection.DeleteAll(Of LotNub)
            connection.DeleteAll(Of StructureNub)
            connection.DeleteAll(Of StationNub)
            connection.DeleteAll(Of SessionNub)

            Dim part As New PartEntity With {.PartNumber = partNumber}
            part.Insert(connection)

            Dim lot As New LotEntity With {.LotNumber = lotNumber, .AutoId = part.AutoId}
            lot.Insert(connection)

            Dim substrate As New SubstrateEntity With {.Amount = 1, .SubstrateTypeId = SubstrateType.Wafer}
            substrate.Insert(connection)

            Dim structureType As StructureType = StructureType.SplitCross
            Dim ordinalNumber As Integer = 1
            Dim structureEntity As New StructureEntity With {.Amount = ordinalNumber, .ForeignId = structureType}
            structureEntity.Insert(connection)

            Dim expectedComputerName As String = Auditor.ServerComputer.Name
            Dim expectedTimeZone As String = TimeZone.CurrentTimeZone.StandardName
            Dim computerStationEntity As New ComputerStationEntity
            r = computerStationEntity.TryObtain(connection, expectedComputerName, expectedComputerName, False)
            Assert.IsTrue(r.Success, r.Details)

            Dim station As StationEntity = computerStationEntity.FetchStationEntity(connection)
            r = station.ValidateStoredEntity($"{NameOf(Entities.StationEntity)}")
            Assert.IsTrue(r.Success, r.Details)

            Dim testLevel As Integer = 0
            Dim session As New SessionEntity With {.Timestamp = DateTime.Now, .SessionLabel = sessionLabel}
            session.Insert(connection)

            Dim sessionStructure As New SessionStructureEntity With {.StructureAutoId = structureEntity.AutoId, .SessionAutoId = session.AutoId}
            Dim actualInserted As Boolean = sessionStructure.Insert(connection)
            Assert.IsTrue(actualInserted, $"{NameOf(Entities.SessionStructureEntity)} should have been inserted")
            r = sessionStructure.ValidateStoredEntity($"Inserted {NameOf(Entities.SessionStructureEntity)}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualFetched As Boolean = sessionStructure.FetchUsingKey(connection)
            Assert.IsTrue(actualFetched, $"{NameOf(sessionStructure)} should have been fetched")
            Assert.AreEqual(sessionStructure.SessionAutoId, session.AutoId, $"{NameOf(Entities.SessionStructureEntity.SessionAutoId)} {NameOf(Entities.SessionEntity.AutoId)} should match")
            Assert.AreEqual(sessionStructure.StructureAutoId, structureEntity.AutoId, $"{NameOf(Entities.SessionStructureEntity.StructureAutoId)} {NameOf(Entities.StructureEntity.AutoId)} should match")

            sessionStructure.FetchSessionEntity(connection)
            Assert.AreEqual(session.AutoId, sessionStructure.SessionAutoId, $"{NameOf(Entities.SessionStructureEntity)} should bring the correct {NameOf(SessionEntity)} ")
            Assert.AreEqual(sessionStructure.StructureAutoId, structureEntity.AutoId, $"Structure Session should point to the correct Structure")
            connection.DeleteAll(Of SessionStructureNub)()
        End Using
    End Sub

    #End Region

    #Region " SUBSTRATE TYPE "

    ''' <summary> Executes the Substrate Type entity test operation. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertSubstrateTypeEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Dim id As Integer = 10
        Dim entity As New SubstrateTypeEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = SubstrateTypeBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            r = entity.ValidateNewEntity($"{NameOf(Entities.SubstrateTypeEntity)} with '{NameOf(Entities.SubstrateTypeEntity.Id)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualFetched As Boolean = entity.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {id} should not exist")
            r = entity.ValidateNewEntity($"{NameOf(Entities.SubstrateTypeEntity)} with '{NameOf(Entities.SubstrateTypeEntity.Id)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)

            id = 1
            actualFetched = entity.FetchUsingKey(connection, id)
            Assert.IsTrue(actualFetched, $" {NameOf(Entities.SubstrateTypeEntity)} with '{NameOf(Entities.SubstrateTypeEntity.Id)}' of '{id}' should exist")
            r = entity.ValidateStoredEntity($"Existing {NameOf(Entities.SubstrateTypeEntity)} with '{NameOf(Entities.SubstrateTypeEntity.Id)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)

            Dim label As String = SubstrateType.Wafer.ToString
            entity.Label = label
            r = entity.ValidateChangedEntity($"Changed {NameOf(Entities.SubstrateTypeEntity)} with '{NameOf(Entities.SubstrateTypeEntity.Label)}' of '{label}'")
            Assert.IsTrue(r.Success, r.Details)

            actualFetched = entity.FetchUsingUniqueIndex(connection)
            Assert.IsTrue(actualFetched, $"{NameOf(Entities.SubstrateTypeEntity)} with '{NameOf(Entities.SubstrateTypeEntity.Label)}' of '{label}' should exist")
            Assert.AreEqual(label, entity.Label, $"{NameOf(Entities.SubstrateTypeEntity)} with '{NameOf(Entities.SubstrateTypeEntity.Label)}' of '{label}' should match")
            r = entity.ValidateStoredEntity($"Fetched {NameOf(Entities.SubstrateTypeEntity)} with '{NameOf(Entities.SubstrateTypeEntity.Label)}' of '{label}'")
            Assert.IsTrue(r.Success, r.Details)

            Dim expectedCount As Integer = 2
            Dim actualCount As Integer = entity.FetchAllEntities(connection)
            Assert.IsTrue(actualCount >= expectedCount, $"Expected at least {expectedCount} found {actualCount} substrate types")
        End Using
    End Sub

    #End Region

    #Region " SUBSTRATE "

    ''' <summary> Asserts substrate entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    Public Shared Sub AssertSubstrateEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                             ByVal partNumber As String, ByVal lotNumber As String)
        Dim id As Integer = 3
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = SubstrateBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of PartNub)()
            connection.DeleteAll(Of LotNub)()
            Dim part As New PartEntity With {.PartNumber = partNumber}
            part.Insert(connection)

            Dim lot As New LotEntity With {.LotNumber = lotNumber, .AutoId = part.AutoId}
            lot.Insert(connection)
            connection.DeleteAll(Of SubstrateNub)()
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = False
            Dim substrate As New SubstrateEntity

            actualFetched = substrate.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.SubstrateEntity)} with  {NameOf(Entities.SubstrateEntity.AutoId)} of {id} should not exist")
            r = substrate.ValidateNewEntity($"{NameOf(Entities.SubstrateEntity)} with  {NameOf(Entities.SubstrateEntity.AutoId)} of {id}")
            Assert.IsTrue(r.Success, r.Details)
            substrate.Clear()

            id = 1
            Dim expectedCount As Integer = 0
            Dim actualCount As Integer = 0
            substrate = New SubstrateEntity With {.Amount = id, .ForeignId = SubstrateType.Plate}
            Dim actualInserted As Boolean = substrate.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(Entities.SubstrateEntity)} with {NameOf(Entities.SubstrateEntity.Amount)} of {id} should have been inserted")
            r = substrate.ValidateStoredEntity($"Inserted {NameOf(Entities.SubstrateEntity)} with {NameOf(Entities.SubstrateEntity.Amount)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            expectedCount += 1
            id = 2
            substrate.Amount = id
            r = substrate.ValidateChangedEntity($"{NameOf(Entities.SubstrateEntity)} {NameOf(Entities.SubstrateEntity.Amount)} changed to {id}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualUpdate As Boolean = substrate.Update(connection, UpdateModes.Refetch)
            Dim expectedUpdate As Boolean = True
            Assert.AreEqual(expectedUpdate, actualUpdate, $"Changed {NameOf(Entities.SubstrateEntity)} with {NameOf(Entities.SubstrateEntity.Amount)} of {id} should be updated")
            r = substrate.ValidateStoredEntity($"Updated {NameOf(Entities.SubstrateEntity)} with {NameOf(Entities.SubstrateEntity.Amount)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            actualUpdate = substrate.Update(connection, UpdateModes.Refetch)
            Assert.IsFalse(actualUpdate, $"Unchanged {NameOf(Entities.SubstrateEntity)} with {NameOf(Entities.SubstrateEntity.Amount)} of {id} should not be updated")
            r = substrate.ValidateStoredEntity($"Unchanged {NameOf(Entities.SubstrateEntity)} with {NameOf(Entities.SubstrateEntity.Amount)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            Dim OrdinalNumber As Integer = substrate.Amount
            actualFetched = substrate.Obtain(connection)
            expectedFetched = True
            Assert.AreEqual(expectedFetched, actualFetched, $"Substrate {substrate.Amount} should have been obtained")
            Assert.AreEqual(OrdinalNumber, substrate.Amount, $"Obtained Substrate {substrate.Amount} should match substrate number")

            connection.DeleteAll(Of SubstrateNub)()
            expectedCount = 10
            For i As Integer = 1 To expectedCount
                substrate.Amount = i
                actualInserted = substrate.Insert(connection)
                expectedInserted = True
                Assert.AreEqual(expectedInserted, actualInserted, $"Substrate {substrate.Amount} should have been inserted")
            Next
            actualCount = substrate.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple Substrates should have been inserted")

            OrdinalNumber = substrate.Amount
            Dim actualDelete As Boolean = substrate.Delete(connection)
            Dim expectedDelete As Boolean = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Substrate {OrdinalNumber} should be deleted")
            expectedCount -= 1

            Dim substrate0 As IKeyForeignNaturalTime = substrate.Substrates(0)
            actualDelete = SubstrateEntity.Delete(connection, substrate0.AutoId)
            expectedDelete = True
            Assert.AreEqual(expectedDelete, actualDelete, $"Substrate {substrate0.Amount} should be deleted")
            expectedCount -= 1

        End Using
    End Sub

    #End Region

    #Region " TEST TYPE "

    ''' <summary> Asserts the test Type entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">               The site. </param>
    ''' <param name="provider">           The provider. </param>
    ''' <param name="existingTestTypeId"> Identifier for the existing test type. </param>
    ''' <param name="expectedCount">      Number of expected. </param>
    Public Shared Sub AssertTestTypeEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                           ByVal existingTestTypeId As Integer, ByVal expectedCount As Integer)
        Dim id As Integer = 10
        Dim testType As New TestTypeEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = TestTypeBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            r = testType.ValidateNewEntity($"{NameOf(TestTypeEntity)} with key {testType.Id}")
            Assert.IsTrue(r.Success, r.Details)

            Dim actualFetched As Boolean = testType.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {id} should not exist")
            r = testType.ValidateNewEntity($"{NameOf(TestTypeEntity)} with {NameOf(TestTypeEntity.Id)} of {id}")
            Assert.IsTrue(r.Success, r.Details)

            id = existingTestTypeId
            actualFetched = testType.FetchUsingKey(connection, id)
            Assert.IsTrue(actualFetched, $"{NameOf(TestTypeEntity)} key {id} should exist")
            r = testType.ValidateStoredEntity($"Existing {NameOf(TestTypeEntity)}  {id}")
            Assert.IsTrue(r.Success, r.Details)

            Dim existingLabel As String = testType.Label
            actualFetched = testType.FetchUsingUniqueIndex(connection, existingLabel)
            Assert.IsTrue(actualFetched, $"{NameOf(TestTypeEntity)} with {NameOf(TestTypeEntity.Label)} of {existingLabel} should exist")
            r = testType.ValidateStoredEntity($"Existing {NameOf(TestTypeEntity)} with {NameOf(TestTypeEntity.Label)} of {existingLabel}")
            Assert.IsTrue(r.Success, r.Details)

            Dim changedLabel As String = $"{existingLabel}.changed"
            If Not String.Equals(testType.Label, changedLabel) Then
                testType.Label = changedLabel
                r = testType.ValidateChangedEntity($"Changed {NameOf(TestTypeEntity)} with {NameOf(TestTypeEntity.Id)} of {id}")
                Assert.IsTrue(r.Success, r.Details)
                ' try to fetch using new name, which was not saved yet; 
                actualFetched = testType.FetchUsingUniqueIndex(connection)
                Assert.IsFalse(actualFetched, $"{NameOf(TestTypeEntity)} with {NameOf(TestTypeEntity.Label)} of {changedLabel} should not exist")
            End If

            Dim actualCount As Integer = testType.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"{NameOf(TestTypeEntity)}s items should equal expected")
        End Using
    End Sub

    ''' <summary> Asserts the test Type entity. </summary>
    ''' <remarks> David, 4/28/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="testTypeId"> Identifier for the test type. </param>
    Public Shared Sub AssertTestTypeEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal testTypeId As TestType)
        Auditor.AssertTestTypeEntity(site, provider, CInt(testTypeId), [Enum].GetValues(GetType(TestType)).Length - 1)
    End Sub

    #End Region

    #Region " TOLERANCE "

    ''' <summary> Asserts Tolerance entity. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertToleranceEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase)
        Dim id As String = String.Empty
        Dim tolerance As New ToleranceEntity
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = ToleranceBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = tolerance.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"key '{id}' should not exist")
            r = tolerance.ValidateNewEntity($"New {NameOf(Entities.ToleranceEntity)} with '{NameOf(Entities.ToleranceEntity.ToleranceCode)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)
            tolerance.Clear()

            id = "A"
            Dim expectedLowerLimit As Double = -0.0005
            Dim expectedUpperLimit As Double = 0.0005
            Dim expectedCaption As String = "±0.05%"
            expectedFetched = True
            actualFetched = tolerance.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.ToleranceEntity)} with '{NameOf(Entities.ToleranceEntity.ToleranceCode)}' of  {id} should exist")
            r = tolerance.ValidateStoredEntity($"Fetched {NameOf(Entities.ToleranceEntity)} with '{NameOf(Entities.ToleranceEntity.ToleranceCode)}' of '{id}'")
            Assert.IsTrue(r.Success, r.Details)

            Assert.AreEqual(expectedLowerLimit, tolerance.LowerLimit, $"{NameOf(tolerance.LowerLimit)} should match")
            Assert.AreEqual(expectedUpperLimit, tolerance.UpperLimit, $"{NameOf(tolerance.UpperLimit)} should match")
            Assert.AreEqual(expectedCaption, tolerance.Caption, $"{NameOf(tolerance.Caption)} should match")

            tolerance.Caption = expectedCaption
            expectedFetched = True
            actualFetched = tolerance.FetchUsingUniqueIndex(connection)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.ToleranceEntity)} with '{NameOf(Entities.ToleranceEntity.CompoundCaption)}' of '{expectedCaption}' should exist")
            r = tolerance.ValidateStoredEntity($"Fetched {NameOf(Entities.ToleranceEntity)} with '{NameOf(Entities.ToleranceEntity.CompoundCaption)}' of '{expectedCaption}'")
            Assert.IsTrue(r.Success, r.Details)

            tolerance.Caption = expectedCaption & "0"
            expectedFetched = False
            actualFetched = tolerance.FetchUsingUniqueIndex(connection)
            Assert.AreEqual(expectedFetched, actualFetched, $"caption {expectedCaption} should not exist")

            Dim expectedCount As Integer = 19
            Dim actualCount As Integer = tolerance.FetchAllEntities(connection)
            Assert.IsTrue(expectedCount <= actualCount, $"Expected {expectedCount} tolerance count should not exceed actual {actualCount}")

            Dim expectedUniqueLabel As Boolean = True
            Dim actualUniqueLabel As Boolean = ToleranceBuilder.UsingUniqueCaption(connection)
            Assert.AreEqual(expectedUniqueLabel, actualUniqueLabel, $"{NameOf(Entities.ToleranceBuilder)} '{NameOf(ToleranceBuilder.UsingUniqueCaption)}' should match")

        End Using
    End Sub

    #End Region

    #Region " UUT "

    ''' <summary> Assert adding uut entity. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="multimeterId">  Identifier for the multimeter. </param>
    ''' <param name="lotNumber">     The lot number. </param>
    ''' <param name="lastUutNumber"> The last uut number. </param>
    ''' <returns> An UutEntity. </returns>
    Public Shared Function AssertAddingUutEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                 ByVal multimeterId As Integer, ByVal lotNumber As String, ByVal lastUutNumber As Integer) As UutEntity
        Dim uut As UutEntity
        Dim lotUut As LotUutEntity
        Dim uutNumber As Integer
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = UutBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")

            tableName = LotUutBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")

            Dim actualObtained As Boolean
            lotUut = New LotUutEntity()
            r = lotUut.TryObtain(connection, lotNumber, -1, UutType.Chip)
            actualObtained = r.Success
            Assert.IsTrue(r.Success, $"{NameOf(Entities.LotUutEntity)} with '{NameOf(Entities.LotUutEntity.LotAutoId)}' of {lotNumber} should have been inserted; {r.Details}")
            r = lotUut.ValidateStoredEntity($"Insert {NameOf(Entities.LotUutEntity)} with '{NameOf(Entities.LotUutEntity.LotAutoId)}' of {lotNumber}")
            Assert.IsTrue(r.Success, r.Details)
            uutNumber = lotUut.UutEntity.UutNumber
            Assert.AreEqual(lastUutNumber + 1, uutNumber, $"{NameOf(Entities.LotUutEntity)} {NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutNumber)} should match")

            r = lotUut.TryObtainUut(connection, lotUut.LotAutoId, uutNumber, Entities.UutType.Chip)
            Assert.IsTrue(r.Success, r.Details)
            Assert.AreEqual(uutNumber, lotUut.UutEntity.UutNumber, $"Fetch {NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutNumber)} should match")

            Dim actualFetched As Boolean = False
            uut = lotUut.FetchUutEntity(connection)
            r = uut.ValidateStoredEntity($"Fetch {NameOf(Entities.UutEntity)} from {NameOf(Entities.LotUutEntity)} with '{NameOf(Entities.LotUutEntity.UutAutoId)}' of {lotUut.UutAutoId}")
            Assert.IsTrue(r.Success, r.Details)

            uut = LotUutEntity.FetchLastUut(connection, lotUut.LotAutoId)
            Assert.AreEqual(uutNumber, uut.UutNumber, $"Fetched last {NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutNumber)} should match")
            r = uut.ValidateStoredEntity($"Fetched last {NameOf(Entities.UutEntity)}")
            Assert.IsTrue(r.Success, r.Details)
            uut.FetchTraits(connection)
            uut.Traits.UutTrait.MultimeterId = multimeterId
            uut.Traits.Upsert(connection)
        End Using
        Return uut
    End Function

    ''' <summary> Assert adding uut entity. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <param name="site">          The site. </param>
    ''' <param name="provider">      The provider. </param>
    ''' <param name="lotAutoId">     Identifier for the <see cref="LotEntity"/>. </param>
    ''' <param name="lastUutNumber"> The last uut number. </param>
    ''' <returns> An UutEntity. </returns>
    Public Shared Function AssertAddingUutEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase,
                                                 ByVal lotAutoId As Integer, ByVal lastUutNumber As Integer) As UutEntity
        Dim uutNumber As Integer
        Dim r As (Success As Boolean, Details As String)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = UutBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")

            tableName = LotUutBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")

            Dim lotUut As New LotUutEntity With {.LotAutoId = lotAutoId}
            r = lotUut.TryObtainUut(connection, lotAutoId, -1, Entities.UutType.Chip)
            Assert.IsTrue(r.Success, r.Details)
            r = lotUut.ValidateStoredEntity($"Insert {NameOf(Entities.LotUutEntity)} with  [{NameOf(Entities.LotUutEntity.LotAutoId)},[{NameOf(Entities.LotUutEntity.UutAutoId)}] of [{lotAutoId},{lotUut.UutAutoId}]")
            Assert.IsTrue(r.Success, r.Details)
            uutNumber = lotUut.UutEntity.UutNumber
            Assert.AreEqual(lastUutNumber + 1, uutNumber, $"{NameOf(Entities.LotUutEntity)} {NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutNumber)} should match")

            r = lotUut.TryObtainUut(connection, lotAutoId, uutNumber, Entities.UutType.Chip)
            Assert.IsTrue(r.Success, r.Details)
            Assert.AreEqual(uutNumber, lotUut.UutEntity.UutNumber, $"Fetch {NameOf(Entities.UutEntity)} {NameOf(Entities.UutEntity.UutNumber)} should match")

            Return lotUut.UutEntity
        End Using
    End Function

    ''' <summary> Assert uut entity. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">       The site. </param>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="lotNumbers"> The lot numbers. </param>
    Public Shared Sub AssertUutEntity(ByVal site As isr.Core.MSTest.TestSiteBase, ByVal provider As ProviderBase, ByVal lotNumbers As String())
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            connection.DeleteAll(Of UutNub)
            connection.DeleteAll(Of LotUutNub)
            connection.DeleteAll(Of UutProductSortNub)
            Dim lotNumber As String
            Dim multimeterId As Integer = 1
            Dim expectedLastUutNumber As Integer = 0
            Dim uut As UutEntity = Nothing
            For Each lotNumber In lotNumbers
                expectedLastUutNumber = 0
                uut = Auditor.AssertAddingUutEntity(site, provider, multimeterId, lotNumber, expectedLastUutNumber)
                expectedLastUutNumber = uut.UutNumber
                Auditor.AssertAddingUutEntity(site, provider, multimeterId, lotNumber, expectedLastUutNumber)
            Next
        End Using
    End Sub

    #End Region

End Class

