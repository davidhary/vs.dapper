Imports Dapper.Contrib.Extensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities
Namespace Global.isr.Dapper.Entities.Tests.Ohmni.Morphe

    Partial Public Class TestSuite

        #Region " LOOKUP TABLES "

        ''' <summary> (Unit Test Method) tests Grade entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub GradeEntityTest()
            Auditor.AssertGradeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests measurement bin entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub MeasurementBinEntityTest()
            Auditor.AssertReadingBinEntity(TestSuite.TestInfo, Me.GetProvider, isr.Dapper.Entities.ReadingBin.Good)
        End Sub

        ''' <summary> (Unit Test Method) tests part specification type entity. </summary>
        ''' <remarks> David, 4/23/2020. </remarks>
        <TestMethod>
        Public Sub PartSpecificationTypeEntityTest()
            Auditor.AssertPartSpecificationTypeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests Polarity entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub PolarityEntityTest()
            Auditor.AssertPolarityEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests Structure Type entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub StructureTypeEntityTest()
            Auditor.AssertStructureTypeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests the Substrate Type entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub SubstrateTypeEntityTest()
            Auditor.AssertSubstrateTypeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests the test level entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub TestTypeEntityTest()
            Auditor.AssertTestTypeEntity(TestSuite.TestInfo, Me.GetProvider, isr.Dapper.Entities.TestType.Trim)
        End Sub

        ''' <summary> (Unit Test Method) tests Tolerance entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub ToleranceEntityTest()
            Auditor.AssertToleranceEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        #End Region

        #Region " PLATFORM "

#If False Then

        ''' <summary> (Unit Test Method) tests revision entity. </summary>
        ''' <remarks> David, 5/8/2020. </remarks>
        <TestMethod>
        Public Sub RevisionEntityTest()
            Auditor.AssertRevisionEntity(TestSuite.TestInfo, Me.GetProvider, TestSuite.BuiltDatabaseVersion)
        End Sub

        ''' <summary> (Unit Test Method) tests Platform entity. </summary>
        <TestMethod>
        Public Sub PlatformEntityTest()
            Auditor.AssertPlatformEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests station entity. </summary>
        <TestMethod>
        Public Sub StationEntityTest()
            Auditor.AssertStationEntity(TestSuite.TestInfo, Me.GetProvider, True)
        End Sub
#End If

        #End Region

        #Region " SESSION "

        ''' <summary> (Unit Test Method) tests session entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub SessionEntityTest()
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Auditor.AssertAddingSessionEntity(TestSuite.TestInfo, Me.GetProvider, $"{lotNumbers(0)}:1")
        End Sub

        ''' <summary> (Unit Test Method) tests part entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub PartEntityTest()
            Dim partNumbers As String() = New String() {"PFC-W0402LF-03-1000-B-4013",
                                                        "PFC-W0402LF-03-1001-B-4013",
                                                        "PFC-D1206LF-03-3202-1002-DB-2728",
                                                        "PFC-D1206LF-03-6122-2442-FB-2728",
                                                        "WIN-T0603LF-03-3091-B",
                                                        "WIN-T0603LF-03-2002-B",
                                                        "WIN-T0603LF-03-1002-B"}
            Auditor.AssertPartEntity(TestSuite.TestInfo, Me.GetProvider, partNumbers)
        End Sub

        ''' <summary> (Unit Test Method) tests lot entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub LotEntityTest()
            ' PFC-D1206LF-03-6122-2442-FB-2728  1725356 1770169
            ' PFC-W0402LF-03-1000-B-4013  1749153  1749156 1725105
            ' PFC-D1206LF-03-3202-1002-DB-2728 1733251 1733247  1733248  1733249
            Dim partNumber As String = "PFC-D1206LF-03-6122-2442-FB-2728"
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Auditor.AssertLotEntity(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumbers)
        End Sub

        ''' <summary> (Unit Test Method) tests substrate entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub SubstrateEntityTest()
            Dim partNumber As String = "PFC-D1206LF-03-3202-1002-DB-2728"
            Dim lotNumbers As String() = New String() {"1733251", "1733247"}
            Auditor.AssertSubstrateEntity(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumbers(0))
        End Sub

        ''' <summary> (Unit Test Method) tests Structure entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub StructureEntityTest()
            Dim partNumber As String = "PFC-D1206LF-03-3202-1002-DB-2728"
            Dim lotNumbers As String() = New String() {"1733251", "1733247"}
            Auditor.AssertSubstrateEntity(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumbers(0))
        End Sub

#If False Then

        ''' <summary> (Unit Test Method) tests Structure session entity. </summary>
        <TestMethod>
        Public Sub StructureSessionEntityTest()
            Dim partNumber As String = "PFC-D1206LF-03-6122-2442-FB-2728"
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Dim lotNumber As String = lotNumbers(0)
            Auditor.AssertSessionStructure(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumber, $"{lotNumber}:1")
        End Sub
#End If

        #End Region

        #Region " IMPORT "

        ''' <summary> (Unit Test Method) tests the <see cref="FileImportStateEntity"/>. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        <TestMethod>
        Public Sub FileImportStateEntityTest()
            Auditor.AssertFileImportStateEntity(TestSuite.TestInfo, Me.GetProvider, [Enum].GetValues(GetType(FileImportState)).Length)
        End Sub

        ''' <summary> (Unit Test Method) tests import file entity. </summary>
        ''' <remarks> David, 6/17/2020. </remarks>
        <TestMethod>
        Public Sub ImportFileEntityTest()
            Auditor.AssertImportFileEntity(TestSuite.TestInfo, Me.GetProvider, TestSuiteSettings.Get.SchemaFolderName, TestSuiteSettings.Get.ImportFileName)
        End Sub

        #End Region

    End Class

End Namespace
