
Imports Dapper
Imports isr.Dapper.Entity
Namespace Global.isr.Dapper.Entities.Tests.Ohmni.Morphe

    Partial Public Class TestSuite

        ''' <summary> Initializes this object. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="provider"> The provider. </param>
        Public Shared Sub Initialize(ByVal provider As ProviderBase)
            TestInfo.TraceMessage($".NET {Environment.Version}")
            TestInfo.TraceMessage($"Dapper {GetType(Global.Dapper.SqlMapper).AssemblyQualifiedName}")
            TestInfo.TraceMessage($"Connection string: {provider.ConnectionString}")
            TestSuite._Provider = provider
            ' test naked connection
            Assert.IsTrue(TestSuite.Provider.IsConnectionExists(), $"connection {TestSuite.Provider.GetConnection} does not exist")
            ' test connection with event handling
            Using connection As System.Data.IDbConnection = TestSuite.Provider.GetConnection()
                Assert.IsTrue(connection.Exists(), "connection does not exist")
            End Using
            If provider Is Nothing Then
                TestSuite._SchemaBuilder = New SchemaBuilder
            Else
                TestSuite._SchemaBuilder = New SchemaBuilder With {.Provider = provider}
                TestSuite.EnumerateSchemaObjects()
            End If
            TestSuite.BuildDatabase(provider.BuildMasterConnectionString(False))
            TestSuite.SchemaBuilder.AssertSchemaObjectsExist()
        End Sub

        ''' <summary> Cleanups this object. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Public Shared Sub Cleanup()
            If TableCategory.None <> TestSuiteSettings.Get.DeleteOption Then
                TestSuite.SchemaBuilder.DeleteAllRecords(TestSuiteSettings.Get.DeleteOption)
            End If
            If TableCategory.None <> TestSuiteSettings.Get.DropTablesOption Then
                TestSuite.SchemaBuilder.DropAllTables(TestSuiteSettings.Get.DropTablesOption, TestSuiteSettings.Get.IgnoreDropTableErrors)
            End If
        End Sub

        ''' <summary> Gets or sets the provider. </summary>
        ''' <value> The provider. </value>
        Public Shared ReadOnly Property Provider As ProviderBase

        ''' <summary> Gets or sets the schema builder. </summary>
        ''' <value> The schema builder. </value>
        Public Shared ReadOnly Property SchemaBuilder As SchemaBuilder

        ''' <summary> Enumerate schema objects. </summary>
        ''' <remarks> David, 2020-07-04. </remarks>
        Public Shared Sub EnumerateSchemaObjects()
            Try
                TestSuite.EnumerateSchemaObjectsThis()
            Catch
                TestSuite.SchemaBuilder.EnumeratedTables.Clear()
                Throw
            End Try
        End Sub

        ''' <summary> Enumerate schema objects this. </summary>
        ''' <remarks> David, 2020-07-04. </remarks>
        Private Shared Sub EnumerateSchemaObjectsThis()

            Dim providerFileLabel As String = If(Provider.ProviderType = ProviderType.SQLite, TestSuiteSettings.Get.SQLiteFileLabel, TestSuiteSettings.Get.SqlServerFileLabel)

            Dim schemaBuilder As SchemaBuilder = TestSuite.SchemaBuilder
            schemaBuilder.EnumeratedTables.Clear()
            Dim tb As New TableMaker

            ' CORE LOOKUP TABLES 
            tb = New TableMaker With {.TableName = ReadingBinBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf ReadingBinBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf ReadingBinBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = PolarityBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf PolarityBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf PolarityBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = TestTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf TestTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf TestTypeBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)


            tb = New TableMaker With {.TableName = ToleranceBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf ToleranceBuilder.CreateTable,
                                      .InsertAction = AddressOf ToleranceBuilder.InsertValues}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' FILE IMPORT 
            tb = New TableMaker With {.TableName = FileImportStateBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf FileImportStateBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf FileImportStateBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' IMPORT FILE
            tb = New TableMaker With {.TableName = Entities.ImportFileBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Return Entities.ImportFileBuilder.CreateTable(x, False)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)
#If False Then
                                      .InsertFileValuesAction = AddressOf Entities.ImportFileBuilder.InsertFileValues,
                                      .ValuesFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                                               String.Format(TestSuiteSettings.Get.AddImportFileRecordsFileNameFormat, providerFileLabel))}
#End If

            ' STATION
            tb = New TableMaker With {.TableName = ComputerBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Return ComputerBuilder.Get.CreateTable(x, True)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' if the station is part of a platform, make the station label non-unique to allow same name for different platforms.
            tb = New TableMaker With {.TableName = StationBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x)
                                                          Return StationBuilder.Get.CreateTable(x, True)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = ComputerStationBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf ComputerStationBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = SessionBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf SessionBuilder.Get.CreateTableNonUniqueLabel}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PART 
            tb = New TableMaker With {.TableName = PartBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf PartBuilder.Get.CreateTableUniqueLabel}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = PartNamingTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = Function(x)
                                                          Dim s As New PartNamingTypeBuilder
                                                          Return s.CreateTable(x)
                                                      End Function,
                                      .InsertAction = Function(x)
                                                          Dim s As New PartNamingTypeBuilder
                                                          Return s.InsertIgnoreDefaultRecords(x)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = LotBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf LotBuilder.Get.CreateTableUniqueLabel}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = PartLotBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf PartLotBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' SUBSTRATE
            tb = New TableMaker With {.TableName = GradeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf GradeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf GradeBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = SubstrateTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf SubstrateTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf SubstrateTypeBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = SubstrateBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x As System.Data.IDbConnection)
                                                          Dim s As New Entities.SubstrateBuilder
                                                          Return s.CreateTable(x, UniqueIndexOptions.None)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' SUB-STRUCTURES
            tb = New TableMaker With {.TableName = CrossEdgeBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf CrossEdgeBuilder.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = LineEdgeBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf LineEdgeBuilder.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

#If PCM Then
            tb = New TableMaker With {.TableName = LineStructureBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf LineStructureBuilder.CreateTable}
            TestSuite.TableBuilder.EnumeratedTables.Add(tb.TableName, tb)
#End If

            ' STRUCTURES
            tb = New TableMaker With {.TableName = StructureTypeBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf StructureTypeBuilder.Get.CreateTable,
                                      .InsertAction = AddressOf StructureTypeBuilder.Get.InsertIgnoreDefaultRecords}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = StructureBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = Function(x As System.Data.IDbConnection)
                                                          Dim s As New Entities.StructureBuilder
                                                          Return s.CreateTable(x, UniqueIndexOptions.None)
                                                      End Function}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = SessionStructureBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf SessionStructureBuilder.Get.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            ' PCM STRUCTURES
            tb = New TableMaker With {.TableName = CrossEdgeResistanceBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf CrossEdgeResistanceBuilder.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = LineEdgeResistanceBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf LineEdgeResistanceBuilder.CreateTable}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)
#If PCM Then
            tb = New TableMaker With {.TableName = StructureLineBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf StructureLineBuilder.CreateTable}
            TestSuite.TableBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = LineStructureNominalBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf LineStructureNominalBuilder.CreateTable}
            TestSuite.TableBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = StructureLineNominalBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf StructureLineNominalBuilder.CreateTable}
            TestSuite.TableBuilder.EnumeratedTables.Add(tb.TableName, tb)

            tb = New TableMaker With {.TableName = SubstrateLineStructureNominalBuilder.TableName, .Category = TableCategory.None,
                                      .CreateAction = AddressOf SubstrateLineStructureNominalBuilder.CreateTable}
            TestSuite.TableBuilder.EnumeratedTables.Add(tb.TableName, tb)
#End If

            ' REVISION
            TestSuite.BuiltDatabaseVersion = (New isr.Core.MyAssemblyInfo(GetType(TestSuite).Assembly)).Version.ToString(3)
            tb = New TableMaker With {.TableName = RevisionBuilder.TableName, .Category = TableCategory.Lookup,
                                      .CreateAction = AddressOf RevisionBuilder.Get.CreateTableUniqueLabel,
                                      .PostCreateAction = AddressOf RevisionBuilder.UpsertRevision,
                                      .PostCreateValue = TestSuite.BuiltDatabaseVersion}
            schemaBuilder.EnumeratedTables.Add(tb.TableName, tb)

        End Sub

        ''' <summary> Gets or sets the built database version. </summary>
        ''' <value> The built database version. </value>
        Public Shared Property BuiltDatabaseVersion As String

        ''' <summary> Creates data base. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        Public Shared Sub CreateDatabase(ByVal masterConnectionString As String)
            If TestSuiteSettings.Get.CreateDatabase Then
                If System.IO.File.Exists(TestSuite.Provider.FileName) Then System.IO.File.Delete(TestSuite.Provider.FileName)
                TestSuite.CreateDatabaseThis(masterConnectionString)
            ElseIf Not TestSuite.Provider.DatabaseExists Then
                TestSuite.CreateDatabaseThis(masterConnectionString)
            End If
        End Sub

        ''' <summary> Creates database. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Private Shared Function CreateDatabaseThis(ByVal masterConnectionString As String) As Boolean
            Dim result As Boolean = TestSuite.Provider.CreateDatabase(masterConnectionString, TestSuite.Provider.DatabaseName)
            Dim providerFileLabel As String = If(TestSuite.Provider.ProviderType = ProviderType.SQLite,
                                                    TestSuiteSettings.Get.SQLiteFileLabel,
                                                    TestSuiteSettings.Get.SqlServerFileLabel)
            Dim count As Integer
            Dim schemaFileName As String
            If result Then
                schemaFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                        String.Format(TestSuiteSettings.Get.SchemaFileNameFormat, providerFileLabel))
                count = TestSuite.Provider.ExecuteScript(schemaFileName)
                result = count > 0
            End If
            If result Then
                schemaFileName = System.IO.Path.Combine(TestSuiteSettings.Get.SchemaFolderName,
                                                        String.Format(TestSuiteSettings.Get.SchemaDataFileNameFormat, providerFileLabel))
                count = TestSuite.Provider.ExecuteScript(schemaFileName)
                result = count > 0
            End If
            Return result
        End Function

        ''' <summary> Create or clear database and add all tables. </summary>
        ''' <remarks> David, 3/23/2020. </remarks>
        ''' <param name="masterConnectionString"> The master connection string. </param>
        Public Shared Sub BuildDatabase(ByVal masterConnectionString As String)
            TestSuite.CreateDatabase(masterConnectionString)
            If TestSuiteSettings.Get.ClearDatabase Then TestSuite.SchemaBuilder.DropAllTables(TestSuiteSettings.Get.IgnoreDropTableErrors)
            TestSuite.SchemaBuilder.BuildSchema()
        End Sub
    End Class

End Namespace
