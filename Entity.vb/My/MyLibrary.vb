Imports System.ComponentModel

Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = ProjectTraceEventId.DapperEntity

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Dapper Entity Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Dapper Entity Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Dapper.Entity"

        ''' <summary> The test assembly strong name. </summary>
        Public Const TestAssemblyStrongName As String = "isr.Dapper.Entity.Tests,PublicKey=" & SolutionInfo.PublicKey

    End Class

    ''' <summary> Values that represent project trace event identifiers. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Enum ProjectTraceEventId

        ''' <summary> An enum constant representing the none option. </summary>
        <Description("Not specified")> None

        ''' <summary> . </summary>
        <Description("Dapper Entity")> DapperEntity = isr.Core.ProjectTraceEventId.Dapper

        ''' <summary> . </summary>
        <Description("Dapper Entities")> DapperEntities = isr.Core.ProjectTraceEventId.Dapper + &H1

        ''' <summary> . </summary>
        <Description("Dapper Ohmni")> DapperOhmni = isr.Core.ProjectTraceEventId.Dapper + &H2

        ''' <summary> . </summary>
        <Description("Dapper Connection UI")> DapperConnectionUI = isr.Core.ProjectTraceEventId.Dapper + &H3

        ''' <summary> . </summary>
        <Description("Dapper Entity UI")> DapperEntityUI = isr.Core.ProjectTraceEventId.Dapper + &H4
    End Enum

End Namespace
