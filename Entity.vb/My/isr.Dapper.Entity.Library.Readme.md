## ISR Dapper Entity<sub>&trade;</sub>: Entity Class Library for Dapper
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*2.2.7422 04/27/20*  
Adds generic entity base.

*2.0.7345 02/10/20*  
Uses readme.md version (2.0.204) as the current version.

*2.2.7209 09/27/19*  
User NUGET version (2.2.203) as the current version.
Restore role of cache while keeping the update proxy intact.

*1.60.7175 08/24/19*  
Updated to version 1.60

*1.50.6905 11/27/18*  
Initial release.

\(C\) 2018 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Dapper Libraries](https://bitbucket.org/davidhary/vs.dapper)  
[Micro Benchmark](http://jonskeet.uk/csharp/benchmark.html)
