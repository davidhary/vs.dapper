Imports System.ComponentModel

Imports Dapper.Contrib.Extensions

Imports isr.Core.Constructs

''' <summary> Defines the contract that must be implemented by view entities. </summary>
''' <remarks>
''' <typeparamref name="TEntityNub"/> Specifies the generic interface type implemented by the
''' <typeparamref name="TEntityNub"/>. The latter also implements IEquatable(Of TIEntity),
''' ITypedCloneable(Of TIEntity), Class.<para>
''' Note that, because of dapper construction of the
''' <see cref="SqlMapperExtensions.IProxy">proxy</see> for change tracking, having the interface
''' inherit from other interfaces, such as IEquitable, causes the Dapper Get() function to fail
''' on constructing the Proxy type.</para><para>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value.</para> <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para><para>
''' David, 4/25/2020, . from the legacy entity base. </para>
''' </remarks>
Public MustInherit Class ViewEntityBase(Of TIEntity As {Class}, TEntityNub As {TIEntity, IEquatable(Of TIEntity), ITypedCloneable(Of TIEntity), Class})
    Inherits isr.Core.Models.ViewModelBase
    Implements IEquatable(Of ViewEntityBase(Of TIEntity, TEntityNub)), IEquatable(Of TIEntity), ITypedCloneable(Of TIEntity)

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="ViewEntityBase" /> class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nub">   The entity nub to use for managing the store and the cache values. </param>
    ''' <param name="cache"> The cached value. </param>
    Protected Sub New(ByVal nub As TEntityNub, ByVal cache As TIEntity)
        MyBase.New
        Me.ICache = cache
        Me.Nub = nub
    End Sub

#End Region

#Region " CACHE "

    ''' <summary> Clears the cache. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Protected Sub ClearCache()
        Me.UpdateCache(Me.Nub.CreateNew)
    End Sub

    ''' <summary> Clears the internal cache and store. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Overridable Sub Clear()
        Me.ClearCache()
    End Sub

    ''' <summary> Updates the cache described by value. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The Part interface. </param>
    Public Overridable Sub UpdateCache(ByVal value As TIEntity)
        ' the cache must be restored for keeping the proxy happy for detecting change.
        Me.ICache = value
    End Sub

    ''' <summary> Gets the entity cache representing the current values of the entity. </summary>
    ''' <remarks>
    ''' The dapper entity (see base class) dirty sentinel indicates if the entity changed.
    ''' </remarks>
    ''' <value> The i cache. </value>
    Protected Property ICache As TIEntity

    ''' <summary> Gets the nub; Must not be nulled. </summary>
    ''' <value> The nub. </value>
    Private ReadOnly Property Nub As TEntityNub

    ''' <summary> Gets the cache. </summary>
    ''' <value> The cache. </value>
    Protected ReadOnly Property Cache As TEntityNub
        Get
            Return TryCast(Me.ICache, TEntityNub)
        End Get
    End Property

#End Region

#Region " ENTITIES "

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public MustOverride Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Indicates whether the current entity Cache is equal to another object Cache of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Function CacheEquals(other As TIEntity) As Boolean Implements IEquatable(Of TIEntity).Equals
        Return other IsNot Nothing AndAlso Me.Cache.Equals(other)
    End Function

    ''' <summary> Compares two entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  Specifies the entity to compare to. </param>
    ''' <param name="right"> Specifies the entity to compare. </param>
    ''' <returns> <c>True</c> if the entities are equal. </returns>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return ViewEntityBase(Of TIEntity, TEntityNub).Equals(TryCast(left, ViewEntityBase(Of TIEntity, TEntityNub)), TryCast(right, ViewEntityBase(Of TIEntity, TEntityNub)))
    End Function

    ''' <summary> Compares two entities. </summary>
    ''' <remarks> The two entities are the same if they have the same X and Y coordinates. </remarks>
    ''' <param name="left">  Specifies the entity to compare to. </param>
    ''' <param name="right"> Specifies the entity to compare. </param>
    ''' <returns> <c>True</c> if the entities are equal. </returns>
    Public Overloads Shared Function Equals(ByVal left As ViewEntityBase(Of TIEntity, TEntityNub), ByVal right As ViewEntityBase(Of TIEntity, TEntityNub)) As Boolean
        Return left IsNot Nothing AndAlso right IsNot Nothing AndAlso left.Equals(right)
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, ViewEntityBase(Of TIEntity, TEntityNub)))
    End Function

    ''' <summary> Compares two entities using their cached values. </summary>
    ''' <remarks> The two entities are the same if they have the same X1 and Y1 coordinates. </remarks>
    ''' <param name="other"> Specifies the other entity. </param>
    ''' <returns> <c>True</c> if the entities are equal. </returns>
    Public Overloads Function Equals(ByVal other As ViewEntityBase(Of TIEntity, TEntityNub)) As Boolean Implements IEquatable(Of ViewEntityBase(Of TIEntity, TEntityNub)).Equals
        Return other IsNot Nothing AndAlso Me.CacheEquals(other.ICache)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As ViewEntityBase(Of TIEntity, TEntityNub), ByVal right As ViewEntityBase(Of TIEntity, TEntityNub)) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As ViewEntityBase(Of TIEntity, TEntityNub), ByVal right As ViewEntityBase(Of TIEntity, TEntityNub)) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

#End Region

#Region " NOTIFY "

    ''' <summary> Synchronously (default) notifies field change on a different thread. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub NotifyFieldChanged(ByVal e As ComponentModel.PropertyChangedEventArgs)
        Me.NotifyPropertyChanged(e)
    End Sub

    ''' <summary> Synchronously (default) notifies Field change on a different thread. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    Protected Sub NotifyFieldChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then Me.NotifyFieldChanged(New ComponentModel.PropertyChangedEventArgs(name))
    End Sub

#End Region

#Region " I TYPED CLONEABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public MustOverride Function CreateNew() As TIEntity Implements ITypedCloneable(Of TIEntity).CreateNew

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public MustOverride Function CreateCopy() As TIEntity Implements ITypedCloneable(Of TIEntity).CreateCopy

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public MustOverride Sub CopyFrom(value As TIEntity) Implements ITypedCloneable(Of TIEntity).CopyFrom

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.CreateCopy()
    End Function

#End Region

End Class

