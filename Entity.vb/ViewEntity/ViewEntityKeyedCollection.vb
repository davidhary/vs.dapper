Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.Constructs

''' <summary>
''' Defines the contract that must be implemented by a collection of
''' <see cref="ViewEntityBase(Of TIEntity, TEntityNub)"/> entities.
''' </summary>
''' <remarks>
''' <typeparamref name="TEntityNub"/> Specifies the generic interface type implemented by the
''' <typeparamref name="TEntityNub"/>. The latter also implements IEquatable(Of TIEntity),
''' ITypedCloneable(Of TIEntity), Class.<para>
''' Note that, because of dapper construction of the
''' <see cref="SqlMapperExtensions.IProxy">proxy</see> for change tracking, having the interface
''' inherit from other interfaces, such as IEquitable, causes the Dapper Get() function to fail
''' on constructing the Proxy type.</para><para>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value.</para> <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public MustInherit Class ViewEntityKeyedCollection(Of TKey As Structure,
                                                  TIEntity As {Class},
                                                  TEntityNub As {TIEntity, IEquatable(Of TIEntity), ITypedCloneable(Of TIEntity), Class},
                                                  TEntity As ViewEntityBase(Of TIEntity, TEntityNub))
    Inherits ObjectModel.KeyedCollection(Of TKey, TEntity)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Protected Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 8/15/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Query if collection contains this item key. </summary>
    ''' <remarks> David, 8/15/2020. </remarks>
    ''' <param name="item"> The item. </param>
    ''' <returns> True if collection contains this item key; otherwise false. </returns>
    Public Function ContainsItemKey(ByVal item As TEntity) As Boolean
        Return Me.Contains(Me.GetKeyForItem(item))
    End Function

    ''' <summary> Adds if not exists. </summary>
    ''' <remarks> David, 8/15/2020. </remarks>
    ''' <param name="item"> The item. </param>
    Public Overridable Sub AddIfNotExists(ByVal item As TEntity)
        If Not Me.Contains(Me.GetKeyForItem(item)) Then
            Me.Add(item)
        End If
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 8/15/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IList(Of TEntity))
        If entities?.Any Then
            For Each item As TEntity In entities
                Me.AddIfNotExists(item)
            Next
        End If
    End Sub

End Class
