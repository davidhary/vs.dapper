Imports Dapper
Imports isr.Core.Constructs

Partial Public Class EntityKeyedCollection(Of TKey As Structure,
                                               TIEntity As {Class},
                                               TEntityNub As {TIEntity, IEquatable(Of TIEntity), ITypedCloneable(Of TIEntity), Class},
                                               TEntity As EntityBase(Of TIEntity, TEntityNub))

#Region " PROPERTIES "

    ''' <summary>
    ''' Enumerates the entity property names excluding computer and key properties.
    ''' </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entity property names in this
    ''' collection.
    ''' </returns>
    Private Function EnumerateEntityPropertyNames() As IList(Of String)
        Dim l As New List(Of String)
        For Each entityProperty As Reflection.PropertyInfo In GetType(TEntityNub).GetProperties
            If Not (Global.Dapper.Contrib.Extensions.Methods.IsKeyProperty(entityProperty) OrElse
                        Global.Dapper.Contrib.Extensions.Methods.IsComputedProperty(entityProperty)) Then
                ' ignore key or computed properties
                l.Add(entityProperty.Name)
            End If
        Next
        Return l
    End Function

    ''' <summary> List of names of the entity properties. </summary>
    Private _EntityPropertyNames As List(Of String)

    ''' <summary> Enumerates entity property names in this collection. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process entity property names in this
    ''' collection.
    ''' </returns>
    Public Function EntityPropertyNames() As IList(Of String)
        If Not Me._EntityPropertyNames?.Any Then
            Me._EntityPropertyNames = New List(Of String)(Me.EnumerateEntityPropertyNames)
        End If
        Return Me._EntityPropertyNames
    End Function

    ''' <summary> Builds parenthesis entity property names. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="prefix"> The prefix. </param>
    ''' <returns> A String. </returns>
    Private Function BuildParenthesisEntityPropertyNames(ByVal prefix As String) As String
        Dim builder As New System.Text.StringBuilder
        builder.Append($"(")
        For Each name As String In Me.EntityPropertyNames
            If builder.Length > 1 Then builder.Append(",")
            builder.Append($"{prefix}{name}")
        Next
        builder.Append($")")
        Return builder.ToString
    End Function

    ''' <summary> List of names of the parenthesis at entity properties. </summary>
    Private _ParenthesisAtEntityPropertyNames As String

    ''' <summary> Parenthesis At Entity property names. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Function ParenthesisAtEntityPropertyNames() As String
        If String.IsNullOrEmpty(Me._ParenthesisAtEntityPropertyNames) Then
            Me._ParenthesisAtEntityPropertyNames = Me.BuildParenthesisEntityPropertyNames("@")
        End If
        Return Me._ParenthesisAtEntityPropertyNames
    End Function

    ''' <summary> List of names of the parenthesis entity properties. </summary>
    Private _ParenthesisEntityPropertyNames As String

    ''' <summary> Parenthesis Entity property names. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Function ParenthesisEntityPropertyNames() As String
        If String.IsNullOrEmpty(Me._ParenthesisEntityPropertyNames) Then
            Me._ParenthesisEntityPropertyNames = Me.BuildParenthesisEntityPropertyNames(String.Empty)
        End If
        Return Me._ParenthesisEntityPropertyNames
    End Function

#End Region

#Region " BULK OPERATIONS "

    ''' <summary> Builds bulk insert command. </summary>
    ''' <remarks>
    ''' David, 5/13/2020.
    ''' https://stackoverflow.com/questions/10689779/bulk-inserts-taking-longer-than-expected-using-dapper/12609410#12609410.
    ''' </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <returns> A String. </returns>
    Public Function BuildBulkInsertCommand(ByVal tableName As String) As String
        Dim builder As New System.Text.StringBuilder
        builder.AppendLine($"Insert {tableName}{Me.ParenthesisEntityPropertyNames}")
        builder.AppendLine($"Values {Me.ParenthesisAtEntityPropertyNames}")
        Return builder.ToString
    End Function

    ''' <summary> Bulk insert. </summary>
    ''' <remarks>
    ''' David, 5/13/2020.
    ''' https://stackoverflow.com/questions/10689779/bulk-inserts-taking-longer-than-expected-using-dapper/12609410#12609410.
    ''' </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="entities">   The entities. </param>
    Public Sub BulkInsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String, ByVal entities As IEnumerable(Of TIEntity))
        Dim transaction As Data.IDbTransaction = Nothing
        Dim wasOpen As Boolean = connection.IsOpen
        Try
            If Not wasOpen Then connection.Open()
            Dim command As String = Me.BuildBulkInsertCommand(tableName)
            transaction = connection.BeginTransaction()
            connection.Execute(command, entities, transaction)
            transaction.Commit()
        Catch
            transaction.Rollback()
            Throw
        Finally
            If Not wasOpen Then connection.Close()
        End Try
    End Sub


#End Region


End Class
