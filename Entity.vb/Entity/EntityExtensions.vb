Imports System.Runtime.CompilerServices

Namespace EntityExtensions

    Public Module Methods

        ''' <summary> Enumerate entity field names. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="type"> The type. </param>
        ''' <returns> A list of. </returns>
        <Extension>
        Public Function EnumerateEntityFieldNames(ByVal type As Type) As IList(Of String)
            Dim l As New List(Of String)
            For Each entityProperty As Reflection.PropertyInfo In type.GetProperties
                l.Add(entityProperty.Name)
            Next
            Return l
        End Function

    End Module

End Namespace
