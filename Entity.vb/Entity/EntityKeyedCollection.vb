Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.Constructs

''' <summary>
''' Defines the contract that must be implemented by a collection of
''' <see cref="EntityBase(Of TIEntity, TEntityNub)"/> entities.
''' </summary>
''' <remarks>
''' <typeparamref name="TEntityNub"/> Specifies the generic interface type implemented by the
''' <typeparamref name="TEntityNub"/>. The latter also implements IEquatable(Of TIEntity),
''' ITypedCloneable(Of TIEntity), Class.<para>
''' Note that, because of dapper construction of the
''' <see cref="SqlMapperExtensions.IProxy">proxy</see> for change tracking, having the interface
''' inherit from other interfaces, such as IEquitable, causes the Dapper Get() function to fail
''' on constructing the Proxy type.</para><para>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value.</para> <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public MustInherit Class EntityKeyedCollection(Of TKey As Structure,
                                                  TIEntity As {Class},
                                                  TEntityNub As {TIEntity, IEquatable(Of TIEntity), ITypedCloneable(Of TIEntity), Class},
                                                  TEntity As EntityBase(Of TIEntity, TEntityNub))
    Inherits ObjectModel.KeyedCollection(Of TKey, TEntity)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Protected Sub New()
        MyBase.New
        Me._UnsavedKeys = New List(Of TKey)
    End Sub

    ''' <summary>
    ''' Cleans all entities marking them as
    ''' <see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/> stored.
    ''' </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    Public Sub Clean()
        For Each entity As TEntity In Me
            entity.UpdateStore()
        Next
    End Sub

    ''' <summary> The unsaved keys. </summary>
    Private ReadOnly _UnsavedKeys As List(Of TKey)

    ''' <summary> Gets the unsaved keys. </summary>
    ''' <value> The unsaved keys. </value>
    Public ReadOnly Property UnsavedKeys As IList(Of TKey)
        Get
            Return Me._UnsavedKeys
        End Get
    End Property

    ''' <summary> Clears the unsaved keys. </summary>
    ''' <remarks> David, 6/19/2020. </remarks>
    Protected Sub ClearUnsavedKeys()
        Me._UnsavedKeys.Clear()
    End Sub

    ''' <summary> Adds an unsaved key. </summary>
    ''' <remarks> David, 6/19/2020. </remarks>
    ''' <param name="key"> The key. </param>
    Protected Sub AddUnsavedKey(ByVal key As TKey)
        Me._UnsavedKeys.Add(key)
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection. </summary>
    ''' <remarks>
    ''' David, 5/13/2020. Entities that failed to save are enumerated in <see cref="UnsavedKeys"/>
    ''' </remarks>
    ''' <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <CLSCompliant(False)>
    Public Overridable Function Upsert(ByVal transcactedConnection As TransactedConnection) As Boolean
        Me.ClearUnsavedKeys()
        ' Dictionary is instantiated only after the collection has values.
        If Not Me.Any Then Return True
        For Each keyValue As KeyValuePair(Of TKey, TEntity) In Me.Dictionary
            If Not keyValue.Value.Upsert(transcactedConnection) Then
                Me.AddUnsavedKey(keyValue.Key)
            End If
        Next
        ' success if no unsaved keys
        Return Not Me.UnsavedKeys.Any
    End Function

    ''' <summary> Inserts or updates all entities using the given connection. </summary>
    ''' <remarks>
    ''' David, 5/13/2020. Entities that failed to save are enumerated in <see cref="UnsavedKeys"/>
    ''' </remarks>
    ''' <param name="connection"> The connection to save. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function Upsert(ByVal connection As System.Data.IDbConnection) As Boolean
        If Me.Any Then
            Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
            If transactedConnection Is Nothing Then
                Dim wasOpen As Boolean = connection.IsOpen
                Try
                    If Not wasOpen Then connection.Open()
                    Using transaction As Data.IDbTransaction = connection.BeginTransaction
                        Try
                            Me.Upsert(New TransactedConnection(connection, transaction))
                            transaction.Commit()
                        Catch
                            transaction.Rollback()
                            Throw
                        Finally
                        End Try
                    End Using
                Catch
                    Throw
                Finally
                    If Not wasOpen Then connection.Close()
                End Try
            Else
                Me.Upsert(transactedConnection)
            End If
        End If
        ' success if no unsaved keys
        Return Not Me.UnsavedKeys.Any
    End Function

    ''' <summary> Inserts or updates all entities using the given connection. </summary>
    ''' <remarks>
    ''' David, 5/13/2020. Entities that failed to save are enumerated in <see cref="UnsavedKeys"/>
    ''' </remarks>
    ''' <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
    ''' <param name="force">                 True to force an update even if entity is not 'Dirty'. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <CLSCompliant(False)>
    Public Overridable Function Upsert(ByVal transcactedConnection As TransactedConnection, ByVal force As Boolean) As Boolean
        ' Dictionary is instantiated only after the collection has values.
        If Not Me.Any Then Return True
        Me.ClearUnsavedKeys()
        For Each keyValue As KeyValuePair(Of TKey, TEntity) In Me.Dictionary
            If Not keyValue.Value.Upsert(transcactedConnection, force) Then
                Me.AddUnsavedKey(keyValue.Key)
            End If
        Next
        ' success if no unsaved keys
        Return Not Me.UnsavedKeys.Any
    End Function

    ''' <summary> Inserts or updates all entities using the given connection. </summary>
    ''' <remarks>
    ''' David, 5/13/2020. Entities that failed to save are enumerated in <see cref="UnsavedKeys"/>
    ''' </remarks>
    ''' <param name="connection"> The connection to save. </param>
    ''' <param name="force">      True to force an update even if entity is not 'Dirty'. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal force As Boolean) As Boolean
        If Me.Any Then
            Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
            If transactedConnection Is Nothing Then
                Dim wasOpen As Boolean = connection.IsOpen
                Try
                    If Not wasOpen Then connection.Open()
                    Using transaction As Data.IDbTransaction = connection.BeginTransaction
                        Try
                            Me.Upsert(New TransactedConnection(connection, transaction), force)
                            transaction.Commit()
                        Catch
                            transaction.Rollback()
                            Throw
                        Finally
                        End Try
                    End Using
                Catch
                    Throw
                Finally
                    If Not wasOpen Then connection.Close()
                End Try
            Else
                Me.Upsert(transactedConnection, force)
            End If
        End If
        ' success if no unsaved keys
        Return Not Me.UnsavedKeys.Any
    End Function

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected MustOverride Function BulkUpsertThis(ByVal connection As System.Data.IDbConnection) As Integer

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 7/10/2020. </remarks>
    ''' <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    <CLSCompliant(False)>
    Public Overridable Function BulkUpsert(ByVal transcactedConnection As TransactedConnection) As Integer
        Dim count As Integer
        If Me.Any Then
            Me.ClearUnsavedKeys()
            count = Me.BulkUpsertThis(transcactedConnection)
        End If
        Return count
    End Function

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Public Function BulkUpsert(ByVal connection As System.Data.IDbConnection) As Integer
        Dim count As Integer
        If Me.Any Then
            Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
            If transactedConnection Is Nothing Then
                Dim wasOpen As Boolean = connection.IsOpen
                Try
                    If Not wasOpen Then connection.Open()
                    Using transaction As Data.IDbTransaction = connection.BeginTransaction
                        Try
                            count = Me.BulkUpsert(New TransactedConnection(connection, transaction))
                            transaction.Commit()
                        Catch
                            transaction.Rollback()
                            Throw
                        Finally
                        End Try
                    End Using
                Catch
                    Throw
                Finally
                    If Not wasOpen Then connection.Close()
                End Try
            Else
                count = Me.BulkUpsert(transactedConnection)
            End If
        End If
        Return count
    End Function

End Class
