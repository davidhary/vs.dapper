Imports System.ComponentModel

Imports Dapper.Contrib.Extensions

Imports isr.Core.Constructs

''' <summary> Defines the contract that must be implemented by entities. </summary>
''' <remarks>
''' <typeparamref name="TEntityNub"/> Specifies the generic interface type implemented by the
''' <typeparamref name="TEntityNub"/>. The latter also implements IEquatable(Of TIEntity),
''' ITypedCloneable(Of TIEntity), Class.<para>
''' Note that, because of dapper construction of the
''' <see cref="SqlMapperExtensions.IProxy">proxy</see> for change tracking, having the interface
''' inherit from other interfaces, such as IEquitable, causes the Dapper Get() function to fail
''' on constructing the Proxy type.</para><para>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value.</para> <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para><para>
''' David, 4/25/2020, . from the legacy entity base. </para>
''' </remarks>
Public MustInherit Class EntityBase(Of TIEntity As {Class}, TEntityNub As {TIEntity, IEquatable(Of TIEntity), ITypedCloneable(Of TIEntity), Class})
    Inherits isr.Core.Models.ViewModelBase
    Implements IEquatable(Of EntityBase(Of TIEntity, TEntityNub)), IEquatable(Of TIEntity), ITypedCloneable(Of TIEntity)

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="EntityBase" /> class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nub">   The entity nub to use for managing the store and the cache values. </param>
    ''' <param name="cache"> The cached value. </param>
    Protected Sub New(ByVal nub As TEntityNub, ByVal cache As TIEntity)
        ' this tags the entity is new
        Me.New(nub, cache, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks>
    ''' The <paramref name="nub"/> is required because with native tracking, the
    ''' <see cref="IsDirtyProxy"/> property is added to the cached interface, which can no longer be
    ''' cast to the
    ''' <typeparamref name="TEntityNub"/>.
    ''' </remarks>
    ''' <param name="nub">   The entity nub to use for managing the store and the cache values. </param>
    ''' <param name="cache"> The cached value. </param>
    ''' <param name="store"> The stored value. </param>
    Protected Sub New(ByVal nub As TEntityNub, ByVal cache As TIEntity, ByVal store As TIEntity)
        MyBase.New
        Me.ICache = cache
        ' marks this as new
        Me.IStore = store
        Me.Nub = nub
    End Sub

#End Region

#Region " CHANGE TRACKING "

    ''' <summary> Gets or sets the proxy. </summary>
    ''' <remarks>
    ''' Change tracking works for Get and Update of interface types.<para>
    ''' Dapper native change tracking does not work when fetching entities as classes. </para><para>
    ''' When <see cref="UsingNativeTracking"/> is <c>True</c> this class stores and retrieves records
    ''' as interfaces, otherwise, records are accessed using the <see cref="Cache"/> and update
    ''' tracking is based on the class structures. Entity classes must be used for database access
    ''' when the table name is specified in the class Table attribute and is different from the class
    ''' interface name.</para>
    ''' </remarks>
    ''' <value> The proxy holding the dirty status of an entity. </value>
    <CLSCompliant(False)>
    Private Property IProxy As SqlMapperExtensions.IProxy

    ''' <summary>
    ''' Gets or sets an indication if the entity has changed since it was stored using the
    ''' <see cref="IProxy"/>.
    ''' </summary>
    ''' <value> The indication that the entity has changed since it was stored. </value>
    Protected Property IsDirtyProxy As Boolean
        Get
            Me.IProxy = TryCast(Me.ICache, SqlMapperExtensions.IProxy)
            Return Me.IProxy IsNot Nothing AndAlso Me.IProxy.IsDirty
        End Get
        Set(value As Boolean)
            Me.IProxy = TryCast(Me.ICache, SqlMapperExtensions.IProxy)
            If Me.IProxy IsNot Nothing Then Me.IProxy.IsDirty = value
        End Set
    End Property

    ''' <summary>
    ''' Determines if the entity has changed since it was stored by comparing the
    ''' <see cref="iStore">database value</see> <see cref="ICache">cache value</see> .
    ''' </summary>
    ''' <remarks> David, 4/25/2020. </remarks>
    ''' <returns> True if dirty cache, false if not. </returns>
    Public Function IsDirtyCache() As Boolean
        Return Not Me.IsNew AndAlso Not Me.CacheEquals(Me.IStore)
    End Function

    ''' <summary> Determines if the entity has changed since it was stored. </summary>
    ''' <remarks> David, 4/25/2020. </remarks>
    ''' <returns> True if dirty, false if not. </returns>
    Public Function IsDirty() As Boolean
        Return Me.IsDirtyProxy OrElse Me.IsDirtyCache
    End Function

    ''' <summary> Determines if the entity is new using the <see cref="IProxy"/>. </summary>
    ''' <remarks> David, 4/25/2020. </remarks>
    ''' <returns> True if new proxy, false if not. </returns>
    Protected Function IsNewProxy() As Boolean
        Me.IProxy = TryCast(Me.ICache, SqlMapperExtensions.IProxy)
        Return Me.IProxy Is Nothing
    End Function

    ''' <summary> Determines if the entity has a new store. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> <c>True</c>  if the entity store is new; Otherwise, <c>False</c>. </returns>
    Protected Function IsNewStore() As Boolean
        Return Me.IStore Is Nothing
    End Function

    ''' <summary> Determines if the entity is new. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> <c>True</c>  if the entity is new; Otherwise, <c>False</c>. </returns>
    Public Function IsNew() As Boolean
        Return Me.IsNewProxy AndAlso Me.IsNewStore
    End Function

    ''' <summary> Determines if the entity is not new or dirty. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> <c>True</c>  if the entity is not new or dirty; Otherwise, <c>False</c>. </returns>
    Public Function IsClean() As Boolean
        Return Not (Me.IsNew OrElse Me.IsDirty)
    End Function

#End Region

#Region " STORE AND CACHE "

    ''' <summary> Updates the store, which holds a record of the stored fields. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
    ''' </returns>
    Public Function UpdateStore() As Boolean
        Me.IStore = Me.Cache.CreateCopy
        Return Me.IsClean
    End Function

    ''' <summary> Clears the cache. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Protected Sub ClearCache()
        Me.UpdateCache(Me.Nub.CreateNew)
    End Sub

    ''' <summary> Clears the internal cache and store. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Overridable Sub Clear()
        Me.ClearCache()
        Me.ClearStore()
    End Sub

    ''' <summary> Clears the store. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Protected Sub ClearStore()
        Me.IStore = Nothing
    End Sub

    ''' <summary> Updates the cache described by value. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The Part interface. </param>
    Public Overridable Sub UpdateCache(ByVal value As TIEntity)
        ' the cache must be restored for keeping the proxy happy for detecting change.
        Me.ICache = value
    End Sub

    ''' <summary> Gets the entity as last stored in or fetched from the data store. </summary>
    ''' <remarks>
    ''' The dapper entity (see base class) dirty sentinel indicates if the entity changed.
    ''' </remarks>
    ''' <value> The i part. </value>
    Private Property IStore As TIEntity

    ''' <summary> Gets the entity cache representing the current values of the entity. </summary>
    ''' <remarks>
    ''' The dapper entity (see base class) dirty sentinel indicates if the entity changed.
    ''' </remarks>
    ''' <value> The i cache. </value>
    Protected Property ICache As TIEntity

    ''' <summary> Gets the nub; Must not be nulled. </summary>
    ''' <remarks>
    ''' The Nub was added to allow casting the <typeparamref name="TIEntity"/>
    ''' to <typeparamref name="TEntityNub"/> with native tracking, which sets the
    ''' <see cref="ICache"/> to <see cref="IProxy"/> adding the <see cref="IsDirtyProxy"/> field to
    ''' the <see cref="ICache"/> thus breaking the interface implementation between to the type
    ''' parameters.
    ''' </remarks>
    ''' <value> The nub. </value>
    Private ReadOnly Property Nub As TEntityNub

    ''' <summary> Gets the cache. </summary>
    ''' <value> The cache. </value>
    Protected ReadOnly Property Cache As TEntityNub
        Get
            If Me.UsingNativeTracking Then
                ' Because native tracking copies the Proxy into the cache, casting the cache from 
                ' interface to Nub returns nothing because the proxy adds the <see cref="IsDirtyProxy"/> 
                ' field to <typeparamref name="TIEntity"/> when using native tracking,
                Me.Nub.CopyFrom(Me.ICache)
                Return Me.Nub
            Else
                Return TryCast(Me.ICache, TEntityNub)
            End If
        End Get
    End Property

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Gets or sets the using native tracking. </summary>
    ''' <remarks>
    ''' Allows using interface storage and retrieval. this assumes that the database table name
    ''' matches the Interface name.
    ''' </remarks>
    ''' <value> The using native tracking. </value>
    Public Property UsingNativeTracking As Boolean

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean

    ''' <summary>
    ''' Updates the <see cref="ICache"/> and the <see cref="IStore"/> if <paramref name="entity"/> is
    ''' not null.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="entity"> The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
    ''' </returns>
    Public Function Enstore(ByVal entity As TIEntity) As Boolean
        If entity Is Nothing Then
            Me.ClearCache()
            Me.ClearStore()
            Return False
        Else
            Me.UpdateCache(entity)
            Return Me.UpdateStore()
        End If
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="ICache"/> using given connection thus
    ''' preserving tracking. Must be overridden by entities with computed fields in order to fetch
    ''' the stored entity.
    ''' </summary>
    ''' <remarks> David, 4/25/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
    ''' </returns>
    Public Overridable Function Insert(ByVal connection As System.Data.IDbConnection) As Boolean
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Me.ClearStore()
        ' With an identity key, SQLite Insert return a numeric primary key; SQL server returns a numeric Identity key.
        ' With a primary key, SQLite insert returns the row number; SQL Server insert returns zero, in which case, 
        ' it is assumed the insert succeeded if an exception has not been thrown.
        If Me.UsingNativeTracking Then
            connection.Insert(Me.ICache)
        Else
            connection.Insert(Me.Cache)
        End If
        Return Me.UpdateStore()
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="ICache"/> using given connection thus
    ''' preserving tracking. Must be overridden by entities with computed fields in order to fetch
    ''' the stored entity.
    ''' </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
    ''' </returns>
    Public Overridable Function Insert(ByVal connection As System.Data.IDbConnection, ByVal entity As TIEntity) As Boolean
        Me.ICache = entity
        Return Me.Insert(connection)
    End Function

    ''' <summary>
    ''' Obtains the entity using the unique index or unique key if not indexed; inserts if entity
    ''' does not exist.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
    ''' </returns>
    Public Overridable Function Obtain(ByVal connection As System.Data.IDbConnection) As Boolean
        Dim temp As TIEntity = Me.CreateCopy()
        Return Me.FetchUsingUniqueIndex(connection) OrElse Me.Insert(connection, temp)
    End Function

    ''' <summary> Updates or inserts the entity. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function Upsert(ByVal connection As System.Data.IDbConnection) As Boolean
        Return If(Me.IsNew, Me.Obtain(connection), Not Me.IsDirty OrElse Me.Update(connection))
    End Function

    ''' <summary> Updates or inserts the entity optionally forcing an update. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="force">      True to force an update overriding the proxy settings. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal force As Boolean) As Boolean
        ' Return If(Me.IsNew, Me.Insert(connection), Me.Update(connection, force))
        Return If(Me.IsNew, Me.Obtain(connection), Me.Update(connection, force))
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As TIEntity) As Boolean

    ''' <summary> Deletes a record of this entity and clears the entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if the entity was deleted; otherwise <c>false</c> </returns>
    Public Function Delete(ByVal connection As System.Data.IDbConnection) As Boolean
        If Me.IsClean Then
            Me.ClearStore()
            If connection.Delete(Of TEntityNub)(Me.Cache) Then Me.ClearCache()
            Return Me.IsNew
        Else
            Throw New InvalidOperationException("Entity was not stored")
        End If
    End Function

    ''' <summary> Updates this. </summary>
    ''' <remarks> David, 6/3/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Private Function UpdateThis(ByVal connection As System.Data.IDbConnection) As Boolean
        Return If(Me.UsingNativeTracking, connection.Update(Me.ICache), Me.IsDirty AndAlso connection.Update(Me.Cache))
    End Function

    ''' <summary> Updates the entity; refetches if the entity was updated. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
    ''' </returns>
    Public Function Update(ByVal connection As System.Data.IDbConnection) As Boolean
        If Me.UpdateThis(connection) Then
            ' at this time, Is Dirty gets cleared only after a refetch
            Return Me.FetchUsingKey(connection)
        Else
            Return Me.IsClean
        End If
    End Function

    ''' <summary> Updates the entity; refetches if the entity was updated. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="force">      True to force an update overriding the proxy settings. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
    ''' </returns>
    Public Function Update(ByVal connection As System.Data.IDbConnection, ByVal force As Boolean) As Boolean
        Dim updated As Boolean
        If force AndAlso Me.UsingNativeTracking Then
            Me.IsDirtyProxy = True
            updated = connection.Update(Me.ICache)
        ElseIf force Then
            updated = connection.Update(Me.Cache)
        Else
            updated = Me.UpdateThis(connection)
        End If
        If updated Then
            ' at this time, Is Dirty gets cleared only after a refetch
            Return Me.FetchUsingKey(connection)
        Else
            Return Me.IsClean
        End If
    End Function

    ''' <summary> Updates the entity. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="updateMode"> The update mode. </param>
    ''' <returns> <c>true</c> if the record was updated; otherwise <c>false</c> </returns>
    Public Function Update(ByVal connection As System.Data.IDbConnection, ByVal updateMode As UpdateModes) As Boolean
        Dim updated As Boolean = False
        If 0 = (UpdateModes.ForceUpdate And updateMode) Then
            updated = If(Me.UsingNativeTracking, connection.Update(Me.ICache), Me.IsDirty AndAlso connection.Update(Me.Cache))
        ElseIf UpdateModes.ForceUpdate = (UpdateModes.ForceUpdate And updateMode) Then
            If Me.UsingNativeTracking Then
                Me.IsDirtyProxy = True
                updated = connection.Update(Me.ICache)
            Else
                updated = connection.Update(Me.Cache)
            End If
        End If
        If updated AndAlso UpdateModes.Refetch = (UpdateModes.Refetch And updateMode) Then
            ' at this time, Is Dirty gets cleared only after a refetch
            Me.FetchUsingKey(connection)
        End If
        Return updated
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public MustOverride Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Indicates whether the current entity store is equal to another object store of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Function StoreEquals(other As TIEntity) As Boolean
        Return other IsNot Nothing AndAlso TryCast(Me.IStore, TEntityNub).Equals(other)
    End Function

    ''' <summary>
    ''' Indicates whether the current entity Cache is equal to another object Cache of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Function CacheEquals(other As TIEntity) As Boolean Implements IEquatable(Of TIEntity).Equals
        Return other IsNot Nothing AndAlso Me.Cache.Equals(other)
    End Function

    ''' <summary> Compares two entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  Specifies the entity to compare to. </param>
    ''' <param name="right"> Specifies the entity to compare. </param>
    ''' <returns> <c>True</c> if the entities are equal. </returns>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return EntityBase(Of TIEntity, TEntityNub).Equals(TryCast(left, EntityBase(Of TIEntity, TEntityNub)), TryCast(right, EntityBase(Of TIEntity, TEntityNub)))
    End Function

    ''' <summary> Compares two entities. </summary>
    ''' <remarks> The two entities are the same if they have the same X and Y coordinates. </remarks>
    ''' <param name="left">  Specifies the entity to compare to. </param>
    ''' <param name="right"> Specifies the entity to compare. </param>
    ''' <returns> <c>True</c> if the entities are equal. </returns>
    Public Overloads Shared Function Equals(ByVal left As EntityBase(Of TIEntity, TEntityNub), ByVal right As EntityBase(Of TIEntity, TEntityNub)) As Boolean
        Return left IsNot Nothing AndAlso right IsNot Nothing AndAlso left.Equals(right)
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, EntityBase(Of TIEntity, TEntityNub)))
    End Function

    ''' <summary> Compares two entities using their cached values. </summary>
    ''' <remarks> The two entities are the same if they have the same X1 and Y1 coordinates. </remarks>
    ''' <param name="other"> Specifies the other entity. </param>
    ''' <returns> <c>True</c> if the entities are equal. </returns>
    Public Overloads Function Equals(ByVal other As EntityBase(Of TIEntity, TEntityNub)) As Boolean Implements IEquatable(Of EntityBase(Of TIEntity, TEntityNub)).Equals
        Return other IsNot Nothing AndAlso Me.CacheEquals(other.ICache)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As EntityBase(Of TIEntity, TEntityNub), ByVal right As EntityBase(Of TIEntity, TEntityNub)) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As EntityBase(Of TIEntity, TEntityNub), ByVal right As EntityBase(Of TIEntity, TEntityNub)) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

#End Region

#Region " NOTIFY "

    ''' <summary> Synchronously (default) notifies field change on a different thread. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub NotifyFieldChanged(ByVal e As ComponentModel.PropertyChangedEventArgs)
        Me.NotifyPropertyChanged(e)
    End Sub

    ''' <summary> Synchronously (default) notifies Field change on a different thread. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    Protected Sub NotifyFieldChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then Me.NotifyFieldChanged(New ComponentModel.PropertyChangedEventArgs(name))
    End Sub

#End Region

#Region " VALIDATE "

    ''' <summary> Validates this entity as new. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="identifier"> The identifier. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function ValidateNewEntity(ByVal identifier As String) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim expectedOutcome As Boolean
        Dim outcomeName As String

        outcomeName = NameOf(EntityBase(Of TIEntity, TEntityNub).IsNew)
        expectedOutcome = Me.IsNew
        If result.Success Then result = (expectedOutcome, If(expectedOutcome, String.Empty, $"{identifier} should be tagged as '{outcomeName}'"))

        outcomeName = $"not {NameOf(EntityBase(Of TIEntity, TEntityNub).IsDirty)}"
        expectedOutcome = Not Me.IsDirty
        If result.Success Then result = (expectedOutcome, If(expectedOutcome, String.Empty, $"{identifier} should be tagged as '{outcomeName}'"))

        outcomeName = $"not {NameOf(EntityBase(Of TIEntity, TEntityNub).IsClean)}"
        expectedOutcome = Not Me.IsClean
        If result.Success Then result = (expectedOutcome, If(expectedOutcome, String.Empty, $"{identifier} should be tagged as '{outcomeName}'"))

        Return result
    End Function

    ''' <summary> Validates this entity as stored. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="identifier"> The identifier. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function ValidateStoredEntity(ByVal identifier As String) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim expectedOutcome As Boolean
        Dim outcomeName As String
        outcomeName = $"not {NameOf(EntityBase(Of TIEntity, TEntityNub).IsNew)}"
        expectedOutcome = Not Me.IsNew
        If result.Success Then result = (expectedOutcome, If(expectedOutcome, String.Empty, $"{identifier} should be tagged as '{outcomeName}'"))

        outcomeName = $"not {NameOf(EntityBase(Of TIEntity, TEntityNub).IsDirty)}"
        expectedOutcome = Not Me.IsDirty
        If result.Success Then result = (expectedOutcome, If(expectedOutcome, String.Empty, $"{identifier} should be tagged as '{outcomeName}'"))

        outcomeName = NameOf(EntityBase(Of TIEntity, TEntityNub).IsClean)
        expectedOutcome = Me.IsClean
        If result.Success Then result = (expectedOutcome, If(expectedOutcome, String.Empty, $"{identifier} should be tagged as '{outcomeName}'"))

        Return result
    End Function

    ''' <summary> Validates this entity as changed. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="identifier"> The identifier. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function ValidateChangedEntity(ByVal identifier As String) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim expectedOutcome As Boolean
        Dim outcomeName As String

        outcomeName = $"not {NameOf(EntityBase(Of TIEntity, TEntityNub).IsNew)}"
        expectedOutcome = Not Me.IsNew
        If result.Success Then result = (expectedOutcome, If(expectedOutcome, String.Empty, $"{identifier} should be tagged as '{outcomeName}'"))

        outcomeName = NameOf(EntityBase(Of TIEntity, TEntityNub).IsDirty)
        expectedOutcome = Me.IsDirty
        If result.Success Then result = (expectedOutcome, If(expectedOutcome, String.Empty, $"{identifier} should be tagged as '{outcomeName}'"))

        outcomeName = $"not {NameOf(EntityBase(Of TIEntity, TEntityNub).IsClean)}"
        expectedOutcome = Not Me.IsClean
        If result.Success Then result = (expectedOutcome, If(expectedOutcome, String.Empty, $"{identifier} should be tagged as '{outcomeName}'"))

        Return result
    End Function

#End Region

#Region " I TYPED CLONEABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public MustOverride Function CreateNew() As TIEntity Implements ITypedCloneable(Of TIEntity).CreateNew

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public MustOverride Function CreateCopy() As TIEntity Implements ITypedCloneable(Of TIEntity).CreateCopy

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public MustOverride Sub CopyFrom(value As TIEntity) Implements ITypedCloneable(Of TIEntity).CopyFrom

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.CreateCopy()
    End Function

#End Region

End Class

''' <summary> A bit-field of flags for specifying update modes. </summary>
''' <remarks> David, 5/9/2020. </remarks>
<Flags>
Public Enum UpdateModes

    ''' <summary> . </summary>
    <Description("Update only")> UpdateOnly = 0

    ''' <summary> . </summary>
    <Description("Force Update")> ForceUpdate = 1

    ''' <summary> . </summary>
    <Description("Refetch after update")> Refetch = 2
End Enum

