Imports Dapper
Imports isr.Core.Constructs

Partial Public Class EntityBase(Of TIEntity As {Class}, TEntityNub As {TIEntity, IEquatable(Of TIEntity), ITypedCloneable(Of TIEntity), Class})

    ''' <summary> Builds bulk insert command. </summary>
    ''' <remarks>
    ''' David, 5/13/2020.
    ''' https://stackoverflow.com/questions/10689779/bulk-inserts-taking-longer-than-expected-using-dapper/12609410#12609410.
    ''' </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <returns> A String. </returns>
    Public Function BuildBulkInsertCommand(ByVal tableName As String) As String
        Dim builder As New System.Text.StringBuilder
        builder.AppendLine($"Insert {tableName}{Me.ParenthesisEntityPropertyNames}")
        builder.AppendLine($"Values {Me.ParenthesisAtEntityPropertyNames}")
        Return builder.ToString
    End Function

    ''' <summary> Bulk insert. </summary>
    ''' <remarks>
    ''' David, 5/13/2020.
    ''' https://stackoverflow.com/questions/10689779/bulk-inserts-taking-longer-than-expected-using-dapper/12609410#12609410.
    ''' </remarks>
    ''' <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
    ''' <param name="tableName">             Name of the table. </param>
    ''' <param name="entities">              The entities. </param>
    ''' <returns> An Integer. </returns>
    <CLSCompliant(False)>
    Public Function BulkInsert(ByVal transcactedConnection As TransactedConnection, ByVal tableName As String, ByVal entities As IList(Of TIEntity)) As Integer
        Dim command As String = Me.BuildBulkInsertCommand(tableName)
        Return transcactedConnection.Execute(command, entities, transcactedConnection.Transaction)
    End Function

    ''' <summary> Bulk insert. </summary>
    ''' <remarks>
    ''' David, 5/13/2020.
    ''' https://stackoverflow.com/questions/10689779/bulk-inserts-taking-longer-than-expected-using-dapper/12609410#12609410.
    ''' </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="entities">   The entities. </param>
    ''' <returns> An Integer. </returns>
    Public Function BulkInsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String, ByVal entities As IList(Of TIEntity)) As Integer
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        Dim count As Integer
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        count = Me.BulkInsert(New TransactedConnection(connection, transaction), tableName, entities)
                        transaction.Commit()
                    Catch
                        transaction.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            count = Me.BulkInsert(transactedConnection, tableName, entities)
        End If
        Return count
    End Function

End Class
