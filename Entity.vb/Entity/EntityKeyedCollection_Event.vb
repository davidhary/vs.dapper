Imports System.ComponentModel

Imports isr.Core.Constructs
Imports isr.Core

Partial Public Class EntityKeyedCollection(Of TKey As Structure,
                                               TIEntity As {Class},
                                               TEntityNub As {TIEntity, IEquatable(Of TIEntity), ITypedCloneable(Of TIEntity), Class},
                                               TEntity As EntityBase(Of TIEntity, TEntityNub))
    Implements INotifyPropertyChanged

    ''' <summary> Removes the property changed event handlers. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Protected Sub RemovePropertyChangedEventHandlers()
        Me._PropertyChangedHandlers.RemoveAll()
    End Sub

    ''' <summary> The property changed handlers. </summary>
    Private ReadOnly _PropertyChangedHandlers As New PropertyChangeEventContextCollection()

    ''' <summary> Event queue for all listeners interested in property changed events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        AddHandler(ByVal value As PropertyChangedEventHandler)
            Me._PropertyChangedHandlers.Add(New PropertyChangedEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As PropertyChangedEventHandler)
            Me._PropertyChangedHandlers.RemoveValue(value)
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            ' defaults to Send to make this thread safe.
            Me._PropertyChangedHandlers.Send(sender, e)
        End RaiseEvent

    End Event

End Class
