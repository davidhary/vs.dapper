Imports isr.Core.Constructs

''' <summary>
''' Defines the contract that must be implemented by entity 'Nub' POCO Classes.
''' </summary>
''' <remarks>
''' <typeparamref name="TIEntity"/> Specifies the generic interface type implemented by class
''' <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para><para>
''' David, 4/25/2020, . from the legacy entity base. </para>
''' </remarks>
Public MustInherit Class EntityNubBase(Of TIEntity As {Class})
    Implements IEquatable(Of TIEntity), ITypedCloneable(Of TIEntity)

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="EntityBase" /> class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " I EQUATABLE "

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public MustOverride Overloads Function Equals(other As TIEntity) As Boolean Implements IEquatable(Of TIEntity).Equals

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, TIEntity))
    End Function


#End Region

#Region " I TYPED CLONEABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public MustOverride Function CreateNew() As TIEntity Implements ITypedCloneable(Of TIEntity).CreateNew

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public MustOverride Function CreateCopy() As TIEntity Implements ITypedCloneable(Of TIEntity).CreateCopy

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public MustOverride Sub CopyFrom(value As TIEntity) Implements ITypedCloneable(Of TIEntity).CopyFrom

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.CreateCopy()
    End Function

#End Region

End Class
