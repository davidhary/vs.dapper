Imports Dapper

Imports isr.Dapper.Entity.ExceptionExtensions

''' <summary> An SqlServer provider. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 8/26/2019 </para>
''' </remarks>
Public Class SqlServerProvider
    Inherits ProviderBase

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ProviderBase

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ProviderBase
        If SqlServerProvider._Instance Is Nothing Then
            SyncLock SyncLocker
                SqlServerProvider._Instance = New SqlServerProvider()
            End SyncLock
        End If
        Return SqlServerProvider._Instance
    End Function

#End Region

#Region " DATABASE CONSTRUCTION "

    ''' <summary> Creates a new SystemDataSqlServerProvider. </summary>
    ''' <remarks> David, 3/11/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <returns> A SqlServerProvider. </returns>
    Public Shared Function Create(ByVal connectionString As String) As SqlServerProvider
        Dim builder As System.Data.SqlClient.SqlConnectionStringBuilder = New System.Data.SqlClient.SqlConnectionStringBuilder(connectionString)
        Return New SqlServerProvider() With {.ConnectionString = connectionString}
    End Function

    ''' <summary> Creates a database. </summary>
    ''' <remarks> David, 3/23/2020. </remarks>
    ''' <param name="masterConnectionString"> The master connection string. </param>
    ''' <param name="databaseName">           Name of the database. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overrides Function CreateDatabase(ByVal masterConnectionString As String, ByVal databaseName As String) As Boolean
        Dim provider As SqlServerProvider = SqlServerProvider.Create(masterConnectionString)
        Return SqlServerProvider.CreateDatabase(provider.GetConnection, databaseName)
    End Function

    ''' <summary> Creates the database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="masterConnection"> The master connection. </param>
    ''' <param name="databaseName">     Name of the database. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Shared Function CreateDatabase(ByVal masterConnection As System.Data.IDbConnection, ByVal databaseName As String) As Boolean
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"
SET NOCOUNT ON; 
DECLARE @DBNAME VARCHAR(255)
SET @DBNAME = 'TestDB'
DECLARE @CREATE_TEMPLATE VARCHAR(MAX)
SET @CREATE_TEMPLATE = 'CREATE DATABASE {{DBNAME}}'; 
if exists (select * from master.dbo.sysdatabases where name='{databaseName}') drop database {databaseName}; 
DECLARE @device_directory NVARCHAR(520)
SELECT @device_directory = SUBSTRING(filename, 1, CHARINDEX(N'master.mdf', LOWER(filename)) - 1) FROM master.dbo.sysaltfiles WHERE dbid = 1 AND fileid = 1; 
EXECUTE (N'CREATE DATABASE {databaseName}
  ON PRIMARY (NAME = N''{databaseName}'', FILENAME = N''' + @device_directory + N'{databaseName}.mdf'')
  LOG ON (NAME = N''{databaseName}_log'',  FILENAME = N''' + @device_directory + N'{databaseName}_log.ldf'')'); 
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [{databaseName}].[dbo].[sp_fulltext_database] @action = 'enable'
end
ALTER DATABASE [{databaseName}] SET ANSI_NULL_DEFAULT OFF; 
ALTER DATABASE [{databaseName}] SET ANSI_NULLS OFF; 
ALTER DATABASE [{databaseName}] SET ANSI_PADDING OFF; 
ALTER DATABASE [{databaseName}] SET ANSI_WARNINGS OFF; 
ALTER DATABASE [{databaseName}] SET ARITHABORT OFF; 
ALTER DATABASE [{databaseName}] SET AUTO_CLOSE OFF; 
ALTER DATABASE [{databaseName}] SET AUTO_SHRINK OFF; 
ALTER DATABASE [{databaseName}] SET AUTO_UPDATE_STATISTICS ON; 
ALTER DATABASE [{databaseName}] SET CURSOR_CLOSE_ON_COMMIT OFF; 
ALTER DATABASE [{databaseName}] SET CURSOR_DEFAULT GLOBAL; 
ALTER DATABASE [{databaseName}] SET CONCAT_NULL_YIELDS_NULL OFF; 
ALTER DATABASE [{databaseName}] SET NUMERIC_ROUNDABORT OFF; 
ALTER DATABASE [{databaseName}] SET QUOTED_IDENTIFIER OFF; 
ALTER DATABASE [{databaseName}] SET RECURSIVE_TRIGGERS OFF; 
ALTER DATABASE [{databaseName}] SET  DISABLE_BROKER; 
ALTER DATABASE [{databaseName}] SET AUTO_UPDATE_STATISTICS_ASYNC OFF; 
ALTER DATABASE [{databaseName}] SET DATE_CORRELATION_OPTIMIZATION OFF; 
ALTER DATABASE [{databaseName}] SET TRUSTWORTHY OFF; 
ALTER DATABASE [{databaseName}] SET ALLOW_SNAPSHOT_ISOLATION OFF; 
ALTER DATABASE [{databaseName}] SET PARAMETERIZATION SIMPLE; 
ALTER DATABASE [{databaseName}] SET READ_COMMITTED_SNAPSHOT OFF; 
ALTER DATABASE [{databaseName}] SET HONOR_BROKER_PRIORITY OFF; 
ALTER DATABASE [{databaseName}] SET RECOVERY SIMPLE; 
ALTER DATABASE [{databaseName}] SET  MULTI_USER; 
ALTER DATABASE [{databaseName}] SET PAGE_VERIFY CHECKSUM;  
ALTER DATABASE [{databaseName}] SET DB_CHAINING OFF; 
ALTER DATABASE [{databaseName}] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ); 
ALTER DATABASE [{databaseName}] SET TARGET_RECOVERY_TIME = 0 SECONDS; 
ALTER DATABASE [{databaseName}] SET DELAYED_DURABILITY = DISABLED; 
ALTER DATABASE [{databaseName}] SET QUERY_STORE = OFF;
ALTER DATABASE [{databaseName}] SET  READ_WRITE; ")
        masterConnection.Execute(queryBuilder.ToString)
        Return True
    End Function

    ''' <summary> Drop connections. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="masterConnectionString"> The master connection string. </param>
    ''' <param name="databaseName">           Filename of the connection file. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overrides Function DropConnections(ByVal masterConnectionString As String, ByVal databaseName As String) As Boolean
        Dim provider As SqlServerProvider = SqlServerProvider.Create(masterConnectionString)
        Return SqlServerProvider.DropConnections(provider.GetConnection, databaseName)
    End Function

    ''' <summary> Drop connections. </summary>
    ''' <remarks> David, 4/2/2020. </remarks>
    ''' <param name="masterConnection"> The master connection. </param>
    ''' <param name="databaseName">     Name of the database. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Shared Function DropConnections(ByVal masterConnection As System.Data.IDbConnection, ByVal databaseName As String) As Boolean
        Dim builder As New System.Text.StringBuilder()
        builder.AppendLine($"
DECLARE @DatabaseName nvarchar(50)
SET @DatabaseName = N'{databaseName}'

DECLARE @SQL varchar(max)

SELECT @SQL = COALESCE(@SQL,'') + 'Kill ' + Convert(varchar, SPId) + ';'
FROM MASTER..SysProcesses
WHERE DBId = DB_ID(@DatabaseName) AND SPId <> @@SPId

EXEC(@SQL)")
        Return -1 <= masterConnection.Execute(builder.ToString)
    End Function

    ''' <summary> Drop database. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="masterConnectionString"> The master connection string. </param>
    ''' <param name="databaseName">           Filename of the connection file. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overrides Function DropDatabase(ByVal masterConnectionString As String, ByVal databaseName As String) As Boolean
        Dim provider As SqlServerProvider = SqlServerProvider.Create(masterConnectionString)
        Return SqlServerProvider.DropDatabase(provider.GetConnection, databaseName)
    End Function

    ''' <summary> Drop database. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="masterConnection"> The master connection. </param>
    ''' <param name="databaseName">     Name of the database. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Shared Function DropDatabase(ByVal masterConnection As System.Data.IDbConnection, ByVal databaseName As String) As Boolean
        Return -1 <= masterConnection.Execute($"DROP DATABASE IF EXISTS {databaseName}")
    End Function

    ''' <summary> Compact database. </summary>
    ''' <remarks> David, 6/22/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="databaseName"> The database name. </param>
    ''' <returns> An Integer. </returns>
    Public Overrides Function CompactDatabase(connection As System.Data.IDbConnection, ByVal databaseName As String) As Integer
        Return connection.Execute($"DBCC SHRINKDATABASE ({databaseName}, 10); ")
    End Function

#End Region

#Region " SQL Connection "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Connection As System.Data.SqlClient.SqlConnection
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the connection. </summary>
    ''' <value> The connection. </value>
    Public ReadOnly Property Connection As System.Data.SqlClient.SqlConnection
        Get
            Return Me._Connection
        End Get
    End Property

    ''' <summary> Gets the connection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The connection. </returns>
    Private Function GetConnectionThis() As System.Data.SqlClient.SqlConnection
        Return New System.Data.SqlClient.SqlConnection(Me.ConnectionString)
    End Function

    ''' <summary> Query if this object is connection exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> <c>true</c> if connection exists; otherwise <c>false</c> </returns>
    Public Overrides Function IsConnectionExists() As Boolean
        Return Me.GetConnectionThis.Exists
    End Function

    ''' <summary> Connection state change. </summary>
    ''' <remarks>
    ''' Used to turn on foreign keys for cascading operations.
    ''' https://stackoverflow.com/questions/13641250/SqlServer-delete-cascade-not-working.
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      State change event information. </param>
    Private Sub Connection_StateChange(sender As Object, e As System.Data.StateChangeEventArgs) Handles _Connection.StateChange
        Select Case e.CurrentState
            Case System.Data.ConnectionState.Open
                ' required only for SQLite, which defaults to OFF on open.
                ' _connection.Execute("PRAGMA foreign_keys = ON; ")
        End Select
    End Sub

    ''' <summary> Gets or sets the connection string builder. </summary>
    ''' <value> The connection string builder. </value>
    Protected Property ConnectionStringBuilder As System.Data.Common.DbConnectionStringBuilder = New System.Data.Common.DbConnectionStringBuilder

    ''' <summary> Gets or sets the connection string. </summary>
    ''' <value> The connection string. </value>
    Public Overrides Property ConnectionString As String
        Get
            Return Me.ConnectionStringBuilder.ConnectionString
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ConnectionString) Then
                Me.ConnectionStringBuilder.ConnectionString = value
            End If
        End Set
    End Property

    ''' <summary> Gets the name of the database. </summary>
    ''' <value> The name of the database. </value>
    Public Overrides ReadOnly Property DatabaseName As String
        Get
            Return Me.ConnectionStringBuilder("initial catalog").ToString
        End Get
    End Property

    ''' <summary> The data source key. </summary>
    Private Const _DataSourceKey As String = "data source"

    ''' <summary> Gets the data source. </summary>
    ''' <value> The data source. </value>
    Public ReadOnly Property DataSource As String
        Get
            Return If(Me.ConnectionStringBuilder.ContainsKey(SqlServerProvider._DataSourceKey),
                                Me.ConnectionStringBuilder.Item(SqlServerProvider._DataSourceKey).ToString,
                                String.Empty)
        End Get
    End Property

    ''' <summary> The initial catalog key. </summary>
    Private Const _InitialCatalogKey As String = "initial catalog"

    ''' <summary> The password key. </summary>
    Private Const _PasswordKey As String = "password"

    ''' <summary> The password. </summary>
    Private _Password As String

    ''' <summary> The user identifier key. </summary>
    Private Const _UserIdKey As String = "user id"

    ''' <summary> The integrated security key. </summary>
    Private Const _IntegratedSecurityKey As String = "integrated security"

    ''' <summary> The persist security information key. </summary>
    Private Const _PersistSecurityInfoKey As String = "persist security info"

    ''' <summary>
    ''' Gets the sentinel indicating if the provider uses integrated security (i.e., Windows
    ''' credentials).
    ''' </summary>
    ''' <value> True if using integrated security. </value>
    Public ReadOnly Property UsingIntegratedSecurity As Boolean
        Get
            Return String.Equals(Me.IntegratedSecurity, "SSPI", StringComparison.OrdinalIgnoreCase) OrElse
                String.Equals(Me.IntegratedSecurity, "True", StringComparison.OrdinalIgnoreCase) OrElse
                String.Equals(Me.IntegratedSecurity, "Yes", StringComparison.OrdinalIgnoreCase)

        End Get
    End Property

    ''' <summary> Gets the integrated security. </summary>
    ''' <value> The integrated security. </value>
    Public ReadOnly Property IntegratedSecurity As String
        Get
            Return If(Me.ConnectionStringBuilder.ContainsKey(SqlServerProvider._IntegratedSecurityKey),
                                Me.ConnectionStringBuilder.Item(SqlServerProvider._IntegratedSecurityKey).ToString,
                                String.Empty)
        End Get
    End Property

    ''' <summary> The credentials. </summary>
    Private _Credentials As Data.SqlClient.SqlCredential

    ''' <summary> Gets the credentials. </summary>
    ''' <value> The credentials. </value>
    Public ReadOnly Property Credentials As Data.SqlClient.SqlCredential
        Get
            If Me._Credentials Is Nothing Then
                Dim ss As New Security.SecureString()
                Me._Password = If(Me.ConnectionStringBuilder.ContainsKey(SqlServerProvider._PasswordKey), Me.ConnectionStringBuilder.Item(SqlServerProvider._PasswordKey).ToString, String.Empty)
                For Each cc As Char In Me._Password.ToCharArray
                    ss.AppendChar(cc)
                Next
                ss.MakeReadOnly()
                Dim userId As String = If(Me.ConnectionStringBuilder.ContainsKey(SqlServerProvider._UserIdKey), Me.ConnectionStringBuilder.Item(SqlServerProvider._UserIdKey).ToString, String.Empty)
                If Not (String.IsNullOrEmpty(userId) OrElse String.IsNullOrEmpty(ss.ToString)) Then
                    Me._Credentials = New Data.SqlClient.SqlCredential(userId, ss)
                End If
            End If
            Return Me._Credentials
        End Get
    End Property

    ''' <summary> Builds integrated security connection string. </summary>
    ''' <remarks> David, 7/14/2020. </remarks>
    ''' <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
    ''' <returns> A String. </returns>
    Public Function BuildIntegratedSecurityConnectionString(ByVal includeInitialCatalog As Boolean) As String
        Dim connectionBuilder As New Data.Common.DbConnectionStringBuilder From {
            {SqlServerProvider._DataSourceKey, Me.ConnectionStringBuilder.Item(SqlServerProvider._DataSourceKey)},
            {SqlServerProvider._IntegratedSecurityKey, "SSPI"},
            {SqlServerProvider._PersistSecurityInfoKey, "False"}
        }
        If includeInitialCatalog Then
            connectionBuilder.Add(SqlServerProvider._InitialCatalogKey, Me.ConnectionStringBuilder.Item(SqlServerProvider._InitialCatalogKey))
        End If
        Return connectionBuilder.ConnectionString
    End Function

    ''' <summary> Builds integrated security connection string. </summary>
    ''' <remarks> David, 3/23/2020. </remarks>
    ''' <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
    ''' <returns> A String. </returns>
    Public Function BuildConnectionString(ByVal includeInitialCatalog As Boolean) As String
        Dim connectionBuilder As New Data.Common.DbConnectionStringBuilder From {
            {SqlServerProvider._DataSourceKey, Me.ConnectionStringBuilder.Item(SqlServerProvider._DataSourceKey)},
            {SqlServerProvider._UserIdKey, Me.Credentials.UserId},
            {SqlServerProvider._PasswordKey, Me._Password},
            {SqlServerProvider._PersistSecurityInfoKey, "False"}
        }
        If includeInitialCatalog Then
            connectionBuilder.Add(SqlServerProvider._InitialCatalogKey, Me.ConnectionStringBuilder.Item(SqlServerProvider._InitialCatalogKey))
        End If
        Return connectionBuilder.ConnectionString
    End Function

    ''' <summary> Builds master connection string. </summary>
    ''' <remarks> David, 7/14/2020. </remarks>
    ''' <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
    ''' <returns> A String. </returns>
    Public Overrides Function BuildMasterConnectionString(ByVal includeInitialCatalog As Boolean) As String
        Return Me.BuildIntegratedSecurityConnectionString(includeInitialCatalog)
    End Function

#End Region

#Region " IDB Connection "

    ''' <summary> Gets a connection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The connection. </returns>
    Public Overrides Function GetConnection() As System.Data.IDbConnection
        Me._Connection = Me.GetConnectionThis
        Return Me._Connection
    End Function

    ''' <summary> Gets or sets the using information schema tables. </summary>
    ''' <value> The using information schema tables. </value>
    Public Shared Property UsingInformationSchemaTables As Boolean

    ''' <summary> Queries if a given database exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="databaseName"> Filename of the connection file. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function DatabaseExists(ByVal connection As System.Data.IDbConnection, databaseName As String) As Boolean
        Return 1 = connection.ExecuteScalar(Of Integer)($"SELECT count(*) FROM master.dbo.sysdatabases WHERE name = N'{databaseName}'")
    End Function

#End Region

#Region " TABLES "

    ''' <summary> Drop table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function DropTable(ByVal connection As System.Data.IDbConnection, ByVal tableName As String) As Boolean
        Return 1 < connection.Execute($"DROP TABLE IF EXISTS {tableName}")
    End Function

    ''' <summary> Queries if a given table exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function TableExists(connection As System.Data.IDbConnection, tableName As String) As Boolean
        Return If(SqlServerProvider.UsingInformationSchemaTables,
            1 = connection.ExecuteScalar(Of Integer)($"SELECT count(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'{tableName}'"),
            1 = connection.ExecuteScalar(Of Integer)($"SELECT count(*) FROM sys.tables WHERE name = N'{tableName}'; "))
    End Function

    ''' <summary> Queries if a given index exists. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="indexName">  Name of the table. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overrides Function IndexExists(ByVal connection As System.Data.IDbConnection, indexName As String) As Boolean
        Return 1 = connection.ExecuteScalar(Of Integer)($"SELECT count(*) FROM sys.indexes WHERE name = N'{indexName}'; ")
    End Function

    ''' <summary> Reseed identity. </summary>
    ''' <remarks> David, 6/1/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> An Integer. </returns>
    Public Overrides Function ReseedIdentity(connection As System.Data.IDbConnection, tableName As String) As Integer
        Return connection.Execute($"DBCC CHECKIDENT ([{tableName}],RESEED); 
DBCC CHECKIDENT ([{tableName}]); ")
    End Function

#End Region

#Region " VIEWS "

    ''' <summary> Drop view. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="viewName">   Name of the view. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overrides Function DropView(ByVal connection As System.Data.IDbConnection, ByVal viewName As String) As Boolean
        Return 1 < connection.Execute($"DROP VIEW IF EXISTS {viewName}")
    End Function

    ''' <summary> Queries if a given View exists. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="viewName">   Name of the View. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function ViewExists(connection As System.Data.IDbConnection, viewName As String) As Boolean
        Return 1 = connection.ExecuteScalar(Of Integer)($"SELECT count(*) FROM sys.views WHERE name = N'{viewName}'; ")
    End Function

#End Region

#Region " LOGIN "

    ''' <summary>
    ''' Establishes login credentials for the specified principal and sets it as the database owner.
    ''' </summary>
    ''' <remarks>
    ''' Creates new server and database principals if none exist or if database principal is not
    ''' longer valid.
    ''' </remarks>
    ''' <param name="masterProvider"> The master provider. </param>
    ''' <param name="databaseName">   Name of the database. </param>
    ''' <param name="principalName">  Name of the principal. </param>
    ''' <param name="password">       The password. </param>
    ''' <returns> The new login. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Shared Function EstablishLoginCredentials(ByVal masterProvider As SqlServerProvider, ByVal databaseName As String,
                                                     ByVal principalName As String, ByVal password As Security.SecureString) As (Success As Boolean, Details As String)
        Dim activity As String = $"Establishing {NameOf(System.Data.IDbConnection)} for '{principalName}'"
        Try
            ' checks if we have a user login issue.
            Dim connection As System.Data.IDbConnection = New System.Data.SqlClient.SqlConnection(masterProvider.ConnectionString)
            activity = $"finding {principalName} at {NameOf(SqlServerProvider.ServerPrincipalExists)}; or {NameOf(SqlServerProvider.QueryUserLoginMatch)}"
            If Not SqlServerProvider.ServerPrincipalExists(connection, principalName) Then
                activity = $"invoking {NameOf(SqlServerProvider.DropDatabaseUser)} for {principalName}"
                SqlServerProvider.DropDatabaseUser(connection, databaseName, principalName)
                activity = $"invoking {NameOf(SqlServerProvider.AddLoginPrincipal)} for {principalName}"
                SqlServerProvider.AddLoginPrincipal(connection, databaseName, principalName, password)
            ElseIf Not SqlServerProvider.QueryUserLoginMatch(connection, databaseName, principalName) Then
                ' there is a mismatch between the login principal and the database user, which needs to be changed
                activity = $"invoking {NameOf(SqlServerProvider.DropDatabaseUser)} for {principalName}"
                SqlServerProvider.DropDatabaseUser(connection, databaseName, principalName)
            End If
            If Not SqlServerProvider.DatabasePrincipalExists(connection, databaseName, principalName) Then
                activity = $"invoking {NameOf(SqlServerProvider.CreateDatabaseUser)} for {principalName}"
                SqlServerProvider.CreateDatabaseUser(connection, databaseName, principalName)
            End If
            activity = $"invoking {NameOf(SqlServerProvider.QueryUserLoginMatch)} for {principalName}"
            If SqlServerProvider.QueryUserLoginMatch(connection, databaseName, principalName) Then
                activity = $"invoking {NameOf(SqlServerProvider.AlterDatabaseOwnerRole)} for {principalName}"
                SqlServerProvider.AlterDatabaseOwnerRole(connection, True, databaseName, principalName)
                Return (True, String.Empty)
            Else
                Return (False, $"Failed establishing login credentials for user {principalName}")
            End If
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}")
        End Try
    End Function

    ''' <summary> Name of the server principals table. </summary>
    Public Const ServerPrincipalsTableName As String = "SYS.SERVER_PRINCIPALS"

    ''' <summary> Name of the database principals table. </summary>
    Public Const DatabasePrincipalsTableName As String = "SYS.DATABASE_PRINCIPALS"

    ''' <summary> Queries if a given server principal exists. </summary>
    ''' <remarks> David, 3/17/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function ServerPrincipalExists(ByVal connection As System.Data.IDbConnection, principalName As String) As Boolean
        Return 1 = connection.ExecuteScalar(Of Integer)($"SELECT count(*) FROM {SqlServerProvider.ServerPrincipalsTableName} WHERE name = N'{principalName}'")
    End Function

    ''' <summary> Queries if a given database principal exists. </summary>
    ''' <remarks> David, 3/17/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="databaseName">  Name of the database. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function DatabasePrincipalExists(ByVal connection As System.Data.IDbConnection, ByVal databaseName As String, ByVal principalName As String) As Boolean
        Return 1 = connection.ExecuteScalar(Of Integer)($"USE [{databaseName}];
SELECT count(*) FROM {SqlServerProvider.DatabasePrincipalsTableName} WHERE name = N'{principalName}'")
    End Function

    ''' <summary>
    ''' Fetch the server principal security identifier (SID) or empty string if principal does not
    ''' exist.
    ''' </summary>
    ''' <remarks> David, 3/17/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> A String. </returns>
    Public Shared Function ServerPrincipalSecurityId(ByVal connection As System.Data.IDbConnection, principalName As String) As String
        Return If(SqlServerProvider.ServerPrincipalExists(connection, principalName),
            connection.ExecuteScalar(Of String)($"SELECT CAST(SID AS VARCHAR(MAX)) FROM {SqlServerProvider.ServerPrincipalsTableName} WHERE name = N'{principalName}'"),
            String.Empty)
    End Function

    ''' <summary>
    ''' Fetch the database principal security identifier (SID) or empty string if principal does not
    ''' exist.
    ''' </summary>
    ''' <remarks> David, 3/17/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="databaseName">  Name of the database. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> A String. </returns>
    Public Shared Function DatabasePrincipalId(ByVal connection As System.Data.IDbConnection, ByVal databaseName As String, principalName As String) As String
        Return If(SqlServerProvider.ServerPrincipalExists(connection, principalName),
            connection.ExecuteScalar(Of String)($"USE [{databaseName}];
SELECT CAST(SID AS VARCHAR(MAX)) FROM  {SqlServerProvider.DatabasePrincipalsTableName} WHERE name = N'{principalName}'"),
            String.Empty)
    End Function

    ''' <summary> Drop Principal. </summary>
    ''' <remarks> David, 3/17/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DropLoginPrincipal(ByVal connection As System.Data.IDbConnection, principalName As String) As Integer
        Return connection.Execute($"IF EXISTS (SELECT * FROM {SqlServerProvider.ServerPrincipalsTableName} WHERE name = N'{principalName}') DROP LOGIN [{principalName}]; ")
    End Function

    ''' <summary> Adds a login principal. </summary>
    ''' <remarks> David, 3/17/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="databaseName">  Filename of the connection file. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <param name="password">      The password. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function AddLoginPrincipal(ByVal connection As System.Data.IDbConnection, ByVal databaseName As String,
                                             ByVal principalName As String, ByVal password As Security.SecureString) As Integer
        Return connection.Execute($"CREATE LOGIN [{principalName}] WITH PASSWORD=N'{password}', DEFAULT_DATABASE=[{databaseName}], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF; ")
    End Function

    ''' <summary> Drop User. </summary>
    ''' <remarks> David, 3/17/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="databaseName">  The database name. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DropDatabaseUser(ByVal connection As System.Data.IDbConnection, ByVal databaseName As String, principalName As String) As Integer
        Return connection.Execute($"USE [{databaseName}];
IF EXISTS (SELECT * FROM {SqlServerProvider.DatabasePrincipalsTableName} WHERE name = N'{principalName}') DROP USER [{principalName}]; ")
    End Function

    ''' <summary>
    ''' Adds a database user <paramref name="principalName"/> to the same login name w/o assigning a
    ''' role.
    ''' </summary>
    ''' <remarks> David, 3/17/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="databaseName">  Filename of the connection file. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function CreateDatabaseUser(ByVal connection As System.Data.IDbConnection, ByVal databaseName As String, ByVal principalName As String) As Integer
        Return connection.Execute($"USE [{databaseName}];
IF  NOT EXISTS (SELECT * FROM {SqlServerProvider.DatabasePrincipalsTableName} WHERE name = N'{principalName}') 
CREATE USER [{principalName}] FOR LOGIN [{principalName}] WITH DEFAULT_SCHEMA=[dbo]; ")
    End Function

    ''' <summary> The database owner role. </summary>
    Private Const _DatabaseOwnerRole As String = "db_owner"

    ''' <summary> The database reader role. </summary>
    Private Const _DatabaseReaderRole As String = "db_datareader"

    ''' <summary> The database writer role. </summary>
    Private Const _DatabaseWriterRole As String = "db_datawriter"

    ''' <summary> The add role member stored procedure. </summary>
    Private Const _AddRoleMemberStoredProcedure As String = "sp_addrolemember"

    ''' <summary> The drop role member stored procedure. </summary>
    Private Const _DropRoleMemberStoredProcedure As String = "sp_droprolemember"

    ''' <summary> Alter database owner role. </summary>
    ''' <remarks> David, 3/25/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="add">           True to add. </param>
    ''' <param name="databaseName">  Filename of the connection file. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function AlterDatabaseOwnerRole(ByVal connection As System.Data.IDbConnection, ByVal add As Boolean,
                                                  ByVal databaseName As String, ByVal principalName As String) As Integer
        Dim action As String = If(add, SqlServerProvider._AddRoleMemberStoredProcedure, SqlServerProvider._DropRoleMemberStoredProcedure)
        Return connection.Execute($"USE [{databaseName}];
EXEC {action} '{SqlServerProvider._DatabaseOwnerRole}', '{principalName}'")
    End Function

    ''' <summary> Alter database scriber role. </summary>
    ''' <remarks> David, 3/25/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="add">           True to add. </param>
    ''' <param name="databaseName">  The database name. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function AlterDatabaseScriberRole(ByVal connection As System.Data.IDbConnection, ByVal add As Boolean,
                                                    ByVal databaseName As String, ByVal principalName As String) As Integer
        Dim action As String = If(add, SqlServerProvider._AddRoleMemberStoredProcedure, SqlServerProvider._DropRoleMemberStoredProcedure)
        Return connection.Execute($"USE [{databaseName}];
EXEC {action} '{SqlServerProvider._DatabaseReaderRole}', '{principalName}'
        EXEC {action} '{SqlServerProvider._DatabaseWriterRole}', '{principalName}'")
    End Function

    ''' <summary> Checks if the database principal has an owner roll. </summary>
    ''' <remarks> David, 4/2/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function QueryDatabasePrincipalOwnerRoll(ByVal connection As System.Data.IDbConnection, ByVal principalName As String) As Boolean
        Return 1 = SqlServerProvider.CountDatabasePrincipalRoles(connection, SqlServerProvider._DatabaseOwnerRole, principalName)
    End Function

    ''' <summary> Count database principal roles. </summary>
    ''' <remarks> David, 4/2/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="role">          The role. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> The total number of database principal roles. </returns>
    Public Shared Function CountDatabasePrincipalRoles(ByVal connection As System.Data.IDbConnection, ByVal role As String, ByVal principalName As String) As Integer
        Dim builder As New System.Text.StringBuilder()
        builder.AppendLine($"WITH RoleMembers (member_principal_id, role_principal_id)    
AS  
( 
  SELECT  
   rm1.member_principal_id,  
   rm1.role_principal_id      
  FROM sys.database_role_members rm1 (NOLOCK)  
   UNION ALL  
  SELECT 
   d.member_principal_id, 
   rm.role_principal_id      
  FROM sys.database_role_members rm (NOLOCK)   
   INNER JOIN RoleMembers AS d     
   ON rm.member_principal_id = d.role_principal_id  
)  
select distinct rp.name as database_role, mp.name as database_userl  
from RoleMembers drm  
  join sys.database_principals rp on (drm.role_principal_id = rp.principal_id)  
  join sys.database_principals mp on (drm.member_principal_id = mp.principal_id) ")
        If String.IsNullOrEmpty(role) Then
            builder.AppendLine($"where rp.name = '{role}' and mp.name = '{principalName}' ")
        Else
            builder.AppendLine($"where mp.name = '{principalName}' ")
        End If
        builder.AppendLine("order by rp.name ")
        Return connection.Execute(builder.ToString)
    End Function

    ''' <summary>
    ''' Queries if there is match between the server login principal and the database user. If a
    ''' match, the user can login.
    ''' </summary>
    ''' <remarks> David, 3/17/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="databaseName">  Name of the database. </param>
    ''' <param name="principalName"> Name of the principal. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function QueryUserLoginMatch(ByVal connection As System.Data.IDbConnection, ByVal databaseName As String, ByVal principalName As String) As Boolean
        Dim loginId As String = SqlServerProvider.ServerPrincipalSecurityId(connection, principalName)
        Dim userId As String = SqlServerProvider.DatabasePrincipalId(connection, databaseName, principalName)
        Return Not String.IsNullOrEmpty(loginId) AndAlso String.Equals(loginId, userId, StringComparison.InvariantCulture)
    End Function

#End Region

End Class

