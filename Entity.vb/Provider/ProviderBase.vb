Imports Dapper

''' <summary> A provider base. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/17/2018 </para>
''' </remarks>
Public MustInherit Class ProviderBase

#Region " PROVIDER SELECTOR "

    ''' <summary> Select provider. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> A ProviderBase. </returns>
    Public Shared Function SelectProvider(ByVal connection As System.Data.IDbConnection) As ProviderBase
        If TryCast(connection, System.Data.SQLite.SQLiteConnection) IsNot Nothing Then
            Return SQLiteProvider.Get
        ElseIf TryCast(connection, System.Data.SqlClient.SqlConnection) IsNot Nothing Then
            Return SqlServerProvider.Get
        ElseIf TryCast(connection, TransactedConnection) IsNot Nothing Then
            Return ProviderBase.SelectProvider(TryCast(connection, TransactedConnection).Connection)
        Else
            Return SqlServerProvider.Get
        End If
    End Function

#End Region

#Region " CONNECTION STRING "

    ''' <summary> Gets the connection string. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The connection string. </value>
    Public Overridable Property ConnectionString As String

    ''' <summary> Gets the filename of the file. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the file. </value>
    Public Property FileName As String

    ''' <summary> Gets the name of the database. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the database. </value>
    Public Overridable ReadOnly Property DatabaseName As String

    ''' <summary> Builds master connection string. </summary>
    ''' <remarks> David, 3/23/2020. </remarks>
    ''' <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
    ''' <returns> A String. </returns>
    Public MustOverride Function BuildMasterConnectionString(ByVal includeInitialCatalog As Boolean) As String

#End Region

#Region " IDB CONNECTION "

    ''' <summary> Gets a connection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The connection. </returns>
    Public MustOverride Function GetConnection() As System.Data.IDbConnection

    ''' <summary> Queries if a given connection exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function ConnectionExists() As Boolean
        Return Me.GetConnection.Exists
    End Function

    ''' <summary> Can open connection. </summary>
    ''' <remarks> David, 3/17/2020. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function CanOpenConnection() As (Success As Boolean, Details As String)
        Return Me.GetConnection.CanOpen
    End Function

    ''' <summary> Query if this object is connection exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> <c>true</c> if connection exists; otherwise <c>false</c> </returns>
    Public MustOverride Function IsConnectionExists() As Boolean

    ''' <summary> Executes the transaction operation. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="command"> The command. </param>
    ''' <returns> An Integer. </returns>
    Public Function ExecuteTransaction(ByVal command As String) As Integer
        Return ProviderBase.ExecuteTransaction(Me.GetConnection, command)
    End Function

    ''' <summary> Executes the transaction operation. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="command">    The command. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function ExecuteTransaction(ByVal connection As System.Data.IDbConnection, ByVal command As String) As Integer
        Dim result As Integer = -1
        Dim wasClosed As Boolean = connection.IsClosed
        Try
            If wasClosed Then connection.Open()
            Using transaction As System.Data.IDbTransaction = connection.BeginTransaction
                Try
                    result = connection.Execute(command, transaction:=transaction)
                    transaction.Commit()
                Catch
                    transaction.Rollback()
                    Throw
                End Try
            End Using
        Catch
            Throw
        Finally
            If wasClosed Then connection.Close()
        End Try
        Return result
    End Function

#End Region

#Region " TABLES "

    ''' <summary> Queries if a given table exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function TableExists(ByVal connection As System.Data.IDbConnection, ByVal tableName As String) As Boolean

    ''' <summary> Queries if a given table exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TableExists(ByVal tableName As String) As Boolean
        Return Me.TableExists(Me.GetConnection, tableName)
    End Function

    ''' <summary> Drop table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function DropTable(ByVal connection As System.Data.IDbConnection, ByVal tableName As String) As Boolean

    ''' <summary> Executes the script from the specified 'FileName'. </summary>
    ''' <remarks> David, 3/25/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="fileName">   The name of the file. </param>
    ''' <returns> The number of affected records. </returns>
    Public Shared Function ExecuteScript(ByVal connection As System.Data.IDbConnection, ByVal fileName As String) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        If fileName Is Nothing Then Throw New ArgumentNullException(NameOf(fileName))
        Try
            Return connection.Execute(System.IO.File.ReadAllText(fileName))
        Catch
            Throw
        End Try
    End Function

    ''' <summary> Executes the script from the specified 'FileName'. </summary>
    ''' <remarks> David, 3/25/2020. </remarks>
    ''' <param name="fileName"> The name of the file. </param>
    ''' <returns> The number of affected records. </returns>
    Public Function ExecuteScript(ByVal fileName As String) As Integer
        Return ProviderBase.ExecuteScript(Me.GetConnection, fileName)
    End Function

    ''' <summary> Queries if a given index exists. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public MustOverride Function IndexExists(ByVal connection As System.Data.IDbConnection, ByVal tableName As String) As Boolean

    ''' <summary> Queries if a given index exists. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function IndexExists(ByVal tableName As String) As Boolean
        Return Me.IndexExists(Me.GetConnection, tableName)
    End Function

    ''' <summary> Counts the number of records in the specified table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The data source connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal tableName As String) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select count(*) from [{tableName}]; ")
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(queryBuilder.ToString)
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql)
    End Function

    ''' <summary> Reseed identity. </summary>
    ''' <remarks> David, 6/1/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> An Integer. </returns>
    Public MustOverride Function ReseedIdentity(ByVal connection As System.Data.IDbConnection, ByVal tableName As String) As Integer

    ''' <summary> Reseed identity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <returns> An Integer. </returns>
    Public Function ReseedIdentity(ByVal tableName As String) As Integer
        Return Me.ReseedIdentity(Me.GetConnection, tableName)
    End Function

#End Region

#Region " VIEWS "

    ''' <summary> Drop view. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="viewName">   Name of the view. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public MustOverride Function DropView(ByVal connection As System.Data.IDbConnection, ByVal viewName As String) As Boolean

    ''' <summary> Queries if a given View exists. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="viewName">   Name of the View. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function ViewExists(ByVal connection As System.Data.IDbConnection, ByVal viewName As String) As Boolean

    ''' <summary> Queries if a given View exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="viewName"> Name of the View. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function ViewExists(ByVal viewName As String) As Boolean
        Return Me.ViewExists(Me.GetConnection, viewName)
    End Function

#End Region

#Region " DATABASE "

    ''' <summary> Queries if a given database exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="databaseName"> Filename of the connection file. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function DatabaseExists(ByVal connection As System.Data.IDbConnection, databaseName As String) As Boolean

    ''' <summary> Queries if a given database exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function DatabaseExists() As Boolean
        Return Me.DatabaseExists(Me.GetConnection, Me.DatabaseName)
    End Function

    ''' <summary> Creates a database. </summary>
    ''' <remarks> David, 3/23/2020. </remarks>
    ''' <param name="masterConnectionString"> The master connection string. </param>
    ''' <param name="databaseName">           Name of the database. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public MustOverride Function CreateDatabase(ByVal masterConnectionString As String, ByVal databaseName As String) As Boolean

    ''' <summary> Drop connections. </summary>
    ''' <remarks> David, 4/2/2020. </remarks>
    ''' <param name="masterConnectionString"> The master connection string. </param>
    ''' <param name="databaseName">           Filename of the connection file. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public MustOverride Function DropConnections(ByVal masterConnectionString As String, ByVal databaseName As String) As Boolean

    ''' <summary> Drop database. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="masterConnectionString"> The master connection string. </param>
    ''' <param name="databaseName">           Filename of the connection file. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public MustOverride Function DropDatabase(ByVal masterConnectionString As String, ByVal databaseName As String) As Boolean

    ''' <summary> Compact database. </summary>
    ''' <remarks> David, 6/22/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="databaseName"> Filename of the connection file. </param>
    ''' <returns> An Integer. </returns>
    Public MustOverride Function CompactDatabase(ByVal connection As System.Data.IDbConnection, ByVal databaseName As String) As Integer

#End Region


#Region " PROVIDER "

    ''' <summary> Type of the provider. </summary>
    Private _ProviderType As ProviderType

    ''' <summary> Gets the type of the provider. </summary>
    ''' <value> The type of the provider. </value>
    Public ReadOnly Property ProviderType As ProviderType
        Get
            If Me._ProviderType = ProviderType.None Then
                Me._ProviderType = ProviderBase.IdentifyProvider(Me.GetConnection)
            End If
            Return Me._ProviderType
        End Get
    End Property

    ''' <summary> Identify provider. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> A ProviderType. </returns>
    Public Shared Function IdentifyProvider(ByVal connection As System.Data.IDbConnection) As ProviderType
        If TryCast(connection, System.Data.SqlClient.SqlConnection) IsNot Nothing Then
            Return ProviderType.SqlServer
        ElseIf TryCast(connection, System.Data.SQLite.SQLiteConnection) IsNot Nothing Then
            Return ProviderType.SQLite
        Else
            Return Nothing
        End If
    End Function

#End Region

#Region " FILE SYSTEM "

    ''' <summary> Creates a directory. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="path"> Full pathname of the file. </param>
    ''' <returns> The new directory. </returns>
    Public Shared Function CreateDirectory(ByVal path As String) As System.IO.DirectoryInfo
        If Not System.IO.Directory.Exists(path) Then
            System.IO.Directory.CreateDirectory(path)
        End If
        Dim di As System.IO.DirectoryInfo
        di = New System.IO.DirectoryInfo(path)
        Return di
    End Function

    ''' <summary> Creates a directory. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new directory. </returns>
    Public Function CreateDirectory() As System.IO.DirectoryInfo
        Dim fi As New System.IO.FileInfo(Me.FileName)
        Return ProviderBase.CreateDirectory(fi.DirectoryName)
    End Function

#End Region

#Region " DATE TIME "

    ''' <summary> Gets or sets the  default date time format. </summary>
    ''' <value> The date time format. </value>
    Public Shared Property DefaultDateTimeFormat As String = "yyyy-MM-dd HH:mm:ss.fff"

    ''' <summary> Converts a value to a default label. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="value"> The value Date/Time. </param>
    ''' <returns> Time value in the <see cref="DefaultDateTimeFormat"/> format. </returns>
    Public Shared Function ToDefaultTimestampFormat(ByVal value As DateTime) As String
        Return ProviderBase.ToTimestampFormat(value, ProviderBase.DefaultDateTimeFormat)
    End Function

    ''' <summary> Gets or sets the date time format. </summary>
    ''' <value> The date time format. </value>
    Public Property DateTimeFormat As String = "yyyy-MM-dd HH:mm:ss.fff"

    ''' <summary> Converts a value to a timestamp format. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="value">  The value Date/Time. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns> Time value in the <see cref="DateTimeFormat"/> format. </returns>
    Public Shared Function ToTimestampFormat(ByVal value As DateTime, ByVal format As String) As String
        Return value.ToString(format)
    End Function


#End Region

End Class

''' <summary> Values that represent provider types. </summary>
''' <remarks> David, 2020-10-02. </remarks>
Public Enum ProviderType

    ''' <summary> An enum constant representing the none option. </summary>
    None

    ''' <summary> An enum constant representing the sq lite option. </summary>
    SQLite

    ''' <summary> An enum constant representing the SQL server option. </summary>
    SqlServer
End Enum

