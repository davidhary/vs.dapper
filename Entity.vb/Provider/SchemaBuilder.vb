Imports Dapper

Imports isr.Core
Imports isr.Dapper.Entity.ConnectionExtensions

''' <summary> The database schema builder. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/25/2019 </para>
''' </remarks>
Public Class SchemaBuilder

    ''' <summary> Gets the provider. </summary>
    ''' <value> The provider. </value>
    Public Property Provider As ProviderBase

#Region " TABLES "

    ''' <summary> Verifies that tables exist. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Private Sub AssertTablesExist()
        Using connection As System.Data.IDbConnection = Me.Provider.GetConnection
            For Each tableName As String In Me.TableNames
                If Not Me.Provider.TableExists(connection, tableName) Then Throw New System.Data.DataException($"table {tableName } should exist")
            Next
        End Using
    End Sub

    ''' <summary> Gets the build tables. </summary>
    ''' <value> The build tables. </value>
    Public ReadOnly Property EnumeratedTables As IDictionary(Of String, TableMaker) = New Dictionary(Of String, TableMaker)

    ''' <summary> Gets a list of names of the tables. </summary>
    ''' <value> A list of names of the tables. </value>
    Public ReadOnly Property TableNames As IEnumerable(Of String)
        Get
            Return Me.EnumeratedTables.Keys
        End Get
    End Property

    ''' <summary> Makes a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="maker">      The maker. </param>
    ''' <returns> A TableCategory. </returns>
    Private Function MakeTableThis(ByVal connection As System.Data.IDbConnection, ByVal maker As TableMaker) As TableCategory
        Dim actionCount As Integer = 0
        Dim queryCommand As String
        Dim tableName As String = String.Empty
        maker.Failed = False
        Dim minimumInsertCount As Integer = 0
        If Me.Provider.TableExists(connection, maker.TableName) Then
            maker.Category = maker.Category Or TableCategory.Existed
            If maker.ExistsAction IsNot Nothing Then
                actionCount = maker.ExistsAction.Invoke(connection)
            End If
            If maker.ExistsBuildAction IsNot Nothing Then
                queryCommand = maker.ExistsBuildAction.Invoke(connection)
            End If
            If maker.ExistsUpdateAction IsNot Nothing Then
                If Not maker.ExistsUpdateAction.Invoke(connection, maker.ExistsUpdateValue) Then
                    Throw New InvalidOperationException($"{NameOf(TableMaker.ExistsUpdateAction)} failed on table {maker.TableName}")
                End If
            End If
        Else
            minimumInsertCount = 1
            maker.Category = maker.Category And Not TableCategory.Existed
            If maker.CreateAction IsNot Nothing Then
                tableName = maker.CreateAction.Invoke(connection)
                If String.Equals(tableName, maker.TableName, StringComparison.OrdinalIgnoreCase) Then
                    maker.Category = maker.Category Or TableCategory.Created
                Else
                    Throw New InvalidOperationException($"Created table name {tableName} does not match maker name {maker.TableName}")
                End If
            End If
            If maker.PostCreateAction IsNot Nothing Then
                If Not maker.PostCreateAction.Invoke(connection, maker.PostCreateValue) Then
                    Throw New InvalidOperationException($"{NameOf(TableMaker.PostCreateAction)} failed on table {maker.TableName}")
                End If
            End If
        End If
        If maker.InsertAction IsNot Nothing Then
            ' the action could return -1 if no rows were affected.
            actionCount = Math.Max(0, maker.InsertAction.Invoke(connection))
            If actionCount < minimumInsertCount Then
                Throw New InvalidOperationException($"{NameOf(TableMaker.InsertAction)} into table {maker.TableName} yielded {actionCount} records")
            End If
        End If
        If maker.InsertFileValuesAction IsNot Nothing Then
            ' the action could return -1 if no rows were affected.
            actionCount = Math.Max(0, maker.InsertFileValuesAction.Invoke(connection, maker.ValuesFileName))
            ' this is required in case the insert replaced no values.
            If actionCount < minimumInsertCount Then
                Throw New InvalidOperationException($"{NameOf(TableMaker.InsertFileValuesAction)} into table {maker.TableName} yielded 0 records")
            End If
        End If
        If maker.InsertValuesAction IsNot Nothing Then
            If Not maker.InsertValuesAction.Invoke(connection, maker.Values) Then
                Throw New InvalidOperationException($"{NameOf(TableMaker.InsertValuesAction)}  failed on table {maker.TableName}")
            End If
        End If
        Return maker.Category
    End Function

    ''' <summary> Makes a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="maker">      The maker. </param>
    ''' <returns> A TableCategory. </returns>
    Private Function MakeTable(ByVal connection As System.Data.IDbConnection, ByVal maker As TableMaker) As TableCategory
        Try
            Me.MakeTableThis(connection, maker)
        Catch
            maker.Failed = True
            Throw
        Finally
            If maker.Failed AndAlso TableCategory.Existed <> (TableCategory.Existed And maker.Category) Then
                ' if failed and not existed, make sure to drop this table
                Try
                    Me.Provider.DropTable(connection, maker.TableName)
                Finally
                End Try
            End If
        End Try
        Return maker.Category
    End Function

    ''' <summary> Adds tables. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Private Sub AddTables()
        Using connection As System.Data.IDbConnection = Me.Provider.GetConnection
            connection.Open()
            For Each kvp As KeyValuePair(Of String, TableMaker) In Me.EnumeratedTables
                Me.MakeTable(connection, kvp.Value)
            Next
        End Using
    End Sub

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="maker">      The maker. </param>
    Private Sub FetchAll(ByVal connection As System.Data.IDbConnection, ByVal maker As TableMaker)
        If Me.Provider.TableExists(connection, maker.TableName) Then
            If maker.FetchAllAction IsNot Nothing Then
                maker.FetchAllAction.Invoke(connection)
            End If
        End If
    End Sub

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    Public Sub FetchAll()
        Using connection As System.Data.IDbConnection = Me.Provider.GetConnection
            connection.Open()
            For Each kvp As KeyValuePair(Of String, TableMaker) In Me.EnumeratedTables
                Me.FetchAll(connection, kvp.Value)
            Next
        End Using
    End Sub

    ''' <summary> Deletes all records. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="category"> The category. </param>
    Public Sub DeleteAllRecords(ByVal category As TableCategory)
        ' exclude lookup tables on first round
        Me.DeleteAllRecords(category, TableCategory.Lookup)
        Me.DeleteAllRecords(category, TableCategory.None)
    End Sub

    ''' <summary> Deletes all records. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="category">         The category. </param>
    ''' <param name="excludedCategory"> Category the excluded belongs to. </param>
    Private Sub DeleteAllRecords(ByVal category As TableCategory, excludedCategory As TableCategory)
        Using connection As System.Data.IDbConnection = Me.Provider.GetConnection
            connection.Open()
            Dim categories As TableCategory() = New TableCategory() {TableCategory.Created, TableCategory.Existed}
            For Each tableCategory As TableCategory In categories
                If tableCategory = (category And tableCategory) Then
                    ' select the tables in the reverse order they were enumerated.
                    For Each kvp As KeyValuePair(Of String, TableMaker) In Me.EnumeratedTables.Reverse
                        If excludedCategory <> (kvp.Value.Category And excludedCategory) AndAlso tableCategory = (kvp.Value.Category And tableCategory) AndAlso Me.Provider.TableExists(connection, kvp.Key) Then
                            ' clear the records if the table is of the specified category and not excluded
                            connection.Execute($"delete from {kvp.Key}")
                        End If
                    Next
                End If
            Next
        End Using
    End Sub

    ''' <summary> Drop all tables. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="ignoreErrors"> True to ignore errors. </param>
    Public Sub DropAllTables(ByVal ignoreErrors As Boolean)
        Me.DropTables(TableCategory.Lookup, ignoreErrors)
        Me.DropTables(TableCategory.None, ignoreErrors)
    End Sub

    ''' <summary> Drop all tables per specified category. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="category">     The category. </param>
    ''' <param name="ignoreErrors"> True to ignore errors. </param>
    Public Sub DropAllTables(ByVal category As TableCategory, ByVal ignoreErrors As Boolean)
        ' set the excluded categories
        Dim c As TableCategory = Not category
        ' exclude lookup tables on first round
        If TableCategory.Lookup = (TableCategory.Lookup And category) Then
            ' if dropping lookup tables, start first by excluding the lookup tables.
            c = c Or TableCategory.Lookup
            Me.DropTables(c, ignoreErrors)
            c = c And Not TableCategory.Lookup
        End If
        Me.DropTables(c, ignoreErrors)
    End Sub

    ''' <summary> Drop tables except those excluded. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="excludedCategory"> Category the excluded belongs to. </param>
    ''' <param name="ignoreErrors">     True to ignore errors. </param>
    Private Sub DropTables(ByVal excludedCategory As TableCategory, ByVal ignoreErrors As Boolean)
        Using connection As System.Data.IDbConnection = Me.Provider.GetConnection
            connection.Open()
            ' select the tables in the reverse order they were enumerated.
            For Each kvp As KeyValuePair(Of String, TableMaker) In Me.EnumeratedTables.Reverse
                If (excludedCategory = TableCategory.None OrElse excludedCategory <> (kvp.Value.Category And excludedCategory)) AndAlso
                        Me.Provider.TableExists(connection, kvp.Key) Then
                    Try
                        Me.Provider.DropTable(connection, kvp.Key)
                    Catch
                        If Not ignoreErrors Then Throw
                    End Try
                End If
            Next
        End Using
    End Sub

#End Region

#Region " VIEWS "

    ''' <summary> Verifies that Views exist. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Private Sub AssertViewsExist()
        Using connection As System.Data.IDbConnection = Me.Provider.GetConnection
            For Each ViewName As String In Me.ViewNames
                If Not Me.Provider.ViewExists(connection, ViewName) Then Throw New System.Data.DataException($"View {ViewName } should exist")
            Next
        End Using
    End Sub

    ''' <summary> Gets the build Views. </summary>
    ''' <value> The build Views. </value>
    Public ReadOnly Property EnumeratedViews As IDictionary(Of String, ViewMaker) = New Dictionary(Of String, ViewMaker)

    ''' <summary> Gets a list of names of the Views. </summary>
    ''' <value> A list of names of the Views. </value>
    Public ReadOnly Property ViewNames As IEnumerable(Of String)
        Get
            Return Me.EnumeratedViews.Keys
        End Get
    End Property

    ''' <summary> Makes a View. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="maker">      The maker. </param>
    ''' <returns> A ViewCategory. </returns>
    Private Function MakeViewThis(ByVal connection As System.Data.IDbConnection, ByVal maker As ViewMaker) As (Success As Boolean, Details As String)
        Dim viewName As String = maker.ViewName
        If maker.CreateAction IsNot Nothing Then
            viewName = maker.CreateAction.Invoke(connection)
            Return If(String.Equals(viewName, maker.ViewName, StringComparison.OrdinalIgnoreCase),
                (True, String.Empty),
                (False, $"Created View name {viewName} does not match maker name {maker.ViewName}"))
        Else
            Return (True, String.Empty)
        End If
    End Function

    ''' <summary> Makes a View. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="maker">      The maker. </param>
    Private Sub MakeView(ByVal connection As System.Data.IDbConnection, ByVal maker As ViewMaker)
        Dim result As (Success As Boolean, Details As String)
        Dim exists As Boolean
        Try
            exists = connection.ViewExists(maker.ViewName)
            result = Me.MakeViewThis(connection, maker)
            If Not result.Success Then
                Throw New OperationFailedException(result.Details)
            End If
        Catch
            result = (False, $"Exception creating View {maker.ViewName};. ")
            Throw
        Finally
            If Not (result.Success OrElse exists) Then
                ' if failed and not existed, make sure to drop this View
                Try
                    Me.Provider.DropView(connection, maker.ViewName)
                Finally
                End Try
            End If
        End Try
    End Sub

    ''' <summary> Adds Views. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Private Sub AddViews()
        Using connection As System.Data.IDbConnection = Me.Provider.GetConnection
            connection.Open()
            For Each kvp As KeyValuePair(Of String, ViewMaker) In Me.EnumeratedViews
                Me.MakeView(connection, kvp.Value)
            Next
        End Using
    End Sub

#End Region

#Region " SCHEMA "

    ''' <summary> Builds the schema. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    Public Sub BuildSchema()
        Me.AddTables()
        Me.AddViews()
    End Sub

    ''' <summary> Assert schema objects exist. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub AssertSchemaObjectsExist()
        Me.AssertTablesExist()
        Me.AssertViewsExist()
    End Sub

#End Region

End Class

''' <summary> A view maker. </summary>
''' <remarks> David, 2020-07-04. </remarks>
Public Class ViewMaker

    ''' <summary> Gets or sets the name of the view. </summary>
    ''' <value> The name of the view. </value>
    Public Property ViewName As String

    ''' <summary> Gets or sets the create action. </summary>
    ''' <value> The create action. </value>
    Public Property CreateAction As Func(Of System.Data.IDbConnection, String)

End Class
