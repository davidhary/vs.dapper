Imports System.IO
Imports System.Data.SQLite
Imports Dapper
Imports System.Data

''' <summary> An SQLite provider. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/17/2018 </para>
''' </remarks>
Public Class SQLiteProvider
    Inherits ProviderBase

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ProviderBase

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ProviderBase
        If SQLiteProvider._Instance Is Nothing Then
            SyncLock SyncLocker
                SQLiteProvider._Instance = New SQLiteProvider()
            End SyncLock
        End If
        Return SQLiteProvider._Instance
    End Function

#End Region

#Region " CREATE PROVIDER "

    ''' <summary> The connection string format. </summary>
    Private Shared _ConnectionStringFormat As String = "Data Source={0};Mode=ReadWriteCreate;datetimeformat=CurrentCulture"

    ''' <summary> Gets or sets the connection string format. </summary>
    ''' <value> The connection string format. </value>
    Public Shared Property ConnectionStringFormat As String
        Get
            Return SQLiteProvider._ConnectionStringFormat
        End Get
        Set(value As String)
            SQLiteProvider._ConnectionStringFormat = value
        End Set
    End Property

    ''' <summary> Extracts database file name from the connection string. </summary>
    ''' <remarks> David, 6/22/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <returns> A String. </returns>
    Public Shared Function ExtractDatabaseFilename(ByVal connectionString As String) As String
        Dim fileName As String = String.Empty
        Dim dataSourcePrefix As String = "Data Source="
        For Each element As String In connectionString.Split(";"c)
            If element.StartsWith(dataSourcePrefix, StringComparison.OrdinalIgnoreCase) Then
                fileName = element.Substring(dataSourcePrefix.Length)
                Exit For
            End If
        Next
        Return fileName
    End Function

    ''' <summary> Builds connection string. </summary>
    ''' <remarks> David, 6/18/2020. </remarks>
    ''' <param name="filename"> Filename of the file. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildConnectionString(ByVal filename As String) As String
        Return String.Format(SQLiteProvider.ConnectionStringFormat, filename)
    End Function

    ''' <summary> Creates a new SQLiteProvider. </summary>
    ''' <remarks> David, 6/18/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <returns> A SQLiteProvider. </returns>
    Public Shared Function Create(ByVal connectionString As String) As SQLiteProvider
        Return New SQLiteProvider() With {.ConnectionString = connectionString}
    End Function

#End Region

#Region " DATABASE CONSTRUCTION "

    ''' <summary> Creates the database. </summary>
    ''' <remarks> David, 3/23/2020. </remarks>
    ''' <param name="masterConnectionString"> The master connection string. </param>
    ''' <param name="databaseName">           Name of the database. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overrides Function CreateDatabase(ByVal masterConnectionString As String, ByVal databaseName As String) As Boolean
        SQLiteConnection.CreateFile(Me.FileName)
        Return True
    End Function

    ''' <summary> Drop connections. </summary>
    ''' <remarks> David, 4/2/2020. </remarks>
    ''' <param name="masterConnectionString"> The master connection string. </param>
    ''' <param name="databaseName">           Filename of the connection file. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overrides Function DropConnections(ByVal masterConnectionString As String, ByVal databaseName As String) As Boolean
        Return True
    End Function

    ''' <summary> Drop database. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="masterConnectionString"> The master connection string. </param>
    ''' <param name="databaseName">           Filename of the connection file. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overrides Function DropDatabase(ByVal masterConnectionString As String, ByVal databaseName As String) As Boolean
        Dim fi As New FileInfo(Me.FileName)
        If fi.Exists Then fi.Delete()
        Return True
    End Function

#End Region

#Region " SQLITE CONNECTION "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Connection As SQLiteConnection
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the connection string. </summary>
    ''' <value> The connection string. </value>
    Public Overrides Property ConnectionString As String
        Get
            Return MyBase.ConnectionString
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ConnectionString) Then
                MyBase.ConnectionString = value
                If Not String.IsNullOrEmpty(value) Then
                    Me.FileName = SQLiteProvider.ExtractDatabaseFilename(value)
                    Dim fi As New FileInfo(Me.FileName)
                    If Not fi.Directory.Exists Then fi.Directory.Create()
                End If
            End If
        End Set
    End Property

    ''' <summary> Gets the connection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The connection. </returns>
    Private Function GetConnectionThis() As SQLiteConnection
        Return New SQLiteConnection(Me.ConnectionString)
    End Function

    ''' <summary> Query if this object is connection exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> <c>true</c> if connection exists; otherwise <c>false</c> </returns>
    Public Overrides Function IsConnectionExists() As Boolean
        Return Me.GetConnectionThis.Exists
    End Function

    ''' <summary> Connection state change. </summary>
    ''' <remarks>
    ''' Used to turn on foreign keys for cascading operations.
    ''' https://stackoverflow.com/questions/13641250/sqlite-delete-cascade-not-working.
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      State change event information. </param>
    Private Sub Connection_StateChange(sender As Object, e As System.Data.StateChangeEventArgs) Handles _Connection.StateChange
        Select Case e.CurrentState
            Case System.Data.ConnectionState.Open
                ' required only for SQLite, which defaults to OFF on open.
                Me._Connection.Execute("PRAGMA foreign_keys = ON; ")
        End Select
    End Sub

    ''' <summary> Builds master connection string. </summary>
    ''' <remarks> David, 3/23/2020. </remarks>
    ''' <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
    ''' <returns> A String. </returns>
    Public Overrides Function BuildMasterConnectionString(ByVal includeInitialCatalog As Boolean) As String
        Return Me.FileName
    End Function

#End Region

#Region " DATABASE "

    ''' <summary> Gets a connection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The connection. </returns>
    Public Overrides Function GetConnection() As System.Data.IDbConnection
        Me._Connection = Me.GetConnectionThis
        Return Me._Connection
    End Function

    ''' <summary> Queries if a given database exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="databaseName"> The database name. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function DatabaseExists(ByVal connection As System.Data.IDbConnection, databaseName As String) As Boolean
        Return File.Exists(Me.FileName)
    End Function

    ''' <summary> Compact database. </summary>
    ''' <remarks> David, 6/22/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="databaseName"> The database name. </param>
    ''' <returns> An Integer. </returns>
    Public Overrides Function CompactDatabase(connection As IDbConnection, databaseName As String) As Integer
        Return connection.Execute($"VACUUM {databaseName}; ")
    End Function

#End Region

#Region " TABLES "

    ''' <summary> Drop table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function DropTable(ByVal connection As System.Data.IDbConnection, ByVal tableName As String) As Boolean
        Return 1 < connection.Execute($"DROP TABLE IF EXISTS {tableName}")
    End Function

    ''' <summary> Gets or sets the name of the sq lite master table. </summary>
    ''' <value> The name of the SQLite master table. </value>
    Public Shared Property SQLiteMasterTableName As String = "sqlite_master"

    ''' <summary> Queries if a given table exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function TableExists(connection As System.Data.IDbConnection, tableName As String) As Boolean
        Return 1 = connection.ExecuteScalar(Of Integer)($"SELECT count(*) FROM {SQLiteProvider.SQLiteMasterTableName} WHERE type = 'table' AND name = '{tableName}'; ")
    End Function

    ''' <summary> Queries if a given index exists. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="indexName">  Name of the table. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overrides Function IndexExists(ByVal connection As System.Data.IDbConnection, ByVal indexName As String) As Boolean
        Return 1 = connection.ExecuteScalar(Of Integer)($"SELECT count(*) FROM {SQLiteProvider.SQLiteMasterTableName} WHERE type = 'index' AND name = '{indexName}'; ")
    End Function

    ''' <summary> Reseed identity. </summary>
    ''' <remarks> David, 6/1/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> An Integer. </returns>
    Public Overrides Function ReseedIdentity(connection As IDbConnection, tableName As String) As Integer
        Return connection.Execute($"UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='{tableName}'; ")
    End Function

#End Region

#Region " VIEWS "

    ''' <summary> Drop view. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="viewName">   Name of the view. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overrides Function DropView(ByVal connection As System.Data.IDbConnection, ByVal viewName As String) As Boolean
        Return 1 < connection.Execute($"DROP VIEW IF EXISTS {viewName}")
    End Function

    ''' <summary> Queries if a given View exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="viewName">   Name of the View. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function ViewExists(connection As System.Data.IDbConnection, viewName As String) As Boolean
        Return 1 = connection.ExecuteScalar(Of Integer)($"SELECT count(*) FROM {SQLiteProvider.SQLiteMasterTableName} WHERE type = 'view' AND name = '{viewName}'; ")
    End Function

#End Region

End Class
