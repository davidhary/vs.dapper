''' <summary> A table maker. </summary>
''' <remarks> David, 7/11/2020. </remarks>
Public Class TableMaker

    ''' <summary> Gets or sets the name of the table. </summary>
    ''' <value> The name of the table. </value>
    Public Property TableName As String

    ''' <summary> Gets or sets the category. </summary>
    ''' <value> The category. </value>
    Public Property Category As TableCategory

    ''' <summary> Gets or sets the create action. </summary>
    ''' <value> The create action. </value>
    Public Property CreateAction As Func(Of System.Data.IDbConnection, String)

    ''' <summary> Gets or sets the insert action. </summary>
    ''' <value> The insert action. </value>
    Public Property InsertAction As Func(Of System.Data.IDbConnection, Integer)

    ''' <summary> Gets or sets the insert file values action. </summary>
    ''' <value> The insert file values action. </value>
    Public Property InsertFileValuesAction As Func(Of System.Data.IDbConnection, String, Integer)

    ''' <summary> Gets or sets the filename of the values file. </summary>
    ''' <value> The filename of the values file. </value>
    Public Property ValuesFileName As String

    ''' <summary> Gets or sets the exists action. </summary>
    ''' <value> The exists action. </value>
    Public Property ExistsAction As Func(Of System.Data.IDbConnection, Integer)

    ''' <summary> Gets or sets the exists update action. </summary>
    ''' <value> The exists update action. </value>
    Public Property ExistsUpdateAction As Func(Of System.Data.IDbConnection, String, Boolean)

    ''' <summary> Gets or sets the exists update value. </summary>
    ''' <value> The exists update value. </value>
    Public Property ExistsUpdateValue As String

    ''' <summary> Gets or sets the exists build action. </summary>
    ''' <value> The exists build action. </value>
    Public Property ExistsBuildAction As Func(Of System.Data.IDbConnection, String)

    ''' <summary> Gets or sets the post create action. </summary>
    ''' <value> The post create action. </value>
    Public Property PostCreateAction As Func(Of System.Data.IDbConnection, String, Boolean)

    ''' <summary> Gets or sets the post create value. </summary>
    ''' <value> The post create value. </value>
    Public Property PostCreateValue As String

    ''' <summary> Gets or sets the insert values action. </summary>
    ''' <value> The insert values action. </value>
    Public Property InsertValuesAction As Func(Of System.Data.IDbConnection, IEnumerable(Of String), Boolean)

    ''' <summary> Gets or sets the values. </summary>
    ''' <value> The values. </value>
    Public Property Values As IEnumerable(Of String)

    ''' <summary> Gets or sets the failed. </summary>
    ''' <value> The failed. </value>
    Public Property Failed As Boolean

    ''' <summary> Gets or sets the fetch all action. </summary>
    ''' <value> The fetch all action. </value>
    Public Property FetchAllAction As Action(Of System.Data.IDbConnection)

End Class

''' <summary> A bit-field of flags for specifying a table category. </summary>
''' <remarks> David, 7/11/2020. </remarks>
<Flags>
Public Enum TableCategory

    ''' <summary> An enum constant representing the none option. </summary>
    None = 0

    ''' <summary> An enum constant representing the lookup option. </summary>
    Lookup = 1 << 0

    ''' <summary> An enum constant representing the existed option. </summary>
    Existed = 1 << 1

    ''' <summary> An enum constant representing the created option. </summary>
    Created = 1 << 2
End Enum

