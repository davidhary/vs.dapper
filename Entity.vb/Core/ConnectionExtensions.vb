Imports System.Runtime.CompilerServices

Namespace ConnectionExtensions

    ''' <summary> The Connection Extensions methods. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    Partial Public Module Methods

        ''' <summary> Executes the command using a transaction operation. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="command">    The command. </param>
        ''' <returns> The number of affected records. </returns>
        <Extension>
        Public Function ExecuteTransaction(ByVal connection As System.Data.IDbConnection, ByVal command As String) As Integer
            Return ProviderBase.ExecuteTransaction(connection, command)
        End Function

        ''' <summary> Counts the number of records in the specified table. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <returns> The total number of entities. </returns>
        <Extension>
        Public Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal tableName As String) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Return ProviderBase.CountEntities(connection, tableName)
        End Function

        ''' <summary> Queries if a given table exists. </summary>
        ''' <remarks> David, 5/16/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        <Extension>
        Public Function TableExists(connection As System.Data.IDbConnection, tableName As String) As Boolean
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Return ProviderBase.SelectProvider(connection).TableExists(connection, tableName)
        End Function

        ''' <summary> Queries if a given index exists. </summary>
        ''' <remarks> David, 5/16/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="indexName">  Name of the index. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        <Extension>
        Public Function IndexExists(connection As System.Data.IDbConnection, indexName As String) As Boolean
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Return ProviderBase.SelectProvider(connection).IndexExists(connection, indexName)
        End Function

        ''' <summary> Reseed table identity. </summary>
        ''' <remarks> David, 6/1/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <returns> An Integer. </returns>
        <Extension>
        Public Function ReseedTableIdentity(connection As System.Data.IDbConnection, tableName As String) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Return ProviderBase.SelectProvider(connection).ReseedIdentity(connection, tableName)
        End Function

        ''' <summary> Queries if a given view exists. </summary>
        ''' <remarks> David, 2020-07-04. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="viewName">   Name of the view. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        <Extension>
        Public Function ViewExists(connection As System.Data.IDbConnection, viewName As String) As Boolean
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Return ProviderBase.SelectProvider(connection).ViewExists(connection, viewName)
        End Function

    End Module

End Namespace

