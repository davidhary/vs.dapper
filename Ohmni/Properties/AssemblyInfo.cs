﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Dapper.Ohmni.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Dapper.Ohmni.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Dapper.Ohmni.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
