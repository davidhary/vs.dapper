using System.Collections.Generic;
using System.Linq;

using Dapper;

namespace isr.Dapper.Ohmni
{
    public partial class CincoStationEntity
    {


        #region " RELATIONS "

        /// <summary> Adds a session. </summary>
        /// <remarks> Assumes that sessions are. </remarks>
        /// <param name="connection">  The connection. </param>
        /// <param name="description"> The description. </param>
        /// <param name="testLevel">   The test level. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public CincoSessionEntity AddSession( System.Data.IDbConnection connection, string description, int testLevel )
        {
            int sessionNumber = 1;
            if ( this.Sessions?.Any() == true )
            {
            }
            else
            {
                _ = this.FetchSessions( connection );
            }

            if ( this.Sessions.Any() )
                sessionNumber = this.Sessions.Last().SessionNumber + 1;
            var session = new CincoSessionEntity() {
                StationAutoId = StationAutoId,
                SessionNumber = sessionNumber,
                StationName = StationName,
                Description = description,
                TestLevelId = testLevel
            };
            _ = session.Insert( connection );
            return session;
        }

        /// <summary> Gets or sets the Session entities. </summary>
        /// <value> The Session entities. </value>
        public IEnumerable<CincoSessionEntity> Sessions { get; private set; }

        /// <summary> Fetches the Sessions. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The Sessions. </returns>
        public int FetchSessions( System.Data.IDbConnection connection )
        {
            this.Sessions = CincoSessionEntity.FetchEntities( connection, this.StationAutoId );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.CincoStationEntity.Sessions ) );
            return this.Sessions?.Any() == true ? this.Sessions.Count() : 0;
        }

        /// <summary> Fetches the Sessions. </summary>
        /// <remarks>
        /// https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
        /// https://dapper-tutorial.net/querymultiple.
        /// </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="stationAutoId"> The identifier of the Station. </param>
        /// <returns> The Sessions. </returns>
        public int FetchSessions( System.Data.IDbConnection connection, int stationAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"select * from [{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] where {nameof( isr.Dapper.Ohmni.CincoStationNub.StationAutoId )} = @{nameof( isr.Dapper.Ohmni.CincoStationNub.StationAutoId )}; " );
            _ = queryBuilder.AppendLine( $"select * from [{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] where {nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )} = @{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )} ORDER BY {nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )}; " );
            var gridReader = connection.QueryMultiple( queryBuilder.ToString(), new CincoStationNub() { StationAutoId = stationAutoId } );
            ICincoStation entity = gridReader.ReadSingle<CincoStationNub>();
            if ( entity is null )
            {
                this.Sessions = new List<CincoSessionEntity>();
            }
            else
            {
                if ( !CincoStationNub.AreEqual( entity, this.ICache ) )
                {
                    this.UpdateCache( entity );
                    _ = this.UpdateStore();
                }

                this.Sessions = CincoSessionEntity.Populate( gridReader.Read<CincoSessionNub>() );
            }

            return this.Sessions?.Any() == true ? this.Sessions.Count() : 0;
        }

        #endregion


    }
}
