
namespace isr.Dapper.Ohmni.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Entity.My.ProjectTraceEventId.DapperOhmni;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Dapper Ohmni Entities Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Dapper Ohmni Entities Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Dapper.Ohmni.Entities";

        /// <summary> The test assembly strong name. </summary>
        public const string TestAssemblyStrongName = "isr.Dapper.Ohmni.Tests,PublicKey=" + global::Dapper.My.SolutionInfo.PublicKey;
    }
}
