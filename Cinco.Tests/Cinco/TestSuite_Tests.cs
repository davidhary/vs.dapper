using isr.Dapper.Ohmni;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Entities.Cinco.Tests
{
    public partial class TestSuite
    {

        /// <summary> (Unit Test Method) tests ohmni attribute type entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void OhmniAttributeTypeEntityTest()
        {
            CincoAuditor.AssertAttributeTypeEntity( TestInfo, this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests lot entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void LotEntityTest()
        {
            CincoAuditor.AssertLotEntity( TestInfo, this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests part entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void PartEntityTest()
        {
            CincoAuditor.AssertPartEntity( TestInfo, this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests part attribute range entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void PartAttributeRangeEntityTest()
        {
            CincoAuditor.AssertPartAttributeRangeEntity( TestInfo, this.GetProvider() );
        }

        /// <summary> (Unit Test Method) releases the history test. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void ReleaseHistoryTest()
        {
            CincoAuditor.AssertReleaseHistoryEntity( TestInfo, this.GetProvider(), BuiltDatabaseVersion );
        }

        /// <summary> (Unit Test Method) tests station entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void StationEntityTest()
        {
            CincoAuditor.AssertStationEntity( TestInfo, this.GetProvider(), false );
        }

        /// <summary> (Unit Test Method) tests session entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void OhmniCincoSessionEntityTest()
        {
            CincoAuditor.AssertSessionEntity( TestInfo, this.GetProvider(), CincoTestLevel.Normal );
        }

        /// <summary> (Unit Test Method) tests ohmni test level entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void OhmniTestLevelEntityTest()
        {
            CincoAuditor.AssertTestLevelEntity( TestInfo, this.GetProvider(), CincoTestLevel.Normal );
        }
    }
}
