using System;

using Dapper;

using isr.Dapper.Entity;
using isr.Dapper.Ohmni;

using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace isr.Dapper.Entities.Cinco.Tests
{
    public partial class TestSuite
    {

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="provider"> The provider. </param>
        public static void Initialize( ProviderBase provider )
        {
            Console.WriteLine( $".NET {Environment.Version}" );
            Console.WriteLine( $"Dapper {typeof( SqlMapper ).AssemblyQualifiedName}" );
            Console.WriteLine( $"Connection string: {provider.ConnectionString}" );
            Provider = provider;
            // test naked connection
            Assert.IsTrue( Provider.IsConnectionExists(), $"connection {Provider.GetConnection()} does not exist" );
            // test connection with event handling
            using ( var connection = Provider.GetConnection() )
            {
                Assert.IsTrue( connection.Exists(), "connection does not exist" );
            }

            if ( provider is null )
            {
                SchemaBuilder = new SchemaBuilder();
            }
            else
            {
                SchemaBuilder = new SchemaBuilder() { Provider = provider };
                EnumerateSchemaObjects( provider.ProviderType );
            }
            // this prevents testing.
            BuildDatabase( provider.BuildMasterConnectionString( false ) );
            SchemaBuilder.AssertSchemaObjectsExist();
        }

        /// <summary> Cleanups this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public static void Cleanup()
        {
            if ( TableCategory.None != TestSuiteSettings.Get().DeleteOption )
            {
                SchemaBuilder.DeleteAllRecords( TestSuiteSettings.Get().DeleteOption );
            }

            if ( TableCategory.None != TestSuiteSettings.Get().DropTablesOption )
            {
                SchemaBuilder.DropAllTables( TestSuiteSettings.Get().DropTablesOption, TestSuiteSettings.Get().IgnoreDropTableErrors );
            }
        }

        /// <summary> Gets or sets the provider. </summary>
        /// <value> The provider. </value>
        public static ProviderBase Provider { get; private set; }

        /// <summary> Gets or sets the schema builder. </summary>
        /// <value> The schema builder. </value>
        public static SchemaBuilder SchemaBuilder { get; private set; }

        /// <summary> Enumerate schema objects. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="providerType"> Type of the provider. </param>
        public static void EnumerateSchemaObjects( ProviderType providerType )
        {
            try
            {
                EnumerateSchemaObjectsThis( providerType );
            }
            catch
            {
                SchemaBuilder.EnumeratedTables.Clear();
                throw;
            }
        }

        /// <summary> Enumerate schema objects this. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="providerType"> Type of the provider. </param>
        private static void EnumerateSchemaObjectsThis( ProviderType providerType )
        {
            string providerFileLabel = providerType == ProviderType.SQLite ? TestSuiteSettings.Get().SQLiteFileLabel : TestSuiteSettings.Get().SqlServerFileLabel;
            var schemaBuilder = SchemaBuilder;
            schemaBuilder.EnumeratedTables.Clear();
            var tb = new TableMaker();

            // ATTRIBUTE TYPE 
            tb = new TableMaker() {
                TableName = OhmniAttributeTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmniAttributeTypeBuilder.CreateTable,
                InsertAction = CincoPartEntity.InsertAttributeTypeValues
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // TEST LEVEL
            tb = new TableMaker() {
                TableName = OhmniTestLevelBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmniTestLevelBuilder.CreateTable,
                InsertAction = CincoSessionEntity.InsertTestLevelValues
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // STATION
            tb = new TableMaker() {
                TableName = CincoStationBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = CincoStationBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SESSION
            tb = new TableMaker() {
                TableName = CincoSessionBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = CincoSessionBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART 
            tb = new TableMaker() {
                TableName = CincoPartBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = CincoPartBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART ATTRIBUTE RANGE
            OhmniPartAttributeRangeBuilder.AttributeTypeTableName = OhmniAttributeTypeBuilder.TableName;
            isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.AttributeTypeForeignKeyName = nameof( isr.Dapper.Ohmni.IOhmniAttributeType.AttributeTypeId );

            tb = new TableMaker() {
                TableName = OhmniPartAttributeRangeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmniPartAttributeRangeBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // LOT
            tb = new TableMaker() {
                TableName = CincoLotBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = CincoLotBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // VERSION
            BuiltDatabaseVersion = new Core.MyAssemblyInfo( typeof( TestSuite ).Assembly ).Version.ToString( 3 );
            tb = new TableMaker() {
                TableName = CincoReleaseHistoryBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = CincoReleaseHistoryBuilder.CreateTable,
                PostCreateAction = CincoReleaseHistoryBuilder.UpsertRevision,
                PostCreateValue = $"{BuiltDatabaseVersion},Initial Release"
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
        }

        /// <summary> Gets or sets the built database version. </summary>
        /// <value> The built database version. </value>
        public static string BuiltDatabaseVersion { get; set; }

        /// <summary> Creates data base. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        public static void CreateDatabase( string masterConnectionString )
        {
            if ( TestSuiteSettings.Get().CreateDatabase )
            {
                if ( System.IO.File.Exists( Provider.FileName ) )
                    System.IO.File.Delete( Provider.FileName );
                _ = CreateDatabaseThis( masterConnectionString );
            }
            else if ( !Provider.DatabaseExists() )
            {
                _ = CreateDatabaseThis( masterConnectionString );
            }
        }

        /// <summary> Creates database. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        private static bool CreateDatabaseThis( string masterConnectionString )
        {
            bool result = Provider.CreateDatabase( masterConnectionString, Provider.DatabaseName );
            string providerFileLabel = Provider.ProviderType == ProviderType.SQLite ? TestSuiteSettings.Get().SQLiteFileLabel : TestSuiteSettings.Get().SqlServerFileLabel;
            int count;
            string schemaFileName;
            if ( result )
            {
                schemaFileName = System.IO.Path.Combine( TestSuiteSettings.Get().SchemaFolderName, string.Format( TestSuiteSettings.Get().SchemaFileNameFormat, providerFileLabel ) );
                count = Provider.ExecuteScript( schemaFileName );
                result = count > 0;
            }

            if ( result )
            {
                schemaFileName = System.IO.Path.Combine( TestSuiteSettings.Get().SchemaFolderName, string.Format( TestSuiteSettings.Get().SchemaDataFileNameFormat, providerFileLabel ) );
                count = Provider.ExecuteScript( schemaFileName );
                result = count > 0;
            }

            return result;
        }

        /// <summary> Create or clear database and add all tables. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        public static void BuildDatabase( string masterConnectionString )
        {
            CreateDatabase( masterConnectionString );
            if ( TestSuiteSettings.Get().ClearDatabase )
                SchemaBuilder.DropAllTables( TestSuiteSettings.Get().IgnoreDropTableErrors );
            SchemaBuilder.BuildSchema();
        }
    }
}
