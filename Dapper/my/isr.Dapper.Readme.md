## ISR Dapper<sub>&trade;</sub>: Dapper Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*2.0.7345 2020-02-10*  
Uses readme.md version (2.0.204) as the current version.

*2.2.7209 2019-09-27*  
Uses NUGET version (2.2.203) as the current version.

*1.60.7175 2019-08-24*  
Updated to version 1.60

*1.50.6792 2018-08-06*  
Created

\(C\) 2017 Stack Exchange. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Dapper Libraries](https://bitbucket.org/davidhary/vs.dapper)
