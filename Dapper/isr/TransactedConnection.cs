﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dapper
{
    /// <summary>   A transacted connection. </summary>
    ///
    public class TransactedConnection : IDbConnection
    {

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="connection">   The connection. </param>
        /// <param name="transaction">  The transaction. </param>

        public TransactedConnection(IDbConnection connection, IDbTransaction transaction)
        {
            _conn = connection;
            _tran = transaction;
        }

        #region ISR

        private readonly IDbConnection _conn;
        public IDbConnection Connection
        {
            get { return _conn; }
        }

        private readonly IDbTransaction _tran;
        public IDbTransaction Transaction
        {
            get { return _tran; }
        }

        #endregion

        /// <summary>   Gets or sets the string used to open a database. </summary>
        ///
        /// <value> A string containing connection settings. </value>

        public string ConnectionString
        {
            get { return _conn.ConnectionString; }
            set { _conn.ConnectionString = value; }
        }

        /// <summary>
        /// Gets the time to wait while trying to establish a connection before terminating the attempt
        /// and generating an error.
        /// </summary>
        ///
        /// <value>
        /// The time (in seconds) to wait for a connection to open. The default value is 15 seconds.
        /// </value>

        public int ConnectionTimeout => _conn.ConnectionTimeout;

        /// <summary>
        /// Gets the name of the current database or the database to be used after a connection is opened.
        /// </summary>
        ///
        /// <value>
        /// The name of the current database or the name of the database to be used once a connection is
        /// open. The default value is an empty string.
        /// </value>

        public string Database => _conn.Database;

        /// <summary>   Gets the current state of the connection. </summary>
        ///
        /// <value> One of the <see cref="T:System.Data.ConnectionState" /> values. </value>

        public ConnectionState State => _conn.State;

        /// <summary>
        /// Begins a database transaction with the specified <see cref="T:System.Data.IsolationLevel" />
        /// value.
        /// </summary>
        ///
        /// <param name="il">   One of the <see cref="T:System.Data.IsolationLevel" /> values. </param>
        ///
        /// <returns>   An object representing the new transaction. </returns>

        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            throw new NotImplementedException();
        }

        /// <summary>   Begins a database transaction. </summary>
        ///
        /// <returns>   An object representing the new transaction. </returns>

        public IDbTransaction BeginTransaction() => _tran;

        /// <summary>   Changes the current database for an open Connection object. </summary>
        ///
        /// <param name="databaseName"> The name of the database to use in place of the current database. </param>

        public void ChangeDatabase(string databaseName) => _conn.ChangeDatabase(databaseName);

        /// <summary>   Closes the connection to the database. </summary>
        public void Close() => _conn.Close();

        /// <summary>   Creates and returns a Command object associated with the connection. </summary>
        ///
        /// <returns>   A Command object associated with the connection. </returns>

        public IDbCommand CreateCommand()
        {
            // The command inherits the "current" transaction.
            var command = _conn.CreateCommand();
            command.Transaction = _tran;
            return command;
        }

        public void Dispose() => _conn.Dispose();

        /// <summary>
        /// Opens a database connection with the settings specified by the ConnectionString property of
        /// the provider-specific Connection object.
        /// </summary>

        public void Open() => _conn.Open();
    }
}
