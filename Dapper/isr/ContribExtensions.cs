﻿using System;
using System.Data;

namespace Dapper.Contrib.Extensions
{
    /// <summary> Includes extensions for <see cref="IDbConnection">Connection</see>. </summary>
    /// <license>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    public static partial class Methods
    {

        #region PROPERTY ATTRIBUTES

        /// <summary>   Query if 'entityProperty' is property attribute. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="entityProperty">   The entity property. </param>
        /// <param name="attributeType">    Type of the attribute. </param>
        /// <returns>   True if property attribute, false if not. </returns>
        public static bool IsPropertyAttribute(System.Reflection.PropertyInfo entityProperty, Type attributeType)
        {
            return entityProperty.GetCustomAttributes(attributeType, false) is object;
        }

        /// <summary>   Query if 'entityProperty' is key property. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="entityProperty">   The entity property. </param>
        /// <returns>   True if key property, false if not. </returns>
        public static bool IsKeyProperty(System.Reflection.PropertyInfo entityProperty)
        {
            return IsPropertyAttribute(entityProperty, typeof(Contrib.Extensions.KeyAttribute));
        }

        /// <summary>   Query if 'entityProperty' is computed property. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="entityProperty">   The entity property. </param>
        /// <returns>   True if computed property, false if not. </returns>
        public static bool IsComputedProperty(System.Reflection.PropertyInfo entityProperty)
        {
            return IsPropertyAttribute(entityProperty, typeof(ComputedAttribute));
        }

        /// <summary>   Query if 'entityProperty' is explicit key property. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="entityProperty">   The entity property. </param>
        /// <returns>   True if explicit key property, false if not. </returns>
        public static bool IsExplicitKeyProperty(System.Reflection.PropertyInfo entityProperty)
        {
            return IsPropertyAttribute(entityProperty, typeof(ExplicitKeyAttribute));
        }

        #endregion

    }

}
