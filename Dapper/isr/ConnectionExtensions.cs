﻿using System;
using System.Data;
using System.Linq;

namespace Dapper
{
    /// <summary> Includes extensions for <see cref="IDbConnection">Connection</see>. </summary>
    /// <license>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    public static partial class Methods
    {

        #region CONNECTION STATE

        /// <summary> Returns true if 'connection' is closed. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if closed; otherwise <c>false</c> </returns>
        public static bool IsClosed(this IDbConnection connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            return connection.State == ConnectionState.Closed;
        }

        /// <summary> Returns true if 'connection' is open. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if open; otherwise <c>false</c> </returns>
        public static bool IsOpen(this IDbConnection connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            return ConnectionState.Open == (connection.State & ConnectionState.Open);
        }

        /// <summary>
        /// An IDbConnection extension method that determine if 'connection' exists.
        /// </summary>
        ///
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        ///
        /// <param name="connection">   The connection. </param>
        ///
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        public static bool Exists(this IDbConnection connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            bool result = false;
            bool wasClosed = true;
            try
            {
                if (connection.IsOpen())
                {
                    result = true;
                    wasClosed = false;
                }
                else
                {
                    connection.Open();
                    result = true;
                }
            }
            catch
            {
                result = false;
            }
            finally
            {
                if (result && wasClosed)
                    connection.Close();
            }
            return result;
        }

        /// <summary>   An IDbConnection extension method that queries if a given connection can open. </summary>
        /// <remarks>   David, 2020-03-17. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   A Tuple. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        public static (bool Success, string Details) CanOpen(this IDbConnection connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            (bool Success, string Details) result = (true, string.Empty);
            bool wasClosed = true;
            try
            {
                if (connection.IsOpen())
                {
                    result = (true, string.Empty);
                    wasClosed = false;
                }
                else
                {
                    connection.Open();
                    result = (true, string.Empty);
                }
            }
            catch (Exception ex)
            {
                result = (false, ex.ToString());
            }
            finally
            {
                if (result.Success && wasClosed)
                    connection.Close();
            }
            return result;
        }


        #endregion

        #region TABLES

        /// <summary>   Queries if a given table exists. </summary>
        ///
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        ///
        /// <param name="connection">   The connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        ///
        /// <returns>   <c>true</c> if it exists; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        public static bool SqlServerTableExists(this IDbConnection connection, string tableName)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentNullException(nameof(tableName));
            try
            {
                connection.ExecuteReader($"Select * from {tableName} Limit 1; ");
                return true;
            }
            catch
            {
                return false;
            }
        }


        /// <summary>   Queries if a given table exists on SQLite database. </summary>
        ///
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        ///
        /// <param name="connection">   The connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        ///
        /// <returns>   <c>true</c> if it exists; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        public static bool SQLiteTableExists(this IDbConnection connection, string tableName)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentNullException(nameof(tableName));
            try
            {
                return (1 == connection.Query<int>($"SELECT count(*) FROM sqlite_master WHERE type = 'table' AND name = '{tableName}'; ").Single());
            }
            catch
            {
                return false;
            }
        }

        #endregion

    }

}
