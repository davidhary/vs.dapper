﻿using System.Collections.Generic;

namespace isr.Dapper.Tests.RepositoryTests
{

    /// <summary> A repository interface. </summary>
    /// <remarks>
    /// David, 2018-08-06, https://www.codeproject.com/script/Membership/View.aspx?mid=3402606. <para>
    /// (c) 2018 Raul Rajat Singh. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IRepository<T> where T : class
    {

        /// <summary> Gets all. </summary>
        /// <returns> all. </returns>
        List<T> GetAll();

        /// <summary> Adds entity. </summary>
        /// <param name="entity"> The entity. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        bool Add( T entity );

        /// <summary> Adds entity. </summary>
        /// <param name="entity"> The entity. </param>
        /// <returns> The entity with the identity assigned. </returns>
        T AddEntity( T entity );

        /// <summary> Gets by identifier. </summary>
        /// <param name="id"> The Identifier to delete. </param>
        /// <returns> The by identifier. </returns>
        T GetById( int id );

        /// <summary> Updates the given entity. </summary>
        /// <param name="entity"> The entity. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        bool Update( T entity );

        /// <summary> Deletes the given ID. </summary>
        /// <param name="id"> The Identifier to delete. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        bool Delete( int id );
    }
}