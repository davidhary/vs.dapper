﻿
namespace isr.Dapper.Tests.RepositoryTests
{

    /// <summary> A book. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class Book
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary> Gets or sets the name of the book. </summary>
        /// <value> The name of the book. </value>
        public string BookName { get; set; }

        /// <summary> Gets or sets the isbn. </summary>
        /// <value> The isbn. </value>
        public string ISBN { get; set; }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return $"{this.Id}.{this.BookName}.{this.ISBN}";
        }
    }
}