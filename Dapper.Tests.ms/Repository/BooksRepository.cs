using System.Collections.Generic;
using System.Linq;

using Dapper;

namespace isr.Dapper.Tests.RepositoryTests
{

    /// <summary> The books repository. </summary>
    /// <remarks>
    /// (c) 2018 Raul Rajat Singh. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-08-06 </para>
    /// </remarks>
    public class BooksRepository : IRepository<Book>
    {

        /// <summary> The database connection. </summary>
        private readonly System.Data.IDbConnection _Dbconnection = null;

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public BooksRepository()
        {
            this._Dbconnection = new System.Data.SqlClient.SqlConnection( ConfigReader.ConnectionString );
        }

        /// <summary> Adds entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="book"> The entity. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool Add( Book book )
        {
            string sql = ConfigReader.InsertCommand;
            int count = this._Dbconnection.Execute( sql, book );
            return count > 0;
        }

        /// <summary> Adds entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="book"> The book. </param>
        /// <returns> The entity with the identity assigned. </returns>
        public Book AddEntity( Book book )
        {
            string sql = ConfigReader.InsertCommandReturnIdentity;
            book.Id = this._Dbconnection.Query<int>( sql, book ).Single();
            return book;
        }

        /// <summary> Deletes the given ID. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="id"> The Identifier to delete. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0037:Use inferred member name", Justification = "<Pending>" )]
        public bool Delete( int id )
        {
            string sql = ConfigReader.DeleteCommand;
            int count = this._Dbconnection.Execute( sql, new { Id = id } );
            return count > 0;
        }

        /// <summary> Gets all. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> all. </returns>
        public List<Book> GetAll()
        {
            string sql = ConfigReader.ReadAllCommand;
            var queryResult = this._Dbconnection.Query<Book>( sql );
            return queryResult.ToList();
        }

        /// <summary> Gets by identifier. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="id"> The Identifier to delete. </param>
        /// <returns> The by identifier. </returns>
        public Book GetById( int id )
        {
            Book book = null;
            string sql = ConfigReader.ReadOneCommand;
            var queryResult = this._Dbconnection.Query<Book>( sql, new { id } );

            if ( queryResult is object )
            {
                book = queryResult.FirstOrDefault();
            }

            return book;
        }

        /// <summary> Updates the given entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="book"> The entity. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool Update( Book book )
        {
            string sql = ConfigReader.UpdateCommand;
            int count = this._Dbconnection.Execute( sql, book );
            return count > 0;
        }
    }
}
