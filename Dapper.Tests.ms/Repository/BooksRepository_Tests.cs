using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Tests.RepositoryTests
{

    /// <summary> Asserts for using Dapper with a Books Repository. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-26 </para>
    /// </remarks>
    [TestClass()]
    public class BooksRepositoryTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                _BooksRepository = new BooksRepository();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            Assert.IsTrue( ConfigReader.Exists, $"{ typeof( isr.Dapper.Tests.RepositoryTests.ConfigReader )}.{nameof( isr.Dapper.Tests.RepositoryTests.ConfigReader )} settings should exist" );
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo?.AssertMessageQueue();
        }

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets or sets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " BOOK TETS "

        /// <summary> The books repository. </summary>
        private static IRepository<Book> _BooksRepository = null;

        /// <summary>   Enumerates the items in this collection that meet given criteria. </summary>
        /// <remarks>   David, 2020-10-05. </remarks>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<Book> Get()
        {
            return _BooksRepository.GetAll();
        }

        /// <summary>   Enumerates the items in this collection that meet given criteria. </summary>
        /// <remarks>   David, 2020-10-05. </remarks>
        /// <param name="id">   The Identifier to delete. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static Book Get( int id )
        {
            return _BooksRepository.GetById( id );
        }

        /// <summary> Adds book. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="book"> The book. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool Add( Book book )
        {
            bool result = _BooksRepository.Add( book );
            Assert.IsTrue( result, $"Failed adding {book}" );
            return result;
        }

        /// <summary> Adds an entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="book"> The book. </param>
        public static void AddEntity( Book book )
        {
            var result = _BooksRepository.AddEntity( book );
            Assert.IsTrue( result.Id > 0, $"Failed adding {book}" );
        }

        /// <summary> Updates this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="id">   The Identifier to delete. </param>
        /// <param name="book"> The book. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool Update( int id, Book book )
        {
            book.Id = id;
            return _BooksRepository.Update( book );
        }

        /// <summary> Deletes the given ID. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="id"> The Identifier to delete. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool Delete( int id )
        {
            return id <= 0 ? throw new ArgumentException( $"invalid id {id}", nameof( id ) ) : _BooksRepository.Delete( id );
        }

        /// <summary> (Unit Test Method) tests full blown exception. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod()]
        public void BooksRepositoryAddTest()
        {
            try
            {
                var book = new Book() { BookName = ConfigReader.BookName1, ISBN = ConfigReader.BookISBN1 };
                AddEntity( book );
                _ = Delete( book.Id );
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary> (Unit Test Method) tests books repository delete. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod()]
        public void BooksRepositoryDeleteTest()
        {
            try
            {
                _ = Delete( 2 );
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }

        #endregion

    }
}
