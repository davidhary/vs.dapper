﻿
namespace isr.Dapper.Tests.RepositoryTests
{

    /// <summary> An information class for the Pith tests. </summary>
    /// <remarks>
    /// David, 2018-04-13 <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    internal partial class ConfigReader
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private ConfigReader() : base()
        {
        }

        /// <summary> Gets the reader. </summary>
        /// <value> The reader. </value>
        private static Core.AppSettingsReader Reader { get; set; } = Core.AppSettingsReader.Get();

        /// <summary> Gets the exists. </summary>
        /// <value> The exists. </value>
        public static bool Exists => Reader.AppSettingBoolean();

        /// <summary> Gets the verbose. </summary>
        /// <value> The verbose. </value>
        public static bool Verbose => Reader.AppSettingBoolean();

        /// <summary> Gets the connection string. </summary>
        /// <value> The connection string. </value>
        public static string ConnectionString => Reader.AppSettingValue();

        /// <summary> Gets the 'insert' command. </summary>
        /// <value> The 'insert' command. </value>
        public static string InsertCommand => Reader.AppSettingValue();

        /// <summary> Gets the insert command return identity. </summary>
        /// <value> The insert command return identity. </value>
        public static string InsertCommandReturnIdentity => Reader.AppSettingValue();

        /// <summary> Gets the 'read all' command. </summary>
        /// <value> The 'read all' command. </value>
        public static string ReadAllCommand => Reader.AppSettingValue();

        /// <summary> Gets the 'read one' command. </summary>
        /// <value> The 'read one' command. </value>
        public static string ReadOneCommand => Reader.AppSettingValue();

        /// <summary> Gets the 'update' command. </summary>
        /// <value> The 'update' command. </value>
        public static string UpdateCommand => Reader.AppSettingValue();

        /// <summary> Gets the 'delete' command. </summary>
        /// <value> The 'delete' command. </value>
        public static string DeleteCommand => Reader.AppSettingValue();

        /// <summary> Gets the book name 1. </summary>
        /// <value> The book name 1. </value>
        public static string BookName1 => Reader.AppSettingValue();

        /// <summary> Gets the book name 2. </summary>
        /// <value> The book name 2. </value>
        public static string BookName2 => Reader.AppSettingValue();

        /// <summary> Gets the book isbn 1. </summary>
        /// <value> The book isbn 1. </value>
        public static string BookISBN1 => Reader.AppSettingValue();

        /// <summary> Gets the book isbn 2. </summary>
        /// <value> The book isbn 2. </value>
        public static string BookISBN2 => Reader.AppSettingValue();
    }
}