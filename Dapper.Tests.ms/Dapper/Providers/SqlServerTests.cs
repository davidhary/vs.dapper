using System;

using Dapper;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Tests.DapperTests
{
    #region " SQL SERVER "

    /// <summary> An SQL server test suite. </summary>
    /// <remarks>
    /// The test suites here implement TestSuiteBase so that each provider runs the entire set of
    /// tests without declarations per method If we want to support a new provider, they need only be
    /// added here - not in multiple places. (c) 2018 Integrated Scientific Resources, Inc. All
    /// rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-08-16 </para>
    /// </remarks>
    [TestClass(), TestCategory( "SqlServer" )]
    public class SqlServerTestSuite : TestBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.WriteLine( $".NET {Environment.Version}" );
                Console.WriteLine( $"Dapper {typeof( SqlMapper ).AssemblyQualifiedName}" );
                Console.WriteLine( $"Connection string: {ConnectionString}" );
                CreateDataTables();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        #endregion

        #region " IMPLEMENTATION "

        /// <summary> The connection string. </summary>
        public static readonly string ConnectionString = ConfigReader.SqlServerConnectionString;

        /// <summary> Gets the connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        public override System.Data.IDbConnection GetConnection()
        {
            return GetNewConnection();
        }

        /// <summary> Gets new connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new connection. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private static System.Data.IDbConnection GetNewConnection()
        {
            System.Data.IDbConnection connection = null;
            try
            {
                connection = new System.Data.SqlClient.SqlConnection( ConnectionString );
            }
            catch ( Exception ex )
            {
                Unavailable = $"is unavailable: {ex.Message}";
            }

            return connection;
        }

        /// <summary> Creates data tables. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private static void CreateDataTables()
        {
            using ( var connection = GetNewConnection() )
            {
                connection.Open();
            }
        }

        #endregion

    }

    #endregion

}
