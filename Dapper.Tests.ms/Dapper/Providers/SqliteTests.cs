using System;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;

using Dapper;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> An SQLite test suite. </summary>
    /// <remarks>
    /// The test suites here implement TestSuiteBase so that each provider runs the entire set of
    /// tests without declarations per method If we want to support a new provider, they need only be
    /// added here - not in multiple places. (c) 2018 Integrated Scientific Resources, Inc. All
    /// rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-08-16 </para>
    /// </remarks>
    [TestClass(), TestCategory( "SQLite" )]
    public class SQLiteTestSuite : TestBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.WriteLine( $".NET {Environment.Version}" );
                Console.WriteLine( $"Dapper {typeof( SqlMapper ).AssemblyQualifiedName}" );
                Console.WriteLine( $"Connection string: {ConnectionString}" );
                CreateDataTables();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        #endregion

        #region " IMPLEMENTATION "

        /// <summary> The connection string. </summary>
        public static readonly string ConnectionString = ConfigReader.SQLiteMemoryConnectionString;

        /// <summary> Gets the connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        public override System.Data.IDbConnection GetConnection()
        {
            return GetNewConnection();
        }

        /// <summary> Gets new connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new connection. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private static System.Data.IDbConnection GetNewConnection()
        {
            System.Data.IDbConnection connection = null;
            try
            {
                connection = new SQLiteConnection( ConnectionString );
            }
            catch ( Exception ex )
            {
                Unavailable = $"is unavailable: {ex.Message}";
            }

            return connection;
        }

        /// <summary> Creates data tables. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private static void CreateDataTables()
        {
            using ( var connection = GetNewConnection() )
            {
                connection.Open();
            }
        }

        #endregion

        #region " CUSTOM TESTS "

        /// <summary> (Unit Test Method) tests dapper enum value. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void DapperEnumValueTest()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                CommonTests.DapperEnumValue( connection );
            }
        }

        /// <summary> (Unit Test Method) issue 466: SQLite hates optimizations. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void SQLiteHatesOptimizations()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                SqlMapper.ResetTypeHandlers();
                var row = connection.Query<HazNameId>( "select 42 as Id" ).First();
                Assert.AreEqual( 42, row.Id );
                row = connection.Query<HazNameId>( "select 42 as Id" ).First();
                Assert.AreEqual( 42, row.Id );
                SqlMapper.ResetTypeHandlers();
                row = connection.QueryFirst<HazNameId>( "select 42 as Id" );
                Assert.AreEqual( 42, row.Id );
                row = connection.QueryFirst<HazNameId>( "select 42 as Id" );
                Assert.AreEqual( 42, row.Id );
            }
        }

        /// <summary> (Unit Test Method) Issue 466: SQLite hates optimizations asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task SQLiteHatesOptimizationsAsync()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                SqlMapper.ResetTypeHandlers();
                var row = (await connection.QueryAsync<HazNameId>( "select 42 as Id" ).ConfigureAwait( false )).First();
                Assert.AreEqual( 42, row.Id );
                row = (await connection.QueryAsync<HazNameId>( "select 42 as Id" ).ConfigureAwait( false )).First();
                Assert.AreEqual( 42, row.Id );
                SqlMapper.ResetTypeHandlers();
                row = await connection.QueryFirstAsync<HazNameId>( "select 42 as Id" ).ConfigureAwait( false );
                Assert.AreEqual( 42, row.Id );
                row = await connection.QueryFirstAsync<HazNameId>( "select 42 as Id" ).ConfigureAwait( false );
                Assert.AreEqual( 42, row.Id );
            }
        }

        /// <summary> (Unit Test Method) sq lite likes parameters with prefix. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void SQLiteLikesParametersWithPrefix()
        {
            this.SQLiteParameterNaming( true );
        }

        /// <summary> (Unit Test Method) sq lite likes parameters without prefix. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void SQLiteLikesParametersWithoutPrefix()
        {
            this.SQLiteParameterNaming( false );
        }

        /// <summary> Asserts SQLite parameter naming (Issue 467). </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="prefix"> True to prefix. </param>
        private void SQLiteParameterNaming( bool prefix )
        {
            using ( var connection = this.GetOpenConnection() )
            {
                var cmd = connection.CreateCommand();
                cmd.CommandText = "select @foo";
                cmd.Parameters.Add( new SQLiteParameter( prefix ? "@foo" : "foo", System.Data.SqlDbType.Int ) { Value = 42 } );
                int i = Convert.ToInt32( cmd.ExecuteScalar() );
                Assert.AreEqual( 42, i );
            }
        }

        #endregion

    }
}
