using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;

using Dapper;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> An SQL CE test suite. </summary>
    /// <remarks>
    /// The test suites here implement TestSuiteBase so that each provider runs the entire set of
    /// tests without declarations per method If we want to support a new provider, they need only be
    /// added here - not in multiple places. (c) 2018 Integrated Scientific Resources, Inc. All
    /// rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-08-16 </para>
    /// </remarks>
    [TestClass(), TestCategory( "SqlCe" )]
    public class SqlCETests : TestBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.WriteLine( $".NET {Environment.Version}" );
                Console.WriteLine( $"Dapper {typeof( SqlMapper ).AssemblyQualifiedName}" );
                Console.WriteLine( $"Connection string: {ConnectionString}" );
                CreateDataTables();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        #endregion

        #region " IMPLEMENTATION "

        /// <summary> Filename of the file. </summary>
        private static readonly string FileName = ConfigReader.SqlCeFileName;

        /// <summary> The connection string. </summary>
        public static readonly string ConnectionString = ConfigReader.SqlCeConnectionString;

        /// <summary> Gets the connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        public override System.Data.IDbConnection GetConnection()
        {
            return GetNewConnection();
        }

        /// <summary> Gets new connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new connection. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private static System.Data.IDbConnection GetNewConnection()
        {
            System.Data.IDbConnection connection = null;
            try
            {
                connection = new SqlCeConnection( ConnectionString );
            }
            catch ( Exception ex )
            {
                Unavailable = $"is unavailable: {ex.Message}";
            }

            return connection;
        }

        /// <summary> Creates data tables. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private static void CreateDataTables()
        {
            if ( File.Exists( FileName ) )
            {
                File.Delete( FileName );
            }

            using ( var engine = new SqlCeEngine( ConnectionString ) )
            {
                engine.CreateDatabase();
            }

            using ( var connection = GetNewConnection() )
            {
                connection.Open();
                connection.Execute( "create table Posts (ID int, Title nvarchar(50), Body nvarchar(50), AuthorID int)" );
                connection.Execute( "create table Authors (ID int, Name nvarchar(50))" );
                connection.Execute( "insert Posts values (1,'title','body',1)" );
                connection.Execute( "insert Posts values(2,'title2','body2',null)" );
                connection.Execute( "insert Authors values(1,'sam')" );
                IEnumerable<PostCE> data = connection.Query<PostCE, AuthorCE, PostCE>( "select * from Posts p left join Authors a on a.ID = p.AuthorID", ( post, author ) => {
                    post.Author = author;
                    return post;
                } ).ToList();
                var firstPost = data.ElementAtOrDefault( 0 );
                Assert.AreEqual( "title", firstPost.Title );
                Assert.AreEqual( "sam", firstPost.Author.Name );
                Assert.IsNull( data.ElementAtOrDefault( 1 ).Author );
            }

            Console.WriteLine( "Created database" );
        }

        /// <summary> A post ce. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private class PostCE
        {

            /// <summary> Gets or sets the identifier. </summary>
            /// <value> The identifier. </value>
            public int Id { get; set; }

            /// <summary> Gets or sets the title. </summary>
            /// <value> The title. </value>
            public string Title { get; set; }

            /// <summary> Gets or sets the body. </summary>
            /// <value> The body. </value>
            public string Body { get; set; }

            /// <summary> Gets or sets the author. </summary>
            /// <value> The author. </value>
            public AuthorCE Author { get; set; }
        }

        /// <summary> An author ce. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private class AuthorCE
        {

            /// <summary> Gets or sets the identifier. </summary>
            /// <value> The identifier. </value>
            public int Id { get; set; }

            /// <summary> Gets or sets the name. </summary>
            /// <value> The name. </value>
            public string Name { get; set; }
        }

        #endregion


    }
}
