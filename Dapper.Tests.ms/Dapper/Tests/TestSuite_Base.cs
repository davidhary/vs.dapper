using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Tests
{

    /// <summary> The test suite base class for Dapper Contrib functions. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-08 </para>
    /// </remarks>
    [TestClass()]
    public abstract partial class TestBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            if ( TestBase.Skip )
                Assert.Inconclusive();
            Assert.IsTrue( ConfigReader.Exists, $"{typeof( ConfigReader )}.{nameof( isr.Dapper.Tests.ConfigReader )} settings should exist" );
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo?.AssertMessageQueue();
        }

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets or sets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " MUST INHERIT "

        /// <summary> Gets the connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        public abstract System.Data.IDbConnection GetConnection();

        /// <summary> Gets closed connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The closed connection. </returns>
        protected System.Data.IDbConnection GetClosedConnection()
        {
            return this.GetConnection();
        }

        /// <summary> Gets open connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The open connection. </returns>
        protected System.Data.IDbConnection GetOpenConnection()
        {
            var connection = this.GetClosedConnection();
            connection.Open();
            return connection;
        }

        /// <summary> Gets or sets the skip. </summary>
        /// <value> The skip. </value>
        public static bool Skip { get; private set; }

        /// <summary> The unavailable. </summary>
        private static string _Unavailable;

        /// <summary> Gets or sets the unavailable. </summary>
        /// <value> The unavailable. </value>
        public static string Unavailable
        {
            get => _Unavailable;

            set {
                _Unavailable = value;
                Skip = !string.IsNullOrEmpty( value );
            }
        }

        #endregion

    }
}
