
namespace isr.Dapper.Tests
{

    /// <summary> An information class for the Pith tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-13 </para>
    /// </remarks>
    internal partial class ConfigReader
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private ConfigReader() : base()
        {
        }

        /// <summary> Gets the reader. </summary>
        /// <value> The reader. </value>
        private static Core.AppSettingsReader Reader { get; set; } = Core.AppSettingsReader.Get();

        /// <summary> Gets the exists. </summary>
        /// <value> The exists. </value>
        public static bool Exists => Reader.AppSettingBoolean();

        /// <summary> Gets the SQL server connection string. </summary>
        /// <value> The SQL server connection string. </value>
        public static string SqlServerConnectionString => Reader.AppSettingValue();

        /// <summary> Gets the sq lite memory connection string. </summary>
        /// <value> The sq lite memory connection string. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        public static string SQLiteMemoryConnectionString => Reader.AppSettingValue();

        /// <summary> Gets the sq lite connection string. </summary>
        /// <value> The sq lite connection string. </value>
        public static string SQLiteConnectionString => Reader.AppSettingValue();

        /// <summary> Gets the SQL ce connection string. </summary>
        /// <value> The SQL ce connection string. </value>
        public static string SqlCeConnectionString => Reader.AppSettingValue();

        /// <summary> Gets the filename of the SQL ce file. </summary>
        /// <value> The filename of the SQL ce file. </value>
        public static string SqlCeFileName => Reader.AppSettingValue();
    }
}
