﻿using System;
using System.Data.Common;

using Dapper;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> A common. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-11 </para>
    /// </remarks>
    public sealed class CommonTests
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private CommonTests()
        {
        }

        /// <summary> Gets some type. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> some type. </returns>
        public static Type GetSomeType()
        {
            return typeof( SomeType );
        }

        /// <summary> Dapper enum value. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        public static void DapperEnumValue( System.Data.IDbConnection connection )
        {

            // test passing as AsEnum, reading as int
            AnEnum v = ( AnEnum ) Conversions.ToInteger( connection.QuerySingle<int>( "select @v, @y, @z", new { v = AnEnum.B, y = ( AnEnum? ) AnEnum.B, z = ( AnEnum? ) default } ) );
            Assert.AreEqual( AnEnum.B, v );
            var args = new DynamicParameters();
            args.Add( "v", AnEnum.B );
            args.Add( "y", AnEnum.B );
            args.Add( "z", null );
            v = ( AnEnum ) Conversions.ToInteger( connection.QuerySingle<int>( "select @v, @y, @z", args ) );
            Assert.AreEqual( AnEnum.B, v );

            // test passing as int, reading as AnEnum
            int k = Conversion.Fix( ( int ) connection.QuerySingle<AnEnum>( "select @v, @y, @z", new { v = Conversion.Fix( ( int ) AnEnum.B ), y = ( int? ) Conversion.Fix( ( int ) AnEnum.B ), z = ( int? ) default } ) );
            Assert.AreEqual( k, Conversion.Fix( ( int ) AnEnum.B ) );
            args = new DynamicParameters();
            args.Add( "v", Conversion.Fix( ( int ) AnEnum.B ) );
            args.Add( "y", Conversion.Fix( ( int ) AnEnum.B ) );
            args.Add( "z", null );
            k = Conversion.Fix( ( int ) connection.QuerySingle<AnEnum>( "select @v, @y, @z", args ) );
            Assert.AreEqual( k, Conversion.Fix( ( int ) AnEnum.B ) );
        }

        /// <summary> Asserts date time. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static void TestDatetime( DbConnection connection )
        {
            DateTime? now = DateTimeOffset.UtcNow.DateTime;
            try
            {
                connection.Execute( "DROP TABLE Persons" );
            }
            catch
            {
            }

            connection.Execute( "CREATE TABLE Persons (id int not null, dob datetime null)" );
            connection.Execute( "INSERT Persons (id, dob) values (@id, @dob)", new { id = 7, dob = ( DateTime? ) default } );
            connection.Execute( "INSERT Persons (id, dob) values (@id, @dob)", new { id = 42, dob = now } );
            var row = connection.QueryFirstOrDefault<NullableDatePerson>( "SELECT id, dob, dob as dob2 FROM Persons WHERE id=@id", new { id = 7 } );
            Assert.IsNotNull( row );
            Assert.AreEqual( 7, row.Id );
            Assert.IsNull( row.DoB );
            Assert.IsNull( row.DoB2 );
            row = connection.QueryFirstOrDefault<NullableDatePerson>( "SELECT id, dob FROM Persons WHERE id=@id", new { id = 42 } );
            Assert.IsNotNull( row );
            Assert.AreEqual( 42, row.Id );
            row.DoB.Equals( now );
            row.DoB2.Equals( now );
        }

        /// <summary> A nullable date person. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private class NullableDatePerson
        {

            /// <summary> Gets or sets the identifier. </summary>
            /// <value> The identifier. </value>
            public int Id { get; set; }

            /// <summary> Gets or sets the do b. </summary>
            /// <value> The do b. </value>
            public DateTime? DoB { get; set; }

            /// <summary> Gets or sets the do b 2. </summary>
            /// <value> The do b 2. </value>
            public DateTime? DoB2 { get; set; }
        }
    }
}