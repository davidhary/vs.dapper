﻿
namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> A comment. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class Comment
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary> Gets or sets information describing the comment. </summary>
        /// <value> Information describing the comment. </value>
        public string CommentData { get; set; }
    }
}