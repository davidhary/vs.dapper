﻿
namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> A bar 1. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class Bar1
    {

        /// <summary> Gets or sets the identifier of the bar. </summary>
        /// <value> The identifier of the bar. </value>
        public int BarId { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }
    }
}