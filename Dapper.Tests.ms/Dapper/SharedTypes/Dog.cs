﻿using System;

namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> A dog. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class Dog
    {

        /// <summary> Gets or sets the age. </summary>
        /// <value> The age. </value>
        public int? Age { get; set; }

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public Guid Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the weight. </summary>
        /// <value> The weight. </value>
        public float? Weight { get; set; }

        /// <summary> Ignored property. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> An Integer. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public int IgnoredProperty()
        {
            return 1;
        }
    }
}