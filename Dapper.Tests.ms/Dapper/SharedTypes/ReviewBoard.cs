﻿
namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> A review board. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class ReviewBoard
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the user 1. </summary>
        /// <value> The user 1. </value>
        public User User1 { get; set; }

        /// <summary> Gets or sets the user 2. </summary>
        /// <value> The user 2. </value>
        public User User2 { get; set; }

        /// <summary> Gets or sets the user 3. </summary>
        /// <value> The user 3. </value>
        public User User3 { get; set; }

        /// <summary> Gets or sets the user 4. </summary>
        /// <value> The user 4. </value>
        public User User4 { get; set; }

        /// <summary> Gets or sets the user 5. </summary>
        /// <value> The user 5. </value>
        public User User5 { get; set; }

        /// <summary> Gets or sets the user 6. </summary>
        /// <value> The user 6. </value>
        public User User6 { get; set; }

        /// <summary> Gets or sets the user 7. </summary>
        /// <value> The user 7. </value>
        public User User7 { get; set; }

        /// <summary> Gets or sets the user 8. </summary>
        /// <value> The user 8. </value>
        public User User8 { get; set; }

        /// <summary> Gets or sets the user 9. </summary>
        /// <value> The user 9. </value>
        public User User9 { get; set; }
    }
}