﻿
namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> Values that represent short enums. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public enum ShortEnum : short
    {

        /// <summary> An enum constant representing the zero option. </summary>
        Zero = 0,

        /// <summary> An enum constant representing the one option. </summary>
        One = 1,

        /// <summary> An enum constant representing the two option. </summary>
        Two = 2,

        /// <summary> An enum constant representing the three option. </summary>
        Three = 3,

        /// <summary> An enum constant representing the four option. </summary>
        Four = 4,

        /// <summary> An enum constant representing the five option. </summary>
        Five = 5,

        /// <summary> An enum constant representing the six option. </summary>
        Six = 6
    }
}