﻿
namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> A person. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class Person
    {

        /// <summary> Gets or sets the identifier of the person. </summary>
        /// <value> The identifier of the person. </value>
        public int PersonId { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the occupation. </summary>
        /// <value> The occupation. </value>
        public string Occupation { get; set; }

        /// <summary> Gets or sets the number of legs. </summary>
        /// <value> The total number of legs. </value>
        public int NumberOfLegs { get; set; } = 2;

        /// <summary> Gets or sets the address. </summary>
        /// <value> The address. </value>
        public Address Address { get; set; }
    }
}