﻿
namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> A foo 1. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class Foo1
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary> Gets or sets the identifier of the bar. </summary>
        /// <value> The identifier of the bar. </value>
        public int BarId { get; set; }
    }
}