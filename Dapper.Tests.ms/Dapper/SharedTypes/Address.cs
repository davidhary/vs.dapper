﻿
namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> An address. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class Address
    {

        /// <summary> Gets or sets the identifier of the address. </summary>
        /// <value> The identifier of the address. </value>
        public int AddressId { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the identifier of the person. </summary>
        /// <value> The identifier of the person. </value>
        public int PersonId { get; set; }
    }
}