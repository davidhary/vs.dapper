﻿
namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> A post. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class Post
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary> Gets or sets the owner. </summary>
        /// <value> The owner. </value>
        public User Owner { get; set; }

        /// <summary> Gets or sets the content. </summary>
        /// <value> The content. </value>
        public string Content { get; set; }

        /// <summary> Gets or sets the comment. </summary>
        /// <value> The comment. </value>
        public Comment Comment { get; set; }
    }
}