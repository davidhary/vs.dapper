﻿
namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> some type. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class SomeType
    {

        /// <summary> Gets or sets a. </summary>
        /// <value> a. </value>
        public int A { get; set; }

        /// <summary> Gets or sets the b. </summary>
        /// <value> The b. </value>
        public string B { get; set; }
    }
}