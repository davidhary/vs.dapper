﻿
namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> A haz name identifier. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class HazNameId
    {

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }
    }
}