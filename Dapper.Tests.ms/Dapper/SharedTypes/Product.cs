﻿
namespace isr.Dapper.Tests.DapperTests
{

    /// <summary> A product. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class Product
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the category. </summary>
        /// <value> The category. </value>
        public Category Category { get; set; }
    }
}