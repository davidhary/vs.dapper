using Microsoft.VisualStudio.TestTools.UnitTesting;
#pragma warning disable IDE1006 // Naming Styles
namespace isr.Dapper.Tests.ContribTests
{
#pragma warning restore IDE1006 // Naming Styles

    /// <summary> The test suite base class for Dapper Contrib functions. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-08 </para>
    /// </remarks>
    [TestClass()]
    public abstract partial class TestSuite
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            Assert.IsTrue( isr.Dapper.Tests.ContribTests.ConfigReader.Exists, $"{typeof( ConfigReader )}.{nameof( isr.Dapper.Tests.ContribTests.ConfigReader )} settings should exist" );
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo?.AssertMessageQueue();
        }

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets or sets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " MUST INHERIT "

        /// <summary> Gets the connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        public abstract System.Data.IDbConnection GetConnection();

        /// <summary> Gets open connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The open connection. </returns>
        private System.Data.IDbConnection GetOpenConnection()
        {
            var connection = this.GetConnection();
            connection.Open();
            return connection;
        }

        #endregion

    }
}
