using System;

using Dapper;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Tests.ContribTests
{

    /// <summary> An SQL server test suite. </summary>
    /// <remarks>
    /// The test suites here implement TestSuiteBase so that each provider runs the entire set of
    /// tests without declarations per method If we want to support a new provider, they need only be
    /// added here - not in multiple places. (c) 2018 Integrated Scientific Resources, Inc. All
    /// rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-08-16 </para>
    /// </remarks>
    [TestClass(), TestCategory("SqlServer")]
    public class SqlServerTestSuite : TestSuite
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.WriteLine( $".NET {Environment.Version}" );
                Console.WriteLine( $"Dapper {typeof( SqlMapper ).AssemblyQualifiedName}" );
                Console.WriteLine( $"Connection string: {ConnectionString}" );
                CreateDataTables();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        #endregion

        #region " IMPLEMENTATION "

        /// <summary> The connection string. </summary>
        public static readonly string ConnectionString = ConfigReader.SqlServerConnectionString;

        /// <summary> Gets the connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        public override System.Data.IDbConnection GetConnection()
        {
            return GetNewConnection();
        }

        /// <summary> Gets new connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new connection. </returns>
        internal static System.Data.IDbConnection GetNewConnection()
        {
            return new System.Data.SqlClient.SqlConnection( ConnectionString );
        }

        /// <summary> Creates data tables. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private static void CreateDataTables()
        {
            using ( var connection = GetNewConnection() )
            {
                // ReSharper disable once AccessToDisposedClosure
                Action<string> dropTable = name => connection.Execute( $"IF OBJECT_ID('{name}', 'U') IS NOT NULL DROP TABLE [{name}]; " );
                connection.Open();
                dropTable( "Stuff" );
                connection.Execute( "CREATE TABLE Stuff (TheId int IDENTITY(1,1) not null, Name nvarchar(100) not null, Created DateTime null);" );
                dropTable( "People" );
                connection.Execute( "CREATE TABLE People (Id int IDENTITY(1,1) not null, Name nvarchar(100) not null);" );
                dropTable( "Users" );
                connection.Execute( "CREATE TABLE Users (Id int IDENTITY(1,1) not null, Name nvarchar(100) not null, Age int not null);" );
                dropTable( "Automobile" );
                connection.Execute( "CREATE TABLE Automobile (Id int IDENTITY(1,1) not null, Name nvarchar(100) not null);" );
                dropTable( "Result" );
                connection.Execute( "CREATE TABLE Result (Id int IDENTITY(1,1) not null, Name nvarchar(100) not null, [Order] int not null);" );
                dropTable( "ObjectX" );
                connection.Execute( "CREATE TABLE ObjectX (ObjectXId nvarchar(100) not null, Name nvarchar(100) not null);" );
                dropTable( "ObjectY" );
                connection.Execute( "CREATE TABLE ObjectY (ObjectYId int not null, Name nvarchar(100) not null);" );
                dropTable( "ObjectZ" );
                connection.Execute( "CREATE TABLE ObjectZ (Id int not null, Name nvarchar(100) not null);" );
                dropTable( "GenericType" );
                connection.Execute( "CREATE TABLE GenericType (Id nvarchar(100) not null, Name nvarchar(100) not null);" );
                dropTable( "NullableDate" );
                connection.Execute( "CREATE TABLE NullableDate (Id int IDENTITY(1,1) not null, DateValue DateTime null);" );
                dropTable( "ObjectZZ" );
                connection.Execute( "CREATE TABLE ObjectZZ (Id nvarchar(100) not null primary key, Name nvarchar(100) not null, IsActive bit not null DEFAULT 1, Timestamp DateTime NOT NULL DEFAULT (GETUTCDATE())) " );
            }
        }

        #endregion

    }
}
