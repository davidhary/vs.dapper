using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

using Dapper;
using Dapper.Contrib.Extensions;

using Microsoft.VisualBasic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Tests.ContribTests
{
    public abstract partial class TestSuite
    {

        #region " CONNECTION "

        /// <summary> (Unit Test Method) tests closed connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void TestClosedConnection()
        {
            using ( var connection = this.GetConnection() )
            {
                Assert.IsTrue( connection.Insert( new Users() { Name = "Adama", Age = 10 } ) > 0L );
                var users = connection.GetAll<Users>();
                Assert.IsTrue( users.Any() );
            }
        }

        /// <summary> (Unit Test Method) queries if a given connection exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void ConnectionExists()
        {
            using ( var connection = this.GetConnection() )
            {
                Assert.IsTrue( connection.Exists(), "connection does not exist" );
            }
        }

        #endregion

        #region " DATA TABLES "

        /// <summary> (Unit Test Method) queries if a given table exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void TableExists()
        {
            using ( var connection = this.GetConnection() )
            {
                // need to use provider bases tests here.
                // Assert.IsTrue(connection.TableExists("User"), "table users does not exist")
                // Assert.IsFalse(connection.TableExists("User1"), "table user1 exists")
            }
        }

        /// <summary> (Unit Test Method) test cars against Automobiles table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void TableName()
        {
            using ( var connection = this.GetConnection() )
            {
                // tests against "Automobiles" table (Table attribute)
                long id = connection.Insert( new Car() { Name = "Volvo" } );
                var car = connection.Get<Car>( id );
                Assert.IsNotNull( car );
                Assert.AreEqual( "Volvo", car.Name );
                Assert.AreEqual( "Volvo", connection.Get<Car>( id ).Name );
                Assert.IsTrue( connection.Update( new Car() { Id = ( int ) Conversion.Fix( id ), Name = "Saab" } ) );
                Assert.AreEqual( "Saab", connection.Get<Car>( id ).Name );
                Assert.IsTrue( connection.Delete( new Car() { Id = ( int ) Conversion.Fix( id ) } ) );
                Assert.IsNull( connection.Get<Car>( id ) );
            }
        }

        /// <summary> (Unit Test Method) asynchronous test cars against Automobiles table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task TableNameAsync()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                // tests against "Automobiles" table (Table attribute)
                int id = await connection.InsertAsync( new Car() { Name = "VolvoAsync" } ).ConfigureAwait( false );
                var car = await connection.GetAsync<Car>( id ).ConfigureAwait( false );
                Assert.IsNotNull( car );
                Assert.AreEqual( "VolvoAsync", car.Name );
                Assert.IsTrue( await connection.UpdateAsync( new Car() { Id = id, Name = "SaabAsync" } ).ConfigureAwait( false ) );
                Assert.AreEqual( "SaabAsync", (await connection.GetAsync<Car>( id ).ConfigureAwait( false )).Name );
                Assert.IsTrue( await connection.DeleteAsync( new Car() { Id = id } ).ConfigureAwait( false ) );
                Assert.IsNull( await connection.GetAsync<Car>( id ).ConfigureAwait( false ) );
            }
        }

        #endregion

        #region " GET "

        /// <summary> (Unit Test Method) tests simple get using the User table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void TestSimpleGet()
        {
            using ( var connection = this.GetConnection() )
            {
                long id = connection.Insert( new Users() { Name = "Adama", Age = 10 } );
                var user = connection.Get<Users>( id );
                Assert.AreEqual( user.Id, ( int ) Conversion.Fix( id ) );
                Assert.AreEqual( "Adama", user.Name );
                connection.Delete( user );
            }
        }

        /// <summary> (Unit Test Method) tests simple get asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task TestSimpleGetAsync()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                int id = await connection.InsertAsync( new Users() { Name = "Adama", Age = 10 } ).ConfigureAwait( false );
                var user = await connection.GetAsync<Users>( id ).ConfigureAwait( false );
                Assert.AreEqual( id, user.Id );
                Assert.AreEqual( "Adama", user.Name );
                await connection.DeleteAsync( user ).ConfigureAwait( false );
            }
        }

        /// <summary> (Unit Test Method) gets all with explicit key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void GetAllWithExplicitKey()
        {
            using ( var connection = this.GetConnection() )
            {
                connection.DeleteAll<ObjectX>();
                string guid = Guid.NewGuid().ToString();
                var o1 = new ObjectX() { ObjectXId = guid, Name = "Foo1" };
                connection.Insert( o1 );
                o1 = new ObjectX() { ObjectXId = Guid.NewGuid().ToString(), Name = "Foo2" };
                connection.Insert( o1 );
                IEnumerable<ObjectX> objectXs = connection.GetAll<ObjectX>().ToList();
                Assert.IsTrue( objectXs.Count() >= 2 );
                Assert.AreEqual( 1, objectXs.Count( x => (x.ObjectXId ?? "") == (guid ?? "") ) );
            }
        }

        /// <summary> (Unit Test Method) gets all explicit key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void GetAllExplicitKey()
        {
            using ( var connection = this.GetConnection() )
            {
                connection.DeleteAll<ObjectZZ>();
                string guid = Guid.NewGuid().ToString();
                // default values not working!
                IObjectZZ o1 = new ObjectZZ() { Id = guid, Name = "Foo1", IsActive = true, Timestamp = DateTime.UtcNow };
                connection.Insert( o1 );
                o1 = connection.Get<ObjectZZ>( o1.Id );
                ;
                // default values not working!
                o1 = new ObjectZZ() { Id = Guid.NewGuid().ToString(), Name = "Foo2", IsActive = true, Timestamp = DateTime.UtcNow };
                connection.Insert( o1 );
                IEnumerable<ObjectZZ> objectXs = connection.GetAll<ObjectZZ>().ToList();
                Assert.IsTrue( objectXs.Count() >= 2 );
                Assert.AreEqual( 1, objectXs.Count( x => (x.Id ?? "") == (guid ?? "") ) );
            }
        }

        /// <summary> (Unit Test Method) gets all. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void GetAll()
        {
            const int numberOfEntities = 10;
            var users = new List<Users>();
            for ( int i = 0; i <= numberOfEntities - 1; i++ )
                users.Add( new Users() { Name = "User " + i, Age = i } );
            using ( var connection = this.GetConnection() )
            {
                connection.DeleteAll<Users>();
                long total = connection.Insert( users );
                Assert.AreEqual( ( int ) total, numberOfEntities );
                users = connection.GetAll<Users>().ToList();
                Assert.AreEqual( users.Count, numberOfEntities );
                IEnumerable<IUsers> iusers = connection.GetAll<IUsers>().ToList();
                Assert.AreEqual( iusers.Count(), numberOfEntities );
                for ( int i = 0; i <= numberOfEntities - 1; i++ )
                    Assert.AreEqual( iusers.ElementAtOrDefault( i ).Age, i );
            }
        }

        /// <summary> (Unit Test Method) gets all asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> all asynchronous. </returns>
        [TestMethod]
        public async Task GetAllAsync()
        {
            const int numberOfEntities = 10;
            var users = new List<Users>();
            for ( int i = 0; i <= numberOfEntities - 1; i++ )
                users.Add( new Users() { Name = "User " + i, Age = i } );
            using ( var connection = this.GetOpenConnection() )
            {
                await connection.DeleteAllAsync<Users>().ConfigureAwait( false );
                int total = await connection.InsertAsync( users ).ConfigureAwait( false );
                Assert.AreEqual( total, numberOfEntities );
                users = ( List<Users> ) await connection.GetAllAsync<Users>().ConfigureAwait( false );
                Assert.AreEqual( users.Count, numberOfEntities );
                var iusers = await connection.GetAllAsync<IUsers>().ConfigureAwait( false );
                Assert.AreEqual( iusers.ToList().Count, numberOfEntities );
            }
        }

        /// <summary> (Unit Test Method) gets and get all with nullable values. Issue 933. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void GetAndGetAllWithNullableValues()
        {
            // Test for issue #933
            using ( var connection = this.GetConnection() )
            {
                long id1 = connection.Insert( new NullableDate() { DateValue = new DateTime( 2011, 7, 14 ) } );
                long id2 = connection.Insert( new NullableDate() { DateValue = default } );
                var value1 = connection.Get<INullableDate>( id1 );
                Assert.AreEqual( new DateTime( 2011, 7, 14 ), value1.DateValue.Value );
                var value2 = connection.Get<INullableDate>( id2 );
                Assert.IsTrue( value2.DateValue is null );
                var value3 = connection.GetAll<INullableDate>().ToList();
                Assert.AreEqual( new DateTime( 2011, 7, 14 ), value3[0].DateValue.Value );
                Assert.IsTrue( value3[1].DateValue is null );
            }
        }

        /// <summary>
        /// (Unit Test Method) gets asynchronous and get all asynchronous with nullable values.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The asynchronous and get all asynchronous with nullable values. </returns>
        [TestMethod]
        public async Task GetAsyncAndGetAllAsyncWithNullableValues()
        {
            // Test for issue #933
            using ( var connection = this.GetOpenConnection() )
            {
                long id1 = connection.Insert( new NullableDate() { DateValue = new DateTime( 2011, 7, 14 ) } );
                long id2 = connection.Insert( new NullableDate() { DateValue = default } );
                var value1 = await connection.GetAsync<INullableDate>( id1 ).ConfigureAwait( false );
                Assert.AreEqual( new DateTime( 2011, 7, 14 ), value1.DateValue.Value );
                var value2 = await connection.GetAsync<INullableDate>( id2 ).ConfigureAwait( false );
                Assert.IsTrue( value2.DateValue is null );
                var value3 = await connection.GetAllAsync<INullableDate>().ConfigureAwait( false );
                IEnumerable<INullableDate> valuesList = value3.ToList();
                Assert.AreEqual( new DateTime( 2011, 7, 14 ), valuesList.ElementAtOrDefault( 0 ).DateValue.Value );
                Assert.IsTrue( valuesList.ElementAtOrDefault( 1 ).DateValue is null );
            }
        }

        #endregion

        #region " INSERT "

        /// <summary> (Unit Test Method) inserts a check key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void InsertCheckKey()
        {
            using ( var connection = this.GetConnection() )
            {
                Assert.IsNull( connection.Get<IUsers>( 3 ) );
                var user = new Users() { Name = "Adamb", Age = 10 };
                int id = ( int ) Conversion.Fix( connection.Insert( user ) );
                Assert.AreEqual( user.Id, id );
            }
        }

        /// <summary> (Unit Test Method) inserts a check key asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task InsertCheckKeyAsync()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                await connection.DeleteAllAsync<Users>().ConfigureAwait( false );
                Assert.IsNull( await connection.GetAsync<IUsers>( 3 ).ConfigureAwait( false ) );
                var user = new Users() { Name = "Adamb", Age = 10 };
                int id = await connection.InsertAsync( user ).ConfigureAwait( false );
                Assert.AreEqual( user.Id, id );
            }
        }

        /// <summary> (Unit Test Method) short identity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void ShortIdentity()
        {
            using ( var connection = this.GetConnection() )
            {
                const string name = "First item";
                long id = connection.Insert( new Stuff() { Name = name } );
                Assert.IsTrue( id > 0L ); // 1-n are valid here, due to parallel tests
                var item = connection.Get<Stuff>( id );
                Assert.AreEqual( item.TheId, ( short ) Conversion.Fix( id ) );
                Assert.AreEqual( item.Name, name );
            }
        }

        /// <summary> (Unit Test Method) null date time. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void NullDatetime()
        {
            using ( var connection = this.GetConnection() )
            {
                connection.Insert( new Stuff() { Name = "First item" } );
                connection.Insert( new Stuff() { Name = "Second item", Created = DateTime.UtcNow } );
                IEnumerable<Stuff> stuff = connection.Query<Stuff>( "select * from Stuff" ).ToList();
                Assert.IsNull( stuff.ElementAtOrDefault( 0 ).Created );
                Assert.IsNotNull( stuff.Last().Created );
            }
        }

        /// <summary> (Unit Test Method) inserts a field with reserved name. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void InsertFieldWithReservedName()
        {
            using ( var connection = this.GetConnection() )
            {
                connection.DeleteAll<Users>();
                long id = connection.Insert( new Result() { Name = "Adam", Order = 1 } );
                var result = connection.Get<Result>( id );
                Assert.AreEqual( 1, result.Order );
            }
        }

        /// <summary> (Unit Test Method) inserts a field with reserved name asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task InsertFieldWithReservedNameAsync()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                await connection.DeleteAllAsync<Users>().ConfigureAwait( false );
                long id = await connection.InsertAsync( new Result() { Name = "Adam", Order = 1 } ).ConfigureAwait( false );
                var result = await connection.GetAsync<Result>( id ).ConfigureAwait( false );
                Assert.AreEqual( 1, result.Order );
            }
        }

        /// <summary> (Unit Test Method) inserts an enumerable. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void InsertEnumerable()
        {
            this.InsertHelper( src => src.AsEnumerable() );
        }

        /// <summary> (Unit Test Method) inserts an array. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void InsertArray()
        {
            this.InsertHelper( src => src.ToArray() );
        }

        /// <summary> (Unit Test Method) inserts a list. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void InsertList()
        {
            this.InsertHelper( src => src.ToList() );
        }

        /// <summary> Helper method that insert. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="helper"> The helper. </param>
        private void InsertHelper<T>( Func<IEnumerable<Users>, T> helper ) where T : class
        {
            const int numberOfEntities = 10;
            var users = new List<Users>();
            for ( int i = 0; i <= numberOfEntities - 1; i++ )
                users.Add( new Users() { Name = "User " + i, Age = i } );
            using ( var connection = this.GetConnection() )
            {
                connection.DeleteAll<Users>();
                long total = connection.Insert( helper( users ) );
                Assert.AreEqual( ( int ) total, numberOfEntities );
                users = connection.Query<Users>( "select * from Users" ).ToList();
                Assert.AreEqual( users.Count, numberOfEntities );
            }
        }

        /// <summary> (Unit Test Method) inserts an enumerable asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task InsertEnumerableAsync()
        {
            await this.InsertHelperAsync( src => src.AsEnumerable() ).ConfigureAwait( false );
        }

        /// <summary> (Unit Test Method) inserts an array asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task InsertArrayAsync()
        {
            await this.InsertHelperAsync( src => src.ToArray() ).ConfigureAwait( false );
        }

        /// <summary> (Unit Test Method) inserts a list asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task InsertListAsync()
        {
            await this.InsertHelperAsync( src => src.ToList() ).ConfigureAwait( false );
        }

        /// <summary> Inserts a helper asynchronous described by helper. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="helper"> The helper. </param>
        /// <returns> A Task. </returns>
        private async Task InsertHelperAsync<T>( Func<IEnumerable<Users>, T> helper ) where T : class
        {
            const int numberOfEntities = 10;
            var users = new List<Users>();
            for ( int i = 0; i <= numberOfEntities - 1; i++ )
                users.Add( new Users() { Name = "User " + i, Age = i } );
            using ( var connection = this.GetOpenConnection() )
            {
                await connection.DeleteAllAsync<Users>().ConfigureAwait( false );
                long total = await connection.InsertAsync( helper( users ) ).ConfigureAwait( false );
                Assert.AreEqual( ( int ) total, numberOfEntities );
                users = connection.Query<Users>( "select * from Users" ).ToList();
                Assert.AreEqual( users.Count, numberOfEntities );
            }
        }

        /// <summary>
        /// (This method is obsolete) (Unit Test Method) inserts a with custom database type.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        [TestMethod]
        [Ignore()]
        [Obsolete( "Not parallel friendly - thinking about how to test this" )]
        public void InsertWithCustomDbType()
        {
            SqlMapperExtensions.GetDatabaseType = conn => "SQLiteConnection";
            bool sqliteCodeCalled = false;
            using ( var connection = this.GetConnection() )
            {
                connection.DeleteAll<Users>();
                Assert.IsNull( connection.Get<Users>( 3 ) );
                try
                {
                    connection.Insert( new Users() { Name = "Adam", Age = 10 } );
                }
                catch ( SqlCeException ex )
                {
                    sqliteCodeCalled = ex.Message.IndexOf( "There was an error parsing the query", StringComparison.OrdinalIgnoreCase ) >= 0;
                }
            }

            SqlMapperExtensions.GetDatabaseType = null;
            if ( !sqliteCodeCalled )
            {
                throw new InvalidOperationException( "Was expecting SQLite code to be called" );
            }
        }

        /// <summary> (Unit Test Method) inserts a with custom table name mapper. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void InsertWithCustomTableNameMapper()
        {
            SqlMapperExtensions.TableNameMapper = type => { switch ( type.Name ?? "" ) { case "Person": { return "People"; } default: { TableAttribute tableattr = type.GetCustomAttributes( false ).SingleOrDefault( attr => attr.GetType().Name == "TableAttribute" ) as TableAttribute; if ( tableattr is object ) { return tableattr.Name; } string name = type.Name + "s"; return type.IsInterface && name.StartsWith( "I", StringComparison.OrdinalIgnoreCase ) ? name.Substring( 1 ) : name; } } };
            using ( var connection = this.GetConnection() )
            {
                long id = connection.Insert( new Person() { Name = "Mr Mapper" } );
                Assert.AreEqual( 1, ( int ) id );
                connection.GetAll<Person>();
            }
        }

        #endregion

        #region " DELETE "

        /// <summary> (Unit Test Method) deletes all. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void DeleteAll()
        {
            using ( var connection = this.GetConnection() )
            {
                long id1 = connection.Insert( new Users() { Name = "Alice", Age = 32 } );
                long id2 = connection.Insert( new Users() { Name = "Bob", Age = 33 } );
                Assert.IsTrue( connection.DeleteAll<Users>() );
                Assert.IsNull( connection.Get<Users>( id1 ) );
                Assert.IsNull( connection.Get<Users>( id2 ) );
            }
        }

        /// <summary> (Unit Test Method) deletes all asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task DeleteAllAsync()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                await connection.DeleteAllAsync<Users>().ConfigureAwait( false );
                long id1 = await connection.InsertAsync( new Users() { Name = "Alice", Age = 32 } ).ConfigureAwait( false );
                long id2 = await connection.InsertAsync( new Users() { Name = "Bob", Age = 33 } ).ConfigureAwait( false );
                await connection.DeleteAllAsync<Users>().ConfigureAwait( false );
                Assert.IsNull( await connection.GetAsync<Users>( id1 ).ConfigureAwait( false ) );
                Assert.IsNull( await connection.GetAsync<Users>( id2 ).ConfigureAwait( false ) );
            }
        }

        /// <summary> (Unit Test Method) deletes the enumerable. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void DeleteEnumerable()
        {
            this.DeleteHelper( src => src.AsEnumerable() );
        }

        /// <summary> (Unit Test Method) deletes the array. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void DeleteArray()
        {
            this.DeleteHelper( src => src.ToArray() );
        }

        /// <summary> (Unit Test Method) deletes the list. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void DeleteList()
        {
            this.DeleteHelper( src => src.ToList() );
        }

        /// <summary> Helper method that delete. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="helper"> The helper. </param>
        private void DeleteHelper<T>( Func<IEnumerable<Users>, T> helper ) where T : class
        {
            const int numberOfEntities = 10;
            var users = new List<Users>();
            for ( int i = 0; i <= numberOfEntities - 1; i++ )
                users.Add( new Users() { Name = "User " + i, Age = i } );
            using ( var connection = this.GetConnection() )
            {
                connection.DeleteAll<Users>();
                long total = connection.Insert( helper( users ) );
                Assert.AreEqual( ( int ) total, numberOfEntities );
                users = connection.Query<Users>( "select * from Users" ).ToList();
                Assert.AreEqual( users.Count, numberOfEntities );
                IEnumerable<Users> usersToDelete = users.Take( 10 ).ToList();
                connection.Delete( helper( usersToDelete ) );
                users = connection.Query<Users>( "select * from Users" ).ToList();
                Assert.AreEqual( users.Count, numberOfEntities - 10 );
            }
        }

        /// <summary> (Unit Test Method) deletes the enumerable asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task DeleteEnumerableAsync()
        {
            await this.DeleteHelperAsync( src => src.AsEnumerable() ).ConfigureAwait( false );
        }

        /// <summary> (Unit Test Method) deletes the array asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task DeleteArrayAsync()
        {
            await this.DeleteHelperAsync( src => src.ToArray() ).ConfigureAwait( false );
        }

        /// <summary> (Unit Test Method) deletes the list asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task DeleteListAsync()
        {
            await this.DeleteHelperAsync( src => src.ToList() ).ConfigureAwait( false );
        }

        /// <summary> Deletes the helper asynchronous described by helper. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="helper"> The helper. </param>
        /// <returns> A Task. </returns>
        private async Task DeleteHelperAsync<T>( Func<IEnumerable<Users>, T> helper ) where T : class
        {
            const int numberOfEntities = 10;
            var users = new List<Users>();
            for ( int i = 0; i <= numberOfEntities - 1; i++ )
                users.Add( new Users() { Name = "User " + i, Age = i } );
            using ( var connection = this.GetOpenConnection() )
            {
                await connection.DeleteAllAsync<Users>().ConfigureAwait( false );
                long total = await connection.InsertAsync( helper( users ) ).ConfigureAwait( false );
                Assert.AreEqual( ( int ) total, numberOfEntities );
                users = connection.Query<Users>( "select * from Users" ).ToList();
                Assert.AreEqual( users.Count, numberOfEntities );
                var usersToDelete = users.Take( 10 ).ToList();
                await connection.DeleteAsync( helper( usersToDelete ) ).ConfigureAwait( false );
                users = connection.Query<Users>( "select * from Users" ).ToList();
                Assert.AreEqual( users.Count, numberOfEntities - 10 );
            }
        }

        #endregion

        #region " UPDATE "

        /// <summary> (Unit Test Method) tests dual update. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void DualUpdateTest()
        {
            using ( var connection = this.GetConnection() )
            {
                connection.DeleteAll<Users>();
                Assert.IsNull( connection.Get<Users>( 3 ) );
                // insert with computed attribute that should be ignored
                connection.Insert( new Car() { Name = "Volvo", Computed = "this property should be ignored" } );
                string userName = "Adam";
                long id = connection.Insert( new Users() { Name = userName, Age = 10 } );
                // get a user with "isdirty" tracking
                var user = connection.Get<IUsers>( id );
                Assert.AreEqual( userName, user.Name );
                string previousUserName = user.Name;
                Assert.IsFalse( connection.Update( user ), $"Updated even though previous user name did not change {previousUserName}={user.Name}" );
                previousUserName = user.Name;
                userName = "Bob";
                user.Name = userName;
                Assert.IsTrue( connection.Update( user ), $"Did not update even though previous user name changed {previousUserName}<>{user.Name}" );
                previousUserName = user.Name;
                // this test fails unless the user is fetched; change tracking works only once.
                user = connection.Get<IUsers>( id );
                Assert.IsFalse( connection.Update( user ), $"Updated even though previous user name did not change {previousUserName}={user.Name}" );
                previousUserName = user.Name;
                userName = "James";
                user.Name = userName;
                Assert.IsTrue( connection.Update( user ), $"Did not update even though previous user name changed {previousUserName}<>{user.Name}" );
                previousUserName = user.Name;
                // this test fails unless the user is fetched; change tracking works only once.
                user = connection.Get<IUsers>( id );
                Assert.IsFalse( connection.Update( user ), $"Updated even though previous user name did not change {previousUserName}={user.Name}" );
            }
        }

        /// <summary> (Unit Test Method) inserts a get update. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void InsertGetUpdate()
        {
            using ( var connection = this.GetConnection() )
            {
                connection.DeleteAll<Users>();
                Assert.IsNull( connection.Get<Users>( 3 ) );
                // insert with computed attribute that should be ignored
                connection.Insert( new Car() { Name = "Volvo", Computed = "this property should be ignored" } );
                long id = connection.Insert( new Users() { Name = "Adam", Age = 10 } );
                // get a user with "isdirty" tracking
                var user = connection.Get<IUsers>( id );
                Assert.AreEqual( "Adam", user.Name );
                SqlMapperExtensions.IProxy IProxyUser = user as SqlMapperExtensions.IProxy;
                Assert.IsNotNull( IProxyUser, "IUser cannot be cast as IProxy" );
                Assert.IsFalse( IProxyUser.IsDirty, "IUser should not be dirty after first fetch from the database" );
                Assert.IsFalse( connection.Update( user ) ); // returns false if not updated, based on tracking
                user.Name = "Bob";
                Assert.IsTrue( IProxyUser.IsDirty, "IUser should be dirty after setting a new value for the user name" );
                Assert.IsTrue( connection.Update( user ) );  // returns true if updated, based on tracking
                user = connection.Get<IUsers>( id );
                IProxyUser = user as SqlMapperExtensions.IProxy;
                Assert.IsFalse( IProxyUser.IsDirty, "IUser should not be dirty after second fetch from the database" );
                Assert.AreEqual( "Bob", user.Name );
                Assert.IsFalse( connection.Update( user ), "IUser updated event through not dirty" );

                // get a user with no tracking
                var notrackedUser = connection.Get<Users>( id );
                IProxyUser = notrackedUser as SqlMapperExtensions.IProxy;
                Assert.IsNull( IProxyUser, "User was cast as IProxy--only IUser should" );
                Assert.AreEqual( "Bob", notrackedUser.Name );
                Assert.IsTrue( connection.Update( notrackedUser ) ); // returns true, even though user was not changed
                notrackedUser.Name = "Cecil";
                Assert.IsTrue( connection.Update( notrackedUser ) );
                Assert.AreEqual( "Cecil", connection.Get<Users>( id ).Name );
                Assert.AreEqual( 1, connection.Query<Users>( "select * from Users" ).Count() );
                Assert.IsTrue( connection.Delete( user ) );
                Assert.IsFalse( connection.Query<Users>( "select * from Users" ).Any() );
                Assert.IsFalse( connection.Update( notrackedUser ) ); // returns false, user not found
            }
        }

        /// <summary> (Unit Test Method) inserts a get update asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task InsertGetUpdateAsync()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                Assert.IsNull( await connection.GetAsync<Users>( 30 ).ConfigureAwait( false ) );
                int originalCount = (await connection.QueryAsync<int>( "select Count(*) from Users" ).ConfigureAwait( false )).First();
                // DH: consider this change
                originalCount = await connection.ExecuteScalarAsync<int>( "select Count(*) from Users" ).ConfigureAwait( false );
                int id = await connection.InsertAsync( new Users() { Name = "Adam", Age = 10 } ).ConfigureAwait( false );

                // get a user with "isdirty" tracking
                var user = await connection.GetAsync<IUsers>( id ).ConfigureAwait( false );
                Assert.AreEqual( "Adam", user.Name );
                Assert.IsFalse( await connection.UpdateAsync( user ).ConfigureAwait( false ) ); // returns false if not updated, based on tracking
                user.Name = "Bob";
                Assert.IsTrue( await connection.UpdateAsync( user ).ConfigureAwait( false ) ); // returns true if updated, based on tracking
                user = await connection.GetAsync<IUsers>( id ).ConfigureAwait( false );
                Assert.AreEqual( "Bob", user.Name );

                // get a user with no tracking
                var notrackedUser = await connection.GetAsync<Users>( id ).ConfigureAwait( false );
                Assert.AreEqual( "Bob", notrackedUser.Name );
                Assert.IsTrue( await connection.UpdateAsync( notrackedUser ).ConfigureAwait( false ) );
                // returns true, even though user was not changed
                notrackedUser.Name = "Cecil";
                Assert.IsTrue( await connection.UpdateAsync( notrackedUser ).ConfigureAwait( false ) );
                Assert.AreEqual( "Cecil", (await connection.GetAsync<Users>( id ).ConfigureAwait( false )).Name );
                Assert.AreEqual( (await connection.QueryAsync<Users>( "select * from Users" ).ConfigureAwait( false )).Count(), originalCount + 1 );
                Assert.IsTrue( await connection.DeleteAsync( user ).ConfigureAwait( false ) );
                Assert.AreEqual( (await connection.QueryAsync<Users>( "select * from Users" ).ConfigureAwait( false )).Count(), originalCount );
                Assert.IsFalse( await connection.UpdateAsync( notrackedUser ).ConfigureAwait( false ) ); // returns false, user not found
                Assert.IsTrue( await connection.InsertAsync( new Users() { Name = "Adam", Age = 10 } ).ConfigureAwait( false ) > originalCount + 1 );
            }
        }

        /// <summary> (Unit Test Method) issue 418. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void Issue418()
        {
            using ( var connection = this.GetConnection() )
            {
                // update first (will fail) then insert
                // added for bug #418
                var updateObject = new ObjectX() { ObjectXId = Guid.NewGuid().ToString(), Name = "Someone" };
                bool updates = connection.Update( updateObject );
                Assert.IsFalse( updates );
                connection.DeleteAll<ObjectX>();
                string objectXId = Guid.NewGuid().ToString();
                var insertObject = new ObjectX() { ObjectXId = objectXId, Name = "Someone else" };
                connection.Insert( insertObject );
                var list = connection.GetAll<ObjectX>();
                Assert.AreEqual( 1, list.Count() );
            }
        }

        /// <summary>
        /// (Unit Test Method) inserts a get update delete with explicit key. issue #351.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>" )]
        public void InsertGetUpdateDeleteWithExplicitKey()
        {
            // Tests for issue #351
            using ( var connection = this.GetConnection() )
            {
                string guid = Guid.NewGuid().ToString();
                var o1 = new ObjectX() { ObjectXId = guid, Name = "Foo" };
                int originalxCount = connection.Query<int>( "Select Count(*) From ObjectX" ).First();

                // DH: consider this change:
                originalxCount = connection.ExecuteScalar<int>( "Select Count(*) From ObjectX" );
                connection.Insert( o1 );
                IEnumerable<ObjectX> list1 = connection.Query<ObjectX>( "select * from ObjectX" ).ToList();
                Assert.AreEqual( list1.Count(), originalxCount + 1 );
                o1 = connection.Get<ObjectX>( guid );
                Assert.AreEqual( o1.ObjectXId, guid );
                o1.Name = "Bar";
                connection.Update( o1 );
                o1 = connection.Get<ObjectX>( guid );
                Assert.AreEqual( "Bar", o1.Name );
                connection.Delete( o1 );
                o1 = connection.Get<ObjectX>( guid );
                Assert.IsNull( o1 );
                const int id = 42;
                var o2 = new ObjectY() { ObjectYId = id, Name = "Foo" };
                int originalyCount = connection.Query<int>( "Select Count(*) From ObjectY" ).First();

                // DH: consider this change:
                originalyCount = connection.ExecuteScalar<int>( "Select Count(*) From ObjectY" );
                connection.Insert( o2 );
                IEnumerable<ObjectY> list2 = connection.Query<ObjectY>( "select * from ObjectY" ).ToList();
                Assert.AreEqual( list2.Count(), originalyCount + 1 );
                o2 = connection.Get<ObjectY>( id );
                Assert.AreEqual( o2.ObjectYId, id );
                o2.Name = "Bar";
                connection.Update( o2 );
                o2 = connection.Get<ObjectY>( id );
                Assert.AreEqual( "Bar", o2.Name );
                connection.Delete( o2 );
                o2 = connection.Get<ObjectY>( id );
                Assert.IsNull( o2 );
            }
        }

        /// <summary>
        /// (Unit Test Method) inserts a get update delete with explicit key asynchronous. issue #351.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task InsertGetUpdateDeleteWithExplicitKeyAsync()
        {
            // Tests for issue #351
            using ( var connection = this.GetOpenConnection() )
            {
                string guid = Guid.NewGuid().ToString();
                var o1 = new ObjectX() { ObjectXId = guid, Name = "Foo" };
                int originalxCount = await connection.ExecuteScalarAsync<int>( "Select Count(*) From ObjectX" ).ConfigureAwait( false );
                await connection.InsertAsync( o1 ).ConfigureAwait( false );
                var list1 = (await connection.QueryAsync<ObjectX>( "select * from ObjectX" ).ConfigureAwait( false )).ToList();
                Assert.AreEqual( list1.Count, originalxCount + 1 );
                o1 = await connection.GetAsync<ObjectX>( guid ).ConfigureAwait( false );
                Assert.AreEqual( o1.ObjectXId, guid );
                o1.Name = "Bar";
                await connection.UpdateAsync( o1 ).ConfigureAwait( false );
                o1 = await connection.GetAsync<ObjectX>( guid ).ConfigureAwait( false );
                Assert.AreEqual( "Bar", o1.Name );
                await connection.DeleteAsync( o1 ).ConfigureAwait( false );
                o1 = await connection.GetAsync<ObjectX>( guid ).ConfigureAwait( false );
                Assert.IsNull( o1 );
                const int id = 42;
                var o2 = new ObjectY() { ObjectYId = id, Name = "Foo" };
                int unused = connection.Query<int>( "Select Count(*) From ObjectY" ).First();
                int originalyCount = connection.ExecuteScalar<int>( "Select Count(*) From ObjectY" );
                await connection.InsertAsync( o2 ).ConfigureAwait( false );
                var list2 = (await connection.QueryAsync<ObjectY>( "select * from ObjectY" ).ConfigureAwait( false )).ToList();
                Assert.AreEqual( list2.Count, originalyCount + 1 );
                o2 = await connection.GetAsync<ObjectY>( id ).ConfigureAwait( false );
                Assert.AreEqual( o2.ObjectYId, id );
                o2.Name = "Bar";
                await connection.UpdateAsync( o2 ).ConfigureAwait( false );
                o2 = await connection.GetAsync<ObjectY>( id ).ConfigureAwait( false );
                Assert.AreEqual( "Bar", o2.Name );
                await connection.DeleteAsync( o2 ).ConfigureAwait( false );
                o2 = await connection.GetAsync<ObjectY>( id ).ConfigureAwait( false );
                Assert.IsNull( o2 );
            }
        }

        /// <summary>
        /// (Unit Test Method) inserts a get update delete with explicit key named identifier.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void InsertGetUpdateDeleteWithExplicitKeyNamedId()
        {
            using ( var connection = this.GetConnection() )
            {
                const int id = 42;
                var o2 = new ObjectZ() { Id = id, Name = "Foo" };
                connection.Insert( o2 );
                IEnumerable<ObjectZ> list2 = connection.Query<ObjectZ>( "select * from ObjectZ" ).ToList();
                Assert.AreEqual( 1, list2.Count() );
                o2 = connection.Get<ObjectZ>( id );
                Assert.AreEqual( o2.Id, id );
            }
        }

        /// <summary> (Unit Test Method) updates the enumerable. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void UpdateEnumerable()
        {
            this.UpdateHelper( src => src.AsEnumerable() );
        }

        /// <summary> (Unit Test Method) updates the array. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void UpdateArray()
        {
            this.UpdateHelper( src => src.ToArray() );
        }

        /// <summary> (Unit Test Method) updates the list. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void UpdateList()
        {
            this.UpdateHelper( src => src.ToList() );
        }

        /// <summary> Helper method that update. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="helper"> The helper. </param>
        private void UpdateHelper<T>( Func<IEnumerable<Users>, T> helper ) where T : class
        {
            const int numberOfEntities = 10;
            var users = new List<Users>();
            for ( int i = 0; i <= numberOfEntities - 1; i++ )
                users.Add( new Users() { Name = "User " + i, Age = i } );
            using ( var connection = this.GetConnection() )
            {
                connection.DeleteAll<Users>();
                long total = connection.Insert( helper( users ) );
                Assert.AreEqual( ( int ) total, numberOfEntities );
                users = connection.Query<Users>( "select * from Users" ).ToList();
                Assert.AreEqual( users.Count, numberOfEntities );
                foreach ( Users user in users )
                    user.Name += " updated";
                connection.Update( helper( users ) );
                string name = connection.Query<Users>( "select * from Users" ).First().Name;
                Assert.IsTrue( name.Contains( "updated" ) );
            }
        }

        /// <summary> (Unit Test Method) updates the enumerable asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task UpdateEnumerableAsync()
        {
            await this.UpdateHelperAsync( src => src.AsEnumerable() ).ConfigureAwait( false );
        }

        /// <summary> (Unit Test Method) updates the array asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task UpdateArrayAsync()
        {
            await this.UpdateHelperAsync( src => src.ToArray() ).ConfigureAwait( false );
        }

        /// <summary> (Unit Test Method) updates the list asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task UpdateListAsync()
        {
            await this.UpdateHelperAsync( src => src.ToList() ).ConfigureAwait( false );
        }

        /// <summary> Updates the helper asynchronous described by helper. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="helper"> The helper. </param>
        /// <returns> A Task. </returns>
        private async Task UpdateHelperAsync<T>( Func<IEnumerable<Users>, T> helper ) where T : class
        {
            const int numberOfEntities = 10;
            var users = new List<Users>();
            for ( int i = 0; i <= numberOfEntities - 1; i++ )
                users.Add( new Users() { Name = "User " + i, Age = i } );
            using ( var connection = this.GetOpenConnection() )
            {
                await connection.DeleteAllAsync<Users>().ConfigureAwait( false );
                long total = await connection.InsertAsync( helper( users ) ).ConfigureAwait( false );
                Assert.AreEqual( ( int ) total, numberOfEntities );
                users = connection.Query<Users>( "select * from Users" ).ToList();
                Assert.AreEqual( users.Count, numberOfEntities );
                foreach ( Users User in users )
                    User.Name += " updated";
                await connection.UpdateAsync( helper( users ) ).ConfigureAwait( false );
                string name = connection.Query<Users>( "select * from Users" ).First().Name;
                Assert.IsTrue( name.Contains( "updated" ) );
            }
        }

        #endregion

        #region " TRANSACTIONS "

        /// <summary> (Unit Test Method) transactions this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void Transactions()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                long id = connection.Insert( new Car() { Name = "one car" } ); // insert outside transaction
                var tran = connection.BeginTransaction();
                var car = connection.Get<Car>( id, tran );
                string orgName = car.Name;
                car.Name = "Another car";
                connection.Update( car, tran );
                tran.Rollback();
                car = connection.Get<Car>( id ); // updates should have been rolled back
                Assert.AreEqual( car.Name, orgName );
            }
        }

        /// <summary> (Unit Test Method) transaction scope. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void TransactionScope()
        {
            using ( var txscope = new TransactionScope() )
            {
                using ( var connection = this.GetConnection() )
                {
                    long id = connection.Insert( new Car() { Name = "one car" } ); // insert car within transaction
                    txscope.Dispose(); // rollback
                    Assert.IsNull( connection.Get<Car>( id ) ); // returns null - car with that id should not exist
                }
            }
        }

        #endregion

        #region " TYPE WITH GENERIC PARAMETER "

        /// <summary> (Unit Test Method) type with generic parameter can be inserted. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void TypeWithGenericParameterCanBeInserted()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                connection.DeleteAll<GenericType<string>>();
                var objectToInsert = new GenericType<string>() { Id = Guid.NewGuid().ToString(), Name = "something" };
                connection.Insert( objectToInsert );
                Assert.AreEqual( 1, connection.GetAll<GenericType<string>>().Count() );
                var objectsToInsert = new List<GenericType<string>>() { new GenericType<string>() { Id = Guid.NewGuid().ToString(), Name = "1" }, new GenericType<string>() { Id = Guid.NewGuid().ToString(), Name = "2" } };
                connection.Insert( objectsToInsert );
                var list = connection.GetAll<GenericType<string>>();
                Assert.AreEqual( 3, list.Count() );
            }
        }

        /// <summary> (Unit Test Method) type with generic parameter can be updated. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void TypeWithGenericParameterCanBeUpdated()
        {
            using ( var connection = this.GetConnection() )
            {
                var objectToInsert = new GenericType<string>() { Id = Guid.NewGuid().ToString(), Name = "something" };
                connection.Insert( objectToInsert );
                objectToInsert.Name = "somethingelse";
                connection.Update( objectToInsert );
                var updatedObject = connection.Get<GenericType<string>>( objectToInsert.Id );
                Assert.AreEqual( objectToInsert.Name, updatedObject.Name );
            }
        }

        /// <summary> (Unit Test Method) type with generic parameter can be deleted. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void TypeWithGenericParameterCanBeDeleted()
        {
            using ( var connection = this.GetConnection() )
            {
                var objectToInsert = new GenericType<string>() { Id = Guid.NewGuid().ToString(), Name = "something" };
                connection.Insert( objectToInsert );
                bool deleted = connection.Delete( objectToInsert );
                Assert.IsTrue( deleted );
            }
        }

        /// <summary>
        /// (Unit Test Method) type with generic parameter can be inserted asynchronous.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task TypeWithGenericParameterCanBeInsertedAsync()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                await connection.DeleteAllAsync<GenericType<string>>();
                var objectToInsert = new GenericType<string>() { Id = Guid.NewGuid().ToString(), Name = "something" };
                await connection.InsertAsync( objectToInsert );
                Assert.AreEqual( 1, connection.GetAll<GenericType<string>>().Count() );
                var objectsToInsert = new List<GenericType<string>>() { new GenericType<string>() { Id = Guid.NewGuid().ToString(), Name = "1" }, new GenericType<string>() { Id = Guid.NewGuid().ToString(), Name = "2" } };
                await connection.InsertAsync( objectsToInsert );
                var list = connection.GetAll<GenericType<string>>();
                Assert.AreEqual( 3, list.Count() );
            }
        }

        /// <summary>
        /// (Unit Test Method) type with generic parameter can be updated asynchronous.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task TypeWithGenericParameterCanBeUpdatedAsync()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                var objectToInsert = new GenericType<string>() { Id = Guid.NewGuid().ToString(), Name = "something" };
                await connection.InsertAsync( objectToInsert );
                objectToInsert.Name = "somethingelse";
                await connection.UpdateAsync( objectToInsert );
                var updatedObject = connection.Get<GenericType<string>>( objectToInsert.Id );
                Assert.AreEqual( objectToInsert.Name, updatedObject.Name );
            }
        }

        /// <summary>
        /// (Unit Test Method) type with generic parameter can be deleted asynchronous.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task TypeWithGenericParameterCanBeDeletedAsync()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                var objectToInsert = new GenericType<string>() { Id = Guid.NewGuid().ToString(), Name = "something" };
                await connection.InsertAsync( objectToInsert );
                bool deleted = await connection.DeleteAsync( objectToInsert );
                Assert.IsTrue( deleted );
            }
        }

        #endregion

        #region " BUILDERS "

        /// <summary> (Unit Test Method) builder select clause. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        [TestMethod]
        public void BuilderSelectClause()
        {
            using ( var connection = this.GetConnection() )
            {
                var rand = new Random( 8675309 );
                var data = new List<Users>();
                for ( int i = 0; i <= 99; i++ )
                {
                    var nU = new Users() { Age = rand.Next( 70 ), Id = i, Name = Guid.NewGuid().ToString() };
                    data.Add( nU );
                    nU.Id = ( int ) Conversion.Fix( connection.Insert( nU ) );
                }

                var builder = new SqlBuilder();
                var justId = builder.AddTemplate( "SELECT /**select**/ from Users" );
                var all = builder.AddTemplate( "SELECT Name, /**select**/, Age from Users" );
                builder.Select( "Id" );
                var ids = connection.Query<int>( justId.RawSql, justId.Parameters );
                var users = connection.Query<Users>( all.RawSql, all.Parameters );
                foreach ( Users u in data )
                {
                    if ( !ids.Any( i => u.Id == i ) )
                    {
                        throw new InvalidOperationException( "Missing ids in select" );
                    }

                    if ( !users.Any( a => a.Id == u.Id && (a.Name ?? "") == (u.Name ?? "") && a.Age == u.Age ) )
                    {
                        throw new InvalidOperationException( "Missing users in select" );
                    }
                }
            }
        }

        /// <summary> (Unit Test Method) builder template without composition. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        [TestMethod]
        public void BuilderTemplateWithoutComposition()
        {
            var builder = new SqlBuilder();
            var template = builder.AddTemplate( "SELECT COUNT(*) from Users WHERE Age = @age", new { age = 5 } );
            // Dim template As SqlBuilder.Template = builder.AddTemplate("SELECT COUNT(*) from Users WHERE (Age = @age and Name = @name)", New With {Key .age = 5, .name = "a"})

            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( "RawSql null" );
            }

            if ( template.Parameters is null )
            {
                throw new InvalidOperationException( "Parameters null" );
            }

            using ( var connection = this.GetConnection() )
            {
                connection.DeleteAll<Users>();
                connection.Insert( new Users() { Age = 5, Name = "Testy McTestington" } );
                if ( connection.Query<int>( template.RawSql, template.Parameters ).Single() != 1 )
                {
                    throw new InvalidOperationException( "Query failed" );
                }

                if ( connection.ExecuteScalar<int>( template.RawSql, template.Parameters ) != 1 )
                {
                    throw new InvalidOperationException( "Query failed" );
                }
            }
        }

        /// <summary> (Unit Test Method) builder select clause asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task BuilderSelectClauseAsync()
        {
            using ( var connection = this.GetOpenConnection() )
            {
                await connection.DeleteAllAsync<Users>().ConfigureAwait( false );
                var rand = new Random( 8675309 );
                var data = new List<Users>();
                for ( int i = 0; i <= 99; i++ )
                {
                    var nU = new Users() { Age = rand.Next( 70 ), Id = i, Name = Guid.NewGuid().ToString() };
                    data.Add( nU );
                    nU.Id = await connection.InsertAsync( nU ).ConfigureAwait( false );
                }

                var builder = new SqlBuilder();
                var justId = builder.AddTemplate( "SELECT /**select**/ from Users" );
                var all = builder.AddTemplate( "SELECT Name, /**select**/, Age from Users" );
                builder.Select( "Id" );
                var ids = await connection.QueryAsync<int>( justId.RawSql, justId.Parameters ).ConfigureAwait( false );
                var users = await connection.QueryAsync<Users>( all.RawSql, all.Parameters ).ConfigureAwait( false );
                foreach ( Users u in data )
                {
                    if ( !ids.Any( i => u.Id == i ) )
                    {
                        throw new Exception( "Missing ids in select" );
                    }

                    if ( !users.Any( a => a.Id == u.Id && (a.Name ?? "") == (u.Name ?? "") && a.Age == u.Age ) )
                    {
                        throw new Exception( "Missing users in select" );
                    }
                }
            }
        }

        /// <summary> (Unit Test Method) builder template without composition asynchronous. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        /// <returns> A Task. </returns>
        [TestMethod]
        public async Task BuilderTemplateWithoutCompositionAsync()
        {
            var builder = new SqlBuilder();
            var template = builder.AddTemplate( "SELECT COUNT(*) from Users WHERE Age = @age", new { age = 5 } );
            if ( template.RawSql is null )
            {
                throw new Exception( "RawSql null" );
            }

            if ( template.Parameters is null )
            {
                throw new Exception( "Parameters null" );
            }

            using ( var connection = this.GetOpenConnection() )
            {
                await connection.DeleteAllAsync<Users>().ConfigureAwait( false );
                await connection.InsertAsync( new Users() { Age = 5, Name = "Testy McTestington" } ).ConfigureAwait( false );
                if ( (await connection.QueryAsync<int>( template.RawSql, template.Parameters ).ConfigureAwait( false )).Single() != 1 )
                {
                    throw new Exception( "Query failed" );
                }

                if ( await connection.ExecuteScalarAsync<int>( template.RawSql, template.Parameters ).ConfigureAwait( false ) != 1 )
                {
                    throw new Exception( "Query failed" );
                }
            }
        }

        #endregion

    }
}
