using System;

using Dapper.Contrib.Extensions;

namespace isr.Dapper.Tests.ContribTests
{

    /// <summary> Interface for object zz. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public interface IObjectZZ
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        [ExplicitKey]
        string Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        string Name { get; set; }

        /// <summary> Gets or sets the is active. </summary>
        /// <value> The is active. </value>
        bool IsActive { get; set; }

        /// <summary> Gets or sets the timestamp. </summary>
        /// <value> The timestamp. </value>
        DateTime Timestamp { get; set; }
    }

    /// <summary> An object zz. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    [Table( "ObjectZZ" )]
    public class ObjectZZ : IObjectZZ
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        [ExplicitKey]
        public string Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the is active. </summary>
        /// <value> The is active. </value>
        public bool IsActive { get; set; }

        /// <summary> Gets or sets the timestamp. </summary>
        /// <value> The timestamp. </value>
        public DateTime Timestamp { get; set; }
    }

    /// <summary> Interface for object x coordinate. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public interface IObjectX
    {

        /// <summary> Gets or sets the identifier of the object x coordinate. </summary>
        /// <value> The identifier of the object x coordinate. </value>
        [ExplicitKey]
        string ObjectXId { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        string Name { get; set; }
    }

    /// <summary> An object x coordinate. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    [Table( "ObjectX" )]
    public class ObjectX : IObjectX
    {

        /// <summary> Gets or sets the identifier of the object x coordinate. </summary>
        /// <value> The identifier of the object x coordinate. </value>
        [ExplicitKey]
        public string ObjectXId { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }
    }

    /// <summary> An object y coordinate. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    [Table( "ObjectY" )]
    public class ObjectY
    {

        /// <summary> Gets or sets the identifier of the object y coordinate. </summary>
        /// <value> The identifier of the object y coordinate. </value>
        [ExplicitKey]
        public int ObjectYId { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }
    }

    /// <summary> An object z coordinate. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    [Table( "ObjectZ" )]
    public class ObjectZ
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        [ExplicitKey]
        public int Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }
    }

    /// <summary> Interface for users. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public interface IUsers
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        [Key]
        int Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        string Name { get; set; }

        /// <summary> Gets or sets the age. </summary>
        /// <value> The age. </value>
        int Age { get; set; }
    }

    /// <summary> An users. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class Users : IUsers
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the age. </summary>
        /// <value> The age. </value>
        public int Age { get; set; }
    }

    /// <summary> Interface for nullable date. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public interface INullableDate
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        [Key]
        int Id { get; set; }

        /// <summary> Gets or sets the date value. </summary>
        /// <value> The date value. </value>
        DateTime? DateValue { get; set; }
    }

    /// <summary> A nullable date. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class NullableDate : INullableDate
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary> Gets or sets the date value. </summary>
        /// <value> The date value. </value>
        public DateTime? DateValue { get; set; }
    }

    /// <summary> A person. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public class Person
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }
    }

    /// <summary> A stuff. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    [Table( "Stuff" )]
    public class Stuff
    {

        /// <summary> Gets or sets the identifier of the. </summary>
        /// <value> The identifier of the. </value>
        [Key]
        public short TheId { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the created. </summary>
        /// <value> The created. </value>
        public DateTime? Created { get; set; }
    }

    /// <summary> A car. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    [Table( "Automobile" )]
    public class Car
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the computed. </summary>
        /// <value> The computed. </value>
        [Computed]
        public string Computed { get; set; }
    }

    /// <summary> A result. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    [Table( "Result" )]
    public class Result
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the order. </summary>
        /// <value> The order. </value>
        public int Order { get; set; }
    }

    /// <summary> A t. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    [Table( "GenericType" )]
    public class GenericType<T>
    {

        /// <summary> Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        [ExplicitKey]
        public string Id { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }
    }
}
