Imports System.ComponentModel
Imports System.Runtime.CompilerServices
Imports System.Runtime.Remoting.Messaging
Imports System.Windows.Forms

Imports isr.Dapper.Entities

Namespace ListControlExtensions

    Public Module Methods

        #Region " LIST CONTROL "

        ''' <summary> Selected identity. </summary>
        ''' <remarks> David, 6/22/2020. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <returns> An Integer. </returns>
        <Extension>
        Public Function SelectedIdentity(ByVal control As System.Windows.Forms.ListControl) As Integer
            Return CInt(control.SelectedValue)
        End Function

        ''' <summary> Selected type. </summary>
        ''' <remarks> David, 6/22/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control"> The control. </param>
        ''' <returns> A T. </returns>
        <Extension>
        Public Function SelectedType(Of T As Class)(ByVal control As System.Windows.Forms.ComboBox) As T
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            Return TryCast(control.SelectedItem, T)
        End Function

        ''' <summary> Select value. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">   The value. </param>
        <Extension>
        Public Sub SelectValue(ByVal control As System.Windows.Forms.ListControl, ByVal value As Integer)
            If control IsNot Nothing Then
                control.SelectedValue = value
            End If
        End Sub

        #End Region

        #Region " IKeyLabelTime "

        ''' <summary> List <see cref="IKeyLabelTime"/> entities. </summary>
        ''' <remarks> David, 6/22/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="entities"> The entities. </param>
        ''' <param name="control">  The control. </param>
        ''' <returns> The number of entities. </returns>
        <Extension>
        Public Function ListEntities(ByVal entities As IEnumerable(Of IKeyLabelTime), ByVal control As ListControl) As Integer
            If entities Is Nothing Then Throw New ArgumentNullException(NameOf(entities))
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            Dim wasEnabled As Boolean = control.Enabled
            Dim label As String = control.Text
            control.Enabled = False
            control.DataSource = Nothing
            control.ValueMember = NameOf(IKeyLabelTime.AutoId)
            control.DisplayMember = NameOf(IKeyLabelTime.Label)
            ' converting to array ensures that values displayed in two controls are not connected.
            control.DataSource = entities
            If Not String.IsNullOrEmpty(label) Then
                Dim entity As IKeyLabelTime = entities.SelectEntity(label)
                If entity IsNot Nothing Then control.SelectedValue = entity.AutoId
            End If
            ' doing this the control may display text even if none selected! control.SelectedIndex = -1
            control.Enabled = wasEnabled
            control.Invalidate()
            Return entities.Count
        End Function

        ''' <summary> Select entity. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="entities"> The entities. </param>
        ''' <param name="label">    The label. </param>
        ''' <returns> An IKeyLabelTime. </returns>
        <Extension>
        Public Function SelectEntity(ByVal entities As IEnumerable(Of IKeyLabelTime), ByVal label As String) As IKeyLabelTime
            Dim entity As IKeyLabelTime = entities.Where(Function(x)
                                                             Return String.Equals(x.Label, label, StringComparison.OrdinalIgnoreCase)
                                                         End Function).FirstOrDefault()
            Return entity
        End Function

        #End Region


    End Module

End Namespace


