Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets Identifies the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Dapper.Entity.My.ProjectTraceEventId.DapperEntities

        Public Const AssemblyTitle As String = "Dapper Entities Library"
        Public Const AssemblyDescription As String = "Dapper Entities Library"
        Public Const AssemblyProduct As String = "Dapper.Entities"

        ''' <summary> The test assembly strong name. </summary>
        Public Const TestAssemblyStrongName As String = "isr.Dapper.Entities.Tests,PublicKey=" & SolutionInfo.PublicKey

    End Class

End Namespace
