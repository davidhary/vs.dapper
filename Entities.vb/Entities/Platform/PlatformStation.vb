Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Platform-Station builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class PlatformStationBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PlatformStationBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PlatformStationBuilder._TableName) Then
                PlatformStationBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PlatformStationNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = PlatformBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(PlatformNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = StationBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(StationNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(PlatformStationEntity.PlatformAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(PlatformStationEntity.StationAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PlatformStationBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PlatformStationBuilder
        If PlatformStationBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PlatformStationBuilder._Instance = New PlatformStationBuilder()
            End SyncLock
        End If
        Return PlatformStationBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Platform Station Nub based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("PlatformStation")>
Public Class PlatformStationNub
    Inherits OneToManyNub
    Implements IOneToMany

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToMany
        Return New PlatformStationNub
    End Function

End Class

''' <summary>
''' The Platform-Station Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class PlatformStationEntity
    Inherits EntityBase(Of IOneToMany, PlatformStationNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New PlatformStationNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Platform-Station interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New PlatformStationNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PlatformStationBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToMany
        Return New PlatformStationNub
    End Function

    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        PlatformStationNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        PlatformStationNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Platform-Station interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        PlatformStationNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="platformAutoId"> Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <param name="stationAutoId">      Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer, ByVal stationAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As PlatformStationNub = PlatformStationEntity.FetchNubs(connection, platformAutoId, stationAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PlatformAutoId, Me.StationAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PlatformAutoId, Me.StationAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="platformAutoId"> Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <param name="stationAutoId">      Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer, ByVal stationAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, platformAutoId, stationAutoId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PlatformStationEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="platformAutoId">     Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer, ByVal stationAutoId As Integer) As Boolean
        Return connection.Delete(Of PlatformStationNub)(New PlatformStationNub With {.PrimaryId = platformAutoId, .SecondaryId = stationAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Platform-Station entities. </summary>
    ''' <value> The Platform-Station entities. </value>
    Public ReadOnly Property PlatformStations As IEnumerable(Of PlatformStationEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PlatformStationEntity)
        Return If(usingNativeTracking, PlatformStationEntity.Populate(connection.GetAll(Of IOneToMany)), PlatformStationEntity.Populate(connection.GetAll(Of PlatformStationNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._PlatformStations = PlatformStationEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(PlatformStationEntity.PlatformStations))
        Return If(Me.PlatformStations?.Any, Me.PlatformStations.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PlatformStationNub)) As IEnumerable(Of PlatformStationEntity)
        Dim l As New List(Of PlatformStationEntity)
        If nubs?.Any Then
            For Each nub As PlatformStationNub In nubs
                l.Add(New PlatformStationEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of PlatformStationEntity)
        Dim l As New List(Of PlatformStationEntity)
        If interfaces?.Any Then
            Dim nub As New PlatformStationNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PlatformStationEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to Station entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="platformAutoId"> Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PlatformStationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PlatformStationNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = platformAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="platformAutoId">  Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer) As IEnumerable(Of PlatformStationEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PlatformStationBuilder.TableName}] WHERE {NameOf(PlatformStationNub.PrimaryId)} = @Id", New With {Key .Id = platformAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return PlatformStationEntity.Populate(connection.Query(Of PlatformStationNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Station. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> The total number of entities by Station. </returns>
    Public Shared Function CountEntitiesByStation(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PlatformStationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PlatformStationNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Stations in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Stations in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByStation(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As IEnumerable(Of PlatformStationEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PlatformStationBuilder.TableName}] WHERE {NameOf(PlatformStationNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return PlatformStationEntity.Populate(connection.Query(Of PlatformStationNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="platformAutoId"> Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <param name="stationAutoId">       Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PlatformStationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PlatformStationNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = platformAutoId})
        sqlBuilder.Where($"{NameOf(PlatformStationNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="platformAutoId"> Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <param name="stationAutoId">       Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer, ByVal stationAutoId As Integer) As IEnumerable(Of PlatformStationNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PlatformStationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PlatformStationNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = platformAutoId})
        sqlBuilder.Where($"{NameOf(PlatformStationNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PlatformStationNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Platform Station exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="platformAutoId"> Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <param name="stationAutoId">       Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer, ByVal stationAutoId As Integer) As Boolean
        Return 1 = PlatformStationEntity.CountEntities(connection, platformAutoId, stationAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Platform entity. </summary>
    ''' <value> The Platform entity. </value>
    Public ReadOnly Property PlatformEntity As PlatformEntity

    ''' <summary> Fetches Platform Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Platform Entity. </returns>
    Public Function FetchPlatformEntity(ByVal connection As System.Data.IDbConnection) As PlatformEntity
        Dim entity As New PlatformEntity()
        entity.FetchUsingKey(connection, Me.PlatformAutoId)
        Me._PlatformEntity = entity
        Return entity
    End Function

    ''' <summary> Count Platforms associated with the specified <paramref name="stationAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> The total number of Platforms. </returns>
    Public Shared Function CountPlatforms(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{PlatformStationBuilder.TableName}] WHERE {NameOf(PlatformStationNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Platforms associated with the specified <paramref name="stationAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Platforms in this collection.
    ''' </returns>
    Public Shared Function FetchPlatforms(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As IEnumerable(Of StationEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PlatformStationBuilder.TableName}] WHERE {NameOf(PlatformStationNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of StationEntity)
        For Each nub As PlatformStationNub In connection.Query(Of PlatformStationNub)(selector.RawSql, selector.Parameters)
            Dim entity As New PlatformStationEntity(nub)
            l.Add(entity.FetchStationEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Platforms associated with the specified <paramref name="stationAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeletePlatforms(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{PlatformStationBuilder.TableName}] WHERE {NameOf(PlatformStationNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = stationAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the Station entity. </summary>
    ''' <value> The Station entity. </value>
    Public ReadOnly Property StationEntity As StationEntity

    ''' <summary> Fetches a Station Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Station Entity. </returns>
    Public Function FetchStationEntity(ByVal connection As System.Data.IDbConnection) As StationEntity
        Dim entity As New StationEntity()
        entity.FetchUsingKey(connection, Me.StationAutoId)
        Me._StationEntity = entity
        Return entity
    End Function

    Public Shared Function CountStations(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{PlatformStationBuilder.TableName}] WHERE {NameOf(PlatformStationNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = platformAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Stations in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="platformAutoId"> Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Stations in this collection.
    ''' </returns>
    Public Shared Function FetchStations(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer) As IEnumerable(Of StationEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PlatformStationBuilder.TableName}] WHERE {NameOf(PlatformStationNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = platformAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of StationEntity)
        For Each nub As PlatformStationNub In connection.Query(Of PlatformStationNub)(selector.RawSql, selector.Parameters)
            Dim entity As New PlatformStationEntity(nub)
            l.Add(entity.FetchStationEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Station related to the specified Platform. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="platformAutoId">  Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteStations(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{PlatformStationBuilder.TableName}] WHERE {NameOf(PlatformStationNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = platformAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches a <see cref="Entities.StationEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="selectQuery">    The select query. </param>
    ''' <param name="platformAutoId"> Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <param name="stationLabel">   The Station label. </param>
    ''' <returns> The Station. </returns>
    Public Shared Function FetchStation(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal platformAutoId As Integer, ByVal stationLabel As String) As StationEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.Id = platformAutoId, .Label = stationLabel})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As StationNub = connection.Query(Of StationNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New StationEntity, New StationEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches a <see cref="Entities.StationEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="platformAutoId"> Identifies the <see cref="PlatformEntity"/>. </param>
    ''' <param name="stationLabel">   The Station label. </param>
    ''' <returns> The Station. </returns>
    Public Shared Function FetchStation(ByVal connection As System.Data.IDbConnection, ByVal platformAutoId As Integer, ByVal stationLabel As String) As StationEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [PlatformStation] on [PlatformStation].SecondaryId = [Station].AutoId where [PlatformStation].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{StationBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{StationBuilder.TableName}] Inner Join [{PlatformStationBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{PlatformStationBuilder.TableName}].{NameOf(PlatformStationNub.SecondaryId)} = [{StationBuilder.TableName}].{NameOf(StationNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{PlatformStationBuilder.TableName}].{NameOf(PlatformStationNub.PrimaryId)} = @Id AND [{StationBuilder.TableName}].{NameOf(StationNub.Label)} = @Label); ")
        Return PlatformStationEntity.FetchStation(connection, queryBuilder.ToString, platformAutoId, stationLabel)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PlatformStationEntity.PlatformAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Platform. </summary>
    ''' <value> Identifies the Platform. </value>
    Public Property PlatformAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PlatformStationEntity.StationAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Station. </summary>
    ''' <value> Identifies the Station. </value>
    Public Property StationAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
