Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A builder for a nominal Revision table identified by a revision number based on the <see cref="IKeyLabelTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class RevisionBuilder
    Inherits KeyLabelTimeBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return RevisionBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(RevisionBuilder._TableName) Then
                RevisionBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(RevisionNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Inserts or update a revision. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="version">    The version. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function UpsertRevision(ByVal connection As System.Data.IDbConnection, ByVal version As String) As Boolean
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim releaseHistory As RevisionNub = New RevisionNub With {.Label = version}
        Dim entity As New RevisionEntity
        Return entity.Upsert(connection, releaseHistory)
    End Function

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 31

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(RevisionEntity.AutoId)

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <value> The name of the label field. </value>
    Public Overrides Property LabelFieldName() As String = NameOf(RevisionEntity.Revision)

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <value> The name of the timestamp field. </value>
    Public Overrides Property TimestampFieldName() As String = NameOf(RevisionEntity.Timestamp)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As RevisionBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As RevisionBuilder
        If RevisionBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                RevisionBuilder._Instance = New RevisionBuilder()
            End SyncLock
        End If
        Return RevisionBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Revision table based on the <see cref="IKeyLabelTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("Revision")>
Public Class RevisionNub
    Inherits KeyLabelTimeNub
    Implements IKeyLabelTime

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IKeyLabelTime
        Return New RevisionNub
    End Function

End Class


''' <summary> The Revision Entity  based on the <see cref="IKeyLabelTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class RevisionEntity
    Inherits EntityBase(Of IKeyLabelTime, RevisionNub)
    Implements IKeyLabelTime

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New RevisionNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Revision interface. </param>
    Public Sub New(ByVal value As IKeyLabelTime)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyLabelTime, ByVal store As IKeyLabelTime)
        MyBase.New(New RevisionNub, cache, store)
        Me.UsingNativeTracking = String.Equals(RevisionBuilder.TableName, NameOf(IKeyLabelTime).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "


    Public Overrides Function CreateNew() As IKeyLabelTime
        Return New RevisionNub
    End Function

    Public Overrides Function CreateCopy() As IKeyLabelTime
        Dim destination As IKeyLabelTime = Me.CreateNew
        RevisionNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IKeyLabelTime)
        RevisionNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Revision interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyLabelTime)
        ' first make the copy to notify of any property change.
        RevisionNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Revision table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyLabelTime)(key), connection.Get(Of RevisionNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Revision)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal revision As String) As Boolean
        Me.ClearStore()
        Dim nub As RevisionNub = RevisionEntity.FetchNubs(connection, revision).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyLabelTime) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If RevisionEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(RevisionBuilder.Get.UsingUniqueLabel(connection),
                                        Me.FetchUsingUniqueIndex(connection, entity.Label),
                                        Me.FetchUsingKey(connection, entity.AutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of RevisionNub)(New RevisionNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Revision entities. </summary>
    ''' <value> The Revision entities. </value>
    Public ReadOnly Property Revisions As IEnumerable(Of RevisionEntity)

    ''' <summary> Fetches entities ordered by the specified field. </summary>
    ''' <param name="connection"> The connection. </param>
    Public Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal orderBy As String) As Integer
        Me._Revisions = RevisionEntity.FetchAllEntities(connection, orderBy)
        Return If(Me.Revisions?.Any, Me.Revisions.Count, 0)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="orderBy">    Specifies the field name for ordering the entities. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal orderBy As String) As IEnumerable(Of RevisionEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select * from [{RevisionBuilder.TableName}] ORDER BY {orderBy} ASC; ")
        Dim gridReader As SqlMapper.GridReader = connection.QueryMultiple(queryBuilder.ToString)
        Return RevisionEntity.Populate(gridReader.Read(Of RevisionNub))
    End Function


    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of RevisionEntity)
        Return If(usingNativeTracking, RevisionEntity.Populate(connection.GetAll(Of IKeyLabelTime)), RevisionEntity.Populate(connection.GetAll(Of RevisionNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Revisions = RevisionEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(RevisionEntity.Revisions))
        Return If(Me.Revisions?.Any, Me.Revisions.Count, 0)
    End Function

    ''' <summary> Populates a list of Revision entities. </summary>
    ''' <param name="nubs"> The Revision nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of RevisionNub)) As IEnumerable(Of RevisionEntity)
        Dim l As New List(Of RevisionEntity)
        If nubs?.Any Then
            For Each nub As RevisionNub In nubs
                l.Add(New RevisionEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Revision entities. </summary>
    ''' <param name="interfaces"> The Revision interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyLabelTime)) As IEnumerable(Of RevisionEntity)
        Dim l As New List(Of RevisionEntity)
        If interfaces?.Any Then
            Dim nub As New RevisionNub
            For Each iFace As IKeyLabelTime In interfaces
                nub.CopyFrom(iFace)
                l.Add(New RevisionEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="revisionName"> The name of the revision. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal revisionName As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{RevisionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(RevisionNub.Label)} = @RevisionName", New With {revisionName})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="revisionName"> The name of the revision. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal revisionName As String) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{RevisionBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(RevisionNub.Label)} IS NULL OR {NameOf(RevisionNub.Label)} = @revisionName)")
        selectSql.Append("; ")
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {revisionName})
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="revisionName"> The name of the revision. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal revisionName As String) As IEnumerable(Of RevisionNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{RevisionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(RevisionNub.Label)} = @RevisionName", New With {revisionName})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of RevisionNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the revision exists. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="revisionName"> The name of the revision. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal revisionName As String) As Boolean
        Return 1 = RevisionEntity.CountEntities(connection, revisionName)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Revision. </summary>
    ''' <value> Identifies the Revision. </value>
    Public Property AutoId As Integer Implements IKeyLabelTime.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Revision number. </summary>
    ''' <value> The Revision number. </value>
    Public Property Label As String Implements IKeyLabelTime.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(RevisionEntity.Revision))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the revision. </summary>
    ''' <value> The revision. </value>
    Public Property Revision As String
        Get
            Return Me.Label
        End Get
        Set(value As String)
            Me.Label = value
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks> Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to convert to local time. </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements IKeyLabelTime.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " FIELDS: TIMESTAMP "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

#End Region

#Region " ACTIVE RELEASE "

    ''' <summary> Fetches the active entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchActive(ByVal connection As System.Data.IDbConnection) As Boolean
        If 0 < Me.FetchEntities(connection, NameOf(RevisionNub.Timestamp)) Then
            Me.UpdateCache(Me.Revisions.LastOrDefault)
            Return Me.FetchUsingKey(connection)
        Else
            Return False
        End If
    End Function

    ''' <summary> Query if 'expectedVersion' is expected current release. </summary>
    ''' <param name="expectedVersion"> The expected version. </param>
    ''' <returns> <c>true</c> if expected current release; otherwise <c>false</c> </returns>
    Public Function IsExpectedCurrentRelease(ByVal expectedVersion As String) As Boolean
        Return String.Equals(Me.Revision, expectedVersion, StringComparison.OrdinalIgnoreCase)
    End Function

    ''' <summary> Returns True if the database version verifies. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="expectedVersion"> The expected version. </param>
    ''' <returns> <c>True</c> if current release; <c>False</c> otherwise. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryVerifyDatabaseVersion(ByVal connection As System.Data.IDbConnection, ByVal expectedVersion As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim outcome As (Success As Boolean, Details As String) = (True, String.Empty)
        Try
            Dim entity As New RevisionEntity
            If entity.FetchActive(connection) Then
                If Not entity.IsExpectedCurrentRelease(expectedVersion) Then
                    outcome = (False, $"Active database version {entity.Revision} does not match the program data version {expectedVersion}")
                End If
            Else
                outcome = (False, "Release history has no active records")
            End If
        Catch ex As Exception
            outcome = (False, $"Exception fetching active release;. {ex.ToFullBlownString}")
        End Try
        Return outcome
    End Function

    ''' <summary> Fetches active release history version. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The active release history version. </returns>
    Public Shared Function FetchActiveReleaseHistoryVersion(ByVal connection As System.Data.IDbConnection) As Version
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return New Version(RevisionEntity.FetchAllEntities(connection, NameOf(RevisionNub.Timestamp)).Last.Revision)
    End Function

#End Region


End Class

