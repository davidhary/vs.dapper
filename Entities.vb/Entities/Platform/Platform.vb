Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Platform builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class PlatformBuilder
    Inherits KeyLabelTimezoneBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PlatformBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PlatformBuilder._TableName) Then
                PlatformBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PlatformNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 50

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(PlatformEntity.AutoId)

    Public Overrides Property LabelFieldName() As String = NameOf(PlatformEntity.PlatformName)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property TimezoneIdFieldName() As String = NameOf(PlatformEntity.TimezoneId)

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <value> The name of the timestamp field. </value>
    Public Overrides Property TimestampFieldName() As String = NameOf(PlatformEntity.Timestamp)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PlatformBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PlatformBuilder
        If PlatformBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PlatformBuilder._Instance = New PlatformBuilder()
            End SyncLock
        End If
        Return PlatformBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Platform table based on the <see cref="IKeyLabelTimezone">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("Platform")>
Public Class PlatformNub
    Inherits KeyLabelTimezoneNub
    Implements IKeyLabelTimezone

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IKeyLabelTimezone
        Return New PlatformNub
    End Function

End Class


''' <summary> The Platform Entity. Implements access to the database using Dapper. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class PlatformEntity
    Inherits EntityBase(Of IKeyLabelTimezone, PlatformNub)
    Implements IKeyLabelTimezone

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New PlatformNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Platform interface. </param>
    Public Sub New(ByVal value As IKeyLabelTimezone)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyLabelTimezone, ByVal store As IKeyLabelTimezone)
        MyBase.New(New PlatformNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PlatformBuilder.TableName, NameOf(IKeyLabelTimezone).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IKeyLabelTimezone
        Return New PlatformNub
    End Function

    Public Overrides Function CreateCopy() As IKeyLabelTimezone
        Dim destination As IKeyLabelTimezone = Me.CreateNew
        PlatformNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IKeyLabelTimezone)
        PlatformNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Platform interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyLabelTimezone)
        ' first make the copy to notify of any property change.
        PlatformNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Platform table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyLabelTimezone)(key), connection.Get(Of PlatformNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PlatformName)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Platform Label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As PlatformNub = PlatformEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyLabelTimezone) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PlatformEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(PlatformBuilder.Get.UsingUniqueLabel(connection),
                                        Me.FetchUsingUniqueIndex(connection, entity.Label),
                                        Me.FetchUsingKey(connection, entity.AutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of PlatformNub)(New PlatformNub With {.AutoId = key})
    End Function

    ''' <summary> Updates the time stamp. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="timestamp">  The UTC timestamp. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function UpdateTimeStamp(ByVal connection As System.Data.IDbConnection, ByVal timestamp As Date) As Boolean
        PlatformBuilder.Get.UpdateTimestamp(connection, Me.PlatformName, timestamp)
        Return Me.FetchUsingKey(connection)
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Platform entities. </summary>
    ''' <value> The Platform entities. </value>
    Public ReadOnly Property Platforms As IEnumerable(Of PlatformEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PlatformEntity)
        Return If(usingNativeTracking, PlatformEntity.Populate(connection.GetAll(Of IKeyLabelTimezone)), PlatformEntity.Populate(connection.GetAll(Of PlatformNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Platforms = PlatformEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(PlatformEntity.Platforms))
        Return If(Me.Platforms?.Any, Me.Platforms.Count, 0)
    End Function

    ''' <summary> Populates a list of Platform entities. </summary>
    ''' <param name="nubs"> The Platform nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PlatformNub)) As IEnumerable(Of PlatformEntity)
        Dim l As New List(Of PlatformEntity)
        If nubs?.Any Then
            For Each nub As PlatformNub In nubs
                l.Add(New PlatformEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Platform entities. </summary>
    ''' <param name="interfaces"> The Platform interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyLabelTimezone)) As IEnumerable(Of PlatformEntity)
        Dim l As New List(Of PlatformEntity)
        If interfaces?.Any Then
            Dim nub As New PlatformNub
            For Each iFace As IKeyLabelTimezone In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PlatformEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="platformName"> The name of the platform. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal platformName As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PlatformBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PlatformNub.Label)} = @PlatformName", New With {platformName})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="platformName"> The name of the platform. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal platformName As String) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{PlatformBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(PlatformNub.Label)} IS NULL OR {NameOf(PlatformNub.Label)} = @platformName)")
        selectSql.Append("; ")
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {platformName})
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="platformName"> The name of the platform. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal platformName As String) As IEnumerable(Of PlatformNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PlatformBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PlatformNub.Label)} = @PlatformName", New With {platformName})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PlatformNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the platform exists. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="platformName"> The name of the platform. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal platformName As String) As Boolean
        Return 1 = PlatformEntity.CountEntities(connection, platformName)
    End Function

#End Region


#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Platform. </summary>
    ''' <value> Identifies the Platform. </value>
    Public Property AutoId As Integer Implements IKeyLabelTimezone.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the label. </summary>
    ''' <value> The label. </value>
    Public Property Label As String Implements IKeyLabelTimezone.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PlatformEntity.PlatformName))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the name of the Platform. </summary>
    ''' <value> The name of the Platform. </value>
    Public Property PlatformName As String
        Get
            Return Me.Label
        End Get
        Set(value As String)
            Me.Label = value
        End Set
    End Property

    ''' <summary> Gets or sets the timezone identity. </summary>
    ''' <remarks>
    ''' The entity <see cref="KeyLabelTimezoneNub.TimezoneId"/> is derived from the
    ''' <see cref="System.TimeZone.CurrentTimeZone"/> Standard Name, which, in-fact, is the timezone
    ''' identity. The Standard Name is used by the
    ''' <see cref="System.TimeZoneInfo.FindSystemTimeZoneById(String)"/> to select the
    ''' <see cref="System.TimeZoneInfo">time zone info</see> for the specified identity (Standard
    ''' Name).
    ''' </remarks>
    ''' <value> The timezone identity. </value>
    Public Property TimezoneId As String Implements IKeyLabelTimezone.TimezoneId
        Get
            Return Me.ICache.TimezoneId
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimezoneId, value) Then
                Me.ICache.TimezoneId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PlatformEntity.PlatformName))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks> Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to convert to local time. </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements IKeyLabelTimezone.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " TIMESTAMP CAPTION "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

#End Region

#Region " ENTITY LOCAL TIMES "

    Private _LocaleTimeZoneInfo As TimeZoneInfo

    ''' <summary> Returns the information describing the time zone in the entity locale. </summary>
    Public Function LocaleTimeZoneInfo() As TimeZoneInfo
        If Me._LocaleTimeZoneInfo Is Nothing Then
            If String.IsNullOrWhiteSpace(Me.TimezoneId) Then Me.TimezoneId = TimeZoneInfo.Local.Id
            Me._LocaleTimeZoneInfo = System.TimeZoneInfo.FindSystemTimeZoneById(Me.TimezoneId)
        End If
        Return Me._LocaleTimeZoneInfo
    End Function

    ''' <summary> Returns the time now in the entity locale based on the Locale <see cref="System.TimeZoneInfo"/>. </summary>
    ''' <returns> The time now in the entity locale. </returns>
    Public Function LocaleTimeNow() As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTime(DateTimeOffset.Now, Me.LocaleTimeZoneInfo)
    End Function

    ''' <summary> Returns the <see cref="Timestamp"/> in the entity local. </summary>
    ''' <returns> The <see cref="Timestamp"/> entity locale. </returns>
    Public Function LocaleTimestamp() As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTimeFromUtc(Me.Timestamp, Me.LocaleTimeZoneInfo)
    End Function

    ''' <summary> Converts an universalTime to a locale timestamp. </summary>
    ''' <param name="universalTime"> The universal time. </param>
    ''' <returns> UniversalTime as a DateTimeOffset. </returns>
    Public Function ToLocaleTimestamp(ByVal universalTime As DateTime) As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTimeFromUtc(universalTime, Me.LocaleTimeZoneInfo)
    End Function

#End Region

End Class

