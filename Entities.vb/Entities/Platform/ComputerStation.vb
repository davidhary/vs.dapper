Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Computer-Station builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class ComputerStationBuilder
    Inherits OneToOneBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ComputerStationBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ComputerStationBuilder._TableName) Then
                ComputerStationBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ComputerStationNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = ComputerBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(ComputerNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = StationBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(StationNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(ComputerStationEntity.ComputerAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(ComputerStationEntity.StationAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ComputerStationBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ComputerStationBuilder
        If ComputerStationBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ComputerStationBuilder._Instance = New ComputerStationBuilder()
            End SyncLock
        End If
        Return ComputerStationBuilder._Instance
    End Function

#End Region

#Region " FULL VIEW "

    ''' <summary> Builds view select query. </summary>
    ''' <remarks> David, 8/11/2020. </remarks>
    ''' <returns> The View Select Query. </returns>
    Protected Overrides Function BuildViewSelectQuery() As String
        Return $"
SELECT 
    [{NameOf(OneToOneNub.PrimaryId)}] AS [{Me.PrimaryIdFieldName}], 
    [{ComputerBuilder.TableName}].[{NameOf(ComputerNub.Label)}] AS [{NameOf(ComputerEntity.ComputerName)}], 
    [{NameOf(OneToOneNub.SecondaryId)}] AS [{Me.SecondaryIdFieldName}],
    [{StationBuilder.TableName}].[{NameOf(StationNub.Label)}] AS [{NameOf(StationEntity.StationName)}],
    [{StationBuilder.TableName}].[{NameOf(StationNub.TimezoneId)}] AS [{NameOf(StationEntity.TimezoneId)}],
    [{StationBuilder.TableName}].[{NameOf(StationNub.Timestamp)}] AS [{NameOf(StationEntity.Timestamp)}]
FROM [{Me.TableNameThis}]
	 INNER JOIN [{ComputerBuilder.TableName}] ON [{ComputerStationBuilder.TableName}].[{NameOf(ComputerStationNub.PrimaryId)}] = [{ComputerBuilder.TableName}].[{NameOf(ComputerNub.AutoId)}]
	 INNER JOIN [{StationBuilder.TableName}] ON [{ComputerStationBuilder.TableName}].[{NameOf(ComputerStationNub.SecondaryId)}] = [{StationBuilder.TableName}].[{NameOf(StationNub.AutoId)}]
"
    End Function

#End Region

End Class

''' <summary> Implements the Computer Station Nub based on the <see cref="IOneToOne">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("ComputerStation")>
Public Class ComputerStationNub
    Inherits OneToOneNub
    Implements IOneToOne

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToOne
        Return New ComputerStationNub
    End Function

End Class

''' <summary>
''' The Computer-Station Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class ComputerStationEntity
    Inherits EntityBase(Of IOneToOne, ComputerStationNub)
    Implements IOneToOne

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New ComputerStationNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Computer-Station interface. </param>
    Public Sub New(ByVal value As IOneToOne)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToOne, ByVal store As IOneToOne)
        MyBase.New(New ComputerStationNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ComputerStationBuilder.TableName, NameOf(IOneToOne).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToOne
        Return New ComputerStationNub
    End Function

    Public Overrides Function CreateCopy() As IOneToOne
        Dim destination As IOneToOne = Me.CreateNew
        ComputerStationNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToOne)
        ComputerStationNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Computer-Station interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToOne)
        ' first make the copy to notify of any property change.
        ComputerStationNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="computerAutoId"> Identifies the <see cref="ComputerEntity"/>. </param>
    ''' <param name="stationAutoId">      Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal computerAutoId As Integer, ByVal stationAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As ComputerStationNub = ComputerStationEntity.FetchNubs(connection, computerAutoId, stationAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.ComputerAutoId, Me.StationAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.ComputerAutoId, Me.StationAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="computerAutoId"> Identifies the <see cref="ComputerEntity"/>. </param>
    ''' <param name="stationAutoId">  Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal computerAutoId As Integer, ByVal stationAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, computerAutoId, stationAutoId)
    End Function

    ''' <summary> Obtains a default entity using the given connection. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> As. </returns>
    Public Function ObtainDefault(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String)
        Return Me.TryObtain(connection, My.Computer.Name, My.Computer.Name, False)
    End Function

    ''' <summary> Attempts to insert or retrieve a <see cref="ComputerStationEntity"/> from the given data. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="computerName">  The name of the computer. </param>
    ''' <param name="promptEnabled"> True to enable, false to disable the prompt. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal computerName As String, ByVal stationName As String, ByVal promptEnabled As Boolean) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        If String.IsNullOrWhiteSpace(computerName) Then Throw New ArgumentNullException(NameOf(computerName))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim computerEntity As New ComputerEntity With {.ComputerName = computerName, .TimezoneId = System.TimeZone.CurrentTimeZone.StandardName}
        If computerEntity.TryObtain(connection, computerName, promptEnabled) Then
            Dim stationEntity As New StationEntity With {.StationName = stationName, .TimezoneId = System.TimeZone.CurrentTimeZone.StandardName}
            If stationEntity.TryObtain(connection, stationName, promptEnabled) Then
                Me.ComputerAutoId = computerEntity.AutoId
                Me.StationAutoId = stationEntity.AutoId
                If Me.Obtain(connection) Then
                    Me.FetchComputerEntity(connection)
                    Me.FetchStationEntity(connection)
                Else
                    result = (False, $"Failed obtaining {NameOf(Entities.ComputerStationEntity)} for [{NameOf(Entities.ComputerEntity.AutoId)}, {NameOf(Entities.StationEntity.AutoId)}] of [{computerEntity.AutoId},{stationEntity.AutoId}]")
                End If
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.StationEntity)} with {NameOf(Entities.StationEntity.StationName)} of {stationName}")
            End If
        Else
            result = (False, $"Failed obtaining {NameOf(Entities.ComputerEntity)} with {NameOf(Entities.ComputerEntity.ComputerName)} of {computerName}")
        End If
        Return result
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToOne) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ComputerStationEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="computerAutoId">     Identifies the <see cref="ComputerEntity"/>. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal computerAutoId As Integer, ByVal stationAutoId As Integer) As Boolean
        Return connection.Delete(Of ComputerStationNub)(New ComputerStationNub With {.PrimaryId = computerAutoId, .SecondaryId = stationAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Computer-Station entities. </summary>
    ''' <value> The Computer-Station entities. </value>
    Public ReadOnly Property ComputerStations As IEnumerable(Of ComputerStationEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ComputerStationEntity)
        Return If(usingNativeTracking, ComputerStationEntity.Populate(connection.GetAll(Of IOneToOne)), ComputerStationEntity.Populate(connection.GetAll(Of ComputerStationNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ComputerStations = ComputerStationEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ComputerStationEntity.ComputerStations))
        Return If(Me.ComputerStations?.Any, Me.ComputerStations.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ComputerStationNub)) As IEnumerable(Of ComputerStationEntity)
        Dim l As New List(Of ComputerStationEntity)
        If nubs?.Any Then
            For Each nub As ComputerStationNub In nubs
                l.Add(New ComputerStationEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToOne)) As IEnumerable(Of ComputerStationEntity)
        Dim l As New List(Of ComputerStationEntity)
        If interfaces?.Any Then
            Dim nub As New ComputerStationNub
            For Each iFace As IOneToOne In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ComputerStationEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to Station entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="computerAutoId"> Identifies the <see cref="ComputerEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal computerAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ComputerStationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ComputerStationNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = computerAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="computerAutoId">  Identifies the <see cref="ComputerEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal computerAutoId As Integer) As IEnumerable(Of ComputerStationEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ComputerStationBuilder.TableName}] WHERE {NameOf(ComputerStationNub.PrimaryId)} = @Id", New With {Key .Id = computerAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ComputerStationEntity.Populate(connection.Query(Of ComputerStationNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Station. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> The total number of entities by Station. </returns>
    Public Shared Function CountEntitiesByStation(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ComputerStationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ComputerStationNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Stations in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Stations in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByStation(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As IEnumerable(Of ComputerStationEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ComputerStationBuilder.TableName}] WHERE {NameOf(ComputerStationNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ComputerStationEntity.Populate(connection.Query(Of ComputerStationNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="computerAutoId"> Identifies the <see cref="ComputerEntity"/>. </param>
    ''' <param name="stationAutoId">       Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal computerAutoId As Integer, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ComputerStationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ComputerStationNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = computerAutoId})
        sqlBuilder.Where($"{NameOf(ComputerStationNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="computerAutoId"> Identifies the <see cref="ComputerEntity"/>. </param>
    ''' <param name="stationAutoId">       Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal computerAutoId As Integer, ByVal stationAutoId As Integer) As IEnumerable(Of ComputerStationNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ComputerStationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ComputerStationNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = computerAutoId})
        sqlBuilder.Where($"{NameOf(ComputerStationNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ComputerStationNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Computer Station exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="computerAutoId"> Identifies the <see cref="ComputerEntity"/>. </param>
    ''' <param name="stationAutoId">       Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal computerAutoId As Integer, ByVal stationAutoId As Integer) As Boolean
        Return 1 = ComputerStationEntity.CountEntities(connection, computerAutoId, stationAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Computer entity. </summary>
    ''' <value> The Computer entity. </value>
    Public ReadOnly Property ComputerEntity As ComputerEntity

    ''' <summary> Fetches Computer Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Computer Entity. </returns>
    Public Function FetchComputerEntity(ByVal connection As System.Data.IDbConnection) As ComputerEntity
        Dim entity As New ComputerEntity()
        entity.FetchUsingKey(connection, Me.ComputerAutoId)
        Me._ComputerEntity = entity
        Return entity
    End Function

    ''' <summary> Count Computers associated with the specified <paramref name="stationAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> The total number of Computers. </returns>
    Public Shared Function CountComputers(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{ComputerStationBuilder.TableName}] WHERE {NameOf(ComputerStationNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Computers associated with the specified <paramref name="stationAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Computers in this collection.
    ''' </returns>
    Public Shared Function FetchComputers(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As IEnumerable(Of StationEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ComputerStationBuilder.TableName}] WHERE {NameOf(ComputerStationNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of StationEntity)
        For Each nub As ComputerStationNub In connection.Query(Of ComputerStationNub)(selector.RawSql, selector.Parameters)
            Dim entity As New ComputerStationEntity(nub)
            l.Add(entity.FetchStationEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Computers associated with the specified <paramref name="stationAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteComputers(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{ComputerStationBuilder.TableName}] WHERE {NameOf(ComputerStationNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = stationAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the Station entity. </summary>
    ''' <value> The Station entity. </value>
    Public ReadOnly Property StationEntity As StationEntity

    ''' <summary> Fetches a Station Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Station Entity. </returns>
    Public Function FetchStationEntity(ByVal connection As System.Data.IDbConnection) As StationEntity
        Dim entity As New StationEntity()
        entity.FetchUsingKey(connection, Me.StationAutoId)
        Me._StationEntity = entity
        Return entity
    End Function

    Public Shared Function CountStations(ByVal connection As System.Data.IDbConnection, ByVal computerAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{ComputerStationBuilder.TableName}] WHERE {NameOf(ComputerStationNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = computerAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Stations in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="computerAutoId"> Identifies the <see cref="ComputerEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Stations in this collection.
    ''' </returns>
    Public Shared Function FetchStations(ByVal connection As System.Data.IDbConnection, ByVal computerAutoId As Integer) As IEnumerable(Of StationEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ComputerStationBuilder.TableName}] WHERE {NameOf(ComputerStationNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = computerAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of StationEntity)
        For Each nub As ComputerStationNub In connection.Query(Of ComputerStationNub)(selector.RawSql, selector.Parameters)
            Dim entity As New ComputerStationEntity(nub)
            l.Add(entity.FetchStationEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Station related to the specified Computer. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="computerAutoId">  Identifies the <see cref="ComputerEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteStations(ByVal connection As System.Data.IDbConnection, ByVal computerAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{ComputerStationBuilder.TableName}] WHERE {NameOf(ComputerStationNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = computerAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToOne.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ComputerStationEntity.ComputerAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Computer. </summary>
    ''' <value> Identifies the Computer. </value>
    Public Property ComputerAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToOne.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ComputerStationEntity.StationAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Station. </summary>
    ''' <value> Identifies the Station. </value>
    Public Property StationAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
