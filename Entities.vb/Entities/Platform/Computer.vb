Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Computer builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class ComputerBuilder
    Inherits KeyLabelTimezoneBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ComputerBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ComputerBuilder._TableName) Then
                ComputerBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ComputerNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 50

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(ComputerEntity.AutoId)

    Public Overrides Property LabelFieldName() As String = NameOf(ComputerEntity.ComputerName)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property TimezoneIdFieldName() As String = NameOf(ComputerEntity.TimezoneId)

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <value> The name of the timestamp field. </value>
    Public Overrides Property TimestampFieldName() As String = NameOf(ComputerEntity.Timestamp)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ComputerBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ComputerBuilder
        If ComputerBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ComputerBuilder._Instance = New ComputerBuilder()
            End SyncLock
        End If
        Return ComputerBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Computer table based on the <see cref="IKeyLabelTimezone">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("Computer")>
Public Class ComputerNub
    Inherits KeyLabelTimezoneNub
    Implements IKeyLabelTimezone

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IKeyLabelTimezone
        Return New ComputerNub
    End Function

End Class


''' <summary> The Computer Entity. Implements access to the database using Dapper. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class ComputerEntity
    Inherits EntityBase(Of IKeyLabelTimezone, ComputerNub)
    Implements IKeyLabelTimezone

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New ComputerNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Computer interface. </param>
    Public Sub New(ByVal value As IKeyLabelTimezone)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyLabelTimezone, ByVal store As IKeyLabelTimezone)
        MyBase.New(New ComputerNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ComputerBuilder.TableName, NameOf(IKeyLabelTimezone).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IKeyLabelTimezone
        Return New ComputerNub
    End Function

    Public Overrides Function CreateCopy() As IKeyLabelTimezone
        Dim destination As IKeyLabelTimezone = Me.CreateNew
        ComputerNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IKeyLabelTimezone)
        ComputerNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Computer interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyLabelTimezone)
        ' first make the copy to notify of any property change.
        ComputerNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Computer table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyLabelTimezone)(key), connection.Get(Of ComputerNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.ComputerName)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal computerName As String) As Boolean
        Me.ClearStore()
        Dim nub As ComputerNub = ComputerEntity.FetchNubs(connection, computerName).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Obtains a default entity using the given connection. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function ObtainDefault(ByVal connection As System.Data.IDbConnection) As Boolean
        If String.IsNullOrEmpty(Me.ComputerName) Then Me.ComputerName = My.Computer.Name
        If String.IsNullOrWhiteSpace(Me.TimezoneId) Then Me.TimezoneId = System.TimeZone.CurrentTimeZone.StandardName
        Return Me.Obtain(connection)
    End Function

    ''' <summary> Attempts to obtain from the given data. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="computerName">  The name of the computer. </param>
    ''' <param name="promptEnabled"> True to enable, false to disable the prompt. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal computerName As String, ByVal promptEnabled As Boolean) As Boolean
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        If String.IsNullOrWhiteSpace(computerName) Then Throw New ArgumentNullException(NameOf(computerName))
        Me.ComputerName = computerName
        If Me.FetchUsingUniqueIndex(connection) Then
            Return Me.IsClean
        ElseIf promptEnabled Then
            Return isr.Core.MyDialogResult.Yes = isr.Core.WindowsForms.ShowDialogNoYes($"{NameOf(ComputerEntity)} found no records for computer '{computerName}'",
                                                                                       "Add Computer to the Database?") AndAlso Me.ObtainDefault(connection)
        Else
            Return Me.ObtainDefault(connection)
        End If
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyLabelTimezone) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ComputerEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(ComputerBuilder.Get.UsingUniqueLabel(connection),
                                        Me.FetchUsingUniqueIndex(connection, entity.Label),
                                        Me.FetchUsingKey(connection, entity.AutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of ComputerNub)(New ComputerNub With {.AutoId = key})
    End Function

    ''' <summary> Updates the time stamp. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="timestamp">  The UTC timestamp. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function UpdateTimeStamp(ByVal connection As System.Data.IDbConnection, ByVal timestamp As Date) As Boolean
        ComputerBuilder.Get.UpdateTimestamp(connection, Me.ComputerName, timestamp)
        Return Me.FetchUsingKey(connection)
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Computer entities. </summary>
    ''' <value> The Computer entities. </value>
    Public ReadOnly Property Computers As IEnumerable(Of ComputerEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ComputerEntity)
        Return If(usingNativeTracking, ComputerEntity.Populate(connection.GetAll(Of IKeyLabelTimezone)), ComputerEntity.Populate(connection.GetAll(Of ComputerNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Computers = ComputerEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ComputerEntity.Computers))
        Return If(Me.Computers?.Any, Me.Computers.Count, 0)
    End Function

    ''' <summary> Populates a list of Computer entities. </summary>
    ''' <param name="nubs"> The Computer nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ComputerNub)) As IEnumerable(Of ComputerEntity)
        Dim l As New List(Of ComputerEntity)
        If nubs?.Any Then
            For Each nub As ComputerNub In nubs
                l.Add(New ComputerEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Computer entities. </summary>
    ''' <param name="interfaces"> The Computer interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyLabelTimezone)) As IEnumerable(Of ComputerEntity)
        Dim l As New List(Of ComputerEntity)
        If interfaces?.Any Then
            Dim nub As New ComputerNub
            For Each iFace As IKeyLabelTimezone In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ComputerEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="computerName"> The name of the computer. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal computerName As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ComputerBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ComputerNub.Label)} = @ComputerName", New With {computerName})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="computerName"> The name of the computer. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal computerName As String) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{ComputerBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(ComputerNub.Label)} IS NULL OR {NameOf(ComputerNub.Label)} = @computerName)")
        selectSql.Append("; ")
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {computerName})
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="computerName"> The name of the computer. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal computerName As String) As IEnumerable(Of ComputerNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ComputerBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ComputerNub.Label)} = @ComputerName", New With {computerName})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ComputerNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the computer exists. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="computerName"> The name of the computer. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal computerName As String) As Boolean
        Return 1 = ComputerEntity.CountEntities(connection, computerName)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Computer. </summary>
    ''' <value> Identifies the Computer. </value>
    Public Property AutoId As Integer Implements IKeyLabelTimezone.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Computer number. </summary>
    ''' <value> The Computer number. </value>
    Public Property Label As String Implements IKeyLabelTimezone.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ComputerEntity.ComputerName))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the name of the computer. </summary>
    ''' <value> The name of the computer. </value>
    Public Property ComputerName As String
        Get
            Return Me.Label
        End Get
        Set(value As String)
            Me.Label = value
        End Set
    End Property

    ''' <summary> Gets or sets the timezone identity. </summary>
    ''' <remarks>
    ''' The entity <see cref="KeyLabelTimezoneNub.TimezoneId"/> is derived from the
    ''' <see cref="System.TimeZone.CurrentTimeZone"/> Standard Name, which, in-fact, is the timezone
    ''' identity. The Standard Name is used by the
    ''' <see cref="System.TimeZoneInfo.FindSystemTimeZoneById(String)"/> to select the
    ''' <see cref="System.TimeZoneInfo">time zone info</see> for the specified identity (Standard
    ''' Name).
    ''' </remarks>
    ''' <value> The timezone identity. </value>
    Public Property TimezoneId As String Implements IKeyLabelTimezone.TimezoneId
        Get
            Return Me.ICache.TimezoneId
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimezoneId, value) Then
                Me.ICache.TimezoneId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ComputerEntity.ComputerName))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks> Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to convert to local time. </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements IKeyLabelTimezone.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " TIMESTAMP CAPTION "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

#End Region

#Region " ENTITY LOCAL TIMES "

    Private _LocaleTimeZoneInfo As TimeZoneInfo

    ''' <summary> Returns the information describing the time zone in the entity locale. </summary>
    ''' <remarks>
    ''' The entity <see cref="KeyLabelTimezoneNub.TimezoneId"/> is derived from the
    ''' <see cref="System.TimeZone.CurrentTimeZone"/> Standard Name The standard name is used by the
    ''' <see cref="System.TimeZoneInfo.FindSystemTimeZoneById(String)"/> to select the
    ''' <see cref="System.TimeZoneInfo">time zone info</see> for the specified identity (Standard Name).
    ''' </remarks>
    ''' <returns> A TimeZoneInfo. </returns>
    Public Function LocaleTimeZoneInfo() As TimeZoneInfo
        If Me._LocaleTimeZoneInfo Is Nothing Then
            If String.IsNullOrWhiteSpace(Me.TimezoneId) Then Me.TimezoneId = TimeZoneInfo.Local.Id
            Me._LocaleTimeZoneInfo = System.TimeZoneInfo.FindSystemTimeZoneById(Me.TimezoneId)
        End If
        Return Me._LocaleTimeZoneInfo
    End Function

    ''' <summary> Returns the time now in the entity locale based on the Locale <see cref="System.TimeZoneInfo"/>. </summary>
    ''' <returns> The time now in the entity locale. </returns>
    Public Function LocaleTimeNow() As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTime(DateTimeOffset.Now, Me.LocaleTimeZoneInfo)
    End Function

    ''' <summary> Returns the <see cref="Timestamp"/> in the entity local. </summary>
    ''' <returns> The <see cref="Timestamp"/> entity locale. </returns>
    Public Function LocaleTimestamp() As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTimeFromUtc(Me.Timestamp, Me.LocaleTimeZoneInfo)
    End Function

    ''' <summary> Converts an universalTime to a locale timestamp. </summary>
    ''' <param name="universalTime"> The universal time. </param>
    ''' <returns> UniversalTime as a DateTimeOffset. </returns>
    Public Function ToLocaleTimestamp(ByVal universalTime As DateTime) As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTimeFromUtc(universalTime, Me.LocaleTimeZoneInfo)
    End Function

#End Region

End Class

