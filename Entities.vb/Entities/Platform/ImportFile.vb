Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions

''' <summary>
''' Interface for the ImportFile nub and entity. Includes the fields as kept in the
''' data table. Allows tracking of property changes.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Interface IImportFile

    ''' <summary> Gets or sets the identifier of the import file. </summary>
    ''' <value> Identifies the import file. </value>
    <ExplicitKey>
    Property Id As Integer

    ''' <summary> Gets or sets the filename of the file. </summary>
    ''' <value> The name of the file. </value>
    Property FileName As String

    ''' <summary> Gets or sets the File UTC timestamp. </summary>
    ''' <remarks> Stored in universal time. </remarks>
    ''' <value> The File UTC timestamp. </value>
    Property FileTimestamp As DateTime

    ''' <summary> Gets or sets the identifier of the file import state. </summary>
    ''' <value> Identifies the file import state. </value>
    Property FileImportStateId As Integer

    ''' <summary> Gets or sets the details. </summary>
    ''' <value> The details. </value>
    Property Details As String

    ''' <summary> Gets or sets the UTC timestamp of the last import. </summary>
    ''' <remarks> Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to convert to local time. </remarks>
    ''' <value> The UTC timestamp. </value>
    Property ImportTimestamp As DateTime

End Interface

''' <summary> An import file builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class ImportFileBuilder

#Region " TABLE BUILDER "

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ImportFileBuilder._TableName) Then
                ImportFileBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ImportFileNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return ImportFileBuilder._TableName
        End Get
    End Property

    ''' <summary> Gets the name of the FileName index. </summary>
    ''' <value> The name of the FileName index. </value>
    Private Shared ReadOnly Property FileNameIndexName As String
        Get
            Return $"UQ_{ImportFileBuilder.TableName}_{NameOf(ImportFileNub.FileName)}"
        End Get
    End Property

    Private Shared _UsingUniqueFileName As Boolean?

    ''' <summary> Indicates if the entity uses a unique FileName. </summary>
    Public Shared Function UsingUniqueFileName(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not ImportFileBuilder._UsingUniqueFileName.HasValue Then
            ImportFileBuilder._UsingUniqueFileName = connection.IndexExists(ImportFileBuilder.FileNameIndexName)
        End If
        Return ImportFileBuilder._UsingUniqueFileName.Value
    End Function

    ''' <summary> Inserts values from file. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="fileName">   Filename of the file. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertFileValues(ByVal connection As System.Data.IDbConnection, ByVal fileName As String) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        If fileName Is Nothing Then Throw New ArgumentNullException(NameOf(fileName))
        Return connection.Execute(System.IO.File.ReadAllText(fileName))
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection, ByVal uniqueFileName As Boolean) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql, uniqueFileName)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite, uniqueFileName)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <param name="connection"> The connection. </param>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection, ByVal uniqueFileName As Boolean) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{ImportFileBuilder.TableName}] (
	[{NameOf(ImportFileNub.Id)}] integer NOT NULL PRIMARY KEY, 
	[{NameOf(ImportFileNub.FileName)}] nvarchar(512) NOT NULL, 
	[{NameOf(ImportFileNub.FileTimestamp)}] datetime NOT NULL, 
	[{NameOf(ImportFileNub.FileImportStateId)}] integer NOT NULL, 
	[{NameOf(ImportFileNub.Details)}] nvarchar(2048) NOT NULL, 
	[{NameOf(ImportFileNub.ImportTimestamp)}] datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP),
FOREIGN KEY ([{NameOf(ImportFileNub.FileImportStateId)}]) REFERENCES [{FileImportStateBuilder.TableName}] ([{NameOf(FileImportStateNub.Id)}])
		ON UPDATE CASCADE ON DELETE CASCADE); ")
        If uniqueFileName Then
            queryBuilder.AppendLine($"CREATE UNIQUE INDEX IF NOT EXISTS [{ImportFileBuilder.FileNameIndexName}] ON [{ImportFileBuilder.TableName}] ([{NameOf(ImportFileNub.FileName)}]); ")
        End If
        connection.Execute(queryBuilder.ToString().Clean())
        Return ImportFileBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <param name="connection"> The connection. </param>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection, ByVal uniqueFileName As Boolean) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{ImportFileBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{ImportFileBuilder.TableName}](
	[{NameOf(ImportFileNub.Id)}] [int] NOT NULL,
	[{NameOf(ImportFileNub.FileName)}] [nvarchar](512) NOT NULL,
	[{NameOf(ImportFileNub.FileTimestamp)}] [datetime] NOT NULL,
	[{NameOf(ImportFileNub.FileImportStateId)}] [int] NOT NULL,
	[{NameOf(ImportFileNub.Details)}] [nvarchar](2048) NOT NULL,
	[{NameOf(ImportFileNub.ImportTimestamp)}] [datetime] NOT NULL DEFAULT (sysutcdatetime()),
 CONSTRAINT [PK_{ImportFileBuilder.TableName}] PRIMARY KEY CLUSTERED 
([{NameOf(ImportFileNub.Id)}] ASC) 
 WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
 ON [PRIMARY]
END; 

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{ImportFileBuilder.TableName}_{FileImportStateBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{ImportFileBuilder.TableName}]'))
ALTER TABLE [dbo].[{ImportFileBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{ImportFileBuilder.TableName}_{FileImportStateBuilder.TableName}] FOREIGN KEY([{NameOf(ImportFileNub.FileImportStateId)}])
REFERENCES [dbo].[{FileImportStateBuilder.TableName}] ([{NameOf(FileImportStateNub.Id)}])
ON UPDATE CASCADE ON DELETE CASCADE; 
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{ImportFileBuilder.TableName}_{FileImportStateBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{ImportFileBuilder.TableName}]'))
ALTER TABLE [dbo].[{ImportFileBuilder.TableName}] CHECK CONSTRAINT [FK_{ImportFileBuilder.TableName}_{FileImportStateBuilder.TableName}]; ")

        If uniqueFileName Then
            queryBuilder.AppendLine($"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{ImportFileBuilder.TableName}]') AND name = N'{ImportFileBuilder.FileNameIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{ImportFileBuilder.FileNameIndexName}] ON [dbo].[{ImportFileBuilder.TableName}] ([{NameOf(ImportFileNub.FileName)}] ASC) 
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
ON [PRIMARY]; ")
        End If
        connection.Execute(queryBuilder.ToString().Clean())
        Return ImportFileBuilder.TableName
    End Function

#End Region

#Region " VIEWS "

    ''' <summary> Creates imported files view sq lite. </summary>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub CreateImportedFilesViewSQLite(ByVal connection As System.Data.IDbConnection)
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append("CREATE VIEW IF NOT EXISTS [ImportedFiles] AS
SELECT ImportFile.FileName, ImportFile.FileTimestamp, ImportFile.Timestamp, FileImportState.Name AS ImportState, ImportFile.Details
FROM   ImportFile INNER JOIN FileImportState ON ImportFile.FileImportStateId = FileImportState.FileImportStateId; ")
        connection.Execute(queryBuilder.ToString().Clean())
    End Sub

    ''' <summary> Creates imported files view SQL server. </summary>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub CreateImportedFilesViewSqlServer(ByVal connection As System.Data.IDbConnection)
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ImportedFiles]'))
EXEC   dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[ImportedFiles] AS
SELECT dbo.ImportFile.FileName, dbo.ImportFile.FileTimestamp, dbo.ImportFile.Timestamp, dbo.FileImportState.Name AS ImportState, dbo.ImportFile.Details
FROM   dbo.ImportFile INNER JOIN dbo.FileImportState ON dbo.ImportFile.FileImportStateId = dbo.FileImportState.FileImportStateId'; ")
        connection.Execute(queryBuilder.ToString().Clean())
    End Sub

#End Region

End Class


''' <summary> Implements the ImportFile table <see cref="IImportFile">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/10/2018 </para></remarks>
<Table("ImportFile")>
Public Class ImportFileNub
    Inherits EntityNubBase(Of IImportFile)
    Implements IImportFile

#Region " CONSTRUCTION "

    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IImportFile
        Return New ImportFileNub
    End Function

    Public Overrides Function CreateCopy() As IImportFile
        Dim destination As IImportFile = Me.CreateNew
        ImportFileNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IImportFile)
        ImportFileNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IImportFile, ByVal destination As IImportFile)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.Details = source.Details
        destination.FileImportStateId = source.FileImportStateId
        destination.FileName = source.FileName
        destination.FileTimestamp = source.FileTimestamp
        destination.Id = source.Id
        destination.ImportTimestamp = source.ImportTimestamp
    End Sub

#End Region

#Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IImportFile))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IImportFile) As Boolean ' Implements IEquatable(Of IImportFile).Equals
        Return other IsNot Nothing AndAlso ImportFileNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IImportFile, ByVal right As IImportFile) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.FileImportStateId, right.FileImportStateId)
            result = result AndAlso String.Equals(left.Details, right.Details)
            result = result AndAlso String.Equals(left.FileName, right.FileName)
            result = result AndAlso Date.Equals(left.FileTimestamp, right.FileTimestamp)
            result = result AndAlso String.Equals(left.Id, right.Id)
            result = result AndAlso Date.Equals(left.ImportTimestamp, right.ImportTimestamp)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.FileImportStateId Xor Me.Details.GetHashCode() Xor Me.FileName.GetHashCode() Xor Me.FileTimestamp.GetHashCode() Xor Me.Id Xor Me.ImportTimestamp.GetHashCode()
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the import file. </summary>
    ''' <value> Identifies the import file. </value>
    <ExplicitKey>
    Public Property Id As Integer Implements IImportFile.Id

    ''' <summary> Gets or sets the filename of the file. </summary>
    ''' <value> The name of the file. </value>
    Public Property FileName As String Implements IImportFile.FileName

    ''' <summary> Gets or sets the File UTC timestamp. </summary>
    ''' <remarks> Stored in universal time. </remarks>
    ''' <value> The File UTC timestamp. </value>
    Public Property FileTimestamp As DateTime Implements IImportFile.FileTimestamp

    ''' <summary> Gets or sets the identifier of the file import state. </summary>
    ''' <value> Identifies the file import state. </value>
    Public Property FileImportStateId As Integer Implements IImportFile.FileImportStateId

    ''' <summary> Gets or sets the details. </summary>
    ''' <value> The details. </value>
    Public Property Details As String Implements IImportFile.Details

    ''' <summary> Gets or sets the UTC timestamp of the last import. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The UTC timestamp. </value>
    Public Property ImportTimestamp As DateTime Implements IImportFile.ImportTimestamp

#End Region

End Class

''' <summary> The ImportFile Entity. Implements access to the database using Dapper. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/10/2018 </para></remarks>
Public Class ImportFileEntity
    Inherits EntityBase(Of IImportFile, ImportFileNub)
    Implements IImportFile

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New ImportFileNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As IImportFile)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IImportFile, ByVal store As IImportFile)
        MyBase.New(New ImportFileNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ImportFileBuilder.TableName, NameOf(IImportFile).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IImportFile
        Return New ImportFileNub
    End Function

    Public Overrides Function CreateCopy() As IImportFile
        Dim destination As IImportFile = Me.CreateNew
        ImportFileNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IImportFile)
        ImportFileNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IImportFile)
        ' first make the copy to notify of any property change.
        ImportFileNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The ImportFile table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IImportFile)(key), connection.Get(Of ImportFileNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function


    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.FileName)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="fileName">   The <see cref="ImportFileEntity"/> file name. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal fileName As String) As Boolean
        Me.ClearStore()
        Dim nub As ImportFileNub = ImportFileEntity.FetchNubs(connection, fileName).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IImportFile) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ImportFileEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.FileName) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Stores an entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Shared Function StoreEntity(ByVal connection As System.Data.IDbConnection, ByVal entity As IImportFile) As Boolean
        Return New ImportFileEntity().Upsert(connection, entity)
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of IImportFile)(New ImportFileNub With {.Id = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the import files. </summary>
    ''' <value> The import files. </value>
    Public ReadOnly Property ImportFiles As IEnumerable(Of ImportFileEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ImportFileEntity)
        Return If(usingNativeTracking, ImportFileEntity.Populate(connection.GetAll(Of IImportFile)), ImportFileEntity.Populate(connection.GetAll(Of ImportFileNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ImportFiles = ImportFileEntity.FetchAllEntities(connection, True)
        Me.NotifyPropertyChanged(NameOf(ImportFileEntity.ImportFiles))
        Return If(Me.ImportFiles?.Any, Me.ImportFiles.Count, 0)
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <param name="nubs"> The entity nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ImportFileNub)) As IEnumerable(Of ImportFileEntity)
        Dim l As New List(Of ImportFileEntity)
        If nubs?.Any Then
            For Each nub As ImportFileNub In nubs
                l.Add(New ImportFileEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <param name="interfaces"> The entity interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IImportFile)) As IEnumerable(Of ImportFileEntity)
        Dim l As New List(Of ImportFileEntity)
        If interfaces?.Any Then
            Dim nub As New ImportFileNub
            For Each iFace As IImportFile In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ImportFileEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="fileName"> The <see cref="ImportFileEntity"/> file name. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal fileName As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{ImportFileBuilder.TableName}]  WHERE {NameOf(ImportFileNub.FileName)} = @FileName", New With {Key .FileName = FileName})
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{ImportFileBuilder.TableName}] WHERE {NameOf(ImportFileNub.FileName)} = @FileName", New With {fileName})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="fileName"> The <see cref="ImportFileEntity"/> file name. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal fileName As String) As IEnumerable(Of ImportFileNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ImportFileBuilder.TableName}] WHERE {NameOf(ImportFileNub.FileName)} = @FileName", New With {fileName})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ImportFileNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the ImportFile exists. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="fileName"> The <see cref="ImportFileEntity"/> file name. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal fileName As String) As Boolean
        Return 1 = ImportFileEntity.CountEntities(connection, fileName)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the import file. </summary>
    ''' <value> Identifies the import file. </value>
    Public Property Id As Integer Implements IImportFile.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the filename of the file. </summary>
    ''' <value> The name of the file. </value>
    Public Property FileName As String Implements IImportFile.FileName
        Get
            Return Me.ICache.FileName
        End Get
        Set(value As String)
            If Not String.Equals(Me.FileName, value) Then
                Me.ICache.FileName = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the File UTC timestamp. </summary>
    ''' <remarks> Stored in universal time. </remarks>
    ''' <value> The File UTC timestamp. </value>
    Public Property FileTimestamp As DateTime Implements IImportFile.FileTimestamp
        Get
            Return Me.ICache.FileTimestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.FileTimestamp, value) Then
                Me.ICache.FileTimestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the file import state. </summary>
    ''' <value> Identifies the file import state. </value>
    Public Property FileImportStateId As Integer Implements IImportFile.FileImportStateId
        Get
            Return Me.ICache.FileImportStateId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.FileImportStateId, value) Then
                Me.ICache.FileImportStateId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ImportFileEntity.FileImportState))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the File Import State. </summary>
    ''' <value> The File Import State. </value>
    Public Property FileImportState As FileImportState
        Get
            Return CType(Me.FileImportStateId, FileImportState)
        End Get
        Set(value As FileImportState)
            Me.FileImportStateId = value
        End Set
    End Property

    ''' <summary> Gets or sets the details. </summary>
    ''' <value> The details. </value>
    Public Property Details As String Implements IImportFile.Details
        Get
            Return Me.ICache.Details
        End Get
        Set(value As String)
            If Not String.Equals(Me.Details, value) Then
                Me.ICache.Details = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp of the last import. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property ImportTimestamp As DateTime Implements IImportFile.ImportTimestamp
        Get
            Return Me.ICache.ImportTimestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.ImportTimestamp, value) Then
                Me.ICache.ImportTimestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " TIMESTAMP "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.ImportTimestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

#End Region

#Region " ACTIVE IMPORT FILE "

    ''' <summary> Gets or sets the active import file identifier. </summary>
    ''' <value> Identifies the active import file. </value>
    Public Shared Property ActiveImportFileId As Integer

    ''' <summary> Gets the active lot import file entity. </summary>
    ''' <value> The active lot import file entity. </value>
    Public Shared ReadOnly Property ActiveImportFileEntity As ImportFileEntity

    ''' <summary> Fetches active import file entity. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function FetchActiveImportFileEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        If ImportFileEntity.ActiveImportFileId <= 0 Then
            Throw New InvalidOperationException($"{NameOf(ImportFileEntity.ActiveImportFileId)} is {ImportFileEntity.ActiveImportFileId} must be set before fetching the active file import record.")
        End If
        ImportFileEntity._ActiveImportFileEntity = New ImportFileEntity() With {.Id = ImportFileEntity.ActiveImportFileId}
        Return ImportFileEntity.ActiveImportFileEntity.FetchUsingKey(connection)
    End Function

    ''' <summary> Import file information. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <returns> A System.IO.FileInfo. </returns>
    Public Shared Function ImportFileInfo() As System.IO.FileInfo
        Dim filename As String = ImportFileEntity.ActiveImportFileEntity.FileName
        If String.IsNullOrWhiteSpace(filename) Then Throw New InvalidOperationException($"{NameOf(ImportFileEntity.ActiveImportFileEntity)} {NameOf(ImportFileEntity.ActiveImportFileEntity.FileName)} is empty")
        Return New System.IO.FileInfo(filename)
    End Function

    ''' <summary> Query if an import required. </summary>
    ''' <returns> <c>true</c> if import required; otherwise <c>false</c> </returns>
    Public Shared Function IsImportRequired() As Boolean
        Return ImportFileEntity.ActiveImportFileEntity.ImportTimestamp < ImportFileEntity.ImportFileInfo.LastWriteTimeUtc
    End Function

    ''' <summary> Queries if a given if import file folder exists. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function IsImportFileFolderExists() As Boolean
        Return ImportFileEntity.ImportFileInfo.Directory.Exists
    End Function

    ''' <summary> Query if this the import file exists. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <returns> True if import file exists, false if not. </returns>
    Public Shared Function IsImportFileExists() As Boolean
        Return ImportFileEntity.ImportFileInfo.Exists
    End Function

    ''' <summary> Stores last import date time. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection">              The connection. </param>
    ''' <param name="lastImportUniversalTime"> The last import universal time. </param>
    Public Shared Sub StoreLastImportDateTime(ByVal connection As System.Data.IDbConnection, ByVal lastImportUniversalTime As DateTime)
        ImportFileEntity.ActiveImportFileEntity.FileTimestamp = ImportFileEntity.ImportFileInfo.LastWriteTimeUtc
        ImportFileEntity.ActiveImportFileEntity.ImportTimestamp = lastImportUniversalTime
        ImportFileEntity.ActiveImportFileEntity.Update(connection)
    End Sub


#End Region

End Class

