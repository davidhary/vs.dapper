Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A File Import State builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class FileImportStateBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return FileImportStateBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(FileImportStateBuilder._TableName) Then
                FileImportStateBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(FileImportStateNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Inserts or ignores the records described by the <see cref="FileImportState"/> enumeration type. </summary>
    ''' <param name="connection"> The connection. </param>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(FileImportState), Array.Empty(Of Integer))
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As FileImportStateBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As FileImportStateBuilder
        If FileImportStateBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                FileImportStateBuilder._Instance = New FileImportStateBuilder()
            End SyncLock
        End If
        Return FileImportStateBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the FileImportState table based on the <see cref="INominal">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("FileImportState")>
Public Class FileImportStateNub
    Inherits NominalNub
    Implements INominal

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As INominal
        Return New FileImportStateNub
    End Function

End Class


''' <summary> The FileImportState Entity. Implements access to the database using Dapper. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class FileImportStateEntity
    Inherits EntityBase(Of INominal, FileImportStateNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New FileImportStateNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The FileImportState interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New FileImportStateNub, cache, store)
        Me.UsingNativeTracking = String.Equals(FileImportStateBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As INominal
        Return New FileImportStateNub
    End Function


    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        FileImportStateNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As INominal)
        FileImportStateNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The FileImportState interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        FileImportStateNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The FileImportState table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of FileImportStateNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The File Import State label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As FileImportStateNub = FileImportStateEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If FileImportStateEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of FileImportStateNub)(New FileImportStateNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the FileImportState entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The FileImportState entities. </value>
    Public Shared ReadOnly Property FileImportStates As IEnumerable(Of FileImportStateEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of FileImportStateEntity)
        Return If(usingNativeTracking, FileImportStateEntity.Populate(connection.GetAll(Of INominal)), FileImportStateEntity.Populate(connection.GetAll(Of FileImportStateNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        FileImportStateEntity._FileImportStates = FileImportStateEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(FileImportStateEntity.FileImportStates?.Any, FileImportStateEntity.FileImportStates.Count, 0)
    End Function

    ''' <summary> Populates a list of FileImportState entities. </summary>
    ''' <param name="nubs"> The FileImportState nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of FileImportStateNub)) As IEnumerable(Of FileImportStateEntity)
        Dim l As New List(Of FileImportStateEntity)
        If nubs?.Any Then
            For Each nub As FileImportStateNub In nubs
                l.Add(New FileImportStateEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of FileImportState entities. </summary>
    ''' <param name="interfaces"> The FileImportState interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of FileImportStateEntity)
        Dim l As New List(Of FileImportStateEntity)
        If interfaces?.Any Then
            Dim nub As New FileImportStateNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New FileImportStateEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, FileImportStateEntity)

    ''' <summary> The File Import State lookup dictionary. </summary>
    ''' <returns> A Dictionary(Of Integer, FileImportStateEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, FileImportStateEntity)
        If Not (FileImportStateEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            FileImportStateEntity._EntityLookupDictionary = New Dictionary(Of Integer, FileImportStateEntity)
            For Each entity As FileImportStateEntity In FileImportStateEntity.FileImportStates
                FileImportStateEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return FileImportStateEntity._EntityLookupDictionary
    End Function

    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (FileImportStateEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            FileImportStateEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As FileImportStateEntity In FileImportStateEntity.FileImportStates
                FileImportStateEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return FileImportStateEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (FileImportStateEntity.FileImportStates?.Any AndAlso
                FileImportStateEntity._EntityLookupDictionary?.Any AndAlso
                FileImportStateEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function


#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The File Import State label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{FileImportStateNub.TableName}] 
        ' WHERE {NameOf(FileImportStateNub.Label)} = @Label", New With {Key .Label = label})
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{FileImportStateBuilder.TableName}] WHERE {NameOf(FileImportStateNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The File Import State label. </param>
    ''' <returns> An IFileImportState . </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of FileImportStateNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{FileImportStateBuilder.TableName}] WHERE {NameOf(FileImportStateNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of FileImportStateNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the FileImportState exists. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The File Import State label. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = FileImportStateEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the File Import State. </summary>
    ''' <value> Identifies the File Import State. </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(FileImportStateEntity.FileImportState))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the File Import State. </summary>
    ''' <value> The File Import State. </value>
    Public Property FileImportState As FileImportState
        Get
            Return CType(Me.Id, FileImportState)
        End Get
        Set(value As FileImportState)
            Me.Id = value
        End Set
    End Property


    ''' <summary> Gets or sets the File Import State label. </summary>
    ''' <value> The File Import State label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the File Import State. </summary>
    ''' <value> The File Import State description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="fileImportStateId"> The entity unique key. </param>
    ''' <returns> A <see cref="FileImportStateEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal fileImportStateId As Integer) As (Success As Boolean, Details As String, Entity As FileImportStateEntity)
        Dim activity As String = String.Empty
        Dim entity As New FileImportStateEntity
        Try
            activity = $"Fetching {NameOf(FileImportStateEntity)} by {NameOf(FileImportStateNub.Id)} of {fileImportStateId}"
            Return If(entity.FetchUsingKey(connection, fileImportStateId),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="fileImportStateLabel"> The File Import State label. </param>
    ''' <returns> A <see cref="FileImportStateEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal fileImportStateLabel As String) As (Success As Boolean, details As String, Entity As FileImportStateEntity)
        Dim activity As String = String.Empty
        Dim entity As New FileImportStateEntity
        Try
            activity = $"Fetching {NameOf(FileImportStateEntity)} by {NameOf(FileImportStateNub.Label)} of {fileImportStateLabel}"
            Return If(entity.FetchUsingUniqueIndex(connection, fileImportStateLabel),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <param name="connection"> The connection. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, details As String, Entities As IEnumerable(Of FileImportStateEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New FileImportStateEntity()
            activity = $"fetching all {NameOf(FileImportStateEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> FileImportStateEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(FileImportStateEntity.EntityLookupDictionary)} count must equal {NameOf(FileImportStateEntity.FileImportStates)} count ")
            ElseIf elementCount <> FileImportStateEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(FileImportStateEntity.KeyLookupDictionary)} count must equal {NameOf(FileImportStateEntity.FileImportStates)} count ")
            End If
            Return (True, String.Empty, FileImportStateEntity.FileImportStates)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of FileImportStateEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Values that represent file import states. </summary>
Public Enum FileImportState

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("<File Import State>")> None

    ''' <summary> An enum constant representing the read failed option. </summary>
    <ComponentModel.Description("Read Failed")> ReadFailed

    ''' <summary> An enum constant representing the ready option. </summary>
    <ComponentModel.Description("Ready")> Ready

    ''' <summary> An enum constant representing the import failed option. </summary>
    <ComponentModel.Description("Import Failed")> ImportFailed

    ''' <summary> An enum constant representing the imported option. </summary>
    <ComponentModel.Description("Imported")> Imported
End Enum

