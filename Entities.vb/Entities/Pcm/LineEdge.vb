Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions

''' <summary>
''' Interface for the LineEdge nub and entity. Includes the fields as kept in the data table.
''' Allows tracking of property changes.
''' </summary>
''' <remarks> David, 2020-10-02. </remarks>
Public Interface ILineEdge

    ''' <summary> Gets the identifier of the line edge. </summary>
    ''' <value> Identifies the line edge. </value>
    <ExplicitKey>
    Property Id As Integer

    ''' <summary> Gets the title. </summary>
    ''' <value> The title. </value>
    Property Title As String

    ''' <summary> Gets a list of source channels. </summary>
    ''' <value> A List of source channels. </value>
    Property SourceChannelList As String

    ''' <summary> Gets a list of measure channels. </summary>
    ''' <value> A List of measure channels. </value>
    Property MeasureChannelList As String

End Interface

''' <summary> A line edge builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class LineEdgeBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(LineEdgeBuilder._TableName) Then
                LineEdgeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(LineEdgeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return LineEdgeBuilder._TableName
        End Get
    End Property

    ''' <summary> Gets the name of the unique label index. </summary>
    ''' <value> The name of the unique label index. </value>
    Private Shared ReadOnly Property TitleIndexName As String
        Get
            Return $"UQ_{LineEdgeBuilder.TableName}_{NameOf(LineEdgeNub.Title)}"
        End Get
    End Property

    ''' <summary> The using unique title. </summary>
    Private Shared _UsingUniqueTitle As Boolean?

    ''' <summary> Indicates if the entity uses a unique Title. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function UsingUniqueTitle(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not LineEdgeBuilder._UsingUniqueTitle.HasValue Then
            LineEdgeBuilder._UsingUniqueTitle = connection.IndexExists(LineEdgeBuilder.TitleIndexName)
        End If
        Return LineEdgeBuilder._UsingUniqueTitle.Value
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{LineEdgeBuilder.TableName}] (
	[{NameOf(LineEdgeNub.Id)}] integer NOT NULL PRIMARY KEY, 
	[{NameOf(LineEdgeNub.Title)}] nvarchar(50) NOT NULL, 
	[{NameOf(LineEdgeNub.SourceChannelList)}] nvarchar(50) NOT NULL,
	[{NameOf(LineEdgeNub.MeasureChannelList)}] nvarchar(50) NOT NULL); 

CREATE UNIQUE INDEX IF NOT EXISTS [{LineEdgeBuilder.TitleIndexName}] ON [{LineEdgeBuilder.TableName}] ([{NameOf(LineEdgeNub.Title)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return LineEdgeBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{LineEdgeBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{LineEdgeBuilder.TableName}](
	[{NameOf(LineEdgeNub.Id)}] [int] NOT NULL,
	[{NameOf(LineEdgeNub.Title)}] [nvarchar](50) NOT NULL,
	[{NameOf(LineEdgeNub.SourceChannelList)}] [nvarchar](50) NOT NULL,
	[{NameOf(LineEdgeNub.MeasureChannelList)}] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_{LineEdgeBuilder.TableName}] PRIMARY KEY CLUSTERED 
([{NameOf(LineEdgeNub.Id)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END; 
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{LineEdgeBuilder.TableName}]') AND name = N'{LineEdgeBuilder.TitleIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{LineEdgeBuilder.TitleIndexName}] ON [dbo].[{LineEdgeBuilder.TableName}] ([{NameOf(LineEdgeNub.Title)}] ASC)
 WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return LineEdgeBuilder.TableName
    End Function

End Class

''' <summary> Implements the LineEdge table <see cref="ILineEdge">interface</see>. </summary>
''' <remarks> David, 2020-10-02. </remarks>
<Table("LineEdge")>
Public Class LineEdgeNub
    Inherits EntityNubBase(Of ILineEdge)
    Implements ILineEdge

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ILineEdge
        Return New LineEdgeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ILineEdge
        Dim destination As ILineEdge = Me.CreateNew
        LineEdgeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ILineEdge)
        LineEdgeNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As ILineEdge, ByVal destination As ILineEdge)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.Id = source.Id
        destination.MeasureChannelList = source.MeasureChannelList
        destination.SourceChannelList = source.SourceChannelList
        destination.Title = source.Title
    End Sub

#End Region

#Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, ILineEdge))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As ILineEdge) As Boolean ' Implements IEquatable(Of ILineEdge).Equals
        Return other IsNot Nothing AndAlso LineEdgeNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As ILineEdge, ByVal right As ILineEdge) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.Id, right.Id)
            result = result AndAlso String.Equals(left.MeasureChannelList, right.MeasureChannelList)
            result = result AndAlso String.Equals(left.SourceChannelList, right.SourceChannelList)
            result = result AndAlso String.Equals(left.Title, right.Title)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.Id Xor Me.MeasureChannelList.GetHashCode() Xor Me.SourceChannelList.GetHashCode() Xor Me.Title.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS  "

    ''' <summary> Gets or sets the identifier of the line edge. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the line edge. </value>
    <ExplicitKey>
    Public Property Id As Integer Implements ILineEdge.Id

    ''' <summary> Gets or sets the title. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The title. </value>
    Public Property Title As String Implements ILineEdge.Title

    ''' <summary> Gets or sets a list of source channels. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> A List of source channels. </value>
    Public Property SourceChannelList As String Implements ILineEdge.SourceChannelList

    ''' <summary> Gets or sets a list of measure channels. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> A List of measure channels. </value>
    Public Property MeasureChannelList As String Implements ILineEdge.MeasureChannelList

#End Region

End Class

''' <summary> The LineEdge Entity. Implements access to the database using Dapper. </summary>
''' <remarks> David, 2020-10-02. </remarks>
Public Class LineEdgeEntity
    Inherits EntityBase(Of ILineEdge, LineEdgeNub)
    Implements ILineEdge

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New LineEdgeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As ILineEdge)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ILineEdge, ByVal store As ILineEdge)
        MyBase.New(New LineEdgeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(LineEdgeBuilder.TableName, NameOf(ILineEdge).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ILineEdge
        Return New LineEdgeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ILineEdge
        Dim destination As ILineEdge = Me.CreateNew
        LineEdgeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ILineEdge)
        LineEdgeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ILineEdge)
        ' first make the copy to notify of any property change.
        LineEdgeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The LineEdge table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of ILineEdge)(key), connection.Get(Of LineEdgeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Title)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="title">      The LineEdge title. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal title As String) As Boolean
        Me.ClearStore()
        Dim nub As LineEdgeNub = LineEdgeEntity.FetchNubs(connection, title).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 5/16/2020. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ILineEdge) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If LineEdgeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Title) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Shared Function StoreEntity(ByVal connection As System.Data.IDbConnection, ByVal entity As ILineEdge) As Boolean
        Return New LineEdgeEntity().Upsert(connection, entity)
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of ILineEdge)(New LineEdgeNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the LineEdge entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The LineEdge entities. </value>
    Public Shared ReadOnly Property LineEdges As IEnumerable(Of LineEdgeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of LineEdgeEntity)
        Return If(usingNativeTracking, LineEdgeEntity.Populate(connection.GetAll(Of ILineEdge)), LineEdgeEntity.Populate(connection.GetAll(Of LineEdgeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        LineEdgeEntity._LineEdges = LineEdgeEntity.FetchAllEntities(connection, True)
        Return If(LineEdgeEntity.LineEdges?.Any, LineEdgeEntity.LineEdges.Count, 0)
    End Function

    ''' <summary> Dictionary of line edge titles. </summary>
    Private Shared _LineEdgeTitleDictionary As IDictionary(Of String, LineEdgeEntity)

    ''' <summary> Line edge title dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of String, LineEdgeEntity) </returns>
    Public Shared Function LineEdgeTitleDictionary() As IDictionary(Of String, LineEdgeEntity)
        If Not (LineEdgeEntity._LineEdgeTitleDictionary?.Any).GetValueOrDefault(False) Then
            LineEdgeEntity._LineEdgeTitleDictionary = New Dictionary(Of String, LineEdgeEntity)
            For Each entity As LineEdgeEntity In LineEdgeEntity.LineEdges
                LineEdgeEntity._LineEdgeTitleDictionary.Add(entity.Title, entity)
            Next
        End If
        Return LineEdgeEntity._LineEdgeTitleDictionary
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The entity nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of LineEdgeNub)) As IEnumerable(Of LineEdgeEntity)
        Dim l As New List(Of LineEdgeEntity)
        If nubs?.Any Then
            For Each nub As LineEdgeNub In nubs
                l.Add(New LineEdgeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The entity interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of ILineEdge)) As IEnumerable(Of LineEdgeEntity)
        Dim l As New List(Of LineEdgeEntity)
        If interfaces?.Any Then
            Dim nub As New LineEdgeNub
            For Each iFace As ILineEdge In interfaces
                nub.CopyFrom(iFace)
                l.Add(New LineEdgeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="title">      The LineEdge title. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal title As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{LineEdgeBuilder.TableName}] WHERE {NameOf(LineEdgeNub.Title)} = @title", New With {title})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="title">      The LineEdge title. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal title As String) As IEnumerable(Of LineEdgeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LineEdgeBuilder.TableName}] WHERE {NameOf(LineEdgeNub.Title)} = @title", New With {title})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LineEdgeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the LineEdge exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="title">      The LineEdge title. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal title As String) As Boolean
        Return 1 = LineEdgeEntity.CountEntities(connection, title)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the line edge. </summary>
    ''' <value> Identifies the line edge. </value>
    Public Property Id As Integer Implements ILineEdge.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the title. </summary>
    ''' <value> The title. </value>
    Public Property Title As String Implements ILineEdge.Title
        Get
            Return Me.ICache.Title
        End Get
        Set(value As String)
            If Not String.Equals(Me.Title, value) Then
                Me.ICache.Title = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets a list of source channels. </summary>
    ''' <value> A List of source channels. </value>
    Public Property SourceChannelList As String Implements ILineEdge.SourceChannelList
        Get
            Return Me.ICache.SourceChannelList
        End Get
        Set(value As String)
            If Not String.Equals(Me.SourceChannelList, value) Then
                Me.ICache.SourceChannelList = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets a list of measure channels. </summary>
    ''' <value> A List of measure channels. </value>
    Public Property MeasureChannelList As String Implements ILineEdge.MeasureChannelList
        Get
            Return Me.ICache.MeasureChannelList
        End Get
        Set(value As String)
            If Not String.Equals(Me.MeasureChannelList, value) Then
                Me.ICache.MeasureChannelList = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

End Class
