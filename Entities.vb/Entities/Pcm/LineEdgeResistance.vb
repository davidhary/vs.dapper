Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions

''' <summary>
''' Interface for the LineEdgeResistance nub and entity. Includes the fields as kept in the data
''' table. Allows tracking of property changes.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface ILineEdgeResistance

    ''' <summary> Gets the identifier of the Line edge resistance. </summary>
    ''' <value> Identifies the Line edge resistance. </value>
    <Key>
    Property AutoId As Integer

    ''' <summary> Gets the identifier of the <see cref="StructureEntity"/>. </summary>
    ''' <value> Identifies the <see cref="StructureEntity"/>. </value>
    Property StructureAutoId As Integer

    ''' <summary> Gets the identifier of the Line edge resistance. </summary>
    ''' <value> Identifies the Line edge resistance. </value>
    Property LineEdgeId As Integer

    ''' <summary> Gets the voltage. </summary>
    ''' <value> The voltage. </value>
    Property Voltage As Double?

    ''' <summary> Gets the current. </summary>
    ''' <value> The current. </value>
    Property Current As Double?

End Interface

''' <summary> The Line edge resistance builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class LineEdgeResistanceBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(LineEdgeResistanceBuilder._TableName) Then
                LineEdgeResistanceBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(LineEdgeResistanceNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return LineEdgeResistanceBuilder._TableName
        End Get
    End Property

    ''' <summary> Gets the name of the unique index. </summary>
    ''' <value> The name of the unique index. </value>
    Private Shared ReadOnly Property UniqueIndexName As String
        Get
            Return $"UQ_{LineEdgeResistanceBuilder.TableName}_{NameOf(LineEdgeResistanceNub.StructureAutoId)}_{NameOf(LineEdgeResistanceNub.LineEdgeId)}"
        End Get
    End Property

    ''' <summary> Zero-based index of the using unique. </summary>
    Private Shared _UsingUniqueIndex As Boolean?

    ''' <summary> Indicates if the entity uses a unique Title. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function UsingUniqueTitle(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not LineEdgeResistanceBuilder._UsingUniqueIndex.HasValue Then
            LineEdgeResistanceBuilder._UsingUniqueIndex = connection.IndexExists(LineEdgeResistanceBuilder.UniqueIndexName)
        End If
        Return LineEdgeResistanceBuilder._UsingUniqueIndex.Value
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{LineEdgeResistanceBuilder.TableName}] (
[{NameOf(LineEdgeResistanceNub.AutoId)}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
[{NameOf(LineEdgeResistanceNub.StructureAutoId)}] integer NOT NULL, 
[{NameOf(LineEdgeResistanceNub.LineEdgeId)}] integer NOT NULL, 
[{NameOf(LineEdgeResistanceNub.Voltage)}] float, 
[{NameOf(LineEdgeResistanceNub.Current)}] float, 
FOREIGN KEY ([{NameOf(LineEdgeResistanceNub.StructureAutoId)}]) REFERENCES [{StructureBuilder.TableName}] ([{NameOf(StructureNub.AutoId)}])
		ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY ([{NameOf(LineEdgeResistanceNub.LineEdgeId)}]) REFERENCES [{LineEdgeBuilder.TableName}] ([{NameOf(LineEdgeNub.Id)}])
		ON UPDATE CASCADE ON DELETE CASCADE);
CREATE UNIQUE INDEX [{LineEdgeResistanceBuilder.UniqueIndexName}] ON [{LineEdgeResistanceBuilder.TableName}] ([{NameOf(LineEdgeResistanceNub.StructureAutoId)}], [{NameOf(LineEdgeResistanceNub.LineEdgeId)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return LineEdgeResistanceBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{LineEdgeResistanceBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{LineEdgeResistanceBuilder.TableName}](
	[{NameOf(LineEdgeResistanceNub.AutoId)}] [int] IDENTITY(1,1) NOT NULL,
	[{NameOf(LineEdgeResistanceNub.StructureAutoId)}] [int] NOT NULL,
	[{NameOf(LineEdgeResistanceNub.LineEdgeId)}] [int] NOT NULL,
	[{NameOf(LineEdgeResistanceNub.Voltage)}] [float] NULL,
	[{NameOf(LineEdgeResistanceNub.Current)}] [float] NULL,
 CONSTRAINT [PK_{LineEdgeResistanceBuilder.TableName}] PRIMARY KEY CLUSTERED ([{NameOf(LineEdgeResistanceNub.AutoId)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END;

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{LineEdgeResistanceBuilder.TableName}]') AND name = N'{LineEdgeResistanceBuilder.UniqueIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{LineEdgeResistanceBuilder.UniqueIndexName}] ON [dbo].[{LineEdgeResistanceBuilder.TableName}] ([{NameOf(LineEdgeResistanceNub.StructureAutoId)}] ASC, [{NameOf(LineEdgeResistanceNub.LineEdgeId)}] ASC) 
 WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY];

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{LineEdgeResistanceBuilder.TableName}_{LineEdgeBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{LineEdgeResistanceBuilder.TableName}]'))
    ALTER TABLE [dbo].[{LineEdgeResistanceBuilder.TableName}] WITH CHECK ADD  CONSTRAINT [FK_{LineEdgeResistanceBuilder.TableName}_{LineEdgeBuilder.TableName}] FOREIGN KEY([{NameOf(LineEdgeResistanceNub.LineEdgeId)}])
    REFERENCES [dbo].[{LineEdgeBuilder.TableName}] ([{NameOf(LineEdgeNub.Id)}])
    ON UPDATE CASCADE ON DELETE CASCADE; 

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{LineEdgeResistanceBuilder.TableName}_{LineEdgeBuilder.TableName}') AND parent_object_id = OBJECT_ID(N'[dbo].[{LineEdgeResistanceBuilder.TableName}]'))
    ALTER TABLE [dbo].[{LineEdgeResistanceBuilder.TableName}] CHECK CONSTRAINT [FK_{LineEdgeResistanceBuilder.TableName}_{LineEdgeBuilder.TableName}]; 

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{LineEdgeResistanceBuilder.TableName}_{StructureBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{LineEdgeResistanceBuilder.TableName}]'))
ALTER TABLE [dbo].[{LineEdgeResistanceBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{LineEdgeResistanceBuilder.TableName}_{StructureBuilder.TableName}] FOREIGN KEY([{NameOf(LineEdgeResistanceNub.StructureAutoId)}])
REFERENCES [dbo].[{StructureBuilder.TableName}] ([{NameOf(StructureNub.AutoId)}])
ON UPDATE CASCADE ON DELETE CASCADE;

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{LineEdgeResistanceBuilder.TableName}_{StructureBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{LineEdgeResistanceBuilder.TableName}]'))
ALTER TABLE [dbo].[{LineEdgeResistanceBuilder.TableName}] CHECK CONSTRAINT [FK_{LineEdgeResistanceBuilder.TableName}_{StructureBuilder.TableName}]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return LineEdgeResistanceBuilder.TableName
    End Function

End Class

''' <summary>
''' Implements the LineEdgeResistance table <see cref="ILineEdgeResistance">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("LineEdgeResistance")>
Public Class LineEdgeResistanceNub
    Inherits EntityNubBase(Of ILineEdgeResistance)
    Implements ILineEdgeResistance

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ILineEdgeResistance
        Return New LineEdgeResistanceNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ILineEdgeResistance
        Dim destination As ILineEdgeResistance = Me.CreateNew
        LineEdgeResistanceNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ILineEdgeResistance)
        LineEdgeResistanceNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As ILineEdgeResistance, ByVal destination As ILineEdgeResistance)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.LineEdgeId = source.LineEdgeId
        destination.AutoId = source.AutoId
        destination.Current = source.Current
        destination.StructureAutoId = source.StructureAutoId
        destination.Voltage = source.Voltage
    End Sub

#End Region

#Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, ILineEdgeResistance))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As ILineEdgeResistance) As Boolean ' Implements IEquatable(Of ILineEdgeResistance).Equals
        Return other IsNot Nothing AndAlso LineEdgeResistanceNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As ILineEdgeResistance, ByVal right As ILineEdgeResistance) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.LineEdgeId, right.LineEdgeId)
            result = result AndAlso Integer.Equals(left.AutoId, right.AutoId)
            result = result AndAlso Double?.Equals(left.Current, right.Current)
            result = result AndAlso Integer.Equals(left.StructureAutoId, right.StructureAutoId)
            result = result AndAlso Double?.Equals(left.Voltage, right.Voltage)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.LineEdgeId Xor Me.AutoId Xor Me.Current.GetHashCode() Xor Me.StructureAutoId Xor Me.Voltage.GetHashCode()
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Line edge resistance. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the Line edge resistance. </value>
    <Key>
    Public Property AutoId As Integer Implements ILineEdgeResistance.AutoId

    ''' <summary> Gets or sets the identifier of the <see cref="StructureEntity"/>. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the <see cref="StructureEntity"/>. </value>
    Public Property StructureAutoId As Integer Implements ILineEdgeResistance.StructureAutoId

    ''' <summary> Gets or sets the identifier of the Line edge resistance. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the Line edge resistance. </value>
    Public Property LineEdgeId As Integer Implements ILineEdgeResistance.LineEdgeId

    ''' <summary> Gets or sets the voltage. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The voltage. </value>
    Public Property Voltage As Double? Implements ILineEdgeResistance.Voltage

    ''' <summary> Gets or sets the current. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The current. </value>
    Public Property Current As Double? Implements ILineEdgeResistance.Current

#End Region

End Class

''' <summary>
''' The LineEdgeResistance Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class LineEdgeResistanceEntity
    Inherits EntityBase(Of ILineEdgeResistance, LineEdgeResistanceNub)
    Implements ILineEdgeResistance

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New LineEdgeResistanceNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As ILineEdgeResistance)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ILineEdgeResistance, ByVal store As ILineEdgeResistance)
        MyBase.New(New LineEdgeResistanceNub, cache, store)
        Me.UsingNativeTracking = String.Equals(LineEdgeResistanceBuilder.TableName, NameOf(ILineEdgeResistance).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ILineEdgeResistance
        Return New LineEdgeResistanceNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ILineEdgeResistance
        Dim destination As ILineEdgeResistance = Me.CreateNew
        LineEdgeResistanceNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ILineEdgeResistance)
        LineEdgeResistanceNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ILineEdgeResistance)
        ' first make the copy to notify of any property change.
        LineEdgeResistanceNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The LineEdgeResistance table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of ILineEdgeResistance)(key), connection.Get(Of LineEdgeResistanceNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.StructureAutoId, Me.LineEdgeId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="lineEdgeId">      Identifies the Line edge resistance. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal lineEdgeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As LineEdgeResistanceNub = LineEdgeResistanceEntity.FetchNubs(connection, structureAutoId, lineEdgeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ILineEdgeResistance) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If LineEdgeResistanceEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.StructureAutoId, entity.LineEdgeId) Then
            ' update the existing record from the specified entity.
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Shared Function StoreEntity(ByVal connection As System.Data.IDbConnection, ByVal entity As ILineEdgeResistance) As Boolean
        Return New LineEdgeResistanceEntity().Upsert(connection, entity)
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of ILineEdgeResistance)(New LineEdgeResistanceNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the LineEdgeResistance entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The LineEdgeResistance entities. </value>
    Public ReadOnly Property LineEdgeResistances As IEnumerable(Of LineEdgeResistanceEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of LineEdgeResistanceEntity)
        Return If(usingNativeTracking, LineEdgeResistanceEntity.Populate(connection.GetAll(Of ILineEdgeResistance)), LineEdgeResistanceEntity.Populate(connection.GetAll(Of LineEdgeResistanceNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._LineEdgeResistances = LineEdgeResistanceEntity.FetchAllEntities(connection, True)
        Me.NotifyPropertyChanged(NameOf(LineEdgeResistanceEntity.LineEdgeResistances))
        Return If(Me.LineEdgeResistances?.Any, Me.LineEdgeResistances.Count, 0)
    End Function

    ''' <summary> Fetches entities in this collection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of LineEdgeResistanceEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{LineEdgeResistanceBuilder.TableName}]
WHERE {NameOf(LineEdgeResistanceNub.StructureAutoId)} = @Id", New With {Key .Id = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LineEdgeResistanceEntity.Populate(connection.Query(Of LineEdgeResistanceNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The entity nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of LineEdgeResistanceNub)) As IEnumerable(Of LineEdgeResistanceEntity)
        Dim l As New List(Of LineEdgeResistanceEntity)
        If nubs?.Any Then
            For Each nub As LineEdgeResistanceNub In nubs
                l.Add(New LineEdgeResistanceEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The entity interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of ILineEdgeResistance)) As IEnumerable(Of LineEdgeResistanceEntity)
        Dim l As New List(Of LineEdgeResistanceEntity)
        If interfaces?.Any Then
            Dim nub As New LineEdgeResistanceNub
            For Each iFace As ILineEdgeResistance In interfaces
                nub.CopyFrom(iFace)
                l.Add(New LineEdgeResistanceEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="lineEdgeId">      Identifies the Line edge resistance. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal lineEdgeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LineEdgeResistanceBuilder.TableName}] /**where**/")
        sqlBuilder.Where("StructureAutoId = @StructureAutoId", New With {structureAutoId})
        sqlBuilder.Where("LineEdgeId = @LineEdgeId", New With {lineEdgeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="lineEdgeId">      Identifies the Line edge resistance. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal lineEdgeId As Integer) As IEnumerable(Of LineEdgeResistanceNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LineEdgeResistanceBuilder.TableName}] /**where**/")
        sqlBuilder.Where("StructureAutoId = @StructureAutoId", New With {structureAutoId})
        sqlBuilder.Where("LineEdgeId = @LineEdgeId", New With {lineEdgeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LineEdgeResistanceNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Line Edge Resistance record exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="lineEdgeId">      Identifies the Line edge resistance. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal lineEdgeId As Integer) As Boolean
        Return 1 = LineEdgeResistanceEntity.CountEntities(connection, structureAutoId, lineEdgeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Line edge entity. </summary>
    ''' <value> The Line edge entity. </value>
    Public ReadOnly Property LineEdgeEntity As LineEdgeEntity

    ''' <summary> Fetches Line edge Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Line edge. </returns>
    Public Function FetchLineEdgeEntity(ByVal connection As System.Data.IDbConnection) As LineEdgeEntity
        Me._LineEdgeEntity = New LineEdgeEntity()
        Me.LineEdgeEntity.FetchUsingKey(connection, Me.LineEdgeId)
        Return Me.LineEdgeEntity
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Line edge resistance. </summary>
    ''' <value> Identifies the Line edge resistance. </value>
    Public Property AutoId As Integer Implements ILineEdgeResistance.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="StructureEntity"/>. </summary>
    ''' <value> Identifies the <see cref="StructureEntity"/>. </value>
    Public Property StructureAutoId As Integer Implements ILineEdgeResistance.StructureAutoId
        Get
            Return Me.ICache.StructureAutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.StructureAutoId, value) Then
                Me.ICache.StructureAutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Line edge resistance. </summary>
    ''' <value> Identifies the Line edge resistance. </value>
    Public Property LineEdgeId As Integer Implements ILineEdgeResistance.LineEdgeId
        Get
            Return Me.ICache.LineEdgeId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.LineEdgeId, value) Then
                Me.ICache.LineEdgeId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the voltage. </summary>
    ''' <value> The voltage. </value>
    Public Property Voltage As Double? Implements ILineEdgeResistance.Voltage
        Get
            Return Me.ICache.Voltage
        End Get
        Set(value As Double?)
            If Not Double?.Equals(Me.Voltage, value) Then
                Me.ICache.Voltage = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the current. </summary>
    ''' <value> The current. </value>
    Public Property Current As Double? Implements ILineEdgeResistance.Current
        Get
            Return Me.ICache.Current
        End Get
        Set(value As Double?)
            If Not Double?.Equals(Me.Current, value) Then
                Me.ICache.Current = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

End Class
