Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A PCM element nominal Real(Double)-Value trait builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class PcmElementNomTraitBuilder
    Inherits OneToManyRealBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PcmElementNomTraitBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PcmElementNomTraitBuilder._TableName) Then
                PcmElementNomTraitBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PcmElementNomTraitNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = PcmElementBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(PcmElementNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = PcmElementNomTraitTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(PcmNomTraitTypeNub.Id)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(PcmElementNomTraitEntity.PcmElementAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(PcmElementNomTraitEntity.PcmNomTraitTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName As String = NameOf(PcmElementNomTraitEntity.Amount)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PcmElementNomTraitBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PcmElementNomTraitBuilder
        If PcmElementNomTraitBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PcmElementNomTraitBuilder._Instance = New PcmElementNomTraitBuilder()
            End SyncLock
        End If
        Return PcmElementNomTraitBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="PcmElementNomTraitEntity"/>
''' <see cref="IOneToManyReal">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("PcmElementNomTrait")>
Public Class PcmElementNomTraitNub
    Inherits OneToManyRealNub
    Implements IOneToManyReal

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToManyReal
        Return New PcmElementNomTraitNub
    End Function

End Class

''' <summary>
''' The <see cref="PcmElementNomTraitEntity"/>. Stores Real(Double)-Valued Pcmelement Nominal
''' Traits.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class PcmElementNomTraitEntity
    Inherits EntityBase(Of IOneToManyReal, PcmElementNomTraitNub)
    Implements IOneToManyReal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New PcmElementNomTraitNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="PcmElementEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IOneToManyReal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToManyReal, ByVal store As IOneToManyReal)
        MyBase.New(New PcmElementNomTraitNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PcmElementNomTraitBuilder.TableName, NameOf(IOneToManyReal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToManyReal
        Return New PcmElementNomTraitNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOneToManyReal
        Dim destination As IOneToManyReal = Me.CreateNew
        PcmElementNomTraitNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="PcmElementNomTraitEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToManyReal)
        PcmElementNomTraitNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="PcmElementEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToManyReal)
        ' first make the copy to notify of any property change.
        PcmElementNomTraitNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="elementAutoId">        Identifies the
    '''                                     <see cref="PcmElementEntity"/>. </param>
    ''' <param name="pcmElementNomTraitId"> Identifies the
    '''                                             <see cref="PcmElementEntity"/> value type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal pcmElementNomTraitId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PcmElementNomTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PcmElementNomTraitNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(PcmElementNomTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = pcmElementNomTraitId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of PcmElementNomTraitNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/15/2020. </remarks>
    ''' <param name="connection">            The connection. </param>
    ''' <param name="elementAutoId">         Identifies the
    '''                                      <see cref="PcmElementEntity"/>. </param>
    ''' <param name="pcmElementTraitTypeId"> Identifies the
    '''                                             <see cref="PcmElementEntity"/> value type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal pcmElementTraitTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As PcmElementNomTraitNub = PcmElementNomTraitEntity.FetchEntities(connection, elementAutoId, pcmElementTraitTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToManyReal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PcmElementNomTraitEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">            The connection. </param>
    ''' <param name="elementAutoId">         Identifies the
    '''                                      <see cref="PcmElementEntity"/>. </param>
    ''' <param name="pcmElementTraitTypeId"> Type of the <see cref="PcmElementEntity"/> value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal pcmElementTraitTypeId As Integer) As Boolean
        Return connection.Delete(Of PcmElementNomTraitNub)(New PcmElementNomTraitNub With {.PrimaryId = elementAutoId, .SecondaryId = pcmElementTraitTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary>
    ''' Gets or sets the <see cref="PcmElementEntity"/> Real(Double)-Value Trait entities.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> the <see cref="PcmElementEntity"/> Real(Double)-Value Trait entities. </value>
    Public ReadOnly Property PcmElementNomTraits As IEnumerable(Of PcmElementNomTraitEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PcmElementNomTraitEntity)
        Return If(usingNativeTracking, PcmElementNomTraitEntity.Populate(connection.GetAll(Of IOneToManyReal)),
                                       PcmElementNomTraitEntity.Populate(connection.GetAll(Of PcmElementNomTraitNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._PcmElementNomTraits = PcmElementNomTraitEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(PcmElementNomTraitEntity.PcmElementNomTraits))
        Return If(Me.PcmElementNomTraits?.Any, Me.PcmElementNomTraits.Count, 0)
    End Function

    ''' <summary> Count entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="PcmElementEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{PcmElementNomTraitBuilder.TableName}] WHERE {NameOf(PcmElementNomTraitNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="PcmElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of PcmElementNomTraitEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PcmElementNomTraitBuilder.TableName}] WHERE {NameOf(PcmElementNomTraitNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return PcmElementNomTraitEntity.Populate(connection.Query(Of PcmElementNomTraitNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Fetches Pcmelement Nominal Traits by PcmElement number and PCM element Nominal Trait PCM
    ''' element nominal Real(Double)-Value Trait Type;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="PcmElementEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of PcmElementNomTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PcmElementNomTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PcmElementNomTraitEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PcmElementNomTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of <see cref="PcmElementNomTraitEntity"/>'s. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> the <see cref="PcmElementNomTraitNub"/>'s. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PcmElementNomTraitNub)) As IEnumerable(Of PcmElementNomTraitEntity)
        Dim l As New List(Of PcmElementNomTraitEntity)
        If nubs?.Any Then
            For Each nub As PcmElementNomTraitNub In nubs
                l.Add(New PcmElementNomTraitEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of <see cref="PcmElementNomTraitEntity"/>'s. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> the <see cref="PcmElementEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToManyReal)) As IEnumerable(Of PcmElementNomTraitEntity)
        Dim l As New List(Of PcmElementNomTraitEntity)
        If interfaces?.Any Then
            Dim nub As New PcmElementNomTraitNub
            For Each iFace As IOneToManyReal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PcmElementNomTraitEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Pcmelement Nominal Traits; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">            The connection. </param>
    ''' <param name="elementAutoId">         Identifies the <see cref="PcmElementEntity"/>. </param>
    ''' <param name="pcmElementTraitTypeId"> Type of the <see cref="PcmElementEntity"/> value. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal pcmElementTraitTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PcmElementNomTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PcmElementNomTraitNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(PcmElementNomTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = pcmElementTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Pcmelement Nominal Traits by PcmElement number and PCM element Nominal Trait PCM
    ''' Element Real(Double)-Value Trait Type;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">            The connection. </param>
    ''' <param name="elementAutoId">         Identifies the <see cref="PcmElementEntity"/>. </param>
    ''' <param name="pcmElementTraitTypeId"> Identifies the <see cref="PcmElementEntity"/> value type. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal pcmElementTraitTypeId As Integer) As IEnumerable(Of PcmElementNomTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PcmElementNomTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PcmElementNomTraitNub.PrimaryId)} = @primaryId", New With {.primaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(PcmElementNomTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = pcmElementTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PcmElementNomTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="PcmElementEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">            The connection. </param>
    ''' <param name="elementAutoId">         Identifies the <see cref="PcmElementEntity"/>. </param>
    ''' <param name="pcmElementTraitTypeId"> The PCM element nominal Real(Double)-Value Trait Type. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal pcmElementTraitTypeId As Integer) As Boolean
        Return 1 = PcmElementNomTraitEntity.CountEntities(connection, elementAutoId, pcmElementTraitTypeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count values associated with this Pcm element Nominal Trait. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of specification values. </returns>
    Public Function CountPcmElementNomTraits(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PcmElementNomTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PcmElementNomTraitNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches PCM element nominal Real(Double)-Value trait values by PcmElement auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchPcmElementNomTraits(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of PcmElementNomTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PcmElementNomTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PcmElementNomTraitNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PcmElementNomTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets or sets the <see cref="PcmElementEntity"/>. </summary>
    ''' <value> the <see cref="PcmElementEntity"/>. </value>
    Public ReadOnly Property PcmElementEntity As PcmElementEntity

    ''' <summary> Fetches a <see cref="PcmElementEntity"/> . </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchPcmElementEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._PcmElementEntity = New PcmElementEntity()
        Return Me.PcmElementEntity.FetchUsingKey(connection, Me.PcmElementAutoId)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.PcmNomTraitTypeEntity"/> . </summary>
    ''' <value> the <see cref="Entities.PcmNomTraitTypeEntity"/> . </value>
    Public ReadOnly Property PcmNomTraitTypeEntity As PcmNomTraitTypeEntity

    ''' <summary> Fetches a <see cref="Entities.PcmNomTraitTypeEntity"/>. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchPcmNomTraitTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._PcmNomTraitTypeEntity = New PcmNomTraitTypeEntity()
        Return Me.PcmNomTraitTypeEntity.FetchUsingKey(connection, Me.PcmNomTraitTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToManyReal.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PcmElementNomTraitEntity.PcmElementAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="PcmElementEntity"/> record. </summary>
    ''' <value> Identifies the <see cref="PcmElementEntity"/> record. </value>
    Public Property PcmElementAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToManyReal.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PcmElementNomTraitEntity.PcmNomTraitTypeId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.PcmNomTraitTypeEntity"/>.
    ''' </summary>
    ''' <value> Identifies the PCM Nominal Trait type. </value>
    Public Property PcmNomTraitTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets a Real(Double)-value assigned to the <see cref="PcmElementEntity"/> for the
    ''' specific <see cref="Entities.PcmNomTraitTypeEntity"/>.
    ''' </summary>
    ''' <value>
    ''' The Real(Double)-value assigned to the <see cref="PcmElementEntity"/> for the specific
    ''' <see cref="Entities.PcmNomTraitTypeEntity"/>.
    ''' </value>
    Public Property Amount As Double Implements IOneToManyReal.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(ByVal value As Double)
            If Not Double.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary>
''' Collection of <see cref="PcmElementNomTraitEntity"/> Real(Double)-value_ entities.
''' </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class PcmElementNomTraitEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToManyReal, PcmElementNomTraitNub, PcmElementNomTraitEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As PcmElementNomTraitEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As PcmElementNomTraitEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all pcmPcmElements from the
    ''' <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of PcmElementNomTraitEntity))
        If entities?.Any Then
            For Each entity As PcmElementNomTraitEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return PcmElementNomTraitBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary>
''' Collection of PcmElement-Unique Pcm element Nominal Trait Real Entities unique to the
''' pcmPcmElement.
''' </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class PcmElementUniqueTraitEntityCollection
    Inherits PcmElementNomTraitEntityCollection

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="pcmPcmElementAutoId"> Identifier for the PCM element automatic. </param>
    Public Sub New(ByVal pcmPcmElementAutoId As Integer)
        Me.New
        Me.PcmElementAutoId = pcmPcmElementAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As PcmElementNomTraitEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.PcmNomTraitTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.PcmNomTraitTypeId)
        Me.NotifyPropertyChanged(PcmNomTraitTypeEntity.EntityLookupDictionary(entity.PcmNomTraitTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all pcmPcmElements from the
    ''' <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains akey. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="pcmNomTraitTypeId"> Identifies the <see cref="PcmNomTraitType"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal pcmNomTraitTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(pcmNomTraitTypeId)
    End Function

    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="pcmNomTraitTypeId"> Identifies the <see cref="PcmNomTraitType"/>. </param>
    ''' <returns> A PcmElementNomTraitEntity. </returns>
    Public Function Entity(ByVal pcmNomTraitTypeId As Integer) As PcmElementNomTraitEntity
        Return If(Me.ContainsKey(pcmNomTraitTypeId), Me(Me._PrimaryKeyDictionary(pcmNomTraitTypeId)), New PcmElementNomTraitEntity)
    End Function

    ''' <summary> Gets the value for the specified Trait Type identifier. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="pcmNomTraitTypeId"> Identifies the <see cref="PcmNomTraitType"/>. </param>
    ''' <returns> A Double? </returns>
    Public Function Getter(ByVal pcmNomTraitTypeId As Integer) As Double?
        Return If(Me.ContainsKey(pcmNomTraitTypeId), Me(Me._PrimaryKeyDictionary(pcmNomTraitTypeId)).Amount, New Double?)
    End Function

    ''' <summary> Set the specified PcmElement value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="pcmNomTraitTypeId"> Identifies the <see cref="PcmNomTraitType"/>. </param>
    ''' <param name="value">             The value. </param>
    Public Sub Setter(ByVal pcmNomTraitTypeId As Integer, ByVal value As Double)
        If Me.ContainsKey(pcmNomTraitTypeId) Then
            Me(Me._PrimaryKeyDictionary(pcmNomTraitTypeId)).Amount = value
        Else
            Me.Add(New PcmElementNomTraitEntity With {.PcmElementAutoId = Me.PcmElementAutoId, .PcmNomTraitTypeId = pcmNomTraitTypeId, .Amount = value})
        End If
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="PcmElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="PcmElementEntity"/>. </value>
    Public ReadOnly Property PcmElementAutoId As Integer

End Class

