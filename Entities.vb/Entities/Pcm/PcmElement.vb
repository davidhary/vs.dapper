Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Dapper.Entities.ConnectionExtensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entity.EntityExtensions

''' <summary>
''' A builder for a PC Element based on the <see cref="IKeyForeignLabel">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class PcmElementBuilder
    Inherits KeyForeignLabelBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PcmElementBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PcmElementBuilder._TableName) Then
                PcmElementBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PcmElementNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the foreign table . </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the foreign table. </value>
    Public Overrides Property ForeignTableName As String = PcmElementTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the foreign table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the foreign table key. </value>
    Public Overrides Property ForeignTableKeyName As String = NameOf(PcmElementTypeNub.Id)

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 50

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(PcmElementEntity.AutoId)

    ''' <summary> Gets or sets the name of the foreign identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName() As String = NameOf(PcmElementEntity.PcmElementTypeId)

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the label field. </value>
    Public Overrides Property LabelFieldName() As String = NameOf(PcmElementEntity.PcmElementLabel)

    ''' <summary>
    ''' Inserts or ignores the records described by the <see cref="PcmElementType"/> enumeration type.
    ''' </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of inserted or total existing records. </returns>
    Public Overloads Shared Function InsertIgnorePcmElementTypeValues(ByVal connection As System.Data.IDbConnection) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return PcmElementBuilder.InsertIgnoreTypeValues(connection, GetType(PcmElementType), New Integer() {CInt(PcmElementType.None)})
    End Function

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="type">       The enumeration type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of inserted or total existing records. </returns>
    Public Overloads Shared Function InsertIgnoreTypeValues(ByVal connection As System.Data.IDbConnection, ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection.InsertIgnoreNameDescriptionRecords(PcmElementTypeBuilder.TableName, GetType(IKeyForeignLabel).EnumerateEntityFieldNames(), type, excluded)
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PcmElementBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PcmElementBuilder
        If PcmElementBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PcmElementBuilder._Instance = New PcmElementBuilder()
            End SyncLock
        End If
        Return PcmElementBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Natural PcmElement table based on the
''' <see cref="IKeyForeignLabel">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("PcmElement")>
Public Class PcmElementNub
    Inherits KeyForeignLabelNub
    Implements IKeyForeignLabel

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IKeyForeignLabel
        Return New PcmElementNub
    End Function

End Class

''' <summary>
''' The Nominal PcmElement Entity based on the <see cref="IKeyForeignLabel">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class PcmElementEntity
    Inherits EntityBase(Of IKeyForeignLabel, PcmElementNub)
    Implements IKeyForeignLabel

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New PcmElementNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The PcmElement interface. </param>
    Public Sub New(ByVal value As IKeyForeignLabel)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyForeignLabel, ByVal store As IKeyForeignLabel)
        MyBase.New(New PcmElementNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PcmElementBuilder.TableName, NameOf(IKeyForeignLabel).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IKeyForeignLabel
        Return New PcmElementNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IKeyForeignLabel
        Dim destination As IKeyForeignLabel = Me.CreateNew
        PcmElementNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IKeyForeignLabel)
        PcmElementNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The PcmElement interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyForeignLabel)
        ' first make the copy to notify of any property change.
        PcmElementNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The PcmElement table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyForeignLabel)(key), connection.Get(Of PcmElementNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="pcmElementLabel"> The PcmElement number. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal pcmElementLabel As String) As Boolean
        Me.ClearStore()
        Dim nub As PcmElementNub = PcmElementEntity.FetchNubs(connection, pcmElementLabel).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.FetchUsingKey(connection, nub.AutoId)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyForeignLabel) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PcmElementEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(UniqueIndexOptions.Label = (PcmElementBuilder.Get.QueryUniqueIndexOptions(connection) And UniqueIndexOptions.Amount),
                                        Me.FetchUsingUniqueIndex(connection, entity.Label),
                                        Me.FetchUsingKey(connection, entity.AutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of PcmElementNub)(New PcmElementNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the PcmElement entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The PcmElement entities. </value>
    Public ReadOnly Property PcmElements As IEnumerable(Of PcmElementEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PcmElementEntity)
        Return If(usingNativeTracking, PcmElementEntity.Populate(connection.GetAll(Of IKeyForeignLabel)), PcmElementEntity.Populate(connection.GetAll(Of PcmElementNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._PcmElements = PcmElementEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(PcmElementEntity.PcmElements))
        Return If(Me.PcmElements?.Any, Me.PcmElements.Count, 0)
    End Function

    ''' <summary> Populates a list of PcmElement entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The PcmElement nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PcmElementNub)) As IEnumerable(Of PcmElementEntity)
        Dim l As New List(Of PcmElementEntity)
        If nubs?.Any Then
            For Each nub As PcmElementNub In nubs
                l.Add(New PcmElementEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of PcmElement entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The PcmElement interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyForeignLabel)) As IEnumerable(Of PcmElementEntity)
        Dim l As New List(Of PcmElementEntity)
        If interfaces?.Any Then
            Dim nub As New PcmElementNub
            For Each iFace As IKeyForeignLabel In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PcmElementEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="pcmElementLabel"> The PcmElement number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal pcmElementLabel As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PcmElementBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PcmElementNub.Label)} = @PcmElementLabel", New With {pcmElementLabel})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="pcmElementLabel"> The PcmElement number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal pcmElementLabel As String) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{PcmElementBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(PcmElementNub.Label)} IS NULL OR {NameOf(PcmElementNub.Label)} = @PcmElementLabel)")
        selectSql.Append("; ")
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {pcmElementLabel})
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="pcmElementLabel"> The PcmElement number. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal pcmElementLabel As String) As IEnumerable(Of PcmElementNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PcmElementBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PcmElementNub.Label)} = @PcmElementLabel", New With {pcmElementLabel})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PcmElementNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the PcmElement exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="pcmElementLabel"> The PcmElement number. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal pcmElementLabel As String) As Boolean
        Return 1 = PcmElementEntity.CountEntities(connection, pcmElementLabel)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="PcmElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="PcmElementEntity"/>. </value>
    Public Property AutoId As Integer Implements IKeyForeignLabel.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="PcmElementEntity"/> type. </summary>
    ''' <value> Identifies the <see cref="PcmElementEntity"/> type. </value>
    Public Property ForeignId As Integer Implements IKeyForeignLabel.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PcmElementEntity.PcmElementTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the PCM element type. </summary>
    ''' <value> Identifies the PCM element type. </value>
    Public Property PcmElementTypeId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(value As Integer)
            Me.ForeignId = value
        End Set
    End Property

    ''' <summary> Gets or sets the element Label. </summary>
    ''' <value> The Label. </value>
    Public Property Label As String Implements IKeyForeignLabel.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PcmElementEntity.PcmElementLabel))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the PcmElement label. </summary>
    ''' <value> The PcmElement label. </value>
    Public Property PcmElementLabel As String
        Get
            Return Me.Label
        End Get
        Set(value As String)
            Me.Label = value
        End Set
    End Property

#End Region

End Class

''' <summary> Collection of pcmElement entities. </summary>
''' <remarks> David, 5/18/2020. </remarks>
Public Class PcmElementEntityCollection
    Inherits EntityKeyedCollection(Of Integer, IKeyForeignLabel, PcmElementNub, PcmElementEntity)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified pcmElement.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The pcmElement from which to extract the key. </param>
    ''' <returns> The key for the specified pcmElement. </returns>
    Protected Overrides Function GetKeyForItem(ByVal item As PcmElementEntity) As Integer
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.AutoId
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As PcmElementEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all pcmElements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of PcmElementEntity))
        If entities?.Any Then
            For Each entity As PcmElementEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Throw New NotImplementedException()
    End Function

End Class

''' <summary> Collection of pcmElement entities uniquely associated with a part. </summary>
''' <remarks> David, 5/18/2020. </remarks>
Public Class PartUniquePcmElementEntityCollection
    Inherits PcmElementEntityCollection

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of Integer, String)
        Me._PrimaryKeyDictionary = New Dictionary(Of String, Integer)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <param name="partAutoId"> Identifies the <see cref="PcmElementEntity"/>. </param>
    Public Sub New(ByVal partAutoId As Integer)
        Me.New
        Me.PartAutoId = partAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of Integer, String)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of String, Integer)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As PcmElementEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.Label, entity.AutoId)
        Me._UniqueIndexDictionary.Add(entity.AutoId, entity.Label)
    End Sub

    ''' <summary>
    ''' Removes all pcmElements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Selects an pcmElement from the collection. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="pcmElementLabel"> The pcmElement label. </param>
    ''' <returns> An PcmElementEntity. </returns>
    Public Function PcmElement(ByVal pcmElementLabel As String) As PcmElementEntity
        Dim id As Integer = Me._PrimaryKeyDictionary(pcmElementLabel)
        Return If(Me.Contains(id), Me(id), New PcmElementEntity)
    End Function

    ''' <summary> PcmElement label. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="pcmElementAutoId"> Identifies the pcmElement. </param>
    ''' <returns> A String. </returns>
    Public Function PcmElementLabel(ByVal pcmElementAutoId As Integer) As String
        Return If(Me._UniqueIndexDictionary.ContainsKey(pcmElementAutoId), Me._UniqueIndexDictionary(pcmElementAutoId), String.Empty)
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="PcmElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="PcmElementEntity"/>. </value>
    Public ReadOnly Property PartAutoId As Integer

End Class

