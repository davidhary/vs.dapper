Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A PCM reading Type builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class PcmReadingTypeBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PcmReadingTypeBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PcmReadingTypeBuilder._TableName) Then
                PcmReadingTypeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PcmReadingTypeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Inserts or ignores the records described by the <see cref="PcmReadingType"/> enumeration type. </summary>
    ''' <param name="connection"> The connection. </param>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(PcmReadingType), New Integer() {CInt(PcmReadingType.None)})
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PcmReadingTypeBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PcmReadingTypeBuilder
        If PcmReadingTypeBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PcmReadingTypeBuilder._Instance = New PcmReadingTypeBuilder()
            End SyncLock
        End If
        Return PcmReadingTypeBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the PcmReadingType table based on the <see cref="INominal">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("PcmReadingType")>
Public Class PcmReadingTypeNub
    Inherits NominalNub
    Implements INominal

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As INominal
        Return New PcmReadingTypeNub
    End Function

End Class


''' <summary> The PcmReadingType Entity. Implements access to the database using Dapper. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class PcmReadingTypeEntity
    Inherits EntityBase(Of INominal, PcmReadingTypeNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New PcmReadingTypeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The PcmReadingType interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New PcmReadingTypeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PcmReadingTypeBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As INominal
        Return New PcmReadingTypeNub
    End Function

    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        PcmReadingTypeNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As INominal)
        PcmReadingTypeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The PcmReadingType interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        PcmReadingTypeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The PcmReadingType table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of PcmReadingTypeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The PCM Reading Type Label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As PcmReadingTypeNub = PcmReadingTypeEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PcmReadingTypeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of PcmReadingTypeNub)(New PcmReadingTypeNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the PcmReadingType entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The PcmReadingType entities. </value>
    Public Shared ReadOnly Property PcmReadingTypes As IEnumerable(Of PcmReadingTypeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PcmReadingTypeEntity)
        Return If(usingNativeTracking, PcmReadingTypeEntity.Populate(connection.GetAll(Of INominal)), PcmReadingTypeEntity.Populate(connection.GetAll(Of PcmReadingTypeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        PcmReadingTypeEntity._PcmReadingTypes = PcmReadingTypeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(PcmReadingTypeEntity.PcmReadingTypes?.Any, PcmReadingTypeEntity.PcmReadingTypes.Count, 0)
    End Function

    ''' <summary> Populates a list of PcmReadingType entities. </summary>
    ''' <param name="nubs"> The PcmReadingType nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PcmReadingTypeNub)) As IEnumerable(Of PcmReadingTypeEntity)
        Dim l As New List(Of PcmReadingTypeEntity)
        If nubs?.Any Then
            For Each nub As PcmReadingTypeNub In nubs
                l.Add(New PcmReadingTypeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of PcmReadingType entities. </summary>
    ''' <param name="interfaces"> The PcmReadingType interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of PcmReadingTypeEntity)
        Dim l As New List(Of PcmReadingTypeEntity)
        If interfaces?.Any Then
            Dim nub As New PcmReadingTypeNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PcmReadingTypeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, PcmReadingTypeEntity)

    ''' <summary> The PCM reading Type lookup dictionary. </summary>
    ''' <returns> A Dictionary(Of Integer, PcmReadingTypeEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, PcmReadingTypeEntity)
        If Not (PcmReadingTypeEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            PcmReadingTypeEntity._EntityLookupDictionary = New Dictionary(Of Integer, PcmReadingTypeEntity)
            For Each entity As PcmReadingTypeEntity In PcmReadingTypeEntity.PcmReadingTypes
                PcmReadingTypeEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return PcmReadingTypeEntity._EntityLookupDictionary
    End Function

    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (PcmReadingTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            PcmReadingTypeEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As PcmReadingTypeEntity In PcmReadingTypeEntity.PcmReadingTypes
                PcmReadingTypeEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return PcmReadingTypeEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (PcmReadingTypeEntity.PcmReadingTypes?.Any AndAlso
                PcmReadingTypeEntity._EntityLookupDictionary?.Any AndAlso
                PcmReadingTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The PCM reading Type label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{PcmReadingTypeNub.TableName}] 
        ' WHERE {NameOf(PcmReadingTypeNub.PlatformName)} = @PlatformName", New With {Key .PlatformName = PlatformName})
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{PcmReadingTypeBuilder.TableName}] WHERE {NameOf(PcmReadingTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The PCM reading Type label. </param>
    ''' <returns> An IPcmReadingType . </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of PcmReadingTypeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PcmReadingTypeBuilder.TableName}] WHERE {NameOf(PcmReadingTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PcmReadingTypeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the PcmReadingType exists. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The PCM Reading Type Label. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = PcmReadingTypeEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="PcmReadingTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="PcmReadingTypeEntity"/>. </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PcmReadingTypeEntity.PcmReadingType))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the PcmReadingType. </summary>
    ''' <value> The PcmReadingType. </value>
    Public Property PcmReadingType As PcmReadingType
        Get
            Return CType(Me.Id, PcmReadingType)
        End Get
        Set(ByVal value As PcmReadingType)
            Me.Id = value
        End Set
    End Property

    ''' <summary> Gets or sets the PCM reading Type label. </summary>
    ''' <value> The PCM reading Type label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the reading type. </summary>
    ''' <value> The PCM reading Type description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="pcmReadingTypeId"> The entity unique key. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As PcmReadingTypeEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection,
                                            ByVal pcmReadingTypeId As Integer) As (Success As Boolean, Details As String, Entity As PcmReadingTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New PcmReadingTypeEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching {NameOf(PcmReadingTypeEntity)} by {NameOf(PcmReadingTypeNub.Id)} of {pcmReadingTypeId}"
            If Not entity.FetchUsingKey(connection, pcmReadingTypeId) Then
                details = $"Failed {activity}"
                success = False
            End If
        Catch ex As Exception
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            success = False
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="pcmReadingTypeLabel"> The PCM reading Type label. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As PcmReadingTypeEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal pcmReadingTypeLabel As String) As (Success As Boolean, Details As String, Entity As PcmReadingTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New PcmReadingTypeEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching {NameOf(PcmReadingTypeEntity)} by {NameOf(PcmReadingTypeNub.Label)} of {pcmReadingTypeLabel}"
            If Not entity.FetchUsingUniqueIndex(connection, pcmReadingTypeLabel) Then
                details = $"Failed {activity}"
                success = False
            End If
        Catch ex As Exception
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            success = False
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of PcmReadingTypeEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of PcmReadingTypeEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New PcmReadingTypeEntity()
            activity = $"fetching all {NameOf(PcmReadingTypeEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> PcmReadingTypeEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(PcmReadingTypeEntity.EntityLookupDictionary)} count must equal {NameOf(PcmReadingTypeEntity.PcmReadingTypes)} count ")
            ElseIf elementCount <> PcmReadingTypeEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(PcmReadingTypeEntity.KeyLookupDictionary)} count must equal {NameOf(PcmReadingTypeEntity.PcmReadingTypes)} count ")
            End If
            Return (True, String.Empty, PcmReadingTypeEntity.PcmReadingTypes)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of PcmReadingTypeEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Values that represent reading types. </summary>
Public Enum PcmReadingType
    <System.ComponentModel.Description("PCM Reading Type")> None
    <System.ComponentModel.Description("Sheet Resistance")> SheetResistance
    <System.ComponentModel.Description("Line Width")> LineWidth
    <System.ComponentModel.Description("Line Width Deviation")> LineWidthDeviation
    <System.ComponentModel.Description("Split Line Width")> SplitLineWidth
    <System.ComponentModel.Description("Split Line Width Deviation")> SplitLineWidthDeviation
    <System.ComponentModel.Description("Line Pitch")> LinePitch
    <System.ComponentModel.Description("Test Current")> TestCurrent
End Enum

