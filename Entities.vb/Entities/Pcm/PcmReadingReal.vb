Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Nut reading Real-value builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class PcmReadingRealBuilder
    Inherits OneToManyRealBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PcmReadingRealBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PcmReadingRealBuilder._TableName) Then
                PcmReadingRealBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PcmReadingRealNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = NutBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(NutNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = PcmReadingTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(PcmReadingTypeNub.Id)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PcmReadingRealBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PcmReadingRealBuilder
        If PcmReadingRealBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PcmReadingRealBuilder._Instance = New PcmReadingRealBuilder()
            End SyncLock
        End If
        Return PcmReadingRealBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="PcmReadingRealEntity"/> <see cref="IOneToManyReal">interface</see>.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("PcmReadingReal")>
Public Class PcmReadingRealNub
    Inherits OneToManyRealNub
    Implements IOneToManyReal

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToManyReal
        Return New PcmReadingRealNub
    End Function

End Class

''' <summary> The <see cref="PcmReadingRealEntity"/>. Stores Real(Double)-value  readings. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class PcmReadingRealEntity
    Inherits EntityBase(Of IOneToManyReal, PcmReadingRealNub)
    Implements IOneToManyReal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New PcmReadingRealNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> the <see cref="Entities.NutEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IOneToManyReal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToManyReal, ByVal store As IOneToManyReal)
        MyBase.New(New PcmReadingRealNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PcmReadingRealBuilder.TableName, NameOf(IOneToManyReal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToManyReal
        Return New PcmReadingRealNub
    End Function

    Public Overrides Function CreateCopy() As IOneToManyReal
        Dim destination As IOneToManyReal = Me.CreateNew
        PcmReadingRealNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="PcmReadingRealEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToManyReal)
        PcmReadingRealNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> the <see cref="Entities.NutEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToManyReal)
        ' first make the copy to notify of any property change.
        PcmReadingRealNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="PcmReadingTypeId"> The identifier of the <see cref="Entities.NutEntity"/> value type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal PcmReadingTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PcmReadingRealBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(PcmReadingRealNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = nutAutoId})
        sqlBuilder.Where($"{NameOf(PcmReadingRealNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = PcmReadingTypeId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of PcmReadingRealNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/15/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="PcmReadingTypeId"> The identifier of the <see cref="Entities.NutEntity"/> value type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal PcmReadingTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As PcmReadingRealNub = PcmReadingRealEntity.FetchEntities(connection, nutAutoId, PcmReadingTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToManyReal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PcmReadingRealEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="PcmReadingTypeId"> Type of the <see cref="Entities.NutEntity"/> value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal PcmReadingTypeId As Integer) As Boolean
        Return connection.Delete(Of PcmReadingRealNub)(New PcmReadingRealNub With {.PrimaryId = nutAutoId, .SecondaryId = PcmReadingTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="Entities.NutEntity"/> PCM Reading Real(Double)-Value entities. </summary>
    ''' <value> the <see cref="Entities.NutEntity"/> PCM Reading Real(Double)-Value entities. </value>
    Public ReadOnly Property PcmReadingReals As IEnumerable(Of PcmReadingRealEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PcmReadingRealEntity)
        Return If(usingNativeTracking, PcmReadingRealEntity.Populate(connection.GetAll(Of IOneToManyReal)),
                                       PcmReadingRealEntity.Populate(connection.GetAll(Of PcmReadingRealNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._PcmReadingReals = PcmReadingRealEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(PcmReadingRealEntity.PcmReadingReals))
        Return If(Me.PcmReadingReals?.Any, Me.PcmReadingReals.Count, 0)
    End Function

    ''' <summary> Count Entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="nutAutoId"> Identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{PcmReadingRealBuilder.TableName}]
WHERE {NameOf(PcmReadingRealNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = nutAutoId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As IEnumerable(Of PcmReadingRealEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{PcmReadingRealBuilder.TableName}]
WHERE {NameOf(PcmReadingRealNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = nutAutoId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return PcmReadingRealEntity.Populate(connection.Query(Of PcmReadingRealNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Fetches PCM Readings bu Nut auto id.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As IEnumerable(Of PcmReadingRealNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PcmReadingRealBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(PcmReadingRealEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = nutAutoId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PcmReadingRealNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of <see cref="PcmReadingRealEntity"/>'s. </summary>
    ''' <param name="nubs"> the <see cref="Entities.NutEntity"/>Value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PcmReadingRealNub)) As IEnumerable(Of PcmReadingRealEntity)
        Dim l As New List(Of PcmReadingRealEntity)
        If nubs?.Any Then
            For Each nub As PcmReadingRealNub In nubs
                l.Add(New PcmReadingRealEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of <see cref="PcmReadingRealEntity"/>'s. </summary>
    ''' <param name="interfaces"> the <see cref="Entities.NutEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToManyReal)) As IEnumerable(Of PcmReadingRealEntity)
        Dim l As New List(Of PcmReadingRealEntity)
        If interfaces?.Any Then
            Dim nub As New PcmReadingRealNub
            For Each iFace As IOneToManyReal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PcmReadingRealEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count PCM Readings by unique index; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="PcmReadingTypeId"> Type of the <see cref="Entities.NutEntity"/> value. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal PcmReadingTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PcmReadingRealBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(PcmReadingRealNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = nutAutoId})
        sqlBuilder.Where($"{NameOf(PcmReadingRealNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = PcmReadingTypeId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches PCM Readings by unique index; expected single or none.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="PcmReadingTypeId"> The identifier of the <see cref="Entities.NutEntity"/> value type. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal PcmReadingTypeId As Integer) As IEnumerable(Of PcmReadingRealNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PcmReadingRealBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(PcmReadingRealNub.PrimaryId)} = @primaryId", New With {.primaryId = nutAutoId})
        sqlBuilder.Where($"{NameOf(PcmReadingRealNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = PcmReadingTypeId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PcmReadingRealNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.NutEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     Identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="PcmReadingTypeId"> The Session Suite Real(Double)-value type. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal PcmReadingTypeId As Integer) As Boolean
        Return 1 = PcmReadingRealEntity.CountEntities(connection, nutAutoId, PcmReadingTypeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count values associated with this entity. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of specification values. </returns>
    Public Function CountReadings(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PcmReadingRealBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(PcmReadingRealNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = Me.PrimaryId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Real(Double)-value Values; expected single or none.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">          The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchReadings(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of PcmReadingRealNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PcmReadingRealBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(PcmReadingRealNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = Me.PrimaryId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PcmReadingRealNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.NutEntity"/> Entity. </summary>
    ''' <value> the <see cref="Entities.NutEntity"/> Entity. </value>
    Public ReadOnly Property NutEntity As NutEntity

    ''' <summary> Fetches a Nut entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchNutEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._NutEntity = New NutEntity()
        Return Me.NutEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.NutEntity"/> <see cref="PcmReadingTypeEntity"/>. </summary>
    ''' <value> the <see cref="Entities.NutEntity"/> value type entity. </value>
    Public ReadOnly Property PcmReadingTypeEntity As PcmReadingTypeEntity

    ''' <summary> Fetches a <see cref="PcmReadingTypeEntity"/> Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchPcmReadingTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._PcmReadingTypeEntity = New PcmReadingTypeEntity()
        Return Me.PcmReadingTypeEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> The identifier of the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToManyReal.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PcmReadingRealEntity.NutAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NutEntity"/> record. </summary>
    ''' <value> The identifier of the <see cref="Entities.NutEntity"/> record. </value>
    Public Property NutAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToManyReal.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PcmReadingTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="PcmReadingTypeEntity"/>. </summary>
    ''' <value> The identifier of the <see cref="PcmReadingTypeEntity"/>. </value>
    Public Property PcmReadingTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets a Real(Double)-value assigned to the <see cref="Entities.NutEntity"/> for the specific <see cref="Entities.PcmReadingTypeEntity"/>. </summary>
    ''' <value> The Real(Double)-value assigned to the <see cref="Entities.NutEntity"/> for the specific <see cref="Entities.PcmReadingTypeEntity"/>. </value>
    Public Property Amount As Double Implements IOneToManyReal.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(ByVal value As Double)
            If Not Double.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of Nut reading Real(Double)-value_ entities. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class PcmReadingRealEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToManyReal, PcmReadingRealNub, PcmReadingRealEntity)

    Protected Overrides Function GetKeyForItem(item As PcmReadingRealEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As PcmReadingRealEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all Nuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of PcmReadingRealEntity))
        If entities?.Any Then
            For Each entity As PcmReadingRealEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return PcmReadingRealBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary> Collection of Nut-Unique Nut reading Entities unique to the Nut. </summary>
''' <remarks> David, 2020-05-05. </remarks>
''' 
Public Class NutUniquePcmReadingRealEntityCollection
    Inherits PcmReadingRealEntityCollection

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    Public Sub New(ByVal nutAutoId As Integer)
        Me.New
        Me.NutAutoId = nutAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As Dictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As Dictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value
    '''                       can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As PcmReadingRealEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.PcmReadingTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.PcmReadingTypeId)
        Me.NotifyPropertyChanged(PcmPcmReadingTypeEntity.EntityLookupDictionary(entity.PcmReadingTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all Nuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains 'PcmReadingType' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="PcmReadingTypeId"> Identifier of the <see cref="PcmReadingTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal PcmReadingTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(PcmReadingTypeId)
    End Function

    ''' <summary>
    ''' gets the <see cref="PcmReadingRealEntity"/> associated with the specified type.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="PcmReadingTypeId"> Identifier of the <see cref="PcmReadingTypeEntity"/>. </param>
    ''' <returns> An <see cref="PcmReadingRealEntity"/>. </returns>
    Public Function Entity(ByVal PcmReadingTypeId As Integer) As PcmReadingRealEntity
        Return If(Me.ContainsKey(PcmReadingTypeId), Me(Me._PrimaryKeyDictionary(PcmReadingTypeId)), New PcmReadingRealEntity)
    End Function

    ''' <summary> Gets the Real(Double)-value for the given reading type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="PcmReadingTypeId"> Identifier of the <see cref="PcmReadingTypeEntity"/>. </param>
    ''' <returns> A Double? </returns>
    Public Function Getter(ByVal PcmReadingTypeId As Integer) As Double?
        Return If(Me.ContainsKey(PcmReadingTypeId), Me(Me._PrimaryKeyDictionary(PcmReadingTypeId)).Amount, New Double?)
    End Function

    ''' <summary> Set the specified element value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="PcmReadingTypeId"> Identifier of the <see cref="PcmReadingTypeEntity"/>. </param>
    ''' <param name="value">         The value. </param>
    Public Sub Setter(ByVal PcmReadingTypeId As Integer, ByVal value As Double)
        If Me.ContainsKey(PcmReadingTypeId) Then
            Me(Me._PrimaryKeyDictionary(PcmReadingTypeId)).Amount = value
        Else
            Me.Add(New PcmReadingRealEntity With {.NutAutoId = Me.NutAutoId, .PcmReadingTypeId = PcmReadingTypeId, .Amount = value})
        End If
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="NutEntity"/>. </summary>
    ''' <value> The identifier of the <see cref="NutEntity"/>. </value>
    Public ReadOnly Property NutAutoId As Integer

End Class

