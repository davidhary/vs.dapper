Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions

''' <summary>
''' Interface for the CrossEdge nub and entity. Includes the fields as kept in the data table.
''' Allows tracking of property changes.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface ICrossEdge

    ''' <summary> Gets the identifier of the cross edge. </summary>
    ''' <value> Identifies the cross edge. </value>
    <ExplicitKey>
    Property Id As Integer

    ''' <summary> Gets the title. </summary>
    ''' <value> The title. </value>
    Property Title As String

    ''' <summary> Gets a list of source channels. </summary>
    ''' <value> A List of source channels. </value>
    Property SourceChannelList As String

    ''' <summary> Gets a list of sense channels. </summary>
    ''' <value> A List of sense channels. </value>
    Property SenseChannelList As String

    ''' <summary> Gets the identifier of the source <see cref="PolarityEntity"/>. </summary>
    ''' <value> The source polarity identifier. </value>
    Property SourcePolarityId As Integer

    ''' <summary> Gets the identifier of the source leads <see cref="PolarityEntity"/>. </summary>
    ''' <value> The source leads polarity identifier. </value>
    Property SourceLeadsPolarityId As Integer

    ''' <summary> Gets the identifier of the sense leads <see cref="PolarityEntity"/>. </summary>
    ''' <value> The sense leads polarity identifier. </value>
    Property SenseLeadsPolarityId As Integer

End Interface

''' <summary> The cross edge builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class CrossEdgeBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(CrossEdgeBuilder._TableName) Then
                CrossEdgeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(CrossEdgeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the unique label index. </summary>
    ''' <value> The name of the unique label index. </value>
    Private Shared ReadOnly Property TitleIndexName As String
        Get
            Return $"UQ_{CrossEdgeBuilder.TableName}_{NameOf(CrossEdgeNub.Title)}"
        End Get
    End Property

    ''' <summary> The using unique title. </summary>
    Private Shared _UsingUniqueTitle As Boolean?

    ''' <summary> Indicates if the entity uses a unique Title. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function UsingUniqueTitle(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not CrossEdgeBuilder._UsingUniqueTitle.HasValue Then
            CrossEdgeBuilder._UsingUniqueTitle = connection.IndexExists(CrossEdgeBuilder.TitleIndexName)
        End If
        Return CrossEdgeBuilder._UsingUniqueTitle.Value
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{CrossEdgeBuilder.TableName}] (
[{NameOf(CrossEdgeNub.Id)}] integer NOT NULL PRIMARY KEY, 
[{NameOf(CrossEdgeNub.Title)}] nvarchar(50) NOT NULL, 
[{NameOf(CrossEdgeNub.SourceChannelList)}] nvarchar(50) NOT NULL, 
[{NameOf(CrossEdgeNub.SenseChannelList)}] nvarchar(50) NOT NULL, 
[{NameOf(CrossEdgeNub.SourcePolarityId)}] integer NOT NULL, 
[{NameOf(CrossEdgeNub.SourceLeadsPolarityId)}] integer NOT NULL, 
[{NameOf(CrossEdgeNub.SenseLeadsPolarityId)}] integer NOT NULL,
FOREIGN KEY ([{NameOf(CrossEdgeNub.SourceLeadsPolarityId)}]) REFERENCES [{PolarityBuilder.TableName}] ([{NameOf(PolarityNub.Id)}])
		ON UPDATE NO ACTION ON DELETE NO ACTION,
FOREIGN KEY ([{NameOf(CrossEdgeNub.SourcePolarityId)}]) REFERENCES [{PolarityBuilder.TableName}] ([{NameOf(PolarityNub.Id)}])
		ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY ([{NameOf(CrossEdgeNub.SenseLeadsPolarityId)}]) REFERENCES [{PolarityBuilder.TableName}] ([{NameOf(PolarityNub.Id)}])
		ON UPDATE NO ACTION ON DELETE NO ACTION); 

CREATE UNIQUE INDEX IF NOT EXISTS [{CrossEdgeBuilder.TitleIndexName}] ON [{CrossEdgeBuilder.TableName}] ([{NameOf(CrossEdgeNub.Title)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CrossEdgeBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].{CrossEdgeBuilder.TableName}') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[{CrossEdgeBuilder.TableName}](
	    [{NameOf(CrossEdgeNub.Id)}] [int] NOT NULL,
	    [{NameOf(CrossEdgeNub.Title)}] [nvarchar](50) NOT NULL,
	    [{NameOf(CrossEdgeNub.SourceChannelList)}] [nvarchar](50) NOT NULL,
	    [{NameOf(CrossEdgeNub.SenseChannelList)}] [nvarchar](50) NOT NULL,
	    [{NameOf(CrossEdgeNub.SourcePolarityId)}] [int] NOT NULL,
	    [{NameOf(CrossEdgeNub.SourceLeadsPolarityId)}] [int] NOT NULL,
	    [{NameOf(CrossEdgeNub.SenseLeadsPolarityId)}] [int] NOT NULL,
    CONSTRAINT [PK_{CrossEdgeBuilder.TableName}] PRIMARY KEY CLUSTERED 
        ([{NameOf(CrossEdgeNub.Id)}] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
        ON [PRIMARY]
END; 

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{CrossEdgeBuilder.TableName}]') AND name = N'{CrossEdgeBuilder.TitleIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{CrossEdgeBuilder.TitleIndexName}] ON [dbo].[{CrossEdgeBuilder.TableName}] ([{NameOf(CrossEdgeNub.Title)}] ASC) 
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; 

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CrossEdgeBuilder.TableName}_{NameOf(CrossEdgeNub.SenseLeadsPolarityId)}]') AND parent_object_id = OBJECT_ID(N'[dbo].{CrossEdgeBuilder.TableName}'))
ALTER TABLE [dbo].[{CrossEdgeBuilder.TableName}] WITH CHECK ADD  CONSTRAINT [FK_{CrossEdgeBuilder.TableName}_{NameOf(CrossEdgeNub.SenseLeadsPolarityId)}] FOREIGN KEY([{NameOf(CrossEdgeNub.SenseLeadsPolarityId)}])
REFERENCES [dbo].[{PolarityBuilder.TableName}] ([{NameOf(PolarityNub.Id)}]); 

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CrossEdgeBuilder.TableName}_{NameOf(CrossEdgeNub.SenseLeadsPolarityId)}]') AND parent_object_id = OBJECT_ID(N'[dbo].{CrossEdgeBuilder.TableName}'))
ALTER TABLE [dbo].[{CrossEdgeBuilder.TableName}] CHECK CONSTRAINT [FK_{CrossEdgeBuilder.TableName}_{NameOf(CrossEdgeNub.SenseLeadsPolarityId)}]; 

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CrossEdgeBuilder.TableName}_{NameOf(CrossEdgeNub.SourceLeadsPolarityId)}]') AND parent_object_id = OBJECT_ID(N'[dbo].{CrossEdgeBuilder.TableName}'))
ALTER TABLE [dbo].[{CrossEdgeBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{CrossEdgeBuilder.TableName}_{NameOf(CrossEdgeNub.SourceLeadsPolarityId)}] FOREIGN KEY([{NameOf(CrossEdgeNub.SourceLeadsPolarityId)}])
REFERENCES [dbo].[{PolarityBuilder.TableName}] ([{NameOf(PolarityNub.Id)}]); 

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CrossEdgeBuilder.TableName}_{NameOf(CrossEdgeNub.SourceLeadsPolarityId)}]') AND parent_object_id = OBJECT_ID(N'[dbo].{CrossEdgeBuilder.TableName}'))
ALTER TABLE [dbo].[{CrossEdgeBuilder.TableName}] CHECK CONSTRAINT [FK_{CrossEdgeBuilder.TableName}_{NameOf(CrossEdgeNub.SourceLeadsPolarityId)}];

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CrossEdgeBuilder.TableName}{NameOf(CrossEdgeNub.SourcePolarityId)}]') AND parent_object_id = OBJECT_ID(N'[dbo].{CrossEdgeBuilder.TableName}'))
ALTER TABLE [dbo].[{CrossEdgeBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{CrossEdgeBuilder.TableName}{NameOf(CrossEdgeNub.SourcePolarityId)}] FOREIGN KEY([{NameOf(CrossEdgeNub.SourcePolarityId)}])
REFERENCES [dbo].[{PolarityBuilder.TableName}] ([{NameOf(PolarityNub.Id)}])
ON UPDATE CASCADE ON DELETE CASCADE; 

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CrossEdgeBuilder.TableName}{NameOf(CrossEdgeNub.SourcePolarityId)}]') AND parent_object_id = OBJECT_ID(N'[dbo].{CrossEdgeBuilder.TableName}'))
ALTER TABLE [dbo].[{CrossEdgeBuilder.TableName}] CHECK CONSTRAINT [FK_{CrossEdgeBuilder.TableName}{NameOf(CrossEdgeNub.SourcePolarityId)}]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CrossEdgeBuilder.TableName
    End Function

End Class

''' <summary> Implements the CrossEdge table <see cref="ICrossEdge">interface</see>. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("CrossEdge")>
Public Class CrossEdgeNub
    Inherits EntityNubBase(Of ICrossEdge)
    Implements ICrossEdge

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICrossEdge
        Return New CrossEdgeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICrossEdge
        Dim destination As ICrossEdge = Me.CreateNew
        CrossEdgeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICrossEdge)
        CrossEdgeNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As ICrossEdge, ByVal destination As ICrossEdge)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.Id = source.Id
        destination.SenseChannelList = source.SenseChannelList
        destination.SenseLeadsPolarityId = source.SenseLeadsPolarityId
        destination.SourceChannelList = source.SourceChannelList
        destination.SourceLeadsPolarityId = source.SourceLeadsPolarityId
        destination.SourcePolarityId = source.SourcePolarityId
        destination.Title = source.Title
    End Sub

#End Region

#Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, ICrossEdge))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As ICrossEdge) As Boolean
        Return other IsNot Nothing AndAlso CrossEdgeNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As ICrossEdge, ByVal right As ICrossEdge) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.Id, right.Id)
            result = result AndAlso String.Equals(left.SenseChannelList, right.SenseChannelList)
            result = result AndAlso Integer.Equals(left.SenseLeadsPolarityId, right.SenseLeadsPolarityId)
            result = result AndAlso String.Equals(left.SourceChannelList, right.SourceChannelList)
            result = result AndAlso Integer.Equals(left.SourceLeadsPolarityId, right.SourceLeadsPolarityId)
            result = result AndAlso Integer.Equals(left.SourcePolarityId, right.SourcePolarityId)
            result = result AndAlso String.Equals(left.Title, right.Title)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.Id Xor Me.SenseChannelList.GetHashCode() Xor Me.SenseLeadsPolarityId Xor Me.SourceChannelList.GetHashCode() Xor Me.SourceLeadsPolarityId Xor Me.SourcePolarityId Xor Me.Title.GetHashCode()
    End Function


    #End Region

    ''' <summary> Gets or sets the identifier of the cross edge. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the cross edge. </value>
    <ExplicitKey>
    Public Property Id As Integer Implements ICrossEdge.Id

    ''' <summary> Gets or sets the title. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The title. </value>
    Public Property Title As String Implements ICrossEdge.Title

    ''' <summary> Gets or sets a list of source channels. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> A List of source channels. </value>
    Public Property SourceChannelList As String Implements ICrossEdge.SourceChannelList

    ''' <summary> Gets or sets a list of sense channels. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> A List of sense channels. </value>
    Public Property SenseChannelList As String Implements ICrossEdge.SenseChannelList

    ''' <summary> Gets or sets the identifier of the source <see cref="PolarityEntity"/>. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The source polarity identifier. </value>
    Public Property SourcePolarityId As Integer Implements ICrossEdge.SourcePolarityId

    ''' <summary>
    ''' Gets or sets the identifier of the source leads <see cref="PolarityEntity"/>.
    ''' </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The source leads polarity identifier. </value>
    Public Property SourceLeadsPolarityId As Integer Implements ICrossEdge.SourceLeadsPolarityId

    ''' <summary>
    ''' Gets or sets the identifier of the sense leads <see cref="PolarityEntity"/>.
    ''' </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The sense leads polarity identifier. </value>
    Public Property SenseLeadsPolarityId As Integer Implements ICrossEdge.SenseLeadsPolarityId

End Class

''' <summary> The CrossEdge Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class CrossEdgeEntity
    Inherits EntityBase(Of ICrossEdge, CrossEdgeNub)
    Implements ICrossEdge

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New CrossEdgeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As ICrossEdge)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ICrossEdge, ByVal store As ICrossEdge)
        MyBase.New(New CrossEdgeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(CrossEdgeBuilder.TableName, NameOf(ICrossEdge).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICrossEdge
        Return New CrossEdgeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICrossEdge
        Dim destination As ICrossEdge = Me.CreateNew
        CrossEdgeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICrossEdge)
        CrossEdgeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ICrossEdge)
        ' first make the copy to notify of any property change.
        CrossEdgeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The CrossEdge table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of ICrossEdge)(key), connection.Get(Of CrossEdgeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Title)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="title">      The CrossEdge title. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal title As String) As Boolean
        Me.ClearStore()
        Dim nub As CrossEdgeNub = CrossEdgeEntity.FetchNubs(connection, title).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ICrossEdge) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If CrossEdgeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.Id) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Shared Function StoreEntity(ByVal connection As System.Data.IDbConnection, ByVal entity As ICrossEdge) As Boolean
        Return New CrossEdgeEntity().Upsert(connection, entity)
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of ICrossEdge)(New CrossEdgeNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the CrossEdge entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The CrossEdge entities. </value>
    Public Shared ReadOnly Property CrossEdges As IEnumerable(Of CrossEdgeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of CrossEdgeEntity)
        Return If(usingNativeTracking, CrossEdgeEntity.Populate(connection.GetAll(Of ICrossEdge)), CrossEdgeEntity.Populate(connection.GetAll(Of CrossEdgeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        CrossEdgeEntity._CrossEdges = CrossEdgeEntity.FetchAllEntities(connection, True)
        Me.NotifyPropertyChanged(NameOf(CrossEdgeEntity.CrossEdges))
        Return If(CrossEdgeEntity.CrossEdges?.Any, CrossEdgeEntity.CrossEdges.Count, 0)
    End Function

    ''' <summary> Dictionary of cross edge titles. </summary>
    Private Shared _CrossEdgeTitleDictionary As IDictionary(Of String, CrossEdgeEntity)

    ''' <summary> Cross edge title dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of String, CrossEdgeEntity) </returns>
    Public Shared Function CrossEdgeTitleDictionary() As IDictionary(Of String, CrossEdgeEntity)
        If Not (CrossEdgeEntity._CrossEdgeTitleDictionary?.Any).GetValueOrDefault(False) Then
            CrossEdgeEntity._CrossEdgeTitleDictionary = New Dictionary(Of String, CrossEdgeEntity)
            For Each entity As CrossEdgeEntity In CrossEdgeEntity.CrossEdges
                CrossEdgeEntity._CrossEdgeTitleDictionary.Add(entity.Title, entity)
            Next
        End If
        Return CrossEdgeEntity._CrossEdgeTitleDictionary
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> the Meter Model nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of CrossEdgeNub)) As IEnumerable(Of CrossEdgeEntity)
        Dim l As New List(Of CrossEdgeEntity)
        If nubs?.Any Then
            For Each nub As CrossEdgeNub In nubs
                l.Add(New CrossEdgeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> the Meter Model interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of ICrossEdge)) As IEnumerable(Of CrossEdgeEntity)
        Dim l As New List(Of CrossEdgeEntity)
        If interfaces?.Any Then
            Dim nub As New CrossEdgeNub
            For Each iFace As ICrossEdge In interfaces
                nub.CopyFrom(iFace)
                l.Add(New CrossEdgeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="title">      The CrossEdge title. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal title As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{CrossEdgeBuilder.TableName}] WHERE {NameOf(CrossEdgeNub.Title)} = @title", New With {title})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single nub or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="title">      The CrossEdge title. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal title As String) As IEnumerable(Of CrossEdgeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{CrossEdgeBuilder.TableName}] WHERE {NameOf(CrossEdgeNub.Title)} = @title", New With {title})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of CrossEdgeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the CrossEdge exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="title">      The CrossEdge title. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal title As String) As Boolean
        Return 1 = CrossEdgeEntity.CountEntities(connection, title)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the cross edge. </summary>
    ''' <value> Identifies the cross edge. </value>
    Public Property Id As Integer Implements ICrossEdge.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the title. </summary>
    ''' <value> The title. </value>
    Public Property Title As String Implements ICrossEdge.Title
        Get
            Return Me.ICache.Title
        End Get
        Set(value As String)
            If Not String.Equals(Me.Title, value) Then
                Me.ICache.Title = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets a list of source channels. </summary>
    ''' <value> A List of source channels. </value>
    Public Property SourceChannelList As String Implements ICrossEdge.SourceChannelList
        Get
            Return Me.ICache.SourceChannelList
        End Get
        Set(value As String)
            If Not String.Equals(Me.SourceChannelList, value) Then
                Me.ICache.SourceChannelList = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets a list of sense channels. </summary>
    ''' <value> A List of sense channels. </value>
    Public Property SenseChannelList As String Implements ICrossEdge.SenseChannelList
        Get
            Return Me.ICache.SenseChannelList
        End Get
        Set(value As String)
            If Not String.Equals(Me.SenseChannelList, value) Then
                Me.ICache.SenseChannelList = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the source <see cref="PolarityEntity"/>. </summary>
    ''' <value> The source polarity identifier. </value>
    Public Property SourcePolarityId As Integer Implements ICrossEdge.SourcePolarityId
        Get
            Return Me.ICache.SourcePolarityId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SourcePolarityId, value) Then
                Me.ICache.SourcePolarityId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(CrossEdgeEntity.SourcePolarity))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Source Polarity. </summary>
    ''' <value> The Source Polarity. </value>
    Public Property SourcePolarity As Polarity
        Get
            Return CType(Me.SourcePolarityId, Polarity)
        End Get
        Set(value As Polarity)
            Me.SourcePolarityId = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the source leads <see cref="PolarityEntity"/>.
    ''' </summary>
    ''' <value> The source leads polarity identifier. </value>
    Public Property SourceLeadsPolarityId As Integer Implements ICrossEdge.SourceLeadsPolarityId
        Get
            Return Me.ICache.SourceLeadsPolarityId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SourceLeadsPolarityId, value) Then
                Me.ICache.SourceLeadsPolarityId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(CrossEdgeEntity.SourceLeadsPolarity))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Source Leads Polarity. </summary>
    ''' <value> The Source Leads Polarity. </value>
    Public Property SourceLeadsPolarity As Polarity
        Get
            Return CType(Me.SourceLeadsPolarityId, Polarity)
        End Get
        Set(value As Polarity)
            Me.SourceLeadsPolarityId = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the sense leads <see cref="PolarityEntity"/>.
    ''' </summary>
    ''' <value> The sense leads polarity identifier. </value>
    Public Property SenseLeadsPolarityId As Integer Implements ICrossEdge.SenseLeadsPolarityId
        Get
            Return Me.ICache.SenseLeadsPolarityId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SenseLeadsPolarityId, value) Then
                Me.ICache.SenseLeadsPolarityId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(CrossEdgeEntity.SenseLeadsPolarity))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Sense Leads Polarity. </summary>
    ''' <value> The Sense Leads Polarity. </value>
    Public Property SenseLeadsPolarity As Polarity
        Get
            Return CType(Me.SenseLeadsPolarityId, Polarity)
        End Get
        Set(value As Polarity)
            Me.SenseLeadsPolarityId = value
        End Set
    End Property

    ''' <summary> Converts a value to a polarity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The CrossEdge interface. </param>
    ''' <returns> Value as a Polarity. </returns>
    Public Shared Function ToPolarity(ByVal value As Integer) As Polarity
        Return CType(value, Polarity)
    End Function

#End Region

End Class
