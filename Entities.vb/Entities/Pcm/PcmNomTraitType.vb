Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A PCM Nominal Trait type builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class PcmElementNomTraitTypeBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PcmElementNomTraitTypeBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PcmElementNomTraitTypeBuilder._TableName) Then
                PcmElementNomTraitTypeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PcmNomTraitTypeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Inserts or ignores the records described by the <see cref="PcmNomTraitType"/> enumeration type. </summary>
    ''' <param name="connection"> The connection. </param>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(PcmNomTraitType), New Integer() {CInt(PcmNomTraitType.None)})
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PcmElementNomTraitTypeBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PcmElementNomTraitTypeBuilder
        If PcmElementNomTraitTypeBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PcmElementNomTraitTypeBuilder._Instance = New PcmElementNomTraitTypeBuilder()
            End SyncLock
        End If
        Return PcmElementNomTraitTypeBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the <see cref="PcmNomTraitTypeEntity"/> table based on the <see cref="INominal">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("PcmNomTraitType")>
Public Class PcmNomTraitTypeNub
    Inherits NominalNub
    Implements INominal

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As INominal
        Return New PcmNomTraitTypeNub
    End Function

End Class


''' <summary> Implements the <see cref="PcmNomTraitTypeEntity"/> table based on the <see cref="INominal">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class PcmNomTraitTypeEntity
    Inherits EntityBase(Of INominal, PcmNomTraitTypeNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New PcmNomTraitTypeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Pcm Nominal Trait Type interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New PcmNomTraitTypeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PcmElementNomTraitTypeBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As INominal
        Return New PcmNomTraitTypeNub
    End Function

    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        PcmNomTraitTypeNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As INominal)
        PcmNomTraitTypeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Pcm Nominal Trait Type interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        PcmNomTraitTypeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Pcm Nominal Trait Type table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of PcmNomTraitTypeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The PCM Nominal Trait Type label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As PcmNomTraitTypeNub = PcmNomTraitTypeEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PcmNomTraitTypeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of PcmNomTraitTypeNub)(New PcmNomTraitTypeNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the Pcm  Trait Type entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> A list of types of the PCM Nominal Traits. </value>
    Public Shared ReadOnly Property PcmNomTraitTypes As IEnumerable(Of PcmNomTraitTypeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PcmNomTraitTypeEntity)
        Return If(usingNativeTracking, PcmNomTraitTypeEntity.Populate(connection.GetAll(Of INominal)), PcmNomTraitTypeEntity.Populate(connection.GetAll(Of PcmNomTraitTypeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        PcmNomTraitTypeEntity._PcmNomTraitTypes = PcmNomTraitTypeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(PcmNomTraitTypeEntity.PcmNomTraitTypes?.Any, PcmNomTraitTypeEntity.PcmNomTraitTypes.Count, 0)
    End Function

    ''' <summary> Populates a list of <see cref="PcmNomTraitTypeEntity"/>'s. </summary>
    ''' <param name="nubs"> The <see cref="PcmNomTraitTypeNub"/>'s. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PcmNomTraitTypeNub)) As IEnumerable(Of PcmNomTraitTypeEntity)
        Dim l As New List(Of PcmNomTraitTypeEntity)
        If nubs?.Any Then
            For Each nub As PcmNomTraitTypeNub In nubs
                l.Add(New PcmNomTraitTypeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of <see cref="PcmNomTraitTypeEntity"/>'s. </summary>
    ''' <param name="interfaces"> The Pcm Nominal Trait Type interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of PcmNomTraitTypeEntity)
        Dim l As New List(Of PcmNomTraitTypeEntity)
        If interfaces?.Any Then
            Dim nub As New PcmNomTraitTypeNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PcmNomTraitTypeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, PcmNomTraitTypeEntity)

    ''' <summary> The PCM Nominal Trait Type lookup dictionary. </summary>
    ''' <remarks> David, 6/12/2020. </remarks>
    ''' <returns> A Dictionary(Of Integer, PcmNomTraitTypeEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, PcmNomTraitTypeEntity)
        If Not (PcmNomTraitTypeEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            PcmNomTraitTypeEntity._EntityLookupDictionary = New Dictionary(Of Integer, PcmNomTraitTypeEntity)
            For Each entity As PcmNomTraitTypeEntity In PcmNomTraitTypeEntity.PcmNomTraitTypes
                PcmNomTraitTypeEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return PcmNomTraitTypeEntity._EntityLookupDictionary
    End Function

    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (PcmNomTraitTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            PcmNomTraitTypeEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As PcmNomTraitTypeEntity In PcmNomTraitTypeEntity.PcmNomTraitTypes
                PcmNomTraitTypeEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return PcmNomTraitTypeEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (PcmNomTraitTypeEntity.PcmNomTraitTypes?.Any AndAlso
                PcmNomTraitTypeEntity._EntityLookupDictionary?.Any AndAlso
                PcmNomTraitTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function


#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The PCM Nominal Trait Type label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{PcmElementNomTraitTypeBuilder.TableName}] WHERE {NameOf(PcmNomTraitTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The PCM Nominal Trait Type label. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the nubs in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of PcmNomTraitTypeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PcmElementNomTraitTypeBuilder.TableName}] WHERE {NameOf(PcmNomTraitTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PcmNomTraitTypeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="PcmNomTraitTypeEntity"/> exists. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = PcmNomTraitTypeEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the PCM Nominal Trait Type . </summary>
    ''' <value> Identifies the PCM Nominal Trait Type . </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the PCM Nominal Trait Type label. </summary>
    ''' <value> The PCM Nominal Trait Type label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the PCM Nominal Trait Type . </summary>
    ''' <value> The PCM Nominal Trait Type description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="pcmNomTraitTypeId"> The entity unique key. </param>
    ''' <returns> A <see cref="PcmNomTraitTypeEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal pcmNomTraitTypeId As Integer) As (Success As Boolean, Details As String, Entity As PcmNomTraitTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New PcmNomTraitTypeEntity
        Try
            activity = $"Fetching {NameOf(PcmNomTraitTypeEntity)} by {NameOf(PcmNomTraitTypeNub.Id)} of {pcmNomTraitTypeId}"
            Return If(entity.FetchUsingKey(connection, pcmNomTraitTypeId),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <param name="connection">                   The connection. </param>
    ''' <param name="pcmNomTraitTypeLabel"> The PCM Nominal Trait Type label. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entity As PcmNomTraitTypeEntity)
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal pcmNomTraitTypeLabel As String) As (Success As Boolean, Details As String, Entity As PcmNomTraitTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New PcmNomTraitTypeEntity
        Try
            activity = $"Fetching {NameOf(PcmNomTraitTypeEntity)} by {NameOf(PcmNomTraitTypeNub.Label)} of {pcmNomTraitTypeLabel}"
            Return If(entity.FetchUsingUniqueIndex(connection, pcmNomTraitTypeLabel),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of
    ''' PcmNomTraitTypeEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of PcmNomTraitTypeEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New PcmNomTraitTypeEntity()
            activity = $"fetching all {NameOf(PcmNomTraitTypeEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> PcmNomTraitTypeEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(PcmNomTraitTypeEntity.EntityLookupDictionary)} count must equal {NameOf(PcmNomTraitTypeEntity.PcmNomTraitTypes)} count ")
            ElseIf elementCount <> PcmNomTraitTypeEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(PcmNomTraitTypeEntity.KeyLookupDictionary)} count must equal {NameOf(PcmNomTraitTypeEntity.PcmNomTraitTypes)} count ")
            End If
            Return (True, String.Empty, PcmNomTraitTypeEntity.PcmNomTraitTypes)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of PcmNomTraitTypeEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Values that represent PCM Nominal Trait Types. </summary>
Public Enum PcmNomTraitType
    <ComponentModel.Description("None")> None = 0
    <ComponentModel.Description("Line Length")> LineLength
    <ComponentModel.Description("Line Width")> LineWidth
    <ComponentModel.Description("Split Line Length")> SplitLineLength
    <ComponentModel.Description("Split Line Width ")> SplitLineWidth
End Enum
