
Partial Public Class PartEntity

    ''' <summary> Gets or sets the <see cref="ProductPartEntity"/>. </summary>
    ''' <value> The <see cref="ProductPartEntity"/> </value>
    Public ReadOnly Property ProductPart As ProductPartEntity

    ''' <summary>
    ''' Gets or sets the <see cref="ProductUniqueElementEntityCollection">part elements</see>.
    ''' </summary>
    ''' <value> The Part elements entities. </value>
    Public ReadOnly Property Elements As ProductUniqueElementEntityCollection

    ''' <summary>
    ''' Fetches the <see cref="ProductUniqueElementEntityCollection">part elements</see>.
    ''' </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="multimeterIdentities"> The multimeter identities. </param>
    ''' <returns> The number of Elements. </returns>
    Public Function FetchElements(ByVal connection As System.Data.IDbConnection, ByVal multimeterIdentities As IEnumerable(Of Integer)) As Integer
        Dim count As Integer = Me.FetchElementsOnly(connection)
        If count > 0 Then Me.FetchNomTraits(connection, multimeterIdentities)
        Return count
    End Function

    ''' <summary> Fetches elements only. </summary>
    ''' <remarks> David, 6/17/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The elements only. </returns>
    Public Function FetchElementsOnly(ByVal connection As System.Data.IDbConnection) As Integer
        If Not (Me.ProductPart?.IsClean).GetValueOrDefault(False) Then
            Me._ProductPart = ProductPartEntity.FetchEntitiesByPart(connection, Me.AutoId).FirstOrDefault
        End If
        If (Me.ProductPart?.IsClean).GetValueOrDefault(False) Then
            ' a part has elements only if a product is associated with the part.
            Me._Elements = PartEntity.FetchElements(connection, Me.ProductPart.ProductAutoId)
        End If
        Me.FetchElementEquations(connection)
        Return If(Me.Elements?.Any, Me.Elements.Count, 0)
    End Function

    ''' <summary>
    ''' Fetches the <see cref="ProductUniqueElementEntityCollection">part elements</see>.
    ''' </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> The Elements. </returns>
    Public Shared Function FetchElements(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As ProductUniqueElementEntityCollection
        If Not NomTraitTypeEntity.IsEnumerated Then NomTraitTypeEntity.TryFetchAll(connection)
        If Not ElementTypeEntity.IsEnumerated Then ElementTypeEntity.TryFetchAll(connection)
        Dim elements As New ProductUniqueElementEntityCollection(productAutoId)
        elements.Populate(ProductElementEntity.FetchOrderedElements(connection, productAutoId))
        elements.FetchNomTypes(connection)
        Return elements
    End Function

    ''' <summary> Gets or sets the element equations. </summary>
    ''' <value> The element equations. </value>
    Public ReadOnly Property ElementEquations As PartUniqueElementEquationEntityCollection

    ''' <summary> Fetches element equations. </summary>
    ''' <remarks> David, 7/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The element equations. </returns>
    Public Function FetchElementEquations(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ElementEquations = PartEntity.FetchElementEquations(connection, Me)
        Return If(Me.ElementEquations?.Any, Me.ElementEquations.Count, 0)
    End Function

    ''' <summary> Fetches element equations. </summary>
    ''' <remarks> David, 7/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="part">       The <see cref="PartEntity"/>. </param>
    ''' <returns> The element equations. </returns>
    Public Shared Function FetchElementEquations(ByVal connection As System.Data.IDbConnection, ByVal part As PartEntity) As PartUniqueElementEquationEntityCollection
        Dim elementEquations As New PartUniqueElementEquationEntityCollection(part)
        elementEquations.Populate(PartElementEquationEntity.FetchEntities(connection, part.AutoId))
        Return elementEquations
    End Function

End Class



