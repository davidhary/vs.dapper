
Partial Public Class PartEntity

    ''' <summary>
    ''' Gets or sets the <see cref=" PartUniqueNamingEntityCollection">Part Naming values</see>.
    ''' </summary>
    ''' <value> The Part Naming trait values. </value>
    Public ReadOnly Property Namings As PartUniqueNamingEntityCollection

    ''' <summary>
    ''' Fetches <see cref=" PartUniqueNamingEntityCollection">Part Naming values</see>.
    ''' </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of specifications. </returns>
    Public Function FetchNamings(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Namings = PartEntity.FetchNamings(connection, Me.AutoId)
        Return If(Me.Namings?.Any, Me.Namings.Count, 0)
    End Function

    ''' <summary>
    ''' Fetches <see cref="PartUniqueNamingEntityCollection">Part Naming values</see>.
    ''' </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> The specifications. </returns>
    Public Shared Function FetchNamings(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As PartUniqueNamingEntityCollection
        If Not PartNamingTypeEntity.IsEnumerated Then PartNamingTypeEntity.TryFetchAll(connection)
        Dim namings As New PartUniqueNamingEntityCollection(partAutoId)
        namings.Populate(PartNamingEntity.FetchEntities(connection, partAutoId))
        Return namings
    End Function

End Class

