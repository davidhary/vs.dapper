''' <summary> The Part Naming class holing the Part Naming values. </summary>
''' <remarks> David, 2020-05-29. </remarks>
Public Class PartNaming
    Inherits isr.Core.Models.ViewModelBase

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter

    ''' <summary> Gets the naming value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Integer. </returns>
    Public Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String
        Return Me.GetterSetter.Getter(name)
    End Function

    ''' <summary> Sets the naming value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Integer. </returns>
    Public Function Setter(ByVal value As String, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String
        If Not String.Equals(value, Me.Getter(name)) Then
            Me.GetterSetter.Setter(value, name)
            Me.NotifyPropertyChanged(name)
        End If
        Return value
    End Function


#If False Then
    ''' <summary> Gets or sets the naming value. </summary>
    ''' <value> The naming value. </value>
    Protected Property NamingValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.NamingValue(name)) Then
                Me.GetterSetter.Setter(value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property
#End If

#End Region

#Region " NAMING ITEMS "

    ''' <summary> Gets or sets the part number. </summary>
    ''' <value> The part number. </value>
    Public Property PartNumber As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the family code. </summary>
    ''' <value> The family code. </value>
    Public Property FamilyCode As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the product number. </summary>
    ''' <value> The product number. </value>
    Public Property ProductNumber As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Product Case Code. </summary>
    ''' <value> The Product Case Code. </value>
    Public Property ProductCaseCode As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the model number. </summary>
    ''' <value> The model number. </value>
    Public Property ModelNumber As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the product number termination. </summary>
    ''' <value> The product number termination. </value>
    Public Property ProductNumberTermination As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the special process specification. </summary>
    ''' <value> The special process specification. </value>
    Public Property SpecialProcessSpecification As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the packaging code. </summary>
    ''' <value> The packaging code. </value>
    Public Property PackagingCode As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the engineering suffix. </summary>
    ''' <value> The engineering suffix. </value>
    Public Property EngineeringSuffix As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the unknown suffix. </summary>
    ''' <value> The unknown suffix. </value>
    Public Property UnknownSuffix As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the tcr code. </summary>
    ''' <value> The tcr code. </value>
    Public Property TcrCode As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the tolerance codes. </summary>
    ''' <value> The tolerance codes. </value>
    Public Property ToleranceCodes As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the tolerance code. </summary>
    ''' <value> The tolerance code. </value>
    Public Property ToleranceCode As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the tracking code. </summary>
    ''' <value> The tracking code. </value>
    Public Property TrackingCode As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the element value 1. </summary>
    ''' <value> The element value 1. </value>
    Public Property ElementValue1 As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the element value 2. </summary>
    ''' <value> The element value 2. </value>
    Public Property ElementValue2 As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the critical element id (label). </summary>
    ''' <value> The critical element label. </value>
    Public Property CriticalElementId As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the primary element id (label). </summary>
    ''' <value> The primary element label. </value>
    Public Property PrimaryElementId As String
        Get
            Return Me.Getter
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

#End Region

End Class
