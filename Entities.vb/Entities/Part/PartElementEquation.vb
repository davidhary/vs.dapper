Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Part Element Equation builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class PartElementEquationBuilder
    Inherits TwoToManyLabelBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PartElementEquationBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PartElementEquationBuilder._TableName) Then
                PartElementEquationBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PartElementEquationNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 255

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = PartBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(PartNub.AutoId)

    ''' <summary> Gets or sets the name of the Secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Secondary table. </value>
    Public Overrides Property SecondaryTableName As String = ElementBuilder.TableName

    ''' <summary> Gets or sets the name of the Secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(ElementNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(PartElementEquationEntity.PartAutoId)

    ''' <summary> Gets or sets the name of the Secondary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(PartElementEquationEntity.ElementAutoId)

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the label field. </value>
    Public Overrides Property LabelFieldName As String = NameOf(PartElementEquationEntity.Equation)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PartElementEquationBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PartElementEquationBuilder
        If PartElementEquationBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PartElementEquationBuilder._Instance = New PartElementEquationBuilder()
            End SyncLock
        End If
        Return PartElementEquationBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="Entities.PartEntity"/> Text table
''' <see cref="ITwoToManyLabel">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("PartElementEquation")>
Public Class PartElementEquationNub
    Inherits TwoToManyLabelNub
    Implements ITwoToManyLabel

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ITwoToManyLabel
        Return New PartElementEquationNub
    End Function

End Class

''' <summary>
''' The <see cref="PartElementEquationEntity"/>. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class PartElementEquationEntity
    Inherits EntityBase(Of ITwoToManyLabel, PartElementEquationNub)
    Implements ITwoToManyLabel

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New PartElementEquationNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.PartEntity"/>Text interface. </param>
    Public Sub New(ByVal value As ITwoToManyLabel)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ITwoToManyLabel, ByVal store As ITwoToManyLabel)
        MyBase.New(New PartElementEquationNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PartElementEquationBuilder.TableName, NameOf(ITwoToManyLabel).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ITwoToManyLabel
        Return New PartElementEquationNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ITwoToManyLabel
        Dim destination As ITwoToManyLabel = Me.CreateNew
        PartElementEquationNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="PartElementEquationEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As ITwoToManyLabel)
        PartElementEquationNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached Text, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.PartEntity"/>Text interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ITwoToManyLabel)
        ' first make the copy to notify of any property change.
        PartElementEquationNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="partAutoId">    Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal elementAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PartElementEquationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartElementEquationNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        sqlBuilder.Where($"{NameOf(PartElementEquationNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of PartElementEquationNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="partAutoId">    Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal elementAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As PartElementEquationNub = PartElementEquationEntity.FetchEntities(connection, partAutoId, elementAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ITwoToManyLabel) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PartElementEquationEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="partAutoId">    Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal elementAutoId As Integer) As Boolean
        Return connection.Delete(Of PartElementEquationNub)(New PartElementEquationNub With {.PrimaryId = partAutoId, .SecondaryId = elementAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="Entities.PartEntity"/>Text entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> the <see cref="Entities.PartEntity"/>Text entities. </value>
    Public ReadOnly Property PartElementEquations As IEnumerable(Of PartElementEquationEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PartElementEquationEntity)
        Return If(usingNativeTracking, PartElementEquationEntity.Populate(connection.GetAll(Of ITwoToManyLabel)),
                                       PartElementEquationEntity.Populate(connection.GetAll(Of PartElementEquationNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._PartElementEquations = PartElementEquationEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(PartElementEquationEntity.PartElementEquations))
        Return If(Me.PartElementEquations?.Any, Me.PartElementEquations.Count, 0)
    End Function

    ''' <summary> Count Part Texts. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{PartElementEquationBuilder.TableName}] WHERE {NameOf(PartElementEquationNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of PartElementEquationEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PartElementEquationBuilder.TableName}] WHERE {NameOf(PartElementEquationNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return PartElementEquationEntity.Populate(connection.Query(Of PartElementEquationNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches Part Texts by Part Auto Id. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of PartElementEquationNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PartElementEquationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartElementEquationEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PartElementEquationNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of Part Text entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> the <see cref="Entities.PartEntity"/>Text nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PartElementEquationNub)) As IEnumerable(Of PartElementEquationEntity)
        Dim l As New List(Of PartElementEquationEntity)
        If nubs?.Any Then
            For Each nub As PartElementEquationNub In nubs
                l.Add(New PartElementEquationEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Part Text entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> the <see cref="Entities.PartEntity"/>Text interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of ITwoToManyLabel)) As IEnumerable(Of PartElementEquationEntity)
        Dim l As New List(Of PartElementEquationEntity)
        If interfaces?.Any Then
            Dim nub As New PartElementEquationNub
            For Each iFace As ITwoToManyLabel In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PartElementEquationEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Part Texts by unique index; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="partAutoId">    Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PartElementEquationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartElementEquationNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        sqlBuilder.Where($"{NameOf(PartElementEquationNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches Part Texts by unique index; expected single or none. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="partAutoId">    Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal elementAutoId As Integer) As IEnumerable(Of PartElementEquationNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PartElementEquationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartElementEquationNub.PrimaryId)} = @primaryId", New With {.primaryId = partAutoId})
        sqlBuilder.Where($"{NameOf(PartElementEquationNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PartElementEquationNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.PartEntity"/>Text exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="partAutoId">    Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal elementAutoId As Integer) As Boolean
        Return 1 = PartElementEquationEntity.CountEntities(connection, partAutoId, elementAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count Texts associated with this part. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of Texts. </returns>
    Public Function CountPartElementEquations(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PartElementEquationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartElementEquationNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Part Text Texts by Part auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 7/5/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchPartElementEquations(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of PartElementEquationNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PartElementEquationBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartElementEquationNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PartElementEquationNub)(selector.RawSql, selector.Parameters)
    End Function

#End Region

#Region " RELATIONS: PART "

    ''' <summary> Gets or sets the Part entity. </summary>
    ''' <value> The Part entity. </value>
    Public ReadOnly Property PartEntity As PartEntity

    ''' <summary> Fetches a Part Entity. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Part Entity. </returns>
    Public Function FetchPartEntity(ByVal connection As System.Data.IDbConnection) As PartEntity
        Dim entity As New PartEntity()
        entity.FetchUsingKey(connection, Me.PartAutoId)
        Me._PartEntity = entity
        Return entity
    End Function

#End Region

#Region " RELATIONS: PRODUCT TEXT TYPE  "

    ''' <summary>
    ''' Gets or sets the <see cref="Entities.PartEntity"/> <see cref="Entities.ElementEntity"/>.
    ''' </summary>
    ''' <value> the <see cref="Entities.PartEntity"/> Text type entity. </value>
    Public ReadOnly Property ElementEntity As Entities.ElementEntity

    ''' <summary> Fetches a element entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchElementEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._ElementEntity = New Entities.ElementEntity()
        Return Me.ElementEntity.FetchUsingKey(connection, Me.ElementAutoId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements ITwoToManyLabel.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PartElementEquationEntity.PartAutoId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.PartEntity"/> record.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.PartEntity"/> record. </value>
    Public Property PartAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements ITwoToManyLabel.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PartElementEquationEntity.ElementAutoId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identity (type) of the <see cref="entities.ElementEntity"/>.
    ''' </summary>
    ''' <value> Identifies the Part Text type. </value>
    Public Property ElementAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the <see cref="entities.PartElementEquationEntity"/> label. </summary>
    ''' <value> the <see cref="entities.PartElementEquationEntity"/> label. </value>
    Public Property Label As String Implements ITwoToManyLabel.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the equation. </summary>
    ''' <value> The equation. </value>
    Public Property Equation As String
        Get
            Return Me.Label
        End Get
        Set(ByVal value As String)
            Me.Label = value
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As TwoKeySelector
        Get
            Return New TwoKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of <see cref="PartElementEquationEntity"/>'s. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class PartElementEquationEntityCollection
    Inherits EntityKeyedCollection(Of TwoKeySelector, ITwoToManyLabel, PartElementEquationNub, PartElementEquationEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As PartElementEquationEntity) As TwoKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As PartElementEquationEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all parts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of PartElementEquationEntity))
        If entities?.Any Then
            For Each entity As PartElementEquationEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return PartElementEquationBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary>
''' Collection of <see cref="Entities.PartEntity"/>-Unique
''' <see cref="PartElementEquationEntity"/>'s.
''' </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class PartUniqueElementEquationEntityCollection
    Inherits PartElementEquationEntityCollection

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="part"> The part. </param>
    Public Sub New(ByVal part As PartEntity)
        MyBase.New
        Me.PartAutoId = part.AutoId
        Me._UniqueIndexDictionary = New Dictionary(Of TwoKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, TwoKeySelector)
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of TwoKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, TwoKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As PartElementEquationEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.ElementAutoId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.ElementAutoId)
    End Sub

    ''' <summary>
    ''' Removes all parts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

#End Region

#Region " SELECTORS "

    ''' <summary> Queries if collection contains 'Element' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal elementAutoId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(elementAutoId)
    End Function

    ''' <summary>
    ''' Returns the equation for the specified identifier of the <see cref="ElementEntity"/>.
    ''' </summary>
    ''' <remarks> David, 7/16/2020. </remarks>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> A String. </returns>
    Public Function Equation(ByVal elementAutoId As Integer) As String
        Return Me(Me._PrimaryKeyDictionary(elementAutoId)).Equation
    End Function

    ''' <summary> Returns the equation for the specified <see cref="NutEntity"/>. </summary>
    ''' <remarks> David, 7/16/2020. </remarks>
    ''' <param name="nut"> The nut. </param>
    ''' <returns> A String. </returns>
    Public Function Equation(ByVal nut As NutEntity) As String
        Return Me(Me._PrimaryKeyDictionary(nut.ElementAutoId)).Equation
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.PartEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.PartEntity"/>. </value>
    Public ReadOnly Property PartAutoId As Integer

#End Region

End Class

