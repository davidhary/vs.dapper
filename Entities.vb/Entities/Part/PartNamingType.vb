Imports System.ComponentModel
Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A Part Naming type builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class PartNamingTypeBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PartNamingTypeBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PartNamingTypeBuilder._TableName) Then
                PartNamingTypeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PartNamingTypeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary>
    ''' Inserts or ignores the records described by the <see cref="PartNamingType"/> enumeration type.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(PartNamingType), Array.Empty(Of Integer)())
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PartNamingTypeBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PartNamingTypeBuilder
        If PartNamingTypeBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PartNamingTypeBuilder._Instance = New PartNamingTypeBuilder()
            End SyncLock
        End If
        Return PartNamingTypeBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the PartSpecificationType table based on the <see cref="INominal">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("PartNamingType")>
Public Class PartNamingTypeNub
    Inherits NominalNub
    Implements INominal

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New PartNamingTypeNub
    End Function

End Class

''' <summary>
''' The <see cref="Entities.PartNamingTypeEntity"/>. Implements access to the database using
''' Dapper.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class PartNamingTypeEntity
    Inherits EntityBase(Of INominal, PartNamingTypeNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New PartNamingTypeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The PartSpecificationType interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New PartNamingTypeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PartNamingTypeBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New PartNamingTypeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        PartNamingTypeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As INominal)
        PartNamingTypeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The PartSpecificationType interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        PartNamingTypeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The PartSpecificationType table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of PartNamingTypeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Part Naming type label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As PartNamingTypeNub = PartNamingTypeEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PartNamingTypeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of PartNamingTypeNub)(New PartNamingTypeNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the PartSpecificationType entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The PartSpecificationType entities. </value>
    Public Shared ReadOnly Property PartSpecificationTypes As IEnumerable(Of PartNamingTypeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PartNamingTypeEntity)
        Return If(usingNativeTracking, PartNamingTypeEntity.Populate(connection.GetAll(Of INominal)), PartNamingTypeEntity.Populate(connection.GetAll(Of PartNamingTypeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        PartNamingTypeEntity._PartSpecificationTypes = PartNamingTypeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(PartNamingTypeEntity.PartSpecificationTypes?.Any, PartNamingTypeEntity.PartSpecificationTypes.Count, 0)
    End Function

    ''' <summary> Populates a list of PartSpecificationType entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The PartSpecificationType nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PartNamingTypeNub)) As IEnumerable(Of PartNamingTypeEntity)
        Dim l As New List(Of PartNamingTypeEntity)
        If nubs?.Any Then
            For Each nub As PartNamingTypeNub In nubs
                l.Add(New PartNamingTypeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of PartSpecificationType entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The PartSpecificationType interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of PartNamingTypeEntity)
        Dim l As New List(Of PartNamingTypeEntity)
        If interfaces?.Any Then
            Dim nub As New PartNamingTypeNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PartNamingTypeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Dictionary of entity lookups. </summary>
    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, PartNamingTypeEntity)

    ''' <summary> The Part Naming type lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of Integer, PartSpecificationTypeEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, PartNamingTypeEntity)
        If Not (PartNamingTypeEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            PartNamingTypeEntity._EntityLookupDictionary = New Dictionary(Of Integer, PartNamingTypeEntity)
            For Each entity As PartNamingTypeEntity In PartNamingTypeEntity.PartSpecificationTypes
                PartNamingTypeEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return PartNamingTypeEntity._EntityLookupDictionary
    End Function

    ''' <summary> Dictionary of key lookups. </summary>
    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (PartNamingTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            PartNamingTypeEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As PartNamingTypeEntity In PartNamingTypeEntity.PartSpecificationTypes
                PartNamingTypeEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return PartNamingTypeEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (PartNamingTypeEntity.PartSpecificationTypes?.Any AndAlso
                PartNamingTypeEntity._EntityLookupDictionary?.Any AndAlso
                PartNamingTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Part Naming type label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{PartSpecificationTypeNub.TableName}] 
        ' WHERE {NameOf(PartSpecificationTypeNub.Label)} = @Label", New With {Key .Label = label})
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{PartNamingTypeBuilder.TableName}] WHERE {NameOf(PartNamingTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Part Naming type label. </param>
    ''' <returns> An IPartSpecificationType . </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of PartNamingTypeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PartNamingTypeBuilder.TableName}] WHERE {NameOf(PartNamingTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PartNamingTypeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the PartSpecificationType exists. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Part Naming type label. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = PartNamingTypeEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Part Naming type. </summary>
    ''' <value> Identifies the Part Naming type. </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PartNamingTypeEntity.PartNamingType))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the type of the Part Naming. </summary>
    ''' <value> The type of the PartSpecification. </value>
    Public Property PartNamingType As PartNamingType
        Get
            Return CType(Me.Id, PartNamingType)
        End Get
        Set(value As PartNamingType)
            Me.Id = value
        End Set
    End Property

    ''' <summary> Gets or sets the Part Naming type label. </summary>
    ''' <value> The Part Naming type label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the Part Naming type. </summary>
    ''' <value> The Part Naming type description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="partNamingTypeId"> The entity unique key. </param>
    ''' <returns> A <see cref="Entities.PartNamingTypeEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal partNamingTypeId As Integer) As (Success As Boolean, Details As String, Entity As PartNamingTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New PartNamingTypeEntity
        Try
            activity = $"Fetching {NameOf(PartNamingTypeEntity)} by {NameOf(PartNamingTypeNub.Id)} of {partNamingTypeId}"
            Return If(entity.FetchUsingKey(connection, partNamingTypeId),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">                 The connection. </param>
    ''' <param name="partSpecificationTypeLabel"> The Part Naming type label. </param>
    ''' <returns> A <see cref="Entities.PartNamingTypeEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal partSpecificationTypeLabel As String) As (Success As Boolean, Details As String, Entity As PartNamingTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New PartNamingTypeEntity
        Try
            activity = $"Fetching {NameOf(PartNamingTypeEntity)} by {NameOf(PartNamingTypeNub.Label)} of {partSpecificationTypeLabel}"
            Return If(entity.FetchUsingUniqueIndex(connection, partSpecificationTypeLabel),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of
    ''' PartSpecificationTypeEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of PartNamingTypeEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New PartNamingTypeEntity()
            activity = $"fetching all {NameOf(PartNamingTypeEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> PartNamingTypeEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(PartNamingTypeEntity.EntityLookupDictionary)} count must equal {NameOf(PartNamingTypeEntity.PartSpecificationTypes)} count ")
            ElseIf elementCount <> PartNamingTypeEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(PartNamingTypeEntity.KeyLookupDictionary)} count must equal {NameOf(PartNamingTypeEntity.PartSpecificationTypes)} count ")
            End If
            Return (True, String.Empty, PartNamingTypeEntity.PartSpecificationTypes)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of PartNamingTypeEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub


#End Region

End Class

''' <summary> Values that represent Part Naming types. </summary>
''' <remarks> David, 2020-10-02. </remarks>
Public Enum PartNamingType

    ''' <summary> An enum constant representing the part number option. </summary>
    <Description("Part Number")> PartNumber

    ''' <summary> An enum constant representing the family code option. </summary>
    <Description("Family Code")> FamilyCode

    ''' <summary> An enum constant representing the product number option. </summary>
    <Description("Product Number")> ProductNumber

    ''' <summary> An enum constant representing the Product Case Code option. </summary>
    <Description("Product Case Code")> ProductCaseCode

    ''' <summary> An enum constant representing the model number option. </summary>
    <Description("Model Number")> ModelNumber

    ''' <summary> An enum constant representing the product number termination option. </summary>
    <Description("Product Number Termination ")> ProductNumberTermination

    ''' <summary> An enum constant representing the special process specification option. </summary>
    <Description("Special Process Specification")> SpecialProcessSpecification

    ''' <summary> An enum constant representing the packaging code option. </summary>
    <Description("Packaging Code")> PackagingCode

    ''' <summary> An enum constant representing the engineering suffix option. </summary>
    <Description("Engineering Suffix")> EngineeringSuffix

    ''' <summary> An enum constant representing the unknown suffix option. </summary>
    <Description("Unknown Suffix")> UnknownSuffix

    ''' <summary> An enum constant representing the tcr code option. </summary>
    <Description("Tcr Code")> TcrCode

    ''' <summary> An enum constant representing the element value 1 option. </summary>
    <Description("Element Value 1")> ElementValue1

    ''' <summary> An enum constant representing the element value 2 option. </summary>
    <Description("Element Value 2")> ElementValue2

    ''' <summary> An enum constant representing the tolerance codes option. </summary>
    <Description("Tolerance Codes")> ToleranceCodes

    ''' <summary> An enum constant representing the tolerance code option. </summary>
    <Description("Tolerance Code")> ToleranceCode

    ''' <summary> An enum constant representing the tracking code option. </summary>
    <Description("Tracking Code")> TrackingCode

    ''' <summary> An enum constant representing the primary element Identifier option. </summary>
    <Description("Primary Element Identity")> PrimaryElementId

    ''' <summary> An enum constant representing the critical element Identifier option. </summary>
    <Description("Critical Element Identity")> CriticalElementId
End Enum
