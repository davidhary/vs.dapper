Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Core

''' <summary> A Part-Meter-Element-Nominal-Trait builder. </summary>
''' <remarks> David, 6/16/2020. </remarks>
Public NotInheritable Class PartNomBuilder
    Inherits ThreeToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PartNomBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PartNomBuilder._TableName) Then
                PartNomBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PartNomNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = PartBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(PartNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = MultimeterBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(MultimeterNub.Id)

    ''' <summary> Gets or sets the name of the Ternary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Ternary table. </value>
    Public Overrides Property TernaryTableName As String = ElementBuilder.TableName

    ''' <summary> Gets or sets the name of the Ternary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Ternary table key. </value>
    Public Overrides Property TernaryTableKeyName As String = NameOf(ElementNub.AutoId)

    ''' <summary> Gets or sets the name of the Ternary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Ternary table. </value>
    Public Overrides Property QuaternaryTableName As String = NomTraitBuilder.TableName

    ''' <summary> Gets or sets the name of the Ternary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Ternary table key. </value>
    Public Overrides Property QuaternaryTableKeyName As String = NameOf(NomTraitNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(PartNomEntity.PartAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(PartNomEntity.MultimeterId)

    ''' <summary> Gets or sets the name of the ternary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the ternary identifier field. </value>
    Public Overrides Property TernaryIdFieldName As String = NameOf(PartNomEntity.ElementAutoId)

    ''' <summary> Gets or sets the name of the quaternary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the quaternary identifier field. </value>
    Public Overrides Property QuaternaryIdFieldName As String = NameOf(PartNomEntity.NomTraitAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PartNomBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PartNomBuilder
        If PartNomBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PartNomBuilder._Instance = New PartNomBuilder()
            End SyncLock
        End If
        Return PartNomBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="PartNomNub"/> based on the
''' <see cref="IThreeToMany">interface</see>.
''' </summary>
''' <remarks> David, 6/16/2020. </remarks>
<Table("PartNom")>
Public Class PartNomNub
    Inherits ThreeToManyNub
    Implements IThreeToMany

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IThreeToMany
        Return New PartNomNub
    End Function

End Class

''' <summary>
''' The <see cref="PartNomEntity"/> based on the
''' <see cref="IThreeToMany">interface</see>.
''' </summary>
''' <remarks> David, 6/16/2020. </remarks>
Public Class PartNomEntity
    Inherits EntityBase(Of IThreeToMany, PartNomNub)
    Implements IThreeToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    Public Sub New()
        Me.New(New PartNomNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="value"> The Part-Element interface. </param>
    Public Sub New(ByVal value As IThreeToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IThreeToMany, ByVal store As IThreeToMany)
        MyBase.New(New PartNomNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PartNomBuilder.TableName, NameOf(IThreeToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IThreeToMany
        Return New PartNomNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IThreeToMany
        Dim destination As IThreeToMany = Me.CreateNew
        PartNomNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IThreeToMany)
        PartNomNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="value"> The Part-Element interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IThreeToMany)
        ' first make the copy to notify of any property change.
        PartNomNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches a <see cref="PartNomEntity"/> using the entity key. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTraitAutoId"> Identifies the <see cref="Entities.NomTraitEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal multimeterId As Integer,
                                            ByVal elementAutoId As Integer, ByVal nomTraitAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As PartNomNub = PartNomEntity.FetchNubs(connection, partAutoId, multimeterId, elementAutoId, nomTraitAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Fetches a <see cref="PartNomEntity"/> using the entity key. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PartAutoId, Me.MultimeterId, Me.ElementAutoId, Me.NomTraitAutoId)
    End Function

    ''' <summary>
    ''' Fetches an existing <see cref="PartNomEntity"/> using the entity unique index.
    ''' </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PartAutoId, Me.MultimeterId, Me.ElementAutoId, Me.NomTraitAutoId)
    End Function

    ''' <summary>
    ''' Fetches an existing <see cref="PartNomEntity"/> using the entity unique index.
    ''' </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTraitAutoId"> Identifies the <see cref="Entities.NomTraitEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal multimeterId As Integer,
                                                    ByVal elementAutoId As Integer, ByVal nomTraitAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, partAutoId, multimeterId, elementAutoId, nomTraitAutoId)
    End Function

    ''' <summary>
    ''' Tries to fetch an existing <see cref="Entities.NomTraitEntity"/> associated with a
    ''' <see cref="Entities.PartEntity"/> and <see cref="Entities.ElementEntity"/>;
    ''' otherwise, inserts a new <see cref="Entities.NomTraitEntity"/>. Then tries to fetch an
    ''' existing or insert a new
    ''' <see cref="PartNomEntity"/>.
    ''' </summary>
    ''' <remarks>
    ''' Assumes that a <see cref="Entities.PartEntity"/> exists for the specified
    ''' <paramref name="partAutoId"/>
    ''' Assumes that a <see cref="Entities.ElementEntity"/> exists for the specified
    ''' <paramref name="elementAutoId"/>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">      Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="nomTraitTypeId"> Identifies the <see cref="NomTraitType"/>. </param>
    ''' <param name="amount">         The amount. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainNomTrail(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal multimeterId As Integer,
                                      ByVal elementAutoId As Integer, ByVal nomTypeId As Integer, ByVal nomTraitTypeId As Integer,
                                      ByVal amount As Double) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._NomTraitEntity = PartNomEntity.FetchNomTrait(connection, partAutoId, elementAutoId, nomTypeId, nomTraitTypeId)
        If Not Me._NomTraitEntity.IsClean Then
            Me._NomTraitEntity = New NomTraitEntity With {.NomTypeId = nomTypeId, .NomTraitTypeId = nomTraitTypeId, .Amount = amount}
            If Not Me.NomTraitEntity.Insert(connection) Then
                result = (False, $"Failed inserting {NameOf(Entities.NomTraitEntity)} with {NameOf(Entities.NomTraitEntity.NomTypeId)} of {nomTypeId} and {NameOf(Entities.NomTraitEntity.NomTraitTypeId)} of {nomTraitTypeId}")
            End If
        End If
        If result.Success Then
            Me.PrimaryId = partAutoId
            Me.SecondaryId = elementAutoId
            Me.TernaryId = Me.NomTraitEntity.AutoId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(PartNomEntity.NomTraitEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.PartNomEntity)} with {NameOf(Entities.PartNomEntity.PartAutoId)} of {partAutoId} and {NameOf(Entities.PartNomEntity.ElementAutoId)} of {elementAutoId} and {NameOf(Entities.PartNomEntity.NomTraitAutoId)} of {Me.NomTraitEntity.AutoId}")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing <see cref="Entities.PartEntity"/> and
    ''' <see cref="Entities.ElementEntity"/> and fetches or inserts a new
    ''' <see cref="Entities.NomTraitEntity"/> and fetches an existing or inserts a new
    ''' <see cref="PartNomEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">      Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="nomTraitTypeId"> Identifies the <see cref="NomTraitType"/>. </param>
    ''' <param name="amount">         The amount. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal multimeterId As Integer,
                              ByVal elementAutoId As Integer, ByVal nomTypeId As Integer, ByVal nomTraitTypeId As Integer,
                              ByVal amount As Double) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.PartEntity Is Nothing OrElse Me.PartEntity.AutoId <> partAutoId Then
            ' make sure we have a part associated with the part auto ID.
            Me._PartEntity = New PartEntity With {.AutoId = partAutoId}
            If Not Me._PartEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.AutoId)} of {partAutoId}")
            End If
        End If
        If Me.MultimeterEntity Is Nothing OrElse Me.MultimeterEntity.Id <> multimeterId Then
            ' make sure we have an multimeter associated with the multimeter ID.
            Me._MultimeterEntity = New MultimeterEntity With {.Id = multimeterId}
            If Not Me._MultimeterEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.MultimeterEntity)} with {NameOf(Entities.MultimeterEntity.Id)} of {multimeterId}")
            End If
        End If
        If Me.ElementEntity Is Nothing OrElse Me.ElementEntity.AutoId <> elementAutoId Then
            ' make sure we have an element associated with the element auto ID.
            Me._ElementEntity = New ElementEntity With {.AutoId = elementAutoId}
            If Not Me._ElementEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.ElementEntity)} with {NameOf(Entities.ElementEntity.AutoId)} of {elementAutoId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainNomTrail(connection, partAutoId, multimeterId, elementAutoId, nomTypeId, nomTraitTypeId, amount)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(PartNomEntity.PartEntity))
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing <see cref="Entities.PartEntity"/> and
    ''' <see cref="Entities.ElementEntity"/> and fetches or inserts a new
    ''' <see cref="Entities.NomTraitEntity"/> and fetches an existing or inserts a new
    ''' <see cref="PartNomEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="nomTypeId">      Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="nomTraitTypeId"> Identifies the <see cref="NomTraitType"/>. </param>
    ''' <param name="amount">         The amount. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal nomTypeId As Integer, ByVal nomTraitTypeId As Integer,
                              ByVal amount As Double) As (Success As Boolean, Details As String)
        Return Me.TryObtain(connection, Me.PartAutoId, Me.MultimeterId, Me.ElementAutoId, nomTypeId, nomTraitTypeId, amount)
    End Function

    ''' <summary>
    ''' Tries to fetch existing <see cref="Entities.PartEntity"/> and
    ''' <see cref="Entities.ElementEntity"/> and fetches or inserts a new
    ''' <see cref="Entities.NomTraitEntity"/> and fetches an existing or inserts a new
    ''' <see cref="PartNomEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">      Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="nomTraitTypeId"> Identifies the <see cref="NomTraitType"/>. </param>
    ''' <param name="amount">         The amount. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Function Obtain(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal multimeterId As Integer,
                                     ByVal elementAutoId As Integer, ByVal nomTypeId As Integer, ByVal nomTraitTypeId As Integer,
                                     ByVal amount As Double) As Boolean
        Dim r As (Success As Boolean, Details As String) = Me.TryObtain(connection, partAutoId, multimeterId, elementAutoId, nomTypeId, nomTraitTypeId, amount)
        If Not r.Success Then Throw New OperationFailedException(r.Details)
        Return r.Success
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IThreeToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PartNomEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId, entity.TernaryId, entity.QuaternaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="partAutoId">    Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer,
                                            ByVal multimeterId As Integer, ByVal elementAutoId As Integer) As Boolean
        Return connection.Delete(Of PartNomNub)(New PartNomNub With {.PrimaryId = partAutoId,
                                                                                                     .SecondaryId = multimeterId, .TernaryId = elementAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="PartNomEntity"/>'s. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Part-Element entities. </value>
    Public ReadOnly Property PartMeterElementNominals As IEnumerable(Of PartNomEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PartNomEntity)
        Return If(usingNativeTracking, PartNomEntity.Populate(connection.GetAll(Of IThreeToMany)), PartNomEntity.Populate(connection.GetAll(Of PartNomNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._PartMeterElementNominals = PartNomEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(PartNomEntity.PartMeterElementNominals))
        Return If(Me.PartMeterElementNominals?.Any, Me.PartMeterElementNominals.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PartNomNub)) As IEnumerable(Of PartNomEntity)
        Dim l As New List(Of PartNomEntity)
        If nubs?.Any Then
            For Each nub As PartNomNub In nubs
                l.Add(New PartNomEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IThreeToMany)) As IEnumerable(Of PartNomEntity)
        Dim l As New List(Of PartNomEntity)
        If interfaces?.Any Then
            Dim nub As New PartNomNub
            For Each iFace As IThreeToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PartNomEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>
    ''' Count <see cref="PartNomEntity"/>'s by <see cref="Entities.PartEntity"/>; returns up to Meter
    ''' Entities Count * Element entities count * Nominal Types Count * Nominal Traits count.
    ''' </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PartNomBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartNomNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the <see cref="PartNomEntity"/>'s by <see cref="Entities.PartEntity"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of PartNomEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PartNomBuilder.TableName}] WHERE {NameOf(PartNomNub.PrimaryId)} = @Id", New With {Key .Id = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return PartNomEntity.Populate(connection.Query(Of PartNomNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Count <see cref="PartNomEntity"/>'s by <see cref="Entities.ElementEntity"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of entities by Element. </returns>
    Public Shared Function CountEntitiesByElement(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PartNomBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartNomNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the <see cref="PartNomEntity"/>'s by Elements in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Elements in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByElement(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of PartNomEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PartNomBuilder.TableName}] WHERE {NameOf(PartNomNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return PartNomEntity.Populate(connection.Query(Of PartNomNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count <see cref="PartNomEntity"/>'s; returns 1 or 0. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTraitAutoId"> Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal multimeterId As Integer,
                                         ByVal elementAutoId As Integer, ByVal nomTraitAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PartNomBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartNomNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        sqlBuilder.Where($"{NameOf(PartNomNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(PartNomNub.TernaryId)} = @TernaryId", New With {.TernaryId = nomTraitAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches <see cref="PartNomNub"/>'s; expects single entity or none. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTraitAutoId"> Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal multimeterId As Integer,
                                     ByVal elementAutoId As Integer, ByVal nomTraitAutoId As Integer) As IEnumerable(Of PartNomNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PartNomBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartNomNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        sqlBuilder.Where($"{NameOf(PartNomNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = multimeterId})
        sqlBuilder.Where($"{NameOf(PartNomNub.TernaryId)} = @TernaryId", New With {.TernaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(PartNomNub.QuaternaryId)} = @QuaternaryId", New With {.QuaternaryId = nomTraitAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        ' Return connection.Query(Of PartNomNub)(selector.RawSql, selector.Parameters, transaction:=TryCast(connection, TransactedConnection)?.Transaction)
        Return connection.Query(Of PartNomNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="PartNomEntity"/> exists. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTraitAutoId"> Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal multimeterId As Integer,
                                    ByVal elementAutoId As Integer, ByVal nomTraitAutoId As Integer) As Boolean
        Return 1 = PartNomEntity.CountEntities(connection, partAutoId, multimeterId, elementAutoId, nomTraitAutoId)
    End Function

#End Region

#Region " RELATIONS: PART "

    ''' <summary> Gets or sets the <see cref="Entities.PartEntity"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Part entity. </value>
    Public ReadOnly Property PartEntity As PartEntity

    ''' <summary> Fetches  <see cref="Entities.PartEntity"/>. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Part Entity. </returns>
    Public Function FetchPartEntity(ByVal connection As System.Data.IDbConnection) As PartEntity
        Dim entity As New PartEntity()
        entity.FetchUsingKey(connection, Me.PartAutoId)
        Me._PartEntity = entity
        Return entity
    End Function

    ''' <summary>
    ''' Count <see cref="Entities.PartEntity"/>'s associated with the specified
    ''' <paramref name="elementAutoId"/>; expected 1.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of Parts. </returns>
    Public Shared Function CountParts(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{PartNomBuilder.TableName}] WHERE {NameOf(PartNomNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the <see cref="Entities.PartEntity"/>'s associated with the specified
    ''' <paramref name="elementAutoId"/>; expected a single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Parts in this collection.
    ''' </returns>
    Public Shared Function FetchParts(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PartNomBuilder.TableName}] WHERE {NameOf(PartNomNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ElementEntity)
        For Each nub As PartNomNub In connection.Query(Of PartNomNub)(selector.RawSql, selector.Parameters)
            Dim entity As New PartNomEntity(nub)
            l.Add(entity.FetchElementEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Parts associated with the specified <paramref name="elementAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteParts(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{PartNomBuilder.TableName}] WHERE {NameOf(PartNomNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

#End Region

#Region " RELATIONS: MULTIMETER "

    ''' <summary> Gets or sets the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="Entities.MultimeterEntity"/>. </value>
    Public ReadOnly Property MultimeterEntity As MultimeterEntity

    ''' <summary> Fetches a Multimeter Entity. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The <see cref="Entities.MultimeterEntity"/>. </returns>
    Public Function FetchMultimeterEntity(ByVal connection As System.Data.IDbConnection) As MultimeterEntity
        Dim entity As New MultimeterEntity()
        entity.FetchUsingKey(connection, Me.MultimeterId)
        Me._MultimeterEntity = entity
        Return entity
    End Function

#End Region

#Region " RELATIONS: ELEMENT "

    ''' <summary> Gets or sets the Element entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Element entity. </value>
    Public ReadOnly Property ElementEntity As ElementEntity

    ''' <summary> Fetches a Element Entity. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Element Entity. </returns>
    Public Function FetchElementEntity(ByVal connection As System.Data.IDbConnection) As ElementEntity
        Dim entity As New ElementEntity()
        entity.FetchUsingKey(connection, Me.ElementAutoId)
        Me._ElementEntity = entity
        Return entity
    End Function

    ''' <summary> Count elements. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> The total number of elements. </returns>
    Public Shared Function CountElements(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{PartNomBuilder.TableName}] WHERE {NameOf(PartNomNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Elements in this collection. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Elements in this collection.
    ''' </returns>
    Public Shared Function FetchElements(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PartNomBuilder.TableName}] WHERE {NameOf(PartNomNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ElementEntity)
        For Each nub As PartNomNub In connection.Query(Of PartNomNub)(selector.RawSql, selector.Parameters)
            Dim entity As New PartNomEntity(nub)
            l.Add(entity.FetchElementEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Element related to the specified Part. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteElements(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{PartNomBuilder.TableName}] WHERE {NameOf(PartNomNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = partAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.ElementEntity"/>'s. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="partAutoId">  Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered elements in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchOrderedElements(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal partAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .Id = partAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return ElementEntity.Populate(connection.Query(Of ElementNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.ElementEntity"/>'s. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered elements in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchOrderedElements(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [PartMeterElementNominal] on [PartMeterElementNominal].SecondaryId = [Element].AutoId where [PartMeterElementNominal].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{ElementBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{ElementBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{PartNomBuilder.TableName}].{NameOf(PartNomNub.SecondaryId)} = [{ElementBuilder.TableName}].{NameOf(ElementNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{PartNomBuilder.TableName}].{NameOf(PartNomNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{ElementBuilder.TableName}].{NameOf(ElementNub.Label)} ASC; ")
        Return PartNomEntity.FetchOrderedElements(connection, queryBuilder.ToString, partAutoId)
    End Function

    ''' <summary> Fetches an <see cref="Entities.ElementEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="selectQuery">  The select query. </param>
    ''' <param name="partAutoId">   Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementLabel"> The <see cref="Entities.ElementEntity"/>label. </param>
    ''' <returns> The element. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchElement(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal partAutoId As Integer,
                                        ByVal multimeterId As Integer, ByVal elementLabel As String) As ElementEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.PartAutoId = partAutoId,
                                                                                                     .MultimeterId = multimeterId,
                                                                                                     .ElementLabel = elementLabel})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As ElementNub = connection.Query(Of ElementNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New ElementEntity, New ElementEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an <see cref="Entities.ElementEntity"/>. </summary>
    ''' <remarks>
    ''' David, 5/19/2020.
    ''' <code>
    ''' SELECT [Element].*
    ''' FROM [Element] Inner Join [PartMeterElementNominal]
    ''' ON [PartMeterElementNominal].[SecondaryId] = [Element].[AutoId]
    ''' WHERE ([PartMeterElementNominal].[PrimaryId] = 8 AND [Element].[Label] = 'R1')
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="partAutoId">   Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementLabel"> The <see cref="Entities.ElementEntity"/>label. </param>
    ''' <returns> The element. </returns>
    Public Shared Function FetchElement(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal multimeterId As Integer,
                                        ByVal elementLabel As String) As ElementEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [PartMeterElementNominal] on [PartMeterElementNominal].SecondaryId = [Element].AutoId where [PartMeterElementNominal].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{ElementBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{ElementBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.SecondaryId)}] = [{ElementBuilder.TableName}].[{NameOf(ElementNub.AutoId)}]")
        queryBuilder.AppendLine($"WHERE ([{PartNomBuilder.TableName}].[{NameOf(PartNomNub.PrimaryId)}] = @PartAutoId ")
        queryBuilder.AppendLine($"AND [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.SecondaryId)}] = @MultimeterId ")
        queryBuilder.AppendLine($"AND [{ElementBuilder.TableName}].[{NameOf(ElementNub.Label)}] = @ElementLabel); ")
        Return PartNomEntity.FetchElement(connection, queryBuilder.ToString, partAutoId, multimeterId, elementLabel)
    End Function

#End Region

#Region " RELATIONS: NOMINAL TRAITS "

    ''' <summary> Gets or sets the <see cref="Entities.NomTraitEntity"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="Entities.NomTraitEntity"/>. </value>
    Public ReadOnly Property NomTraitEntity As NomTraitEntity

    ''' <summary> Fetches a <see cref="Entities.NomTraitEntity"/>. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The <see cref="Entities.NomTraitEntity"/>. </returns>
    Public Function FetchNomTraitEntity(ByVal connection As System.Data.IDbConnection) As NomTraitEntity
        Me._NomTraitEntity = PartNomEntity.FetchNomTrait(connection, Me.PartAutoId, Me.MultimeterId, Me.ElementAutoId, Me.NomTraitAutoId)
        Return Me._NomTraitEntity
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.NomTraitEntity"/>'s. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="selectQuery">   The select query. </param>
    ''' <param name="partAutoId">    Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered
    ''' <see cref="Entities.NomTraitEntity"/>'s in this collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchOrderedNomTraits(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String,
                                                 ByVal partAutoId As Integer, ByVal multimeterId As Integer, ByVal elementAutoId As Integer) As IEnumerable(Of NomTraitEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.PartAutoId = partAutoId,
                                                                                                     .MultimeterId = multimeterId,
                                                                                                     .ElementAutoId = elementAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return NomTraitEntity.Populate(connection.Query(Of NomTraitNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.NomTraitEntity"/>'s. </summary>
    ''' <remarks>
    ''' David, 5/9/2020.
    ''' <code>
    ''' SELECT [NomTrait].*
    ''' FROM [NomTrait] Inner Join [PartMeterElementNominal]
    ''' ON [PartMeterElementNominal].[TernaryId] = [NomTrait].[AutoId]
    ''' WHERE ([PartMeterElementNominal].[PrimaryId] = 8 AND [PartMeterElementNominal].[SecondaryId] = 1  AND [PartMeterElementNominal].[TernaryId] = 1)
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="partAutoId">    Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered
    ''' <see cref="Entities.NomTraitEntity"/>'s in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedNomTraits(ByVal connection As System.Data.IDbConnection,
                                                 ByVal partAutoId As Integer, ByVal multimeterId As Integer, ByVal elementAutoId As Integer) As IEnumerable(Of NomTraitEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [PartMeterElementNominal] on [PartMeterElementNominal].SecondaryId = [NomTrait].AutoId where [PartMeterElementNominal].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{NomTraitBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{NomTraitBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{PartNomBuilder.TableName}].{NameOf(PartNomNub.QuaternaryId)} = [{NomTraitBuilder.TableName}].{NameOf(NomTraitNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{PartNomBuilder.TableName}].[{NameOf(PartNomNub.PrimaryId)}] = @PartAutoId ")
        queryBuilder.AppendLine($"AND [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.SecondaryId)}] = @MultimeterId ")
        queryBuilder.AppendLine($"AND [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.TernaryId)}] = @ElementAutoId) ")
        queryBuilder.AppendLine($"ORDER BY [{NomTraitBuilder.TableName}].{NameOf(NomTraitNub.FirstForeignId)} ASC, [{NomTraitBuilder.TableName}].{NameOf(NomTraitNub.SecondForeignId)} ASC; ")
        Return PartNomEntity.FetchOrderedNomTraits(connection, queryBuilder.ToString, partAutoId, multimeterId, elementAutoId)
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.NomTraitEntity"/>'s. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="selectQuery">   The select query. </param>
    ''' <param name="partAutoId">    Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered
    ''' <see cref="Entities.NomTraitEntity"/>'s in this collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchOrderedNomTraits(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal partAutoId As Integer,
                                                 ByVal multimeterId As Integer, ByVal elementAutoId As Integer,
                                                 ByVal nomTypeId As Integer) As IEnumerable(Of NomTraitEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.PartAutoId = partAutoId,
                                                                                                     .MultimeterId = multimeterId,
                                                                                                     .ElementAutoId = elementAutoId,
                                                                                                     .NomTypeId = nomTypeId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return NomTraitEntity.Populate(connection.Query(Of NomTraitNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.NomTraitEntity"/>'s. </summary>
    ''' <remarks>
    ''' David, 5/9/2020.
    ''' <code>
    ''' SELECT [NomTrait].*
    ''' FROM [NomTrait] Inner Join [PartMeterElementNominal]
    ''' ON [PartMeterElementNominal].[TernaryId] = [NomTrait].[AutoId]
    ''' WHERE ([PartMeterElementNominal].[PrimaryId] = 8 AND [PartMeterElementNominal].[SecondaryId] = 1 And [NomTrait].[FirstForeignId]=1)
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="partAutoId">    Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered
    ''' <see cref="Entities.NomTraitEntity"/>'s in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedNomTraits(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal multimeterId As Integer,
                                                 ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As IEnumerable(Of NomTraitEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"SELECT [{NomTraitBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{NomTraitBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{PartNomBuilder.TableName}].{NameOf(PartNomNub.QuaternaryId)} = [{NomTraitBuilder.TableName}].{NameOf(NomTraitNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{PartNomBuilder.TableName}].[{NameOf(PartNomNub.PrimaryId)}] = @{NameOf(partAutoId)} ")
        queryBuilder.AppendLine($"AND [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.SecondaryId)}] = @{NameOf(multimeterId)} ")
        queryBuilder.AppendLine($"AND [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.TernaryId)}] = @{NameOf(elementAutoId)} ")
        queryBuilder.AppendLine($"AND [{NomTraitBuilder.TableName}].[{NameOf(NomTraitNub.FirstForeignId)}] = @{NameOf(nomTypeId)}) ")
        queryBuilder.AppendLine($"ORDER BY [{NomTraitBuilder.TableName}].{NameOf(NomTraitNub.SecondForeignId)} ASC; ")
        Return PartNomEntity.FetchOrderedNomTraits(connection, queryBuilder.ToString, partAutoId, multimeterId, elementAutoId)
    End Function

    ''' <summary> Fetches an <see cref="Entities.NomTraitEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="selectQuery">    The select query. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTraitAutoId"> Identifies the <see cref="Entities.NomTraitEntity"/>. </param>
    ''' <returns> The Nominal Trait Entity. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchNomTrait(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal partAutoId As Integer,
                                         ByVal multimeterId As Integer, ByVal elementAutoId As Integer, ByVal nomTraitAutoId As Integer) As NomTraitEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.PartAutoId = partAutoId,
                                                                                                     .MultimeterId = multimeterId,
                                                                                                     .ElementAutoId = elementAutoId,
                                                                                                     .NomTraitAutoId = nomTraitAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As NomTraitNub = connection.Query(Of NomTraitNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New NomTraitEntity, New NomTraitEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an <see cref="Entities.NomTraitEntity"/>. </summary>
    ''' <remarks>
    ''' David, 6/16/2020.
    ''' <code>
    ''' SELECT [NomTrait].*
    ''' FROM [NomTrait] Inner Join [PartMeterElementNominal]
    ''' ON [PartMeterElementNominal].[TernaryId] = [NomTrait].[AutoId]
    ''' WHERE ([PartMeterElementNominal].[PrimaryId] = 8 AND [PartMeterElementNominal].[SecondaryId] = 1 AND [PartMeterElementNominal].[TernaryId] = 1 )
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTraitAutoId"> Identifies the <see cref="Entities.NomTraitEntity"/>. </param>
    ''' <returns> The Nom Trait Entity. </returns>
    Public Shared Function FetchNomTrait(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal multimeterId As Integer,
                                             ByVal elementAutoId As Integer, ByVal nomTraitAutoId As Integer) As NomTraitEntity
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"SELECT [{NomTraitBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{NomTraitBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.TernaryId)}] = [{NomTraitBuilder.TableName}].[{NameOf(NomTraitNub.AutoId)}]")
        queryBuilder.AppendLine($"WHERE ([{PartNomBuilder.TableName}].[{NameOf(PartNomNub.PrimaryId)}] = @{NameOf(partAutoId)} ")
        queryBuilder.AppendLine($"AND [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.SecondaryId)}] = @{NameOf(multimeterId)} ")
        queryBuilder.AppendLine($"AND [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.TernaryId)}] = @{NameOf(elementAutoId)} ")
        queryBuilder.AppendLine($"AND [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.QuaternaryId)}] = @{NameOf(nomTraitAutoId)}); ")
        Return PartNomEntity.FetchNomTrait(connection, queryBuilder.ToString, partAutoId, multimeterId, elementAutoId, nomTraitAutoId)
    End Function

    ''' <summary> Fetches an <see cref="Entities.NomTraitEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="selectQuery">    The select query. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">      Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="nomTraitTypeId"> Identifies the <see cref="NomTraitTypeEntity"/>. </param>
    ''' <returns> The Nom Trait entity. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchNomTrait(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal partAutoId As Integer,
                                         ByVal multimeterId As Integer, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer,
                                         ByVal nomTraitTypeId As Integer) As NomTraitEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.PartAutoId = partAutoId,
                                                                                                     .MultimeterId = multimeterId,
                                                                                                     .ElementAutoId = elementAutoId,
                                                                                                     .NomTypeId = nomTypeId,
                                                                                                     .NomTraitTypeId = nomTraitTypeId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As NomTraitNub = connection.Query(Of NomTraitNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New NomTraitEntity, New NomTraitEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an <see cref="Entities.NomTraitEntity"/>. </summary>
    ''' <remarks>
    ''' David, 6/16/2020.
    ''' <code>
    ''' SELECT [NomTrait].*
    ''' FROM [NomTrait] Inner Join [PartMeterElementNominal]
    ''' ON [PartMeterElementNominal].[TernaryId] = [NomTrait].[AutoId]
    ''' WHERE ([PartMeterElementNominal].[PrimaryId] = 8 AND [NomTrait].[FirstForeignId] = 1 AND [NomTrait].[SecondForeignId] = 1 )
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="partAutoId">     Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">  Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">      Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="nomTraitTypeId"> Identifies the <see cref="NomTraitTypeEntity"/>. </param>
    ''' <returns> The Nom Trait entity. </returns>
    Public Shared Function FetchNomTrait(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer,
                                         ByVal multimeterId As Integer, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer,
                                         ByVal nomTraitTypeId As Integer) As NomTraitEntity
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"SELECT [{NomTraitBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{NomTraitBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.TernaryId)}] = [{NomTraitBuilder.TableName}].[{NameOf(NomTraitNub.AutoId)}]")
        queryBuilder.AppendLine($"WHERE ([{PartNomBuilder.TableName}].[{NameOf(PartNomNub.PrimaryId)}] = @{NameOf(partAutoId)} ")
        queryBuilder.AppendLine($"AND [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.SecondaryId)}] = @{NameOf(multimeterId)} ")
        queryBuilder.AppendLine($"AND [{PartNomBuilder.TableName}].[{NameOf(PartNomNub.TernaryId)}] = @{NameOf(elementAutoId)} ")
        queryBuilder.AppendLine($"AND [{NomTraitBuilder.TableName}].[{NameOf(NomTraitNub.FirstForeignId)}] = @{NameOf(nomTypeId)} ")
        queryBuilder.AppendLine($"AND [{NomTraitBuilder.TableName}].[{NameOf(NomTraitNub.SecondForeignId)}] = @{NameOf(nomTraitTypeId)}); ")
        Return PartNomEntity.FetchNomTrait(connection, queryBuilder.ToString, partAutoId, multimeterId, elementAutoId, nomTypeId, nomTraitTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IThreeToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PartNomEntity.PartAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.PartEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.PartEntity"/> </value>
    Public Property PartAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IThreeToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PartNomEntity.MultimeterId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/>.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Ternary reference. </summary>
    ''' <value> The identifier of Ternary reference. </value>
    Public Property TernaryId As Integer Implements IThreeToMany.TernaryId
        Get
            Return Me.ICache.TernaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.TernaryId, value) Then
                Me.ICache.TernaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PartNomEntity.ElementAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public Property ElementAutoId As Integer
        Get
            Return Me.TernaryId
        End Get
        Set(value As Integer)
            Me.TernaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Ternary reference. </summary>
    ''' <value> The identifier of Ternary reference. </value>
    Public Property QuaternaryId As Integer Implements IThreeToMany.QuaternaryId
        Get
            Return Me.ICache.QuaternaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.QuaternaryId, value) Then
                Me.ICache.QuaternaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PartNomEntity.NomTraitAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NomTraitEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NomTraitEntity"/>. </value>
    Public Property NomTraitAutoId As Integer
        Get
            Return Me.QuaternaryId
        End Get
        Set(value As Integer)
            Me.QuaternaryId = value
        End Set
    End Property

#End Region

End Class

''' <summary>
''' Collection of unique <see cref="Entities.PartEntity"/> +
''' <see cref="Entities.MultimeterEntity"/> +
''' <see cref="Entities.ElementEntity"/> <see cref="NomTraitEntity"/>'s.
''' </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class PartNomEntityCollection
    Inherits NomTraitEntityCollection
    Implements isr.Core.Constructs.IGetterSetter(Of Double)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="part">       The <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeter"> The <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="element">    The <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">  Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    Public Sub New(ByVal part As PartEntity, ByVal multimeter As MultimeterEntity, ByVal element As ElementEntity, ByVal nomTypeId As Integer)
        MyBase.New()
        Me.PartAutoId = part.AutoId
        Me.MultimeterId = multimeter.Id
        Me.ElementAutoId = element.AutoId
        Me._UniqueIndexDictionary = New Dictionary(Of ThreeKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, ThreeKeySelector)
        Me._NomTrait = New NomTrait(Me, element, nomTypeId, multimeter)
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of ThreeKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, ThreeKeySelector)

    ''' <summary> Gets Identifies the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NomTypeEntity"/>. </value>
    Public ReadOnly Property NomTypeId As Integer
        Get
            Return Me.NomTrait.NomType
        End Get
    End Property

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As NomTraitEntity)
        If entity.NomTypeId = Me.NomTypeId Then
            MyBase.Add(entity)
            Me._PrimaryKeyDictionary.Add(entity.NomTraitTypeId, entity.EntitySelector)
            Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.NomTraitTypeId)
            Me.NotifyPropertyChanged(NomTraitTypeEntity.EntityLookupDictionary(entity.NomTraitTypeId).Label)
        End If
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains a key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="nomTraitTypeId"> Identifies the <see cref="NomTraitTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsTrait(ByVal nomTraitTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(nomTraitTypeId)
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.PartEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.PartEntity"/>. </value>
    Public ReadOnly Property PartAutoId As Integer

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public ReadOnly Property ElementAutoId As Integer

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/>.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public ReadOnly Property MultimeterId As Integer

#End Region

#Region " GETTER SETTER "

    ''' <summary>
    ''' Gets the Real(Double)-value for the given <see cref="NomTraitTypeEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable Double. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double? Implements Core.Constructs.IGetterSetter(Of Double).Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary>
    ''' Set the specified element value for the given <see cref="NomTraitTypeEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value">                                      value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>    ''' <returns> A Double. </returns>
    Protected Function Setter(ByVal value As Double, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double Implements Core.Constructs.IGetterSetter(Of Double).Setter
        Me.Setter(Me.ToKey(name), value)
        Me.NotifyPropertyChanged(name)
        Return value
    End Function

    ''' <summary> Gets or sets the Nominal Trait. </summary>
    ''' <value> The Nominal Trait. </value>
    Public ReadOnly Property NomTrait As NomTrait

#End Region

#Region " TRAIT SELECTION "

    ''' <summary>
    ''' Converts a name to a key using the
    ''' <see cref="NomTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
    ''' extend the element Nominal Traits beyond the values of the enumeration type that is used to
    ''' populate this table.
    ''' </summary>
    ''' <remarks> David, 5/11/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return NomTraitTypeEntity.KeyLookupDictionary(name)
    End Function

    ''' <summary> Sets the trait value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Double. </returns>
    Public Function Setter(ByVal value As Double?, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        If value.HasValue AndAlso Not Nullable.Equals(value, Me.Getter(name)) Then
            Me.Setter(Me.ToKey(name), value.Value)
            Me.NotifyPropertyChanged(name)
        End If
        Return value
    End Function

#If False Then
    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.Getter(Me.ToKey(name))
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.Setter(Me.ToKey(name), value.Value)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property
#End If

    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="nomTraitTypeId"> Identifies the <see cref="NomTraitTypeEntity"/>. </param>
    ''' <returns> An elementReadingRealEntity. </returns>
    Public Function Entity(ByVal nomTraitTypeId As Integer) As NomTraitEntity
        Return If(Me.ContainsTrait(nomTraitTypeId), Me(Me._PrimaryKeyDictionary(nomTraitTypeId)), New NomTraitEntity)
    End Function

    ''' <summary> Gets the Real(Double)-value for the given Trait Type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="nomTraitTypeId"> Identifies the <see cref="NomTraitTypeEntity"/>. </param>
    ''' <returns> A Double? </returns>
    Public Function Getter(ByVal nomTraitTypeId As Integer) As Double?
        Return If(Me.ContainsTrait(nomTraitTypeId), Me(Me._PrimaryKeyDictionary(nomTraitTypeId)).Amount, New Double?)
    End Function

    ''' <summary> Set the specified element value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="nomTraitTypeId"> Identifies the <see cref="NomTraitTypeEntity"/>. </param>
    ''' <param name="value">          The value. </param>
    Public Sub Setter(ByVal nomTraitTypeId As Integer, ByVal value As Double)
        If Me.ContainsTrait(nomTraitTypeId) Then
            Me(Me._PrimaryKeyDictionary(nomTraitTypeId)).Amount = value
        Else
            Me.Add(New NomTraitEntity With {.NomTypeId = Me.NomTypeId, .NomTraitTypeId = nomTraitTypeId, .Amount = value})
        End If
    End Sub

#End Region

#Region " UPSERT "

    ''' <summary> Inserts or updates all entities using the given connection. </summary>
    ''' <remarks>
    ''' David, 5/13/2020. Entities that failed to save are enumerated in
    ''' <see cref="P:isr.Dapper.Entity.EntityKeyedCollection`4.UnsavedKeys" />
    ''' </remarks>
    ''' <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <CLSCompliant(False)>
    Public Overrides Function Upsert(ByVal transcactedConnection As TransactedConnection) As Boolean
        Me.ClearUnsavedKeys()
        ' dictionary is instantiated only after the collection has values.
        If Not Me.Any Then Return True
        Dim partMeterElementNominal As PartNomEntity
        For Each keyValue As KeyValuePair(Of ThreeKeySelector, NomTraitEntity) In Me.Dictionary
            If keyValue.Value.Upsert(transcactedConnection) Then
                partMeterElementNominal = New PartNomEntity() With {.PartAutoId = Me.PartAutoId,
                                                                    .MultimeterId = Me.MultimeterId,
                                                                    .ElementAutoId = Me.ElementAutoId,
                                                                    .NomTraitAutoId = keyValue.Value.AutoId}
                Try
                    If Not partMeterElementNominal.Obtain(transcactedConnection) Then
                        Me.AddUnsavedKey(keyValue.Key)
                    End If
                Catch
                    Throw
                End Try
            Else
                Me.AddUnsavedKey(keyValue.Key)
            End If
        Next

        ' success if no unsaved keys
        Return Not Me.UnsavedKeys.Any
    End Function

#End Region

End Class
