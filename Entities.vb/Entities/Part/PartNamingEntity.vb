Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Part Naming builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class PartNamingBuilder
    Inherits OneToManyLabelBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PartNamingBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PartNamingBuilder._TableName) Then
                PartNamingBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PartNamingNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = PartBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(PartNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = PartNamingTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(PartNamingTypeNub.Id)

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 63

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(PartNamingEntity.PartAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(PartNamingEntity.PartNamingTypeId)

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the label field. </value>
    Public Overrides Property LabelFieldName As String = NameOf(PartNamingEntity.Label)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PartNamingBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PartNamingBuilder
        If PartNamingBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PartNamingBuilder._Instance = New PartNamingBuilder()
            End SyncLock
        End If
        Return PartNamingBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="Entities.PartEntity"/> Naming table
''' <see cref="IOneToManyLabel">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("PartNaming")>
Public Class PartNamingNub
    Inherits OneToManyLabelNub
    Implements IOneToManyLabel

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToManyLabel
        Return New PartNamingNub
    End Function

End Class

''' <summary>
''' The <see cref="PartNamingEntity"/>. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class PartNamingEntity
    Inherits EntityBase(Of IOneToManyLabel, PartNamingNub)
    Implements IOneToManyLabel

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New PartNamingNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.PartEntity"/>Naming interface. </param>
    Public Sub New(ByVal value As IOneToManyLabel)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToManyLabel, ByVal store As IOneToManyLabel)
        MyBase.New(New PartNamingNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PartNamingBuilder.TableName, NameOf(IOneToManyLabel).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToManyLabel
        Return New PartNamingNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOneToManyLabel
        Dim destination As IOneToManyLabel = Me.CreateNew
        PartNamingNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="PartNamingEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToManyLabel)
        PartNamingNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached Naming, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.PartEntity"/>Naming interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToManyLabel)
        ' first make the copy to notify of any property change.
        PartNamingNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="partAutoId">       Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="partNamingTypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal partNamingTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PartNamingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartNamingNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        sqlBuilder.Where($"{NameOf(PartNamingNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = partNamingTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of PartNamingNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="partAutoId">       Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="partNamingTypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal partNamingTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As PartNamingNub = PartNamingEntity.FetchEntities(connection, partAutoId, partNamingTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToManyLabel) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PartNamingEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="partAutoId">       Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="partNamingTypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal partNamingTypeId As Integer) As Boolean
        Return connection.Delete(Of PartNamingNub)(New PartNamingNub With {.PrimaryId = partAutoId, .SecondaryId = partNamingTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="Entities.PartEntity"/>Naming entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> the <see cref="Entities.PartEntity"/>Naming entities. </value>
    Public ReadOnly Property PartNamings As IEnumerable(Of PartNamingEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PartNamingEntity)
        Return If(usingNativeTracking, PartNamingEntity.Populate(connection.GetAll(Of IOneToManyLabel)),
                                       PartNamingEntity.Populate(connection.GetAll(Of PartNamingNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._PartNamings = PartNamingEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(PartNamingEntity.PartNamings))
        Return If(Me.PartNamings?.Any, Me.PartNamings.Count, 0)
    End Function

    ''' <summary> Count Part Namings. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{PartNamingBuilder.TableName}] WHERE {NameOf(PartNamingNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of PartNamingEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PartNamingBuilder.TableName}] WHERE {NameOf(PartNamingNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return PartNamingEntity.Populate(connection.Query(Of PartNamingNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches Part Namings by Part Auto Id. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of PartNamingNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PartNamingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartNamingEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PartNamingNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of Part Naming entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> the <see cref="Entities.PartEntity"/>Naming nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PartNamingNub)) As IEnumerable(Of PartNamingEntity)
        Dim l As New List(Of PartNamingEntity)
        If nubs?.Any Then
            For Each nub As PartNamingNub In nubs
                l.Add(New PartNamingEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Part Naming entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> the <see cref="Entities.PartEntity"/>Naming interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToManyLabel)) As IEnumerable(Of PartNamingEntity)
        Dim l As New List(Of PartNamingEntity)
        If interfaces?.Any Then
            Dim nub As New PartNamingNub
            For Each iFace As IOneToManyLabel In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PartNamingEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Part Namings by unique index; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="partAutoId">       Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="partNamingTypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal partNamingTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PartNamingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartNamingNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        sqlBuilder.Where($"{NameOf(PartNamingNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = partNamingTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches Part Namings by unique index; expected single or none. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="partAutoId">       Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="partNamingTypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal partNamingTypeId As Integer) As IEnumerable(Of PartNamingNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PartNamingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartNamingNub.PrimaryId)} = @primaryId", New With {.primaryId = partAutoId})
        sqlBuilder.Where($"{NameOf(PartNamingNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = partNamingTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PartNamingNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.PartEntity"/>Naming exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="partAutoId">       Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="partNamingtypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal partNamingtypeId As Integer) As Boolean
        Return 1 = PartNamingEntity.CountEntities(connection, partAutoId, partNamingtypeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count Namings associated with this part. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of Namings. </returns>
    Public Function CountPartNamings(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PartNamingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartNamingNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Part Naming Namings by Part auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 7/5/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchPartNamings(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of PartNamingNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PartNamingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartNamingNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PartNamingNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.PartEntity"/> Entity. </summary>
    ''' <value> the <see cref="Entities.PartEntity"/> Entity. </value>
    Public ReadOnly Property PartEntity As PartEntity

    ''' <summary> Fetches a Part Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchPartEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._PartEntity = New PartEntity()
        Return Me.PartEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

    ''' <summary>
    ''' Gets or sets the <see cref="Entities.PartEntity"/> <see cref="PartNamingTypeEntity"/>.
    ''' </summary>
    ''' <value> the <see cref="Entities.PartEntity"/> Naming type entity. </value>
    Public ReadOnly Property PartNamingTypeEntity As PartNamingTypeEntity

    ''' <summary> Fetches a part naming type Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchPartNamingTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._PartNamingTypeEntity = New PartNamingTypeEntity()
        Return Me.PartNamingTypeEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToManyLabel.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PartNamingEntity.PartAutoId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.PartEntity"/> record.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.PartEntity"/> record. </value>
    Public Property PartAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToManyLabel.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PartNamingEntity.PartNamingTypeId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identity (type) of the <see cref="entities.PartNamingEntity"/>.
    ''' </summary>
    ''' <value> Identifies the Part Naming type. </value>
    Public Property PartNamingTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the <see cref="entities.PartNamingEntity"/> label. </summary>
    ''' <value> the <see cref="entities.PartNamingEntity"/> label. </value>
    Public Property Label As String Implements IOneToManyLabel.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of <see cref="PartNamingEntity"/>'s. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class PartNamingEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToManyLabel, PartNamingNub, PartNamingEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As PartNamingEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As PartNamingEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of PartNamingEntity))
        If entities?.Any Then
            For Each entity As PartNamingEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return PartNamingBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary>
''' Collection of <see cref="Entities.PartEntity"/>-Unique <see cref="PartNamingEntity"/>'s.
''' </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class PartUniqueNamingEntityCollection
    Inherits PartNamingEntityCollection
    Implements Core.Constructs.IGetterSetter

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
        Me.PartNaming = New PartNaming With {.GetterSetter = Me}
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    Public Sub New(ByVal partAutoId As Integer)
        Me.New
        Me.PartAutoId = partAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As PartNamingEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.PartNamingTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.PartNamingTypeId)
        Me.NotifyPropertyChanged(PartNamingTypeEntity.EntityLookupDictionary(entity.PartNamingTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains 'partNamingType' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="partNamingTypeId"> Identifies the <see cref="Entities.PartNamingTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal partNamingTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(partNamingTypeId)
    End Function

#End Region

#Region " GETTER SETTER "

    ''' <summary>
    ''' Gets the Real(String)-value for the given <see cref="PartNamingEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable String. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String Implements Core.Constructs.IGetterSetter.Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary>
    ''' Set the specified element value for the given <see cref="PartNamingEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value">                                      value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A String. </returns>
    Protected Function Setter(ByVal value As String, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String Implements Core.Constructs.IGetterSetter.Setter
        If Not String.Equals(value, Me.Getter(name)) Then
            Me.Setter(Me.ToKey(name), value)
            Me.NotifyPropertyChanged(name)
        End If
        Return value
    End Function

    ''' <summary> Gets or sets the Part Naming. </summary>
    ''' <value> The Part Naming. </value>
    Public ReadOnly Property PartNaming As PartNaming

#End Region

#Region " TRAIT SELECTION "

    ''' <summary> Converts a name to a key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return PartNamingTypeEntity.KeyLookupDictionary(name)
    End Function

#If False Then
    ''' <summary> Gets or sets the naming value. </summary>
    ''' <value> The naming value. </value>
    Protected Property NamingValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String
        Get
            Return Me.Getter(Me.ToKey(name))
        End Get
        Set(value As String)
            Me.Setter(Me.ToKey(name), value)
        End Set
    End Property
#End If

    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="partNamingTypeId"> Identifies the <see cref="Entities.PartNamingTypeEntity"/>. </param>
    ''' <returns> An PartNamingEntity. </returns>
    Public Function Entity(ByVal partNamingTypeId As Integer) As PartNamingEntity
        Return If(Me.ContainsKey(partNamingTypeId), Me(Me._PrimaryKeyDictionary(partNamingTypeId)), New PartNamingEntity)
    End Function

    ''' <summary> Gets the Real(Double)-value for the given Naming type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="partNamingTypeId"> Identifies the <see cref="Entities.PartNamingTypeEntity"/>. </param>
    ''' <returns> A String. </returns>
    Public Function Getter(ByVal partNamingTypeId As Integer) As String
        Return If(Me.ContainsKey(partNamingTypeId), Me(Me._PrimaryKeyDictionary(partNamingTypeId)).Label, String.Empty)
    End Function

    ''' <summary> Set the specified Part value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="partNamingTypeId"> Identifies the
    '''                                 <see cref="Entities.PartNamingTypeEntity"/>. </param>
    ''' <param name="value">            The value. </param>
    Public Sub Setter(ByVal partNamingTypeId As Integer, ByVal value As String)
        If Me.ContainsKey(partNamingTypeId) Then
            Me(Me._PrimaryKeyDictionary(partNamingTypeId)).Label = value
        Else
            Me.Add(New PartNamingEntity With {.PartAutoId = Me.PartAutoId, .PartNamingTypeId = partNamingTypeId, .Label = value})
        End If
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.PartEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.PartEntity"/>. </value>
    Public ReadOnly Property PartAutoId As Integer

#End Region

End Class

