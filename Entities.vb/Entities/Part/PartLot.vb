Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Core

''' <summary> A Part-Lot builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class PartLotBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return PartLotBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(PartLotBuilder._TableName) Then
                PartLotBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(PartLotNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = PartBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(PartNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = LotBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(LotNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(PartLotEntity.PartAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(PartLotEntity.LotAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As PartLotBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As PartLotBuilder
        If PartLotBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                PartLotBuilder._Instance = New PartLotBuilder()
            End SyncLock
        End If
        Return PartLotBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Part Lot Nub based on the <see cref="IOneToMany">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
<Table("PartLot")>
Public Class PartLotNub
    Inherits OneToManyNub
    Implements IOneToMany

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToMany
        Return New PartLotNub
    End Function

End Class

''' <summary> The Part-Lot Entity based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class PartLotEntity
    Inherits EntityBase(Of IOneToMany, PartLotNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New PartLotNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Part-Lot interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New PartLotNub, cache, store)
        Me.UsingNativeTracking = String.Equals(PartLotBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToMany
        Return New PartLotNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        PartLotNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        PartLotNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Part-Lot interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        PartLotNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal lotAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As PartLotNub = PartLotEntity.FetchNubs(connection, partAutoId, lotAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PartAutoId, Me.LotAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PartAutoId, Me.LotAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal lotAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, partAutoId, lotAutoId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If PartLotEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal lotAutoId As Integer) As Boolean
        Return connection.Delete(Of PartLotNub)(New PartLotNub With {.PrimaryId = partAutoId, .SecondaryId = lotAutoId})
    End Function

#End Region

#Region " OBTAIN "

    ''' <summary>
    ''' Tries to fetch an existing or insert a new <see cref="Entities.LotEntity"/>  and fetch an
    ''' existing or insert a new <see cref="PartLotEntity"/>. If the lot number is non-unique, a lot
    ''' is inserted if this lot number is not associated with this part.
    ''' </summary>
    ''' <remarks>
    ''' Assumes that a <see cref="Entities.PartEntity"/> exists for the specified
    ''' <paramref name="partAutoId"/>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainLot(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal lotNumber As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If LotBuilder.Get.UsingUniqueLabel(connection) Then
            ' if using a unique lot number, the lot number is unique across all parts
            Me._LotEntity = New LotEntity() With {.LotNumber = lotNumber}
            If Not Me.LotEntity.Obtain(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.LotNumber)} of {lotNumber}")
            End If
        Else
            ' if not using a unique lot number, the lot number is unique for this part
            Me._LotEntity = PartLotEntity.FetchLot(connection, partAutoId, lotNumber)
            If Not Me.LotEntity.IsClean Then
                Me._LotEntity = New LotEntity() With {.LotNumber = lotNumber}
                If Not Me.LotEntity.Insert(connection) Then
                    result = (False, $"Failed inserting {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.LotNumber)} of {lotNumber}")
                End If
            End If
        End If
        If result.Success Then
            Me.PrimaryId = partAutoId
            Me.SecondaryId = Me.LotEntity.AutoId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(PartLotEntity.LotEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.PartLotEntity)} with {NameOf(Entities.PartLotEntity.PartAutoId)} of {partAutoId} and {NameOf(Entities.PartLotEntity.LotAutoId)} of {Me.LotEntity.AutoId}")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.PartEntity"/> and
    ''' <see cref="Entities.LotEntity"/> entities and fetches an existing or inserts a new
    ''' <see cref="PartLotEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal lotNumber As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.PartEntity Is Nothing OrElse Me.PartEntity.AutoId <> partAutoId Then
            ' make sure a part exists for the auto id.
            Me._PartEntity = New PartEntity With {.AutoId = partAutoId}
            If Not Me._PartEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.AutoId)} of {partAutoId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainLot(connection, partAutoId, lotNumber)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(PartLotEntity.PartEntity))
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.PartEntity"/> and
    ''' <see cref="Entities.LotEntity"/> entities and fetches an existing or inserts a new
    ''' <see cref="PartLotEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As (Success As Boolean, Details As String)
        Return Me.TryObtain(connection, Me.PartAutoId, lotNumber)
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.PartEntity"/> and
    ''' <see cref="Entities.LotEntity"/>
    ''' entities and fetches an existing or inserts a new <see cref="PartLotEntity"/>.
    ''' </summary>
    ''' <remarks> David, 6/20/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal partNumber As String, ByVal lotNumber As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.PartEntity Is Nothing OrElse Not String.Equals(Me.PartEntity.PartNumber, partNumber) Then
            Me._PartEntity = New PartEntity With {.PartNumber = partNumber}
            If Me.PartEntity.Obtain(connection) Then
                Me.PartAutoId = Me.PartEntity.AutoId
            Else
                result = (False, $"Failed fetching {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.AutoId)} of {Me.PartAutoId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainLot(connection, Me.PartAutoId, lotNumber)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(PartLotEntity.PartEntity))
        Return result
    End Function

    ''' <summary>
    ''' Fetches an existing or inserts new <see cref="Entities.PartEntity"/> and
    ''' <see cref="Entities.LotEntity"/> entities and fetches an existing or inserts a new
    ''' <see cref="PartLotEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Function Obtain(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal lotNumber As String) As Boolean
        Dim r As (Success As Boolean, Details As String) = Me.TryObtain(connection, partAutoId, lotNumber)
        If Not r.Success Then Throw New OperationFailedException(r.Details)
        Return r.Success
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Part-Lot entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Part-Lot entities. </value>
    Public ReadOnly Property PartLots As IEnumerable(Of PartLotEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of PartLotEntity)
        Return If(usingNativeTracking, PartLotEntity.Populate(connection.GetAll(Of IOneToMany)), PartLotEntity.Populate(connection.GetAll(Of PartLotNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._PartLots = PartLotEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(PartLotEntity.PartLots))
        Return If(Me.PartLots?.Any, Me.PartLots.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of PartLotNub)) As IEnumerable(Of PartLotEntity)
        Dim l As New List(Of PartLotEntity)
        If nubs?.Any Then
            For Each nub As PartLotNub In nubs
                l.Add(New PartLotEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of PartLotEntity)
        Dim l As New List(Of PartLotEntity)
        If interfaces?.Any Then
            Dim nub As New PartLotNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New PartLotEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns up to Lot entities count. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PartLotBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartLotNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of PartLotEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PartLotBuilder.TableName}] WHERE {NameOf(PartLotNub.PrimaryId)} = @Id", New With {Key .Id = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return PartLotEntity.Populate(connection.Query(Of PartLotNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Lot. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns> The total number of entities by Lot. </returns>
    Public Shared Function CountEntitiesByLot(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PartLotBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartLotNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Lots in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Lots in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByLot(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of PartLotEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PartLotBuilder.TableName}] WHERE {NameOf(PartLotNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return PartLotEntity.Populate(connection.Query(Of PartLotNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{PartLotBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartLotNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        sqlBuilder.Where($"{NameOf(PartLotNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal lotAutoId As Integer) As IEnumerable(Of PartLotNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{PartLotBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(PartLotNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = partAutoId})
        sqlBuilder.Where($"{NameOf(PartLotNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of PartLotNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Part Lot exists. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal lotAutoId As Integer) As Boolean
        Return 1 = PartLotEntity.CountEntities(connection, partAutoId, lotAutoId)
    End Function

#End Region

#Region " RELATIONS: PART "

    ''' <summary> Gets or sets the Part entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Part entity. </value>
    Public ReadOnly Property PartEntity As PartEntity

    ''' <summary> Fetches Part Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Part Entity. </returns>
    Public Function FetchPartEntity(ByVal connection As System.Data.IDbConnection) As PartEntity
        Dim entity As New PartEntity()
        entity.FetchUsingKey(connection, Me.PartAutoId)
        Me._PartEntity = entity
        Return entity
    End Function

    ''' <summary> Fetches Part Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Part Entity. </returns>
    <CLSCompliant(False)>
    Public Function FetchPartEntity(ByVal connection As TransactedConnection) As PartEntity
        Dim entity As New PartEntity()
        entity.FetchUsingKey(connection, Me.PartAutoId)
        Me._PartEntity = entity
        Return entity
    End Function

    ''' <summary>
    ''' Count Parts associated with the specified <paramref name="LotAutoId"/>; expected 1.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns> The total number of Parts. </returns>
    Public Shared Function CountParts(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{PartLotBuilder.TableName}] WHERE {NameOf(PartLotNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Parts associated with the specified <paramref name="LotAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Parts in this collection.
    ''' </returns>
    Public Shared Function FetchParts(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of PartEntity)
        Dim result As IEnumerable(Of PartEntity)
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        result = PartLotEntity.FetchParts(New TransactedConnection(connection, transaction), lotAutoId)
                        transaction.Commit()
                    Catch
                        transaction.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            result = PartLotEntity.FetchParts(transactedConnection, lotAutoId)
        End If
        Return result
    End Function

    ''' <summary>
    ''' Fetches the Parts associated with the specified <paramref name="LotAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 7/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Parts in this collection.
    ''' </returns>
    <CLSCompliant(False)>
    Public Shared Function FetchParts(ByVal connection As TransactedConnection, ByVal lotAutoId As Integer) As IEnumerable(Of PartEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PartLotBuilder.TableName}] WHERE {NameOf(PartLotNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of PartEntity)
        For Each nub As PartLotNub In connection.Query(Of PartLotNub)(selector.RawSql, selector.Parameters)
            Dim entity As New PartLotEntity(nub)
            l.Add(entity.FetchPartEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Parts associated with the specified <paramref name="LotAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteParts(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{PartLotBuilder.TableName}] WHERE {NameOf(PartLotNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

#End Region

#Region " RELATIONS: LOTS "

    ''' <summary> Gets or sets the Lot entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Lot entity. </value>
    Public ReadOnly Property LotEntity As LotEntity

    ''' <summary> Fetches a Lot Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Lot Entity. </returns>
    Public Function FetchLotEntity(ByVal connection As System.Data.IDbConnection) As LotEntity
        Dim entity As New LotEntity()
        entity.FetchUsingKey(connection, Me.LotAutoId)
        Me._LotEntity = entity
        Return entity
    End Function

    ''' <summary> Gets or sets the <see cref="IEnumerable(Of LotEntity)"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="IEnumerable(Of LotEntity)"/>. </value>
    Public ReadOnly Property Lots As IEnumerable(Of LotEntity)

    ''' <summary> Fetches the <see cref="IEnumerable(Of LotEntity)"/> in this collection. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Lots in this collection.
    ''' </returns>
    Public Function FetchOrderedLots(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of LotEntity)
        Me._Lots = PartLotEntity.FetchOrderedLots(connection, Me.PartAutoId)
        Return Me.Lots
    End Function

    ''' <summary> Count lots. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> The total number of lots. </returns>
    Public Shared Function CountLots(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{PartLotBuilder.TableName}] WHERE {NameOf(PartLotNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Lots in this collection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Lots in this collection.
    ''' </returns>
    Public Shared Function FetchLots(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of LotEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{PartLotBuilder.TableName}] WHERE {NameOf(PartLotNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of LotEntity)
        For Each nub As PartLotNub In connection.Query(Of PartLotNub)(selector.RawSql, selector.Parameters)
            Dim entity As New PartLotEntity(nub)
            l.Add(entity.FetchLotEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Lot related to the specified Part. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteLots(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{PartLotBuilder.TableName}] WHERE {NameOf(PartLotNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = partAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches a <see cref="Entities.LotEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="partAutoId">  Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="lotLabel">    The lot label. </param>
    ''' <returns> The lot. </returns>
    Public Shared Function FetchLot(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal partAutoId As Integer, ByVal lotLabel As String) As LotEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {partAutoId, lotLabel})
        ' Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.partAutoId = partAutoId, .lotLabel = lotLabel})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As LotNub = connection.Query(Of LotNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New LotEntity, New LotEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches a <see cref="Entities.LotEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="lotLabel">   The lot label. </param>
    ''' <returns> The lot. </returns>
    Public Shared Function FetchLot(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal lotLabel As String) As LotEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [PartLot] on [PartLot].SecondaryId = [Lot].AutoId where [PartLot].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{LotBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{LotBuilder.TableName}] Inner Join [{PartLotBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{PartLotBuilder.TableName}].{NameOf(PartLotNub.SecondaryId)} = [{LotBuilder.TableName}].{NameOf(LotNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{PartLotBuilder.TableName}].{NameOf(PartLotNub.PrimaryId)} = @{NameOf(partAutoId)} ")
        queryBuilder.AppendLine($"  AND [{LotBuilder.TableName}].{NameOf(LotNub.Label)} = @{NameOf(lotLabel)}); ")
        Return PartLotEntity.FetchLot(connection, queryBuilder.ToString, partAutoId, lotLabel)
    End Function

    ''' <summary> Fetches a <see cref="Entities.LotEntity"/>'s. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="partAutoId">  Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> The lot. </returns>
    Public Shared Function FetchOrderedLots(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal partAutoId As Integer) As IEnumerable(Of LotEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {partAutoId})
        ' Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.partAutoId = partAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return LotEntity.Populate(connection.Query(Of LotNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches <see cref="Entities.LotEntity"/>'s. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> The lot. </returns>
    Public Shared Function FetchOrderedLots(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of LotEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [PartLot] on [PartLot].SecondaryId = [Lot].AutoId where [PartLot].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{LotBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{LotBuilder.TableName}] Inner Join [{PartLotBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{PartLotBuilder.TableName}].{NameOf(PartLotNub.SecondaryId)} = [{LotBuilder.TableName}].{NameOf(LotNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{PartLotBuilder.TableName}].{NameOf(PartLotNub.PrimaryId)} = @{NameOf(partAutoId)}) ")
        queryBuilder.AppendLine($"ORDER BY [{LotBuilder.TableName}].{NameOf(LotNub.Label)} ASC; ")
        Return PartLotEntity.FetchOrderedLots(connection, queryBuilder.ToString, partAutoId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PartLotEntity.PartAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.PartEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.PartEntity"/>. </value>
    Public Property PartAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(PartLotEntity.LotAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.LotEntity"/> </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/> </value>
    Public Property LotAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
