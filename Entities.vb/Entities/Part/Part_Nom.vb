Imports System.Collections.ObjectModel
Imports System.ComponentModel

Imports Dapper

Partial Public Class PartEntity

    ''' <summary> Gets or sets the <see cref="MeterElementNomTypeNomTraitCollection"/>. </summary>
    ''' <value> The <see cref="MeterElementNomTypeNomTraitCollection"/>. </value>
    Public ReadOnly Property NomTraits As MeterElementNomTypeNomTraitCollection

    ''' <summary> Fetches the <see cref="MeterElementNomTypeNomTraitCollection"/>. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="multimeterIdentities"> The multimeter identities. </param>
    ''' <returns> The <see cref="MeterElementNomTypeNomTraitCollection"/>. </returns>
    Public Function FetchNomTraits(ByVal connection As System.Data.IDbConnection, ByVal multimeterIdentities As IEnumerable(Of Integer)) As MeterElementNomTypeNomTraitCollection
        If Not Entities.NomTypeEntity.IsEnumerated Then Entities.NomTypeEntity.TryFetchAll(connection)
        If Not Entities.MultimeterEntity.IsEnumerated Then Entities.MultimeterEntity.TryFetchAll(connection)
        Me._NomTraits = New MeterElementNomTypeNomTraitCollection
        Dim nomType As NomType
        Dim multimeterId As Integer
        For Each element As ElementEntity In Me.Elements
            element.FetchNoms(connection, Me, multimeterIdentities)
            For Each nominals As PartNomEntityCollection In element.NomTraitsCollection
                multimeterId = nominals.MultimeterId
                nomType = CType(nominals.NomTypeId, NomType)
                Me._NomTraits.Add(nominals)
            Next
        Next
        Return Me.NomTraits
    End Function

    ''' <summary>
    ''' Select critical <see cref="NomTraitEntity"/> for having the most stringent meter settings.
    ''' </summary>
    ''' <remarks> David, 6/18/2020. </remarks>
    ''' <returns> A <see cref="NomTraitEntity"/>. </returns>
    Public Function SelectCriticalNomTraitEntity() As NomTraitEntity
        Dim result As New NomTraitEntity
        For Each element As ElementEntity In Me.Elements
            For Each nomTypeEntity As NomTypeEntity In element.NomTypes
                If nomTypeEntity.Id = CInt(NomType.Resistance) Then
                    result = element.NomTraitsCollection(nomTypeEntity.Id).Entity(nomTypeEntity.Id)
                    Exit For
                End If
            Next
            If result.IsClean Then Exit For
        Next
        Return result
    End Function

End Class

''' <summary>
''' Keyed collection of <see cref="PartNomEntityCollection"/> keyed by
''' <see cref="MeterElementNomTypeSelector"/> of <see cref="Entities.ElementEntity"/> and
''' <see cref="Entities.MultimeterEntity"/> and
''' <see cref="Entities.NomTypeEntity"/>.
''' </summary>
''' <remarks> David, 6/16/2020. </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class MeterElementNomTypeNomTraitCollection
    Inherits KeyedCollection(Of MeterElementNomTypeSelector, PartNomEntityCollection)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    Public Sub New()
        MyBase.New()
        Me._BindingList = New isr.Core.Constructs.InvokingBindingList(Of NomTrait)
        Me.MultimeterBindingListDictionary = New Dictionary(Of Integer, isr.Core.Constructs.InvokingBindingList(Of NomTrait))
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 6/22/2020. </remarks>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As PartNomEntityCollection) As MeterElementNomTypeSelector
        Return New MeterElementNomTypeSelector(item)
    End Function

    ''' <summary> Updates or inserts entities using the given connection. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    <CLSCompliant(False)>
    Public Sub Upsert(ByVal connection As TransactedConnection)
        For Each nomTraitEntityCollection As PartNomEntityCollection In Me
            nomTraitEntityCollection.Upsert(connection)
        Next
    End Sub

    ''' <summary> Updates or inserts entities using the given connection. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Sub Upsert(ByVal connection As System.Data.IDbConnection)
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        Me.Upsert(New TransactedConnection(connection, transaction))
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            Me.Upsert(transactedConnection)
        End If
    End Sub

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overloads Sub Add(ByVal item As PartNomEntityCollection)
        MyBase.Add(item)
        Me.BindingList.Add(item.NomTrait)
        If Not Me.MultimeterBindingListDictionary.ContainsKey(item.MultimeterId) Then
            Me.MultimeterBindingListDictionary.Add(item.MultimeterId, New isr.Core.Constructs.InvokingBindingList(Of NomTrait))
        End If
        Me.MultimeterBindingListDictionary(item.MultimeterId).Add(item.NomTrait)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    Public Overloads Sub Clear()
        MyBase.Clear()
        Me.BindingList.Clear()
        For Each bindingList As BindingList(Of NomTrait) In Me.MultimeterBindingListDictionary.Values
            bindingList.Clear()
        Next
    End Sub

    ''' <summary> Gets or sets a list of Nominal Trait bindings. </summary>
    ''' <value> A list of bindings. </value>
    Public ReadOnly Property BindingList As isr.Core.Constructs.InvokingBindingList(Of NomTrait)

    ''' <summary> Gets or sets a dictionary of multimeter binding lists. </summary>
    ''' <value> A dictionary of multimeter binding lists. </value>
    Public ReadOnly Property MultimeterBindingListDictionary As IDictionary(Of Integer, isr.Core.Constructs.InvokingBindingList(Of NomTrait))

    ''' <summary> Toggle binding list change events. </summary>
    ''' <remarks> David, 8/11/2020. </remarks>
    ''' <param name="enabled"> True to enable, false to disable. </param>
    Public Sub ToggleBindingListChangeEvents(ByVal enabled As Boolean)
        Me.BindingList.RaiseListChangedEvents = enabled
        For Each bl As isr.Core.Constructs.InvokingBindingList(Of NomTrait) In Me.MultimeterBindingListDictionary.Values
            bl.RaiseListChangedEvents = enabled
            If enabled Then bl.ResetBindings()
        Next
    End Sub

End Class

