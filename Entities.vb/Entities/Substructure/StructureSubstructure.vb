Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Structure-Substructure builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class StructureSubstructureBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return StructureSubstructureBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(StructureSubstructureBuilder._TableName) Then
                StructureSubstructureBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(StructureSubstructureNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = StructureBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(StructureNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = SubstructureBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(SubstructureNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(StructureSubstructureEntity.StructureAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(StructureSubstructureEntity.SubstructureAutoId)


#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As StructureSubstructureBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As StructureSubstructureBuilder
        If StructureSubstructureBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                StructureSubstructureBuilder._Instance = New StructureSubstructureBuilder()
            End SyncLock
        End If
        Return StructureSubstructureBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Structure Substructure Nub based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("StructureSubstructure")>
Public Class StructureSubstructureNub
    Inherits OneToManyNub
    Implements IOneToMany

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToMany
        Return New StructureSubstructureNub
    End Function

End Class

''' <summary>
''' The Structure-Substructure Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class StructureSubstructureEntity
    Inherits EntityBase(Of IOneToMany, StructureSubstructureNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New StructureSubstructureNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Structure-Substructure interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New StructureSubstructureNub, cache, store)
        Me.UsingNativeTracking = String.Equals(StructureSubstructureBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToMany
        Return New StructureSubstructureNub
    End Function

    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        StructureSubstructureNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        StructureSubstructureNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Structure-Substructure interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        StructureSubstructureNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="substructureAutoId">      Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal substructureAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As StructureSubstructureNub = StructureSubstructureEntity.FetchNubs(connection, structureAutoId, substructureAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.StructureAutoId, Me.SubstructureAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.StructureAutoId, Me.SubstructureAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="substructureAutoId">      Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal substructureAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, structureAutoId, substructureAutoId)
    End Function

    ''' <summary>
    ''' Attempts to retrieve an existing or insert a new <see cref="StructureSubstructureEntity"/>
    ''' from the given data. Specifying new <paramref name="structureNumber"/> adds this Structure;
    ''' Specifying a negative
    ''' <paramref name="substructureNumber"/> adds a new Substructure with the next Substructure
    ''' number for
    ''' this Structure. Updates the <see cref="StructureSubstructureEntity.StructureEntity"/> and
    ''' <see cref="StructureSubstructureEntity.SubstructureEntity"/>
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="structureNumber">    The Structure number. </param>
    ''' <param name="substructureNumber"> The Substructure number. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal structureNumber As Integer, ByVal substructureNumber As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._StructureEntity = New StructureEntity With {.StructureNumber = structureNumber}
        Me._SubstructureEntity = New SubstructureEntity With {.SubstructureNumber = substructureNumber}
        If Me.StructureEntity.Obtain(connection) Then
            If 0 < substructureNumber Then
                If Not Me.SubstructureEntity.FetchUsingUniqueIndex(connection) Then
                    result = (False, $"Failed fetching existing {NameOf(Entities.SubstructureEntity)} with {NameOf(Entities.SubstructureEntity.SubstructureNumber)} of {substructureNumber}")
                End If
            Else
                Me._SubstructureEntity = StructureSubstructureEntity.FetchLastSubstructure(connection, Me.StructureEntity.AutoId)
                substructureNumber = If(Me.SubstructureEntity.IsClean, Me.SubstructureEntity.SubstructureNumber + 1, 1)
                Me._SubstructureEntity = New SubstructureEntity() With {.SubstructureNumber = substructureNumber}
                If Not Me.SubstructureEntity.Obtain(connection) Then
                    result = (False, $"Failed obtaining {NameOf(Entities.SubstructureEntity)} with {NameOf(Entities.SubstructureEntity.SubstructureNumber)} of {substructureNumber}")
                End If
            End If
        Else
            result = (False, $"Failed obtaining {NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.StructureNumber)} of {structureNumber}")
        End If
        If result.Success Then
            Me.StructureAutoId = Me.StructureEntity.AutoId
            Me.SubstructureAutoId = Me.SubstructureEntity.AutoId
            If Not Me.Obtain(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.StructureSubstructureEntity)} for [{NameOf(Entities.StructureEntity.StructureNumber)}, {NameOf(Entities.SubstructureEntity.SubstructureNumber)}] of [{structureNumber},{substructureNumber}]")
            End If
        End If
        Me.NotifyPropertyChanged(NameOf(StructureSubstructureEntity.SubstructureEntity))
        Me.NotifyPropertyChanged(NameOf(StructureSubstructureEntity.StructureEntity))
        Return result
    End Function


    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If StructureSubstructureEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId">     Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="substructureAutoId"> Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal substructureAutoId As Integer) As Boolean
        Return connection.Delete(Of StructureSubstructureNub)(New StructureSubstructureNub With {.PrimaryId = structureAutoId, .SecondaryId = substructureAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Structure-Substructure entities. </summary>
    ''' <value> The Structure-Substructure entities. </value>
    Public ReadOnly Property StructureSubstructures As IEnumerable(Of StructureSubstructureEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of StructureSubstructureEntity)
        Return If(usingNativeTracking, StructureSubstructureEntity.Populate(connection.GetAll(Of IOneToMany)), StructureSubstructureEntity.Populate(connection.GetAll(Of StructureSubstructureNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._StructureSubstructures = StructureSubstructureEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(StructureSubstructureEntity.StructureSubstructures))
        Return If(Me.StructureSubstructures?.Any, Me.StructureSubstructures.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of StructureSubstructureNub)) As IEnumerable(Of StructureSubstructureEntity)
        Dim l As New List(Of StructureSubstructureEntity)
        If nubs?.Any Then
            For Each nub As StructureSubstructureNub In nubs
                l.Add(New StructureSubstructureEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of StructureSubstructureEntity)
        Dim l As New List(Of StructureSubstructureEntity)
        If interfaces?.Any Then
            Dim nub As New StructureSubstructureNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New StructureSubstructureEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to Substructure entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StructureSubstructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureSubstructureNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of StructureSubstructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureSubstructureBuilder.TableName}] WHERE {NameOf(StructureSubstructureNub.PrimaryId)} = @Id", New With {Key .Id = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return StructureSubstructureEntity.Populate(connection.Query(Of StructureSubstructureNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Substructure. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substructureAutoId"> Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns> The total number of entities by Substructure. </returns>
    Public Shared Function CountEntitiesBySubstructure(ByVal connection As System.Data.IDbConnection, ByVal substructureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StructureSubstructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureSubstructureNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = substructureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Substructures in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substructureAutoId"> Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Substructures in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesBySubstructure(ByVal connection As System.Data.IDbConnection, ByVal substructureAutoId As Integer) As IEnumerable(Of StructureSubstructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureSubstructureBuilder.TableName}] WHERE {NameOf(StructureSubstructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = substructureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return StructureSubstructureEntity.Populate(connection.Query(Of StructureSubstructureNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="substructureAutoId">       Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal substructureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StructureSubstructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureSubstructureNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = structureAutoId})
        sqlBuilder.Where($"{NameOf(StructureSubstructureNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = substructureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="substructureAutoId">       Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal substructureAutoId As Integer) As IEnumerable(Of StructureSubstructureNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{StructureSubstructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureSubstructureNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = structureAutoId})
        sqlBuilder.Where($"{NameOf(StructureSubstructureNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = substructureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of StructureSubstructureNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Structure Substructure exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="substructureAutoId">       Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal substructureAutoId As Integer) As Boolean
        Return 1 = StructureSubstructureEntity.CountEntities(connection, structureAutoId, substructureAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Structure entity. </summary>
    ''' <value> The Structure entity. </value>
    Public ReadOnly Property StructureEntity As StructureEntity

    ''' <summary> Fetches Structure Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Structure Entity. </returns>
    Public Function FetchStructureEntity(ByVal connection As System.Data.IDbConnection) As StructureEntity
        Dim entity As New StructureEntity()
        entity.FetchUsingKey(connection, Me.StructureAutoId)
        Me._StructureEntity = entity
        Return entity
    End Function

    ''' <summary> Count Structures associated with the specified <paramref name="substructureAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substructureAutoId"> Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns> The total number of Structures. </returns>
    Public Shared Function CountStructures(ByVal connection As System.Data.IDbConnection, ByVal substructureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{StructureSubstructureBuilder.TableName}] WHERE {NameOf(StructureSubstructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = substructureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Structures associated with the specified <paramref name="substructureAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substructureAutoId"> Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Structures in this collection.
    ''' </returns>
    Public Shared Function FetchStructures(ByVal connection As System.Data.IDbConnection, ByVal substructureAutoId As Integer) As IEnumerable(Of SubstructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureSubstructureBuilder.TableName}] WHERE {NameOf(StructureSubstructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = substructureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of SubstructureEntity)
        For Each nub As StructureSubstructureNub In connection.Query(Of StructureSubstructureNub)(selector.RawSql, selector.Parameters)
            Dim entity As New StructureSubstructureEntity(nub)
            l.Add(entity.FetchSubstructureEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Structures associated with the specified <paramref name="substructureAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substructureAutoId"> Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteStructures(ByVal connection As System.Data.IDbConnection, ByVal substructureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{StructureSubstructureBuilder.TableName}] WHERE {NameOf(StructureSubstructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = substructureAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the Substructure entity. </summary>
    ''' <value> The Substructure entity. </value>
    Public ReadOnly Property SubstructureEntity As SubstructureEntity

    ''' <summary> Fetches a Substructure Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Substructure Entity. </returns>
    Public Function FetchSubstructureEntity(ByVal connection As System.Data.IDbConnection) As SubstructureEntity
        Dim entity As New SubstructureEntity()
        entity.FetchUsingKey(connection, Me.SubstructureAutoId)
        Me._SubstructureEntity = entity
        Return entity
    End Function

    Public Shared Function CountSubstructures(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{StructureSubstructureBuilder.TableName}] WHERE {NameOf(StructureSubstructureNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Substructures in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Substructures in this collection.
    ''' </returns>
    Public Shared Function FetchSubstructures(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of SubstructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureSubstructureBuilder.TableName}] WHERE {NameOf(StructureSubstructureNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of SubstructureEntity)
        For Each nub As StructureSubstructureNub In connection.Query(Of StructureSubstructureNub)(selector.RawSql, selector.Parameters)
            Dim entity As New StructureSubstructureEntity(nub)
            l.Add(entity.FetchSubstructureEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Substructure related to the specified Structure. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteSubstructures(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{StructureSubstructureBuilder.TableName}] WHERE {NameOf(StructureSubstructureNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = structureAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered substructures in this collection. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="structureAutoId">   Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered substructures in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedSubstructures(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal structureAutoId As Integer) As IEnumerable(Of SubstructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .Id = structureAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return SubstructureEntity.Populate(connection.Query(Of SubstructureNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered substructures in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered substructures in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedSubstructures(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of SubstructureEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [StructureSubstructure] on [StructureSubstructure].SecondaryId = [Substructure].AutoId where [StructureSubstructure].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{SubstructureBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{SubstructureBuilder.TableName}] Inner Join [{StructureSubstructureBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{StructureSubstructureBuilder.TableName}].{NameOf(StructureSubstructureNub.SecondaryId)} = [{SubstructureBuilder.TableName}].{NameOf(SubstructureNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{StructureSubstructureBuilder.TableName}].{NameOf(StructureSubstructureNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{SubstructureBuilder.TableName}].{NameOf(SubstructureNub.Amount)} ASC; ")
        Return StructureSubstructureEntity.FetchOrderedSubstructures(connection, queryBuilder.ToString, structureAutoId)
    End Function

    ''' <summary> Fetches the last Substructure in this collection. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="structureAutoId">   Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Substructures in this collection.
    ''' </returns>
    Public Shared Function FetchFirstSubstructure(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal structureAutoId As Integer) As SubstructureEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery, New With {Key .Id = structureAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As SubstructureNub = connection.Query(Of SubstructureNub)(template.RawSql, template.Parameters).FirstOrDefault
        Return If(nub Is Nothing, New SubstructureEntity, New SubstructureEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches the last Substructure in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Substructures in this collection.
    ''' </returns>
    Public Shared Function FetchLastSubstructure(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As SubstructureEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [StructureSubstructure] on [StructureSubstructure].SecondaryId = [Substructure].AutoId where [StructureSubstructure].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{SubstructureBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{SubstructureBuilder.TableName}] Inner Join [{StructureSubstructureBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{StructureSubstructureBuilder.TableName}].{NameOf(StructureSubstructureNub.SecondaryId)} = [{SubstructureBuilder.TableName}].{NameOf(SubstructureNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{StructureSubstructureBuilder.TableName}].{NameOf(StructureSubstructureNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{SubstructureBuilder.TableName}].{NameOf(SubstructureNub.Amount)} DESC; ")
        Return StructureSubstructureEntity.FetchFirstSubstructure(connection, queryBuilder.ToString, structureAutoId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(StructureSubstructureEntity.StructureAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="StructureEntity"/>. </summary>
    ''' <value> Identifies the <see cref="StructureEntity"/>. </value>
    Public Property StructureAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(StructureSubstructureEntity.SubstructureAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="SubstructureEntity"/>. </summary>
    ''' <value> Identifies the <see cref="SubstructureEntity"/>. </value>
    Public Property SubstructureAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
