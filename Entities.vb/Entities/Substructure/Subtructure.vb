Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A builder for a natural Substructure based on the <see cref="IKeyForeignNaturalTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class SubstructureBuilder
    Inherits KeyForeignNaturalTimeBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return SubstructureBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(SubstructureBuilder._TableName) Then
                SubstructureBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(SubstructureNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the foreign table . </summary>
    ''' <value> The name of the foreign table. </value>
    Public Overrides Property ForeignTableName As String = SubstructureTypeBuilder.TableName

    ''' <summary> Gets the name of the foreign table key. </summary>
    ''' <value> The name of the foreign table key. </value>
    Public Overrides Property ForeignTableKeyName As String = NameOf(SubstructureTypeNub.Id)

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(SubstructureEntity.AutoId)

    ''' <summary> Gets or sets the name of the foreign identifier field. </summary>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName() As String = NameOf(SubstructureEntity.SubstructureTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName() As String = NameOf(SubstructureEntity.SubstructureNumber)

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <value> The name of the timestamp field. </value>
    Public Overrides Property TimestampFieldName() As String = NameOf(SubstructureEntity.Timestamp)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SubstructureBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SubstructureBuilder
        If SubstructureBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                SubstructureBuilder._Instance = New SubstructureBuilder()
            End SyncLock
        End If
        Return SubstructureBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Substructure table based on the <see cref="IKeyForeignNaturalTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("Substructure")>
Public Class SubstructureNub
    Inherits KeyForeignNaturalTimeNub
    Implements IKeyForeignNaturalTime

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IKeyForeignNaturalTime
        Return New SubstructureNub
    End Function

End Class


''' <summary> The Substructure Entity based on the <see cref="IKeyForeignNaturalTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class SubstructureEntity
    Inherits EntityBase(Of IKeyForeignNaturalTime, SubstructureNub)
    Implements IKeyForeignNaturalTime

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New SubstructureNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Substructure interface. </param>
    Public Sub New(ByVal value As IKeyForeignNaturalTime)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyForeignNaturalTime, ByVal store As IKeyForeignNaturalTime)
        MyBase.New(New SubstructureNub, cache, store)
        Me.UsingNativeTracking = String.Equals(SubstructureBuilder.TableName, NameOf(IKeyForeignNaturalTime).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "


    Public Overrides Function CreateNew() As IKeyForeignNaturalTime
        Return New SubstructureNub
    End Function

    Public Overrides Function CreateCopy() As IKeyForeignNaturalTime
        Dim destination As IKeyForeignNaturalTime = Me.CreateNew
        SubstructureNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IKeyForeignNaturalTime)
        SubstructureNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Substructure interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyForeignNaturalTime)
        ' first make the copy to notify of any property change.
        SubstructureNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Substructure table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyForeignNaturalTime)(key), connection.Get(Of SubstructureNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.SubstructureNumber)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="substrucutreNumber">     Identifies the <see cref="SubstructureEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal substrucutreNumber As Integer) As Boolean
        Me.ClearStore()
        Dim nub As SubstructureNub = SubstructureEntity.FetchNubs(connection, substrucutreNumber).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.FetchUsingKey(connection, nub.AutoId)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyForeignNaturalTime) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If SubstructureEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' update the existing record from the specified entity.
        ' insert a new record based on the specified entity values.
        ' try fetching an existing record
        Dim fetched As Boolean = If(UniqueIndexOptions.Amount = (SubstructureBuilder.Get.QueryUniqueIndexOptions(connection) And UniqueIndexOptions.Amount),
                                        Me.FetchUsingUniqueIndex(connection, entity.Amount),
                                        Me.FetchUsingKey(connection, entity.AutoId))
        If fetched Then
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of SubstructureNub)(New SubstructureNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Substructure entities. </summary>
    ''' <value> The Substructure entities. </value>
    Public ReadOnly Property Substructures As IEnumerable(Of SubstructureEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of SubstructureEntity)
        Return If(usingNativeTracking, SubstructureEntity.Populate(connection.GetAll(Of IKeyForeignNaturalTime)), SubstructureEntity.Populate(connection.GetAll(Of SubstructureNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Substructures = SubstructureEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(SubstructureEntity.Substructures))
        Return If(Me.Substructures?.Any, Me.Substructures.Count, 0)
    End Function

    ''' <summary> Populates a list of Substructure entities. </summary>
    ''' <param name="nubs"> The Substructure nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of SubstructureNub)) As IEnumerable(Of SubstructureEntity)
        Dim l As New List(Of SubstructureEntity)
        If nubs?.Any Then
            For Each nub As SubstructureNub In nubs
                l.Add(New SubstructureEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Substructure entities. </summary>
    ''' <param name="interfaces"> The Substructure interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyForeignNaturalTime)) As IEnumerable(Of SubstructureEntity)
        Dim l As New List(Of SubstructureEntity)
        If interfaces?.Any Then
            Dim nub As New SubstructureNub
            For Each iFace As IKeyForeignNaturalTime In interfaces
                nub.CopyFrom(iFace)
                l.Add(New SubstructureEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="substructureNumber">  The substructure number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal substructureNumber As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SubstructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SubstructureNub.Amount)} = @SubstructureNumber", New With {substructureNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="substructureNumber">  The substructure number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal substructureNumber As Integer) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{SubstructureBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(SubstructureNub.Amount)} IS NULL OR {NameOf(SubstructureNub.Amount)} = @SubstructureNumber)")
        selectSql.Append("; ")
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {substructureNumber})
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="substructureNumber">  The substructure number. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal substructureNumber As Integer) As IEnumerable(Of SubstructureNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SubstructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SubstructureNub.Amount)} = @SubstructureNumber", New With {substructureNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SubstructureNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the substructure exists. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="substructureNumber">  The substructure number. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal substructureNumber As Integer) As Boolean
        Return 1 = SubstructureEntity.CountEntities(connection, substructureNumber)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="SubstructureEntity"/>. </summary>
    ''' <value> Identifies the <see cref="SubstructureEntity"/>. </value>
    Public Property AutoId As Integer Implements IKeyForeignNaturalTime.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="SubstructureEntity"/> type. </summary>
    ''' <value> Identifies the <see cref="SubstructureEntity"/> type. </value>
    Public Property ForeignId As Integer Implements IKeyForeignNaturalTime.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SubstructureEntity.SubstructureTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="SubstructureEntity"/> type. </summary>
    ''' <value> Identifies the <see cref="SubstructureEntity"/> type. </value>
    Public Property SubstructureTypeId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(value As Integer)
            Me.ForeignId = value
        End Set
    End Property

    ''' <summary> Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value. </summary>
    ''' <value> The natural amount. </value>
    Public Property Amount As Integer Implements IKeyForeignNaturalTime.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(value As Integer)
            If Me.Amount <> value Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SubstructureEntity.SubstructureNumber))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the substructure number. </summary>
    ''' <value> The structure number. </value>
    Public Property SubstructureNumber As Integer
        Get
            Return Me.Amount
        End Get
        Set(value As Integer)
            Me.Amount = value
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks> Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to convert to local time. </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements IKeyForeignNaturalTime.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " FIELDS: CUSTOM "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

#End Region

End Class

