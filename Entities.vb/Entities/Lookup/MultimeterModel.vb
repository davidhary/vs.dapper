Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A Meter Model builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class MultimeterModelBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return MultimeterModelBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(MultimeterModelBuilder._TableName) Then
                MultimeterModelBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(MultimeterModelNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary>
    ''' Inserts or ignores the records described by the <see cref="MultimeterModel"/> enumeration
    ''' type.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(MultimeterModel), New Integer() {CInt(MultimeterModel.None)})
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As MultimeterModelBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As MultimeterModelBuilder
        If MultimeterModelBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                MultimeterModelBuilder._Instance = New MultimeterModelBuilder()
            End SyncLock
        End If
        Return MultimeterModelBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Meter Model table based on the <see cref="INominal">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("MultimeterModel")>
Public Class MultimeterModelNub
    Inherits NominalNub
    Implements INominal

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New MultimeterModelNub
    End Function

End Class

''' <summary>
''' The Meter Model Entity implements the <see cref="INominal"/> interface.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class MultimeterModelEntity
    Inherits EntityBase(Of INominal, MultimeterModelNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New MultimeterModelNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Meter Model interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New MultimeterModelNub, cache, store)
        Me.UsingNativeTracking = String.Equals(MultimeterModelBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New MultimeterModelNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        MultimeterModelNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As INominal)
        MultimeterModelNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        MultimeterModelNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Meter Model table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of MultimeterModelNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Meter Model label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As MultimeterModelNub = MultimeterModelEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If MultimeterModelEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of MultimeterModelNub)(New MultimeterModelNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the Meter Model entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Meter Model entities. </value>
    Public Shared ReadOnly Property MultimeterModels As IEnumerable(Of MultimeterModelEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of MultimeterModelEntity)
        Return If(usingNativeTracking, MultimeterModelEntity.Populate(connection.GetAll(Of INominal)), MultimeterModelEntity.Populate(connection.GetAll(Of MultimeterModelNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        MultimeterModelEntity._MultimeterModels = MultimeterModelEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(MultimeterModelEntity.MultimeterModels?.Any, MultimeterModelEntity.MultimeterModels.Count, 0)
    End Function

    ''' <summary> Populates a list of Meter Model entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The Meter Model nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of MultimeterModelNub)) As IEnumerable(Of MultimeterModelEntity)
        Dim l As New List(Of MultimeterModelEntity)
        If nubs?.Any Then
            For Each nub As MultimeterModelNub In nubs
                l.Add(New MultimeterModelEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Meter Model entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The Meter Model interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of MultimeterModelEntity)
        Dim l As New List(Of MultimeterModelEntity)
        If interfaces?.Any Then
            Dim nub As New MultimeterModelNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New MultimeterModelEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Dictionary of entity lookups. </summary>
    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, MultimeterModelEntity)

    ''' <summary> The entity lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of Integer, MeterModelEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, MultimeterModelEntity)
        If Not (MultimeterModelEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            MultimeterModelEntity._EntityLookupDictionary = New Dictionary(Of Integer, MultimeterModelEntity)
            For Each entity As MultimeterModelEntity In MultimeterModelEntity.MultimeterModels
                MultimeterModelEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return MultimeterModelEntity._EntityLookupDictionary
    End Function

    ''' <summary> Dictionary of key lookups. </summary>
    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (MultimeterModelEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            MultimeterModelEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As MultimeterModelEntity In MultimeterModelEntity.MultimeterModels
                MultimeterModelEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return MultimeterModelEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (MultimeterModelEntity.MultimeterModels?.Any AndAlso
                MultimeterModelEntity._EntityLookupDictionary?.Any AndAlso
                MultimeterModelEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Meter Model label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{MeterModelNub.TableName}]
        '  WHERE {NameOf(MeterModelNub.PlatformName)} = @PlatformName", New With {Key .PlatformName = PlatformName})
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{MultimeterModelBuilder.TableName}] WHERE {NameOf(MultimeterModelNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Meter Model label. </param>
    ''' <returns> An IMeterModel . </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of MultimeterModelNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{MultimeterModelBuilder.TableName}] WHERE {NameOf(MultimeterModelNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of MultimeterModelNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Meter Model exists. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Meter Model label. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = MultimeterModelEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/> Model.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/> Model. </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(MultimeterModelEntity.MultimeterModel))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Meter Model. </summary>
    ''' <value> the Meter Model. </value>
    Public Property MultimeterModel As MultimeterModel
        Get
            Return CType(Me.Id, MultimeterModel)
        End Get
        Set(ByVal value As MultimeterModel)
            Me.Id = value
        End Set
    End Property

    ''' <summary> Gets or sets the Meter Model label. </summary>
    ''' <value> The Meter Model label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the Multimeter Model. </summary>
    ''' <value> The Meter Model description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="meterModelId"> The entity unique key. </param>
    ''' <returns> A <see cref="MultimeterModelEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal meterModelId As Integer) As (Success As Boolean, Details As String, Entity As MultimeterModelEntity)
        Dim activity As String = String.Empty
        Dim entity As New MultimeterModelEntity
        Try
            activity = $"Fetching {NameOf(MultimeterModelEntity)} by {NameOf(MultimeterModelNub.Id)} of {meterModelId}"
            Return If(entity.FetchUsingKey(connection, meterModelId),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 4/28/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="meterModelLabel"> The Meter Model label. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As MeterModelEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal meterModelLabel As String) As (Success As Boolean, Details As String, Entity As MultimeterModelEntity)
        Dim activity As String = String.Empty
        Dim entity As New MultimeterModelEntity
        Try
            activity = $"Fetching {NameOf(MultimeterModelEntity)} by {NameOf(MultimeterModelNub.Label)} of {meterModelLabel}"
            Return If(entity.FetchUsingUniqueIndex(connection, meterModelLabel),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, details As String, Entities As IEnumerable(Of MeterModelEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, details As String, Entities As IEnumerable(Of MultimeterModelEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New MultimeterModelEntity()
            activity = $"fetching all {NameOf(MultimeterModelEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> MultimeterModelEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(MultimeterModelEntity.EntityLookupDictionary)} count must equal {NameOf(MultimeterModelEntity.MultimeterModels)} count ")
            ElseIf elementCount <> MultimeterModelEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(MultimeterModelEntity.KeyLookupDictionary)} count must equal {NameOf(MultimeterModelEntity.MultimeterModels)} count ")
            End If
            Return (True, String.Empty, MultimeterModelEntity.MultimeterModels)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of MultimeterModelEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Values that represent Multimeter Models. </summary>
''' <remarks> David, 2020-10-02. </remarks>
Public Enum MultimeterModel

    ''' <summary> An enum constant representing the none option. </summary>
    <System.ComponentModel.Description("None")> None

    ''' <summary> An enum constant representing the 7510 option. </summary>
    <System.ComponentModel.Description("Keithley 7510")> K7510

    ''' <summary> An enum constant representing the 2002 option. </summary>
    <System.ComponentModel.Description("Keithley 2002")> K2002
End Enum
