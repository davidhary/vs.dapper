Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A test suite type builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class TestSuiteTypeBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return TestSuiteTypeBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(TestSuiteTypeBuilder._TableName) Then
                TestSuiteTypeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(TestSuiteTypeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary>
    ''' Inserts or ignores the records described by the <see cref="TestSuiteType"/> enumeration type.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(TestSuiteType), New Integer() {CInt(TestSuiteType.None)})
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As TestSuiteTypeBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As TestSuiteTypeBuilder
        If TestSuiteTypeBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                TestSuiteTypeBuilder._Instance = New TestSuiteTypeBuilder()
            End SyncLock
        End If
        Return TestSuiteTypeBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the TestSuiteType table based on the <see cref="INominal">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("TestSuiteType")>
Public Class TestSuiteTypeNub
    Inherits NominalNub
    Implements INominal

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New TestSuiteTypeNub
    End Function

End Class

''' <summary> The TestSuiteType Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class TestSuiteTypeEntity
    Inherits EntityBase(Of INominal, TestSuiteTypeNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New TestSuiteTypeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The TestSuiteType interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New TestSuiteTypeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(TestSuiteTypeBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New TestSuiteTypeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        TestSuiteTypeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As INominal)
        TestSuiteTypeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The TestSuiteType interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        TestSuiteTypeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The TestSuiteType table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of TestSuiteTypeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The test suite type label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As TestSuiteTypeNub = TestSuiteTypeEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If TestSuiteTypeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of TestSuiteTypeNub)(New TestSuiteTypeNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the TestSuiteType entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The TestSuiteType entities. </value>
    Public Shared ReadOnly Property TestSuiteTypes As IEnumerable(Of TestSuiteTypeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of TestSuiteTypeEntity)
        Return If(usingNativeTracking, TestSuiteTypeEntity.Populate(connection.GetAll(Of INominal)), TestSuiteTypeEntity.Populate(connection.GetAll(Of TestSuiteTypeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        TestSuiteTypeEntity._TestSuiteTypes = TestSuiteTypeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(TestSuiteTypeEntity.TestSuiteTypes?.Any, TestSuiteTypeEntity.TestSuiteTypes.Count, 0)
    End Function

    ''' <summary> Populates a list of TestSuiteType entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The TestSuiteType nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of TestSuiteTypeNub)) As IEnumerable(Of TestSuiteTypeEntity)
        Dim l As New List(Of TestSuiteTypeEntity)
        If nubs?.Any Then
            For Each nub As TestSuiteTypeNub In nubs
                l.Add(New TestSuiteTypeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of TestSuiteType entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The TestSuiteType interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of TestSuiteTypeEntity)
        Dim l As New List(Of TestSuiteTypeEntity)
        If interfaces?.Any Then
            Dim nub As New TestSuiteTypeNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New TestSuiteTypeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Dictionary of entity lookups. </summary>
    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, TestSuiteTypeEntity)

    ''' <summary> The test suite type lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of Integer, TestSuiteTypeEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, TestSuiteTypeEntity)
        If Not (TestSuiteTypeEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            TestSuiteTypeEntity._EntityLookupDictionary = New Dictionary(Of Integer, TestSuiteTypeEntity)
            For Each entity As TestSuiteTypeEntity In TestSuiteTypeEntity.TestSuiteTypes
                TestSuiteTypeEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return TestSuiteTypeEntity._EntityLookupDictionary
    End Function

    ''' <summary> Dictionary of key lookups. </summary>
    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (TestSuiteTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            TestSuiteTypeEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As TestSuiteTypeEntity In TestSuiteTypeEntity.TestSuiteTypes
                TestSuiteTypeEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return TestSuiteTypeEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (TestSuiteTypeEntity.TestSuiteTypes?.Any AndAlso
                TestSuiteTypeEntity._EntityLookupDictionary?.Any AndAlso
                TestSuiteTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      Name of the test suite type. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{TestSuiteTypeNub.TableName}]
        '  WHERE {NameOf(TestSuiteTypeNub.label)} = @label", New With {Key .Label = label})
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{TestSuiteTypeBuilder.TableName}] WHERE {NameOf(TestSuiteTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      Name of the test suite type. </param>
    ''' <returns> An ITestSuiteType . </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of TestSuiteTypeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{TestSuiteTypeBuilder.TableName}] WHERE {NameOf(TestSuiteTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of TestSuiteTypeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the TestSuiteType exists. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      Name of the test suite type. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = TestSuiteTypeEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the test suite type. </summary>
    ''' <value> Identifies the test suite type. </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(TestSuiteTypeEntity.TestSuiteType))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the TestSuiteType. </summary>
    ''' <value> The TestSuiteType. </value>
    Public Property TestSuiteType As TestSuiteType
        Get
            Return CType(Me.Id, TestSuiteType)
        End Get
        Set(value As TestSuiteType)
            Me.Id = value
        End Set
    End Property

    ''' <summary> Gets or sets the test suite type label. </summary>
    ''' <value> The test suite type label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the test suite type. </summary>
    ''' <value> The test suite type description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="testSuiteTypeId"> The entity unique key. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As TestSuiteTypeEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection,
                                            ByVal testSuiteTypeId As Integer) As (Success As Boolean, Details As String, Entity As TestSuiteTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New TestSuiteTypeEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching {NameOf(TestSuiteTypeEntity)} by {NameOf(TestSuiteTypeNub.Id)} of {testSuiteTypeId}"
            If Not entity.FetchUsingKey(connection, testSuiteTypeId) Then
                details = $"Failed {activity}"
                success = False
            End If
        Catch ex As Exception
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            success = False
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="testSuiteTypeLabel"> The test suite type label. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As TestSuiteTypeEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal testSuiteTypeLabel As String) As (Success As Boolean, Details As String, Entity As TestSuiteTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New TestSuiteTypeEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching {NameOf(TestSuiteTypeEntity)} by {NameOf(TestSuiteTypeNub.Label)} of {testSuiteTypeLabel}"
            If Not entity.FetchUsingUniqueIndex(connection, testSuiteTypeLabel) Then
                details = $"Failed {activity}"
                success = False
            End If
        Catch ex As Exception
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            success = False
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of TestSuiteTypeEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of TestSuiteTypeEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New TestSuiteTypeEntity()
            activity = $"fetching all {NameOf(TestSuiteTypeEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> TestSuiteTypeEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(TestSuiteTypeEntity.EntityLookupDictionary)} count must equal {NameOf(TestSuiteTypeEntity.TestSuiteTypes)} count ")
            ElseIf elementCount <> TestSuiteTypeEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(TestSuiteTypeEntity.KeyLookupDictionary)} count must equal {NameOf(TestSuiteTypeEntity.TestSuiteTypes)} count ")
            End If
            Return (True, String.Empty, TestSuiteTypeEntity.TestSuiteTypes)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of TestSuiteTypeEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Values that represent TestSuite types. </summary>
''' <remarks> David, 2020-10-02. </remarks>
Public Enum TestSuiteType

    ''' <summary> . </summary>
    <ComponentModel.Description("<Test Suite Type>")> None = 0

    ''' <summary> . </summary>
    <ComponentModel.Description("TCR")> Tcr = 1

    ''' <summary> . </summary>
    <ComponentModel.Description("Delta")> Delta = 2

    ''' <summary> . </summary>
    <ComponentModel.Description("Solo")> Solo = 3
End Enum
