Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary>
''' Interface for the Tolerance nub and entity. Includes the fields as kept in the data table.
''' Allows tracking of property changes.
''' </summary>
''' <remarks>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value. <para>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface ITolerance

    ''' <summary> Gets the identifier of the Tolerance. </summary>
    ''' <value> Identifies the Tolerance. </value>
    <ExplicitKey>
    Property ToleranceCode As String

    ''' <summary> Gets the lower limit. </summary>
    ''' <value> The lower limit. </value>
    Property LowerLimit As Double

    ''' <summary> Gets the upper limit. </summary>
    ''' <value> The upper limit. </value>
    Property UpperLimit As Double

    ''' <summary> Gets the caption. </summary>
    ''' <value> The caption. </value>
    Property Caption As String

    ''' <summary> Gets the compound caption. </summary>
    ''' <value> The compound caption. </value>
    <Computed>
    Property CompoundCaption As String

End Interface

''' <summary> A tolerance builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class ToleranceBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ToleranceBuilder._TableName) Then
                ToleranceBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ToleranceNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValues(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return InsertValues(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return InsertValues(sqlite)
        Return 0
    End Function

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValues(ByVal connection As System.Data.SQLite.SQLiteConnection) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim builder As New Text.StringBuilder()
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('A', -0.0005, 0.0005,   '±0.05%' ,'A: ±0.05%' ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('B', -0.001, 0.001,     '±0.1%'  ,'B: ±0.1%'  ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('C', -0.0025, 0.0025,   '±0.25%' ,'C: ±0.25%' ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('D', -0.005, 0.005,     '±0.5%'  ,'D: ±0.5%'  ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('F', -0.01, 0.01,       '±1%'    ,'F: ±1%'    ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('G', -0.02, 0.02,       '±2%'    ,'G: ±2%'    ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('H', -0.03, 0.03,       '±3%'    ,'H: ±3%'    ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('J', -0.05, 0.05,       '±5%'    ,'J: ±5%'    ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('K', -0.1, 0.1,         '±10%'   ,'K: ±10%'   ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('M', -0.2, 0.2,         '±20%'   ,'M: ±20%'   ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('P', -0.0015, 0.0015,   '±0.15%' ,'P: ±0.15%' ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('Q', -0.0002, 0.0002,   '±0.02%' ,'Q: ±0.02%' ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('R', -0.002, 0.002,     '±0.2%'  ,'R: ±0.2%'  ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('S', -0.00025, 0.00025, '±0.025%','S: ±0.025%'); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('T', -0.0001, 0.0001,   '±0.01%' ,'T: ±0.01%' ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('U', -0.0003, 0.0003,   '±0.03%' ,'U: ±0.03%' ); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('V', -0.0005, 0.0005,   '±0.005%','V: ±0.005%'); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('Y', -0.00015, 0.00015, '±0.015%','Y: ±0.015%'); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{ToleranceBuilder.TableName}] VALUES ('Z', -0.99, 0.99,       '±99%'   ,'Z: ±99%'   ); ")
        Return connection.Execute(builder.ToString)
    End Function

    ''' <summary> Inserts or ignores the records using an SQL client connection. </summary>
    ''' <remarks> David, 3/14/2020. Supports SQL Client only library. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValuesSqlClient(ByVal connection As System.Data.IDbConnection) As Integer
        Return ToleranceBuilder.InsertValues(TryCast(connection, System.Data.SqlClient.SqlConnection))
    End Function

    ''' <summary> Inserts the values SQL lite described by connection. </summary>
    ''' <remarks> David, 6/18/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValuesSQLite(ByVal connection As System.Data.IDbConnection) As Integer
        Return ToleranceBuilder.InsertValues(TryCast(connection, System.Data.SQLite.SQLiteConnection))
    End Function

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValues(ByVal connection As System.Data.SqlClient.SqlConnection) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim builder As New Text.StringBuilder()
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'A') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'A', -0.0005, 0.0005, N'±0.05%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'B') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'B', -0.001, 0.001, N'±0.1%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'C') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'C', -0.0025, 0.0025, N'±0.25%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'D') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'D', -0.005, 0.005, N'±0.5%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'F') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'F', -0.01, 0.01, N'±1%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'G') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'G', -0.02, 0.02, N'±2%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'H') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'H', -0.03, 0.03, N'±3%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'J') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'J', -0.05, 0.05, N'±5%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'K') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'K', -0.1, 0.1, N'±10%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'M') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'M', -0.2, 0.2, N'±20%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'P') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'P', -0.0015, 0.0015, N'±0.15%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'Q') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'Q', -0.0002, 0.0002, N'±0.02%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'R') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'R', -0.002, 0.002, N'±0.2%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'S') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'S', -0.00025, 0.00025, N'±0.025%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'T') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'T', -0.0001, 0.0001, N'±0.01%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'U') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'U', -0.0003, 0.0003, N'±0.03%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'V') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'V', -0.0005, 0.0005, N'±0.005%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'Y') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'Y', -0.00015, 0.00015, N'±0.015%'); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{ToleranceBuilder.TableName}] WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = N'Z') INSERT INTO [{ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'Z', -0.99, 0.99, N'±99%'); ")
        Return connection.Execute(builder.ToString)
    End Function

    ''' <summary> Updates the compound caption. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The key. </param>
    ''' <param name="value">      The value. </param>
    Public Shared Sub UpdateCompoundCaption(ByVal connection As System.Data.IDbConnection, ByVal key As String, ByVal value As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"UPDATE [{ToleranceBuilder.TableName}] ")
        queryBuilder.Append($"SET [{NameOf(ToleranceNub.CompoundCaption)}] = '{value}' ")
        queryBuilder.Append($"WHERE [{NameOf(ToleranceNub.ToleranceCode)}] = '{key}'; ")
        connection.Execute(queryBuilder.ToString().Clean())
    End Sub

    ''' <summary> Gets the name of the unique label index. </summary>
    ''' <value> The name of the unique label index. </value>
    Private Shared ReadOnly Property CaptionIndexName As String
        Get
            Return $"UQ_{ToleranceBuilder.TableName}_{NameOf(ToleranceNub.Caption)}"
        End Get
    End Property

    ''' <summary> The using unique caption. </summary>
    Private Shared _UsingUniqueCaption As Boolean?

    ''' <summary> Indicates if the entity uses a unique Caption. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function UsingUniqueCaption(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not ToleranceBuilder._UsingUniqueCaption.HasValue Then
            ToleranceBuilder._UsingUniqueCaption = connection.IndexExists(ToleranceBuilder.CaptionIndexName)
        End If
        Return ToleranceBuilder._UsingUniqueCaption.Value
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The SQL Query or empty. </returns>
    Public Shared Function CreateUniqueIndex(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateUniqueIndex(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateUniqueIndex(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates unique index for SQLite database table. </summary>
    ''' <remarks> David, 6/8/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The SQL Script. </returns>
    Private Shared Function CreateUniqueIndex(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"CREATE UNIQUE INDEX IF NOT EXISTS [{ToleranceBuilder.CaptionIndexName}] ON [{ToleranceBuilder.TableName}] ([{NameOf(ToleranceNub.Caption)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return queryBuilder.ToString
    End Function

    ''' <summary> Creates unique index for SQL Server database table. </summary>
    ''' <remarks> David, 6/8/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The SQL Script. </returns>
    Private Shared Function CreateUniqueIndex(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{ToleranceBuilder.TableName}]') AND name = N'{ToleranceBuilder.CaptionIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{ToleranceBuilder.CaptionIndexName}] ON [dbo].[{ToleranceBuilder.TableName}] ([{NameOf(ToleranceNub.Caption)}] ASC) 
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return queryBuilder.ToString
    End Function

    ''' <summary> Removes the duplicates described by connection. </summary>
    ''' <remarks> David, 6/8/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of records affected. </returns>
    Public Shared Function RemoveDuplicates(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return RemoveDuplicates(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return RemoveDuplicates(sqlite)
        Return 0
    End Function

    ''' <summary> Removes the duplicates described by connection. </summary>
    ''' <remarks> David, 6/8/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of records affected. </returns>
    Private Shared Function RemoveDuplicates(ByVal connection As System.Data.SQLite.SQLiteConnection) As Integer
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"DELETE FROM [dbo].[{ToleranceBuilder.TableName}] WHERE {NameOf(ToleranceNub.ToleranceCode)} = '@'; ")
        Return connection.Execute(queryBuilder.ToString().Clean())
    End Function

    ''' <summary> Removes the duplicates described by connection. </summary>
    ''' <remarks> David, 6/8/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of records affected. </returns>
    Private Shared Function RemoveDuplicates(ByVal connection As System.Data.SqlClient.SqlConnection) As Integer
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"DELETE FROM [dbo].[{ToleranceBuilder.TableName}] WHERE {NameOf(ToleranceNub.ToleranceCode)} = N'@'; ")
        Return connection.Execute(queryBuilder.ToString().Clean())
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{ToleranceBuilder.TableName}] (
	[{NameOf(ToleranceNub.ToleranceCode)}] [nchar](1) NOT NULL PRIMARY KEY, 
	[{NameOf(ToleranceNub.LowerLimit)}] [float] NOT NULL,
	[{NameOf(ToleranceNub.UpperLimit)}] [float] NOT NULL,
	[{NameOf(ToleranceNub.Caption)}] [nvarchar](10) NOT NULL,
	[{NameOf(ToleranceNub.CompoundCaption)}] [nvarchar](13) NOT NULL); 
CREATE UNIQUE INDEX [{ToleranceBuilder.CaptionIndexName}] ON [{ToleranceBuilder.TableName}] ([{NameOf(ToleranceNub.Caption)}]); 
CREATE TRIGGER [{ToleranceBuilder.TableName}_Tr] AFTER UPDATE OF [{NameOf(ToleranceNub.ToleranceCode)}], [{NameOf(ToleranceNub.Caption)}] ON [{ToleranceBuilder.TableName}]
BEGIN
    UPDATE [{ToleranceBuilder.TableName}]
    SET    [{NameOf(ToleranceNub.CompoundCaption)}] = (([{NameOf(ToleranceNub.ToleranceCode)}]+': ')+[{NameOf(ToleranceNub.Caption)}])
    WHERE  [rowId] = [NEW].[RowId];
END;")
        connection.Execute(queryBuilder.ToString().Clean())
        Return ToleranceBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{ToleranceBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{ToleranceBuilder.TableName}](
	[{NameOf(ToleranceNub.ToleranceCode)}] [nchar](1) NOT NULL,
	[{NameOf(ToleranceNub.LowerLimit)}] [float] NOT NULL,
	[{NameOf(ToleranceNub.UpperLimit)}] [float] NOT NULL,
	[{NameOf(ToleranceNub.Caption)}] [nvarchar](10) NOT NULL,
	[{NameOf(ToleranceNub.CompoundCaption)}]  AS (([{NameOf(ToleranceNub.ToleranceCode)}]+': ')+[{NameOf(ToleranceNub.Caption)}]) PERSISTED NOT NULL,
 CONSTRAINT [PK_{ToleranceBuilder.TableName}] PRIMARY KEY CLUSTERED 
([{NameOf(ToleranceNub.ToleranceCode)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END;
CREATE UNIQUE INDEX [{ToleranceBuilder.CaptionIndexName}] ON [{ToleranceBuilder.TableName}] ([{NameOf(ToleranceNub.Caption)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return ToleranceBuilder.TableName
    End Function



End Class

''' <summary> Implements the Tolerance table <see cref="ITolerance">interface</see>. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2019 </para>
''' </remarks>
<Table("Tolerance")>
Public Class ToleranceNub
    Inherits EntityNubBase(Of ITolerance)
    Implements ITolerance

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ITolerance
        Return New ToleranceNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ITolerance
        Dim destination As ITolerance = Me.CreateNew
        ToleranceNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ITolerance)
        ToleranceNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As ITolerance, ByVal destination As ITolerance)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.Caption = source.Caption
        destination.LowerLimit = source.LowerLimit
        destination.UpperLimit = source.UpperLimit
        destination.ToleranceCode = source.ToleranceCode
    End Sub

#End Region

#Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, ITolerance))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As ITolerance) As Boolean
        Return other IsNot Nothing AndAlso ToleranceNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As ITolerance, ByVal right As ITolerance) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso String.Equals(left.Caption, right.Caption)
            result = result AndAlso Double.Equals(left.LowerLimit, right.LowerLimit)
            result = result AndAlso Double.Equals(left.UpperLimit, right.UpperLimit)
            result = result AndAlso String.Equals(left.ToleranceCode, right.ToleranceCode)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.Caption.GetHashCode() Xor Me.LowerLimit.GetHashCode() Xor Me.UpperLimit.GetHashCode() Xor Me.ToleranceCode.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Tolerance. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the Tolerance. </value>
    <ExplicitKey>
    Public Property ToleranceCode As String Implements ITolerance.ToleranceCode

    ''' <summary> Gets or sets the lower limit. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The lower limit. </value>
    Public Property LowerLimit As Double Implements ITolerance.LowerLimit

    ''' <summary> Gets or sets the upper limit. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The upper limit. </value>
    Public Property UpperLimit As Double Implements ITolerance.UpperLimit

    ''' <summary> Gets or sets the caption. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The caption. </value>
    Public Property Caption As String Implements ITolerance.Caption

    ''' <summary> Gets or sets the timestamp. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The timestamp. </value>
    <Computed>
    Public Property CompoundCaption As String Implements ITolerance.CompoundCaption

#End Region

End Class

''' <summary> The Tolerance Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2019 </para>
''' </remarks>
Public Class ToleranceEntity
    Inherits EntityBase(Of ITolerance, ToleranceNub)
    Implements ITolerance

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New ToleranceNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As ITolerance)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ITolerance, ByVal store As ITolerance)
        MyBase.New(New ToleranceNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ToleranceBuilder.TableName, NameOf(ITolerance).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ITolerance
        Return New ToleranceNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ITolerance
        Dim destination As ITolerance = Me.CreateNew
        ToleranceNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ITolerance)
        ToleranceNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ITolerance)
        ' first make the copy to notify of any property change.
        ToleranceNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        the Meter Model table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As String) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of ITolerance)(key), connection.Get(Of ToleranceNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.ToleranceCode)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Caption)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 4/28/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="caption">    The tolerance caption. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal caption As String) As Boolean
        Me.ClearStore()
        Dim nub As ToleranceNub = ToleranceEntity.FetchNubs(connection, caption).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Overrides in order to fetch the stored
    ''' entity.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ITolerance) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ToleranceEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Caption) Then
            ' update the existing record from the specified entity.
            entity.ToleranceCode = Me.ToleranceCode
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As String) As Boolean
        Return connection.Delete(Of ToleranceNub)(New ToleranceNub With {.ToleranceCode = key})
    End Function

    ''' <summary> Updates the compound caption. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="caption">    The tolerance caption. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function UpdateCompoundCaption(ByVal connection As System.Data.IDbConnection, ByVal caption As String) As Boolean
        ToleranceBuilder.UpdateCompoundCaption(connection, Me.ToleranceCode, caption)
        Return Me.FetchUsingKey(connection)
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the tolerance entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Tolerance entities. </value>
    Public Shared ReadOnly Property Tolerances As IEnumerable(Of ToleranceEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ToleranceEntity)
        Return If(usingNativeTracking, ToleranceEntity.Populate(connection.GetAll(Of ITolerance)), ToleranceEntity.Populate(connection.GetAll(Of ToleranceNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        ToleranceEntity._Tolerances = ToleranceEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(ToleranceEntity.Tolerances?.Any, ToleranceEntity.Tolerances.Count, 0)
    End Function

    ''' <summary> Populates a list of tolerance entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> the Meter Model nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ToleranceNub)) As IEnumerable(Of ToleranceEntity)
        Dim l As New List(Of ToleranceEntity)
        If nubs?.Any Then
            For Each nub As ToleranceNub In nubs
                l.Add(New ToleranceEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of tolerance entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> the Meter Model interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of ITolerance)) As IEnumerable(Of ToleranceEntity)
        Dim l As New List(Of ToleranceEntity)
        If interfaces?.Any Then
            Dim nub As New ToleranceNub
            For Each iFace As ITolerance In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ToleranceEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Dictionary of entity lookups. </summary>
    Private Shared _EntityLookupDictionary As IDictionary(Of String, ToleranceEntity)

    ''' <summary> The tolerance entity lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of Integer, ToleranceEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of String, ToleranceEntity)
        If Not (ToleranceEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            ToleranceEntity._EntityLookupDictionary = New Dictionary(Of String, ToleranceEntity)
            For Each entity As ToleranceEntity In ToleranceEntity.Tolerances
                ToleranceEntity._EntityLookupDictionary.Add(entity.ToleranceCode, entity)
            Next
        End If
        Return ToleranceEntity._EntityLookupDictionary
    End Function

    ''' <summary> Dictionary of key lookups. </summary>
    Private Shared _KeyLookupDictionary As IDictionary(Of String, String)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, String)
        If Not (ToleranceEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            ToleranceEntity._KeyLookupDictionary = New Dictionary(Of String, String)
            For Each entity As ToleranceEntity In ToleranceEntity.Tolerances
                ToleranceEntity._KeyLookupDictionary.Add(entity.Caption, entity.ToleranceCode)
            Next
        End If
        Return ToleranceEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (ToleranceEntity.Tolerances?.Any AndAlso
                ToleranceEntity._EntityLookupDictionary?.Any AndAlso
                ToleranceEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="caption">    The tolerance caption. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal caption As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ToleranceBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ToleranceNub.Caption)} = @Caption", New With {caption})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="caption">    The tolerance caption number. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal caption As String) As IEnumerable(Of ToleranceNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ToleranceBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ToleranceNub.Caption)} = @Caption", New With {caption})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ToleranceNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Tolerance exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="caption">    The tolerance caption. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal caption As String) As Boolean
        Return 1 = ToleranceEntity.CountEntities(connection, caption)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Tolerance. </summary>
    ''' <value> Identifies the Tolerance. </value>
    Public Property ToleranceCode As String Implements ITolerance.ToleranceCode
        Get
            Return Me.ICache.ToleranceCode
        End Get
        Set(value As String)
            If Not Integer.Equals(Me.ToleranceCode, value) Then
                Me.ICache.ToleranceCode = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the lower limit. </summary>
    ''' <value> The lower limit. </value>
    Public Property LowerLimit As Double Implements ITolerance.LowerLimit
        Get
            Return Me.ICache.LowerLimit
        End Get
        Set(value As Double)
            If Not Integer.Equals(Me.LowerLimit, value) Then
                Me.ICache.LowerLimit = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the upper limit. </summary>
    ''' <value> The upper limit. </value>
    Public Property UpperLimit As Double Implements ITolerance.UpperLimit
        Get
            Return Me.ICache.UpperLimit
        End Get
        Set(value As Double)
            If Me.UpperLimit <> value Then
                Me.ICache.UpperLimit = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the caption. </summary>
    ''' <value> The caption. </value>
    Public Property Caption As String Implements ITolerance.Caption
        Get
            Return Me.ICache.Caption
        End Get
        Set(value As String)
            If Not String.Equals(Me.Caption, value) Then
                Me.ICache.Caption = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ToleranceEntity.Identity))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the timestamp. </summary>
    ''' <value> The timestamp. </value>
    Public Property CompoundCaption As String Implements ITolerance.CompoundCaption
        Get
            Return Me.ICache.CompoundCaption
        End Get
        Set(value As String)
            If Not String.Equals(Me.CompoundCaption, value) Then
                Me.ICache.CompoundCaption = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " CUSTOM FIELDS "

    ''' <summary> Gets the identity. </summary>
    ''' <value> The identity. </value>
    Public ReadOnly Property Identity As String
        Get
            Return $"{Me.Caption}"
        End Get
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="toleranceCode"> The entity unique key. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As ToleranceEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection,
                                            ByVal toleranceCode As String) As (Success As Boolean, Details As String, Entity As ToleranceEntity)
        Dim activity As String = String.Empty
        Dim entity As New ToleranceEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching {NameOf(ToleranceEntity)} by {NameOf(ToleranceNub.ToleranceCode)} of {toleranceCode}"
            If Not entity.FetchUsingKey(connection, toleranceCode) Then
                details = $"Failed {activity}"
                success = False
            End If
        Catch ex As Exception
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            success = False
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="toleranceCaption"> The tolerance caption. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As ToleranceEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal toleranceCaption As String) As (Success As Boolean, Details As String, Entity As ToleranceEntity)
        Dim activity As String = String.Empty
        Dim entity As New ToleranceEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching {NameOf(ToleranceEntity)} by {NameOf(ToleranceNub.Caption)} of {toleranceCaption}"
            If Not entity.FetchUsingUniqueIndex(connection, toleranceCaption) Then
                details = $"Failed {activity}"
                success = False
            End If
        Catch ex As Exception
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            success = False
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of ToleranceEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of ToleranceEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New ToleranceEntity()
            activity = $"fetching all {NameOf(ToleranceEntity)}s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> ToleranceEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(ToleranceEntity.EntityLookupDictionary)} count must equal {NameOf(ToleranceEntity.Tolerances)} count ")
            ElseIf elementCount <> ToleranceEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(ToleranceEntity.KeyLookupDictionary)} count must equal {NameOf(ToleranceEntity.Tolerances)} count ")
            End If
            Return (True, String.Empty, ToleranceEntity.Tolerances)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of ToleranceEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

