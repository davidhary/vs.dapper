Imports System.ComponentModel
Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

Imports System.Drawing

''' <summary> A Bucket Bin builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class BucketBinBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return BucketBinBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(BucketBinBuilder._TableName) Then
                BucketBinBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(BucketBinNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary>
    ''' Inserts or ignore the records as described by the <see cref="BucketBin"/> enumeration type.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(BucketBin), Array.Empty(Of Integer)())
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As BucketBinBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As BucketBinBuilder
        If BucketBinBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                BucketBinBuilder._Instance = New BucketBinBuilder()
            End SyncLock
        End If
        Return BucketBinBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the BucketBin table based on the <see cref="INominal">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("BucketBin")>
Public Class BucketBinNub
    Inherits NominalNub
    Implements INominal

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New BucketBinNub
    End Function

End Class

''' <summary> The BucketBin Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class BucketBinEntity
    Inherits EntityBase(Of INominal, BucketBinNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New BucketBinNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The BucketBin interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New BucketBinNub, cache, store)
        Me.UsingNativeTracking = String.Equals(BucketBinBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New BucketBinNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        BucketBinNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As INominal)
        BucketBinNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The BucketBin interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        BucketBinNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The BucketBin table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of BucketBinNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Bucket Bin label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As BucketBinNub = BucketBinEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If BucketBinEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of BucketBinNub)(New BucketBinNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the BucketBin entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The BucketBin entities. </value>
    Public Shared ReadOnly Property BucketBins As IEnumerable(Of BucketBinEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of BucketBinEntity)
        Return If(usingNativeTracking, BucketBinEntity.Populate(connection.GetAll(Of INominal)), BucketBinEntity.Populate(connection.GetAll(Of BucketBinNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        BucketBinEntity._BucketBins = BucketBinEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(BucketBinEntity.BucketBins?.Any, BucketBinEntity.BucketBins.Count, 0)
    End Function

    ''' <summary> Populates a list of BucketBin entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The BucketBin nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of BucketBinNub)) As IEnumerable(Of BucketBinEntity)
        Dim l As New List(Of BucketBinEntity)
        If nubs?.Any Then
            For Each nub As BucketBinNub In nubs
                l.Add(New BucketBinEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of BucketBin entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The BucketBin interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of BucketBinEntity)
        Dim l As New List(Of BucketBinEntity)
        If interfaces?.Any Then
            Dim nub As New BucketBinNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New BucketBinEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Dictionary of entity lookups. </summary>
    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, BucketBinEntity)

    ''' <summary> The Bucket Bin Lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of Integer, BucketBinEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, BucketBinEntity)
        If Not (BucketBinEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            BucketBinEntity._EntityLookupDictionary = New Dictionary(Of Integer, BucketBinEntity)
            For Each entity As BucketBinEntity In BucketBinEntity.BucketBins
                BucketBinEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return BucketBinEntity._EntityLookupDictionary
    End Function

    ''' <summary> Dictionary of key lookups. </summary>
    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (BucketBinEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            BucketBinEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As BucketBinEntity In BucketBinEntity.BucketBins
                BucketBinEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return BucketBinEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (BucketBinEntity.BucketBins?.Any AndAlso
                BucketBinEntity._EntityLookupDictionary?.Any AndAlso
                BucketBinEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Bucket Bin label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{BucketBinNub.TableName}]
        '  WHERE {NameOf(BucketBinNub.PlatformName)} = @PlatformName", New With {Key .PlatformName = PlatformName})
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{BucketBinBuilder.TableName}] WHERE {NameOf(BucketBinNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Bucket Bin label. </param>
    ''' <returns> An IBucketBin . </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of BucketBinNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{BucketBinBuilder.TableName}] WHERE {NameOf(BucketBinNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of BucketBinNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the BucketBin exists. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Bucket Bin label. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = BucketBinEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Bucket Bin. </summary>
    ''' <value> Identifies the Bucket Bin. </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(BucketBinEntity.BucketBin))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the BucketBin. </summary>
    ''' <value> The BucketBin. </value>
    Public Property BucketBin As BucketBin
        Get
            Return CType(Me.Id, BucketBin)
        End Get
        Set(ByVal value As BucketBin)
            Me.Id = value
        End Set
    End Property

    ''' <summary> Gets or sets the Bucket Bin label. </summary>
    ''' <value> The Bucket Bin label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the Bucket Bin. </summary>
    ''' <value> The Bucket Bin description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="bucketBinId"> The entity unique key. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As BucketBinEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection,
                                            ByVal bucketBinId As Integer) As (Success As Boolean, Details As String, Entity As BucketBinEntity)
        Dim activity As String = String.Empty
        Dim entity As New BucketBinEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching {NameOf(BucketBinEntity)} by {NameOf(BucketBinNub.Id)} of {bucketBinId}"
            If Not entity.FetchUsingKey(connection, bucketBinId) Then
                details = $"Failed {activity}"
                success = False
            End If
        Catch ex As Exception
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            success = False
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="bucketBinLabel"> The Bucket bin label. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As BucketBinEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal bucketBinLabel As String) As (Success As Boolean, Details As String, Entity As BucketBinEntity)
        Dim activity As String = String.Empty
        Dim entity As New BucketBinEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching {NameOf(BucketBinEntity)} by {NameOf(BucketBinNub.Label)} of {bucketBinLabel}"
            If Not entity.FetchUsingUniqueIndex(connection, bucketBinLabel) Then
                details = $"Failed {activity}"
                success = False
            End If
        Catch ex As Exception
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            success = False
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of BucketBinEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of BucketBinEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New BucketBinEntity()
            activity = $"fetching all {NameOf(BucketBinEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> BucketBinEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(BucketBinEntity.EntityLookupDictionary)} count must equal {NameOf(BucketBinEntity.BucketBins)} count ")
            ElseIf elementCount <> BucketBinEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(BucketBinEntity.KeyLookupDictionary)} count must equal {NameOf(BucketBinEntity.BucketBins)} count ")
            End If
            Return (True, String.Empty, BucketBinEntity.BucketBins)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of BucketBinEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

#Region " DEFAULT BUCKET BINS "

    ''' <summary> Converts a <see cref="BucketBin"/> to a bucket number. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <param name="bucketBin"> The bucket bin. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function ToBucketNumber(ByVal bucketBin As BucketBin) As Integer
        Return bucketBin + 1
    End Function

    ''' <summary> The bucket number bin mapper. </summary>
    Private Shared _BucketNumberBinMapper As IDictionary(Of Integer, BucketBin)

    ''' <summary> Gets or sets the bucket mapper. </summary>
    ''' <value> The bucket mapper. </value>
    Public Shared Property BucketNumberBinMapper As IDictionary(Of Integer, BucketBin)
        Get
            If BucketBinEntity._BucketNumberBinMapper Is Nothing Then
                BucketBinEntity._BucketNumberBinMapper = New Dictionary(Of Integer, BucketBin) From {
                    {BucketBinEntity.ToBucketNumber(BucketBin.Good), BucketBin.Good},
                    {BucketBinEntity.ToBucketNumber(BucketBin.Bad), BucketBin.Bad},
                    {BucketBinEntity.ToBucketNumber(BucketBin.Overflow), BucketBin.Overflow},
                    {BucketBinEntity.ToBucketNumber(BucketBin.Unknown), BucketBin.Unknown}
                }
            End If
            Return BucketBinEntity._BucketNumberBinMapper
        End Get
        Set(value As IDictionary(Of Integer, BucketBin))
            BucketBinEntity._BucketNumberBinMapper = value
        End Set
    End Property

    ''' <summary> The product sort bin mapper. </summary>
    Private Shared _ProductSortBinMapper As IDictionary(Of Integer, BucketBin)

    ''' <summary> Gets or sets the mapper from product sort to Bucket Bin. </summary>
    ''' <value> The bucket mapper. </value>
    Public Shared Property ProductSortBinMapper As IDictionary(Of Integer, BucketBin)
        Get
            If BucketBinEntity._ProductSortBinMapper Is Nothing Then
                BucketBinEntity._ProductSortBinMapper = New Dictionary(Of Integer, BucketBin) From {
                    {Entities.ProductSort.CompoundResistorGuardFailed, BucketBin.Bad},
                    {Entities.ProductSort.ComputedValueGuardFailed, BucketBin.Bad},
                    {Entities.ProductSort.Good, BucketBin.Good},
                    {Entities.ProductSort.Nameless, BucketBin.Unknown},
                    {Entities.ProductSort.ReadingFailed, BucketBin.Unknown},
                    {Entities.ProductSort.ReadingOverflow, BucketBin.Overflow},
                    {Entities.ProductSort.ResistorGuardFailed, BucketBin.Bad}
                }
            End If
            Return BucketBinEntity._ProductSortBinMapper
        End Get
        Set(value As IDictionary(Of Integer, BucketBin))
            BucketBinEntity._ProductSortBinMapper = value
        End Set
    End Property

    ''' <summary> Converts a <see cref="BucketBin"/> to a bucket number. </summary>
    ''' <remarks> David, 7/7/2020. </remarks>
    ''' <param name="productSort"> The product sort. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function ToBucketNumber(ByVal productSort As ProductSort) As Integer
        Return BucketBinEntity.ToBucketNumber(BucketBinEntity.ProductSortBinMapper(productSort))
    End Function

    ''' <summary> Converts a value to a bucket bin. </summary>
    ''' <remarks> David, 7/7/2020. </remarks>
    ''' <param name="value"> The BucketBin interface. </param>
    ''' <returns> Value as a BucketBin. </returns>
    Public Shared Function ToBucketBin(ByVal value As Integer) As BucketBin
        Return If([Enum].IsDefined(GetType(BucketBin), value), CType(value, BucketBin), BucketBin.Unknown)
    End Function

    ''' <summary> Query if 'bucketBin' is present. </summary>
    ''' <remarks> David, 7/8/2020. </remarks>
    ''' <param name="bucketBin"> The bucket bin. </param>
    ''' <returns> True if present, false if not. </returns>
    Public Shared Function IsPresent(ByVal bucketBin As BucketBin) As Boolean
        Return Not ((bucketBin = BucketBin.Overflow) OrElse (bucketBin = BucketBin.Unknown))
    End Function

#End Region

#Region " DEFAULT BUCKET BIN COLOR "

    ''' <summary> The back color mapper. </summary>
    Private Shared _BackColorMapper As IDictionary(Of BucketBin, Integer)

    ''' <summary> Gets or sets the bucket background color mapper. </summary>
    ''' <value> The bucket background color mapper. </value>
    Public Shared Property BackColorMapper As IDictionary(Of BucketBin, Integer)
        Get
            If BucketBinEntity._BackColorMapper Is Nothing Then
                BucketBinEntity._BackColorMapper = New Dictionary(Of BucketBin, Integer) From {
                    {BucketBin.Good, Color.SeaGreen.ToArgb},
                    {BucketBin.Bad, Color.Crimson.ToArgb},
                    {BucketBin.Overflow, Color.OrangeRed.ToArgb},
                    {BucketBin.Unknown, Color.Silver.ToArgb}
                }
            End If
            Return BucketBinEntity._BackColorMapper
        End Get
        Set(value As IDictionary(Of BucketBin, Integer))
            BucketBinEntity._BackColorMapper = value
        End Set
    End Property

#End Region

#Region " DEFAULT BUCKET BIN DIGITAL OUTPUT "

    ''' <summary> The digital output mapper. </summary>
    Private Shared _DigitalOutputMapper As IDictionary(Of BucketBin, Integer)

    ''' <summary> Gets or sets the bucket digital output mapper. </summary>
    ''' <value> The bucket digital output mapper. </value>
    Public Shared Property DigitalOutputMapper As IDictionary(Of BucketBin, Integer)
        Get
            If BucketBinEntity._DigitalOutputMapper Is Nothing Then
                BucketBinEntity._DigitalOutputMapper = New Dictionary(Of BucketBin, Integer) From {
                    {BucketBin.Good, 1},
                    {BucketBin.Bad, 2},
                    {BucketBin.Overflow, 4},
                    {BucketBin.Unknown, 4}
                }
            End If
            Return BucketBinEntity._DigitalOutputMapper
        End Get
        Set(value As IDictionary(Of BucketBin, Integer))
            BucketBinEntity._DigitalOutputMapper = value
        End Set
    End Property

#End Region

#Region " DEFAULT BUCKET CONTINUOUS FAILURE COUNT LIMIT "

    ''' <summary> The continuous failure count limit mapper. </summary>
    Private Shared _ContinuousFailureCountLimitMapper As IDictionary(Of BucketBin, Integer)

    ''' <summary> Gets or sets the continuous failure count limit mapper. </summary>
    ''' <value> The bucket continuous failure count limit mapper. </value>
    Public Shared Property ContinuousFailureCountLimitMapper As IDictionary(Of BucketBin, Integer)
        Get
            If BucketBinEntity._ContinuousFailureCountLimitMapper Is Nothing Then
                BucketBinEntity._ContinuousFailureCountLimitMapper = New Dictionary(Of BucketBin, Integer) From {
                    {BucketBin.Good, 0},
                    {BucketBin.Bad, 10},
                    {BucketBin.Overflow, 10},
                    {BucketBin.Unknown, 10}
                }
            End If
            Return BucketBinEntity._ContinuousFailureCountLimitMapper
        End Get
        Set(value As IDictionary(Of BucketBin, Integer))
            BucketBinEntity._ContinuousFailureCountLimitMapper = value
        End Set
    End Property

#End Region

End Class

''' <summary> Values that represent bucket bins. </summary>
''' <remarks> David, 2020-07-04. </remarks>
Public Enum BucketBin

    ''' <summary> . </summary>
    <Description("Good")> Good = 0

    ''' <summary> . </summary>
    <Description("Bad")> Bad = 1

    ''' <summary> . </summary>
    <Description("Overflow")> Overflow = 2

    ''' <summary> . </summary>
    <Description("Unknown")> Unknown = 3
End Enum


