Imports System.ComponentModel
Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A Grade builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class GradeBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return GradeBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(GradeBuilder._TableName) Then
                GradeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(GradeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary>
    ''' Inserts or ignores the records described by the <see cref="Grade"/> enumeration type.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(Grade), Array.Empty(Of Integer)())
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As GradeBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As GradeBuilder
        If GradeBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                GradeBuilder._Instance = New GradeBuilder()
            End SyncLock
        End If
        Return GradeBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Grade table based on the <see cref="INominal">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("Grade")>
Public Class GradeNub
    Inherits NominalNub
    Implements INominal

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New GradeNub
    End Function

End Class

''' <summary> The Grade Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class GradeEntity
    Inherits EntityBase(Of INominal, GradeNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New GradeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Grade interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New GradeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(GradeBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New GradeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        GradeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As INominal)
        GradeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Grade interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        GradeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Grade table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of GradeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      Name of the Grade. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As GradeNub = GradeEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If GradeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of GradeNub)(New GradeNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the Grade entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Grade entities. </value>
    Public Shared ReadOnly Property Grades As IEnumerable(Of GradeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of GradeEntity)
        Return If(usingNativeTracking, GradeEntity.Populate(connection.GetAll(Of INominal)), GradeEntity.Populate(connection.GetAll(Of GradeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        GradeEntity._Grades = GradeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(GradeEntity.Grades?.Any, GradeEntity.Grades.Count, 0)
    End Function

    ''' <summary> Populates a list of Grade entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The Grade nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of GradeNub)) As IEnumerable(Of GradeEntity)
        Dim l As New List(Of GradeEntity)
        If nubs?.Any Then
            For Each nub As GradeNub In nubs
                l.Add(New GradeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Grade entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The Grade interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of GradeEntity)
        Dim l As New List(Of GradeEntity)
        If interfaces?.Any Then
            Dim nub As New GradeNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New GradeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Dictionary of entity lookups. </summary>
    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, GradeEntity)

    ''' <summary> The Entity Lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of Integer, GradeEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, GradeEntity)
        If Not (GradeEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            GradeEntity._EntityLookupDictionary = New Dictionary(Of Integer, GradeEntity)
            For Each entity As GradeEntity In GradeEntity.Grades
                GradeEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return GradeEntity._EntityLookupDictionary
    End Function

    ''' <summary> Dictionary of key lookups. </summary>
    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (GradeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            GradeEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As GradeEntity In GradeEntity.Grades
                GradeEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return GradeEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (GradeEntity.Grades?.Any AndAlso
                GradeEntity._EntityLookupDictionary?.Any AndAlso
                GradeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      Name of the Grade. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{GradeNub.TableName}] 
        ' WHERE {NameOf(GradeNub.PlatformName)} = @PlatformName", New With {Key .PlatformName = PlatformName})
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{GradeBuilder.TableName}] WHERE {NameOf(GradeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      Name of the Grade. </param>
    ''' <returns> An IGrade . </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of GradeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{GradeBuilder.TableName}] WHERE {NameOf(GradeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of GradeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Grade exists. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      Name of the Grade. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = GradeEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Grade. </summary>
    ''' <value> Identifies the Grade. </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(GradeEntity.Grade))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Grade. </summary>
    ''' <value> The Grade. </value>
    Public Property Grade As Grade
        Get
            Return CType(Me.Id, Grade)
        End Get
        Set(ByVal value As Grade)
            Me.Id = value
        End Set
    End Property

    ''' <summary> Gets or sets the Grade label. </summary>
    ''' <value> The Grade label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the Grade. </summary>
    ''' <value> The Grade description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="id">         The entity unique key. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As GradeEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection,
                                            ByVal id As Integer) As (Success As Boolean, Details As String, Entity As GradeEntity)
        Dim activity As String = String.Empty
        Dim entity As New GradeEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching {NameOf(GradeEntity)} by {NameOf(GradeNub.Id)} of {id}"
            If Not entity.FetchUsingKey(connection, id) Then
                details = $"Failed {activity}"
                success = False
            End If
        Catch ex As Exception
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            success = False
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Grade label. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As GradeEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal label As String) As (Success As Boolean, Details As String, Entity As GradeEntity)
        Dim activity As String = String.Empty
        Dim entity As New GradeEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching {NameOf(GradeEntity)} by {NameOf(GradeNub.Label)} of {label}"
            If Not entity.FetchUsingUniqueIndex(connection, label) Then
                details = $"Failed {activity}"
                success = False
            End If
        Catch ex As Exception
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            success = False
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of GradeEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of GradeEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New GradeEntity()
            activity = $"fetching all {NameOf(GradeEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> GradeEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(GradeEntity.EntityLookupDictionary)} count must equal {NameOf(GradeEntity.Grades)} count ")
            ElseIf elementCount <> GradeEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(GradeEntity.KeyLookupDictionary)} count must equal {NameOf(GradeEntity.Grades)} count ")
            End If
            Return (True, String.Empty, GradeEntity.Grades)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of GradeEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Values that represent Tan-Tcr Grades. </summary>
''' <remarks> David, 2020-10-02. </remarks>
Public Enum Grade

    ''' <summary> . </summary>
    <Description("6A")> Zero = 0

    ''' <summary> . </summary>
    <Description("5A")> One = 1

    ''' <summary> . </summary>
    <Description("4A")> Two = 2

    ''' <summary> . </summary>
    <Description("AAA")> Three = 3

    ''' <summary> . </summary>
    <Description("AA")> Four = 4

    ''' <summary> . </summary>
    <Description("A")> Five = 5

    ''' <summary> . </summary>
    <Description("B")> Six = 6

    ''' <summary> . </summary>
    <Description("Not Graded")> Nameless = 30
End Enum
