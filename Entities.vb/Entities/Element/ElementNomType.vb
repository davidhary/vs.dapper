Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Core

''' <summary> A Element-Nominal-Type builder. </summary>
''' <remarks> David, 6/24/2020. </remarks>
Public NotInheritable Class ElementNomTypeBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ElementNomTypeBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ElementNomTypeBuilder._TableName) Then
                ElementNomTypeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ElementNomTypeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = ElementBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(ElementNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = NomTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(NomTypeNub.Id)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(ElementNomTypeEntity.ElementAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(ElementNomTypeEntity.NomTypeId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ElementNomTypeBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ElementNomTypeBuilder
        If ElementNomTypeBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ElementNomTypeBuilder._Instance = New ElementNomTypeBuilder()
            End SyncLock
        End If
        Return ElementNomTypeBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="ElementNomTypeNub"/> based on the
''' <see cref="IOneToMany">interface</see>.
''' </summary>
''' <remarks> David, 6/24/2020. </remarks>
<Table("ElementNomType")>
Public Class ElementNomTypeNub
    Inherits OneToManyNub
    Implements IOneToMany

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToMany
        Return New ElementNomTypeNub
    End Function

End Class

''' <summary>
''' The Element-Nominal-Type Entity based on the <see cref="IOnetoMany">interface</see>.
''' </summary>
''' <remarks> David, 6/24/2020. </remarks>
Public Class ElementNomTypeEntity
    Inherits EntityBase(Of IOneToMany, ElementNomTypeNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    Public Sub New()
        Me.New(New ElementNomTypeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="value"> The Element-Nominal-Type interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New ElementNomTypeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ElementNomTypeBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
        Me.NomTypes = New NomTypeEntityCollection
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToMany
        Return New ElementNomTypeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        ElementNomTypeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        ElementNomTypeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="value"> The Element-Nominal-Type interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        ElementNomTypeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As ElementNomTypeNub = ElementNomTypeEntity.FetchNubs(connection, elementAutoId, nomTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.ElementAutoId, Me.NomTypeId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.ElementAutoId, Me.NomTypeId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, elementAutoId, nomTypeId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ElementNomTypeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As Boolean
        Return connection.Delete(Of ElementNomTypeNub)(New ElementNomTypeNub With {.PrimaryId = elementAutoId, .SecondaryId = nomTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Element-Nominal-Type entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Element-Nominal-Type entities. </value>
    Public ReadOnly Property ElementNomTypes As IEnumerable(Of ElementNomTypeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ElementNomTypeEntity)
        Return If(usingNativeTracking, ElementNomTypeEntity.Populate(connection.GetAll(Of IOneToMany)), ElementNomTypeEntity.Populate(connection.GetAll(Of ElementNomTypeNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ElementNomTypes = ElementNomTypeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ElementNomTypeEntity.ElementNomTypes))
        Return If(Me.ElementNomTypes?.Any, Me.ElementNomTypes.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ElementNomTypeNub)) As IEnumerable(Of ElementNomTypeEntity)
        Dim l As New List(Of ElementNomTypeEntity)
        If nubs?.Any Then
            For Each nub As ElementNomTypeNub In nubs
                l.Add(New ElementNomTypeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of ElementNomTypeEntity)
        Dim l As New List(Of ElementNomTypeEntity)
        If interfaces?.Any Then
            Dim nub As New ElementNomTypeNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ElementNomTypeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>
    ''' Count entities; returns up to <see cref="Entities.NomTypeEntity"/>'s count.
    ''' </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ElementNomTypeBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNomTypeNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of ElementNomTypeEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ElementNomTypeBuilder.TableName}] WHERE {NameOf(ElementNomTypeNub.PrimaryId)} = @Id", New With {Key .Id = elementAutoId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ElementNomTypeEntity.Populate(connection.Query(Of ElementNomTypeNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Nominal Type. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nomTypeId">  Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> The total number of entities by Nominal Type. </returns>
    Public Shared Function CountEntitiesByNomType(ByVal connection As System.Data.IDbConnection, ByVal nomTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ElementNomTypeBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNomTypeNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = nomTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Nominal Type in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nomTypeId">  Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Nominal Types in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByNomType(ByVal connection As System.Data.IDbConnection, ByVal nomTypeId As Integer) As IEnumerable(Of ElementNomTypeEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ElementNomTypeBuilder.TableName}] WHERE {NameOf(ElementNomTypeNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = nomTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ElementNomTypeEntity.Populate(connection.Query(Of ElementNomTypeNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ElementNomTypeBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNomTypeNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(ElementNomTypeNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = nomTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As IEnumerable(Of ElementNomTypeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ElementNomTypeBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNomTypeNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(ElementNomTypeNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = nomTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ElementNomTypeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Element Nominal Type exists. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As Boolean
        Return 1 = ElementNomTypeEntity.CountEntities(connection, elementAutoId, nomTypeId)
    End Function

#End Region

#Region " RELATIONS: ELEMENT "

    ''' <summary> Gets or sets the Element entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Element entity. </value>
    Public ReadOnly Property ElementEntity As ElementEntity

    ''' <summary> Fetches Element Entity. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Element Entity. </returns>
    Public Function FetchElementEntity(ByVal connection As System.Data.IDbConnection) As ElementEntity
        Dim entity As New ElementEntity()
        entity.FetchUsingKey(connection, Me.ElementAutoId)
        Me._ElementEntity = entity
        Return entity
    End Function

    ''' <summary>
    ''' Count elements associated with the specified <paramref name="nomTypeId"/>; expected 1.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nomTypeId">  Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> The total number of elements. </returns>
    Public Shared Function CountElements(ByVal connection As System.Data.IDbConnection, ByVal nomTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{ElementNomTypeBuilder.TableName}] WHERE {NameOf(ElementNomTypeNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = nomTypeId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Elements associated with the specified <paramref name="nomTypeId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nomTypeId">  Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Elements in this collection.
    ''' </returns>
    Public Shared Function FetchElements(ByVal connection As System.Data.IDbConnection, ByVal nomTypeId As Integer) As IEnumerable(Of NomTypeEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ElementNomTypeBuilder.TableName}] WHERE {NameOf(ElementNomTypeNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = nomTypeId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of NomTypeEntity)
        For Each nub As ElementNomTypeNub In connection.Query(Of ElementNomTypeNub)(selector.RawSql, selector.Parameters)
            Dim entity As New ElementNomTypeEntity(nub)
            l.Add(entity.FetchNomTypeEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all elements associated with the specified <paramref name="nomTypeId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nomTypeId">  Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteElements(ByVal connection As System.Data.IDbConnection, ByVal nomTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{ElementNomTypeBuilder.TableName}] WHERE {NameOf(ElementNomTypeNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = nomTypeId})

        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

#End Region

#Region " RELATIONS: NOMINAL TYPE "

    ''' <summary> Gets or sets the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="Entities.NomTypeEntity"/>. </value>
    Public ReadOnly Property NomTypeEntity As NomTypeEntity

    ''' <summary> Fetches a <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The <see cref="Entities.NomTypeEntity"/>. </returns>
    Public Function FetchNomTypeEntity(ByVal connection As System.Data.IDbConnection) As NomTypeEntity
        Dim entity As New NomTypeEntity()
        entity.FetchUsingKey(connection, Me.NomTypeId)
        Me._NomTypeEntity = entity
        Return entity
    End Function

    ''' <summary> Gets or sets a list of types of the nominals. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> A list of types of the nominals. </value>
    Public ReadOnly Property NomTypes As NomTypeEntityCollection

    ''' <summary> Fetches the <see cref="NomTypeEntityCollection"/>. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Nominal Types in this collection.
    ''' </returns>
    Public Function FetchNomTypes(ByVal connection As System.Data.IDbConnection) As NomTypeEntityCollection
        Me.NomTypes.Clear()
        Me.NomTypes.Populate(ElementNomTypeEntity.FetchOrderedNomTypes(connection, Me.ElementAutoId))
        Return Me.NomTypes
    End Function

    ''' <summary> Count nominal types. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of nominal types. </returns>
    Public Shared Function CountNomTypes(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{ElementNomTypeBuilder.TableName}] WHERE {NameOf(ElementNomTypeNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = elementAutoId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the <see cref="Entities.NomTypeEntity"/>'s. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Nominal Types in this collection.
    ''' </returns>
    Public Shared Function FetchNomTypes(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of NomTypeEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ElementNomTypeBuilder.TableName}] WHERE {NameOf(ElementNomTypeNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of NomTypeEntity)
        For Each nub As ElementNomTypeNub In connection.Query(Of ElementNomTypeNub)(selector.RawSql, selector.Parameters)
            Dim entity As New ElementNomTypeEntity(nub)
            l.Add(entity.FetchNomTypeEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all <see cref="Entities.NomTypeEntity"/> related to the specified Element.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteNomTypes(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{ElementNomTypeBuilder.TableName}] WHERE {NameOf(ElementNomTypeNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = elementAutoId})

        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.NomTypeEntity"/>'s. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="selectQuery">   The select query. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered nominal Types in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchOrderedNomTypes(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal elementAutoId As Integer) As IEnumerable(Of NomTypeEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {elementAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return NomTypeEntity.Populate(connection.Query(Of NomTypeNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.NomTypeEntity"/>'s. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered nominal Types in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchOrderedNomTypes(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of NomTypeEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [ElementNomType] on [ElementNomType].SecondaryId = [NomType].AutoId where [ElementNomType].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{NomTypeBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{NomTypeBuilder.TableName}] Inner Join [{ElementNomTypeBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{ElementNomTypeBuilder.TableName}].{NameOf(ElementNomTypeNub.SecondaryId)} = [{NomTypeBuilder.TableName}].{NameOf(NomTypeNub.Id)}")
        queryBuilder.AppendLine($"WHERE [{ElementNomTypeBuilder.TableName}].{NameOf(ElementNomTypeNub.PrimaryId)} = @{NameOf(elementAutoId)}")
        queryBuilder.AppendLine($"ORDER BY [{NomTypeBuilder.TableName}].{NameOf(NomTypeNub.Id)} ASC; ")
        Return ElementNomTypeEntity.FetchOrderedNomTypes(connection, queryBuilder.ToString, elementAutoId)
    End Function

    ''' <summary> Fetches the first <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="selectQuery">   The select query. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Nominal Types in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchFirstNomType(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal elementAutoId As Integer) As NomTypeEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery, New With {elementAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As NomTypeNub = connection.Query(Of NomTypeNub)(template.RawSql, template.Parameters).FirstOrDefault
        Return If(nub Is Nothing, New NomTypeEntity, New NomTypeEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches the last <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Nominal Type in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchLastNomType(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As NomTypeEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [ElementNomType] on [ElementNomType].SecondaryId = [NomType].AutoId where [ElementNomType].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{NomTypeBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{NomTypeBuilder.TableName}] Inner Join [{ElementNomTypeBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{ElementNomTypeBuilder.TableName}].{NameOf(ElementNomTypeNub.SecondaryId)} = [{NomTypeBuilder.TableName}].{NameOf(NomTypeNub.Id)}")
        queryBuilder.AppendLine($"WHERE [{ElementNomTypeBuilder.TableName}].{NameOf(ElementNomTypeNub.PrimaryId)} = @{NameOf(elementAutoId)}")
        queryBuilder.AppendLine($"ORDER BY [{NomTypeBuilder.TableName}].{NameOf(NomTypeNub.Id)} DESC; ")
        Return ElementNomTypeEntity.FetchFirstNomType(connection, queryBuilder.ToString, elementAutoId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ElementNomTypeEntity.ElementAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/> </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/> </value>
    Public Property ElementAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ElementNomTypeEntity.NomTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NomTypeEntity"/>. </value>
    Public Property NomTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of <see cref="ElementNomTypeEntity"/>'s. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class ElementNomTypeEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToMany, ElementNomTypeNub, ElementNomTypeEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As ElementNomTypeEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As ElementNomTypeEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all Elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of ElementNomTypeEntity))
        If entities?.Any Then
            For Each entity As ElementNomTypeEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return ElementNomTypeBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary>
''' Collection of unique <see cref="Entities.ElementEntity"/> +
''' <see cref="Entities.NomTypeEntity"/>'s.
''' </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class ElementUniqueNomTypeEntityCollection
    Inherits ElementNomTypeEntityCollection

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    Public Sub New(ByVal elementAutoId As Integer)
        MyBase.New()
        Me.ElementAutoId = elementAutoId
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As ElementNomTypeEntity)
        If entity.ElementAutoId = Me.ElementAutoId Then
            MyBase.Add(entity)
            Me._PrimaryKeyDictionary.Add(entity.NomTypeId, entity.EntitySelector)
            Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.NomTypeId)
        End If
    End Sub

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    Public Overloads Sub Add(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer)
        Dim elementNomType As New ElementNomTypeEntity With {.ElementAutoId = elementAutoId, .NomTypeId = nomTypeId}
        If elementNomType.Obtain(connection) Then
            Me.Add(elementNomType)
        Else
            Throw New OperationFailedException($"Failed obtaining {NameOf(Entities.ElementNomTypeEntity)} with [{NameOf(Entities.ElementNomTypeEntity.ElementAutoId)},{NameOf(Entities.ElementNomTypeEntity.NomTypeId)} ] of [{elementAutoId},{nomTypeId}] ")
        End If
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Query if collection contains nominal type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="nomTypeId"> Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsNomType(ByVal nomTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(nomTypeId)
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public ReadOnly Property ElementAutoId As Integer

#End Region

End Class

