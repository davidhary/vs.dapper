''' <summary> The Element Text class holing the Element Text values. </summary>
''' <remarks> David, 2020-05-29. </remarks>
Public Class ElementText
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:ElementText" /> class. </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSetter"> The getter setter. </param>
    ''' <param name="element">      The <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifier for the <see cref="Entities.NomTypeEntity"/>. </param>
    Public Sub New(ByVal getterSetter As isr.Core.Constructs.IGetterSetter, ByVal element As ElementEntity, ByVal nomTypeId As Integer)
        MyBase.New()
        Me.GetterSetter = getterSetter
        Me.ElementAutoId = element.AutoId
        Me.ElementLabel = element.ElementLabel
        Me.ElementType = CType(element.ElementTypeId, ElementType)
        Me.NomType = CType(nomTypeId, NomType)
        Me.Selector = nomTypeId
    End Sub

    ''' <summary> Gets or sets the selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property Selector As Integer

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter

    ''' <summary> Gets the text value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Integer. </returns>
    Public Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String
        Return Me.GetterSetter.Getter(name)
    End Function

    ''' <summary> Sets the text value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Integer. </returns>
    Public Function Setter(ByVal value As String, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String
        If Not String.Equals(value, Me.Getter(name)) Then
            Me.GetterSetter.Setter(value, name)
            Me.NotifyPropertyChanged(name)
        End If
        Return value
    End Function

#If False Then
    ''' <summary> Gets or sets the text value. </summary>
    ''' <value> The text value. </value>
    Protected Property TextValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.TextValue(name)) Then
                Me.GetterSetter.Setter(value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property
#End If

#End Region

#Region " IDENTIFIERS "

    ''' <summary> Type of the nom. </summary>
    Private _NomType As NomType

    ''' <summary> Gets or sets the type of the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> The type of the nominal. </value>
    Public Property NomType As NomType
        Get
            Return Me._NomType
        End Get
        Set
            If Value <> Me.NomType Then
                Me._NomType = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Identifier for the element automatic. </summary>
    Private _ElementAutoId As Integer

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property ElementAutoId As Integer
        Get
            Return Me._ElementAutoId
        End Get
        Set
            If Value <> Me.ElementAutoId Then
                Me._ElementAutoId = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " ELEMENT PROPERTIES "

    ''' <summary> The element label. </summary>
    Private _ElementLabel As String

    ''' <summary> Gets or sets the Label of the Element. </summary>
    ''' <value> The Label of the Element. </value>
    Public Property ElementLabel As String
        Get
            Return Me._ElementLabel
        End Get
        Set
            If Not String.Equals(Value, Me.ElementLabel) Then
                Me._ElementLabel = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Type of the element. </summary>
    Private _ElementType As ElementType

    ''' <summary> Gets or sets the type of the Element. </summary>
    ''' <value> The type of the Element. </value>
    Public Property ElementType As ElementType
        Get
            Return Me._ElementType
        End Get
        Set
            If Value <> Me.ElementType Then
                Me._ElementType = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " SCANNNING "

    ''' <summary> Gets or sets a list of scans. </summary>
    ''' <value> A list of scans. </value>
    Public Property ScanList As String
        Get
            Return Me.Getter()
        End Get
        Set(value As String)
            Me.Setter(value)
        End Set
    End Property

#End Region

End Class
