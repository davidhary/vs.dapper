''' <summary> Information about the element. </summary>
''' <remarks> David, 6/15/2020. </remarks>
Public Structure ElementInfo
    Implements IEquatable(Of ElementInfo)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="label">         The label. </param>
    ''' <param name="ordinalNumber"> The ordinal number. </param>
    ''' <param name="elementTypeId"> Identifier for the <see cref="Entities.ElementTypeEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifier for the <see cref="entities.NomTypeEntity"/>. </param>
    ''' <param name="isCritical">    The critical identifier. </param>
    ''' <param name="isPrimary">     The primary identifier. </param>
    Public Sub New(ByVal label As String, ByVal ordinalNumber As Integer, ByVal elementTypeId As Integer, ByVal nomTypeId As Integer,
                   ByVal isCritical As Boolean, ByVal isPrimary As Boolean)
        Me.Label = label
        Me.OrdinalNumber = ordinalNumber
        Me.ElementTypeId = elementTypeId
        Me.NomTypeId = nomTypeId
        Me.IsCritical = isCritical
        Me.IsPrimary = isPrimary
    End Sub

    ''' <summary> Gets or sets the ordinal number. </summary>
    ''' <value> The ordinal number. </value>
    Public Property OrdinalNumber As Integer

    ''' <summary> Gets or sets the label. </summary>
    ''' <value> The label. </value>
    Public Property Label As String

    ''' <summary>
    ''' Gets or sets the identifier for the <see cref="Entities.ElementTypeEntity"/>.
    ''' </summary>
    ''' <value> The identifier of the <see cref="Entities.ElementTypeEntity"/>. </value>
    Public Property ElementTypeId As Integer

    ''' <summary> Gets or sets the identifier for the <see cref="entities.NomTypeEntity"/>. </summary>
    ''' <value> The identifier for the <see cref="entities.NomTypeEntity"/>. </value>
    Public Property NomTypeId As Integer

    ''' <summary> Identifies the critical element. </summary>
    ''' <value> The critical identifier. </value>
    Public Property IsCritical As Boolean

    ''' <summary> Identifies the primary element. </summary>
    ''' <value> The primary identifier. </value>
    Public Property IsPrimary As Boolean

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="obj"> The object to compare with the current instance. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
    ''' represent the same value; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.Equals(CType(obj, ElementInfo))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Function Equals(other As ElementInfo) As Boolean Implements IEquatable(Of ElementInfo).Equals
        Return Me.Label = other.Label AndAlso Me.ElementTypeId = other.ElementTypeId AndAlso Me.OrdinalNumber = other.OrdinalNumber
    End Function

    ''' <summary> Returns the hash code for this instance. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A 32-bit signed integer that is the hash code for this instance. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.Label.GetHashCode Xor Me.ElementTypeId Xor Me.OrdinalNumber
    End Function

    ''' <summary> Cast that converts the given ElementInfo to a =. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(left As ElementInfo, right As ElementInfo) As Boolean
        Return left.Equals(right)
    End Operator

    ''' <summary> Cast that converts the given ElementInfo to a &lt;&gt; </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(left As ElementInfo, right As ElementInfo) As Boolean
        Return Not left = right
    End Operator
End Structure

''' <summary> Collection of part specification elements. </summary>
''' <remarks> David, 4/18/2020. </remarks>
Public Class ElementInfoCollection
    Inherits ObjectModel.KeyedCollection(Of String, ElementInfo)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 7/12/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._OrdinalNumberKeyDictionary = New Dictionary(Of Integer, ElementInfo)
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 4/18/2020. </remarks>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As ElementInfo) As String
        Return item.Label
    End Function

    ''' <summary> Gets or sets a dictionary of ordinal number keys. </summary>
    ''' <value> A dictionary of ordinal number keys. </value>
    Public ReadOnly Property OrdinalNumberKeyDictionary As IDictionary(Of Integer, ElementInfo)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 7/12/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overloads Sub Add(ByVal item As ElementInfo)
        MyBase.Add(item)
        Me._OrdinalNumberKeyDictionary.Add(item.OrdinalNumber, item)
    End Sub

End Class

