
Partial Public Class ElementEntity

    ''' <summary> Gets or sets the <see cref="ElementNomTypeEntity"/>'s. </summary>
    ''' <value> The <see cref="ElementNomTypeEntity"/>'s. </value>
    Public ReadOnly Property NomTypeEntities As ElementUniqueNomTypeEntityCollection

    ''' <summary> Fetches <see cref="ElementNomTypeEntity"/>'s. </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The <see cref="ElementNomTypeEntity"/>'s. </returns>
    Public Function FetchElementNomTypeEntities(ByVal connection As System.Data.IDbConnection) As ElementUniqueNomTypeEntityCollection
        Me._NomTypeEntities = New ElementUniqueNomTypeEntityCollection(Me.AutoId)
        Me._NomTypeEntities.Populate(ElementNomTypeEntity.FetchEntities(connection, Me.AutoId))
        Return Me.NomTypeEntities
    End Function

    ''' <summary> Gets or sets <see cref="NomTypeEntityCollection"/>. </summary>
    ''' <value> <see cref="NomTypeEntityCollection"/>. </value>
    Public ReadOnly Property NomTypes As NomTypeEntityCollection

    ''' <summary> Fetches <see cref="NomTypeEntityCollection"/> </summary>
    ''' <remarks> David, 6/24/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The <see cref="NomTypeEntityCollection"/>. </returns>
    Public Function FetchNomTypes(ByVal connection As System.Data.IDbConnection) As NomTypeEntityCollection
        Dim elementNomType As New ElementNomTypeEntity With {.ElementAutoId = Me.AutoId}
        Me._NomTypes = elementNomType.FetchNomTypes(connection)
        Return Me.NomTypes
    End Function

End Class

