Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Element Nominal Text builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class ElementNomTextBuilder
    Inherits ThreeToManyLabelBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ElementNomTextBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ElementNomTextBuilder._TableName) Then
                ElementNomTextBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ElementNomTextNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = ElementBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(ElementNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = NomTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the Ternary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Ternary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(NomTypeNub.Id)

    ''' <summary> Gets or sets the name of the Ternary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Ternary table. </value>
    Public Overrides Property TernaryTableName As String = ElementTextTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the Ternary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Ternary table key. </value>
    Public Overrides Property TernaryTableKeyName As String = NameOf(ElementTextTypeNub.Id)

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 255

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(ElementNomTextEntity.ElementAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(ElementNomTextEntity.NomTypeId)

    ''' <summary> Gets or sets the name of the ternary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the ternary identifier field. </value>
    Public Overrides Property TernaryIdFieldName As String = NameOf(ElementNomTextEntity.ElementTextTypeId)

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the label field. </value>
    Public Overrides Property LabelFieldName As String = NameOf(ElementNomTextEntity.Label)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ElementNomTextBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ElementNomTextBuilder
        If ElementNomTextBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ElementNomTextBuilder._Instance = New ElementNomTextBuilder()
            End SyncLock
        End If
        Return ElementNomTextBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="Entities.ElementEntity"/> Text table
''' <see cref="IThreeToManyLabel">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
'''     Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("ElementNomText")>
Public Class ElementNomTextNub
    Inherits ThreeToManyLabelNub
    Implements IThreeToManyLabel

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IThreeToManyLabel
        Return New ElementNomTextNub
    End Function

End Class

''' <summary>
''' The <see cref="ElementNomTextEntity"/>. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
'''     Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class ElementNomTextEntity
    Inherits EntityBase(Of IThreeToManyLabel, ElementNomTextNub)
    Implements IThreeToManyLabel

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New ElementNomTextNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.ElementEntity"/>Text interface. </param>
    Public Sub New(ByVal value As IThreeToManyLabel)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IThreeToManyLabel, ByVal store As IThreeToManyLabel)
        MyBase.New(New ElementNomTextNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ElementNomTextBuilder.TableName, NameOf(IThreeToManyLabel).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IThreeToManyLabel
        Return New ElementNomTextNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IThreeToManyLabel
        Dim destination As IThreeToManyLabel = Me.CreateNew
        ElementNomTextNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="ElementNomTextEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IThreeToManyLabel)
        ElementNomTextNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached Text, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.ElementEntity"/>Text interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IThreeToManyLabel)
        ' first make the copy to notify of any property change.
        ElementNomTextNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="elementAutoId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="elementTextTypeId"> Identifies the <see cref="ElementTextTypeEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal elementTextTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ElementNomTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNomTextNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(ElementNomTextNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementTextTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of ElementNomTextNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="elementAutoId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="elementTextTypeId"> Identifies the <see cref="ElementTextTypeEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal elementTextTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As ElementNomTextNub = ElementNomTextEntity.FetchEntities(connection, elementAutoId, elementTextTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IThreeToManyLabel) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ElementNomTextEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="elementAutoId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="elementTextTypeId"> Identifies the <see cref="ElementTextTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal elementTextTypeId As Integer) As Boolean
        Return connection.Delete(Of ElementNomTextNub)(New ElementNomTextNub With {.PrimaryId = elementAutoId, .SecondaryId = elementTextTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="Entities.ElementEntity"/>Text entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> the <see cref="Entities.ElementEntity"/>Text entities. </value>
    Public ReadOnly Property ElementTexts As IEnumerable(Of ElementNomTextEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ElementNomTextEntity)
        Return If(usingNativeTracking, ElementNomTextEntity.Populate(connection.GetAll(Of IThreeToManyLabel)),
                                       ElementNomTextEntity.Populate(connection.GetAll(Of ElementNomTextNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ElementTexts = ElementNomTextEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ElementNomTextEntity.ElementTexts))
        Return If(Me.ElementTexts?.Any, Me.ElementTexts.Count, 0)
    End Function

    ''' <summary> Count Element Texts. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{ElementNomTextBuilder.TableName}] WHERE {NameOf(ElementNomTextNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of ElementNomTextEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ElementNomTextBuilder.TableName}] WHERE {NameOf(ElementNomTextNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = elementAutoId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ElementNomTextEntity.Populate(connection.Query(Of ElementNomTextNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches Element Texts by Element Auto Id. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of ElementNomTextNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ElementNomTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNomTextEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ElementNomTextNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of Element Text entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> the <see cref="Entities.ElementEntity"/>Text nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ElementNomTextNub)) As IEnumerable(Of ElementNomTextEntity)
        Dim l As New List(Of ElementNomTextEntity)
        If nubs?.Any Then
            For Each nub As ElementNomTextNub In nubs
                l.Add(New ElementNomTextEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Element Text entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> the <see cref="Entities.ElementEntity"/>Text interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IThreeToManyLabel)) As IEnumerable(Of ElementNomTextEntity)
        Dim l As New List(Of ElementNomTextEntity)
        If interfaces?.Any Then
            Dim nub As New ElementNomTextNub
            For Each iFace As IThreeToManyLabel In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ElementNomTextEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Element Texts by unique index; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="elementAutoId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="elementTextTypeId"> Identifies the <see cref="ElementTextTypeEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal elementTextTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ElementNomTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNomTextNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(ElementNomTextNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementTextTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches Element Texts by unique index; expected single or none. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="elementAutoId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="elementTextTypeId"> Identifies the <see cref="ElementTextTypeEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal elementTextTypeId As Integer) As IEnumerable(Of ElementNomTextNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ElementNomTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNomTextNub.PrimaryId)} = @primaryId", New With {.primaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(ElementNomTextNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementTextTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ElementNomTextNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.ElementEntity"/>Text exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="elementAutoId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="elementTexttypeId"> Identifies the <see cref="ElementTextTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal elementTexttypeId As Integer) As Boolean
        Return 1 = ElementNomTextEntity.CountEntities(connection, elementAutoId, elementTexttypeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count Texts associated with this element. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of Texts. </returns>
    Public Function CountElementTexts(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ElementNomTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNomTextNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Element Text Texts by Element auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 7/5/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchElementTexts(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of ElementNomTextNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ElementNomTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNomTextNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ElementNomTextNub)(selector.RawSql, selector.Parameters)
    End Function

#End Region

#Region " RELATIONS: ELEMENT "

    ''' <summary> Gets or sets the Element entity. </summary>
    ''' <value> The Element entity. </value>
    Public ReadOnly Property ElementEntity As ElementEntity

    ''' <summary> Fetches a Element Entity. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Element Entity. </returns>
    Public Function FetchElementEntity(ByVal connection As System.Data.IDbConnection) As ElementEntity
        Dim entity As New ElementEntity()
        entity.FetchUsingKey(connection, Me.ElementAutoId)
        Me._ElementEntity = entity
        Return entity
    End Function

#End Region

#Region " RELATIONS: NOM TYPE  "

    ''' <summary>
    ''' Gets or sets the <see cref="Entities.ElementEntity"/> <see cref="NomTypeEntity"/>.
    ''' </summary>
    ''' <value> the <see cref="Entities.ElementEntity"/> Text type entity. </value>
    Public ReadOnly Property NomTypeEntity As NomTypeEntity

    ''' <summary> Fetches a element text type Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchNomTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._NomTypeEntity = New NomTypeEntity()
        Return Me.NomTypeEntity.FetchUsingKey(connection, Me.NomTypeId)
    End Function

#End Region

#Region " RELATIONS: ELEMENT TEXT TYPE  "

    ''' <summary>
    ''' Gets or sets the <see cref="Entities.ElementEntity"/> <see cref="ElementTextTypeEntity"/>.
    ''' </summary>
    ''' <value> the <see cref="Entities.ElementEntity"/> Text type entity. </value>
    Public ReadOnly Property ElementTextTypeEntity As ElementTextTypeEntity

    ''' <summary> Fetches a element text type Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchElementTextTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._ElementTextTypeEntity = New ElementTextTypeEntity()
        Return Me.ElementTextTypeEntity.FetchUsingKey(connection, Me.ElementTextTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IThreeToManyLabel.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ElementNomTextEntity.ElementAutoId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.ElementEntity"/> record.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/> record. </value>
    Public Property ElementAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IThreeToManyLabel.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ElementNomTextEntity.NomTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identity of the <see cref="entities.NomTypeEntity"/>. </summary>
    ''' <value> Identifies the Element Text type. </value>
    Public Property NomTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Ternary reference. </summary>
    ''' <value> The identifier of Ternary reference. </value>
    Public Property TernaryId As Integer Implements IThreeToManyLabel.TernaryId
        Get
            Return Me.ICache.TernaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.TernaryId, value) Then
                Me.ICache.TernaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ElementNomTextEntity.ElementTextTypeId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identity (type) of the <see cref="entities.ElementNomTextEntity"/>.
    ''' </summary>
    ''' <value> Identifies the Element Text type. </value>
    Public Property ElementTextTypeId As Integer
        Get
            Return Me.TernaryId
        End Get
        Set(ByVal value As Integer)
            Me.TernaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the <see cref="entities.ElementNomTextEntity"/> label. </summary>
    ''' <value> the <see cref="entities.ElementNomTextEntity"/> label. </value>
    Public Property Label As String Implements IThreeToManyLabel.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As ThreeKeySelector
        Get
            Return New ThreeKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of <see cref="ElementNomTextEntity"/>'s. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class ElementNomTextEntityCollection
    Inherits EntityKeyedCollection(Of ThreeKeySelector, IThreeToManyLabel, ElementNomTextNub, ElementNomTextEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As ElementNomTextEntity) As ThreeKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As ElementNomTextEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of ElementNomTextEntity))
        If entities?.Any Then
            For Each entity As ElementNomTextEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return ElementNomTextBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary>
''' Collection of <see cref="Entities.ElementEntity"/>-Unique
''' <see cref="ElementNomTextEntity"/>'s.
''' </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class ElementUniqueNomTextEntityCollection
    Inherits ElementNomTextEntityCollection
    Implements Core.Constructs.IGetterSetter

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="element">   The element. </param>
    ''' <param name="nomTypeId"> Identifier for the nom type. </param>
    Public Sub New(ByVal element As ElementEntity, ByVal nomTypeId As Integer)
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of ThreeKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, ThreeKeySelector)
        Me.ElementText = New ElementText(Me, element, nomTypeId)
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of ThreeKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, ThreeKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As ElementNomTextEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.ElementTextTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.ElementTextTypeId)
        Me.NotifyPropertyChanged(ElementTextTypeEntity.EntityLookupDictionary(entity.ElementTextTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains 'elementTextType' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="elementTextTypeId"> Identifies the <see cref="Entities.ElementTextTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal elementTextTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(elementTextTypeId)
    End Function

#End Region

#Region " GETTER SETTER "

    ''' <summary>
    ''' Gets the Real(String)-value for the given <see cref="ElementNomTextEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable String. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String Implements Core.Constructs.IGetterSetter.Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary>
    ''' Set the specified element value for the given <see cref="ElementNomTextEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value">                                      value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A String. </returns>
    Protected Function Setter(ByVal value As String, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String Implements Core.Constructs.IGetterSetter.Setter
        Me.Setter(Me.ToKey(name), value)
        Me.NotifyPropertyChanged(name)
        Return value
    End Function

    ''' <summary> Gets or sets the Element Text. </summary>
    ''' <value> The Element Text. </value>
    Public ReadOnly Property ElementText As ElementText

#End Region

#Region " TRAIT SELECTION "

    ''' <summary> Converts a name to a key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return ElementTextTypeEntity.KeyLookupDictionary(name)
    End Function

    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="elementTextTypeId"> Identifies the <see cref="Entities.ElementTextTypeEntity"/>. </param>
    ''' <returns> An ElementTextEntity. </returns>
    Public Function Entity(ByVal elementTextTypeId As Integer) As ElementNomTextEntity
        Return If(Me.ContainsKey(elementTextTypeId), Me(Me._PrimaryKeyDictionary(elementTextTypeId)), New ElementNomTextEntity)
    End Function

    ''' <summary> Gets the Real(Double)-value for the given Text type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="elementTextTypeId"> Identifies the <see cref="Entities.ElementTextTypeEntity"/>. </param>
    ''' <returns> A String. </returns>
    Public Function Getter(ByVal elementTextTypeId As Integer) As String
        Return If(Me.ContainsKey(elementTextTypeId), Me(Me._PrimaryKeyDictionary(elementTextTypeId)).Label, String.Empty)
    End Function

    ''' <summary> Set the specified Element value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="elementTextTypeId"> Identifies the
    '''                                  <see cref="Entities.ElementTextTypeEntity"/>. </param>
    ''' <param name="value">             The value. </param>
    Public Sub Setter(ByVal elementTextTypeId As Integer, ByVal value As String)
        If Me.ContainsKey(elementTextTypeId) Then
            Me(Me._PrimaryKeyDictionary(elementTextTypeId)).Label = value
        Else
            Me.Add(New ElementNomTextEntity With {.ElementAutoId = Me.ElementAutoId, .ElementTextTypeId = elementTextTypeId, .Label = value})
        End If
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public ReadOnly Property ElementAutoId As Integer

#End Region

End Class

