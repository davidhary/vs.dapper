Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Dapper.Entities.ConnectionExtensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary>
''' A builder for a Element table based on the
''' <see cref="IKeyForeignLabelNatural">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class ElementBuilder
    Inherits KeyForeignLabelNaturalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ElementBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ElementBuilder._TableName) Then
                ElementBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ElementNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the foreign table . </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the foreign table. </value>
    Public Overrides Property ForeignTableName As String = ElementTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the foreign table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the foreign table key. </value>
    Public Overrides Property ForeignTableKeyName As String = NameOf(ElementTypeNub.Id)

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 50

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(ElementEntity.AutoId)

    ''' <summary> Gets or sets the name of the foreign identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName() As String = NameOf(ElementEntity.ElementTypeId)

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the label field. </value>
    Public Overrides Property LabelFieldName() As String = NameOf(ElementEntity.ElementLabel)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName() As String = NameOf(ElementEntity.OrdinalNumber)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ElementBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ElementBuilder
        If ElementBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ElementBuilder._Instance = New ElementBuilder()
            End SyncLock
        End If
        Return ElementBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Natural Element table based on the
''' <see cref="IKeyForeignLabelNatural">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("Element")>
Public Class ElementNub
    Inherits KeyForeignLabelNaturalNub
    Implements IKeyForeignLabelNatural

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IKeyForeignLabelNatural
        Return New ElementNub
    End Function

End Class

''' <summary>
''' The Nominal Element Entity based on the <see cref="IKeyForeignLabelNatural">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class ElementEntity
    Inherits EntityBase(Of IKeyForeignLabelNatural, ElementNub)
    Implements IKeyForeignLabelNatural

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New ElementNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Element interface. </param>
    Public Sub New(ByVal value As IKeyForeignLabelNatural)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyForeignLabelNatural, ByVal store As IKeyForeignLabelNatural)
        MyBase.New(New ElementNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ElementBuilder.TableName, NameOf(IKeyForeignLabelNatural).TrimStart("I"c))
        Me.NomTraitsCollection = New PartNomEntitiesCollection
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IKeyForeignLabelNatural
        Return New ElementNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IKeyForeignLabelNatural
        Dim destination As IKeyForeignLabelNatural = Me.CreateNew
        ElementNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IKeyForeignLabelNatural)
        ElementNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Element interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyForeignLabelNatural)
        ' first make the copy to notify of any property change.
        ElementNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Element table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyForeignLabelNatural)(key), connection.Get(Of ElementNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="elementLabel"> The Element number. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal elementLabel As String) As Boolean
        Me.ClearStore()
        Dim nub As ElementNub = ElementEntity.FetchNubs(connection, elementLabel).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyForeignLabelNatural) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ElementEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(UniqueIndexOptions.Label = (ElementBuilder.Get.QueryUniqueIndexOptions(connection) And UniqueIndexOptions.Label),
                                        Me.FetchUsingUniqueIndex(connection, entity.Label),
                                        Me.FetchUsingKey(connection, entity.AutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of ElementNub)(New ElementNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Element entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Element entities. </value>
    Public ReadOnly Property Elements As IEnumerable(Of ElementEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ElementEntity)
        Return If(usingNativeTracking, ElementEntity.Populate(connection.GetAll(Of IKeyForeignLabelNatural)), ElementEntity.Populate(connection.GetAll(Of ElementNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Elements = ElementEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ElementEntity.Elements))
        Return If(Me.Elements?.Any, Me.Elements.Count, 0)
    End Function

    ''' <summary> Populates a list of Element entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The Element nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal nubs As IEnumerable(Of ElementNub)) As IEnumerable(Of ElementEntity)
        Dim l As New List(Of ElementEntity)
        If nubs?.Any Then
            For Each nub As ElementNub In nubs
                l.Add(New ElementEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Element entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The Element interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyForeignLabelNatural)) As IEnumerable(Of ElementEntity)
        Dim l As New List(Of ElementEntity)
        If interfaces?.Any Then
            Dim nub As New ElementNub
            For Each iFace As IKeyForeignLabelNatural In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ElementEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="elementLabel"> The Element number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal elementLabel As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ElementBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNub.Label)} = @elementLabel", New With {elementLabel})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="elementLabel"> The Element number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal elementLabel As String) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{ElementBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(ElementNub.Label)} IS NULL OR {NameOf(ElementNub.Label)} = @ElementLabel)")
        selectSql.Append("; ")
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {elementLabel})
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="elementLabel"> The Element number. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal elementLabel As String) As IEnumerable(Of ElementNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ElementBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ElementNub.Label)} = @elementLabel", New With {elementLabel})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ElementNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Element exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="elementLabel"> The Element number. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal elementLabel As String) As Boolean
        Return 1 = ElementEntity.CountEntities(connection, elementLabel)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public Property AutoId As Integer Implements IKeyForeignLabelNatural.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Element type. </summary>
    ''' <value> Identifies the Element type. </value>
    Public Property ForeignId As Integer Implements IKeyForeignLabelNatural.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ElementEntity.ElementTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identity of the Element Type. </summary>
    ''' <value> The type of the identity of the Element Type. </value>
    Public Property ElementTypeId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(value As Integer)
            Me.ForeignId = value
        End Set
    End Property

    ''' <summary> Gets or sets the element Label. </summary>
    ''' <value> The Label. </value>
    Public Property Label As String Implements IKeyForeignLabelNatural.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ElementEntity.ElementLabel))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the element label. </summary>
    ''' <value> The element label. </value>
    Public Property ElementLabel As String
        Get
            Return Me.Label
        End Get
        Set(value As String)
            Me.Label = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
    ''' </summary>
    ''' <value> The natural amount. </value>
    Public Property Amount As Integer Implements IKeyForeignLabelNatural.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(value As Integer)
            If Me.Amount <> value Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(MultimeterEntity.MultimeterNumber))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the ordinal number. </summary>
    ''' <value> The ordinal number. </value>
    Public Property OrdinalNumber As Integer
        Get
            Return Me.Amount
        End Get
        Set(value As Integer)
            Me.Amount = value
        End Set
    End Property

#End Region

End Class

''' <summary> Collection of element entities. </summary>
''' <remarks> David, 5/18/2020. </remarks>
Public Class ElementEntityCollection
    Inherits EntityKeyedCollection(Of Integer, IKeyForeignLabelNatural, ElementNub, ElementEntity)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(ByVal item As ElementEntity) As Integer
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.AutoId
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As ElementEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of ElementEntity))
        If entities?.Any Then
            For Each entity As ElementEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return connection.Upsert(ElementBuilder.TableName, Me.Items)
    End Function

End Class

''' <summary> Collection of element entities uniquely associated with a product. </summary>
''' <remarks> David, 5/18/2020. </remarks>
Public Class ProductUniqueElementEntityCollection
    Inherits ElementEntityCollection

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of Integer, String)
        Me._PrimaryKeyDictionary = New Dictionary(Of String, Integer)
        Me._OrdinalNumberDictionary = New Dictionary(Of Integer, Integer)
        Me._ElementTypeCountDictionary = New Dictionary(Of Integer, Integer)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    Public Sub New(ByVal productAutoId As Integer)
        Me.New
        Me.ProductAutoId = productAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of Integer, String)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of String, Integer)

    ''' <summary> Dictionary of ordinal numbers. </summary>
    Private ReadOnly _OrdinalNumberDictionary As IDictionary(Of Integer, Integer)

    ''' <summary> Dictionary of element type counts. </summary>
    Private ReadOnly _ElementTypeCountDictionary As IDictionary(Of Integer, Integer)

    ''' <summary> Gets or sets the number of expected readings. </summary>
    ''' <value> The number of expected readings. </value>
    Public ReadOnly Property ExpectedReadingCount As Integer

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As ElementEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.Label, entity.AutoId)
        Me._UniqueIndexDictionary.Add(entity.AutoId, entity.Label)
        Me._OrdinalNumberDictionary.Add(entity.OrdinalNumber, entity.AutoId)
        If Me.PrimaryElement Is Nothing OrElse String.Equals(entity.ElementLabel, "R1") Then
            Me._PrimaryElement = entity
        End If
        If Me._ElementTypeCountDictionary.ContainsKey(entity.ElementTypeId) Then
            Me._ElementTypeCountDictionary(entity.ElementTypeId) += 1
        Else
            Me._ElementTypeCountDictionary.Add(entity.ElementTypeId, 1)
        End If
        If ElementType.Resistor = entity.ElementTypeId OrElse ElementType.CompoundResistor = entity.ElementTypeId OrElse
            ElementType.OpenCircuit = entity.ElementTypeId OrElse ElementType.ShortCircuit = entity.ElementTypeId Then
            Me._ExpectedReadingCount += 1
        End If
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
        Me._ElementTypeCountDictionary.Clear()
        Me._OrdinalNumberDictionary.Clear()
        Me._ExpectedReadingCount = 0
    End Sub

    ''' <summary> Query if this  contains the given elementLabel. </summary>
    ''' <remarks> David, 6/10/2020. </remarks>
    ''' <param name="elementLabel"> The element label. </param>
    ''' <returns> True if the object is in this collection, false if not. </returns>
    Public Overloads Function Contains(ByVal elementLabel As String) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(elementLabel)
    End Function

    ''' <summary> Selects an element from the collection. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="elementLabel"> The element label. </param>
    ''' <returns> An ElementEntity. </returns>
    Public Function Element(ByVal elementLabel As String) As ElementEntity
        Dim id As Integer = Me._PrimaryKeyDictionary(elementLabel)
        Return If(Me.Contains(id), Me(id), New ElementEntity)
    End Function

    ''' <summary> Selects an element from the collection by its ordinal number. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="elementOrdinalNumber"> The element ordinal number. </param>
    ''' <returns> An ElementEntity. </returns>
    Public Function Element(ByVal elementOrdinalNumber As Integer) As ElementEntity
        Dim id As Integer = Me._OrdinalNumberDictionary(elementOrdinalNumber)
        Return If(Me.Contains(id), Me(id), New ElementEntity)
    End Function

    ''' <summary> Element label. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> A String. </returns>
    Public Function ElementLabel(ByVal elementAutoId As Integer) As String
        Return If(Me._UniqueIndexDictionary.ContainsKey(elementAutoId), Me._UniqueIndexDictionary(elementAutoId), String.Empty)
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public ReadOnly Property ProductAutoId As Integer

    ''' <summary> Fetches nominal types. </summary>
    ''' <remarks> David, 6/25/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of elements in this collection. </returns>
    Public Function FetchNomTypes(ByVal connection As System.Data.IDbConnection) As Integer
        For Each element As ElementEntity In Me
            element.FetchNomTypes(connection)
        Next
        Return Me.Count
    End Function

    ''' <summary> Gets or sets the primary element. </summary>
    ''' <value> The primary element. </value>
    Public ReadOnly Property PrimaryElement() As ElementEntity

End Class

