Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Core

''' <summary> A Product-Element builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class ProductElementBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ProductElementBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ProductElementBuilder._TableName) Then
                ProductElementBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ProductElementNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = ProductBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(ProductNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = ElementBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(ElementNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(ProductElementEntity.ProductAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(ProductElementEntity.ElementAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ProductElementBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ProductElementBuilder
        If ProductElementBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ProductElementBuilder._Instance = New ProductElementBuilder()
            End SyncLock
        End If
        Return ProductElementBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Product Element Nub based on the <see cref="IOneToMany">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
<Table("ProductElement")>
Public Class ProductElementNub
    Inherits OneToManyNub
    Implements IOneToMany

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToMany
        Return New ProductElementNub
    End Function

End Class

''' <summary>
''' The Product-Element Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class ProductElementEntity
    Inherits EntityBase(Of IOneToMany, ProductElementNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New ProductElementNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Product-Element interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New ProductElementNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ProductElementBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToMany
        Return New ProductElementNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        ProductElementNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        ProductElementNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Product-Element interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        ProductElementNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal elementAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As ProductElementNub = ProductElementEntity.FetchNubs(connection, productAutoId, elementAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.ProductAutoId, Me.ElementAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.ProductAutoId, Me.ElementAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the Product. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal elementAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, productAutoId, elementAutoId)
    End Function

    ''' <summary>
    ''' Tries to fetch an existing <see cref="Entities.ElementEntity"/> associated with this product;
    ''' otherwise, inserts a new entity. Then tries to fetch an existing or insert a new
    ''' <see cref="ProductElementEntity"/>.
    ''' </summary>
    ''' <remarks>
    ''' Assumes that a <see cref="Entities.ProductEntity"/> exists for the specified
    ''' <paramref name="productAutoId"/>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="productAutoId">        Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementLabel">         The element label. </param>
    ''' <param name="elementTypeId">        Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="elementOrdinalNumber"> The element ordinal number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainElement(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer,
                                     ByVal elementLabel As String, ByVal elementTypeId As Integer,
                                     ByVal elementOrdinalNumber As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._ElementEntity = ProductElementEntity.FetchElement(connection, productAutoId, elementLabel)
        If Me.ElementEntity.IsClean Then
            If Me.ElementEntity.ElementTypeId <> elementTypeId Then
                result = (False, $"Attempt to change {NameOf(Entities.ElementEntity)} {NameOf(Me._ElementEntity.ElementLabel)} of type {NameOf(Me._ElementEntity.ElementTypeId)} to {elementTypeId} was aborted")
            End If
        Else
            Me._ElementEntity = New ElementEntity() With {.ElementLabel = elementLabel, .ElementTypeId = elementTypeId, .OrdinalNumber = elementOrdinalNumber}
            If Not Me.ElementEntity.Insert(connection) Then
                result = (False, $"Failed inserting {NameOf(Entities.ElementEntity)} with {NameOf(Entities.ElementEntity.ElementLabel)} of {elementLabel} and {NameOf(Entities.ElementEntity.ElementTypeId)} of {elementTypeId}")
            End If
        End If
        If result.Success Then
            Me.PrimaryId = productAutoId
            Me.SecondaryId = Me.ElementEntity.AutoId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(ProductElementEntity.ElementEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.ProductElementEntity)} with {NameOf(Entities.ProductElementEntity.ProductAutoId)} of {productAutoId} and {NameOf(Entities.ProductElementEntity.ElementAutoId)} of {Me.ElementEntity.AutoId}")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch an existing <see cref="Entities.ProductEntity"/>, fetch or insert a new
    ''' <see cref="Entities.ElementEntity"/> and fetches an existing or inserts a new
    ''' <see cref="ProductElementEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="productAutoId">        Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementLabel">         The element label. </param>
    ''' <param name="elementTypeId">        Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="elementOrdinalNumber"> The element ordinal number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer,
                             ByVal elementLabel As String, ByVal elementTypeId As Integer,
                              ByVal elementOrdinalNumber As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.ProductEntity Is Nothing OrElse Me.ProductEntity.AutoId <> productAutoId Then
            ' make sure we have a product associated with the product auto ID.
            Me._ProductEntity = New ProductEntity With {.AutoId = productAutoId}
            If Not Me._ProductEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.ProductEntity)} with {NameOf(Entities.ProductEntity.AutoId)} of {productAutoId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainElement(connection, productAutoId, elementLabel, elementTypeId, elementOrdinalNumber)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(ProductElementEntity.ProductEntity))
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch an existing <see cref="Entities.ProductEntity"/>, fetch or insert a new
    ''' <see cref="Entities.ElementEntity"/> and fetches an existing or inserts a new
    ''' <see cref="ProductElementEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="elementLabel">         The element label. </param>
    ''' <param name="elementTypeId">        Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="elementOrdinalNumber"> The element ordinal number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection,
                              ByVal elementLabel As String, ByVal elementTypeId As Integer, ByVal elementOrdinalNumber As Integer) As (Success As Boolean, Details As String)
        Return Me.TryObtain(connection, Me.ProductAutoId, elementLabel, elementTypeId, elementOrdinalNumber)
    End Function

    ''' <summary>
    ''' Fetches an existing <see cref="Entities.ProductEntity"/>, fetch or insert a new
    ''' <see cref="Entities.ElementEntity"/> and fetches an existing or inserts a new
    ''' <see cref="ProductElementEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="productAutoId">        Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementLabel">         The element label. </param>
    ''' <param name="elementTypeId">        Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="elementOrdinalNumber"> The element ordinal number. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Function Obtain(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal elementLabel As String,
                                     ByVal elementTypeId As Integer, ByVal elementOrdinalNumber As Integer) As Boolean
        Dim result As (Success As Boolean, Details As String) = Me.TryObtain(connection, productAutoId, elementLabel, elementTypeId, elementOrdinalNumber)
        If Not result.Success Then Throw New OperationFailedException(result.Details)
        Return result.Success
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ProductElementEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal elementAutoId As Integer) As Boolean
        Return connection.Delete(Of ProductElementNub)(New ProductElementNub With {.PrimaryId = productAutoId, .SecondaryId = elementAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Product-Element entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Product-Element entities. </value>
    Public ReadOnly Property ProductElements As IEnumerable(Of ProductElementEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ProductElementEntity)
        Return If(usingNativeTracking, ProductElementEntity.Populate(connection.GetAll(Of IOneToMany)), ProductElementEntity.Populate(connection.GetAll(Of ProductElementNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ProductElements = ProductElementEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ProductElementEntity.ProductElements))
        Return If(Me.ProductElements?.Any, Me.ProductElements.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ProductElementNub)) As IEnumerable(Of ProductElementEntity)
        Dim l As New List(Of ProductElementEntity)
        If nubs?.Any Then
            For Each nub As ProductElementNub In nubs
                l.Add(New ProductElementEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of ProductElementEntity)
        Dim l As New List(Of ProductElementEntity)
        If interfaces?.Any Then
            Dim nub As New ProductElementNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ProductElementEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns up to Element entities count. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductElementBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductElementNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As IEnumerable(Of ProductElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductElementBuilder.TableName}] WHERE {NameOf(ProductElementNub.PrimaryId)} = @Id", New With {Key .Id = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ProductElementEntity.Populate(connection.Query(Of ProductElementNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Element. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of entities by Element. </returns>
    Public Shared Function CountEntitiesByElement(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductElementBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductElementNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Elements in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Elements in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByElement(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of ProductElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductElementBuilder.TableName}] WHERE {NameOf(ProductElementNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ProductElementEntity.Populate(connection.Query(Of ProductElementNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductElementBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductElementNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        sqlBuilder.Where($"{NameOf(ProductElementNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal elementAutoId As Integer) As IEnumerable(Of ProductElementNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductElementBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductElementNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        sqlBuilder.Where($"{NameOf(ProductElementNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductElementNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Product Element exists. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal elementAutoId As Integer) As Boolean
        Return 1 = ProductElementEntity.CountEntities(connection, productAutoId, elementAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Product entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Product entity. </value>
    Public ReadOnly Property ProductEntity As ProductEntity

    ''' <summary> Fetches Product Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Product Entity. </returns>
    Public Function FetchProductEntity(ByVal connection As System.Data.IDbConnection) As ProductEntity
        Dim entity As New ProductEntity()
        entity.FetchUsingKey(connection, Me.ProductAutoId)
        Me._ProductEntity = entity
        Return entity
    End Function

    ''' <summary>
    ''' Count Products associated with the specified <paramref name="elementAutoId"/>; expected 1.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of Products. </returns>
    Public Shared Function CountProducts(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{ProductElementBuilder.TableName}] WHERE {NameOf(ProductElementNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Products associated with the specified <paramref name="elementAutoId"/>; expected
    ''' a single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Products in this collection.
    ''' </returns>
    Public Shared Function FetchProducts(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductElementBuilder.TableName}] WHERE {NameOf(ProductElementNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ElementEntity)
        For Each nub As ProductElementNub In connection.Query(Of ProductElementNub)(selector.RawSql, selector.Parameters)
            Dim entity As New ProductElementEntity(nub)
            l.Add(entity.FetchElementEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Products associated with the specified <paramref name="elementAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteProducts(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{ProductElementBuilder.TableName}] WHERE {NameOf(ProductElementNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the Element entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Element entity. </value>
    Public ReadOnly Property ElementEntity As ElementEntity

    ''' <summary> Fetches a Element Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Element Entity. </returns>
    Public Function FetchElementEntity(ByVal connection As System.Data.IDbConnection) As ElementEntity
        Dim entity As New ElementEntity()
        entity.FetchUsingKey(connection, Me.ElementAutoId)
        Me._ElementEntity = entity
        Return entity
    End Function

    ''' <summary> Count elements. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> The total number of elements. </returns>
    Public Shared Function CountElements(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{ProductElementBuilder.TableName}] WHERE {NameOf(ProductElementNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Elements in this collection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Elements in this collection.
    ''' </returns>
    Public Shared Function FetchElements(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductElementBuilder.TableName}] WHERE {NameOf(ProductElementNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ElementEntity)
        For Each nub As ProductElementNub In connection.Query(Of ProductElementNub)(selector.RawSql, selector.Parameters)
            Dim entity As New ProductElementEntity(nub)
            l.Add(entity.FetchElementEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Element related to the specified Product. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteElements(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{ProductElementBuilder.TableName}] WHERE {NameOf(ProductElementNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered elements in this collection. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="selectQuery">   The select query. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered elements in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchOrderedElements(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal productAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .Id = productAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return ElementEntity.Populate(connection.Query(Of ElementNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered elements in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered elements in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchOrderedElements(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [ProductElement] on [ProductElement].SecondaryId = [Element].AutoId where [ProductElement].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{ElementBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{ElementBuilder.TableName}] Inner Join [{ProductElementBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{ProductElementBuilder.TableName}].{NameOf(ProductElementNub.SecondaryId)} = [{ElementBuilder.TableName}].{NameOf(ElementNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{ProductElementBuilder.TableName}].{NameOf(ProductElementNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{ElementBuilder.TableName}].{NameOf(ElementNub.Amount)} ASC; ")
        Return ProductElementEntity.FetchOrderedElements(connection, queryBuilder.ToString, productAutoId)
    End Function

    ''' <summary> Fetches an <see cref="Entities.ElementEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="selectQuery">   The select query. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementLabel">  The element label. </param>
    ''' <returns> The element. </returns>
    Public Shared Function FetchElement(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal productAutoId As Integer, ByVal elementLabel As String) As ElementEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.Id = productAutoId, .Label = elementLabel})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As ElementNub = connection.Query(Of ElementNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New ElementEntity, New ElementEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an <see cref="Entities.ElementEntity"/>. </summary>
    ''' <remarks>
    ''' David, 5/19/2020.
    ''' <code>
    ''' SELECT [Element].*
    ''' FROM [Element] Inner Join [ProductElement]
    ''' ON [ProductElement].[SecondaryId] = [Element].[AutoId]
    ''' WHERE ([ProductElement].[PrimaryId] = 8 AND [Element].[Label] = 'R1')
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementLabel">  The element label. </param>
    ''' <returns> The element. </returns>
    Public Shared Function FetchElement(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal elementLabel As String) As ElementEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [ProductElement] on [ProductElement].SecondaryId = [Element].AutoId where [ProductElement].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{ElementBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{ElementBuilder.TableName}] Inner Join [{ProductElementBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{ProductElementBuilder.TableName}].[{NameOf(ProductElementNub.SecondaryId)}] = [{ElementBuilder.TableName}].[{NameOf(ElementNub.AutoId)}]")
        queryBuilder.AppendLine($"WHERE ([{ProductElementBuilder.TableName}].[{NameOf(ProductElementNub.PrimaryId)}] = @Id AND [{ElementBuilder.TableName}].[{NameOf(ElementNub.Label)}] = @Label); ")
        Return ProductElementEntity.FetchElement(connection, queryBuilder.ToString, productAutoId, elementLabel)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductElementEntity.ProductAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Product. </summary>
    ''' <value> Identifies the Product. </value>
    Public Property ProductAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductElementEntity.ElementAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public Property ElementAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

#Region " SHARED "

    ''' <summary> Enumerates select element information in this collection. </summary>
    ''' <remarks> David, 6/15/2020. </remarks>
    ''' <param name="productNumber"> Name of the product. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process select element information in this
    ''' collection.
    ''' </returns>
    Public Shared Function SelectElementInfo(ByVal productNumber As String) As IEnumerable(Of ElementInfo)
        Select Case productNumber
            Case "D1206LF"
                Return New List(Of ElementInfo) From {New ElementInfo("R1", 1, ElementType.Resistor, NomType.Resistance, True, True),
                                                      New ElementInfo("R2", 2, ElementType.Resistor, NomType.Resistance, False, False),
                                                      New ElementInfo("D1", 3, ElementType.CompoundResistor, NomType.Resistance, False, False),
                                                      New ElementInfo("M1", 4, ElementType.Equation, NomType.Equation, False, False)}
            Case Else
                Return New List(Of ElementInfo) From {New ElementInfo("R1", 1, ElementType.Resistor, NomType.Resistance, True, True)}
        End Select
    End Function

#End Region

#Region " ELEMENTS "

    ''' <summary>
    ''' Tries to fetch an existing <see cref="Entities.ElementEntity"/> associated with this product;
    ''' otherwise, inserts a new entity. Then tries to fetch an existing or insert a new
    ''' <see cref="ProductElementEntity"/>.
    ''' </summary>
    ''' <remarks>
    ''' Assumes that a <see cref="Entities.ProductEntity"/> exists for the specified.
    ''' </remarks>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="elementInfo"> Information describing the element. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainElement(ByVal connection As System.Data.IDbConnection, ByVal elementInfo As ElementInfo) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = Me.TryObtain(connection, Me.ProductAutoId, elementInfo.Label,
                                                                             elementInfo.ElementTypeId, elementInfo.OrdinalNumber)
        If Not result.Success Then Return result
        Me.ElementEntity.FetchElementNomTypeEntities(connection)
        Me.ElementEntity.NomTypeEntities.Add(connection, Me.ElementEntity.AutoId, elementInfo.NomTypeId)
        Dim expectedCount As Integer = 1
        If Me.ElementEntity.NomTypeEntities.Any Then
            If Me.ElementEntity.NomTypeEntities.Count = expectedCount Then
                If Not Me.ElementEntity.NomTypeEntities.ContainsNomType(elementInfo.NomTypeId) Then
                    result = (False, $"{NameOf(ElementEntity.NomTypeEntities)} should contain key {elementInfo.NomTypeId}")
                End If
            Else
                result = (False, $"{NameOf(ElementEntity.NomTypeEntities)} count {Me.ElementEntity.NomTypeEntities.Count} should be {expectedCount}")
            End If
        Else
            result = (False, $"{NameOf(ElementEntity.NomTypeEntities)} is empty where it should have items")
        End If
        If Not result.Success Then Return result

        Me.ElementEntity.FetchNomTypes(connection)
        expectedCount = 1
        If Me.ElementEntity.NomTypes.Any Then
            If Me.ElementEntity.NomTypes.Count = expectedCount Then
                If Not Me.ElementEntity.NomTypes.Contains(elementInfo.NomTypeId) Then
                    result = (False, $"{NameOf(ElementEntity.NomTypes)} should contain key {elementInfo.NomTypeId}")
                End If
            Else
                result = (False, $"{NameOf(ElementEntity.NomTypes)} count {Me.ElementEntity.NomTypeEntities.Count} should be {expectedCount}")
            End If
        Else
            result = (False, $"{NameOf(ElementEntity.NomTypes)} is empty where it should have items")
        End If
        Return result
    End Function

    ''' <summary> Adds the elements to 'productName'. </summary>
    ''' <remarks> David, 6/15/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productNumber"> The product number. </param>
    Public Sub AddElements(ByVal connection As System.Data.IDbConnection, ByVal productNumber As String)
        Dim id As Integer = Me.ProductAutoId
        Dim result As (Success As Boolean, Details As String)
        For Each elementinfo As ElementInfo In ProductElementEntity.SelectElementInfo(productNumber)
            result = Me.TryObtainElement(connection, elementinfo)
            If Not result.Success Then Throw New OperationFailedException(result.Details)
            If elementinfo.IsCritical Then Me.CriticalElementLabel = elementinfo.Label
            If elementinfo.IsPrimary Then Me.PrimaryElementLabel = elementinfo.Label
        Next
    End Sub

    ''' <summary> The critical element label. </summary>
    Private _CriticalElementLabel As String

    ''' <summary> Gets or sets the critical element label. </summary>
    ''' <value> The critical element label. </value>
    Public Property CriticalElementLabel As String
        Get
            Return Me._CriticalElementLabel
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.CriticalElementLabel) Then
                Me._CriticalElementLabel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The primary element label. </summary>
    Private _PrimaryElementLabel As String

    ''' <summary> Gets or sets the Primary element label. </summary>
    ''' <value> The Primary element label. </value>
    Public Property PrimaryElementLabel As String
        Get
            Return Me._PrimaryElementLabel
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.PrimaryElementLabel) Then
                Me._PrimaryElementLabel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class

