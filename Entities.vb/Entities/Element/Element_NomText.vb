
Imports System.Collections.ObjectModel

Imports Dapper

Partial Public Class ElementEntity

    ''' <summary>
    ''' Gets or sets the <see cref=" ElementUniqueNomTextEntityCollection">Element Nom Text
    ''' values</see>.
    ''' </summary>
    ''' <value> The Element Text trait values. </value>
    Public ReadOnly Property NomTexts As ElementTextCollection

    ''' <summary> Initializes the nominal texts. </summary>
    ''' <remarks> David, 7/6/2020. </remarks>
    Public Sub InitializeNomTexts()
        Me._NomTexts = New ElementTextCollection
        For Each nomTypeEntity As NomTypeEntity In Me.NomTypes
            Me.NomTexts.Add(New ElementUniqueNomTextEntityCollection(Me, nomTypeEntity.Id))
        Next
    End Sub

    ''' <summary> Fetches <see cref=" ElementTextCollection">Element Text values</see>. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of specifications. </returns>
    Public Function FetchNomTexts(ByVal connection As System.Data.IDbConnection) As Integer
        If Not (Me._NomTexts?.Any).GetValueOrDefault(False) Then Me.InitializeNomTexts()
        Me.NomTexts.Add(ElementNomTextEntity.FetchEntities(connection, Me.AutoId))
        Return If(Me.NomTexts?.Any, Me.NomTexts.Count, 0)
    End Function

End Class

''' <summary> Collection of sample traits. </summary>
''' <remarks> David, 7/3/2020. </remarks>
Public Class ElementTextCollection
    Inherits KeyedCollection(Of Integer, ElementUniqueNomTextEntityCollection)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As ElementUniqueNomTextEntityCollection) As Integer
        Return item.ElementText.Selector
    End Function

    ''' <summary> Adds texts. </summary>
    ''' <remarks> David, 7/6/2020. </remarks>
    ''' <param name="texts"> The texts to add. </param>
    Public Overloads Sub Add(ByVal texts As IEnumerable(Of ElementNomTextEntity))
        For Each text As ElementNomTextEntity In texts
            Me(text.NomTypeId).Add(text)
        Next
    End Sub

    ''' <summary> Updates or inserts entities using the given connection. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    <CLSCompliant(False)>
    Public Sub Upsert(ByVal connection As TransactedConnection)
        For Each item As ElementUniqueNomTextEntityCollection In Me
            item.Upsert(connection)
        Next
    End Sub

    ''' <summary> Updates or inserts entities using the given connection. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Sub Upsert(ByVal connection As System.Data.IDbConnection)
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        Me.Upsert(New TransactedConnection(connection, transaction))
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            Me.Upsert(transactedConnection)
        End If
    End Sub

End Class
