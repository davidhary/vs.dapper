
Imports System.Collections.ObjectModel


Partial Public Class ElementEntity

    ''' <summary>
    ''' Gets or sets the Keyed collection of
    ''' <see cref="PartNomEntityCollection"/> index by
    ''' <see cref="Entities.MultimeterEntity"/> and <see cref="Entities.NomTypeEntity"/> identities.
    ''' </summary>
    ''' <value> The nominals. </value>
    Public ReadOnly Property NomTraitsCollection As PartNomEntitiesCollection

    ''' <summary> Fetches the collection of nominal trait entities for all meters. </summary>
    ''' <remarks> David, 7/31/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="part">         The <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterId"> Identifier for the multimeter. </param>
    ''' <returns> The nominals. </returns>
    Public Function FetchNoms(ByVal connection As System.Data.IDbConnection, ByVal part As PartEntity, ByVal multimeterId As Integer) As PartNomEntitiesCollection
        If Not Entities.NomTypeEntity.IsEnumerated Then Entities.NomTypeEntity.TryFetchAll(connection)
        Dim partNomTraits As PartNomEntityCollection
        For Each nomType As NomTypeEntity In Me.NomTypes
            partNomTraits = New PartNomEntityCollection(part, MultimeterEntity.EntityLookupDictionary(multimeterId), Me, nomType.Id)
            Dim entities As IEnumerable(Of NomTraitEntity) = PartNomEntity.FetchOrderedNomTraits(connection, part.AutoId, multimeterId, Me.AutoId)
            partNomTraits.Populate(entities)
            Me._NomTraitsCollection.Add(partNomTraits)
        Next
        Return Me.NomTraitsCollection
    End Function

    ''' <summary> Fetches the collection of nominal trait entities for all meters. </summary>
    ''' <remarks> David, 6/20/2020. </remarks>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="part">                 The <see cref="Entities.PartEntity"/>. </param>
    ''' <param name="multimeterIdentities"> The multimeter identities. </param>
    ''' <returns> The nominals. </returns>
    Public Function FetchNoms(ByVal connection As System.Data.IDbConnection, ByVal part As PartEntity,
                              ByVal multimeterIdentities As IEnumerable(Of Integer)) As PartNomEntitiesCollection
        If Not Entities.NomTypeEntity.IsEnumerated Then Entities.NomTypeEntity.TryFetchAll(connection)
        Me.NomTraitsCollection.Clear()
        For Each multimeterId As Integer In multimeterIdentities
            Me.FetchNoms(connection, part, multimeterId)
        Next
        Return Me.NomTraitsCollection
    End Function

End Class

''' <summary>
''' Keyed collection of <see cref="PartNomEntityCollection"/> indexed by
''' <see cref="Entities.MultimeterEntity"/> and <see cref="Entities.NomTypeEntity"/> identities.
''' </summary>
''' <remarks> David, 6/16/2020. </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class PartNomEntitiesCollection
    Inherits KeyedCollection(Of MeterNomTypeSelector, PartNomEntityCollection)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    Public Sub New()
        MyBase.New()
        Me.NomTraitBindingList = New isr.Core.Constructs.InvokingBindingList(Of NomTrait)
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 6/22/2020. </remarks>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As PartNomEntityCollection) As MeterNomTypeSelector
        Return New MeterNomTypeSelector(item.MultimeterId, item.NomTypeId)
    End Function

    ''' <summary> Gets or sets a list of meter Nominal Trait bindings. </summary>
    ''' <value> A list of meter Nominal Trait bindings. </value>
    Public ReadOnly Property NomTraitBindingList As isr.Core.Constructs.InvokingBindingList(Of NomTrait)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overloads Sub Add(ByVal item As PartNomEntityCollection)
        MyBase.Add(item)
        Me.NomTraitBindingList.Add(item.NomTrait)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    Public Overloads Sub Clear()
        MyBase.Clear()
        Me.NomTraitBindingList.Clear()
    End Sub

End Class

