
Partial Public Class SessionSuiteEntity

    ''' <summary> Gets or sets the Traits. </summary>
    ''' <value> The Traits. </value>
    Public ReadOnly Property Traits As SessionSuiteUniqueTraitEntityCollection

    ''' <summary> Fetches the <see cref="SessionSuiteUniqueTraitEntityCollection"/> Natural(Integer)-value type trait entity collection. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Sub FetchTraits(ByVal connection As System.Data.IDbConnection)
        If Not SessionTraitTypeEntity.IsEnumerated() Then ProductSortTraitTypeEntity.TryFetchAll(connection)
        Me._Traits = New SessionSuiteUniqueTraitEntityCollection(Me.SessionSuiteAutoId)
        Me._Traits.Populate(SessionSuiteTraitEntity.FetchEntities(connection, Me.SessionSuiteAutoId))
    End Sub

End Class
