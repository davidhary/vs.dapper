Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A builder for a Session Suite based on the <see cref="IKeyForeignLabelTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class SessionSuiteBuilder
    Inherits KeyForeignLabelTimeBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return SessionSuiteBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(SessionSuiteBuilder._TableName) Then
                SessionSuiteBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(SessionSuiteNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the foreign table . </summary>
    ''' <value> The name of the foreign table. </value>
    Public Overrides Property ForeignTableName As String = SessionSuiteTypeBuilder.TableName

    ''' <summary> Gets the name of the foreign table key. </summary>
    ''' <value> The name of the foreign table key. </value>
    Public Overrides Property ForeignTableKeyName As String = NameOf(SessionSuiteTypeNub.Id)

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 50

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(SessionSuiteEntity.AutoId)

    ''' <summary> Gets or sets the name of the foreign identifier field. </summary>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName() As String = NameOf(SessionSuiteEntity.SessionSuiteTypeId)

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <value> The name of the label field. </value>
    Public Overrides Property LabelFieldName() As String = NameOf(SessionSuiteEntity.Label)

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <value> The name of the timestamp field. </value>
    Public Overrides Property TimestampFieldName() As String = NameOf(SessionSuiteEntity.Timestamp)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SessionSuiteBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SessionSuiteBuilder
        If SessionSuiteBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                SessionSuiteBuilder._Instance = New SessionSuiteBuilder()
            End SyncLock
        End If
        Return SessionSuiteBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Nominal SessionSuite table based on the <see cref="IKeyForeignLabelTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("SessionSuite")>
Public Class SessionSuiteNub
    Inherits KeyForeignLabelTimeNub
    Implements IKeyForeignLabelTime

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IKeyForeignLabelTime
        Return New SessionSuiteNub
    End Function

End Class

''' <summary> The Nominal Session Suite Entity based on the <see cref="IKeyForeignLabelTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class SessionSuiteEntity
    Inherits EntityBase(Of IKeyForeignLabelTime, SessionSuiteNub)
    Implements IKeyForeignLabelTime

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New SessionSuiteNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The SessionSuite interface. </param>
    Public Sub New(ByVal value As IKeyForeignLabelTime)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyForeignLabelTime, ByVal store As IKeyForeignLabelTime)
        MyBase.New(New SessionSuiteNub, cache, store)
        Me.UsingNativeTracking = String.Equals(SessionSuiteBuilder.TableName, NameOf(IKeyForeignLabelTime).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IKeyForeignLabelTime
        Return New SessionSuiteNub
    End Function

    Public Overrides Function CreateCopy() As IKeyForeignLabelTime
        Dim destination As IKeyForeignLabelTime = Me.CreateNew
        SessionSuiteNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IKeyForeignLabelTime)
        SessionSuiteNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The SessionSuite interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyForeignLabelTime)
        ' first make the copy to notify of any property change.
        SessionSuiteNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The SessionSuite table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyForeignLabelTime)(key), connection.Get(Of SessionSuiteNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Session Suite Label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As SessionSuiteNub = SessionSuiteEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyForeignLabelTime) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If SessionSuiteEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(UniqueIndexOptions.Label = (SessionSuiteBuilder.Get.QueryUniqueIndexOptions(connection) And UniqueIndexOptions.Label),
                                        Me.FetchUsingUniqueIndex(connection, entity.Label),
                                        Me.FetchUsingKey(connection, entity.AutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of SessionSuiteNub)(New SessionSuiteNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the SessionSuite entities. </summary>
    ''' <value> The SessionSuite entities. </value>
    Public ReadOnly Property SessionSuites As IEnumerable(Of SessionSuiteEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of SessionSuiteEntity)
        Return If(usingNativeTracking, SessionSuiteEntity.Populate(connection.GetAll(Of IKeyForeignLabelTime)), SessionSuiteEntity.Populate(connection.GetAll(Of SessionSuiteNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._SessionSuites = SessionSuiteEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(SessionSuiteEntity.SessionSuites))
        Return If(Me.SessionSuites?.Any, Me.SessionSuites.Count, 0)
    End Function

    ''' <summary> Populates a list of SessionSuite entities. </summary>
    ''' <param name="nubs"> The SessionSuite nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of SessionSuiteNub)) As IEnumerable(Of SessionSuiteEntity)
        Dim l As New List(Of SessionSuiteEntity)
        If nubs?.Any Then
            For Each nub As SessionSuiteNub In nubs
                l.Add(New SessionSuiteEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of SessionSuite entities. </summary>
    ''' <param name="interfaces"> The SessionSuite interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyForeignLabelTime)) As IEnumerable(Of SessionSuiteEntity)
        Dim l As New List(Of SessionSuiteEntity)
        If interfaces?.Any Then
            Dim nub As New SessionSuiteNub
            For Each iFace As IKeyForeignLabelTime In interfaces
                nub.CopyFrom(iFace)
                l.Add(New SessionSuiteEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/16/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="sessionSuiteLabel"> The session suite label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionSuiteLabel As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SessionSuiteBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionSuiteNub.Label)} = @SessionSuiteLabel", New With {sessionSuiteLabel})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/16/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="sessionSuiteLabel"> The session suite label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal sessionSuiteLabel As String) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{SessionSuiteBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(SessionSuiteNub.Label)} IS NULL OR {NameOf(SessionSuiteNub.Label)} = @sessionSuiteLabel)")
        selectSql.Append("; ")
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {sessionSuiteLabel})
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/16/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="sessionSuiteLabel"> The session suite label. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal sessionSuiteLabel As String) As IEnumerable(Of SessionSuiteNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SessionSuiteBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionSuiteNub.Label)} = @SessionSuiteLabel", New With {sessionSuiteLabel})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SessionSuiteNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the sessionSuite exists. </summary>
    ''' <remarks> David, 5/16/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="sessionSuiteLabel"> The session suite label. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal sessionSuiteLabel As String) As Boolean
        Return 1 = SessionSuiteEntity.CountEntities(connection, sessionSuiteLabel)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the SessionSuite. </summary>
    ''' <value> Identifies the SessionSuite. </value>
    Public Property AutoId As Integer Implements IKeyForeignLabelTime.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the session suite. </summary>
    ''' <value> Identifies the session suite. </value>
    Public Property SessionSuiteAutoId As Integer
        Get
            Return Me.AutoId
        End Get
        Set(value As Integer)
            Me.AutoId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the element type. </summary>
    ''' <value> Identifies the element type. </value>
    Public Property ForeignId As Integer Implements IKeyForeignLabelTime.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SessionSuiteEntity.SessionSuiteTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="SessionSuiteTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="SessionSuiteTypeEntity"/>. </value>
    Public Property SessionSuiteTypeId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(value As Integer)
            Me.ForeignId = value
        End Set
    End Property

    ''' <summary> Gets or sets the label. </summary>
    ''' <value> The label. </value>
    Public Property Label As String Implements IKeyForeignLabelTime.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks> Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to convert to local time. </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements IKeyForeignLabelTime.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " FIELDS: TIMESTAMP "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

#End Region

End Class

