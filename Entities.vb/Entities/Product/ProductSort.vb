Imports System.ComponentModel

Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.EnumExtensions
Imports isr.Dapper.Entities.ConnectionExtensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Product Sort builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class ProductSortBuilder
    Inherits KeyForeignLabelNaturalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ProductSortBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ProductSortBuilder._TableName) Then
                ProductSortBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ProductSortNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the foreign table . </summary>
    ''' <value> The name of the foreign table. </value>
    Public Overrides Property ForeignTableName As String = ProductBuilder.TableName

    ''' <summary> Gets the name of the foreign table key. </summary>
    ''' <value> The name of the foreign table key. </value>
    Public Overrides Property ForeignTableKeyName As String = NameOf(ProductNub.AutoId)

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 50

    ''' <summary> Inserts a product sorts. The first sort label must be 'Good' </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="labels">        The labels. </param>
    ''' <returns> An Integer. </returns>
    Public Function InsertSortLables(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal labels As IEnumerable(Of String)) As Integer
        Dim entities As New List(Of ProductSortNub)
        Dim sortOrder As Integer = 0
        For Each label As String In labels
            sortOrder += 1
            entities.Add(New ProductSortNub With {.ForeignId = productAutoId, .Label = label, .Amount = sortOrder})
        Next
        Return connection.InsertIgnore(Me.TableNameThis, entities)
    End Function

    ''' <summary> Inserts a sort labels. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="type">          The type. </param>
    ''' <returns> An Integer. </returns>
    Public Function InsertSortLabels(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal type As Type) As Integer
        Return Me.InsertSortLables(connection, productAutoId, type.Descriptions)
    End Function

    ''' <summary> Inserts a the sorts from the <see cref="ProductSort">Enum type</see>/>. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Function InsertDefaultProductSortRecords(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As Integer
        Return Me.InsertSortLabels(connection, productAutoId, GetType(ProductSort))
    End Function

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(ProductSortEntity.AutoId)

    ''' <summary> Gets or sets the name of the foreign identifier field. </summary>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName() As String = NameOf(ProductSortEntity.ProductAutoId)

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <value> The name of the label field. </value>
    Public Overrides Property LabelFieldName() As String = NameOf(ProductSortEntity.SortLabel)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName() As String = NameOf(ProductSortEntity.SortOrder)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ProductSortBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ProductSortBuilder
        If ProductSortBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ProductSortBuilder._Instance = New ProductSortBuilder()
            End SyncLock
        End If
        Return ProductSortBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the ProductSort table based on the <see cref="IKeyForeignLabelNatural">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("ProductSort")>
Public Class ProductSortNub
    Inherits KeyForeignLabelNaturalNub
    Implements IKeyForeignLabelNatural

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IKeyForeignLabelNatural
        Return New ProductSortNub
    End Function

End Class


''' <summary> The ProductSort Entity. Implements access to the database using Dapper. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class ProductSortEntity
    Inherits EntityBase(Of IKeyForeignLabelNatural, ProductSortNub)
    Implements IKeyForeignLabelNatural

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New ProductSortNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The ProductSort interface. </param>
    Public Sub New(ByVal value As IKeyForeignLabelNatural)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyForeignLabelNatural, ByVal store As IKeyForeignLabelNatural)
        MyBase.New(New ProductSortNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ProductSortBuilder.TableName, NameOf(IKeyForeignLabelNatural).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IKeyForeignLabelNatural
        Return New ProductSortNub
    End Function

    Public Overrides Function CreateCopy() As IKeyForeignLabelNatural
        Dim destination As IKeyForeignLabelNatural = Me.CreateNew
        ProductSortNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IKeyForeignLabelNatural)
        ProductSortNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The ProductSort interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyForeignLabelNatural)
        ' first make the copy to notify of any property change.
        ProductSortNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The ProductSort table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyForeignLabelNatural)(key), connection.Get(Of ProductSortNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the ProductSort. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, autoId)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyForeignLabelNatural) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ProductSortEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.AutoId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of ProductSortNub)(New ProductSortNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the ProductSort entities. </summary>
    ''' <value> The ProductSort entities. </value>
    Public ReadOnly Property ProductSorts As IEnumerable(Of ProductSortEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ProductSortEntity)
        Return If(usingNativeTracking, ProductSortEntity.Populate(connection.GetAll(Of IKeyForeignLabelNatural)), ProductSortEntity.Populate(connection.GetAll(Of ProductSortNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ProductSorts = ProductSortEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ProductSortEntity.ProductSorts))
        Return If(Me.ProductSorts?.Any, Me.ProductSorts.Count, 0)
    End Function

    ''' <summary> Count ProductAttributeValues. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{ProductSortBuilder.TableName}] WHERE {NameOf(ProductSortNub.ForeignId)} = @ForeignId", New With {.ForeignId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As IEnumerable(Of ProductSortEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductSortBuilder.TableName}] WHERE {NameOf(ProductSortNub.ForeignId)} = @ForeignId", New With {.ForeignId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ProductSortEntity.Populate(connection.Query(Of ProductSortNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Fetches Product Traits by Product number and Product Trait Product Trait Range Value type;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As IEnumerable(Of ProductSortNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductSortBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductSortNub.ForeignId)} = @ForeignId", New With {.ForeignId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductSortNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of ProductSort entities. </summary>
    ''' <param name="nubs"> The ProductSort nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal nubs As IEnumerable(Of ProductSortNub)) As IEnumerable(Of ProductSortEntity)
        Dim l As New List(Of ProductSortEntity)
        If nubs?.Any Then
            For Each nub As ProductSortNub In nubs
                l.Add(New ProductSortEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of ProductSort entities. </summary>
    ''' <param name="interfaces"> The ProductSort interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyForeignLabelNatural)) As IEnumerable(Of ProductSortEntity)
        Dim l As New List(Of ProductSortEntity)
        If interfaces?.Any Then
            Dim nub As New ProductSortNub
            For Each iFace As IKeyForeignLabelNatural In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ProductSortEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the ProductSort. </summary>
    ''' <value> Identifies the ProductSort. </value>
    Public Property AutoId As Integer Implements IKeyForeignLabelNatural.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the foreign entity. </summary>
    ''' <value> Identifies the foreign entity. </value>
    Public Property ForeignId As Integer Implements IKeyForeignLabelNatural.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductSortEntity.ProductAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="ProductEntity"/>. </summary>
    ''' <value> Identifies the <see cref="ProductEntity"/>. </value>
    Public Property ProductAutoId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(value As Integer)
            Me.ForeignId = value
        End Set
    End Property

    ''' <summary> Gets or sets the element Label. </summary>
    ''' <value> The Label. </value>
    Public Property Label As String Implements IKeyForeignLabelNatural.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ElementEntity.ElementLabel))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the sort label. </summary>
    ''' <value> The sort label. </value>
    Public Property SortLabel As String
        Get
            Return Me.Label
        End Get
        Set(value As String)
            Me.Label = value
        End Set
    End Property

    ''' <summary> Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value. </summary>
    ''' <value> The natural amount. </value>
    Public Property Amount As Integer Implements IKeyForeignLabelNatural.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(value As Integer)
            If Not String.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductSortEntity.SortOrder))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the sort order. </summary>
    ''' <value> The sort order. </value>
    Public Property SortOrder As Integer
        Get
            Return Me.Amount
        End Get
        Set(value As Integer)
            Me.Amount = value
        End Set
    End Property

#End Region

End Class

''' <summary> Collection of ProductSort entities. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class ProductSortEntityCollection
    Inherits EntityKeyedCollection(Of Integer, IKeyForeignLabelNatural, ProductSortNub, ProductSortEntity)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified product.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The product from which to extract the key. </param>
    ''' <returns> The key for the specified product. </returns>
    Protected Overrides Function GetKeyForItem(ByVal item As ProductSortEntity) As Integer
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.AutoId
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As ProductSortEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Function Populate(ByVal entities As IEnumerable(Of ProductSortEntity)) As Integer
        If entities?.Any Then
            For Each entity As ProductSortEntity In entities
                Me.Add(entity)
            Next
        End If
        Return If(Me.Any, Me.Count, 0)
    End Function

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Throw New NotImplementedException()
    End Function

    ''' <summary> Inserts or updates all entities using the given connection including the traits. </summary>
    ''' <remarks>
    ''' David, 5/13/2020. Entities that failed to save are enumerated in
    ''' <see cref="P:isr.Dapper.Entity.EntityKeyedCollection`4.UnsavedKeys" />
    ''' </remarks>
    ''' <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <CLSCompliant(False)>
    Public Overrides Function Upsert(ByVal transcactedConnection As TransactedConnection) As Boolean
        Me.ClearUnsavedKeys()
        ' Dictionary is instantiated only after the collection has values.
        If Not Me.Any Then Return True
        For Each productSort As ProductSortEntity In Me
            If productSort.Upsert(transcactedConnection) Then
                productSort.Traits.Upsert(transcactedConnection)
            Else
                Me.AddUnsavedKey(productSort.AutoId)
            End If
        Next
        ' success if no unsaved keys
        Return Not Me.UnsavedKeys.Any
    End Function

End Class

''' <summary> Collection of productSort entities uniquely associated with a product; this is also a UUT set of sorts. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class ProductUniqueProductSortEntityCollection
    Inherits ProductSortEntityCollection

#Region " CONSTUCTION "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of Integer, String)
        Me._PrimaryKeyDictionary = New Dictionary(Of String, Integer)
        Me._SortedSortLabelDix = New SortedDictionary(Of Integer, String)
        Me._SortedSortEntityDix = New SortedDictionary(Of Integer, ProductSortEntity)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    Public Sub New(ByVal productAutoId As Integer)
        Me.New
        Me.ProductAutoId = productAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of Integer, String)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of String, Integer)

    ''' <summary> The dictionary of sort labels ordered by the sort order. </summary>
    Private ReadOnly _SortedSortLabelDix As SortedDictionary(Of Integer, String)

    ''' <summary> The dictionary of sort entities ordered by the sort order. </summary>
    Private ReadOnly _SortedSortEntityDix As SortedDictionary(Of Integer, ProductSortEntity)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As ProductSortEntity)
        MyBase.Add(entity)
        Me._SortedSortLabelDix.Add(entity.SortOrder, entity.SortLabel)
        Me._SortedSortEntityDix.Add(entity.SortOrder, entity)
        Me._PrimaryKeyDictionary.Add(entity.Label, entity.AutoId)
        Me._UniqueIndexDictionary.Add(entity.AutoId, entity.Label)
    End Sub

    ''' <summary>
    ''' Removes all productSorts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Populates entities fetched for the <see cref="ProductAutoId"/>. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Overloads Function Populate(ByVal connection As System.Data.IDbConnection) As Integer
        Return Me.Populate(ProductSortEntity.FetchEntities(connection, Me.ProductAutoId))
    End Function

#End Region

#Region " PRODUCT SORT "

    ''' <summary> Selects an productSort from the collection. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="sortLabel"> The sort label. </param>
    ''' <returns> An ProductSortEntity. </returns>
    Public Function ProductSort(ByVal sortLabel As String) As ProductSortEntity
        Dim id As Integer = Me._PrimaryKeyDictionary(sortLabel)
        Return If(Me.Contains(id), Me(id), New ProductSortEntity)
    End Function

    ''' <summary> Product sort label. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="productSortAutoId"> Identifies the <see cref="Entities.ProductEntity"/> sort. </param>
    ''' <returns> A String. </returns>
    Public Function ProductSortLabel(ByVal productSortAutoId As Integer) As String
        Return If(Me._UniqueIndexDictionary.ContainsKey(productSortAutoId), Me._UniqueIndexDictionary(productSortAutoId), String.Empty)
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ProductEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ProductEntity"/>. </value>
    Public ReadOnly Property ProductAutoId As Integer

    ''' <summary> Gets the order sorts. </summary>
    ''' <value> The order sorts. </value>
    Public ReadOnly Property OrderedSorts As IEnumerable(Of ProductSortEntity)
        Get
            Dim result As New List(Of ProductSortEntity)
            For Each sortLabel As String In Me._SortedSortLabelDix.Keys
                result.Add(Me.ProductSort(sortLabel))
            Next
            Return result
        End Get
    End Property

#End Region

#Region " BUCKETS "

    ''' <summary> Parse sort description bucket bin. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <param name="sortLabel"> The sort label. </param>
    ''' <returns> A BucketBin. </returns>
    Public Function SelectBucketBin1(ByVal sortLabel As String) As BucketBin
        Return CType(Me.ProductSort(sortLabel).Traits.ProductSortTrait.BucketBin, BucketBin)
    End Function

#End Region

End Class

''' <summary> Values that represent a product Sorts. </summary>
Public Enum ProductSort
    <Description("Good")> Good = 0
    <Description("R-Guard")> ResistorGuardFailed = 1
    <Description("D-Guard")> CompoundResistorGuardFailed = 2
    <Description("M-Guard")> ComputedValueGuardFailed = 3
    <Description("Overflow")> ReadingOverflow = 4
    <Description("Failed")> ReadingFailed = 5
    <Description("Not Sorted")> Nameless = 30
End Enum
