Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Product Text builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class ProductTextBuilder
    Inherits TwoToManyLabelBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ProductTextBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ProductTextBuilder._TableName) Then
                ProductTextBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ProductTextNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 255

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = ProductBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(ProductNub.AutoId)

    ''' <summary> Gets or sets the name of the Secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Secondary table. </value>
    Public Overrides Property SecondaryTableName As String = ProductTextTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the Secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(ProductTextTypeNub.Id)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(ProductTextEntity.ProductAutoId)

    ''' <summary> Gets or sets the name of the Secondary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(ProductTextEntity.ProductTextTypeId)

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <value> The name of the label field. </value>
    Public Overrides Property LabelFieldName As String = NameOf(ProductTextEntity.Label)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ProductTextBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ProductTextBuilder
        If ProductTextBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ProductTextBuilder._Instance = New ProductTextBuilder()
            End SyncLock
        End If
        Return ProductTextBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="Entities.ProductEntity"/> Text table <see cref="ITwoToManyLabel">interface</see>.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("ProductText")>
Public Class ProductTextNub
    Inherits TwoToManyLabelNub
    Implements ITwoToManyLabel

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As ITwoToManyLabel
        Return New ProductTextNub
    End Function

End Class

''' <summary> The <see cref="ProductTextEntity"/>. Implements access to the database using Dapper. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class ProductTextEntity
    Inherits EntityBase(Of ITwoToManyLabel, ProductTextNub)
    Implements ITwoToManyLabel

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New ProductTextNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> the <see cref="Entities.ProductEntity"/>Text interface. </param>
    Public Sub New(ByVal value As ITwoToManyLabel)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ITwoToManyLabel, ByVal store As ITwoToManyLabel)
        MyBase.New(New ProductTextNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ProductTextBuilder.TableName, NameOf(ITwoToManyLabel).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As ITwoToManyLabel
        Return New ProductTextNub
    End Function

    Public Overrides Function CreateCopy() As ITwoToManyLabel
        Dim destination As ITwoToManyLabel = Me.CreateNew
        ProductTextNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="ProductTextEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As ITwoToManyLabel)
        ProductTextNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached Text, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> the <see cref="Entities.ProductEntity"/>Text interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ITwoToManyLabel)
        ' first make the copy to notify of any property change.
        ProductTextNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="productAutoId">       Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTextTypeId"> Identifies the <see cref="ProductTextTypeEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTextTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTextNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        sqlBuilder.Where($"{NameOf(ProductTextNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productTextTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of ProductTextNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="productAutoId">       Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTextTypeId"> Identifies the <see cref="ProductTextTypeEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTextTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As ProductTextNub = ProductTextEntity.FetchEntities(connection, productAutoId, productTextTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ITwoToManyLabel) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ProductTextEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="productAutoId">       Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTextTypeId"> Identifies the <see cref="ProductTextTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTextTypeId As Integer) As Boolean
        Return connection.Delete(Of ProductTextNub)(New ProductTextNub With {.PrimaryId = productAutoId, .SecondaryId = productTextTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="Entities.ProductEntity"/>Text entities. </summary>
    ''' <value> the <see cref="Entities.ProductEntity"/>Text entities. </value>
    Public ReadOnly Property ProductTexts As IEnumerable(Of ProductTextEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ProductTextEntity)
        Return If(usingNativeTracking, ProductTextEntity.Populate(connection.GetAll(Of ITwoToManyLabel)),
                                       ProductTextEntity.Populate(connection.GetAll(Of ProductTextNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ProductTexts = ProductTextEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ProductTextEntity.ProductTexts))
        Return If(Me.ProductTexts?.Any, Me.ProductTexts.Count, 0)
    End Function

    ''' <summary> Count Product Texts. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{ProductTextBuilder.TableName}] WHERE {NameOf(ProductTextNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As IEnumerable(Of ProductTextEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductTextBuilder.TableName}] WHERE {NameOf(ProductTextNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ProductTextEntity.Populate(connection.Query(Of ProductTextNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Fetches Product Texts by Product Auto Id
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As IEnumerable(Of ProductTextNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTextEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductTextNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of Product Text entities. </summary>
    ''' <param name="nubs"> the <see cref="Entities.ProductEntity"/>Text nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ProductTextNub)) As IEnumerable(Of ProductTextEntity)
        Dim l As New List(Of ProductTextEntity)
        If nubs?.Any Then
            For Each nub As ProductTextNub In nubs
                l.Add(New ProductTextEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Product Text entities. </summary>
    ''' <param name="interfaces"> the <see cref="Entities.ProductEntity"/>Text interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of ITwoToManyLabel)) As IEnumerable(Of ProductTextEntity)
        Dim l As New List(Of ProductTextEntity)
        If interfaces?.Any Then
            Dim nub As New ProductTextNub
            For Each iFace As ITwoToManyLabel In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ProductTextEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Product Texts by unique index; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="productAutoId">       Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTextTypeId"> Identifies the <see cref="ProductTextTypeEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTextTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTextNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        sqlBuilder.Where($"{NameOf(ProductTextNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productTextTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches Product Texts by unique index; expected single or none. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="productAutoId">       Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTextTypeId"> Identifies the <see cref="ProductTextTypeEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTextTypeId As Integer) As IEnumerable(Of ProductTextNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTextNub.PrimaryId)} = @primaryId", New With {.primaryId = productAutoId})
        sqlBuilder.Where($"{NameOf(ProductTextNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productTextTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductTextNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.ProductEntity"/>Text exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="productAutoId">       Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTexttypeId"> Identifies the <see cref="ProductTextTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTexttypeId As Integer) As Boolean
        Return 1 = ProductTextEntity.CountEntities(connection, productAutoId, productTexttypeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count Texts associated with this product. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of Texts. </returns>
    Public Function CountProductTexts(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTextNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Product Text Texts by Product auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 7/5/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchProductTexts(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of ProductTextNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductTextBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTextNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductTextNub)(selector.RawSql, selector.Parameters)
    End Function

#End Region

#Region " RELATIONS: PRODUCT "

    ''' <summary> Gets or sets the Product entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Product entity. </value>
    Public ReadOnly Property ProductEntity As ProductEntity

    ''' <summary> Fetches a Product Entity. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Product Entity. </returns>
    Public Function FetchProductEntity(ByVal connection As System.Data.IDbConnection) As ProductEntity
        Dim entity As New ProductEntity()
        entity.FetchUsingKey(connection, Me.ProductAutoId)
        Me._ProductEntity = entity
        Return entity
    End Function

#End Region

#Region " RELATIONS: PRODUCT TEXT TYPE  "

    ''' <summary> Gets or sets the <see cref="Entities.ProductEntity"/> <see cref="ProductTextTypeEntity"/>. </summary>
    ''' <value> the <see cref="Entities.ProductEntity"/> Text type entity. </value>
    Public ReadOnly Property ProductTextTypeEntity As ProductTextTypeEntity

    ''' <summary> Fetches a product text type Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchProductTextTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._ProductTextTypeEntity = New ProductTextTypeEntity()
        Return Me.ProductTextTypeEntity.FetchUsingKey(connection, Me.ProductTextTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements ITwoToManyLabel.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductTextEntity.ProductAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ProductEntity"/> record. </summary>
    ''' <value> Identifies the <see cref="Entities.ProductEntity"/> record. </value>
    Public Property ProductAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements ITwoToManyLabel.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductTextEntity.ProductTextTypeId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identity (type) of the <see cref="entities.ProductTextEntity"/>.
    ''' </summary>
    ''' <value> Identifies the Product Text type. </value>
    Public Property ProductTextTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property


    ''' <summary> Gets or sets the <see cref="entities.ProductTextEntity"/> label. </summary>
    ''' <value> the <see cref="entities.ProductTextEntity"/> label. </value>
    Public Property Label As String Implements ITwoToManyLabel.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As TwoKeySelector
        Get
            Return New TwoKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of <see cref="ProductTextEntity"/>'s. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class ProductTextEntityCollection
    Inherits EntityKeyedCollection(Of TwoKeySelector, ITwoToManyLabel, ProductTextNub, ProductTextEntity)

    Protected Overrides Function GetKeyForItem(item As ProductTextEntity) As TwoKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As ProductTextEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of ProductTextEntity))
        If entities?.Any Then
            For Each entity As ProductTextEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return ProductTextBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary> Collection of <see cref="Entities.ProductEntity"/>-Unique <see cref="ProductTextEntity"/>'s. </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class ProductUniqueTextEntityCollection
    Inherits ProductTextEntityCollection
    Implements Core.Constructs.IGetterSetter

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New(ByVal product As ProductEntity)
        MyBase.New
        Me.ProductAutoId = product.AutoId
        Me._UniqueIndexDictionary = New Dictionary(Of TwoKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, TwoKeySelector)
        Me.ProductText = New ProductText(Me, product)
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of TwoKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, TwoKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value
    '''                       can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As ProductTextEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.ProductTextTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.ProductTextTypeId)
        Me.NotifyPropertyChanged(ProductTextTypeEntity.EntityLookupDictionary(entity.ProductTextTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains 'productTextType' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productTextTypeId"> Identifies the <see cref="Entities.ProductTextTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal productTextTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(productTextTypeId)
    End Function

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets the Real(String)-value for the given <see cref="ProductTextEntity.Label"/>. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable String. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String Implements Core.Constructs.IGetterSetter.Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary> Set the specified product value for the given <see cref="ProductTextEntity.Label"/>. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A String. </returns>
    Protected Function Setter(ByVal value As String, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String Implements Core.Constructs.IGetterSetter.Setter
        Me.Setter(Me.ToKey(name), value)
        Me.NotifyPropertyChanged(name)
        Return value
    End Function

    ''' <summary> Gets or sets the Product Text. </summary>
    ''' <value> The Product Text. </value>
    Public ReadOnly Property ProductText As ProductText

#End Region

#Region " TRAIT SELECTION "

    ''' <summary> Converts a name to a key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return ProductTextTypeEntity.KeyLookupDictionary(name)
    End Function

    ''' <summary> Gets or sets the text value. </summary>
    ''' <value> The text value. </value>
    Protected Property TextValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String
        Get
            Return Me.Getter(Me.ToKey(name))
        End Get
        Set(value As String)
            Me.Setter(Me.ToKey(name), value)
        End Set
    End Property


    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productTextTypeId"> Identifies the <see cref="Entities.ProductTextTypeEntity"/>. </param>
    ''' <returns> An ProductTextEntity. </returns>
    Public Function Entity(ByVal productTextTypeId As Integer) As ProductTextEntity
        Return If(Me.ContainsKey(productTextTypeId), Me(Me._PrimaryKeyDictionary(productTextTypeId)), New ProductTextEntity)
    End Function

    ''' <summary> Gets the Real(Double)-value for the given Text type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productTextTypeId"> Identifies the <see cref="Entities.ProductTextTypeEntity"/>. </param>
    ''' <returns> A String. </returns>
    Public Function Getter(ByVal productTextTypeId As Integer) As String
        Return If(Me.ContainsKey(productTextTypeId), Me(Me._PrimaryKeyDictionary(productTextTypeId)).Label, String.Empty)
    End Function

    ''' <summary> Set the specified Product value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productTextTypeId"> Identifies the
    '''                                 <see cref="Entities.ProductTextTypeEntity"/>. </param>
    ''' <param name="value">            The value. </param>
    Public Sub Setter(ByVal productTextTypeId As Integer, ByVal value As String)
        If Me.ContainsKey(productTextTypeId) Then
            Me(Me._PrimaryKeyDictionary(productTextTypeId)).Label = value
        Else
            Me.Add(New ProductTextEntity With {.ProductAutoId = Me.ProductAutoId, .ProductTextTypeId = productTextTypeId, .Label = value})
        End If
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ProductEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ProductEntity"/>. </value>
    Public ReadOnly Property ProductAutoId As Integer

#End Region

End Class

