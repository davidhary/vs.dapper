Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Product Real(Double)-Value trait builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class ProductTraitBuilder
    Inherits OneToManyRealBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ProductTraitBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ProductTraitBuilder._TableName) Then
                ProductTraitBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ProductTraitNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = ProductBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(ProductNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = ProductTraitTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(ProductTraitTypeNub.Id)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(ProductTraitEntity.ProductAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(ProductTraitEntity.ProductTraitTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName As String = NameOf(ProductTraitEntity.Amount)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ProductTraitBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ProductTraitBuilder
        If ProductTraitBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ProductTraitBuilder._Instance = New ProductTraitBuilder()
            End SyncLock
        End If
        Return ProductTraitBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="ProductTraitEntity"/> <see cref="IOneToManyReal">interface</see>.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("ProductTrait")>
Public Class ProductTraitNub
    Inherits OneToManyRealNub
    Implements IOneToManyReal

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToManyReal
        Return New ProductTraitNub
    End Function

End Class

''' <summary> The <see cref="ProductTraitEntity"/> stores Real(Double)-Valued Product Traits. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class ProductTraitEntity
    Inherits EntityBase(Of IOneToManyReal, ProductTraitNub)
    Implements IOneToManyReal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New ProductTraitNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> the <see cref="Entities.ProductEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IOneToManyReal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToManyReal, ByVal store As IOneToManyReal)
        MyBase.New(New ProductTraitNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ProductTraitBuilder.TableName, NameOf(IOneToManyReal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToManyReal
        Return New ProductTraitNub
    End Function

    Public Overrides Function CreateCopy() As IOneToManyReal
        Dim destination As IOneToManyReal = Me.CreateNew
        ProductTraitNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="ProductTraitEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToManyReal)
        ProductTraitNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> the <see cref="Entities.ProductEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToManyReal)
        ' first make the copy to notify of any property change.
        ProductTraitNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId">      Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTraitTypeId"> Identifies the <see cref="ProductTraittypeEntity"/>
    '''                                   value type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTraitTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTraitNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        sqlBuilder.Where($"{NameOf(ProductTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of ProductTraitNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/15/2020. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId">      Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTraitTypeId"> Identifies the <see cref="ProductTraitTypeEntity"/>
    '''                                   value type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTraitTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As ProductTraitNub = ProductTraitEntity.FetchEntities(connection, productAutoId, productTraitTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToManyReal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ProductTraitEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId">      Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTraitTypeId"> Type of the <see cref="ProductTraitTypeEntity"/> value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTraitTypeId As Integer) As Boolean
        Return connection.Delete(Of ProductTraitNub)(New ProductTraitNub With {.PrimaryId = productAutoId, .SecondaryId = productTraitTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="Entities.ProductEntity"/> Real(Double)-Value Trait entities. </summary>
    ''' <value> the <see cref="Entities.ProductEntity"/> Real(Double)-Value Trait entities. </value>
    Public ReadOnly Property ProductAttributeReals As IEnumerable(Of ProductTraitEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ProductTraitEntity)
        Return If(usingNativeTracking, ProductTraitEntity.Populate(connection.GetAll(Of IOneToManyReal)),
                                       ProductTraitEntity.Populate(connection.GetAll(Of ProductTraitNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ProductAttributeReals = ProductTraitEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ProductTraitEntity.ProductAttributeReals))
        Return If(Me.ProductAttributeReals?.Any, Me.ProductAttributeReals.Count, 0)
    End Function

    ''' <summary> Count ProductAttributeValues. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{ProductTraitBuilder.TableName}] WHERE {NameOf(ProductTraitNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As IEnumerable(Of ProductTraitEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductTraitBuilder.TableName}] WHERE {NameOf(ProductTraitNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ProductTraitEntity.Populate(connection.Query(Of ProductTraitNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Fetches Product Traits by Product number and Product Trait Product Real(Double)-Value Trait Type;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As IEnumerable(Of ProductTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTraitEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of ProductAttributeValue entities. </summary>
    ''' <param name="nubs"> the <see cref="Entities.ProductEntity"/>Value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ProductTraitNub)) As IEnumerable(Of ProductTraitEntity)
        Dim l As New List(Of ProductTraitEntity)
        If nubs?.Any Then
            For Each nub As ProductTraitNub In nubs
                l.Add(New ProductTraitEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of ProductAttributeValue entities. </summary>
    ''' <param name="interfaces"> the <see cref="Entities.ProductEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToManyReal)) As IEnumerable(Of ProductTraitEntity)
        Dim l As New List(Of ProductTraitEntity)
        If interfaces?.Any Then
            Dim nub As New ProductTraitNub
            For Each iFace As IOneToManyReal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ProductTraitEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Product Traits; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId">      Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTraitTypeId"> Type of the <see cref="ProductTraitTypeEntity"/> value. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTraitTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTraitNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        sqlBuilder.Where($"{NameOf(ProductTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Product Traits by Product number and Product Trait Product Trait Real Value type;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId">      Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTraitTypeId"> Identifies the <see cref="ProductTraitTypeEntity"/>
    '''                                   value type. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTraitTypeId As Integer) As IEnumerable(Of ProductTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTraitNub.PrimaryId)} = @primaryId", New With {.primaryId = productAutoId})
        sqlBuilder.Where($"{NameOf(ProductTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.ProductEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId">      Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="productTraitTypeId"> Identifies the <see cref="ProductTraittypeEntity"/>
    '''                                   value type. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal productTraitTypeId As Integer) As Boolean
        Return 1 = ProductTraitEntity.CountEntities(connection, productAutoId, productTraitTypeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count values associated with this Product Trait. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of specification values. </returns>
    Public Function CountProductAttributeValues(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTraitNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Product Real(Double)-Value trait values by Product auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">          The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchProductAttributeValues(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of ProductTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductTraitNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.ProductEntity"/>. </summary>
    ''' <value> the <see cref="Entities.ProductEntity"/>. </value>
    Public ReadOnly Property ProductEntity As ProductEntity

    ''' <summary> Fetches a <see cref="Entities.ProductEntity"/> . </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchProductEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._ProductEntity = New ProductEntity()
        Return Me.ProductEntity.FetchUsingKey(connection, Me.ProductAutoId)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.ProductTraitTypeEntity"/> . </summary>
    ''' <value> the <see cref="Entities.ProductTraitTypeEntity"/> . </value>
    Public ReadOnly Property ProductTraitTypeEntity As ProductTraitTypeEntity

    ''' <summary> Fetches a <see cref="Entities.ProductTraitTypeEntity"/>. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchProductTraitTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._ProductTraitTypeEntity = New ProductTraitTypeEntity()
        Return Me.ProductTraitTypeEntity.FetchUsingKey(connection, Me.ProductTraitTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToManyReal.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductTraitEntity.ProductAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ProductEntity"/> record. </summary>
    ''' <value> Identifies the <see cref="Entities.ProductEntity"/> record. </value>
    Public Property ProductAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToManyReal.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductTraitEntity.ProductTraitTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="entities.ProductTraitTypeEntity"/>. </summary>
    ''' <value> The type of Identifies the <see cref="entities.ProductTraitTypeEntity"/>. </value>
    Public Property ProductTraitTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets a Real(Double)-value assigned to the <see cref="Entities.ProductEntity"/> for the specific <see cref="Entities.ProductTraitTypeEntity"/>. </summary>
    ''' <value> The Real(Double)-value assigned to the <see cref="Entities.ProductEntity"/> for the specific <see cref="Entities.ProductTraitTypeEntity"/>. </value>
    Public Property Amount As Double Implements IOneToManyReal.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(ByVal value As Double)
            If Not Double.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of <see cref="ProductTraitEntity"/>'s. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class ProductTraitEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToManyReal, ProductTraitNub, ProductTraitEntity)

    Protected Overrides Function GetKeyForItem(item As ProductTraitEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As ProductTraitEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all Products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of ProductTraitEntity))
        If entities?.Any Then
            For Each entity As ProductTraitEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return ProductTraitBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary> Collection of unique <see cref="Entities.ProductEntity"/> <see cref="ProductTraitEntity"/>'s. </summary>
''' <remarks> David, 2020-05-05. </remarks>
''' 
Public Class ProductUniqueTraitEntityCollection
    Inherits ProductTraitEntityCollection
    Implements isr.Core.Constructs.IGetterSetter(Of Double)

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
        Me.ProductTrait = New ProductTrait(Me)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    Public Sub New(ByVal productAutoId As Integer)
        Me.New
        Me.ProductAutoId = productAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As ProductTraitEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.ProductTraitTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.ProductTraitTypeId)
        Me.NotifyPropertyChanged(ProductTraitTypeEntity.EntityLookupDictionary(entity.ProductTraitTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all Products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productTraitTypeId"> Identifies the <see cref="Entities.ProductTraitTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal productTraitTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(productTraitTypeId)
    End Function

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets the Real(Double)-value for the given <see cref="ProductTraitTypeEntity.Label"/>. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable Double. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double? Implements Core.Constructs.IGetterSetter(Of Double).Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary> Set the specified element value for the given <see cref="ProductTraitTypeEntity.Label"/>. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Double. </returns>
    Protected Function Setter(ByVal value As Double, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double Implements Core.Constructs.IGetterSetter(Of Double).Setter
        Me.Setter(Me.ToKey(name), value)
        Me.NotifyPropertyChanged(name)
        Return value
    End Function

    ''' <summary> Gets or sets the product trait. </summary>
    ''' <value> The product trait. </value>
    Public ReadOnly Property ProductTrait As ProductTrait


#End Region

#Region " TRAIT SELECTION "

    ''' <summary>
    ''' Converts a name to a key using the
    ''' <see cref="ProductTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
    ''' extend the Product Traits beyond the values of the enumeration type that is used to
    ''' populate this table.
    ''' </summary>
    ''' <remarks> David, 5/11/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return ProductTraitTypeEntity.KeyLookupDictionary(name)
    End Function

    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.Getter(Me.ToKey(name))
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.Setter(Me.ToKey(name), value.Value)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productTraitTypeId"> Identifies the <see cref="Entities.ProductTraitTypeEntity"/>. </param>
    ''' <returns> An ProductAttributeRealEntity. </returns>
    Public Function Entity(ByVal productTraitTypeId As Integer) As ProductTraitEntity
        Return If(Me.ContainsKey(productTraitTypeId), Me(Me._PrimaryKeyDictionary(productTraitTypeId)), New ProductTraitEntity)
    End Function

    ''' <summary> Gets the Real(Double)-value for the given reading type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productTraitTypeId"> Identifies the <see cref="Entities.ProductTraitTypeEntity"/>. </param>
    Public Function Getter(ByVal productTraitTypeId As Integer) As Double?
        Return If(Me.ContainsKey(productTraitTypeId), Me(Me._PrimaryKeyDictionary(productTraitTypeId)).Amount, New Double?)
    End Function

    ''' <summary> Set the specified Product value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productTraitTypeId"> Identifies the
    '''                                   <see cref="Entities.ProductTraitTypeEntity"/>. </param>
    ''' <param name="value">              The value. </param>
    Public Sub Setter(ByVal productTraitTypeId As Integer, ByVal value As Double)
        If Me.ContainsKey(productTraitTypeId) Then
            Me(Me._PrimaryKeyDictionary(productTraitTypeId)).Amount = value
        Else
            Me.Add(New ProductTraitEntity With {.ProductAutoId = Me.ProductAutoId, .ProductTraitTypeId = productTraitTypeId, .Amount = value})
        End If
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ProductEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ProductEntity"/>. </value>
    Public ReadOnly Property ProductAutoId As Integer

#End Region

End Class

