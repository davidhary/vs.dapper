
Partial Public Class ProductEntity

    ''' <summary>
    ''' Gets or sets the <see cref=" ProductUniqueBinningEntityCollection">Product
    ''' <see cref="ProductBinningEntity"/>'s</see>.
    ''' </summary>
    ''' <value> The Product ProductSort trait values. </value>
    Public ReadOnly Property ProductBinnings As ProductUniqueBinningEntityCollection

    ''' <summary>
    ''' Fetches the <see cref=" ProductUniqueBinningEntityCollection">Product
    ''' <see cref="ProductBinningEntity"/>'s</see>.
    ''' </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of ProductBinning. </returns>
    Public Function FetchProductBinning(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ProductBinnings = ProductEntity.FetchProductBinning(connection, Me.AutoId)
        Return If(Me.ProductBinnings?.Any, Me.ProductBinnings.Count, 0)
    End Function

    ''' <summary>
    ''' Fetches the <see cref=" ProductUniqueBinningEntityCollection">Product
    ''' <see cref="ProductBinningEntity"/>'s</see>.
    ''' </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> The ProductBinning. </returns>
    Public Shared Function FetchProductBinning(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As ProductUniqueBinningEntityCollection
        Dim productBinning As New ProductUniqueBinningEntityCollection(productAutoId)
        productBinning.Populate(ProductBinningEntity.FetchEntities(connection, productAutoId))
        Return productBinning
    End Function

End Class

