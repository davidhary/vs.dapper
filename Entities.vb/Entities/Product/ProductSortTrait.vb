''' <summary> The Product Sort Trait class holing the Product Sort trait values. </summary>
''' <remarks> David, 2020-05-29. </remarks>
Public Class ProductSortTrait
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSestter"> The getter setter. </param>
    Public Sub New(ByVal getterSestter As isr.Core.Constructs.IGetterSetter(Of Integer))
        MyBase.New()
        Me.GetterSetter = getterSestter
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter(Of Integer)

    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As Integer?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.GetterSetter.Setter(value.Value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

#End Region

#Region " TRAITS "

    ''' <summary> Gets or sets the continuous failure count limit. </summary>
    ''' <value> The continuous failure count limit. </value>
    Public Property ContinuousFailureCountLimit As Integer?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the digital output. </summary>
    ''' <value> The digital output. </value>
    Public Property DigitalOutput As Integer?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the bucket number. </summary>
    ''' <value> The bucket number. </value>
    Public Property BucketNumber As Integer?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the bucket Bin. </summary>
    ''' <value> The bucket Bin. </value>
    Public Property BucketBin As Integer?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the background color of the bucket. </summary>
    ''' <value> The color of the bucket. </value>
    Public Property BucketBackColor As Integer?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

#End Region

End Class
