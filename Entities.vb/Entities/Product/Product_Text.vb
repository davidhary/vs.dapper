
Partial Public Class ProductEntity

    ''' <summary> Gets or sets the <see cref=" ProductUniqueTextEntityCollection">Product Nom Text values</see>. </summary>
    ''' <value> The Product Text trait values. </value>
    Public ReadOnly Property Texts As ProductUniqueTextEntityCollection

    ''' <summary> Initializes the nominal texts. </summary>
    ''' <remarks> David, 7/6/2020. </remarks>
    Public Sub InitializeTexts()
        Me._Texts = New ProductUniqueTextEntityCollection(Me)
    End Sub

    ''' <summary>
    ''' Fetches <see cref="ProductEntity.Texts"/> <see cref="ProductUniqueTextEntityCollection"/>.
    ''' </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of specifications. </returns>
    Public Function FetchTexts(ByVal connection As System.Data.IDbConnection) As Integer
        If Not ProductTextTypeEntity.IsEnumerated Then ProductTextTypeEntity.TryFetchAll(connection)
        If Not (Me._Texts?.Any).GetValueOrDefault(False) Then Me.InitializeTexts()
        Me.Texts.Populate(ProductTextEntity.FetchEntities(connection, Me.AutoId))
        Return If(Me.Texts?.Any, Me.Texts.Count, 0)
    End Function

    ''' <summary> Select scan list. </summary>
    ''' <remarks> David, 7/6/2020. </remarks>
    ''' <param name="productNumber"> Name of the product. </param>
    ''' <returns> A String. </returns>
    Public Shared Function SelectScanList(ByVal productNumber As String) As String
        Select Case productNumber
            Case "D1206LF"
                Return "(@1:3)"
            Case Else
                Return String.Empty
        End Select
    End Function

End Class

