Imports isr.Core.EnumExtensions
Partial Public Class ProductEntity

    ''' <summary> Gets or sets the <see cref=" ProductUniqueProductSortEntityCollection">Product ProductSort values</see>. </summary>
    ''' <value> The Product ProductSort trait values. </value>
    Public ReadOnly Property ProductSorts As ProductUniqueProductSortEntityCollection

    ''' <summary>
    ''' Fetches <see cref=" ProductUniqueProductSortEntityCollection">Product ProductSort values</see>.
    ''' </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of ProductSorts. </returns>
    Public Function FetchProductSorts(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ProductSorts = ProductEntity.FetchProductSorts(connection, Me.AutoId)
        Return If(Me.ProductSorts?.Any, Me.ProductSorts.Count, 0)
    End Function

    ''' <summary> Fetches <see cref="ProductUniqueProductSortEntityCollection">Product ProductSort values</see>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> The ProductSorts. </returns>
    Public Shared Function FetchProductSorts(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As ProductUniqueProductSortEntityCollection
        If Not ProductSortTraitTypeEntity.IsEnumerated Then ProductSortTraitTypeEntity.TryFetchAll(connection)
        If Not BucketBinEntity.IsEnumerated Then BucketBinEntity.TryFetchAll(connection)
        Dim productSorts As New ProductUniqueProductSortEntityCollection(productAutoId)
        productSorts.Populate(ProductSortEntity.FetchEntities(connection, productAutoId))
        For Each productSort As ProductSortEntity In productSorts
            productSort.FetchTraitEntities(connection)
        Next
        Return productSorts
    End Function

    ''' <summary> Assigns product sort buckets and bucket bins. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <returns> An Integer. </returns>
    Public Overridable Function AssignProductSortBuckets() As Integer
        Dim productSortEntity As ProductSortEntity
        Dim count As Integer = 0
        For Each productSort As ProductSort In [Enum].GetValues(GetType(ProductSort))
            productSortEntity = Me.ProductSorts.ProductSort(productSort.Description)
            Dim sort As ProductSortTrait = productSortEntity.Traits.ProductSortTrait
            sort.BucketNumber = BucketBinEntity.ToBucketNumber(productSort)
            count += 1
            sort.BucketBin = BucketBinEntity.ProductSortBinMapper(productSort)
            count += 1
            sort.BucketBackColor = BucketBinEntity.BackColorMapper(BucketBinEntity.ProductSortBinMapper(productSort))
            count += 1
            sort.DigitalOutput = BucketBinEntity.DigitalOutputMapper(BucketBinEntity.ProductSortBinMapper(productSort))
            count += 1
            sort.ContinuousFailureCountLimit = BucketBinEntity.ContinuousFailureCountLimitMapper(BucketBinEntity.ProductSortBinMapper(productSort))
            count += 1
        Next
        Return count
    End Function

    ''' <summary> Assign product sort traits. </summary>
    ''' <remarks> David, 7/7/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="productSort">                 The product sort. </param>
    ''' <param name="bucketNumber">                The bucket number. </param>
    ''' <param name="bucketBin">                   The bucket bin. </param>
    ''' <param name="bucketBackColor">             The bucket back color. </param>
    ''' <param name="digitalOutput">               The digital output. </param>
    ''' <param name="continuousFailureCountLimit"> The continuous failure count limit. </param>
    ''' <returns> A ProductSortEntity. </returns>
    '''
    Public Function AssignProductSortTraits(ByVal productSort As Dapper.Entities.ProductSort,
                                            ByVal bucketNumber As Integer, ByVal bucketBin As Integer, ByVal bucketBackColor As Integer,
                                            ByVal digitalOutput As Integer, ByVal continuousFailureCountLimit As Integer) As ProductSortEntity
        Dim sortLabel As String = productSort.Description
        Dim productSortEntity As ProductSortEntity = Me.ProductSorts.ProductSort(sortLabel)
        Dim r As (Success As Boolean, Details As String)
        r = productSortEntity.ValidateStoredEntity($"{NameOf(Dapper.Entities.ProductSortEntity)} with {NameOf(Dapper.Entities.ProductSortEntity.Label)} of {sortLabel}")
        If Not r.Success Then Throw New isr.Core.OperationFailedException(r.Details)
        Dim sort As ProductSortTrait = productSortEntity.Traits.ProductSortTrait
        sort.BucketNumber = bucketNumber
        sort.BucketBin = bucketBin
        sort.BucketBackColor = bucketBackColor
        sort.DigitalOutput = digitalOutput
        sort.ContinuousFailureCountLimit = continuousFailureCountLimit
        Return productSortEntity
    End Function


End Class

