Partial Public Class ProductEntity

    ''' <summary> Gets or sets the <see cref="ProductUniqueElementEntityCollection">product elements</see>. </summary>
    ''' <value> The Product elements entities. </value>
    Public ReadOnly Property Elements As ProductUniqueElementEntityCollection

    ''' <summary>
    ''' Fetches the <see cref="ProductUniqueElementEntityCollection">product elements</see>. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of Elements. </returns>
    Public Function FetchElements(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Elements = ProductEntity.FetchElements(connection, Me.AutoId)
        Return (Me.Elements?.Count).GetValueOrDefault(0)
    End Function

    ''' <summary> Fetches the <see cref="ProductUniqueElementEntityCollection">product elements</see>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> The Elements. </returns>
    Public Shared Function FetchElements(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As ProductUniqueElementEntityCollection
        If Not ElementTypeEntity.IsEnumerated Then ElementTypeEntity.TryFetchAll(connection)
        Dim elements As New ProductUniqueElementEntityCollection(productAutoId)
        elements.Populate(ProductElementEntity.FetchOrderedElements(connection, productAutoId))
        elements.FetchNomTypes(connection)
        Return elements
    End Function

End Class

