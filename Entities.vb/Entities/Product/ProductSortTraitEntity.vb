Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Product Sort Trait Natural(Integer)-value builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class ProductSortTraitBuilder
    Inherits OneToManyNaturalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ProductSortTraitBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ProductSortTraitBuilder._TableName) Then
                ProductSortTraitBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ProductSortTraitNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = ProductSortBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(ProductSortNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = ProductSortTraitTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(ProductSortTraitTypeNub.Id)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(ProductSortTraitEntity.ProductSortAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(ProductSortTraitEntity.ProductSortTraitTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName As String = NameOf(ProductSortTraitEntity.Amount)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ProductSortTraitBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ProductSortTraitBuilder
        If ProductSortTraitBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ProductSortTraitBuilder._Instance = New ProductSortTraitBuilder()
            End SyncLock
        End If
        Return ProductSortTraitBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="ProductSortTraitEntity"/> <see cref="IOneToManyNatural">interface</see>.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("ProductSortTrait")>
Public Class ProductSortTraitNub
    Inherits OneToManyNaturalNub
    Implements IOneToManyNatural

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToManyNatural
        Return New ProductSortTraitNub
    End Function

End Class

''' <summary> The <see cref="ProductSortTraitEntity"/> stores Natural(Integer)-valued product Sort Traits. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class ProductSortTraitEntity
    Inherits EntityBase(Of IOneToManyNatural, ProductSortTraitNub)
    Implements IOneToManyNatural

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New ProductSortTraitNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> the <see cref="ProductSortEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IOneToManyNatural)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToManyNatural, ByVal store As IOneToManyNatural)
        MyBase.New(New ProductSortTraitNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ProductSortTraitBuilder.TableName, NameOf(IOneToManyNatural).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToManyNatural
        Return New ProductSortTraitNub
    End Function

    Public Overrides Function CreateCopy() As IOneToManyNatural
        Dim destination As IOneToManyNatural = Me.CreateNew
        ProductSortTraitNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="ProductSortTraitEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToManyNatural)
        ProductSortTraitNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> the <see cref="ProductSortEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToManyNatural)
        ' first make the copy to notify of any property change.
        ProductSortTraitNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">             The connection. </param>
    ''' <param name="productSortAutoId">      Identifies the <see cref="ProductSortEntity"/>
    '''                                       record. </param>
    ''' <param name="productSortTraitTypeId"> Identifies the
    '''                                       <see cref="Entities.ProductSortTraitTypeEntity"/> value type. 
    ''' </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer, ByVal productSortTraitTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductSortTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductSortTraitNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productSortAutoId})
        sqlBuilder.Where($"{NameOf(ProductSortTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productSortTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of ProductSortTraitNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/15/2020. </remarks>
    ''' <param name="connection">             The connection. </param>
    ''' <param name="productSortAutoId">      Identifies the <see cref="ProductSortEntity"/>
    '''                                       record. </param>
    ''' <param name="productSortTraitTypeId"> Identifies the
    '''                                       <see cref="Entities.ProductSortTraitTypeEntity"/> value type. 
    ''' </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer, ByVal productSortTraitTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As ProductSortTraitNub = ProductSortTraitEntity.FetchEntities(connection, productSortAutoId, productSortTraitTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToManyNatural) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ProductSortTraitEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">             The connection. </param>
    ''' <param name="productSortAutoId">      Identifies the <see cref="ProductSortEntity"/>
    '''                                       record. </param>
    ''' <param name="productSortTraitTypeId"> Type of the <see cref="Entities.ProductSortTraitTypeEntity"/>
    '''                                       value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer, ByVal productSortTraitTypeId As Integer) As Boolean
        Return connection.Delete(Of ProductSortTraitNub)(New ProductSortTraitNub With {.PrimaryId = productSortAutoId, .SecondaryId = productSortTraitTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="ProductSortEntity"/> Attribute Natural(Integer)-Value entities. </summary>
    ''' <value> the <see cref="ProductSortEntity"/> Attribute Natural(Integer)-Value entities. </value>
    Public ReadOnly Property ProductSortNaturals As IEnumerable(Of ProductSortTraitEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ProductSortTraitEntity)
        Return If(usingNativeTracking, ProductSortTraitEntity.Populate(connection.GetAll(Of IOneToManyNatural)),
                                       ProductSortTraitEntity.Populate(connection.GetAll(Of ProductSortTraitNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ProductSortNaturals = ProductSortTraitEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ProductSortTraitEntity.ProductSortNaturals))
        Return If(Me.ProductSortNaturals?.Any, Me.ProductSortNaturals.Count, 0)
    End Function

    ''' <summary> Count ProductSortValues. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productSortAutoId"> Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{ProductSortTraitBuilder.TableName}] WHERE {NameOf(ProductSortTraitNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productSortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productSortAutoId"> Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer) As IEnumerable(Of ProductSortTraitEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductSortTraitBuilder.TableName}] WHERE {NameOf(ProductSortTraitNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productSortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ProductSortTraitEntity.Populate(connection.Query(Of ProductSortTraitNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Fetches Product Sort Traits by product sort auto id.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productSortAutoId"> Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer) As IEnumerable(Of ProductSortTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductSortTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductSortTraitEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productSortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductSortTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of ProductSortValue entities. </summary>
    ''' <param name="nubs"> the <see cref="ProductSortEntity"/>Value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ProductSortTraitNub)) As IEnumerable(Of ProductSortTraitEntity)
        Dim l As New List(Of ProductSortTraitEntity)
        If nubs?.Any Then
            For Each nub As ProductSortTraitNub In nubs
                l.Add(New ProductSortTraitEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of ProductSortValue entities. </summary>
    ''' <param name="interfaces"> the <see cref="ProductSortEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToManyNatural)) As IEnumerable(Of ProductSortTraitEntity)
        Dim l As New List(Of ProductSortTraitEntity)
        If interfaces?.Any Then
            Dim nub As New ProductSortTraitNub
            For Each iFace As IOneToManyNatural In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ProductSortTraitEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Product Sort Traits by unique index; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="productSortAutoId"> Identifies the <see cref="ProductSortEntity"/> record. </param>
    ''' <param name="productSortTraitTypeId">   Type of the <see cref="ProductSortEntity"/> value. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer, ByVal productSortTraitTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductSortTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductSortTraitNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productSortAutoId})
        sqlBuilder.Where($"{NameOf(ProductSortTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productSortTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches Product Sort Traits by unique index; expected single or none. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">             The connection. </param>
    ''' <param name="productSortAutoId">      Identifies the <see cref="ProductSortEntity"/>
    '''                                       record. </param>
    ''' <param name="productSortTraitTypeId"> Identifies the <see cref="ProductSortEntity"/>
    '''                                       value type. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer, ByVal productSortTraitTypeId As Integer) As IEnumerable(Of ProductSortTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductSortTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductSortTraitNub.PrimaryId)} = @primaryId", New With {.primaryId = productSortAutoId})
        sqlBuilder.Where($"{NameOf(ProductSortTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productSortTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductSortTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="ProductSortEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">             The connection. </param>
    ''' <param name="productSortAutoId">      Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <param name="productSortTraitTypeId"> The Session Suite Natural(Integer)-value type. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer, ByVal productSortTraitTypeId As Integer) As Boolean
        Return 1 = ProductSortTraitEntity.CountEntities(connection, productSortAutoId, productSortTraitTypeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count values associated with this session suite. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of specification values. </returns>
    Public Function CountProductSortValues(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductSortTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductSortTraitNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Session Suite Natural(Integer)-value Values by Part auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">          The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchProductSortValues(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of ProductSortTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductSortTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductSortTraitNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductSortTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets or sets the <see cref="ProductSortEntity"/> Entity. </summary>
    ''' <value> the <see cref="ProductSortEntity"/> Entity. </value>
    Public ReadOnly Property ProductSortEntity As ProductSortEntity

    ''' <summary> Fetches a ProductSort Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchProductSortEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._ProductSortEntity = New ProductSortEntity()
        Return Me.ProductSortEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

    ''' <summary> Gets or sets the <see cref="ProductSortEntity"/> <see cref="Entities.ProductSortTraitTypeEntity"/>. </summary>
    ''' <value> the <see cref="ProductSortEntity"/> value type entity. </value>
    Public ReadOnly Property ProductSortTraitTypeEntity As ProductSortTraitEntity

    ''' <summary> Fetches a <see cref="Entities.ProductSortTraitTypeEntity"/>. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchProductSortTraitTypeEntity(ByVal connection As System.Data.IDbConnection, ByVal productSortAttributeId As Integer) As Boolean
        Me._ProductSortTraitTypeEntity = New ProductSortTraitEntity()
        Return Me.ProductSortTraitTypeEntity.FetchUsingKey(connection, Me.ProductSortAutoId, productSortAttributeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToManyNatural.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductSortTraitEntity.ProductSortAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="ProductSortEntity"/> record. </summary>
    ''' <value> Identifies the <see cref="ProductSortEntity"/> record. </value>
    Public Property ProductSortAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property


    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToManyNatural.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductSortTraitEntity.ProductSortTraitTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ProductSortTraitTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ProductSortTraitTypeEntity"/>. </value>
    Public Property ProductSortTraitTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets a Natural (whole number) value assigned to the <see cref="ProductSortEntity"/> for the specific <see cref="ProductSortTraitTypeEntity"/>. </summary>
    ''' <value> The Natural(Integer)-value assigned to the <see cref="ProductSortEntity"/> for the specific <see cref="ProductSortTraitTypeEntity"/>. </value>
    Public Property Amount As Integer Implements IOneToManyNatural.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(ByVal value As Integer)
            If Not Double.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class


''' <summary> Collection of <see cref="ProductSortTraitEntity"/>'s. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class ProductSortTraitEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToManyNatural, ProductSortTraitNub, ProductSortTraitEntity)

    Protected Overrides Function GetKeyForItem(item As ProductSortTraitEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As ProductSortTraitEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all productSorts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of ProductSortTraitEntity))
        If entities?.Any Then
            For Each entity As ProductSortTraitEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return ProductSortTraitBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary> Collection of <see cref="ProductSortEntity"/> unique <see cref="ProductSortTraitEntity"/>'s. </summary>
''' <remarks> David, 2020-05-05. </remarks>
''' 
Public Class ProductUniqueSortTraitEntityCollection
    Inherits ProductSortTraitEntityCollection
    Implements isr.Core.Constructs.IGetterSetter(Of Integer)

#Region " CONSTRUCTION "


    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
        Me._ProductSortTrait = New ProductSortTrait(Me)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    Public Sub New(ByVal productSortAutoId As Integer)
        Me.New
        Me.ProductSortAutoId = productSortAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value
    '''                       can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As ProductSortTraitEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.ProductSortTraitTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.ProductSortTraitTypeId)
        Me.NotifyPropertyChanged(ProductSortTraitTypeEntity.EntityLookupDictionary(entity.ProductSortTraitTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all productSorts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets the Real(Integer)-value for the given <see cref="ProductSortTraitTypeEntity.Label"/>. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable Integer. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer? Implements Core.Constructs.IGetterSetter(Of Integer).Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary> Set the specified element value for the given <see cref="ProductSortTraitTypeEntity.Label"/>. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Integer. </returns>
    Protected Function Setter(ByVal value As Integer, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer Implements Core.Constructs.IGetterSetter(Of Integer).Setter
        Me.Setter(Me.ToKey(name), value)
        Me.NotifyPropertyChanged(name)
        Return value
    End Function

    ''' <summary> Gets or sets the product Sort Trait. </summary>
    ''' <value> The product Sort Trait. </value>
    Public ReadOnly Property ProductSortTrait As ProductSortTrait

#End Region

#Region " TRAIT SELECTION "

    ''' <summary> Queries if collection contains a key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productSortTraitTypeId"> Identifies the <see cref="ProductSortTraitTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal productSortTraitTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(productSortTraitTypeId)
    End Function

    ''' <summary>
    ''' Converts a name to a key using the
    ''' <see cref="ProductSortTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
    ''' extend the element Nominal Traits beyond the values of the enumeration type that is used to
    ''' populate this table.
    ''' </summary>
    ''' <remarks> David, 5/24/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return ProductSortTraitTypeEntity.KeyLookupDictionary(name)
    End Function

    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The Trait value. Returns <see cref="Double.NaN"/> if value does not exist</value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        Get
            Return Me.Getter(Me.ToKey(name))
        End Get
        Set(value As Integer?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.Setter(Me.ToKey(name), value.Value)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productSortTraitTypeId"> Identified for the
    '''                                <see cref="ProductSortTraitTypeEntity"/>. </param>
    ''' <returns> An ProductSortAttributeNaturalEntity. </returns>
    Public Function Entity(ByVal productSortTraitTypeId As Integer) As ProductSortTraitEntity
        Return If(Me.ContainsKey(productSortTraitTypeId), Me(Me._PrimaryKeyDictionary(productSortTraitTypeId)), New ProductSortTraitEntity)
    End Function

    ''' <summary> Gets the Real(Double)-value for the given reading type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productSortTraitTypeId"> Identified for the
    '''                                <see cref="ProductSortTraitTypeEntity"/>. </param>
    ''' <returns> An Integer? </returns>
    Public Function Getter(ByVal productSortTraitTypeId As Integer) As Integer?
        Return If(Me.ContainsKey(productSortTraitTypeId), Me(Me._PrimaryKeyDictionary(productSortTraitTypeId)).Amount, New Integer?)
    End Function

    ''' <summary> Set the specified ProductSort value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="productSortTraitTypeId"> Identified for the
    '''                                       <see cref="ProductSortTraitTypeEntity"/>. </param>
    ''' <param name="value">                  The value. </param>
    Public Sub Setter(ByVal productSortTraitTypeId As Integer, ByVal value As Integer)
        If Me.ContainsKey(productSortTraitTypeId) Then
            Me(Me._PrimaryKeyDictionary(productSortTraitTypeId)).Amount = value
        Else
            Me.Add(New ProductSortTraitEntity With {.ProductSortAutoId = Me.ProductSortAutoId, .ProductSortTraitTypeId = productSortTraitTypeId, .Amount = value})
        End If
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="ProductSortEntity"/>. </summary>
    ''' <value> Identifies the <see cref="ProductSortEntity"/>. </value>
    Public ReadOnly Property ProductSortAutoId As Integer

#End Region

End Class

