''' <summary> The Product Text class holing the Product Text values. </summary>
''' <remarks> David, 2020-05-29. </remarks>
Public Class ProductText
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:ProductText" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSetter"> The getter setter. </param>
    ''' <param name="product">      The <see cref="Entities.ProductEntity"/>. </param>
    Public Sub New(ByVal getterSetter As isr.Core.Constructs.IGetterSetter, ByVal product As ProductEntity)
        MyBase.New()
        Me.GetterSetter = getterSetter
        Me.ProductAutoId = product.AutoId
        Me.ProductNumber = product.ProductNumber
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter

    ''' <summary> Gets or sets the text value. </summary>
    ''' <value> The text value. </value>
    Protected Property TextValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.TextValue(name)) Then
                Me.GetterSetter.Setter(value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

#End Region

#Region " IDENTIFIERS "

    Private _ProductAutoId As Integer

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ProductEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property ProductAutoId As Integer
        Get
            Return Me._ProductAutoId
        End Get
        Set
            If Value <> Me.ProductAutoId Then
                Me._ProductAutoId = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " Product PROPERTIES "

    Private _ProductNumber As String

    ''' <summary> Gets or sets the product number. </summary>
    ''' <value> The product number. </value>
    Public Property ProductNumber As String
        Get
            Return Me._ProductNumber
        End Get
        Set
            If Not String.Equals(Value, Me.ProductNumber) Then
                Me._ProductNumber = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " SCANNNING "

    Public Property ScanList As String
        Get
            Return Me.TextValue
        End Get
        Set(value As String)
            Me.TextValue = value
        End Set
    End Property

#End Region

End Class
