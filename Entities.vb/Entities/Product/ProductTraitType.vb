Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A Product Trait Type builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class ProductTraitTypeBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ProductTraitTypeBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ProductTraitTypeBuilder._TableName) Then
                ProductTraitTypeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ProductTraitTypeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Inserts or ignore the records as described by the <see cref="ProductTraitType"/> enumeration type. </summary>
    ''' <param name="connection"> The connection. </param>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(ProductTraitType), New Integer() {CInt(ProductTraitType.None)})
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ProductTraitTypeBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ProductTraitTypeBuilder
        If ProductTraitTypeBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ProductTraitTypeBuilder._Instance = New ProductTraitTypeBuilder()
            End SyncLock
        End If
        Return ProductTraitTypeBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the <see cref="Entities.ProductTraitTypeEntity"/>table based on the <see cref="INominal">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("ProductTraitType")>
Public Class ProductTraitTypeNub
    Inherits NominalNub
    Implements INominal

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As INominal
        Return New ProductTraitTypeNub
    End Function

End Class


''' <summary> The <see cref="Entities.ProductTraitTypeEntity"/> table based on the <see cref="INominal">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class ProductTraitTypeEntity
    Inherits EntityBase(Of INominal, ProductTraitTypeNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New ProductTraitTypeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The ProductTraitType interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New ProductTraitTypeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ProductTraitTypeBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As INominal
        Return New ProductTraitTypeNub
    End Function

    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        ProductTraitTypeNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As INominal)
        ProductTraitTypeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The ProductTraitType interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        ProductTraitTypeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The ProductTraitTypeId table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of ProductTraitTypeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Product Trait Type label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As ProductTraitTypeNub = ProductTraitTypeEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ProductTraitTypeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of ProductTraitTypeNub)(New ProductTraitTypeNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the ProductTraitType entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The ProductTraitType entities. </value>
    Public Shared ReadOnly Property ProductTraitTypeEntities As IEnumerable(Of ProductTraitTypeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ProductTraitTypeEntity)
        Return If(usingNativeTracking, ProductTraitTypeEntity.Populate(connection.GetAll(Of INominal)), ProductTraitTypeEntity.Populate(connection.GetAll(Of ProductTraitTypeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        ProductTraitTypeEntity._ProductTraitTypeEntities = ProductTraitTypeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(ProductTraitTypeEntity.ProductTraitTypeEntities?.Any, ProductTraitTypeEntity.ProductTraitTypeEntities.Count, 0)
    End Function

    ''' <summary> Populates a list of ProductTraitType entities. </summary>
    ''' <param name="nubs"> The ProductTraitType nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ProductTraitTypeNub)) As IEnumerable(Of ProductTraitTypeEntity)
        Dim l As New List(Of ProductTraitTypeEntity)
        If nubs?.Any Then
            For Each nub As ProductTraitTypeNub In nubs
                l.Add(New ProductTraitTypeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of ProductTraitType entities. </summary>
    ''' <param name="interfaces"> The ProductTraitType interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of ProductTraitTypeEntity)
        Dim l As New List(Of ProductTraitTypeEntity)
        If interfaces?.Any Then
            Dim nub As New ProductTraitTypeNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ProductTraitTypeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, ProductTraitTypeEntity)

    ''' <summary> The Product Trait Type lookup dictionary. </summary>
    ''' <returns> A Dictionary(Of Integer, ProductTraitTypeEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, ProductTraitTypeEntity)
        If Not (ProductTraitTypeEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            ProductTraitTypeEntity._EntityLookupDictionary = New Dictionary(Of Integer, ProductTraitTypeEntity)
            For Each entity As ProductTraitTypeEntity In ProductTraitTypeEntity.ProductTraitTypeEntities
                ProductTraitTypeEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return ProductTraitTypeEntity._EntityLookupDictionary
    End Function

    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (ProductTraitTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            ProductTraitTypeEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As ProductTraitTypeEntity In ProductTraitTypeEntity.ProductTraitTypeEntities
                ProductTraitTypeEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return ProductTraitTypeEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (ProductTraitTypeEntity.ProductTraitTypeEntities?.Any AndAlso
                ProductTraitTypeEntity._EntityLookupDictionary?.Any AndAlso
                ProductTraitTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Product Trait Type label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{ProductTraitTypeNub.TableName}]
        ' WHERE {NameOf(ProductTraitTypeNub.Label)} = @label", New With {Key .Label = label})
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{ProductTraitTypeBuilder.TableName}] WHERE {NameOf(ProductTraitTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Product Trait Type label. </param>
    ''' <returns> An IProductTraitType . </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of ProductTraitTypeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductTraitTypeBuilder.TableName}] WHERE {NameOf(ProductTraitTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductTraitTypeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the ProductTraitType exists. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = ProductTraitTypeEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Product Trait Type. </summary>
    ''' <value> Identifies the Product Trait Type. </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Product Trait Type label. </summary>
    ''' <value> The Product Trait Type label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the Product Trait Type. </summary>
    ''' <value> The Product Trait Type description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="productTraitTypeId"> The entity unique key. </param>
    ''' <param name="e">           Action event information. </param>
    ''' <returns> A <see cref="Entities.ProductTraitTypeEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal productTraitTypeId As Integer, ByVal e As isr.Core.ActionEventArgs) As ProductTraitTypeEntity
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Dim result As New ProductTraitTypeEntity
        Try
            activity = $"Fetching {NameOf(ProductTraitTypeEntity)} by {NameOf(ProductTraitTypeNub.Id)} of {productTraitTypeId}"
            If Not result.FetchUsingKey(connection, productTraitTypeId) Then
                e.RegisterFailure($"Failed {activity}")
            End If
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return result
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <param name="connection">            The connection. </param>
    ''' <param name="productTraitTypeLabel"> The Product Trait Type label. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entity As ProductTraitTypeEntity)
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal productTraitTypeLabel As String) As (Success As Boolean, Details As String, Entity As ProductTraitTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New ProductTraitTypeEntity
        Try
            activity = $"Fetching {NameOf(ProductTraitTypeEntity)} by {NameOf(ProductTraitTypeNub.Label)} of {productTraitTypeLabel}"
            Return If(entity.FetchUsingUniqueIndex(connection, productTraitTypeLabel),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of ProductTraitTypeEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of ProductTraitTypeEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New ProductTraitTypeEntity()
            activity = $"fetching all {NameOf(ProductTraitType)}s"
            Dim ProductCount As Integer = entity.FetchAllEntities(connection)
            If ProductCount <> ProductTraitTypeEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(ProductTraitTypeEntity.EntityLookupDictionary)} count must equal {NameOf(ProductTraitTypeEntity.ProductTraitTypeEntities)} count ")
            ElseIf ProductCount <> ProductTraitTypeEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(ProductTraitTypeEntity.KeyLookupDictionary)} count must equal {NameOf(ProductTraitTypeEntity.ProductTraitTypeEntities)} count ")
            End If
            Return (True, String.Empty, ProductTraitTypeEntity.ProductTraitTypeEntities)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of ProductTraitTypeEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Values that represent Product Trait Types. </summary>
Public Enum ProductTraitType
    <ComponentModel.Description("None")> None = 0
    <ComponentModel.Description("Minimum Cp")> MinimumCp = 1
    <ComponentModel.Description("Minimum Cpk")> MinimumCpk = 2
    <ComponentModel.Description("Minimum Cpm")> MinimumCpm = 3
    <ComponentModel.Description("Sigma Level")> SigmaLevel = 4
End Enum
