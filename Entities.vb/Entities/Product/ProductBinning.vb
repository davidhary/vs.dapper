Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Core.EnumExtensions
''' <summary> A Product Binning builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class ProductBinningBuilder
    Inherits FourToManyIdBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ProductBinningBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ProductBinningBuilder._TableName) Then
                ProductBinningBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ProductBinningNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = ProductBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(ProductNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = ElementBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(ElementNub.AutoId)

    ''' <summary> Gets or sets the name of the Ternary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Ternary table. </value>
    Public Overrides Property TernaryTableName As String = NomTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the Ternary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Ternary table key. </value>
    Public Overrides Property TernaryTableKeyName As String = NameOf(NomTypeNub.Id)

    ''' <summary> Gets or sets the name of the Quaternary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Quaternary table. </value>
    Public Overrides Property QuaternaryTableName As String = ReadingBinBuilder.TableName

    ''' <summary> Gets or sets the name of the Quaternary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Quaternary table key. </value>
    Public Overrides Property QuaternaryTableKeyName As String = NameOf(ReadingBinNub.Id)

    ''' <summary> Gets or sets the name of the Id table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Id table. </value>
    Public Overrides Property ForeignIdTableName As String = ProductSortBuilder.TableName

    ''' <summary> Gets or sets the name of the Id table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Id table key. </value>
    Public Overrides Property ForeignIdTableKeyName As String = NameOf(ProductSortNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(ProductBinningEntity.ProductAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(ProductBinningEntity.ElementAutoId)

    ''' <summary> Gets or sets the name of the ternary identifier field. </summary>
    ''' <value> The name of the ternary identifier field. </value>
    Public Overrides Property TernaryIdFieldName As String = NameOf(ProductBinningEntity.NomTypeId)

    ''' <summary> Gets or sets the name of the quaternary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the quaternary identifier field. </value>
    Public Overrides Property QuaternaryIdFieldName As String = NameOf(ProductBinningEntity.ReadingBinId)

    ''' <summary> Gets or sets the name of the foreign identifier field. </summary>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName As String = NameOf(ProductBinningEntity.ProductSortAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ProductBinningBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ProductBinningBuilder
        If ProductBinningBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ProductBinningBuilder._Instance = New ProductBinningBuilder()
            End SyncLock
        End If
        Return ProductBinningBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="ProductBinningEntity"/> <see cref="IFourToManyId">interface</see>.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("ProductBinning")>
Public Class ProductBinningNub
    Inherits FourToManyIdNub
    Implements IFourToManyId

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IFourToManyId
        Return New ProductBinningNub
    End Function

End Class

''' <summary> The <see cref="ProductBinningEntity"/> storing product binning information. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class ProductBinningEntity
    Inherits EntityBase(Of IFourToManyId, ProductBinningNub)
    Implements IFourToManyId

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New ProductBinningNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> the <see cref="Entities.ProductEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IFourToManyId)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IFourToManyId, ByVal store As IFourToManyId)
        MyBase.New(New ProductBinningNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ProductBinningBuilder.TableName, NameOf(IFourToManyId).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IFourToManyId
        Return New ProductBinningNub
    End Function

    Public Overrides Function CreateCopy() As IFourToManyId
        Dim destination As IFourToManyId = Me.CreateNew
        ProductBinningNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="ProductBinningEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IFourToManyId)
        ProductBinningNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> the <see cref="Entities.ProductEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IFourToManyId)
        ' first make the copy to notify of any property change.
        ProductBinningNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="productId">    Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementId">    Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection,
                                            ByVal productId As Integer, ByVal elementId As Integer,
                                            ByVal nomTypeId As Integer, ByVal readingBinId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductBinningBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductBinningNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productId})
        sqlBuilder.Where($"{NameOf(ProductBinningNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementId})
        sqlBuilder.Where($"{NameOf(ProductBinningNub.TernaryId)} = @TernaryId", New With {.TernaryId = nomTypeId})
        sqlBuilder.Where($"{NameOf(ProductBinningNub.QuaternaryId)} = @QuaternaryId", New With {.QuaternaryId = readingBinId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of ProductBinningNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId, Me.TernaryId, Me.QuaternaryId)
    End Function


    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId, Me.TernaryId, Me.QuaternaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/15/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId">  Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal productAutoId As Integer, ByVal elementId As Integer,
                                                    ByVal nomTypeId As Integer, ByVal readingBinId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As ProductBinningNub = ProductBinningEntity.FetchNubs(connection, productAutoId, elementId, nomTypeId, readingBinId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IFourToManyId) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ProductBinningEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId, entity.TernaryId, entity.QuaternaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId">  Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <param name="id">            The identifier of Quaternary reference. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer,
                                            ByVal elementId As Integer, ByVal nomTypeId As Integer, ByVal readingBinId As Integer, ByVal id As Integer) As Boolean
        Return connection.Delete(Of ProductBinningNub)(New ProductBinningNub With {.PrimaryId = productAutoId, .SecondaryId = elementId,
                                                       .TernaryId = nomTypeId, .QuaternaryId = readingBinId, .ForeignId = id})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="Entities.ProductEntity"/> Binning entities. </summary>
    ''' <value> the <see cref="Entities.ProductEntity"/> Binning entities. </value>
    Public ReadOnly Property ProductBinnings As IEnumerable(Of ProductBinningEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ProductBinningEntity)
        Return If(usingNativeTracking, ProductBinningEntity.Populate(connection.GetAll(Of IFourToManyId)),
                                       ProductBinningEntity.Populate(connection.GetAll(Of ProductBinningNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ProductBinnings = ProductBinningEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ProductBinningEntity.ProductBinnings))
        Return If(Me.ProductBinnings?.Any, Me.ProductBinnings.Count, 0)
    End Function

    ''' <summary> Count ProductAttributeValues. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productId">  Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{ProductBinningBuilder.TableName}] WHERE {NameOf(ProductBinningNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productId">  Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal productId As Integer) As IEnumerable(Of ProductBinningEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductBinningBuilder.TableName}] WHERE {NameOf(ProductBinningNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ProductBinningEntity.Populate(connection.Query(Of ProductBinningNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Fetches Product Traits by Product number and Product Trait Product Trait Real
    ''' Value type;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productId">  Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal productId As Integer) As IEnumerable(Of ProductBinningNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductBinningBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductBinningEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductBinningNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="productId">    Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementId">    Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal productId As Integer,
                                         ByVal elementId As Integer, ByVal nomTypeId As Integer, ByVal readingBinId As Integer) As IEnumerable(Of ProductBinningEntity)
        Return ProductBinningEntity.Populate(ProductBinningEntity.FetchNubs(connection, productId, elementId, nomTypeId, readingBinId))
    End Function

    ''' <summary>
    ''' Fetches Product Traits by Product number and Product Trait Product Trait Real Value type;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="productId">    Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementId">    Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal productId As Integer,
                                         ByVal elementId As Integer, ByVal nomTypeId As Integer, ByVal readingBinId As Integer) As IEnumerable(Of ProductBinningNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{ProductBinningBuilder.TableName}]
WHERE (
        {NameOf(ProductBinningNub.PrimaryId)} = @PrimaryId
    AND {NameOf(ProductBinningNub.SecondaryId)} = @SecondaryId
    AND {NameOf(ProductBinningNub.TernaryId)} = @TernaryId
    AND {NameOf(ProductBinningNub.QuaternaryId)} = @QuaternaryId
      ) ", New With {.PrimaryId = productId, .SecondaryId = elementId, .TernaryId = nomTypeId, .QuaternaryId = readingBinId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductBinningNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of ProductAttributeValue entities. </summary>
    ''' <param name="nubs"> the <see cref="Entities.ProductEntity"/>Value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ProductBinningNub)) As IEnumerable(Of ProductBinningEntity)
        Dim l As New List(Of ProductBinningEntity)
        If nubs?.Any Then
            For Each nub As ProductBinningNub In nubs
                l.Add(New ProductBinningEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of ProductAttributeValue entities. </summary>
    ''' <param name="interfaces"> the <see cref="Entities.ProductEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IFourToManyId)) As IEnumerable(Of ProductBinningEntity)
        Dim l As New List(Of ProductBinningEntity)
        If interfaces?.Any Then
            Dim nub As New ProductBinningNub
            For Each iFace As IFourToManyId In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ProductBinningEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Product Traits; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="productId">    Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementId">    Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productId As Integer,
                                         ByVal elementId As Integer, ByVal nomTypeId As Integer, ByVal readingBinId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductBinningBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductBinningNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productId})
        sqlBuilder.Where($"{NameOf(ProductBinningNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementId})
        sqlBuilder.Where($"{NameOf(ProductBinningNub.TernaryId)} = @TernaryId", New With {.TernaryId = nomTypeId})
        sqlBuilder.Where($"{NameOf(ProductBinningNub.QuaternaryId)} = @QuaternaryId", New With {.QuaternaryId = readingBinId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Product Traits by Product number and Product Trait Product Trait Real Value type;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="productId">    Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementId">    Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntitiesUsingBuilder(ByVal connection As System.Data.IDbConnection,
                                         ByVal productId As Integer, ByVal elementId As Integer,
                                                     ByVal nomTypeId As Integer, ByVal readingBinId As Integer) As IEnumerable(Of ProductBinningNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductBinningBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductBinningNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productId})
        sqlBuilder.Where($"{NameOf(ProductBinningNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementId})
        sqlBuilder.Where($"{NameOf(ProductBinningNub.TernaryId)} = @TernaryId", New With {.TernaryId = nomTypeId})
        sqlBuilder.Where($"{NameOf(ProductBinningNub.QuaternaryId)} = @QuaternaryId", New With {.QuaternaryId = readingBinId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductBinningNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.ProductEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="productId">    Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="elementId">    Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal productId As Integer,
                                    ByVal elementId As Integer, ByVal nomTypeId As Integer, ByVal readingBinId As Integer) As Boolean
        Return 1 = ProductBinningEntity.CountEntities(connection, productId, elementId, nomTypeId, readingBinId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count values associated with this Product Trait. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of specification values. </returns>
    Public Function CountProductAttributeValues(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductBinningBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductBinningNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Product BinningValues by Product auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">          The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchProductAttributeValues(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of ProductBinningNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductBinningBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductBinningNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = Me.ProductAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductBinningNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.ProductEntity"/>. </summary>
    ''' <value> the <see cref="Entities.ProductEntity"/>. </value>
    Public ReadOnly Property ProductEntity As ProductEntity

    ''' <summary> Fetches a <see cref="Entities.ProductEntity"/> . </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchProductEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._ProductEntity = New ProductEntity()
        Return Me.ProductEntity.FetchUsingKey(connection, Me.ProductAutoId)
    End Function

    ''' <summary> Gets or sets the <see cref="ProductSortEntity"/> . </summary>
    ''' <value> the <see cref="ProductSortEntity"/> . </value>
    Public ReadOnly Property ProductSortEntity As ProductSortEntity

    ''' <summary> Fetches a <see cref="ProductSortEntity"/>. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchProductSortEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._ProductSortEntity = New ProductSortEntity()
        Return Me.ProductSortEntity.FetchUsingKey(connection, Me.ProductSortAutoId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IFourToManyId.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductBinningEntity.ProductAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ProductEntity"/> record. </summary>
    ''' <value> Identifies the <see cref="Entities.ProductEntity"/> record. </value>
    Public Property ProductAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IFourToManyId.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductBinningEntity.ElementAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public Property ElementAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Ternary reference. </summary>
    ''' <value> The identifier of Ternary reference. </value>
    Public Property TernaryId As Integer Implements IFourToManyId.TernaryId
        Get
            Return Me.ICache.TernaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.TernaryId, value) Then
                Me.ICache.TernaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductBinningEntity.NomTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NomTypeEntity"/>. </value>
    Public Property NomTypeId As Integer
        Get
            Return Me.TernaryId
        End Get
        Set(ByVal value As Integer)
            Me.TernaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Quaternary reference. </summary>
    ''' <value> The identifier of Quaternary reference. </value>
    Public Property QuaternaryId As Integer Implements IFourToManyId.QuaternaryId
        Get
            Return Me.ICache.QuaternaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.QuaternaryId, value) Then
                Me.ICache.QuaternaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductBinningEntity.ReadingBinId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the reading bin. </summary>
    ''' <value> Identifies the reading bin. </value>
    Public Property ReadingBinId As Integer
        Get
            Return Me.QuaternaryId
        End Get
        Set(ByVal value As Integer)
            Me.QuaternaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the Three-to-Many referenced table identifier. </summary>
    ''' <value> The identifier of Three-to-Many referenced table. </value>
    Public Property ForeignId As Integer Implements IFourToManyId.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductBinningEntity.ProductSortAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="ProductSortEntity"/>. </summary>
    ''' <value> Identifies the <see cref="ProductSortEntity"/>. </value>
    Public Property ProductSortAutoId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(ByVal value As Integer)
            Me.ForeignId = value
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The entity selector. </value>
    Public ReadOnly Property EntitySelector As FourKeySelector
        Get
            Return New FourKeySelector(Me)
        End Get
    End Property

    ''' <summary> Gets the unique selector. </summary>
    ''' <value> The unique selector. </value>
    Public ReadOnly Property UniqueSelector As ProductBinningUniqueKey
        Get
            Return New ProductBinningUniqueKey(Me)
        End Get
    End Property

#End Region

End Class

''' <summary>
''' Collection of Product Binning Entities uniquely identified by the
''' <see cref="ProductBinningEntity.EntitySelector"/> <see cref="FourKeySelector"/>.
''' </summary>
''' <remarks> David, 5/23/2020. </remarks>
Public Class ProductBinningEntityCollection
    Inherits EntityKeyedCollection(Of FourKeySelector, IFourToManyId, ProductBinningNub, ProductBinningEntity)

    Protected Overrides Function GetKeyForItem(item As ProductBinningEntity) As FourKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As ProductBinningEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary> Updates or adds the given entity. </summary>
    ''' <remarks> David, 6/15/2020. </remarks>
    ''' <param name="entity"> The entity. </param>
    Public Overridable Sub Upadd(ByVal entity As ProductBinningEntity)
        If Me.Contains(entity.EntitySelector) Then
            Me(entity.EntitySelector).ProductSortAutoId = entity.ProductSortAutoId
        Else
            Me.Add(entity)
        End If
    End Sub

    ''' <summary>
    ''' Removes all products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <param name="entities"> The entities. </param>
    Public Function Populate(ByVal entities As IEnumerable(Of ProductBinningEntity)) As Integer
        If entities?.Any Then
            For Each entity As ProductBinningEntity In entities
                Me.Add(entity)
            Next
        End If
        Return If(Me.Any, Me.Count, 0)
    End Function

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return ProductBinningBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary> Collection of Product-Unique Meter nominal Real(Double)-Value Trait entities. </summary>
''' <remarks> David, 5/23/2020. </remarks>
Public Class ProductUniqueBinningEntityCollection
    Inherits ProductBinningEntityCollection

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._ElementReadingBinDictionary = New Dictionary(Of ProductBinningUniqueKey, Integer)
        Me._UniqueIndexDictionary = New Dictionary(Of FourKeySelector, ProductBinningUniqueKey)
        Me._PrimaryKeyDictionary = New Dictionary(Of ProductBinningUniqueKey, FourKeySelector)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    Public Sub New(ByVal productAutoId As Integer)
        Me.New
        Me.ProductAutoId = productAutoId
    End Sub

    Private ReadOnly _ElementReadingBinDictionary As IDictionary(Of ProductBinningUniqueKey, Integer)
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of FourKeySelector, ProductBinningUniqueKey)
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of ProductBinningUniqueKey, FourKeySelector)

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ProductEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ProductEntity"/>. </value>
    Public ReadOnly Property ProductAutoId As Integer

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value
    '''                       can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As ProductBinningEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.UniqueSelector, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.UniqueSelector)
        Me._ElementReadingBinDictionary.Add(entity.UniqueSelector, entity.ProductSortAutoId)
    End Sub

    ''' <summary>
    ''' Removes all products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._ElementReadingBinDictionary.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Fetch and populates entities for the <see cref="ProductAutoId"/>. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Overloads Function Populate(ByVal connection As System.Data.IDbConnection) As Integer
        Return Me.Populate(ProductBinningEntity.FetchEntities(connection, Me.ProductAutoId))
    End Function

    ''' <summary> Query if the collection contains 'productSortId' key. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifier for the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId">  Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal elementAutoId As Integer, ByVal nomTypeId As Integer, ByVal readingBinId As Integer) As Boolean
        Return Me.ContainsKey(New ProductBinningUniqueKey(elementAutoId, nomTypeId, readingBinId))
    End Function

    ''' <summary> Query if the collection contains 'productSortId' key. </summary>
    ''' <remarks> David, 6/15/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal entity As ProductBinningEntity) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(entity.UniqueSelector)
    End Function

    ''' <summary> Query if the collection contains 'productSortId' key. </summary>
    ''' <remarks> David, 6/15/2020. </remarks>
    ''' <param name="uniqueKey"> The unique key. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal uniqueKey As ProductBinningUniqueKey) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(uniqueKey)
    End Function

    ''' <summary> Selects the entity associated with the given element and bin identities. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifier for the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId">  Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns> A ProductBinningEntity. </returns>
    Public Function Entity(ByVal elementAutoId As Integer, ByVal nomTypeId As Integer, ByVal readingBinId As Integer) As ProductBinningEntity
        Return Me.Entity(New ProductBinningUniqueKey(elementAutoId, nomTypeId, readingBinId))
    End Function

    Public Function Entity(ByVal selector As ProductBinningUniqueKey) As ProductBinningEntity
        Return If(Me._ElementReadingBinDictionary.ContainsKey(selector), Me(Me._ElementReadingBinDictionary(selector)), New ProductBinningEntity)
    End Function

    ''' <summary> Gets the identity of the sort for the given element and bin identities. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifier for the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId">  Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns> An Integer? </returns>
    Public Function Getter(ByVal elementAutoId As Integer, ByVal nomTypeId As Integer, ByVal readingBinId As Integer) As Integer?
        Return Me.Getter(New ProductBinningUniqueKey(elementAutoId, nomTypeId, readingBinId))
    End Function

    ''' <summary> Gets the identity of the sort for the given element and bin identities. </summary>
    ''' <remarks> David, 6/15/2020. </remarks>
    ''' <param name="selector"> The selector. </param>
    ''' <returns> An Integer? </returns>
    Public Function Getter(ByVal selector As ProductBinningUniqueKey) As Integer?
        Return If(Me._ElementReadingBinDictionary.ContainsKey(selector), Me._ElementReadingBinDictionary(selector), New Integer?)
    End Function

    ''' <summary> Set the specified product sort value. </summary>
    ''' <remarks> David, 6/14/2020. </remarks>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId">  Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <param name="productSortId"> Identifies the <see cref="Entities.ProductEntity"/> sort. </param>
    Public Sub Setter(ByVal elementAutoId As Integer, ByVal nomTypeId As Integer, ByVal readingBinId As Integer, ByVal productSortId As Integer)
        Dim selector As New ProductBinningUniqueKey(elementAutoId, nomTypeId, readingBinId)
        If Me._ElementReadingBinDictionary.ContainsKey(selector) Then
            Me._ElementReadingBinDictionary(selector) = productSortId
        Else
            Me.Add(New ProductBinningEntity With {.ProductAutoId = Me.ProductAutoId, .ElementAutoId = elementAutoId, .NomTypeId = nomTypeId, .ReadingBinId = .ReadingBinId, .ForeignId = productSortId})
        End If
    End Sub

#End Region

#Region " BUILD "

    ''' <summary> Select guarded product sort. </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="elementType"> The <see cref="Entities.ElementTypeNub"/>. </param>
    ''' <param name="nomType">     The <see cref="NomTypeEntity"/>. </param>
    ''' <returns> A ProductSort. </returns>
    Public Overridable Function SelectGuardedProductSort(ByVal elementType As ElementType, ByVal nomType As NomType) As ProductSort
        Dim productSort As ProductSort
        Select Case nomType
            Case NomType.Resistance
                Select Case elementType
                    Case ElementType.Resistor
                        productSort = ProductSort.ResistorGuardFailed
                    Case ElementType.CompoundResistor
                        productSort = ProductSort.CompoundResistorGuardFailed
                    Case Else
                        Throw New ArgumentException($"unhandled {NameOf(elementType)} = {elementType} for {NameOf(nomType)}={nomType}")
                End Select
            Case NomType.Equation
                Select Case elementType
                    Case ElementType.Equation
                        productSort = ProductSort.ComputedValueGuardFailed
                    Case Else
                        Throw New ArgumentException($"unhandled {NameOf(elementType)} = {elementType} for {NameOf(nomType)}={nomType}")
                End Select
        End Select

        Return productSort
    End Function

    ''' <summary> Select product sort. </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <param name="elementType"> The <see cref="Entities.ElementTypeNub"/>. </param>
    ''' <param name="nomType">     The <see cref="NomTypeEntity"/>. </param>
    ''' <param name="readingBin">  The <see cref="Entities.ReadingBin"/>. </param>
    ''' <returns> A ProductSort. </returns>
    Public Overridable Function SelectProductSort(ByVal elementType As ElementType, ByVal nomType As NomType, ByVal readingBin As ReadingBin) As ProductSort
        Dim productSort As ProductSort = ProductSort.Good
        Select Case readingBin
            Case ReadingBin.Good
                productSort = ProductSort.Good
            Case ReadingBin.High
                productSort = Me.SelectGuardedProductSort(elementType, nomType)
            Case ReadingBin.Invalid
                productSort = ProductSort.ReadingFailed
            Case ReadingBin.Low
                productSort = Me.SelectGuardedProductSort(elementType, nomType)
            Case ReadingBin.Nameless
                ' not sorted
                productSort = ProductSort.Nameless
            Case ReadingBin.Overflow
                productSort = ProductSort.ReadingOverflow
        End Select
        Return productSort
    End Function

    ''' <summary> Fetch and populates entities for the <see cref="ProductAutoId"/>. </summary>
    ''' <remarks> David, 6/15/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elements">      The elements. </param>
    ''' <param name="sorts">         The sorts. </param>
    Public Overloads Sub Populate(ByVal connection As System.Data.IDbConnection, ByVal elements As ProductUniqueElementEntityCollection,
                                  ByVal sorts As ProductUniqueProductSortEntityCollection)
        Dim element As ElementEntity
        Dim readingBin As ReadingBin
        Dim productSort As ProductSort
        Dim productSortId As Integer
        For Each element In elements
            For Each nomTypeEntity As NomTypeEntity In element.NomTypes
                For Each readingBin In [Enum].GetValues(GetType(ReadingBin))
                    productSort = Me.SelectProductSort(CType(element.ElementTypeId, ElementType), CType(nomTypeEntity.Id, NomType), readingBin)
                    productSortId = sorts.ProductSort(productSort.Description).AutoId
                    ' add mapping from bin to sort for all elements and all nominal types
                    Me.Upadd(New ProductBinningEntity With {.ProductAutoId = Me.ProductAutoId, .ElementAutoId = element.AutoId,
                                                        .NomTypeId = nomTypeEntity.Id, .ReadingBinId = readingBin,
                                                        .ProductSortAutoId = productSortId})
                Next
            Next
        Next
        ' save the collection
        Me.Upsert(connection)
    End Sub

#End Region

End Class

''' <summary> Product binning unique key selector. </summary>
''' <remarks> David, 6/14/2020. </remarks>
Public Structure ProductBinningUniqueKey
    Implements IEquatable(Of ProductBinningUniqueKey)
    Public Sub New(ByVal binning As ProductBinningEntity)
        Me.New
        Me.ElementId = binning.ElementAutoId
        Me.NomTypeId = binning.NomTypeId
        Me.ReadingBinId = binning.ReadingBinId
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="elementAutoId"> Identifier for the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifier for the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="readingBinId">  Identifier for the <see cref="Entities.ReadingBinEntity"/>. </param>
    Public Sub New(ByVal elementAutoId As Integer, ByVal nomTypeId As Integer, ByVal readingBinId As Integer)
        Me.New
        Me.ElementId = elementAutoId
        Me.NomTypeId = nomTypeId
        Me.ReadingBinId = readingBinId
    End Sub
    Public Property ElementId As Integer
    Public Property NomTypeId As Integer
    Public Property ReadingBinId As Integer

    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.Equals(CType(obj, ProductBinningUniqueKey))
    End Function

    Public Overloads Function Equals(other As ProductBinningUniqueKey) As Boolean Implements IEquatable(Of ProductBinningUniqueKey).Equals
        Return Me.ElementId = other.ElementId AndAlso Me.NomTypeId = other.NomTypeId AndAlso Me.ReadingBinId = other.ReadingBinId
    End Function

    Public Overrides Function GetHashCode() As Integer
        Return Me.ElementId Xor Me.NomTypeId Xor Me.ReadingBinId
    End Function

    Public Shared Operator =(left As ProductBinningUniqueKey, right As ProductBinningUniqueKey) As Boolean
        Return left.Equals(right)
    End Operator

    Public Shared Operator <>(left As ProductBinningUniqueKey, right As ProductBinningUniqueKey) As Boolean
        Return Not left = right
    End Operator


End Structure

