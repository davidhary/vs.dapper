Imports Dapper

Partial Public Class ProductEntity

    ''' <summary>
    ''' Gets or sets the <see cref="ProductTraitEntity">Product Trait Real(Double)-value
    ''' entities</see>.
    ''' </summary>
    ''' <value> The product Trait entities. </value>
    Public ReadOnly Property TraitEntities As IEnumerable(Of ProductTraitEntity)

    ''' <summary>
    ''' Fetches the <see cref="ProductTraitEntity">Product Trait Real(Double)-value
    ''' entities</see>.
    ''' </summary>
    ''' <remarks> David, 5/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The product Trait entities. </returns>
    Public Function FetchTraitEntities(ByVal connection As System.Data.IDbConnection) As Integer
        If Not ProductTraitTypeEntity.IsEnumerated() Then ProductTraitTypeEntity.TryFetchAll(connection)
        Return Me.Populate(ProductTraitEntity.FetchEntities(connection, Me.AutoId))
    End Function

    ''' <summary> Fetches the <see cref="ProductTraitEntity">Product Real(Double)-Value Trait entities</see>. </summary>
    ''' <remarks>
    ''' https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
    ''' https://dapper-tutorial.net/querymultiple.
    ''' </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the Product. </param>
    ''' <returns> The number of Product Trait entities. </returns>
    Public Function FetchTraitEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As Integer
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select * from [{ProductBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(ProductNub.AutoId)} = @Id; ")
        queryBuilder.AppendLine($"select * from [{ProductTraitBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(ProductTraitNub.PrimaryId)} = @Id; ")
        Dim gridReader As SqlMapper.GridReader = connection.QueryMultiple(queryBuilder.ToString, New With {.Id = productAutoId})
        Dim entity As ProductNub = gridReader.ReadSingle(Of ProductNub)()
        If entity Is Nothing Then
            Return Me.Populate(New List(Of ProductTraitEntity))
        Else
            If Not ProductNub.AreEqual(entity, Me.ICache) Then
                Me.UpdateCache(entity)
                Me.UpdateStore()
            End If
            If Not ProductTraitTypeEntity.IsEnumerated() Then ProductTraitTypeEntity.TryFetchAll(connection)
            Return Me.Populate(ProductTraitEntity.Populate(gridReader.Read(Of ProductTraitNub)))
        End If
    End Function

    ''' <summary> Gets or sets the <see cref="ProductUniqueTraitEntityCollection">Product Trait Real(Double)-values</see>. </summary>
    ''' <value> The Product Trait Real(Double)-values. </value>
    Public ReadOnly Property Traits As ProductUniqueTraitEntityCollection

    ''' <summary>
    ''' Fetches the <see cref="ProductUniqueTraitEntityCollection">Product Trait Real(Double)-
    ''' values</see>.
    ''' </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of <see cref="Entities.ProductUniqueTraitEntityCollection"/>'s. </returns>
    Public Function FetchTraits(ByVal connection As System.Data.IDbConnection) As Integer
        If Not ProductTraitTypeEntity.IsEnumerated() Then ProductTraitTypeEntity.TryFetchAll(connection)
        Return Me.Populate(ProductTraitEntity.FetchEntities(connection, Me.AutoId))
    End Function

    ''' <summary>
    ''' Fetches the <see cref="ProductUniqueTraitEntityCollection">Product Trait Real(Double)-
    ''' values</see>.
    ''' </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> The Product Traits. </returns>
    Public Shared Function FetchTraits(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As ProductUniqueTraitEntityCollection
        If Not ProductTraitTypeEntity.IsEnumerated() Then ProductTraitTypeEntity.TryFetchAll(connection)
        Dim Values As New ProductUniqueTraitEntityCollection(productAutoId)
        Values.Populate(ProductTraitEntity.FetchEntities(connection, productAutoId))
        Return Values
    End Function

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    ''' <returns> The number of entities. </returns>
    Private Overloads Function Populate(ByVal entities As IEnumerable(Of ProductTraitEntity)) As Integer
        Me._TraitEntities = entities
        If Me._Traits Is Nothing Then
            Me._Traits = New ProductUniqueTraitEntityCollection(Me.AutoId)
        Else
            Me._Traits.Clear()
        End If
        Me._Traits.Populate(entities)
        Me.NotifyPropertyChanged(NameOf(ProductEntity.TraitEntities))
        Me.NotifyPropertyChanged(NameOf(ProductEntity.Traits))
        Return If(entities?.Any, entities.Count, 0)
    End Function

End Class

