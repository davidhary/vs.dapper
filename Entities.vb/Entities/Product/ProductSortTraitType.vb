Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A Product Sort Trait type builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class ProductSortTraitTypeBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ProductSortTraitTypeBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ProductSortTraitTypeBuilder._TableName) Then
                ProductSortTraitTypeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ProductSortTraitTypeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Inserts or ignore the records as described by the <see cref="ProductSortTraitType"/> enumeration type. </summary>
    ''' <param name="connection"> The connection. </param>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(ProductSortTraitType), New Integer() {CInt(ProductSortTraitType.None)})
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ProductSortTraitTypeBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ProductSortTraitTypeBuilder
        If ProductSortTraitTypeBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ProductSortTraitTypeBuilder._Instance = New ProductSortTraitTypeBuilder()
            End SyncLock
        End If
        Return ProductSortTraitTypeBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the <see cref="ProductSortTraitTypeEntity"/> nub based on the <see cref="INominal">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("ProductSortTraitType")>
Public Class ProductSortTraitTypeNub
    Inherits NominalNub
    Implements INominal

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As INominal
        Return New ProductSortTraitTypeNub
    End Function

End Class

''' <summary> The <see cref="ProductSortTraitTypeEntity"/> based on the <see cref="INominal">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class ProductSortTraitTypeEntity
    Inherits EntityBase(Of INominal, ProductSortTraitTypeNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New ProductSortTraitTypeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The ProductSortTraitType interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New ProductSortTraitTypeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ProductSortTraitTypeBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As INominal
        Return New ProductSortTraitTypeNub
    End Function

    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        ProductSortTraitTypeNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As INominal)
        ProductSortTraitTypeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The ProductSortTraitType interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        ProductSortTraitTypeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The ProductSortTraitType table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of ProductSortTraitTypeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Trait Type label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As ProductSortTraitTypeNub = ProductSortTraitTypeEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ProductSortTraitTypeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of ProductSortTraitTypeNub)(New ProductSortTraitTypeNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the ProductSortTraitType entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The ProductSortTraitType entities. </value>
    Public Shared ReadOnly Property ProductSortTraitTypes As IEnumerable(Of ProductSortTraitTypeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ProductSortTraitTypeEntity)
        Return If(usingNativeTracking, ProductSortTraitTypeEntity.Populate(connection.GetAll(Of INominal)), ProductSortTraitTypeEntity.Populate(connection.GetAll(Of ProductSortTraitTypeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        ProductSortTraitTypeEntity._ProductSortTraitTypes = ProductSortTraitTypeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(ProductSortTraitTypeEntity.ProductSortTraitTypes?.Any, ProductSortTraitTypeEntity.ProductSortTraitTypes.Count, 0)
    End Function

    ''' <summary> Populates a list of ProductSortTraitType entities. </summary>
    ''' <param name="nubs"> The ProductSortTraitType nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ProductSortTraitTypeNub)) As IEnumerable(Of ProductSortTraitTypeEntity)
        Dim l As New List(Of ProductSortTraitTypeEntity)
        If nubs?.Any Then
            For Each nub As ProductSortTraitTypeNub In nubs
                l.Add(New ProductSortTraitTypeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of ProductSortTraitType entities. </summary>
    ''' <param name="interfaces"> The ProductSortTraitType interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of ProductSortTraitTypeEntity)
        Dim l As New List(Of ProductSortTraitTypeEntity)
        If interfaces?.Any Then
            Dim nub As New ProductSortTraitTypeNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ProductSortTraitTypeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, ProductSortTraitTypeEntity)

    ''' <summary> The Sort Trait type lookup dictionary. </summary>
    ''' <returns> A Dictionary(Of Integer, ProductSortTraitTypeEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, ProductSortTraitTypeEntity)
        If Not (ProductSortTraitTypeEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            ProductSortTraitTypeEntity._EntityLookupDictionary = New Dictionary(Of Integer, ProductSortTraitTypeEntity)
            For Each entity As ProductSortTraitTypeEntity In ProductSortTraitTypeEntity.ProductSortTraitTypes
                ProductSortTraitTypeEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return ProductSortTraitTypeEntity._EntityLookupDictionary
    End Function

    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (ProductSortTraitTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            ProductSortTraitTypeEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As ProductSortTraitTypeEntity In ProductSortTraitTypeEntity.ProductSortTraitTypes
                ProductSortTraitTypeEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return ProductSortTraitTypeEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (ProductSortTraitTypeEntity.ProductSortTraitTypes?.Any AndAlso
                ProductSortTraitTypeEntity._EntityLookupDictionary?.Any AndAlso
                ProductSortTraitTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Sort Trait type label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{ProductSortTraitTypeNub.TableName}]
        ' WHERE {NameOf(ProductSortTraitTypeNub.Label)} = @label", New With {Key .Label = label})
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{ProductSortTraitTypeBuilder.TableName}] WHERE {NameOf(ProductSortTraitTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Sort Trait type label. </param>
    ''' <returns> An IProductSortTraitType . </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of ProductSortTraitTypeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductSortTraitTypeBuilder.TableName}] WHERE {NameOf(ProductSortTraitTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductSortTraitTypeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the ProductSortTraitType exists. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = ProductSortTraitTypeEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Sort Trait type. </summary>
    ''' <value> Identifies the Sort Trait type. </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductSortTraitTypeEntity.ProductSortTraitType))
            End If
        End Set
    End Property

    ''' <summary> Identifies the <see cref="Entities.ProductSortTraitType"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ProductSortTraitType"/>. </value>
    Public Property ProductSortTraitType As ProductSortTraitType
        Get
            Return CType(Me.Id, ProductSortTraitType)
        End Get
        Set(ByVal value As ProductSortTraitType)
            Me.Id = value
        End Set
    End Property

    ''' <summary> Gets or sets the Sort Trait type label. </summary>
    ''' <value> The Sort Trait type label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the Sort Trait type. </summary>
    ''' <value> The Sort Trait type description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="ProductSortTraitTypeId"> The entity unique key. </param>
    ''' <param name="e">           Action event information. </param>
    ''' <returns> A <see cref="ProductSortTraitTypeEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal productSortTraitTypeId As Integer, ByVal e As isr.Core.ActionEventArgs) As ProductSortTraitTypeEntity
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Dim result As New ProductSortTraitTypeEntity
        Try
            activity = $"Fetching {NameOf(ProductSortTraitTypeEntity)} by {NameOf(ProductSortTraitTypeNub.Id)} of {productSortTraitTypeId}"
            If Not result.FetchUsingKey(connection, productSortTraitTypeId) Then
                e.RegisterFailure($"Failed {activity}")
            End If
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return result
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <param name="connection">             The connection. </param>
    ''' <param name="productSortTraitTypeLabel"> The Sort Trait type label. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entity As ProductSortTraitTypeEntity)
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal productSortTraitTypeLabel As String) As (Success As Boolean, Details As String, Entity As ProductSortTraitTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New ProductSortTraitTypeEntity
        Try
            activity = $"Fetching {NameOf(ProductSortTraitTypeEntity)} by {NameOf(ProductSortTraitTypeNub.Label)} of {productSortTraitTypeLabel}"
            Return If(entity.FetchUsingUniqueIndex(connection, productSortTraitTypeLabel),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of
    ''' ProductSortTraitTypeEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of ProductSortTraitTypeEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New ProductSortTraitTypeEntity()
            activity = $"fetching all {NameOf(ProductSortTraitTypeEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> ProductSortTraitTypeEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(ProductSortTraitTypeEntity.EntityLookupDictionary)} count must equal {NameOf(ProductSortTraitTypeEntity.ProductSortTraitTypes)} count ")
            ElseIf elementCount <> ProductSortTraitTypeEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(ProductSortTraitTypeEntity.KeyLookupDictionary)} count must equal {NameOf(ProductSortTraitTypeEntity.ProductSortTraitTypes)} count ")
            End If
            Return (True, String.Empty, ProductSortTraitTypeEntity.ProductSortTraitTypes)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of ProductSortTraitTypeEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Values that represent Product Sort Trait types. </summary>
Public Enum ProductSortTraitType
    <ComponentModel.Description("None")> None = 0
    <ComponentModel.Description("Continuous Failure Count Limit")> ContinuousFailureCountLimit = 1
    <ComponentModel.Description("Digital Output")> DigitalOutput = 2
    <ComponentModel.Description("Bucket Number")> BucketNumber = 3

    ''' <summary> Identity of the <see cref="BucketBinEntity"/>. </summary>
    <ComponentModel.Description("Bucket Bin")> BucketBin = 4
    <ComponentModel.Description("Bucket Background Color")> BucketBackColor = 5
End Enum
