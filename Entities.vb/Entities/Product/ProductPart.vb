Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Core

''' <summary> A Product-Part builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class ProductPartBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return ProductPartBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(ProductPartBuilder._TableName) Then
                ProductPartBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(ProductPartNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = ProductBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(ProductNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = PartBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(PartNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(ProductPartEntity.ProductAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(ProductPartEntity.PartAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As ProductPartBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As ProductPartBuilder
        If ProductPartBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                ProductPartBuilder._Instance = New ProductPartBuilder()
            End SyncLock
        End If
        Return ProductPartBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Product Part Nub based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("ProductPart")>
Public Class ProductPartNub
    Inherits OneToManyNub
    Implements IOneToMany

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToMany
        Return New ProductPartNub
    End Function

End Class

''' <summary> The Product-Part Entity based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 05/22/2020 </para></remarks>
Public Class ProductPartEntity
    Inherits EntityBase(Of IOneToMany, ProductPartNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New ProductPartNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Product-Part interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New ProductPartNub, cache, store)
        Me.UsingNativeTracking = String.Equals(ProductPartBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToMany
        Return New ProductPartNub
    End Function

    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        ProductPartNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        ProductPartNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Product-Part interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        ProductPartNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="PartAutoId">      Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal partAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As ProductPartNub = ProductPartEntity.FetchNubs(connection, productAutoId, partAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.ProductAutoId, Me.PartAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.ProductAutoId, Me.PartAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productAutoId"> Identifies the Product. </param>
    ''' <param name="partAutoId">  Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal partAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, productAutoId, partAutoId)
    End Function

    ''' <summary>
    ''' Tries to fetch an existing or insert a new <see cref="Entities.PartEntity"/>  and fetch an existing or
    ''' insert a new <see cref="ProductPartEntity"/>. If the part number is non-unique, a part is inserted
    ''' if this part number is not associated with this product.
    ''' </summary>
    ''' <remarks>
    ''' Assumes that a <see cref="Entities.ProductEntity"/> exists for the specified <paramref name="productAutoId"/>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="partNumber">  The part number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainPart(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal partNumber As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If PartBuilder.Get.UsingUniqueLabel(connection) Then
            ' if using a unique part number, the part number is unique across all products
            Me._PartEntity = New PartEntity() With {.PartNumber = partNumber}
            If Not Me.PartEntity.Obtain(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} of {partNumber}")
            End If
        Else
            ' if not using a unique part number, the part number is unique for this product
            Me._PartEntity = ProductPartEntity.FetchPart(connection, productAutoId, partNumber)
            If Not Me.PartEntity.IsClean Then
                Me._PartEntity = New PartEntity() With {.PartNumber = partNumber}
                If Not Me.PartEntity.Insert(connection) Then
                    result = (False, $"Failed inserting {NameOf(Entities.PartEntity)} with {NameOf(Entities.PartEntity.PartNumber)} of {partNumber}")
                End If
            End If
        End If
        If result.Success Then
            Me.PrimaryId = productAutoId
            Me.SecondaryId = Me.PartEntity.AutoId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(ProductPartEntity.PartEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.ProductPartEntity)} with {NameOf(Entities.ProductPartEntity.ProductAutoId)} of {productAutoId} and {NameOf(Entities.ProductPartEntity.PartAutoId)} of {Me.PartEntity.AutoId}")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.ProductEntity"/> and <see cref="Entities.PartEntity"/> entities
    ''' and fetches an existing or inserts a new <see cref="ProductPartEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="partNumber">  The part number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal partNumber As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.ProductEntity Is Nothing OrElse Me.ProductEntity.AutoId <> productAutoId Then
            ' make sure a product exists for the auto id.
            Me._ProductEntity = New ProductEntity With {.AutoId = productAutoId}
            If Not Me._ProductEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.ProductEntity)} with {NameOf(Entities.ProductEntity.AutoId)} of {productAutoId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainPart(connection, productAutoId, partNumber)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(ProductPartEntity.ProductEntity))
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.ProductEntity"/> and <see cref="Entities.PartEntity"/> entities
    ''' and fetches an existing or inserts a new <see cref="ProductPartEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal partNumber As String) As (Success As Boolean, Details As String)
        Return Me.TryObtain(connection, Me.ProductAutoId, partNumber)
    End Function

    ''' <summary>
    ''' Fetches an existing or inserts new <see cref="Entities.ProductEntity"/> and <see cref="Entities.PartEntity"/> entities
    ''' and fetches an existing or inserts a new <see cref="ProductPartEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productAutoId"> Identifies the Product. </param>
    ''' <param name="partNumber">  The part number. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Function Obtain(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal partNumber As String) As Boolean
        Dim r As (Success As Boolean, Details As String) = Me.TryObtain(connection, productAutoId, partNumber)
        If Not r.Success Then Throw New OperationFailedException(r.Details)
        Return r.Success
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If ProductPartEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId">     Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="PartAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal partAutoId As Integer) As Boolean
        Return connection.Delete(Of ProductPartNub)(New ProductPartNub With {.PrimaryId = productAutoId, .SecondaryId = partAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Product-Part entities. </summary>
    ''' <value> The Product-Part entities. </value>
    Public ReadOnly Property ProductParts As IEnumerable(Of ProductPartEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of ProductPartEntity)
        Return If(usingNativeTracking, ProductPartEntity.Populate(connection.GetAll(Of IOneToMany)), ProductPartEntity.Populate(connection.GetAll(Of ProductPartNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ProductParts = ProductPartEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(ProductPartEntity.ProductParts))
        Return If(Me.ProductParts?.Any, Me.ProductParts.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of ProductPartNub)) As IEnumerable(Of ProductPartEntity)
        Dim l As New List(Of ProductPartEntity)
        If nubs?.Any Then
            For Each nub As ProductPartNub In nubs
                l.Add(New ProductPartEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of ProductPartEntity)
        Dim l As New List(Of ProductPartEntity)
        If interfaces?.Any Then
            Dim nub As New ProductPartNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New ProductPartEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to Part entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductPartBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductPartNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productAutoId">  Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As IEnumerable(Of ProductPartEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductPartBuilder.TableName}] WHERE {NameOf(ProductPartNub.PrimaryId)} = @Id", New With {Key .Id = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ProductPartEntity.Populate(connection.Query(Of ProductPartNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Part. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="PartAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> The total number of entities by Part. </returns>
    Public Shared Function CountEntitiesByPart(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductPartBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductPartNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Parts in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="PartAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Parts in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByPart(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of ProductPartEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductPartBuilder.TableName}] WHERE {NameOf(ProductPartNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return ProductPartEntity.Populate(connection.Query(Of ProductPartNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="PartAutoId">       Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{ProductPartBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductPartNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        sqlBuilder.Where($"{NameOf(ProductPartNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="PartAutoId">       Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal partAutoId As Integer) As IEnumerable(Of ProductPartNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{ProductPartBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(ProductPartNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = productAutoId})
        sqlBuilder.Where($"{NameOf(ProductPartNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of ProductPartNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Product Part exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="PartAutoId">       Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal partAutoId As Integer) As Boolean
        Return 1 = ProductPartEntity.CountEntities(connection, productAutoId, partAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Product entity. </summary>
    ''' <value> The Product entity. </value>
    Public ReadOnly Property ProductEntity As ProductEntity

    ''' <summary> Fetches Product Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Product Entity. </returns>
    Public Function FetchProductEntity(ByVal connection As System.Data.IDbConnection) As ProductEntity
        Dim entity As New ProductEntity()
        entity.FetchUsingKey(connection, Me.ProductAutoId)
        Me._ProductEntity = entity
        Return entity
    End Function

    ''' <summary> Count Products associated with the specified <paramref name="PartAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="PartAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> The total number of Products. </returns>
    Public Shared Function CountProducts(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{ProductPartBuilder.TableName}] WHERE {NameOf(ProductPartNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Products associated with the specified <paramref name="PartAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="PartAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Products in this collection.
    ''' </returns>
    Public Shared Function FetchProducts(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of PartEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductPartBuilder.TableName}] WHERE {NameOf(ProductPartNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = partAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of PartEntity)
        For Each nub As ProductPartNub In connection.Query(Of ProductPartNub)(selector.RawSql, selector.Parameters)
            Dim entity As New ProductPartEntity(nub)
            l.Add(entity.FetchPartEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Products associated with the specified <paramref name="PartAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="PartAutoId"> Identifies the <see cref="Entities.PartEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteProducts(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{ProductPartBuilder.TableName}] WHERE {NameOf(ProductPartNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = partAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the Part entity. </summary>
    ''' <value> The Part entity. </value>
    Public ReadOnly Property PartEntity As PartEntity

    ''' <summary> Fetches a Part Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Part Entity. </returns>
    Public Function FetchPartEntity(ByVal connection As System.Data.IDbConnection) As PartEntity
        Dim entity As New PartEntity()
        entity.FetchUsingKey(connection, Me.PartAutoId)
        Me._PartEntity = entity
        Return entity
    End Function

    Public Shared Function CountParts(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{ProductPartBuilder.TableName}] WHERE {NameOf(ProductPartNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Parts in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="productAutoId"> Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Parts in this collection.
    ''' </returns>
    Public Shared Function FetchParts(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As IEnumerable(Of PartEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{ProductPartBuilder.TableName}] WHERE {NameOf(ProductPartNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of PartEntity)
        For Each nub As ProductPartNub In connection.Query(Of ProductPartNub)(selector.RawSql, selector.Parameters)
            Dim entity As New ProductPartEntity(nub)
            l.Add(entity.FetchPartEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Part related to the specified Product. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="productAutoId">  Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteParts(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{ProductPartBuilder.TableName}] WHERE {NameOf(ProductPartNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = productAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches a <see cref="Entities.PartEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="selectQuery">  The select query. </param>
    ''' <param name="productAutoId">   Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="partLabel"> The part label. </param>
    ''' <returns> The part. </returns>
    Public Shared Function FetchPart(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal productAutoId As Integer, ByVal partLabel As String) As PartEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.Id = productAutoId, .Label = partLabel})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As PartNub = connection.Query(Of PartNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New PartEntity, New PartEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches a <see cref="Entities.PartEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="productAutoId">   Identifies the <see cref="Entities.ProductEntity"/>. </param>
    ''' <param name="partLabel"> The part label. </param>
    ''' <returns> The part. </returns>
    Public Shared Function FetchPart(ByVal connection As System.Data.IDbConnection, ByVal productAutoId As Integer, ByVal partLabel As String) As PartEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [ProductPart] on [ProductPart].SecondaryId = [Part].AutoId where [ProductPart].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{PartBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{PartBuilder.TableName}] Inner Join [{ProductPartBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{ProductPartBuilder.TableName}].{NameOf(ProductPartNub.SecondaryId)} = [{PartBuilder.TableName}].{NameOf(PartNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{ProductPartBuilder.TableName}].{NameOf(ProductPartNub.PrimaryId)} = @Id AND [{PartBuilder.TableName}].{NameOf(PartNub.Label)} = @Label); ")
        Return ProductPartEntity.FetchPart(connection, queryBuilder.ToString, productAutoId, partLabel)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductPartEntity.ProductAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Product. </summary>
    ''' <value> Identifies the Product. </value>
    Public Property ProductAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(ProductPartEntity.PartAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.PartEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.PartEntity"/>. </value>
    Public Property PartAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
