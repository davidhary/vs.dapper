Imports Dapper

Partial Public Class ProductSortEntity

    ''' <summary>
    ''' Gets or sets the <see cref="IEnumerable(Of ProductSortTraitEntity)">Product Sort
    ''' Natural(Integer)-value Trait entities. </see>.
    ''' </summary>
    ''' <value> The product Sort Trait entities. </value>
    Public ReadOnly Property TraitEntities As IEnumerable(Of ProductSortTraitEntity)

    ''' <summary>
    ''' Fetches the <see cref="IEnumerable(Of ProductSortTraitEntity)">Product Sort Natural(Integer)-value Trait entities</see>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The entities count. </returns>
    Public Function FetchTraitEntities(ByVal connection As System.Data.IDbConnection) As Integer
        If Not ProductSortTraitTypeEntity.IsEnumerated() Then ProductSortTraitTypeEntity.TryFetchAll(connection)
        Return Me.Populate(ProductSortTraitEntity.FetchEntities(connection, Me.AutoId))
    End Function

    ''' <summary>
    ''' Fetches the <see cref="IEnumerable(Of ProductSortTraitEntity)">Product Sort Natural(Integer)-value Trait entities</see>.
    ''' </summary>
    ''' <remarks>
    ''' https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
    ''' https://dapper-tutorial.net/querymultiple.
    ''' </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="ProductSortAutoId">  Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns> The entities count. </returns>
    Public Function FetchTraitEntities(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer) As Integer
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select * from [{ProductSortBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(ProductSortNub.AutoId)} = @Id; ")
        queryBuilder.AppendLine($"select * from [{ProductSortTraitBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(ProductSortTraitNub.PrimaryId)} = @Id; ")
        Dim gridReader As SqlMapper.GridReader = connection.QueryMultiple(queryBuilder.ToString, New With {.Id = productSortAutoId})
        Dim entity As ProductSortNub = gridReader.ReadSingle(Of ProductSortNub)()
        If entity Is Nothing Then
            Return Me.Populate(New List(Of ProductSortTraitEntity))
        Else
            If Not ProductSortNub.AreEqual(entity, Me.ICache) Then
                Me.UpdateCache(entity)
                Me.UpdateStore()
            End If
            If Not ProductSortTraitTypeEntity.IsEnumerated() Then ProductSortTraitTypeEntity.TryFetchAll(connection)
            Return Me.Populate(ProductSortTraitEntity.Populate(gridReader.Read(Of ProductSortTraitNub)))
        End If
    End Function

    ''' <summary> Gets or sets the <see cref="ProductUniqueSortTraitEntityCollection"/>. </summary>
    ''' <value> The <see cref="ProductUniqueSortTraitEntityCollection"/>. </value>
    Public ReadOnly Property Traits As ProductUniqueSortTraitEntityCollection

    ''' <summary> Fetches <see cref="ProductUniqueSortTraitEntityCollection"/>. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The <see cref="ProductUniqueSortTraitEntityCollection"/>. </returns>
    Public Function FetchTraits(ByVal connection As System.Data.IDbConnection) As Integer
        If Not ProductSortTraitTypeEntity.IsEnumerated() Then ProductSortTraitTypeEntity.TryFetchAll(connection)
        Return Me.Populate(ProductSortTraitEntity.FetchEntities(connection, Me.AutoId))
    End Function

    ''' <summary> Fetches <see cref="ProductUniqueSortTraitEntityCollection"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="productSortAutoId"> Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns> The <see cref="ProductUniqueSortTraitEntityCollection"/>. </returns>
    Public Shared Function FetchTraits(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer) As ProductUniqueSortTraitEntityCollection
        If Not ProductSortTraitTypeEntity.IsEnumerated() Then ProductSortTraitTypeEntity.TryFetchAll(connection)
        Dim Naturals As New ProductUniqueSortTraitEntityCollection(productSortAutoId)
        Naturals.Populate(ProductSortTraitEntity.FetchEntities(connection, productSortAutoId))
        Return Naturals
    End Function

    ''' <summary> Populates a list of <see cref="ProductSortTraitEntity"/>'s. </summary>
    ''' <remarks> David, 5/24/2020. </remarks>
    ''' <param name="entities"> The <see cref="ProductSortTraitEntity"/>'s. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Private Overloads Function Populate(ByVal entities As IEnumerable(Of ProductSortTraitEntity)) As Integer
        Me._TraitEntities = entities
        If Me._Traits Is Nothing Then
            Me._Traits = New ProductUniqueSortTraitEntityCollection(Me.AutoId)
        Else
            Me._Traits.Clear()
        End If
        Me._Traits.Populate(entities)
        Me.NotifyPropertyChanged(NameOf(ProductSortEntity.TraitEntities))
        Me.NotifyPropertyChanged(NameOf(ProductSortEntity.Traits))
        Return If(entities?.Any, entities.Count, 0)
    End Function

End Class

