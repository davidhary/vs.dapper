''' <summary> The Product Trait class holing the Product trait values. </summary>
''' <remarks> David, 2020-05-29. </remarks>
Public Class ProductTrait
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSestter"> The getter setter. </param>
    Public Sub New(ByVal getterSestter As isr.Core.Constructs.IGetterSetter(Of Double))
        MyBase.New()
        Me.GetterSetter = getterSestter
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter(Of Double)

    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.GetterSetter.Setter(value.Value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

#End Region

#Region " PCM "

    ''' <summary> Assign default PCM traits. </summary>
    ''' <remarks> David, 7/7/2020. </remarks>
    ''' <returns> An Integer. </returns>
    Public Function AssignDefaultPcmTraits() As Integer
        Dim addedCount As Integer = 0
        If Not Me.MinimumCp.HasValue Then addedCount += 1
        Me.MinimumCp = Me.DefaultCp
        If Not Me.MinimumCpk.HasValue Then addedCount += 1
        Me.MinimumCpk = Me.DefaultCpk
        If Not Me.MinimumCpm.HasValue Then addedCount += 1
        Me.MinimumCpm = Me.DefaultCpm
        If Not Me.SigmaLevel.HasValue Then addedCount += 1
        Me.SigmaLevel = Me.DefaultSigmaLevel
        Return addedCount
    End Function


    Private _DefaultCp As Double = 1.33

    ''' <summary> Gets or sets the default cp. </summary>
    ''' <value> The default cp. </value>
    Public Property DefaultCp As Double
        Get
            Return Me._DefaultCp
        End Get
        Set(value As Double)
            If value <> Me.DefaultCp Then
                Me._DefaultCp = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the minimum Cp (process capability for a centered process). </summary>
    ''' <value> The minimum Cp. </value>
    Public Property MinimumCp As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    Private _DefaultCpk As Double = 1.33

    ''' <summary> Gets or sets the default cpk. </summary>
    ''' <value> The default cpk. </value>
    Public Property DefaultCpk As Double
        Get
            Return Me._DefaultCpk
        End Get
        Set(value As Double)
            If value <> Me.DefaultCpk Then
                Me._DefaultCpk = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the minimum Cpk (process capability for a process that is not centered). </summary>
    ''' <value> The minimum Cpk. </value>
    Public Property MinimumCpk As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    Private _DefaultCpm As Double = 1.33

    ''' <summary> Gets or sets the default cpm. </summary>
    ''' <value> The default cpm. </value>
    Public Property DefaultCpm As Double
        Get
            Return Me._DefaultCpm
        End Get
        Set(value As Double)
            If value <> Me.DefaultCpm Then
                Me._DefaultCpm = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the minimum Cpm (process capability with bias consideration). </summary>
    ''' <value> The minimum Cpm. </value>
    Public Property MinimumCpm As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    Private _DefaultSigmaLevel As Double = 6

    ''' <summary> Gets or sets the default sigma level. </summary>
    ''' <value> The default sigma level. </value>
    Public Property DefaultSigmaLevel As Double
        Get
            Return Me._DefaultSigmaLevel
        End Get
        Set(value As Double)
            If value <> Me.DefaultSigmaLevel Then
                Me._DefaultSigmaLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the sigma level. </summary>
    ''' <value> The sigma level. </value>
    Public Property SigmaLevel As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

#End Region

End Class
