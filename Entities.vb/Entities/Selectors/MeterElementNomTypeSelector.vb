''' <summary> An multimeter element nominal type selector. </summary>
''' <remarks> David, 6/16/2020. </remarks>
Public Structure MeterElementNomTypeSelector
    Implements IEquatable(Of MeterElementNomTypeSelector)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="multimeterId">  Identifier for the <see cref="MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifier for the <see cref="ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifier for the <see cref="NomTypeEntity"/>. </param>
    Public Sub New(ByVal multimeterId As Integer, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer)
        Me.MultimeterId = multimeterId
        Me.ElementAutoId = elementAutoId
        Me.NomTypeId = nomTypeId
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="nominals"> The nominals. </param>
    Public Sub New(ByVal nominals As PartNomEntityCollection)
        Me.New(nominals.MultimeterId, nominals.ElementAutoId, nominals.NomTypeId)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="sampleTrait"> The sample trait. </param>
    Public Sub New(ByVal sampleTrait As SampleTrait)
        Me.New(sampleTrait.MultimeterId, sampleTrait.ElementAutoId, sampleTrait.NomType)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="nomReading"> The nom reading. </param>
    Public Sub New(ByVal nomReading As NomReading)
        Me.New(nomReading.MultimeterId, nomReading.ElementAutoId, nomReading.NomType)
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="MultimeterEntity"/>. </summary>
    ''' <value> The identifier of the <see cref="MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer

    ''' <summary> Gets or sets the identifier of the <see cref="ElementEntity"/>. </summary>
    ''' <value> The identifier of the <see cref="ElementEntity"/>. </value>
    Public Property ElementAutoId As Integer

    ''' <summary> Gets or sets the identifier of the <see cref="NomTypeEntity"/>. </summary>
    ''' <value> The identifier of the <see cref="NomTypeEntity"/>. </value>
    Public Property NomTypeId As Integer

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <param name="obj"> The object to compare with the current instance. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
    ''' represent the same value; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.Equals(CType(obj, MeterElementNomTypeSelector))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Function Equals(other As MeterElementNomTypeSelector) As Boolean Implements IEquatable(Of MeterElementNomTypeSelector).Equals
        Return Me.MultimeterId = other.MultimeterId AndAlso Me.ElementAutoId = other.ElementAutoId AndAlso Me.NomTypeId = other.NomTypeId
    End Function

    ''' <summary> Returns the hash code for this instance. </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <returns> A 32-bit signed integer that is the hash code for this instance. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.MultimeterId Xor Me.ElementAutoId Xor Me.NomTypeId
    End Function

    ''' <summary> Cast that converts the given MeterElementNomTypeSelector to a =. </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(left As MeterElementNomTypeSelector, right As MeterElementNomTypeSelector) As Boolean
        Return left.Equals(right)
    End Operator

    ''' <summary> Cast that converts the given MeterElementNomTypeSelector to a &lt;&gt; </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(left As MeterElementNomTypeSelector, right As MeterElementNomTypeSelector) As Boolean
        Return Not left = right
    End Operator

End Structure
