''' <summary> A nut multimeter nominal type selector. </summary>
''' <remarks> David, 7/1/2020. </remarks>
Public Structure NutMultimeterNomTypeSelector
    Implements IEquatable(Of NutMultimeterNomTypeSelector)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <param name="nominals"> The nominals. </param>
    Public Sub New(ByVal nominals As NutUniqueNomReadingEntityCollection)
        Me.New(nominals.NutAutoId, nominals.NomReading.MultimeterId, nominals.NomReading.NomType)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="nutAutoId">    Identifier for the nut automatic. </param>
    ''' <param name="multimeterId"> Identifier for the <see cref="MultimeterEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifier for the <see cref="NomTypeEntity"/>. </param>
    Public Sub New(ByVal nutAutoId As Integer, ByVal multimeterId As Integer, ByVal nomTypeId As Integer)
        Me.NutAutoId = nutAutoId
        Me.MultimeterId = multimeterId
        Me.NomTypeId = nomTypeId
    End Sub

    ''' <summary> Gets or sets the identifier of the nut automatic. </summary>
    ''' <value> The identifier of the nut automatic. </value>
    Public Property NutAutoId As Integer

    ''' <summary> Gets or sets the identifier of the multimeter. </summary>
    ''' <value> The identifier of the multimeter. </value>
    Public Property MultimeterId As Integer

    ''' <summary> Gets or sets the identifier of the nom type. </summary>
    ''' <value> The identifier of the nom type. </value>
    Public Property NomTypeId As Integer

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <param name="obj"> The object to compare with the current instance. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
    ''' represent the same value; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.Equals(CType(obj, NutMultimeterNomTypeSelector))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Function Equals(other As NutMultimeterNomTypeSelector) As Boolean Implements IEquatable(Of NutMultimeterNomTypeSelector).Equals
        Return Me.MultimeterId = other.MultimeterId AndAlso Me.NutAutoId = other.NutAutoId AndAlso Me.NomTypeId = other.NomTypeId
    End Function

    ''' <summary> Returns the hash code for this instance. </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <returns> A 32-bit signed integer that is the hash code for this instance. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.MultimeterId Xor Me.NutAutoId Xor Me.NomTypeId
    End Function

    ''' <summary> Cast that converts the given NutMultimeterNomTypeSelector to a =. </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(left As NutMultimeterNomTypeSelector, right As NutMultimeterNomTypeSelector) As Boolean
        Return left.Equals(right)
    End Operator

    ''' <summary>
    ''' Cast that converts the given NutMultimeterNomTypeSelector to a &lt;&gt;
    ''' </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(left As NutMultimeterNomTypeSelector, right As NutMultimeterNomTypeSelector) As Boolean
        Return Not left = right
    End Operator

End Structure
