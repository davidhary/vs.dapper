
Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Nut reading Real-value builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class NutReadingBuilder
    Inherits OneToManyRealBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return NutReadingBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(NutReadingBuilder._TableName) Then
                NutReadingBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(NutReadingNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = NutBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(NutNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = ReadingTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(ReadingTypeNub.Id)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As NutReadingBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As NutReadingBuilder
        If NutReadingBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                NutReadingBuilder._Instance = New NutReadingBuilder()
            End SyncLock
        End If
        Return NutReadingBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="NutReadingEntity"/> <see cref="IOneToManyReal">interface</see>.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("NutReading")>
Public Class NutReadingNub
    Inherits OneToManyRealNub
    Implements IOneToManyReal

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToManyReal
        Return New NutReadingNub
    End Function

End Class

''' <summary> The <see cref="NutReadingEntity"/> stores Real(Double)-value  readings. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class NutReadingEntity
    Inherits EntityBase(Of IOneToManyReal, NutReadingNub)
    Implements IOneToManyReal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New NutReadingNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> the <see cref="Entities.NutEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IOneToManyReal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToManyReal, ByVal store As IOneToManyReal)
        MyBase.New(New NutReadingNub, cache, store)
        Me.UsingNativeTracking = String.Equals(NutReadingBuilder.TableName, NameOf(IOneToManyReal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToManyReal
        Return New NutReadingNub
    End Function

    Public Overrides Function CreateCopy() As IOneToManyReal
        Dim destination As IOneToManyReal = Me.CreateNew
        NutReadingNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="NutReadingEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToManyReal)
        NutReadingNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> the <see cref="Entities.NutEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToManyReal)
        ' first make the copy to notify of any property change.
        NutReadingNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="readingtypeId"> The identifier of the <see cref="Entities.NutEntity"/> value type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal readingtypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NutReadingBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(NutReadingNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = nutAutoId})
        sqlBuilder.Where($"{NameOf(NutReadingNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = readingtypeId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of NutReadingNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/15/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="readingTypeId"> The identifier of the <see cref="Entities.NutEntity"/> value type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal readingTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As NutReadingNub = NutReadingEntity.FetchEntities(connection, nutAutoId, readingTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToManyReal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If NutReadingEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="readingTypeId"> Type of the <see cref="Entities.NutEntity"/> value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal readingTypeId As Integer) As Boolean
        Return connection.Delete(Of NutReadingNub)(New NutReadingNub With {.PrimaryId = nutAutoId, .SecondaryId = readingTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="Entities.NutEntity"/> Reading Real(Double)-Value entities. </summary>
    ''' <value> the <see cref="Entities.NutEntity"/> Reading Real(Double)-Value entities. </value>
    Public ReadOnly Property NutReadings As IEnumerable(Of NutReadingEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of NutReadingEntity)
        Return If(usingNativeTracking, NutReadingEntity.Populate(connection.GetAll(Of IOneToManyReal)),
                                       NutReadingEntity.Populate(connection.GetAll(Of NutReadingNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._NutReadings = NutReadingEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(NutReadingEntity.NutReadings))
        Return If(Me.NutReadings?.Any, Me.NutReadings.Count, 0)
    End Function

    ''' <summary> Count Nut Readings. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{NutReadingBuilder.TableName}]
WHERE {NameOf(NutReadingNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = nutAutoId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As IEnumerable(Of NutReadingEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{NutReadingBuilder.TableName}]
WHERE {NameOf(NutReadingNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = nutAutoId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return NutReadingEntity.Populate(connection.Query(Of NutReadingNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Fetches Nut Readings by Nut Auto Id.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As IEnumerable(Of NutReadingNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NutReadingBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(NutReadingEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = nutAutoId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NutReadingNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of <see cref="NutReadingEntity"/>'s. </summary>
    ''' <param name="nubs"> the <see cref="Entities.NutEntity"/>Value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of NutReadingNub)) As IEnumerable(Of NutReadingEntity)
        Dim l As New List(Of NutReadingEntity)
        If nubs?.Any Then
            For Each nub As NutReadingNub In nubs
                l.Add(New NutReadingEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of <see cref="NutReadingEntity"/>'s. </summary>
    ''' <param name="interfaces"> the <see cref="Entities.NutEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToManyReal)) As IEnumerable(Of NutReadingEntity)
        Dim l As New List(Of NutReadingEntity)
        If interfaces?.Any Then
            Dim nub As New NutReadingNub
            For Each iFace As IOneToManyReal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New NutReadingEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Nut Readings; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="readingTypeId"> Type of the <see cref="Entities.NutEntity"/> value. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal readingTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{NutReadingBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(NutReadingNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = nutAutoId})
        sqlBuilder.Where($"{NameOf(NutReadingNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = readingTypeId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Nut Readings by unique Index; expected single or none.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     The identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="readingTypeId"> The identifier of the <see cref="Entities.NutEntity"/> value type. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal readingTypeId As Integer) As IEnumerable(Of NutReadingNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NutReadingBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(NutReadingNub.PrimaryId)} = @primaryId", New With {.primaryId = nutAutoId})
        sqlBuilder.Where($"{NameOf(NutReadingNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = readingTypeId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NutReadingNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.NutEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="nutAutoId">     Identifier of the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="readingTypeId"> The Session Suite Real(Double)-value type. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal readingTypeId As Integer) As Boolean
        Return 1 = NutReadingEntity.CountEntities(connection, nutAutoId, readingTypeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count all readings; expects 0 or 1.. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of specification values. </returns>
    Public Function CountNutReadings(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{NutReadingBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(NutReadingNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = Me.PrimaryId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches all Real(Double)-value Nut Readings associated with this entity; Should fetch up to 1. 
    ''' expected single or none.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">          The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchNutReadings(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of NutReadingNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NutReadingBuilder.TableName}] /**where**/")

        sqlBuilder.Where($"{NameOf(NutReadingNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = Me.PrimaryId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NutReadingNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.NutEntity"/> Entity. </summary>
    ''' <value> the <see cref="Entities.NutEntity"/> Entity. </value>
    Public ReadOnly Property NutEntity As NutEntity

    ''' <summary> Fetches a Nut entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchNutEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._NutEntity = New NutEntity()
        Return Me.NutEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.NutEntity"/> <see cref="ReadingTypeEntity"/>. </summary>
    ''' <value> the <see cref="Entities.NutEntity"/> value type entity. </value>
    Public ReadOnly Property ReadingTypeEntity As ReadingTypeEntity

    ''' <summary> Fetches a <see cref="ReadingTypeEntity"/> Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchReadingTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._ReadingTypeEntity = New ReadingTypeEntity()
        Return Me.ReadingTypeEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> The identifier of the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToManyReal.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(NutReadingEntity.NutAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NutEntity"/> record. </summary>
    ''' <value> The identifier of the <see cref="Entities.NutEntity"/> record. </value>
    Public Property NutAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToManyReal.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(NutReadingEntity.ReadingTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="ReadingTypeEntity"/>. </summary>
    ''' <value> The identifier of the <see cref="ReadingTypeEntity"/>. </value>
    Public Property ReadingTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets a Real(Double)-value assigned to the <see cref="Entities.NutEntity"/> for the specific <see cref="Entities.ReadingTypeEntity"/>. </summary>
    ''' <value> The Real(Double)-value assigned to the <see cref="Entities.NutEntity"/> for the specific <see cref="Entities.ReadingTypeEntity"/>. </value>
    Public Property Amount As Double Implements IOneToManyReal.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(ByVal value As Double)
            If Not Double.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class


''' <summary> Collection of <see cref="NutReadingEntity"/>'s. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class NutReadingEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToManyReal, NutReadingNub, NutReadingEntity)

    Protected Overrides Function GetKeyForItem(item As NutReadingEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As NutReadingEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all Nuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of NutReadingEntity))
        If entities?.Any Then
            For Each entity As NutReadingEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return NutReadingBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary> Collection <see cref="NutEntity"/>-Unique <see cref="NutReadingEntity"/>. </summary>
''' <remarks> David, 2020-05-05. </remarks>
''' 
Public Class NutUniqueReadingEntityCollection
    Inherits NutReadingEntityCollection
    Implements isr.Core.Constructs.IGetterSetter(Of Double)

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
        Me.NutReading = New NutReading With {.GetterSetter = Me}
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    Public Sub New(ByVal nutAutoId As Integer)
        Me.New
        Me.NutAutoId = nutAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As Dictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As Dictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value
    '''                       can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As NutReadingEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.ReadingTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.ReadingTypeId)
        Me.NotifyPropertyChanged(ReadingTypeEntity.EntityLookupDictionary(entity.ReadingTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all Nuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets the Real(Double)-value for the given <see cref="ReadingTypeEntity.Label"/>. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable Double. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double? Implements Core.Constructs.IGetterSetter(Of Double).Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary> Set the specified element value for the given <see cref="ReadingTypeEntity.Label"/>. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Double. </returns>
    Protected Function Setter(ByVal value As Double, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double Implements Core.Constructs.IGetterSetter(Of Double).Setter
        Me.Setter(Me.ToKey(name), value)
        Me.NotifyPropertyChanged(name)
        Return value
    End Function

    ''' <summary> Gets or sets the Nut reading. </summary>
    ''' <value> The Nut reading. </value>
    Public ReadOnly Property NutReading As NutReading

#End Region

#Region " ATTRIBUTE SELECTION "

    ''' <summary> Queries if collection contains 'readingType' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="readingTypeId"> Identifier for the <see cref="ReadingTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal readingTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(readingTypeId)
    End Function

    ''' <summary>
    ''' Converts a name to a key using the
    ''' <see cref="ReadingTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
    ''' extend the element nominal attributes beyond the values of the enumeration type that is used to
    ''' populate this table.
    ''' </summary>
    ''' <remarks> David, 5/24/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return ReadingTypeEntity.KeyLookupDictionary(name)
    End Function

    ''' <summary> Gets or sets the Reading value. </summary>
    ''' <value> The Reading value. Returns <see cref="Double.NaN"/> if value does not exist</value>
    Protected Property ReadingValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.Getter(Me.ToKey(name))
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.ReadingValue(name)) Then
                Me.Setter(Me.ToKey(name), value.Value)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="readingTypeId"> Identifier for the <see cref="ReadingTypeEntity"/>. </param>
    ''' <returns> An NutReadingRealEntity. </returns>
    Public Function Entity(ByVal readingTypeId As Integer) As NutReadingEntity
        Return If(Me.ContainsKey(readingTypeId), Me(Me._PrimaryKeyDictionary(readingTypeId)), New NutReadingEntity)
    End Function

    ''' <summary> Gets the Real(Double)-value for the given reading type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="readingTypeId"> Identifier for the <see cref="ReadingTypeEntity"/>. </param>
    ''' <returns> A Double? </returns>
    Public Function Getter(ByVal readingTypeId As Integer) As Double?
        Return If(Me.ContainsKey(readingTypeId), Me(Me._PrimaryKeyDictionary(readingTypeId)).Amount, New Double?)
    End Function

    ''' <summary> Set the specified element value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="readingTypeId"> Identifier for the <see cref="ReadingTypeEntity"/>. </param>
    ''' <param name="value">         The value. </param>
    Public Sub Setter(ByVal readingTypeId As Integer, ByVal value As Double)
        If Me.ContainsKey(readingTypeId) Then
            Me(Me._PrimaryKeyDictionary(readingTypeId)).Amount = value
        Else
            Me.Add(New NutReadingEntity With {.NutAutoId = Me.NutAutoId, .ReadingTypeId = readingTypeId, .Amount = value})
        End If
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="NutEntity"/>. </summary>
    ''' <value> The identifier of the <see cref="NutEntity"/>. </value>
    Public ReadOnly Property NutAutoId As Integer

#End Region

End Class

