Partial Public Class UutEntity

    ''' <summary> Populates the element <see cref="NutEntity"/> readings. </summary>
    ''' <remarks> David, 6/11/2020. </remarks>
    ''' <param name="multimeterId">  Identifier of the <see cref="MultimeterEntity"/>. </param>
    ''' <param name="nominalTypeId"> Identifier of the <see cref="NominalTypeEntity"/>. </param>
    ''' <returns> The number of <see cref="NutReading"/>'s in <see cref="Uutentity.NutReadings"/>. </returns>
    Public Function PopulateElementNutReadings(ByVal elements As ElementEntityCollection, ByVal multimeterId As Integer, ByVal nominalTypeId As Integer) As Integer
        Me.MultimeterId = multimeterId
        Me.MultimeterNumber = Dapper.Entities.MultimeterEntity.EntityLookupDictionary(multimeterId).MultimeterNumber
        Me.NutReadings.Clear()
        For Each nut As NutEntity In Me.Nuts
            Dim element As ElementEntity = elements(nut.ElementId)
            Me._NutReadings.Add(New NutReading With {.GetterSetter = nut.Readings, .MultimeterId = .MultimeterId,
                                                     .ElementType = CType(element.ElementTypeId, ElementType),
                                                     .ElementLabel = element.ElementLabel, .MultimeterNumber = Me.MultimeterNumber,
                                                     .NominalType = CType(nominalTypeId, NominalType)})
        Next
        Return If(Me.NutReadings?.Any, Me.NutReadings.Count, 0)
    End Function

    ''' <summary> Gets the element nominal readings. </summary>
    ''' <value> The element nominal readings. </value>
    Public ReadOnly Property NutReadings As New NutReadingBindingListView

End Class

