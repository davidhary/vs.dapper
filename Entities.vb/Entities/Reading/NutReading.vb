''' <summary> The Nut reading class holing the Nut reading values. </summary>
''' <remarks> David, 2020-05-29. </remarks>
Public Class NutReading
    Inherits isr.Core.Models.ViewModelBase

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter(Of Double)

    ''' <summary> Gets or sets the Reading value. </summary>
    ''' <value> The Reading value. </value>
    Protected Property ReadingValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.ReadingValue(name)) Then
                Me.GetterSetter.Setter(value.Value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

#End Region

#Region " ELEMENT PROPERTIES "

    Private _ElementLabel As String

    ''' <summary> Gets or sets the Label of the Element. </summary>
    ''' <value> The Label of the Element. </value>
    Public Property ElementLabel As String
        Get
            Return Me._ElementLabel
        End Get
        Set
            If Not String.Equals(Value, Me.ElementLabel) Then
                Me._ElementLabel = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _ElementType As ElementType

    ''' <summary> Gets or sets the type of the Element. </summary>
    ''' <value> The type of the Element. </value>
    Public Property ElementType As ElementType
        Get
            Return Me._ElementType
        End Get
        Set
            If Value <> Me.ElementType Then
                Me._ElementType = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _MultimeterId As Integer

    ''' <summary> Gets or sets the identifier of the multimeter. </summary>
    ''' <value> The identifier of the multimeter. </value>
    Public Property MultimeterId As Integer
        Get
            Return Me._MultimeterId
        End Get
        Set
            If Value <> Me.MultimeterId Then
                Me._MultimeterId = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _MultimeterNumber As Integer

    ''' <summary> Gets or sets the multimeter number. </summary>
    ''' <value> The multimeter number. </value>
    Public Property MultimeterNumber As Integer
        Get
            Return Me._MultimeterNumber
        End Get
        Set
            If Value <> Me.MultimeterNumber Then
                Me._MultimeterNumber = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _NominalType As NominalType

    ''' <summary> Gets or sets the type of the Nominal. </summary>
    ''' <value> The type of the Nominal. </value>
    Public Property NominalType As NominalType
        Get
            Return Me._NominalType
        End Get
        Set
            If Value <> Me.NominalType Then
                Me._NominalType = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " ATTRIBUTES "

    ''' <summary> Gets or sets the equation reading. </summary>
    ''' <value> The equation reading. </value>
    Public Property Equation As Double?
        Get
            Return Me.ReadingValue
        End Get
        Set(value As Double?)
            Me.ReadingValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the resistance. </summary>
    ''' <value> The resistance. </value>
    Public Property Resistance As Double?
        Get
            Return Me.ReadingValue
        End Get
        Set(value As Double?)
            Me.ReadingValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the voltage. </summary>
    ''' <value> The voltage. </value>
    Public Property Voltage As Double?
        Get
            Return Me.ReadingValue
        End Get
        Set(value As Double?)
            Me.ReadingValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the current. </summary>
    ''' <value> The current. </value>
    Public Property Current As Double?
        Get
            Return Me.ReadingValue
        End Get
        Set(value As Double?)
            Me.ReadingValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the status. </summary>
    ''' <value> The status. </value>
    Public Property Status As Integer?
        Get
            Return CType(Me.ReadingValue, Integer?)
        End Get
        Set(value As Integer?)
            Me.ReadingValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the Bin Number. </summary>
    ''' <value> The Bin Number. </value>
    Public Property BinNumber As Integer?
        Get
            Return CType(Me.ReadingValue, Integer?)
        End Get
        Set(value As Integer?)
            Me.ReadingValue = value
        End Set
    End Property

#End Region


End Class

''' <summary> An Nut reading binding list view. </summary>
''' <remarks> David, 6/12/2020. </remarks>
Public Class NutReadingBindingListView
    Inherits isr.Core.Services.AggregateBindingListView(Of NutReading)

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 6/11/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me.Sort = $"{NameOf(NutReading.ElementLabel)} ASC"
    End Sub

    ''' <summary> Adds value. </summary>
    ''' <remarks> David, 6/11/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value to add. </param>
    Public Sub Add(ByVal value As NutReading)
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Me.SourceLists.Add(New List(Of NutReading) From {value})
    End Sub

    ''' <summary> Removes all items from the <see cref="T:System.Collections.IList" />. </summary>
    ''' <remarks> David, 6/11/2020. </remarks>
    Public Sub Clear()
        Me.SourceLists.Clear()
    End Sub

    ''' <summary> True if has any values.  </summary>
    ''' <remarks> David, 6/11/2020. </remarks>
    Public Function Any() As Boolean
        Return Me.Count > 0
    End Function

End Class

