Imports Dapper

Partial Public Class NutEntity

    ''' <summary> Gets or sets the <see cref="IEnumerable(Of NutReadingEntity)">Nut reading entities</see>. </summary>
    ''' <value> The Nut reading values. </value>
    Public ReadOnly Property ReadingEntities As IEnumerable(Of NutReadingEntity)

    ''' <summary>
    ''' Fetches the <see cref="IEnumerable(Of NutReadingEntity)">Nut reading entities</see>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of reading real entities. </returns>
    Public Function FetchReadingEntities(ByVal connection As System.Data.IDbConnection) As Integer
        If Not ReadingTypeEntity.IsEnumerated() Then ReadingTypeEntity.TryFetchAll(connection)
        Return Me.Populate(NutReadingEntity.FetchEntities(connection, Me.AutoId))
    End Function

    ''' <summary>
    ''' Fetches the <see cref="IEnumerable(Of NutReadingEntity)">Nut reading entities</see>.
    ''' </summary>
    ''' <remarks>
    ''' https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
    ''' https://dapper-tutorial.net/querymultiple.
    ''' </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  The Identifier of the <see cref="NutEntity"/>. </param>
    ''' <returns> The number of reading real entities. </returns>
    Public Function FetchReadingEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Integer
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select * from [{NutBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(NutNub.AutoId)} = @Id; ")
        queryBuilder.AppendLine($"select * from [{NutReadingBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(NutReadingNub.PrimaryId)} = @Id; ")
        Dim gridReader As SqlMapper.GridReader = connection.QueryMultiple(queryBuilder.ToString, New With {.Id = nutAutoId})
        Dim entity As NutNub = gridReader.ReadSingle(Of NutNub)()
        If entity Is Nothing Then
            Return Me.Populate(New List(Of NutReadingEntity))
        Else
            If Not NutNub.AreEqual(entity, Me.ICache) Then
                Me.UpdateCache(entity)
                Me.UpdateStore()
            End If
            If Not ReadingTypeEntity.IsEnumerated() Then ReadingTypeEntity.TryFetchAll(connection)
            Return Me.Populate(NutReadingEntity.Populate(gridReader.Read(Of NutReadingNub)))
        End If
    End Function

    ''' <summary> Gets or sets the <see cref="NutUniqueReadingEntityCollection">Nut reading Real(Double)-values</see>. </summary>
    ''' <value> The Nut Real(Double)-value Readings. </value>
    Public ReadOnly Property Readings As NutUniqueReadingEntityCollection

    ''' <summary>
    ''' Fetches <see cref="NutUniqueReadingEntityCollection">Nut reading Real(Double)-values</see>.
    ''' </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of reading reals. </returns>
    Public Function FetchReadings(ByVal connection As System.Data.IDbConnection) As Integer
        If Not ReadingTypeEntity.IsEnumerated() Then ReadingTypeEntity.TryFetchAll(connection)
        Return Me.Populate(NutReadingEntity.FetchEntities(connection, Me.AutoId))
    End Function

    ''' <summary>
    ''' Fetches <see cref="NutUniqueReadingEntityCollection">Nut reading Real(Double)-values</see>.
    ''' </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifier of the <see cref="NutEntity"/>. </param>
    ''' <returns> The reading reals. </returns>
    Public Shared Function FetchReadings(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As NutUniqueReadingEntityCollection
        If Not ReadingTypeEntity.IsEnumerated() Then ReadingTypeEntity.TryFetchAll(connection)
        Dim Values As New NutUniqueReadingEntityCollection(nutAutoId)
        Values.Populate(NutReadingEntity.FetchEntities(connection, nutAutoId))
        Return Values
    End Function

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    ''' <returns> The number of entities. </returns>
    Private Overloads Function Populate(ByVal entities As IEnumerable(Of NutReadingEntity)) As Integer
        Me._ReadingEntities = entities
        If Me._Readings Is Nothing Then
            Me._Readings = New NutUniqueReadingEntityCollection(Me.AutoId)
        Else
            Me._Readings.Clear()
        End If
        Me.Readings.Populate(entities)
        Me.NotifyPropertyChanged(NameOf(NutEntity.ReadingEntities))
        Me.NotifyPropertyChanged(NameOf(NutEntity.Readings))
        Return (If(entities?.Any, entities.Count, 0))
    End Function

    ''' <summary> Select reading for the specified <see cref="NominalType"/>. </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="nominalType"> Type of the nominal. </param>
    ''' <returns> A Double? </returns>
    Public Function SelectReading(ByVal nominalType As NominalType) As Double?
        Select Case nominalType
            Case NominalType.Equation
                Return Me.Readings.NutReading.Equation
            Case NominalType.Resistance
                Return Me.Readings.NutReading.Resistance
            Case Else
                Throw New ArgumentException($"Unhandled {NameOf(Dapper.Entities.NominalType)} of {nominalType}")
        End Select
    End Function

End Class

