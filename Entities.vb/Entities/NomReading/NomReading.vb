''' <summary> The Nominal reading class holing the Nominal reading values. </summary>
''' <remarks> David, 2020-05-29. </remarks>
Public Class NomReading
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSetter"> The getter setter. </param>
    ''' <param name="element">      The <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifier for the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="uut">          The <see cref="Entities.UutEntity"/>. </param>
    Public Sub New(ByVal getterSetter As isr.Core.Constructs.IGetterSetter(Of Double), ByVal element As ElementEntity, ByVal nomTypeId As Integer,
                   ByVal uut As UutEntity)
        MyBase.New()
        Me.GetterSetter = getterSetter
        Me.ElementLabel = element.ElementLabel
        Me.ElementType = CType(element.ElementTypeId, ElementType)
        Me.ElementOrdinalNumber = element.OrdinalNumber
        Me.NomType = CType(nomTypeId, NomType)
        Me.UutNumber = uut.UutNumber
        Me.MultimeterId = uut.Traits.UutTrait.MultimeterId
        Me.ElementAutoId = element.AutoId
        Dim trait As NomTrait = element.NomTraitsCollection(New MeterNomTypeSelector(Me.MultimeterId, nomTypeId)).NomTrait
        Me.NominalValue = trait.NominalValue
        Me.GuardedSpecificationRange = trait.GuardedSpecificationRange
        Me.OverflowRange = trait.OverflowRange
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter(Of Double)

    ''' <summary> Gets the Nom Reading value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Double. </returns>
    Public Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Return Me.GetterSetter.Getter(name)
    End Function

    ''' <summary> Sets the Nom Reading value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Double. </returns>
    Public Function Setter(ByVal value As Double?, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        If value.HasValue AndAlso Not Nullable.Equals(value, Me.Getter(name)) Then
            Me.GetterSetter.Setter(value.Value, name)
            Me.NotifyPropertyChanged(name)
        End If
        Return value
    End Function

#If False Then
    ''' <summary> Gets or sets the nom reading value. </summary>
    ''' <value> The nom reading value. </value>
    Protected Property NomReadingValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.NomReadingValue(name)) Then
                Me.GetterSetter.Setter(value.Value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property
#End If

#End Region

#Region " IDENTIFIERS "

    ''' <summary> Identifier for the element. </summary>
    Private _ElementId As Integer

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public Property ElementAutoId As Integer
        Get
            Return Me._ElementId
        End Get
        Set
            If Value <> Me.ElementAutoId Then
                Me._ElementId = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Identifier for the multimeter. </summary>
    Private _MultimeterId As Integer

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/>.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer
        Get
            Return Me._MultimeterId
        End Get
        Set
            If Value <> Me.MultimeterId Then
                Me._MultimeterId = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Type of the nom. </summary>
    Private _NomType As NomType

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NomTypeEntity"/>. </value>
    Public Property NomType As NomType
        Get
            Return Me._NomType
        End Get
        Set
            If Value <> Me.NomType Then
                Me._NomType = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " DERIVED TRAITS "

    ''' <summary> The multimeter. </summary>
    Private _Multimeter As MultimeterEntity

    ''' <summary> Gets the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <value> The <see cref="Entities.MultimeterEntity"/>. </value>
    Public ReadOnly Property Multimeter As MultimeterEntity
        Get
            If Not (Me._Multimeter?.IsClean()).GetValueOrDefault(False) Then
                Me._Multimeter = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
            End If
            Return Me._Multimeter
        End Get
    End Property

    ''' <summary> The multimeter model. </summary>
    Private _MultimeterModel As MultimeterModelEntity

    ''' <summary> Gets the Meter Model entity. </summary>
    ''' <value> The Meter Model entity. </value>
    Public ReadOnly Property MultimeterModel As MultimeterModelEntity
        Get
            If Not (Me._MultimeterModel?.IsClean()).GetValueOrDefault(False) Then
                Me._MultimeterModel = MultimeterModelEntity.EntityLookupDictionary(Me.Multimeter.MultimeterModelId)
            End If
            Return Me._MultimeterModel
        End Get
    End Property

#End Region

#Region " UUT and ELEMENT PROPERTIES "

    ''' <summary> The uut number. </summary>
    Private _UutNumber As Integer

    ''' <summary> Gets or sets the uut number. </summary>
    ''' <value> The uut number. </value>
    Public Property UutNumber As Integer
        Get
            Return Me._UutNumber
        End Get
        Set
            If Not Integer.Equals(Value, Me.UutNumber) Then
                Me._UutNumber = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The element label. </summary>
    Private _ElementLabel As String

    ''' <summary> Gets or sets the Label of the Element. </summary>
    ''' <value> The Label of the Element. </value>
    Public Property ElementLabel As String
        Get
            Return Me._ElementLabel
        End Get
        Set
            If Not String.Equals(Value, Me.ElementLabel) Then
                Me._ElementLabel = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Type of the element. </summary>
    Private _ElementType As ElementType

    ''' <summary> Gets or sets the type of the Element. </summary>
    ''' <value> The type of the Element. </value>
    Public Property ElementType As ElementType
        Get
            Return Me._ElementType
        End Get
        Set
            If Value <> Me.ElementType Then
                Me._ElementType = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The element ordinal number. </summary>
    Private _ElementOrdinalNumber As Integer

    ''' <summary> Gets or sets the Element Ordinal number. </summary>
    ''' <value> The ElementOrdinal number. </value>
    Public Property ElementOrdinalNumber As Integer
        Get
            Return Me._ElementOrdinalNumber
        End Get
        Set
            If Not Integer.Equals(Value, Me.ElementOrdinalNumber) Then
                Me._ElementOrdinalNumber = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " NOMINAL VALUES "

    ''' <summary> The nominal value. </summary>
    Private _NominalValue As Double?

    ''' <summary> Gets or sets the Nominal Value. </summary>
    ''' <value> The Nominal Value. </value>
    Public Property NominalValue As Double?
        Get
            Return Me._NominalValue
        End Get
        Set(value As Double?)
            If Not Nullable.Equals(value, Me._NominalValue) Then
                Me._NominalValue = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The overflow range. </summary>
    Private _OverflowRange As isr.Core.Constructs.RangeR

    ''' <summary> Gets or sets the overflow range. </summary>
    ''' <value> The overflow range. </value>
    Public Property OverflowRange As isr.Core.Constructs.RangeR
        Get
            Return Me._OverflowRange
        End Get
        Set(value As isr.Core.Constructs.RangeR)
            If value <> Me.OverflowRange Then
                Me._OverflowRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The guarded specification range. </summary>
    Private _GuardedSpecificationRange As isr.Core.Constructs.RangeR

    ''' <summary> Gets or sets the Guarded Specification range. </summary>
    ''' <value> The Guarded Specification range. </value>
    Public Property GuardedSpecificationRange As isr.Core.Constructs.RangeR
        Get
            Return Me._GuardedSpecificationRange
        End Get
        Set(value As isr.Core.Constructs.RangeR)
            If value <> Me.GuardedSpecificationRange Then
                Me._GuardedSpecificationRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " READING VALUES "

    ''' <summary> Gets or sets the reading. </summary>
    ''' <value> The reading. </value>
    Public Property Reading As Double?
        Get
            Return Me.Getter
        End Get
        Set(value As Double?)
            Me.Setter(value)
            Me._Delta = Me.EstimateDelta
        End Set
    End Property

    ''' <summary> Gets or sets the status. </summary>
    ''' <value> The status. </value>
    Public Property Status As Integer?
        Get
            Return CType(Me.Getter, Integer?)
        End Get
        Set(value As Integer?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Bin Number. </summary>
    ''' <value> The Bin Number. </value>
    Public Property BinNumber As Integer?
        Get
            Return CType(Me.Getter, Integer?)
        End Get
        Set(value As Integer?)
            Me.Setter(value)
        End Set
    End Property

#End Region

#Region " DERIVED VALUES "

    ''' <summary> Estimate delta. </summary>
    ''' <remarks> David, 6/30/2020. </remarks>
    ''' <returns> A Double? </returns>
    Private Function EstimateDelta() As Double?
        Dim readingValue As Double? = Me.Reading
        Return If(readingValue.HasValue AndAlso Me.NominalValue.HasValue AndAlso Me.NominalValue <> 0,
                            (readingValue / Me.NominalValue - 1),
                            New Double?)
    End Function

    ''' <summary> The delta. </summary>
    Private _Delta As Double?

    ''' <summary> Gets the delta. </summary>
    ''' <value> The delta. </value>
    Public ReadOnly Property Delta As Double?
        Get
            If Not Me._Delta.HasValue Then
                Me._Delta = Me.EstimateDelta
            End If
            Return Me._Delta
        End Get
    End Property

#End Region

End Class

