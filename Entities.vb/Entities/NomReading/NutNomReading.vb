Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Nut Nominal Reading Real-value builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class NutNomReadingBuilder
    Inherits OneToManyRealBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return NutNomReadingBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(NutNomReadingBuilder._TableName) Then
                NutNomReadingBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(NutNomReadingNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = NutBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(NutNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = NomReadingTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(NomReadingTypeNub.Id)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(NutNomReadingEntity.NutAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(NutNomReadingEntity.NomReadingTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName As String = NameOf(NutNomReadingEntity.Amount)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As NutNomReadingBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As NutNomReadingBuilder
        If NutNomReadingBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                NutNomReadingBuilder._Instance = New NutNomReadingBuilder()
            End SyncLock
        End If
        Return NutNomReadingBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="NutNomReadingEntity"/> <see cref="IOneToManyReal">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("NutNomReading")>
Public Class NutNomReadingNub
    Inherits OneToManyRealNub
    Implements IOneToManyReal

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToManyReal
        Return New NutNomReadingNub
    End Function

End Class

''' <summary>
''' The <see cref="NutNomReadingEntity"/> stores Real(Double)-value nominal Readings.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class NutNomReadingEntity
    Inherits EntityBase(Of IOneToManyReal, NutNomReadingNub)
    Implements IOneToManyReal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New NutNomReadingNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.NutEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IOneToManyReal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToManyReal, ByVal store As IOneToManyReal)
        MyBase.New(New NutNomReadingNub, cache, store)
        Me.UsingNativeTracking = String.Equals(NutNomReadingBuilder.TableName, NameOf(IOneToManyReal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToManyReal
        Return New NutNomReadingNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOneToManyReal
        Dim destination As IOneToManyReal = Me.CreateNew
        NutNomReadingNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="NutNomReadingEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToManyReal)
        NutNomReadingNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.NutEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToManyReal)
        ' first make the copy to notify of any property change.
        NutNomReadingNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="nutAutoId">        Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="nomReadingTypeId"> Identifies the <see cref="Entities.NutEntity"/> value type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal nomReadingTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NutNomReadingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NutNomReadingNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = nutAutoId})
        sqlBuilder.Where($"{NameOf(NutNomReadingNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = nomReadingTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of NutNomReadingNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/15/2020. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="nutAutoId">        Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="nomReadingTypeId"> Identifies the <see cref="Entities.NutEntity"/> value type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal nomReadingTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As NutNomReadingNub = NutNomReadingEntity.FetchEntities(connection, nutAutoId, nomReadingTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToManyReal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If NutNomReadingEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="nutAutoId">        Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="nomReadingTypeId"> Type of the <see cref="Entities.NutEntity"/> value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal nomReadingTypeId As Integer) As Boolean
        Return connection.Delete(Of NutNomReadingNub)(New NutNomReadingNub With {.PrimaryId = nutAutoId, .SecondaryId = nomReadingTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary>
    ''' Gets or sets the <see cref="Entities.NutEntity"/> Nominal Reading Real(Double)-Value entities.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value>
    ''' the <see cref="Entities.NutEntity"/> Nominal Reading Real(Double)-Value entities.
    ''' </value>
    Public ReadOnly Property NutNomReadings As IEnumerable(Of NutNomReadingEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of NutNomReadingEntity)
        Return If(usingNativeTracking, NutNomReadingEntity.Populate(connection.GetAll(Of IOneToManyReal)),
                                       NutNomReadingEntity.Populate(connection.GetAll(Of NutNomReadingNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._NutNomReadings = NutNomReadingEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(NutNomReadingEntity.NutNomReadings))
        Return If(Me.NutNomReadings?.Any, Me.NutNomReadings.Count, 0)
    End Function

    ''' <summary> Count Nut <see cref="NutNomReadingEntity"/>'s. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()

        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{NutNomReadingBuilder.TableName}]
WHERE {NameOf(NutNomReadingNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = nutAutoId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As IEnumerable(Of NutNomReadingEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{NutNomReadingBuilder.TableName}] WHERE {NameOf(NutNomReadingNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return NutNomReadingEntity.Populate(connection.Query(Of NutNomReadingNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches Nut <see cref="NutNomReadingEntity"/>'s by Nut Auto Id. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As IEnumerable(Of NutNomReadingNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NutNomReadingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NutNomReadingEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NutNomReadingNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of <see cref="NutNomReadingEntity"/>'s. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> the <see cref="Entities.NutEntity"/>Value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of NutNomReadingNub)) As IEnumerable(Of NutNomReadingEntity)
        Dim l As New List(Of NutNomReadingEntity)
        If nubs?.Any Then
            For Each nub As NutNomReadingNub In nubs
                l.Add(New NutNomReadingEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of <see cref="NutNomReadingEntity"/>'s. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> the <see cref="Entities.NutEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToManyReal)) As IEnumerable(Of NutNomReadingEntity)
        Dim l As New List(Of NutNomReadingEntity)
        If interfaces?.Any Then
            Dim nub As New NutNomReadingNub
            For Each iFace As IOneToManyReal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New NutNomReadingEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Nut <see cref="NutNomReadingEntity"/>'s; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="nutAutoId">        Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="nomReadingTypeId"> Type of the <see cref="Entities.NutEntity"/> value. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal nomReadingTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{NutNomReadingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NutNomReadingNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = nutAutoId})
        sqlBuilder.Where($"{NameOf(NutNomReadingNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = nomReadingTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches <see cref="NutNomReadingEntity"/>'s by unique Index; expected single or none.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="nutAutoId">        Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="nomReadingTypeId"> Identifies the <see cref="Entities.NutEntity"/> value type. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal nomReadingTypeId As Integer) As IEnumerable(Of NutNomReadingNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NutNomReadingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NutNomReadingNub.PrimaryId)} = @primaryId", New With {.primaryId = nutAutoId})
        sqlBuilder.Where($"{NameOf(NutNomReadingNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = nomReadingTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NutNomReadingNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.NutEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="nutAutoId">        Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="nomReadingTypeId"> The Session Suite Real(Double)-value type. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal nomReadingTypeId As Integer) As Boolean
        Return 1 = NutNomReadingEntity.CountEntities(connection, nutAutoId, nomReadingTypeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count all <see cref="NutNomReadingEntity"/>'s; expects 0 or 1. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of specification values. </returns>
    Public Function CountNutNomReadings(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{NutNomReadingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NutNomReadingNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches all Real(Double)-value Nut <see cref="NutNomReadingEntity"/>'s associated with this
    ''' entity; Should fetch up to 1. expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchNutNomReadings(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of NutNomReadingNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NutNomReadingBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NutNomReadingNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NutNomReadingNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.NutEntity"/> Entity. </summary>
    ''' <value> the <see cref="Entities.NutEntity"/> Entity. </value>
    Public ReadOnly Property NutEntity As NutEntity

    ''' <summary> Fetches a Nut entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchNutEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._NutEntity = New NutEntity()
        Return Me.NutEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

    ''' <summary>
    ''' Gets or sets the <see cref="Entities.NutEntity"/> <see cref="NomReadingTypeEntity"/>.
    ''' </summary>
    ''' <value> the <see cref="Entities.NutEntity"/> value type entity. </value>
    Public ReadOnly Property NomReadingTypeEntity As NomReadingTypeEntity

    ''' <summary> Fetches a <see cref="NomReadingTypeEntity"/> Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchNomReadingTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._NomReadingTypeEntity = New NomReadingTypeEntity()
        Return Me.NomReadingTypeEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToManyReal.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(NutNomReadingEntity.NutAutoId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.NutEntity"/> record.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.NutEntity"/> record. </value>
    Public Property NutAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToManyReal.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(NutNomReadingEntity.NomReadingTypeId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.NomReadingTypeEntity"/>.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.NomReadingTypeEntity"/>. </value>
    Public Property NomReadingTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets a Real(Double)-value assigned to the <see cref="Entities.NutEntity"/> for the
    ''' specific <see cref="Entities.NomReadingTypeEntity"/>.
    ''' </summary>
    ''' <value>
    ''' The Real(Double)-value assigned to the <see cref="Entities.NutEntity"/> for the specific
    ''' <see cref="Entities.NomReadingTypeEntity"/>.
    ''' </value>
    Public Property Amount As Double Implements IOneToManyReal.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(ByVal value As Double)
            If Not Double.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> A nut nominal reading selector. </summary>
''' <remarks> David, 6/29/2020. </remarks>
Public Structure NutNomReadingSelector
    Implements IEquatable(Of NutNomReadingSelector)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="nutNomReading"> The <see cref="NutNomReadingEntity"/>. </param>
    Public Sub New(ByVal nutNomReading As NutNomReadingEntity)
        Me.New(nutNomReading.NutAutoId, nutNomReading.NomReadingTypeId)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="nutAutoId">        Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="nomReadingTypeId"> Identifies the <see cref="NomReadingTypeEntity"/>. </param>
    Public Sub New(ByVal nutAutoId As Integer, ByVal nomReadingTypeId As Integer)
        Me.NutAutoId = nutAutoId
        Me.NomReadingTypeId = nomReadingTypeId
    End Sub

    ''' <summary> Gets or sets the identifier of the nut automatic. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The identifier of the nut automatic. </value>
    Public Property NutAutoId As Integer

    ''' <summary> Gets or sets the identifier of the nom reading type. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The identifier of the nom reading type. </value>
    Public Property NomReadingTypeId As Integer

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="obj"> The object to compare with the current instance. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
    ''' represent the same value; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.Equals(CType(obj, NutNomReadingSelector))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Function Equals(other As NutNomReadingSelector) As Boolean Implements IEquatable(Of NutNomReadingSelector).Equals
        Return Me.NutAutoId = other.NutAutoId AndAlso Me.NomReadingTypeId = other.NomReadingTypeId
    End Function

    ''' <summary> Returns the hash code for this instance. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A 32-bit signed integer that is the hash code for this instance. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.NutAutoId Xor Me.NomReadingTypeId
    End Function

    ''' <summary> Cast that converts the given NutNomReadingSelector to a =. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(left As NutNomReadingSelector, right As NutNomReadingSelector) As Boolean
        Return left.Equals(right)
    End Operator

    ''' <summary> Cast that converts the given NutNomReadingSelector to a &lt;&gt; </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(left As NutNomReadingSelector, right As NutNomReadingSelector) As Boolean
        Return Not left = right
    End Operator

End Structure

''' <summary> Collection of <see cref="NutNomReadingEntity"/>'s. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class NutNomReadingEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToManyReal, NutNomReadingNub, NutNomReadingEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As NutNomReadingEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As NutNomReadingEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all Nuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of NutNomReadingEntity))
        If entities?.Any Then
            For Each entity As NutNomReadingEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return NutNomReadingBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary>
''' Collection <see cref="Entities.NutEntity"/>-Unique <see cref="NutNomReadingEntity"/>.
''' </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class NutUniqueNomReadingEntityCollection
    Inherits NutNomReadingEntityCollection
    Implements isr.Core.Constructs.IGetterSetter(Of Double)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="nutAutoId"> Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="element">   The <see cref="ElementEntity"/>. </param>
    ''' <param name="nomTypeId"> Identifier for the <see cref="NomTypeEntity"/>. </param>
    ''' <param name="uut">       The <see cref="UutEntity"/>. </param>
    Public Sub New(ByVal nutAutoId As Integer, ByVal element As ElementEntity, ByVal nomTypeId As Integer, ByVal uut As UutEntity)
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
        Me.NomReading = New NomReading(Me, element, nomTypeId, uut)
        Me.NutAutoId = nutAutoId
    End Sub


    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As NutNomReadingEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.NomReadingTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.NomReadingTypeId)
        Me.NotifyPropertyChanged(NomReadingTypeEntity.EntityLookupDictionary(entity.NomReadingTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all Nuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary>
    ''' Gets the Real(Double)-value for the given <see cref="NomReadingTypeEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable Double. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double? Implements Core.Constructs.IGetterSetter(Of Double).Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary>
    ''' Set the specified element value for the given <see cref="NomReadingTypeEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value">                                      value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Double. </returns>
    Protected Function Setter(ByVal value As Double, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double Implements Core.Constructs.IGetterSetter(Of Double).Setter
        Me.Setter(Me.ToKey(name), value)
        Me.NotifyPropertyChanged(name)
        Return value
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.NomReading"/>. </summary>
    ''' <value> The <see cref="Entities.NomReading"/>. </value>
    Public ReadOnly Property NomReading As NomReading

#End Region

#Region " TRAIT SELECTION "

    ''' <summary> Queries if collection contains 'NomReadingType' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="nomReadingTypeId"> Identifies the <see cref="NomReadingTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal nomReadingTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(nomReadingTypeId)
    End Function

    ''' <summary>
    ''' Converts a name to a key using the
    ''' <see cref="NomReadingTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
    ''' extend the element Nominal Traits beyond the values of the enumeration type that is used to
    ''' populate this table.
    ''' </summary>
    ''' <remarks> David, 5/24/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return NomReadingTypeEntity.KeyLookupDictionary(name)
    End Function

    ''' <summary> Sets the Nom Reading value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Double. </returns>
    Public Function Setter(ByVal value As Double?, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        If value.HasValue AndAlso Not Nullable.Equals(value, Me.Getter(name)) Then
            Me.Setter(Me.ToKey(name), value.Value)
            Me.NotifyPropertyChanged(name)
        End If
        Return value
    End Function

#If False Then
    ''' <summary> Gets or sets the Nominal Reading value. </summary>
    ''' <value>
    ''' The Nominal Reading value. Returns <see cref="Double.NaN"/> if value does not exist.
    ''' </value>
    Protected Property NomReadingValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.Getter(Me.ToKey(name))
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.NomReadingValue(name)) Then
                Me.Setter(Me.ToKey(name), value.Value)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property
#End If

    ''' <summary>
    ''' Gets the <see cref="NutNomReadingEntity"/> associated with the specified type.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="nomReadingTypeId"> Identifies the <see cref="NomReadingTypeEntity"/>. </param>
    ''' <returns> An <see cref="NutNomReadingEntity"/>. </returns>
    Public Function Entity(ByVal nomReadingTypeId As Integer) As NutNomReadingEntity
        Return If(Me.ContainsKey(nomReadingTypeId), Me(Me._PrimaryKeyDictionary(nomReadingTypeId)), New NutNomReadingEntity)
    End Function

    ''' <summary> Gets the Real(Double)-value for the given identity. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="nomReadingTypeId"> Identifies the <see cref="NomReadingTypeEntity"/>. </param>
    ''' <returns> A Double? </returns>
    Public Function Getter(ByVal nomReadingTypeId As Integer) As Double?
        Return If(Me.ContainsKey(nomReadingTypeId), Me(Me._PrimaryKeyDictionary(nomReadingTypeId)).Amount, New Double?)
    End Function

    ''' <summary> Set the specified element value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="nomReadingTypeId"> Identifies the <see cref="NomReadingTypeEntity"/>. </param>
    ''' <param name="value">            The value. </param>
    Public Sub Setter(ByVal nomReadingTypeId As Integer, ByVal value As Double)
        If Me.ContainsKey(nomReadingTypeId) Then
            Me(Me._PrimaryKeyDictionary(nomReadingTypeId)).Amount = value
        Else
            Me.Add(New NutNomReadingEntity With {.NutAutoId = Me.NutAutoId, .NomReadingTypeId = nomReadingTypeId, .Amount = value})
        End If
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NutEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NutEntity"/>. </value>
    Public ReadOnly Property NutAutoId As Integer

#End Region

End Class

