
Partial Public Class NutEntity

    ''' <summary>
    ''' Gets or sets the <see cref="NutUniqueNomReadingEntityCollection">Nut Nominal Reading
    ''' Real(Double)-values</see>.
    ''' </summary>
    ''' <value> The Nut Real(Double)-value Nominal Readings. </value>
    Public ReadOnly Property NomReadings As NutUniqueNomReadingEntityCollection

    ''' <summary> Initializes the nom readings. </summary>
    ''' <remarks> David, 7/2/2020. </remarks>
    ''' <param name="uut">     The <see cref="UutEntity"/>. </param>
    ''' <param name="element"> The <see cref="ElementEntity"/>. </param>
    Public Sub InitializeNomReadings(ByVal uut As UutEntity, ByVal element As ElementEntity)
        Me._NomReadings = New NutUniqueNomReadingEntityCollection(Me.AutoId, element, Me.NomTypeId, uut)
    End Sub

    ''' <summary>
    ''' Fetches <see cref="NutUniqueNomReadingEntityCollection">Nut Nominal Reading Real(Double)-
    ''' values</see>.
    ''' </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of Nominal Readings. </returns>
    Public Function FetchNomReadings(ByVal connection As System.Data.IDbConnection) As Integer
        If Not MultimeterEntity.IsEnumerated Then Entities.MultimeterEntity.TryFetchAll(connection)
        If Not NomReadingTypeEntity.IsEnumerated() Then NomReadingTypeEntity.TryFetchAll(connection)
        Me.NomReadings.Clear()
        Me.NomReadings.Populate(NutNomReadingEntity.FetchEntities(connection, Me.AutoId))
        Return Me.NomReadings.Count
    End Function

    ''' <summary>
    ''' Fetches <see cref="NutUniqueNomReadingEntityCollection">Nut Nominal Reading Real(Double)-
    ''' values</see>.
    ''' </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="element">    The <see cref="ElementEntity"/>. </param>
    ''' <param name="nomTypeId">  Identifier for the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="uut">        The <see cref="UutEntity"/>. </param>
    ''' <returns> The Nominal Readings. </returns>
    Public Shared Function FetchNomReadings(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer,
                                            ByVal element As ElementEntity, ByVal nomTypeId As Integer,
                                            ByVal uut As UutEntity) As NutUniqueNomReadingEntityCollection
        If Not NomReadingTypeEntity.IsEnumerated() Then NomReadingTypeEntity.TryFetchAll(connection)
        Dim Values As New NutUniqueNomReadingEntityCollection(nutAutoId, element, nomTypeId, uut)
        Dim entities As IEnumerable(Of NutNomReadingEntity) = NutNomReadingEntity.FetchEntities(connection, nutAutoId)
        Values.Populate(entities)
        Return Values
    End Function

End Class

