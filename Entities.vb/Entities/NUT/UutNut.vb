Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

Imports isr.Core

''' <summary> A Uut-Nut builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class UutNutBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return UutNutBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(UutNutBuilder._TableName) Then
                UutNutBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(UutNutNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = UutBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(UutNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = NutBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(NutNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(UutNutEntity.UutAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(UutNutEntity.NutAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As UutNutBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As UutNutBuilder
        If UutNutBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                UutNutBuilder._Instance = New UutNutBuilder()
            End SyncLock
        End If
        Return UutNutBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Uut+Nut Nub based on the <see cref="IOneToMany">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
<Table("UutNut")>
Public Class UutNutNub
    Inherits OneToManyNub
    Implements IOneToMany

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToMany
        Return New UutNutNub
    End Function

End Class

''' <summary> The Uut-Nut Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class UutNutEntity
    Inherits EntityBase(Of IOneToMany, UutNutNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New UutNutNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Uut+Nut interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New UutNutNub, cache, store)
        Me.UsingNativeTracking = String.Equals(UutNutBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToMany
        Return New UutNutNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        UutNutNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        UutNutNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Uut+Nut interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        UutNutNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal nutAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As UutNutNub = UutNutEntity.FetchNubs(connection, uutAutoId, nutAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.UutAutoId, Me.NutAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.UutAutoId, Me.NutAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal nutAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, uutAutoId, nutAutoId)
    End Function

    ''' <summary>
    ''' Tries to fetch an existing <see cref="Entities.NutEntity"/> associated with this
    ''' <see cref="Entities.UutEntity"/>; otherwise, inserts a new <see cref="Entities.NutEntity"/>
    ''' .Then
    ''' tries to fetch an existing or insert a new <see cref="UutNutEntity"/>.
    ''' </summary>
    ''' <remarks>
    ''' Assumes that a <see cref="Entities.UutEntity"/> exists for the specified
    ''' <paramref name="uutAutoId"/>
    ''' Using this method assumes the Nut has a non-unique <see cref="NutEntity.ElementAutoId"/>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="uutAutoId">    Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutElementId"> Identifies the <see cref="Entities.ElementEntity"/> owning this
    '''                             NUT. </param>
    ''' <param name="nomTypeId">    Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainNut(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer,
                                 ByVal nutElementId As Integer, ByVal nomTypeId As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._NutEntity = UutNutEntity.FetchNut(connection, uutAutoId, nutElementId, nomTypeId)
        If Not Me.NutEntity.IsClean Then
            ' if Nut already exists for this UUT, then this is to be used; otherwise, add a new Nut
            Me._NutEntity = New NutEntity With {.ElementAutoId = nutElementId, .NomTypeId = nomTypeId}
            If Not Me.NutEntity.Insert(connection) Then
                result = (False, $"Failed inserting {NameOf(Entities.NutEntity)} with {NameOf(Entities.NutEntity.ElementAutoId)} of {nutElementId}")
            End If
        End If
        If result.Success Then
            Me.UutAutoId = uutAutoId
            Me.NutAutoId = Me.NutEntity.AutoId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(UutNutEntity.UutEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.UutNutEntity)} for [{NameOf(Entities.UutEntity.AutoId)},{NameOf(Entities.NutEntity.AutoId)}] of [{uutAutoId},{Me.NutAutoId}]")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch an existing or insert a new <see cref="Entities.UutEntity"/> and
    ''' <see cref="Entities.NutEntity"/> entities and fetch an existing or insert a new
    ''' <see cref="UutNutEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="uutAutoId">    Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutElementId"> Identifies the <see cref="Entities.ElementEntity"/> owning this
    '''                             Nut. </param>
    ''' <param name="nomTypeId">    Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer,
                              ByVal nutElementId As Integer, ByVal nomTypeId As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.UutEntity Is Nothing OrElse Me.UutEntity.AutoId <> uutAutoId Then
            Me._UutEntity = New UutEntity With {.AutoId = uutAutoId}
            If Not Me._UutEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.UutEntity)} with {NameOf(Entities.UutEntity.AutoId)} of {uutAutoId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainNut(connection, uutAutoId, nutElementId, nomTypeId)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(UutNutEntity.UutEntity))
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch an existing <see cref="Entities.NutEntity"/> associated with this
    ''' <see cref="Entities.UutEntity"/>; otherwise, inserts a new <see cref="Entities.NutEntity"/>
    ''' .Then
    ''' tries to fetch an existing or insert a new <see cref="UutNutEntity"/>.
    ''' </summary>
    ''' <remarks>
    ''' Assumes that a <see cref="Entities.UutEntity"/> exists for the specified
    ''' <paramref name="uutAutoId"/>
    ''' Using this method assumes the Nut has a non-unique <see cref="NutEntity.ElementAutoId"/>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainNut(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer,
                                 ByVal nutAutoId As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._NutEntity = New NutEntity With {.AutoId = nutAutoId}
        If Not Me._NutEntity.FetchUsingKey(connection) Then
            result = (False, $"Failed fetching {NameOf(Entities.NutEntity)} with {NameOf(Entities.NutEntity.AutoId)} of {nutAutoId}")
        End If
        If result.Success Then
            Me.UutAutoId = Me.UutEntity.AutoId
            Me.NutAutoId = Me.NutEntity.AutoId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(UutNutEntity.UutEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.UutNutEntity)} for [{NameOf(Entities.UutEntity.AutoId)},{NameOf(Entities.NutEntity.AutoId)}] of [{uutAutoId},{Me.NutAutoId}]")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch an existing or insert a new <see cref="Entities.UutEntity"/> and
    ''' <see cref="Entities.NutEntity"/> entities and fetch an existing or insert a new
    ''' <see cref="UutNutEntity"/>.
    ''' </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal nutAutoId As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.UutEntity Is Nothing OrElse Me.UutEntity.AutoId <> uutAutoId Then
            Me._UutEntity = New UutEntity With {.AutoId = uutAutoId}
            If Not Me._UutEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.UutEntity)} with {NameOf(Entities.UutEntity.AutoId)} of {uutAutoId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainNut(connection, uutAutoId, nutAutoId)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(UutNutEntity.UutEntity))
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch an existing or insert a new <see cref="Entities.UutEntity"/> and
    ''' <see cref="Entities.NutEntity"/> entities and fetch an existing or insert a new
    ''' <see cref="UutNutEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String)
        Return Me.TryObtain(connection, Me.UutAutoId, Me.NutAutoId)
    End Function

    ''' <summary>
    ''' Fetches an existing or inserts new <see cref="Entities.UutEntity"/> and
    ''' <see cref="Entities.NutEntity"/> entities and fetches an existing or inserts a new
    ''' <see cref="UutNutEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="uutAutoId">    Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutElementId"> Identifies the <see cref="Entities.ElementEntity"/> owning this
    '''                             Nut. </param>
    ''' <param name="nomTypeId">    Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Function Obtain(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer,
                                     ByVal nutElementId As Integer, ByVal nomTypeId As Integer) As Boolean
        Dim r As (Success As Boolean, Details As String) = Me.TryObtain(connection, uutAutoId, nutElementId, nomTypeId)
        If Not r.Success Then Throw New OperationFailedException(r.Details)
        Return r.Success
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If UutNutEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal nutAutoId As Integer) As Boolean
        Return connection.Delete(Of UutNutNub)(New UutNutNub With {.PrimaryId = uutAutoId, .SecondaryId = nutAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Uut-Nut entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Uut-Nut entities. </value>
    Public ReadOnly Property UutNuts As IEnumerable(Of UutNutEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of UutNutEntity)
        Return If(usingNativeTracking, UutNutEntity.Populate(connection.GetAll(Of IOneToMany)), UutNutEntity.Populate(connection.GetAll(Of UutNutNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._UutNuts = UutNutEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(UutNutEntity.UutNuts))
        Return If(Me.UutNuts?.Any, Me.UutNuts.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of UutNutNub)) As IEnumerable(Of UutNutEntity)
        Dim l As New List(Of UutNutEntity)
        If nubs?.Any Then
            For Each nub As UutNutNub In nubs
                l.Add(New UutNutEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of UutNutEntity)
        Dim l As New List(Of UutNutEntity)
        If interfaces?.Any Then
            Dim nub As New UutNutNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New UutNutEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns up to Nut entities count. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{UutNutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(UutNutNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As IEnumerable(Of UutNutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{UutNutBuilder.TableName}] WHERE {NameOf(UutNutNub.PrimaryId)} = @Id", New With {Key .Id = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return UutNutEntity.Populate(connection.Query(Of UutNutNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Nut. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The total number of entities by Nut. </returns>
    Public Shared Function CountEntitiesByNut(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{UutNutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(UutNutNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Nuts in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Nuts in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByNut(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As IEnumerable(Of UutNutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{UutNutBuilder.TableName}] WHERE {NameOf(UutNutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return UutNutEntity.Populate(connection.Query(Of UutNutNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal nutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{UutNutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(UutNutNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = uutAutoId})
        sqlBuilder.Where($"{NameOf(UutNutNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal nutAutoId As Integer) As IEnumerable(Of UutNutNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{UutNutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(UutNutNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = uutAutoId})
        sqlBuilder.Where($"{NameOf(UutNutNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of UutNutNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Uut Nut exists. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal nutAutoId As Integer) As Boolean
        Return 1 = UutNutEntity.CountEntities(connection, uutAutoId, nutAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Uut entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Uut entity. </value>
    Public ReadOnly Property UutEntity As UutEntity

    ''' <summary> Fetches Uut Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Uut Entity. </returns>
    Public Function FetchUutEntity(ByVal connection As System.Data.IDbConnection) As UutEntity
        Dim entity As New UutEntity()
        entity.FetchUsingKey(connection, Me.UutAutoId)
        Me._UutEntity = entity
        Return entity
    End Function

    ''' <summary>
    ''' Count Uuts associated with the specified <paramref name="nutAutoId"/>; expected 1.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The total number of Uuts. </returns>
    Public Shared Function CountUuts(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{UutNutBuilder.TableName}] WHERE {NameOf(UutNutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Uuts associated with the specified <paramref name="nutAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Uuts in this collection.
    ''' </returns>
    Public Shared Function FetchUuts(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As IEnumerable(Of NutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{UutNutBuilder.TableName}] WHERE {NameOf(UutNutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of NutEntity)
        For Each nub As UutNutNub In connection.Query(Of UutNutNub)(selector.RawSql, selector.Parameters)
            Dim entity As New UutNutEntity(nub)
            l.Add(entity.FetchNutEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Uuts associated with the specified <paramref name="nutAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteUuts(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{UutNutBuilder.TableName}] WHERE {NameOf(UutNutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = nutAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the "Entities.NutEntity". </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> the "Entities.NutEntity". </value>
    Public ReadOnly Property NutEntity As NutEntity

    ''' <summary> Fetches a Nut entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> the "Entities.NutEntity". </returns>
    Public Function FetchNutEntity(ByVal connection As System.Data.IDbConnection) As NutEntity
        Dim entity As New NutEntity()
        entity.FetchUsingKey(connection, Me.NutAutoId)
        Me._NutEntity = entity
        Return entity
    End Function

    ''' <summary> Count nuts. </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> The total number of nuts. </returns>
    Public Shared Function CountNuts(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{UutNutBuilder.TableName}] WHERE {NameOf(UutNutNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Nuts in this collection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Nuts in this collection.
    ''' </returns>
    Public Shared Function FetchNuts(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As IEnumerable(Of NutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{UutNutBuilder.TableName}] WHERE {NameOf(UutNutNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of NutEntity)
        For Each nub As UutNutNub In connection.Query(Of UutNutNub)(selector.RawSql, selector.Parameters)
            Dim entity As New UutNutEntity(nub)
            l.Add(entity.FetchNutEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all <see cref="Entities.NutEntity"/>'s related to the specified Uut.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteNuts(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{UutNutBuilder.TableName}] WHERE {NameOf(UutNutNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = uutAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered Nuts in this collection. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="uutAutoId">   Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered Nuts in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedNuts(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal uutAutoId As Integer) As IEnumerable(Of NutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .Id = uutAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return NutEntity.Populate(connection.Query(Of NutNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered Nuts in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered Nuts in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedNuts(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As IEnumerable(Of NutEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [UutNut] on [UutNut].SecondaryId = [Nut].AutoId where [UutNut].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{NutBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{NutBuilder.TableName}] Inner Join [{UutNutBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{UutNutBuilder.TableName}].{NameOf(UutNutNub.SecondaryId)} = [{NutBuilder.TableName}].{NameOf(NutNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{UutNutBuilder.TableName}].{NameOf(UutNutNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{NutBuilder.TableName}].{NameOf(NutNub.FirstForeignId)} ASC, [{NutBuilder.TableName}].{NameOf(NutNub.SecondForeignId)} ASC; ")
        Return UutNutEntity.FetchOrderedNuts(connection, queryBuilder.ToString, uutAutoId)
    End Function

    ''' <summary> Fetches an <see cref="Entities.NutEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="selectQuery">  The select query. </param>
    ''' <param name="uutAutoId">    Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutElementId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> The Nut. </returns>
    Public Shared Function FetchNut(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal uutAutoId As Integer,
                                    ByVal nutElementId As Integer, ByVal nomTypeId As Integer) As NutEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString,
                                                                     New With {uutAutoId, nutElementId, nomTypeId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As NutNub = connection.Query(Of NutNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New NutEntity, New NutEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an <see cref="Entities.NutEntity"/>. </summary>
    ''' <remarks>
    ''' David, 5/19/2020.<code>
    ''' SELECT [Nut].* FROM [Nut] Inner Join [UutNut] ON [UutNut].SecondaryId = [Nut].AutoId WHERE
    ''' ([UutNut].PrimaryId = 7 AND [Nut].FirstForeignId = 13 AND [Nut].SecondForeignId = 1);</code>
    ''' </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="uutAutoId">    Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="nutElementId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> The Nut. </returns>
    Public Shared Function FetchNut(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer,
                                    ByVal nutElementId As Integer, ByVal nomTypeId As Integer) As NutEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [UutNut] on [UutNut].SecondaryId = [Nut].AutoId where [UutNut].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{NutBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{NutBuilder.TableName}] Inner Join [{UutNutBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{UutNutBuilder.TableName}].{NameOf(UutNutNub.SecondaryId)} = [{NutBuilder.TableName}].{NameOf(NutNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{UutNutBuilder.TableName}].{NameOf(UutNutNub.PrimaryId)} = @{NameOf(uutAutoId)} ")
        queryBuilder.AppendLine($"AND [{NutBuilder.TableName}].{NameOf(NutNub.FirstForeignId)} = @{NameOf(nutElementId)} ")
        queryBuilder.AppendLine($"AND [{NutBuilder.TableName}].{NameOf(NutNub.SecondForeignId)} = @{NameOf(nomTypeId)}); ")
        Return UutNutEntity.FetchNut(connection, queryBuilder.ToString, uutAutoId, nutElementId, nomTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(UutNutEntity.UutAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Uut. </summary>
    ''' <value> Identifies the Uut. </value>
    Public Property UutAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(UutNutEntity.NutAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NutEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NutEntity"/>. </value>
    Public Property NutAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
