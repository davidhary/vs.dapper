Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Nut builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class NutBuilder
    Inherits KeyTwoForeignTimeBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return NutBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(NutBuilder._TableName) Then
                NutBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(NutNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the first foreign reference table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the first foreign reference table. </value>
    Public Overrides Property FirstForeignTableName As String = ElementBuilder.TableName

    ''' <summary> Gets or sets the name of the first foreign reference table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the first foreign reference table key. </value>
    Public Overrides Property FirstForeignTableKeyName As String = NameOf(ElementNub.AutoId)

    ''' <summary> Gets or sets the name of the Second foreign reference table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Second foreign reference table. </value>
    Public Overrides Property SecondForeignTableName As String = NomTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the Second foreign reference table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Second foreign reference table key. </value>
    Public Overrides Property SecondForeignTableKeyName As String = NameOf(NomTypeNub.Id)

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The new table. </returns>
    Public Overloads Function CreateTable(connection As System.Data.IDbConnection) As String
        ' the same Element ID and Nominal Type would repeat for the NUT associated with the next UUT.
        Return MyBase.CreateTable(connection, UniqueIndexOptions.None)
    End Function

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(NutEntity.AutoId)

    ''' <summary> Gets or sets the name of the First foreign identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the First foreign identifier field. </value>
    Public Overrides Property FirstForeignIdFieldName() As String = NameOf(NutEntity.ElementAutoId)

    ''' <summary> Gets or sets the name of the second foreign identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the second foreign identifier field. </value>
    Public Overrides Property SecondForeignIdFieldName() As String = NameOf(NutEntity.NomTypeId)

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the timestamp field. </value>
    Public Overrides Property TimestampFieldName() As String = NameOf(NutEntity.Timestamp)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As NutBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As NutBuilder
        If NutBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                NutBuilder._Instance = New NutBuilder()
            End SyncLock
        End If
        Return NutBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Nut table based on the <see cref="IKeyTwoForeignTime">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("Nut")>
Public Class NutNub
    Inherits KeyTwoForeignTimeNub
    Implements IKeyTwoForeignTime

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IKeyTwoForeignTime
        Return New NutNub
    End Function

End Class

''' <summary> The "Entities.NutEntity". Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class NutEntity
    Inherits EntityBase(Of IKeyTwoForeignTime, NutNub)
    Implements IKeyTwoForeignTime

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New NutNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Nut interface. </param>
    Public Sub New(ByVal value As IKeyTwoForeignTime)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyTwoForeignTime, ByVal store As IKeyTwoForeignTime)
        MyBase.New(New NutNub, cache, store)
        Me.UsingNativeTracking = String.Equals(NutBuilder.TableName, NameOf(IKeyTwoForeignTime).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IKeyTwoForeignTime
        Return New NutNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IKeyTwoForeignTime
        Dim destination As IKeyTwoForeignTime = Me.CreateNew
        NutNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IKeyTwoForeignTime)
        NutNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Nut interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyTwoForeignTime)
        ' first make the copy to notify of any property change.
        NutNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Nut table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyTwoForeignTime)(key), connection.Get(Of NutNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.ElementAutoId, Me.NomTypeId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.NutEntity"/>
    '''                              <see cref="Entities.ElementEntity"/> . </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/> of the
    '''                                  <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As NutNub = NutEntity.FetchEntities(connection, elementAutoId, nomTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyTwoForeignTime) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If NutEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.FirstForeignId, entity.SecondForeignId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of NutNub)(New NutNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="Entities.NutEntity"/>'s. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="Entities.NutEntity"/>'s. </value>
    Public ReadOnly Property Nuts As IEnumerable(Of NutEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of NutEntity)
        Return If(usingNativeTracking, NutEntity.Populate(connection.GetAll(Of IKeyTwoForeignTime)), NutEntity.Populate(connection.GetAll(Of NutNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Nuts = NutEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(NutEntity.Nuts))
        Return If(Me.Nuts?.Any, Me.Nuts.Count, 0)
    End Function

    ''' <summary> Populates a list of Nut entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The Nut nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal nubs As IEnumerable(Of NutNub)) As IEnumerable(Of NutEntity)
        Dim l As New List(Of NutEntity)
        If nubs?.Any Then
            For Each nub As NutNub In nubs
                l.Add(New NutEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Nut entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The Nut interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyTwoForeignTime)) As IEnumerable(Of NutEntity)
        Dim l As New List(Of NutEntity)
        If interfaces?.Any Then
            Dim nub As New NutNub
            For Each iFace As IKeyTwoForeignTime In interfaces
                nub.CopyFrom(iFace)
                l.Add(New NutEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Nut entities by unique index; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.NutEntity"/>
    '''                              <see cref="Entities.ElementEntity"/> . </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/> of the
    '''                              <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{NutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NutNub.FirstForeignId)} = @firstForeignId", New With {.firstForeignId = elementAutoId})
        sqlBuilder.Where($"{NameOf(NutNub.SecondForeignId)} = @secondForeignId ", New With {.secondForeignId = nomTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches Nut entities by unique index; expected single or none. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.NutEntity"/>
    '''                              <see cref="Entities.ElementEntity"/> . </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/> of the
    '''                              <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As IEnumerable(Of NutNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NutNub.FirstForeignId)} = @firstForeignId", New With {.firstForeignId = elementAutoId})
        sqlBuilder.Where($"{NameOf(NutNub.SecondForeignId)} = @secondForeignId ", New With {.secondForeignId = nomTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NutNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.NutEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.NutEntity"/>
    '''                              <see cref="Entities.ElementEntity"/> . </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/> of the
    '''                              <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As Boolean
        Return 1 = NutEntity.CountEntities(connection, elementAutoId, nomTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NutEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NutEntity"/>. </value>
    Public Property AutoId As Integer Implements IKeyTwoForeignTime.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the first foreign key. </summary>
    ''' <value> Identifies the first foreign key. </value>
    Public Property FirstForeignId As Integer Implements IKeyTwoForeignTime.FirstForeignId
        Get
            Return Me.ICache.FirstForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.FirstForeignId, value) Then
                Me.ICache.FirstForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(NutEntity.ElementAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public Property ElementAutoId As Integer
        Get
            Return Me.FirstForeignId
        End Get
        Set(value As Integer)
            Me.FirstForeignId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the second foreign key. </summary>
    ''' <value> Identifies the Second foreign key. </value>
    Public Property SecondForeignId As Integer Implements IKeyTwoForeignTime.SecondForeignId
        Get
            Return Me.ICache.SecondForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondForeignId, value) Then
                Me.ICache.SecondForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(NutEntity.NomTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="NomTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NomTypeEntity"/>. </value>
    Public Property NomTypeId As Integer
        Get
            Return Me.SecondForeignId
        End Get
        Set(value As Integer)
            Me.SecondForeignId = value
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements IKeyTwoForeignTime.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the unique selector. </summary>
    ''' <value> The unique selector. </value>
    Public ReadOnly Property UniqueSelector As NutUniqueKey
        Get
            Return New NutUniqueKey(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of Nut entities. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class NutEntityCollection
    Inherits EntityKeyedCollection(Of Integer, IKeyTwoForeignTime, NutNub, NutEntity)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(ByVal item As NutEntity) As Integer
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.AutoId
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As NutEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of NutEntity))
        If entities?.Any Then
            For Each entity As NutEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Fetches nominal readings. </summary>
    ''' <remarks> David, 2020-06-27. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The nominal readings. </returns>
    Public Function FetchNomReadings(ByVal connection As System.Data.IDbConnection) As Integer
        Dim count As Integer = 0
        For Each nut As NutEntity In Me
            count += nut.FetchNomReadings(connection)
        Next
        Return count
    End Function

    ''' <summary> Initializes the nom readings. </summary>
    ''' <remarks> David, 7/2/2020. </remarks>
    ''' <param name="uut">      The <see cref="UutEntity"/>. </param>
    ''' <param name="elements"> The <see cref="ElementEntityCollection"/>. </param>
    Public Sub InitializeNomReadings(ByVal uut As UutEntity, ByVal elements As ElementEntityCollection)
        For Each nut As NutEntity In Me
            nut.InitializeNomReadings(uut, elements(nut.ElementAutoId))
        Next
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return NutBuilder.Get.Upsert(connection, Me)
    End Function


End Class

''' <summary> Collection of Nut entities uniquely associated with a UUT. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class UutUniqueNutEntityCollection
    Inherits NutEntityCollection

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of Integer, NutUniqueKey)
        Me._PrimaryKeyDictionary = New Dictionary(Of NutUniqueKey, Integer)
        Me._NutUniqueKeyReadingBinEntityDictionary = New Dictionary(Of NutUniqueKey, ReadingBinEntity)
        Me._NutIdentityReadingBinEntityDictionary = New Dictionary(Of Integer, ReadingBinEntity)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.NutEntity"/>. </param>
    Public Sub New(ByVal uutAutoId As Integer)
        Me.New
        Me.UutAutoId = uutAutoId
    End Sub

#End Region

#Region " NUT SELECTION "


    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of Integer, NutUniqueKey)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of NutUniqueKey, Integer)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As NutEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(New NutUniqueKey(entity), entity.AutoId)
        Me._UniqueIndexDictionary.Add(entity.AutoId, New NutUniqueKey(entity))
    End Sub

    ''' <summary>
    ''' Removes all Nuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Selects a <see cref="Entities.NutEntity"/> from the collection. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="elementAutoId"> Identifier for the <see cref="ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifier for the <see cref="NomTypeEntity"/>. </param>
    ''' <returns> An <see cref="Entities.NutEntity"/>. </returns>
    Public Function Nut(ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As NutEntity
        Dim id As Integer = Me._PrimaryKeyDictionary(New NutUniqueKey(elementAutoId, nomTypeId))
        Return If(Me.Contains(id), Me(id), New NutEntity)
    End Function

    ''' <summary>
    ''' Selects a <see cref="Entities.NutEntity"/> from the collection using the
    ''' <see cref="ElementEntity"/> ordinal number and the specified identified of the
    ''' <see cref="NutEntity"/> <see cref="NomTypeEntity"/>.
    ''' </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="elements">             The <see cref="ElementEntity"/>'s. </param>
    ''' <param name="elementOrdinalNumber"> The <see cref="ElementEntity"/> ordinal number. </param>
    ''' <param name="nomTypeId">            Identifier for the <see cref="NomTypeEntity"/>. </param>
    ''' <returns> An <see cref="Entities.NutEntity"/>. </returns>
    Public Function Nut(ByVal elements As ProductUniqueElementEntityCollection, ByVal elementOrdinalNumber As Integer, ByVal nomTypeId As Integer) As NutEntity
        Return Me.Nut(elements.Element(elementOrdinalNumber).AutoId, nomTypeId)
    End Function

    ''' <summary>
    ''' Selects a <see cref="Entities.NutEntity"/> from the collection using the
    ''' <see cref="ElementEntity"/> ordinal number and the default single (index 0)
    ''' <see cref="NomTypeEntity"/> of the <see cref="ElementEntity"/>.
    ''' </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="elements">             The <see cref="ElementEntity"/>'s. </param>
    ''' <param name="elementOrdinalNumber"> The <see cref="ElementEntity"/> ordinal number. </param>
    ''' <returns> An <see cref="Entities.NutEntity"/>. </returns>
    Public Function Nut(ByVal elements As ProductUniqueElementEntityCollection, ByVal elementOrdinalNumber As Integer) As NutEntity
        Dim element As ElementEntity = elements.Element(elementOrdinalNumber)
        Return Me.Nut(element.AutoId, element.NomTypes.First.Id)
    End Function

    ''' <summary> Nut element identifier. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="nutAutoId"> Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The element id or -1 if not found. </returns>
    Public Function NutElementId(ByVal nutAutoId As Integer) As Integer
        Return If(Me._UniqueIndexDictionary.ContainsKey(nutAutoId), Me._UniqueIndexDictionary(nutAutoId).ElementId, -1)
    End Function

    ''' <summary> Nut nominal type identifier. </summary>
    ''' <remarks> David, 6/14/2020. </remarks>
    ''' <param name="nutAutoId"> Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Function NutNomTypeId(ByVal nutAutoId As Integer) As Integer
        Return If(Me._UniqueIndexDictionary.ContainsKey(nutAutoId), Me._UniqueIndexDictionary(nutAutoId).NomTypeId, -1)
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NutEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NutEntity"/>. </value>
    Public ReadOnly Property UutAutoId As Integer

#End Region

#Region " BIN SELECTION "

    ''' <summary>
    ''' Gets or sets the <see cref="Entities.ReadingBinEntity"/>'s by the
    ''' <see cref="Entities.NutEntity"/> <see cref="NutEntity.AutoId"/>.
    ''' </summary>
    ''' <value> The reading bins. </value>
    Public ReadOnly Property NutIdentityReadingBinEntityDictionary As IDictionary(Of Integer, ReadingBinEntity)

    ''' <summary>
    ''' Gets or sets the <see cref="Entities.ReadingBinEntity"/>'s by the
    ''' <see cref="Entities.NutEntity"/> <see cref="NutEntity.UniqueSelector"/>.
    ''' </summary>
    ''' <value> The element reading bin identities. </value>
    Public ReadOnly Property NutUniqueKeyReadingBinEntityDictionary As IDictionary(Of NutUniqueKey, ReadingBinEntity)

    ''' <summary> Clears the reading bins. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    Public Sub ClearReadingBins()
        Me.NutUniqueKeyReadingBinEntityDictionary.Clear()
        Me.NutIdentityReadingBinEntityDictionary.Clear()
    End Sub

    ''' <summary> Adds a reading bin to 'readingBinEntity'. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="nut">        The "Entities.NutEntity". </param>
    ''' <param name="readingBin"> The "Entities.ReadingBinEntity". </param>
    Public Sub AddReadingBin(ByVal nut As NutEntity, ByVal readingBin As ReadingBinEntity)
        Me.NutIdentityReadingBinEntityDictionary.Add(nut.AutoId, readingBin)
        Me.NutUniqueKeyReadingBinEntityDictionary.Add(New NutUniqueKey(nut), readingBin)
    End Sub

    ''' <summary> Update or add a reading bin. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="nut">        The "Entities.NutEntity". </param>
    ''' <param name="readingBin"> The "Entities.ReadingBinEntity". </param>
    Public Sub UpaddReadingBin(ByVal nut As NutEntity, ByVal readingBin As ReadingBinEntity)
        If Me.NutIdentityReadingBinEntityDictionary.ContainsKey(nut.AutoId) Then
            Me.NutIdentityReadingBinEntityDictionary(nut.AutoId) = readingBin
        Else
            Me.NutIdentityReadingBinEntityDictionary.Add(nut.AutoId, readingBin)
        End If
        Dim key As NutUniqueKey = New NutUniqueKey(nut)
        If Me.NutUniqueKeyReadingBinEntityDictionary.ContainsKey(key) Then
            Me.NutUniqueKeyReadingBinEntityDictionary(key) = readingBin
        Else
            Me.NutUniqueKeyReadingBinEntityDictionary.Add(key, readingBin)
        End If
    End Sub

    ''' <summary> Populates a reading bins. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Function PopulateReadingBins(ByVal connection As System.Data.IDbConnection) As Integer
        Me.ClearReadingBins()
        For Each nutEntity As NutEntity In Me
            Me.AddReadingBin(nutEntity, NutReadingBinEntity.FetchReadingBin(connection, nutEntity.AutoId))
        Next
        Return Me.NutIdentityReadingBinEntityDictionary.Count
    End Function

#End Region

End Class

''' <summary> A nut unique key. </summary>
''' <remarks> David, 6/14/2020. </remarks>
Public Structure NutUniqueKey
    Implements IEquatable(Of NutUniqueKey)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nut"> The nut. </param>
    Public Sub New(ByVal nut As NutEntity)
        Me.New(nut.ElementAutoId, nut.NomTypeId)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="elementAutoId"> Identifier for the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifier for the <see cref="Entities.NomTypeEntity"/>. </param>
    Public Sub New(ByVal elementAutoId As Integer, ByVal nomTypeId As Integer)
        Me.ElementId = elementAutoId
        Me.NomTypeId = nomTypeId
    End Sub

    ''' <summary> Gets or sets the identifier of the element. </summary>
    ''' <value> The identifier of the element. </value>
    Public Property ElementId As Integer

    ''' <summary> Gets or sets the identifier of the nom type. </summary>
    ''' <value> The identifier of the nom type. </value>
    Public Property NomTypeId As Integer

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="obj"> The object to compare with the current instance. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
    ''' represent the same value; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.Equals(CType(obj, NutUniqueKey))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Function Equals(other As NutUniqueKey) As Boolean Implements IEquatable(Of NutUniqueKey).Equals
        Return Me.ElementId = other.ElementId AndAlso Me.NomTypeId = other.NomTypeId
    End Function

    ''' <summary> Returns the hash code for this instance. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A 32-bit signed integer that is the hash code for this instance. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.ElementId Xor Me.NomTypeId
    End Function

    ''' <summary> Cast that converts the given NutUniqueKey to a =. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(left As NutUniqueKey, right As NutUniqueKey) As Boolean
        Return left.Equals(right)
    End Operator

    ''' <summary> Cast that converts the given NutUniqueKey to a &lt;&gt; </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(left As NutUniqueKey, right As NutUniqueKey) As Boolean
        Return Not left = right
    End Operator

End Structure

