Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Nut+ReadingBin builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class NutReadingBinBuilder
    Inherits OneToOneBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return NutReadingBinBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(NutReadingBinBuilder._TableName) Then
                NutReadingBinBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(NutReadingBinNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = NutBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(NutNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = ReadingBinBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(ReadingBinNub.Id)

    ''' <summary>
    ''' Inserts or ignores the records described by the <see cref="ReadingBin"/> enumeration type.
    ''' </summary>
    ''' <remarks> David, 5/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertIgnoreDefaultValues(ByVal connection As System.Data.IDbConnection) As Integer
        Return ReadingBinBuilder.Get.InsertIgnore(connection, GetType(ReadingBin), Array.Empty(Of Integer)())
    End Function

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(NutReadingBinEntity.NutAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(NutReadingBinEntity.ReadingBinId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As NutReadingBinBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As NutReadingBinBuilder
        If NutReadingBinBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                NutReadingBinBuilder._Instance = New NutReadingBinBuilder()
            End SyncLock
        End If
        Return NutReadingBinBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Nut+ReadingBin Nub based on the <see cref="IOneToOne">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
<Table("NutReadingBin")>
Public Class NutReadingBinNub
    Inherits OneToOneNub
    Implements IOneToOne

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToOne
        Return New NutReadingBinNub
    End Function

End Class

''' <summary> The Nut+ReadingBin Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class NutReadingBinEntity
    Inherits EntityBase(Of IOneToOne, NutReadingBinNub)
    Implements IOneToOne

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New NutReadingBinNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Nut+ReadingBin interface. </param>
    Public Sub New(ByVal value As IOneToOne)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToOne, ByVal store As IOneToOne)
        MyBase.New(New NutReadingBinNub, cache, store)
        Me.UsingNativeTracking = String.Equals(NutReadingBinBuilder.TableName, NameOf(IOneToOne).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToOne
        Return New NutReadingBinNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOneToOne
        Dim destination As IOneToOne = Me.CreateNew
        NutReadingBinNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToOne)
        NutReadingBinNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Nut+ReadingBin interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToOne)
        ' first make the copy to notify of any property change.
        NutReadingBinNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IOneToOne)(key), connection.Get(Of NutReadingBinNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.NutAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.NutAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, nutAutoId)
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert a new <see cref="Entities.ReadingBinEntity"/> entity and
    ''' fetches an existing or inserts a new <see cref="NutReadingBinEntity"/>.
    ''' </summary>
    ''' <remarks>
    ''' Assumes that an <see cref="Entities.NutEntity"/> exists for the specified
    ''' <paramref name="nutAutoId"/>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="binId">      Identifies the bin. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainReadingBin(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal binId As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._ReadingBinEntity = New ReadingBinEntity With {.Id = binId}
        If Not Me.ReadingBinEntity.FetchUsingKey(connection) Then
            result = (False, $"Failed obtaining {NameOf(Entities.ReadingBinEntity)} with {NameOf(Entities.ReadingBinEntity.Id)} of {binId}")
        End If
        If result.Success Then
            Me.NutAutoId = nutAutoId
            Me.ReadingBinId = binId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(NutReadingBinEntity.ReadingBinEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.NutReadingBinEntity)} for [{NameOf(Entities.NutEntity.AutoId)},{NameOf(Entities.ReadingBinEntity.Id)}] of [{nutAutoId},{binId}]")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.NutEntity"/> and
    ''' <see cref="Entities.ReadingBinEntity"/> entities and fetches an existing or inserts a new
    ''' <see cref="NutReadingBinEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="binId">      Identifies the bin. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal binId As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.NutEntity Is Nothing OrElse Me.NutEntity.AutoId <> nutAutoId Then
            ' ensure an Nut entity exists.
            Me._NutEntity = New NutEntity With {.AutoId = nutAutoId}
            If Not Me.NutEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.NutEntity)} with {NameOf(Entities.NutEntity.AutoId)} of {nutAutoId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainReadingBin(connection, nutAutoId, binId)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(NutReadingBinEntity.NutEntity))
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.NutEntity"/> and
    ''' <see cref="Entities.ReadingBinEntity"/> entities and fetches an existing or inserts a new
    ''' <see cref="NutReadingBinEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String)
        Return Me.TryObtain(connection, Me.NutAutoId, Me.ReadingBinId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToOne) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If NutReadingBinEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="nutAutoId">        Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <param name="readingBinAutoId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer, ByVal readingBinAutoId As Integer) As Boolean
        Return connection.Delete(Of NutReadingBinNub)(New NutReadingBinNub With {.PrimaryId = nutAutoId, .SecondaryId = readingBinAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Nut+ReadingBin entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Nut+ReadingBin entities. </value>
    Public ReadOnly Property NutReadingBins As IEnumerable(Of NutReadingBinEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of NutReadingBinEntity)
        Return If(usingNativeTracking, NutReadingBinEntity.Populate(connection.GetAll(Of IOneToOne)), NutReadingBinEntity.Populate(connection.GetAll(Of NutReadingBinNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._NutReadingBins = NutReadingBinEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(NutReadingBinEntity.NutReadingBins))
        Return If(Me.NutReadingBins?.Any, Me.NutReadingBins.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of NutReadingBinNub)) As IEnumerable(Of NutReadingBinEntity)
        Dim l As New List(Of NutReadingBinEntity)
        If nubs?.Any Then
            For Each nub As NutReadingBinNub In nubs
                l.Add(New NutReadingBinEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToOne)) As IEnumerable(Of NutReadingBinEntity)
        Dim l As New List(Of NutReadingBinEntity)
        If interfaces?.Any Then
            Dim nub As New NutReadingBinNub
            For Each iFace As IOneToOne In interfaces
                nub.CopyFrom(iFace)
                l.Add(New NutReadingBinEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; expects one ot zero. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{NutReadingBinBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NutReadingBinNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection; expects a single entity or none. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As IEnumerable(Of NutReadingBinEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{NutReadingBinBuilder.TableName}] WHERE {NameOf(NutReadingBinNub.PrimaryId)} = @Id", New With {Key .Id = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return NutReadingBinEntity.Populate(connection.Query(Of NutReadingBinNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by ReadingBin. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="readingBinAutoId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns> The total number of entities by ReadingBin. </returns>
    Public Shared Function CountEntitiesByReadingBin(ByVal connection As System.Data.IDbConnection, ByVal readingBinAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{NutReadingBinBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NutReadingBinNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = readingBinAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by ReadingBins in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="readingBinAutoId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by ReadingBins in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByReadingBin(ByVal connection As System.Data.IDbConnection, ByVal readingBinAutoId As Integer) As IEnumerable(Of NutReadingBinEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{NutReadingBinBuilder.TableName}] WHERE {NameOf(NutReadingBinNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = readingBinAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return NutReadingBinEntity.Populate(connection.Query(Of NutReadingBinNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As IEnumerable(Of NutReadingBinNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NutReadingBinBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NutReadingBinNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NutReadingBinNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Nut ReadingBin exists. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Boolean
        Return 1 = NutReadingBinEntity.CountEntities(connection, nutAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the "Entities.NutEntity". </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> the "Entities.NutEntity". </value>
    Public ReadOnly Property NutEntity As NutEntity

    ''' <summary> Fetches Nut entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> the "Entities.NutEntity". </returns>
    Public Function FetchNutEntity(ByVal connection As System.Data.IDbConnection) As NutEntity
        Dim entity As New NutEntity()
        entity.FetchUsingKey(connection, Me.NutAutoId)
        Me._NutEntity = entity
        Return entity
    End Function

    ''' <summary>
    ''' Count Nuts associated with the specified <paramref name="readingBinAutoId"/>; expected 1.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="readingBinAutoId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns> The total number of Nuts. </returns>
    Public Shared Function CountNuts(ByVal connection As System.Data.IDbConnection, ByVal readingBinAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{NutReadingBinBuilder.TableName}] WHERE {NameOf(NutReadingBinNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = readingBinAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Nuts associated with the specified <paramref name="readingBinAutoId"/>; expected
    ''' a single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="readingBinAutoId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Nuts in this collection.
    ''' </returns>
    Public Shared Function FetchNuts(ByVal connection As System.Data.IDbConnection, ByVal readingBinAutoId As Integer) As IEnumerable(Of ReadingBinEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{NutReadingBinBuilder.TableName}] WHERE {NameOf(NutReadingBinNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = readingBinAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ReadingBinEntity)
        For Each nub As NutReadingBinNub In connection.Query(Of NutReadingBinNub)(selector.RawSql, selector.Parameters)
            Dim entity As New NutReadingBinEntity(nub)
            l.Add(entity.FetchReadingBinEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Nuts associated with the specified <paramref name="readingBinAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="readingBinAutoId"> Identifies the <see cref="Entities.ReadingBinEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteNuts(ByVal connection As System.Data.IDbConnection, ByVal readingBinAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{NutReadingBinBuilder.TableName}] WHERE {NameOf(NutReadingBinNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = readingBinAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the ReadingBin entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The ReadingBin entity. </value>
    Public ReadOnly Property ReadingBinEntity As ReadingBinEntity

    ''' <summary> Fetches a ReadingBin Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The ReadingBin Entity. </returns>
    Public Function FetchReadingBinEntity(ByVal connection As System.Data.IDbConnection) As ReadingBinEntity
        Dim entity As New ReadingBinEntity()
        entity.FetchUsingKey(connection, Me.ReadingBinId)
        Me._ReadingBinEntity = entity
        Return entity
    End Function

    ''' <summary> Count reading bins. </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The total number of reading bins. </returns>
    Public Shared Function CountReadingBins(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{NutReadingBinBuilder.TableName}] WHERE {NameOf(NutReadingBinNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the ReadingBins in this collection. </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ReadingBins in this collection.
    ''' </returns>
    Public Shared Function FetchReadingBins(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As IEnumerable(Of ReadingBinEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{NutReadingBinBuilder.TableName}] WHERE {NameOf(NutReadingBinNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = nutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ReadingBinEntity)
        For Each nub As NutReadingBinNub In connection.Query(Of NutReadingBinNub)(selector.RawSql, selector.Parameters)
            Dim entity As New NutReadingBinEntity(nub)
            l.Add(entity.FetchReadingBinEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all ReadingBin related to the specified Nut. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteReadingBins(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{NutReadingBinBuilder.TableName}] WHERE {NameOf(NutReadingBinNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = nutAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches a <see cref="Entities.ReadingBinEntity"/>. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="nutAutoId">   Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The ReadingBin. </returns>
    Public Shared Function FetchReadingBin(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal nutAutoId As Integer) As ReadingBinEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.Id = nutAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As ReadingBinNub = connection.Query(Of ReadingBinNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New ReadingBinEntity, New ReadingBinEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches a <see cref="Entities.ReadingBinEntity"/>. </summary>
    ''' <remarks>
    ''' David, 5/23/2020.
    ''' <code>
    ''' SELECT [ReadingBin].*
    ''' FROM [ReadingBin] Inner Join [NutReadingBin]
    ''' ON [NutReadingBin].[SecondaryId] = [ReadingBin].[AutoId]
    ''' WHERE ([NutReadingBin].[PrimaryId] = 8)
    ''' </code>
    ''' </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutAutoId">  Identifies the <see cref="Entities.NutEntity"/>. </param>
    ''' <returns> The ReadingBin. </returns>
    Public Shared Function FetchReadingBin(ByVal connection As System.Data.IDbConnection, ByVal nutAutoId As Integer) As ReadingBinEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [NutReadingBin] on [NutReadingBin].SecondaryId = [ReadingBin].AutoId where [NutReadingBin].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{ReadingBinBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{ReadingBinBuilder.TableName}] Inner Join [{NutReadingBinBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{NutReadingBinBuilder.TableName}].[{NameOf(NutReadingBinNub.SecondaryId)}] = [{ReadingBinBuilder.TableName}].[{NameOf(ReadingBinNub.Id)}]")
        queryBuilder.AppendLine($"WHERE ([{NutReadingBinBuilder.TableName}].[{NameOf(NutReadingBinNub.PrimaryId)}] = @Id); ")
        Return NutReadingBinEntity.FetchReadingBin(connection, queryBuilder.ToString, nutAutoId)
    End Function


#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToOne.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(NutReadingBinEntity.NutAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NutEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NutEntity"/>. </value>
    Public Property NutAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToOne.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(NutReadingBinEntity.ReadingBinId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.ReadingBinEntity"/>.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.ReadingBinEntity"/>. </value>
    Public Property ReadingBinId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class


