''' <summary> A product test monitor. </summary>
''' <remarks> David, 2020-05-29. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Class SessionMonitor
    Inherits isr.Core.Models.ViewModelBase

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SessionMonitor

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SessionMonitor
        If SessionMonitor._Instance Is Nothing Then
            SyncLock SyncLocker
                SessionMonitor._Instance = New SessionMonitor()
            End SyncLock
        End If
        Return SessionMonitor._Instance
    End Function

#End Region

    Public Property Part As PartEntity

    Public Property Lot As LotEntity

    Public Property Session As SessionEntity

    Public Property Product As ProductEntity

    ''' <summary> Gets or sets the Uut monitor. </summary>
    ''' <value> The Uut monitor. </value>
    Public Property UutMonitor As UutMonitor

    ''' <summary> Gets or sets the elements. </summary>
    ''' <value> The elements. </value>
    Public Property Elements As ProductUniqueElementEntityCollection

    ''' <summary> Gets or sets the uuts. </summary>
    ''' <value> The uuts. </value>
    Public Property Uuts As LotUniqueUutEntityCollection

    ''' <summary> Gets or sets the sorts. </summary>
    ''' <value> The sorts. </value>
    Public Property Sorts As ProductUniqueProductSortEntityCollection

    ''' <summary> Gets or sets the Binning Entities. </summary>
    ''' <value> The Binning Entities. </value>
    Public Property Binnings As Entities.ProductUniqueBinningEntityCollection


End Class
