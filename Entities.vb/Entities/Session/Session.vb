
Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A builder for the nominal test session based on the <see cref="IKeyLabelTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class SessionBuilder
    Inherits KeyLabelTimeBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return SessionBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(SessionBuilder._TableName) Then
                SessionBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(SessionNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 50

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(SessionEntity.AutoId)

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <value> The name of the label field. </value>
    Public Overrides Property LabelFieldName() As String = NameOf(SessionEntity.SessionNumber)

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <value> The name of the timestamp field. </value>
    Public Overrides Property TimestampFieldName() As String = NameOf(SessionEntity.Timestamp)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SessionBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SessionBuilder
        If SessionBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                SessionBuilder._Instance = New SessionBuilder()
            End SyncLock
        End If
        Return SessionBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the POCO representation of the nominal test session based on the <see cref="IKeyLabelTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("Session")>
Public Class SessionNub
    Inherits KeyLabelTimeNub
    Implements IKeyLabelTime

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IKeyLabelTime
        Return New SessionNub
    End Function

End Class


''' <summary> The nominal session entity based on the <see cref="IKeyLabelTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class SessionEntity
    Inherits EntityBase(Of IKeyLabelTime, SessionNub)
    Implements IKeyLabelTime

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New SessionNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Session interface. </param>
    Public Sub New(ByVal value As IKeyLabelTime)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyLabelTime, ByVal store As IKeyLabelTime)
        MyBase.New(New SessionNub, cache, store)
        Me.UsingNativeTracking = String.Equals(SessionBuilder.TableName, NameOf(IKeyLabelTime).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IKeyLabelTime
        Return New SessionNub
    End Function

    Public Overrides Function CreateCopy() As IKeyLabelTime
        Dim destination As IKeyLabelTime = Me.CreateNew
        SessionNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IKeyLabelTime)
        SessionNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Session interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyLabelTime)
        ' first make the copy to notify of any property change.
        SessionNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Session table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyLabelTime)(key), connection.Get(Of SessionNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.SessionLabel)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The session label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As SessionNub = SessionEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyLabelTime) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If SessionEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(SessionBuilder.Get.UsingUniqueLabel(connection),
                                        Me.FetchUsingUniqueIndex(connection, entity.Label),
                                        Me.FetchUsingKey(connection, entity.AutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of SessionNub)(New SessionNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Session entities. </summary>
    ''' <value> The Session entities. </value>
    Public ReadOnly Property Sessions As IEnumerable(Of SessionEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of SessionEntity)
        Return If(usingNativeTracking, SessionEntity.Populate(connection.GetAll(Of IKeyLabelTime)), SessionEntity.Populate(connection.GetAll(Of SessionNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Sessions = SessionEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(SessionEntity.Sessions))
        Return If(Me.Sessions?.Any, Me.Sessions.Count, 0)
    End Function

    ''' <summary> Populates a list of Session entities. </summary>
    ''' <param name="nubs"> The Session nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of SessionNub)) As IEnumerable(Of SessionEntity)
        Dim l As New List(Of SessionEntity)
        If nubs?.Any Then
            For Each nub As SessionNub In nubs
                l.Add(New SessionEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Session entities. </summary>
    ''' <param name="interfaces"> The Session interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyLabelTime)) As IEnumerable(Of SessionEntity)
        Dim l As New List(Of SessionEntity)
        If interfaces?.Any Then
            Dim nub As New SessionNub
            For Each iFace As IKeyLabelTime In interfaces
                nub.CopyFrom(iFace)
                l.Add(New SessionEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="sessionName"> The name of the Session. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionName As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SessionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionNub.Label)} = @SessionName", New With {sessionName})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="sessionName"> The name of the Session. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal sessionName As String) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{SessionBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(SessionNub.Label)} IS NULL OR {NameOf(SessionNub.Label)} = @SessionName)")
        selectSql.Append("; ")
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {sessionName})
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="sessionName"> The name of the Session. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal sessionName As String) As IEnumerable(Of SessionNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SessionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionNub.Label)} = @SessionName", New With {sessionName})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SessionNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Session exists. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="sessionName"> The name of the Session. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal sessionName As String) As Boolean
        Return 1 = SessionEntity.CountEntities(connection, sessionName)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Session. </summary>
    ''' <value> Identifies the Session. </value>
    Public Property AutoId As Integer Implements IKeyLabelTime.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Label. </summary>
    ''' <value> The Label. </value>
    Public Property Label As String Implements IKeyLabelTime.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SessionEntity.SessionLabel))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the session label. </summary>
    ''' <value> The session label. </value>
    Public Property SessionLabel As String
        Get
            Return Me.Label
        End Get
        Set(value As String)
            Me.Label = value
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks> Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to convert to local time. </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements IKeyLabelTime.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " TIMESTAMP "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

#End Region

#Region " SESSION NUMBER "

    Private _SessionNumber As String

    ''' <summary> Gets the session number. </summary>
    ''' <value> The session number. </value>
    Public ReadOnly Property SessionNumber As String
        Get
            If String.IsNullOrEmpty(Me._SessionNumber) Then
                Me._SessionNumber = If(Me.SessionLabel.Contains(SessionEntity.SessionIdentityDelimiter),
                    Me.SessionLabel.Split(SessionEntity.SessionIdentityDelimiter.ToCharArray).Last,
                    "?")
            End If
            Return Me._SessionNumber
        End Get
    End Property

    ''' <summary> Gets or sets the session identity delimiter. </summary>
    ''' <value> The session identity delimiter. </value>
    Public Shared Property SessionIdentityDelimiter As String = ":"

    ''' <summary> Builds session identity. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <param name="lotLabel">             The lot label. </param>
    ''' <param name="sessionOrdinalNumber"> The session ordinal number. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildSessionIdentity(ByVal lotLabel As String, ByVal sessionOrdinalNumber As Integer) As String
        Return SessionEntity.BuildSessionIdentity(SessionEntity.SessionIdentityDelimiter, lotLabel, sessionOrdinalNumber)
    End Function

    ''' <summary> Builds session identity. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <param name="delimiter">            The delimiter. </param>
    ''' <param name="lotLabel">             The lot label. </param>
    ''' <param name="sessionOrdinalNumber"> The session ordinal number. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildSessionIdentity(ByVal delimiter As String, ByVal lotLabel As String, ByVal sessionOrdinalNumber As Integer) As String
        Return $"{lotLabel}{delimiter}{sessionOrdinalNumber}"
    End Function

    ''' <summary> Builds session identity. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <param name="lotLabel">         The lot label. </param>
    ''' <param name="lastSessionLabel"> The last session label. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildSessionIdentity(ByVal lotLabel As String, ByVal lastSessionLabel As String) As String
        Dim labelElements As String() = lastSessionLabel.Split(SessionEntity.SessionIdentityDelimiter.ToCharArray)
        Dim ordinalNumber As Integer = Integer.Parse(labelElements(1))
        Return SessionEntity.BuildSessionIdentity(SessionEntity.SessionIdentityDelimiter, lotLabel, ordinalNumber + 1)
    End Function

    ''' <summary> Builds session identity. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <param name="lastSessionLabel"> The last session label. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildSessionIdentity(ByVal delimiter As String, ByVal lotLabel As String, ByVal lastSessionLabel As String) As String
        Dim labelElements As String() = lastSessionLabel.Split(delimiter.ToCharArray)
        Dim ordinalNumber As Integer = Integer.Parse(labelElements(1))
        Return SessionEntity.BuildSessionIdentity(delimiter, lotLabel, ordinalNumber + 1)
    End Function



#End Region
End Class

