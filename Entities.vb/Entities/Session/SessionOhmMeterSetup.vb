Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Session OhmMeterSetup builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class SessionOhmMeterSetupBuilder
    Inherits OneToManyIdBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return SessionOhmMeterSetupBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(SessionOhmMeterSetupBuilder._TableName) Then
                SessionOhmMeterSetupBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(SessionOhmMeterSetupNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <OhmMeterSetup> The name of the primary table. </OhmMeterSetup>
    Public Overrides Property PrimaryTableName As String = SessionBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <OhmMeterSetup> The name of the primary table key. </OhmMeterSetup>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(SessionNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <OhmMeterSetup> The name of the secondary table. </OhmMeterSetup>
    Public Overrides Property SecondaryTableName As String = MultimeterBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <OhmMeterSetup> The name of the secondary table key. </OhmMeterSetup>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(MultimeterNub.Id)

    ''' <summary> Gets or sets the name of the Ternary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Ternary table. </value>
    Public Overrides Property ForeignIdTableName As String = OhmMeterSetupBuilder.TableName

    ''' <summary> Gets or sets the name of the Ternary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the Ternary table key. </value>
    Public Overrides Property ForeignIdTableKeyName As String = NameOf(OhmMeterSetupNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(SessionOhmMeterSetupEntity.SessionAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(SessionOhmMeterSetupEntity.MultimeterId)

    ''' <summary> Gets the name of the foreign identifier field. </summary>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName As String = NameOf(SessionOhmMeterSetupEntity.OhmMeterSetupAutoId)


#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SessionOhmMeterSetupBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SessionOhmMeterSetupBuilder
        If SessionOhmMeterSetupBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                SessionOhmMeterSetupBuilder._Instance = New SessionOhmMeterSetupBuilder()
            End SyncLock
        End If
        Return SessionOhmMeterSetupBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="SessionEntity"/> OhmMeterSetup table <see cref="IOneToManyId">interface</see>.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("SessionOhmMeterSetup")>
Public Class SessionOhmMeterSetupNub
    Inherits OneToManyIdNub
    Implements IOneToManyId

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToManyId
        Return New SessionOhmMeterSetupNub
    End Function

End Class

''' <summary> The <see cref="SessionEntity"/>OhmMeterSetup Entity. Implements access to the database using Dapper. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class SessionOhmMeterSetupEntity
    Inherits EntityBase(Of IOneToManyId, SessionOhmMeterSetupNub)
    Implements IOneToManyId

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New SessionOhmMeterSetupNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> the <see cref="SessionEntity"/>OhmMeterSetup interface. </param>
    Public Sub New(ByVal value As IOneToManyId)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToManyId, ByVal store As IOneToManyId)
        MyBase.New(New SessionOhmMeterSetupNub, cache, store)
        Me.UsingNativeTracking = String.Equals(SessionOhmMeterSetupBuilder.TableName, NameOf(IOneToManyId).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "


    Public Overrides Function CreateNew() As IOneToManyId
        Return New SessionOhmMeterSetupNub
    End Function

    Public Overrides Function CreateCopy() As IOneToManyId
        Dim destination As IOneToManyId = Me.CreateNew
        SessionOhmMeterSetupNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="SessionOhmMeterSetupEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToManyId)
        SessionOhmMeterSetupNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached OhmMeterSetup, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> the <see cref="SessionEntity"/>OhmMeterSetup interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToManyId)
        ' first make the copy to notify of any property change.
        SessionOhmMeterSetupNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="meterId">       Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal meterId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As SessionOhmMeterSetupNub = SessionOhmMeterSetupEntity.FetchNubs(connection, sessionAutoId, meterId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="meterId">       Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal meterId As Integer) As Boolean
        Me.ClearStore()
        Return Me.FetchUsingKey(connection, sessionAutoId, meterId)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToManyId) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If SessionOhmMeterSetupEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="meterId">       Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal meterId As Integer) As Boolean
        Return connection.Delete(Of SessionOhmMeterSetupNub)(New SessionOhmMeterSetupNub With {.PrimaryId = sessionAutoId, .SecondaryId = meterId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="SessionEntity"/>OhmMeterSetup entities. </summary>
    ''' <OhmMeterSetup> the <see cref="SessionEntity"/>OhmMeterSetup entities. </OhmMeterSetup>
    Public ReadOnly Property SessionOhmMeterSetups As IEnumerable(Of SessionOhmMeterSetupEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of SessionOhmMeterSetupEntity)
        Return If(usingNativeTracking, SessionOhmMeterSetupEntity.Populate(connection.GetAll(Of IOneToManyId)),
                                       SessionOhmMeterSetupEntity.Populate(connection.GetAll(Of SessionOhmMeterSetupNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._SessionOhmMeterSetups = SessionOhmMeterSetupEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(SessionOhmMeterSetupEntity.SessionOhmMeterSetups))
        Return If(Me.SessionOhmMeterSetups?.Any, Me.SessionOhmMeterSetups.Count, 0)
    End Function

    ''' <summary> Count SessionOhmMeterSetups. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{SessionOhmMeterSetupBuilder.TableName}] WHERE {NameOf(SessionOhmMeterSetupNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As IEnumerable(Of SessionOhmMeterSetupEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{SessionOhmMeterSetupBuilder.TableName}] WHERE {NameOf(SessionOhmMeterSetupNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return SessionOhmMeterSetupEntity.Populate(connection.Query(Of SessionOhmMeterSetupNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Fetches Session ohm meter setup by Session auto id; expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As IEnumerable(Of SessionOhmMeterSetupNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SessionOhmMeterSetupBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionOhmMeterSetupEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SessionOhmMeterSetupNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of SessionOhmMeterSetup entities. </summary>
    ''' <param name="nubs"> the <see cref="SessionEntity"/>OhmMeterSetup nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of SessionOhmMeterSetupNub)) As IEnumerable(Of SessionOhmMeterSetupEntity)
        Dim l As New List(Of SessionOhmMeterSetupEntity)
        If nubs?.Any Then
            For Each nub As SessionOhmMeterSetupNub In nubs
                l.Add(New SessionOhmMeterSetupEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of SessionOhmMeterSetup entities. </summary>
    ''' <param name="interfaces"> the <see cref="SessionEntity"/>OhmMeterSetup interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToManyId)) As IEnumerable(Of SessionOhmMeterSetupEntity)
        Dim l As New List(Of SessionOhmMeterSetupEntity)
        If interfaces?.Any Then
            Dim nub As New SessionOhmMeterSetupNub
            For Each iFace As IOneToManyId In interfaces
                nub.CopyFrom(iFace)
                l.Add(New SessionOhmMeterSetupEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Session Traits; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="meterId">       Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal meterId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SessionOhmMeterSetupBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionOhmMeterSetupNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = sessionAutoId})
        sqlBuilder.Where($"{NameOf(SessionOhmMeterSetupNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = meterId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Session Ohm Meter Setup by Session Auto ID and Meter Id;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="meterId">       Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal meterId As Integer) As IEnumerable(Of SessionOhmMeterSetupNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SessionOhmMeterSetupBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionOhmMeterSetupNub.PrimaryId)} = @primaryId", New With {.primaryId = sessionAutoId})
        sqlBuilder.Where($"{NameOf(SessionOhmMeterSetupNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = meterId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SessionOhmMeterSetupNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="SessionEntity"/>OhmMeterSetup exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="meterId">       Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal meterId As Integer) As Boolean
        Return 1 = SessionOhmMeterSetupEntity.CountEntities(connection, sessionAutoId, meterId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count OhmMeterSetups associated with this Session. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of OhmMeterSetup OhmMeterSetups. </returns>
    Public Function CountSessionOhmMeterSetups(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SessionOhmMeterSetupBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionOhmMeterSetupNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Session OhmMeterSetup OhmMeterSetups by <see cref="SessionOhmMeterSetupEntity.SessionAutoId"/>;
    ''' expected as manger as the number of meters.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">          The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchSessionOhmMeterSetups(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of SessionOhmMeterSetupNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SessionOhmMeterSetupBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionOhmMeterSetupNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SessionOhmMeterSetupNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets or sets the <see cref="SessionEntity"/> Entity. </summary>
    ''' <OhmMeterSetup> the <see cref="SessionEntity"/> Entity. </OhmMeterSetup>
    Public ReadOnly Property SessionEntity As SessionEntity

    ''' <summary> Fetches a Session Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchSessionEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._SessionEntity = New SessionEntity()
        Return Me.SessionEntity.FetchUsingKey(connection, Me.SessionAutoId)
    End Function

#Region " MULTIMETER "

    Private _MultimeterEntity As MultimeterEntity

    ''' <summary> Gets or sets the meter model entity. </summary>
    ''' <value> The meter model entity. </value>
    Public ReadOnly Property MultimeterEntity As MultimeterEntity
        Get
            If Me._MultimeterEntity Is Nothing Then
                Me._MultimeterEntity = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
            End If
            Return Me._MultimeterEntity
        End Get
    End Property

    ''' <summary> Fetches the meter. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchMultimeterEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not MultimeterEntity.IsEnumerated Then MultimeterEntity.TryFetchAll(connection)
        Me._MultimeterEntity = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.MultimeterEntity))
        Return Me._MultimeterEntity IsNot Nothing
    End Function

#End Region

#Region " METER MODEL "

    Private _MultimeterModelEntity As MultimeterModelEntity

    ''' <summary> Gets or sets the Meter Model entity. </summary>
    ''' <value> The meter model entity. </value>
    Public ReadOnly Property MultimeterModelEntity As MultimeterModelEntity
        Get
            If Me._MultimeterModelEntity Is Nothing Then
                Me._MultimeterModelEntity = Me.MultimeterEntity.MultimeterModelEntity
            End If
            Return Me._MultimeterModelEntity
        End Get
    End Property

    ''' <summary> Fetches the Meter Model entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchMultimeterModel(ByVal connection As System.Data.IDbConnection) As Boolean
        Me.FetchMultimeterEntity(connection)
        Me.MultimeterEntity.FetchMultimeterModel(connection)
        Me._MultimeterModelEntity = Me.MultimeterEntity.MultimeterModelEntity
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.MeterModelEntity))
        Return Me.MultimeterModelEntity IsNot Nothing
    End Function

#End Region

#Region " OHM METER SETUP "

    Public ReadOnly Property OhmMeterSetupEntity As OhmMeterSetupEntity

    ''' <summary> Fetches ohm meter setup entity. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function FetchOhmMeterSetupEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._OhmMeterSetupEntity = New OhmMeterSetupEntity()
        Return Me.OhmMeterSetupEntity.FetchUsingKey(connection, Me.OhmMeterSetupAutoId)
    End Function

#End Region

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToManyId.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SessionOhmMeterSetupEntity.SessionAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="SessionEntity"/> record. </summary>
    ''' <OhmMeterSetup> Identifies the <see cref="SessionEntity"/> record. </OhmMeterSetup>
    Public Property SessionAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property


    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToManyId.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SessionOhmMeterSetupEntity.MultimeterId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identity of the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <OhmMeterSetup> The identity of the <see cref="Entities.MultimeterEntity"/>. </OhmMeterSetup>
    Public Property MultimeterId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the Three-to-Many referenced table identifier. </summary>
    ''' <value> The identifier of Three-to-Many referenced table. </value>
    Public Property ForeignId As Integer Implements IOneToManyId.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SessionOhmMeterSetupEntity.OhmMeterSetupAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identity of the <see cref="OhmMeterSetupEntity"/>. </summary>
    ''' <OhmMeterSetup> The identity of the <see cref="OhmMeterSetupEntity"/>. </OhmMeterSetup>
    Public Property OhmMeterSetupAutoId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(ByVal value As Integer)
            Me.ForeignId = value
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class


''' <summary> Collection of session attribute Natural (Integer-value) entities. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class SessionOhmMeterSetupEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToManyId, SessionOhmMeterSetupNub, SessionOhmMeterSetupEntity)

    Protected Overrides Function GetKeyForItem(item As SessionOhmMeterSetupEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As SessionOhmMeterSetupEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all sessions from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of SessionOhmMeterSetupEntity))
        If entities?.Any Then
            For Each entity As SessionOhmMeterSetupEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return SessionOhmMeterSetupBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary> Collection of Session-Unique Session Attribute Natural (Integer-value) Entities unique to the session. </summary>
''' <remarks> David, 2020-05-05. </remarks>
''' 
Public Class SessionUniqueOhmMeterSetupEntityCollection
    Inherits SessionOhmMeterSetupEntityCollection

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    Public Sub New(ByVal sessionAutoId As Integer)
        Me.New
        Me.SessionAutoId = sessionAutoId
    End Sub


    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value
    '''                       can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As SessionOhmMeterSetupEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.OhmMeterSetupAutoId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.OhmMeterSetupAutoId)
    End Sub

    ''' <summary>
    ''' Removes all sessions from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains 'ohmMeterSetupAutoId' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="ohmMeterSetupAutoId"> Identifies the <see cref="OhmMeterSetupEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal ohmMeterSetupAutoId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(ohmMeterSetupAutoId)
    End Function

    ''' <summary> Gets the setup for the specified ID. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Function OhmMeterSetup(ByVal ohmMeterSetupAutoId As Integer) As SessionOhmMeterSetupEntity
        Return If(Me.ContainsKey(ohmMeterSetupAutoId), Me(Me._PrimaryKeyDictionary(ohmMeterSetupAutoId)), New SessionOhmMeterSetupEntity)
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="SessionEntity"/>. </summary>
    ''' <value> Identifies the <see cref="SessionEntity"/>. </value>
    Public ReadOnly Property SessionAutoId As Integer

End Class

