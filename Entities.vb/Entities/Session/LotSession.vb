Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Lot-Session builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class LotSessionBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return LotSessionBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(LotSessionBuilder._TableName) Then
                LotSessionBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(LotSessionNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = LotBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(LotNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = SessionBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(SessionNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(LotSessionEntity.LotAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(LotSessionEntity.SessionAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As LotSessionBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As LotSessionBuilder
        If LotSessionBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                LotSessionBuilder._Instance = New LotSessionBuilder()
            End SyncLock
        End If
        Return LotSessionBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Lot Session Nub based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("LotSession")>
Public Class LotSessionNub
    Inherits OneToManyNub
    Implements IOneToMany

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToMany
        Return New LotSessionNub
    End Function

End Class

''' <summary>
''' The Lot-Session Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class LotSessionEntity
    Inherits EntityBase(Of IOneToMany, LotSessionNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New LotSessionNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Lot-Session interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New LotSessionNub, cache, store)
        Me.UsingNativeTracking = String.Equals(LotSessionBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToMany
        Return New LotSessionNub
    End Function

    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        LotSessionNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        LotSessionNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Lot-Session interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        LotSessionNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="sessionAutoId">      Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal sessionAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As LotSessionNub = LotSessionEntity.FetchNubs(connection, lotAutoId, sessionAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.LotAutoId, Me.SessionAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.LotAutoId, Me.SessionAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId">     Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal sessionAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, lotAutoId, sessionAutoId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If LotSessionEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId">     Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal sessionAutoId As Integer) As Boolean
        Return connection.Delete(Of LotSessionNub)(New LotSessionNub With {.PrimaryId = lotAutoId, .SecondaryId = sessionAutoId})
    End Function

#End Region

#Region " OBTAIN "

    ''' <summary>
    ''' Tries to fetch an existing or insert a new <see cref="SessionEntity"/>  and fetch an existing or
    ''' insert a new <see cref="LotSessionEntity"/>. If the session label is non-unique, a session is inserted
    ''' if this session label is not associated with this lot.
    ''' </summary>
    ''' <remarks>
    ''' Assumes that a <see cref="Entities.LotEntity"/> exists for the specified <paramref name="lotAutoId"/>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="sessionLabel">  The session label. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainSession(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal sessionLabel As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If SessionBuilder.Get.UsingUniqueLabel(connection) Then
            ' if using a unique session label, the session label is unique across all lots
            Me._SessionEntity = New SessionEntity() With {.SessionLabel = sessionLabel}
            If Not Me.SessionEntity.Obtain(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.SessionEntity)} with {NameOf(Entities.SessionEntity.SessionLabel)} of {sessionLabel}")
            End If
        Else
            ' if not using a unique session label, the session label is unique for this lot
            Me._SessionEntity = LotSessionEntity.FetchSession(connection, lotAutoId, sessionLabel)
            If Not Me.SessionEntity.IsClean Then
                Me._SessionEntity = New SessionEntity() With {.SessionLabel = sessionLabel}
                If Not Me.SessionEntity.Insert(connection) Then
                    result = (False, $"Failed inserting {NameOf(Entities.SessionEntity)} with {NameOf(Entities.SessionEntity.SessionLabel)} of {sessionLabel}")
                End If
            End If
        End If
        If result.Success Then
            Me.SessionEntity.FetchTraits(connection)
            Me.LotAutoId = lotAutoId
            Me.SessionAutoId = Me.SessionEntity.AutoId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(LotSessionEntity.SessionEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.LotSessionEntity)} with {NameOf(Entities.LotSessionEntity.LotAutoId)} of {lotAutoId} and {NameOf(Entities.LotSessionEntity.SessionAutoId)} of {Me.SessionEntity.AutoId}")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.LotEntity"/> and <see cref="SessionEntity"/> entities
    ''' and fetches an existing or inserts a new <see cref="LotSessionEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="sessionLabel">  The session label. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal sessionLabel As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.LotEntity Is Nothing OrElse Me.LotEntity.AutoId <> lotAutoId Then
            ' make sure a lot exists for the auto id.
            Me._LotEntity = New LotEntity With {.AutoId = lotAutoId}
            If Not Me._LotEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.AutoId)} of {lotAutoId}")
            End If
        End If
        If result.Success Then
            Me.LotEntity.FetchTraits(connection)
            result = Me.TryObtainSession(connection, lotAutoId, sessionLabel)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(LotSessionEntity.LotEntity))
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.LotEntity"/> and <see cref="SessionEntity"/> entities
    ''' and fetches an existing or inserts a new <see cref="LotSessionEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal sessionLabel As String) As (Success As Boolean, Details As String)
        Return Me.TryObtain(connection, Me.LotAutoId, sessionLabel)
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.LotEntity"/> and <see cref="SessionEntity"/>
    ''' entities and fetches an existing or inserts a new <see cref="LotSessionEntity"/>.
    ''' </summary>
    ''' <remarks> David, 6/20/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber"> The lot number. </param>
    ''' <param name="sessionLabel">  The session label. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String, ByVal sessionLabel As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.LotEntity Is Nothing OrElse Not String.Equals(Me.LotEntity.LotNumber, lotNumber) Then
            Me._LotEntity = New LotEntity With {.LotNumber = lotNumber}
            If Me.LotEntity.Obtain(connection) Then
                Me.LotAutoId = Me.LotEntity.AutoId
            Else
                result = (False, $"Failed fetching {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.AutoId)} of {Me.LotAutoId}")
            End If
        End If
        If result.Success Then
            Me.LotEntity.FetchTraits(connection)
            result = Me.TryObtainSession(connection, Me.LotAutoId, sessionLabel)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(LotSessionEntity.LotEntity))
        Return result
    End Function

    ''' <summary>
    ''' Fetches an existing or inserts new <see cref="Entities.LotEntity"/> and <see cref="SessionEntity"/> entities
    ''' and fetches an existing or inserts a new <see cref="LotSessionEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="sessionLabel">  The session label. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Function Obtain(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal sessionLabel As String) As Boolean
        Dim r As (Success As Boolean, Details As String) = Me.TryObtain(connection, lotAutoId, sessionLabel)
        If Not r.Success Then Throw New OperationFailedException(r.Details)
        Return r.Success
    End Function

    ''' <summary> Try obtain new session. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainNewSession(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.LotEntity Is Nothing OrElse Not String.Equals(Me.LotEntity.LotNumber, lotNumber) Then
            Me._LotEntity = New LotEntity With {.LotNumber = lotNumber}
            If Me.LotEntity.Obtain(connection) Then
                Me.LotAutoId = Me.LotEntity.AutoId
            Else
                result = (False, $"Failed fetching {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.AutoId)} of {Me.LotAutoId}")
            End If
        End If
        If result.Success Then
            Dim lastSession As SessionEntity = LotSessionEntity.FetchOrderedSessions(connection, Me.LotAutoId).LastOrDefault
            Dim lastSessionNumber As Integer = If((lastSession?.IsClean).GetValueOrDefault(False),
                                                  Integer.Parse(lastSession.SessionNumber),
                                                  0)
            Dim lastSessionLabel As String = SessionEntity.BuildSessionIdentity(Me.LotEntity.LotNumber, lastSessionNumber)
            result = Me.TryObtainSession(connection, Me.LotAutoId, SessionEntity.BuildSessionIdentity(Me.LotEntity.LotNumber, lastSessionLabel))
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(LotSessionEntity.LotEntity))
        Return result
    End Function

    ''' <summary> Obtain new session. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    Public Sub ObtainNewSession(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String)
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        Dim r As (Success As Boolean, Details As String) = Me.TryObtainNewSession(New TransactedConnection(connection, transaction), lotNumber)
                        If Not r.Success Then Throw New OperationFailedException(r.Details)
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            Dim r As (Success As Boolean, Details As String) = Me.TryObtainNewSession(transactedConnection, lotNumber)
            If Not r.Success Then Throw New OperationFailedException(r.Details)
        End If
    End Sub

    ''' <summary> Try obtain the last or a new session. </summary>
    ''' <remarks> David, 6/25/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainLastSession(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.LotEntity Is Nothing OrElse Not String.Equals(Me.LotEntity.LotNumber, lotNumber) Then
            Me._LotEntity = New LotEntity With {.LotNumber = lotNumber}
            If Not Me.LotEntity.Obtain(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.AutoId)} of {Me.LotAutoId}")
            End If
        End If
        Dim lastSession As New SessionEntity
        If result.Success Then
            Me.LotAutoId = Me.LotEntity.AutoId
            lastSession = LotSessionEntity.FetchOrderedSessions(connection, Me.LotAutoId).LastOrDefault
            If (lastSession?.IsClean).GetValueOrDefault(False) Then
                Me._SessionEntity = lastSession
            Else
                Dim lastSessionLabel As String = SessionEntity.BuildSessionIdentity(Me.LotEntity.LotNumber, 0)
                result = Me.TryObtainSession(connection, Me.LotAutoId, SessionEntity.BuildSessionIdentity(Me.LotEntity.LotNumber, lastSessionLabel))
            End If
        End If
        If result.Success Then
            Me.LotAutoId = Me.LotEntity.AutoId
            Me.SessionAutoId = Me.SessionEntity.AutoId
            If Not Me.Obtain(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.LotSessionEntity)} with {NameOf(Entities.LotSessionEntity.LotAutoId)} of {Me.LotAutoId} and {NameOf(Entities.LotSessionEntity.SessionAutoId)} of {Me.SessionAutoId}")
            End If
        End If
        If result.Success Then
            Me.NotifyPropertyChanged(NameOf(LotSessionEntity.SessionEntity))
            Me.NotifyPropertyChanged(NameOf(LotSessionEntity.LotEntity))
        End If
        Return result
    End Function

    ''' <summary> Obtain the last or a new session. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    Public Sub ObtainLastSession(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String)
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        Dim r As (Success As Boolean, Details As String) = Me.TryObtainLastSession(New TransactedConnection(connection, transaction), lotNumber)
                        If Not r.Success Then Throw New OperationFailedException(r.Details)
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            Dim r As (Success As Boolean, Details As String) = Me.TryObtainLastSession(transactedConnection, lotNumber)
            If Not r.Success Then Throw New OperationFailedException(r.Details)
        End If
    End Sub

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Lot-Session entities. </summary>
    ''' <value> The Lot-Session entities. </value>
    Public ReadOnly Property LotSessions As IEnumerable(Of LotSessionEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of LotSessionEntity)
        Return If(usingNativeTracking, LotSessionEntity.Populate(connection.GetAll(Of IOneToMany)), LotSessionEntity.Populate(connection.GetAll(Of LotSessionNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._LotSessions = LotSessionEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(LotSessionEntity.LotSessions))
        Return If(Me.LotSessions?.Any, Me.LotSessions.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of LotSessionNub)) As IEnumerable(Of LotSessionEntity)
        Dim l As New List(Of LotSessionEntity)
        If nubs?.Any Then
            For Each nub As LotSessionNub In nubs
                l.Add(New LotSessionEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of LotSessionEntity)
        Dim l As New List(Of LotSessionEntity)
        If interfaces?.Any Then
            Dim nub As New LotSessionNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New LotSessionEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to session entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotSessionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSessionNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of LotSessionEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSessionBuilder.TableName}] WHERE {NameOf(LotSessionNub.PrimaryId)} = @Id", New With {Key .Id = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotSessionEntity.Populate(connection.Query(Of LotSessionNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by session. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> The total number of entities by session. </returns>
    Public Shared Function CountEntitiesBySession(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotSessionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSessionNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by sessions in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by sessions in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesBySession(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As IEnumerable(Of LotSessionEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSessionBuilder.TableName}] WHERE {NameOf(LotSessionNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotSessionEntity.Populate(connection.Query(Of LotSessionNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="sessionAutoId">       Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotSessionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSessionNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotSessionNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="sessionAutoId">       Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal sessionAutoId As Integer) As IEnumerable(Of LotSessionNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotSessionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSessionNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotSessionNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotSessionNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Lot Session exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="sessionAutoId">       Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal sessionAutoId As Integer) As Boolean
        Return 1 = LotSessionEntity.CountEntities(connection, lotAutoId, sessionAutoId)
    End Function

#End Region

#Region " RELATIONS: LOT "

    ''' <summary> Gets or sets the Lot entity. </summary>
    ''' <value> The Lot entity. </value>
    Public ReadOnly Property LotEntity As LotEntity

    ''' <summary> Fetches Lot Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Lot Entity. </returns>
    Public Function FetchLotEntity(ByVal connection As System.Data.IDbConnection) As LotEntity
        Dim entity As New LotEntity()
        entity.FetchUsingKey(connection, Me.LotAutoId)
        Me._LotEntity = entity
        Return entity
    End Function

    ''' <summary> Count lots associated with the specified <paramref name="sessionAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> The total number of lots. </returns>
    Public Shared Function CountLots(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{LotSessionBuilder.TableName}] WHERE {NameOf(LotSessionNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Lots associated with the specified <paramref name="sessionAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Lots in this collection.
    ''' </returns>
    Public Shared Function FetchLots(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As IEnumerable(Of SessionEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSessionBuilder.TableName}] WHERE {NameOf(LotSessionNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of SessionEntity)
        For Each nub As LotSessionNub In connection.Query(Of LotSessionNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotSessionEntity(nub)
            l.Add(entity.FetchSessionEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all lots associated with the specified <paramref name="sessionAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteLots(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotSessionBuilder.TableName}] WHERE {NameOf(LotSessionNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sessionAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

#End Region

#Region " RELATIONS: SESSION "

    ''' <summary> Gets or sets the session entity. </summary>
    ''' <value> The session entity. </value>
    Public ReadOnly Property SessionEntity As SessionEntity

    ''' <summary> Fetches a session Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The session Entity. </returns>
    Public Function FetchSessionEntity(ByVal connection As System.Data.IDbConnection) As SessionEntity
        Dim entity As New SessionEntity()
        entity.FetchUsingKey(connection, Me.SessionAutoId)
        Me._SessionEntity = entity
        Return entity
    End Function

    ''' <summary> Gets or sets the <see cref="IEnumerable(Of SessionEntity)"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="IEnumerable(Of SessionEntity)"/>. </value>
    Public ReadOnly Property Sessions As IEnumerable(Of SessionEntity)

    ''' <summary> Fetches the <see cref="IEnumerable(Of SessionEntity)"/> in this collection. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Sessions in this collection.
    ''' </returns>
    Public Function FetchOrderedSessions(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of SessionEntity)
        Me._Sessions = LotSessionEntity.FetchOrderedSessions(connection, Me.LotAutoId)
        Return Me.Sessions
    End Function

    Public Shared Function CountSessions(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{LotSessionBuilder.TableName}] WHERE {NameOf(LotSessionNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the sessions in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the sessions in this collection.
    ''' </returns>
    Public Shared Function FetchSessions(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of SessionEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSessionBuilder.TableName}] WHERE {NameOf(LotSessionNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of SessionEntity)
        For Each nub As LotSessionNub In connection.Query(Of LotSessionNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotSessionEntity(nub)
            l.Add(entity.FetchSessionEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all session related to the specified Lot. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteSessions(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotSessionBuilder.TableName}] WHERE {NameOf(LotSessionNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches a <see cref="Entities.SessionEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="selectQuery">  The select query. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="sessionLabel"> The session label. </param>
    ''' <returns> The session. </returns>
    Public Shared Function FetchSession(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer, ByVal sessionLabel As String) As SessionEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {lotAutoId, sessionLabel})
        ' Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.lotAutoId = lotAutoId, .sessionLabel = sessionLabel})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As SessionNub = connection.Query(Of SessionNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New SessionEntity, New SessionEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches a <see cref="Entities.SessionEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="sessionLabel"> The session label. </param>
    ''' <returns> The session. </returns>
    Public Shared Function FetchSession(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal sessionLabel As String) As SessionEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotSession] on [LotSession].SecondaryId = [Session].AutoId where [LotSession].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{SessionBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{SessionBuilder.TableName}] Inner Join [{LotSessionBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotSessionBuilder.TableName}].{NameOf(LotSessionNub.SecondaryId)} = [{SessionBuilder.TableName}].{NameOf(SessionNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{LotSessionBuilder.TableName}].{NameOf(LotSessionNub.PrimaryId)} = @{NameOf(lotAutoId)} ")
        queryBuilder.AppendLine($"  AND [{SessionBuilder.TableName}].{NameOf(SessionNub.Label)} = @{NameOf(sessionLabel)}); ")
        Return LotSessionEntity.FetchSession(connection, queryBuilder.ToString, lotAutoId, sessionLabel)
    End Function

    ''' <summary> Fetches a <see cref="Entities.SessionEntity"/>'s. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The session. </returns>
    Public Shared Function FetchOrderedSessions(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer) As IEnumerable(Of SessionEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {lotAutoId})
        ' Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.lotAutoId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return SessionEntity.Populate(connection.Query(Of SessionNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches <see cref="Entities.SessionEntity"/>'s. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The <see cref="IEnumerable(Of SessionEntity)"/>. </returns>
    Public Shared Function FetchOrderedSessions(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of SessionEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotSession] on [LotSession].SecondaryId = [Session].AutoId where [LotSession].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{SessionBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{SessionBuilder.TableName}] Inner Join [{LotSessionBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotSessionBuilder.TableName}].{NameOf(LotSessionNub.SecondaryId)} = [{SessionBuilder.TableName}].{NameOf(SessionNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{LotSessionBuilder.TableName}].{NameOf(LotSessionNub.PrimaryId)} = @{NameOf(lotAutoId)}) ")
        queryBuilder.AppendLine($"ORDER BY [{SessionBuilder.TableName}].{NameOf(SessionNub.Label)} ASC; ")
        Return LotSessionEntity.FetchOrderedSessions(connection, queryBuilder.ToString, lotAutoId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotSessionEntity.LotAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.LotEntity"/> </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/> </value>
    Public Property LotAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotSessionEntity.SessionAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the session. </summary>
    ''' <value> Identifies the session. </value>
    Public Property SessionAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
