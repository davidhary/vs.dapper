''' <summary> The Session Attribute class holing the Session trait values. </summary>
''' <remarks> David, 2020-05-29. </remarks>
Public Class SessionTrait
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSestter"> The getter setter. </param>
    Public Sub New(ByVal getterSestter As isr.Core.Constructs.IGetterSetter(Of Integer))
        MyBase.New()
        Me.GetterSetter = getterSestter
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter(Of Integer)

    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As Integer?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.GetterSetter.Setter(value.Value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

#End Region

#Region " TRAITS "

    Public Property TestType As Integer
        Get
            Return CType(Me.TraitValue(), Integer)
        End Get
        Set(value As Integer)
            Me.TraitValue() = (value)
        End Set
    End Property

    Public Property SessionNumber As Integer
        Get
            Return CType(Me.TraitValue(), Integer)
        End Get
        Set(value As Integer)
            Me.TraitValue() = (value)
        End Set
    End Property

    Public Property UsingGuard As Boolean
        Get
            Return CType(Me.TraitValue(), Boolean)
        End Get
        Set(value As Boolean)
            Me.TraitValue() = (If(value, 1, 0))
        End Set
    End Property

    Public Property FirstSerialNumber As Integer
        Get
            Return CType(Me.TraitValue(), Integer)
        End Get
        Set(value As Integer)
            Me.TraitValue() = (value)
        End Set
    End Property

    Public Property LastSerialNumber As Integer
        Get
            Return CType(Me.TraitValue(), Integer)
        End Get
        Set(value As Integer)
            Me.TraitValue() = (value)
        End Set
    End Property

    Public Property BoardNumber As Integer
        Get
            Return CType(Me.TraitValue(), Integer)
        End Get
        Set(value As Integer)
            Me.TraitValue() = (value)
        End Set
    End Property

    Public Property BoardSize As Integer
        Get
            Return CType(Me.TraitValue(), Integer)
        End Get
        Set(value As Integer)
            Me.TraitValue() = (value)
        End Set
    End Property

    Public Property BinningMode As Integer
        Get
            Return CType(Me.TraitValue(), Integer)
        End Get
        Set(value As Integer)
            Me.TraitValue() = (value)
        End Set
    End Property

    Public Property SerializationMode As Integer
        Get
            Return CType(Me.TraitValue(), Integer)
        End Get
        Set(value As Integer)
            Me.TraitValue() = (value)
        End Set
    End Property

#End Region

End Class
