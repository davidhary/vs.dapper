
Partial Public Class SessionEntity

    ''' <summary> Gets or sets the <see cref="SessionTraitEntity"/> collection. </summary>
    ''' <value> The traits. </value>
    Public ReadOnly Property Traits As SessionUniqueTraitEntityCollection

    ''' <summary>
    ''' Fetches the <see cref="SessionUniqueTraitEntityCollection"/> Natural(Integer)-value type
    ''' trait entity collection.
    ''' </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of traits. </returns>
    Public Function FetchTraits(ByVal connection As System.Data.IDbConnection) As Integer
        If Not SessionTraitTypeEntity.IsEnumerated() Then SessionTraitTypeEntity.TryFetchAll(connection)
        Me._Traits = New SessionUniqueTraitEntityCollection(Me.AutoId)
        Me._Traits.Populate(SessionTraitEntity.FetchEntities(connection, Me.AutoId))
        Return Me._Traits.Count
    End Function

End Class
