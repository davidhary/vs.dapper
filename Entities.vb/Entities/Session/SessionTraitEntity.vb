Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A session Natural(Integer)-value trait builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class SessionTraitBuilder
    Inherits OneToManyNaturalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return SessionTraitBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(SessionTraitBuilder._TableName) Then
                SessionTraitBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(SessionTraitNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = SessionBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(SessionNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = SessionTraitTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(SessionTraitTypeNub.Id)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(SessionTraitEntity.SessionAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(SessionTraitEntity.SessionTraitTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName As String = NameOf(SessionTraitEntity.Amount)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SessionTraitBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SessionTraitBuilder
        If SessionTraitBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                SessionTraitBuilder._Instance = New SessionTraitBuilder()
            End SyncLock
        End If
        Return SessionTraitBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="SessionTraitEntity"/> value table <see cref="IOneToManyNatural">interface</see>.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("SessionTrait")>
Public Class SessionTraitNub
    Inherits OneToManyNaturalNub
    Implements IOneToManyNatural

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToManyNatural
        Return New SessionTraitNub
    End Function

End Class

''' <summary> The <see cref="SessionTraitEntity"/> stores <see cref="SessionEntity"/> traits. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class SessionTraitEntity
    Inherits EntityBase(Of IOneToManyNatural, SessionTraitNub)
    Implements IOneToManyNatural

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New SessionTraitNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> the <see cref="SessionEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IOneToManyNatural)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToManyNatural, ByVal store As IOneToManyNatural)
        MyBase.New(New SessionTraitNub, cache, store)
        Me.UsingNativeTracking = String.Equals(SessionTraitBuilder.TableName, NameOf(IOneToManyNatural).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToManyNatural
        Return New SessionTraitNub
    End Function

    Public Overrides Function CreateCopy() As IOneToManyNatural
        Dim destination As IOneToManyNatural = Me.CreateNew
        SessionTraitNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="SessionTraitEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToManyNatural)
        SessionTraitNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> the <see cref="SessionEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToManyNatural)
        ' first make the copy to notify of any property change.
        SessionTraitNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId">      Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="sessionTraitTypeId"> Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal sessionTraitTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SessionTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionTraitNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = sessionAutoId})
        sqlBuilder.Where($"{NameOf(SessionTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sessionTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of SessionTraitNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId">      Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="sessionTraitTypeId"> Identifies the <see cref="SessionEntity"/>
    '''                                      value type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal sessionTraitTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As SessionTraitNub = SessionTraitEntity.FetchEntities(connection, sessionAutoId, sessionTraitTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToManyNatural) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If SessionTraitEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId">      Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="sessionTraitTypeId"> Type of the <see cref="SessionEntity"/> value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal sessionTraitTypeId As Integer) As Boolean
        Return connection.Delete(Of SessionTraitNub)(New SessionTraitNub With {.PrimaryId = sessionAutoId, .SecondaryId = sessionTraitTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="SessionEntity"/> Attribute Natural(Integer)-Value entities. </summary>
    ''' <value> the <see cref="SessionEntity"/> Attribute Natural(Integer)-Value entities. </value>
    Public ReadOnly Property SessionNaturals As IEnumerable(Of SessionTraitEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of SessionTraitEntity)
        Return If(usingNativeTracking, SessionTraitEntity.Populate(connection.GetAll(Of IOneToManyNatural)),
                                       SessionTraitEntity.Populate(connection.GetAll(Of SessionTraitNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._SessionNaturals = SessionTraitEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(SessionTraitEntity.SessionNaturals))
        Return If(Me.SessionNaturals?.Any, Me.SessionNaturals.Count, 0)
    End Function

    ''' <summary> Count SessionValues. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{SessionTraitBuilder.TableName}] WHERE {NameOf(SessionTraitNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As IEnumerable(Of SessionTraitEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{SessionTraitBuilder.TableName}] WHERE {NameOf(SessionTraitNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return SessionTraitEntity.Populate(connection.Query(Of SessionTraitNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Fetches Session traits by session auto id.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As IEnumerable(Of SessionTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SessionTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionTraitEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SessionTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of SessionValue entities. </summary>
    ''' <param name="nubs"> the <see cref="SessionEntity"/>Value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of SessionTraitNub)) As IEnumerable(Of SessionTraitEntity)
        Dim l As New List(Of SessionTraitEntity)
        If nubs?.Any Then
            For Each nub As SessionTraitNub In nubs
                l.Add(New SessionTraitEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of SessionValue entities. </summary>
    ''' <param name="interfaces"> the <see cref="SessionEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToManyNatural)) As IEnumerable(Of SessionTraitEntity)
        Dim l As New List(Of SessionTraitEntity)
        If interfaces?.Any Then
            Dim nub As New SessionTraitNub
            For Each iFace As IOneToManyNatural In interfaces
                nub.CopyFrom(iFace)
                l.Add(New SessionTraitEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Session traits buy unique index; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId">      Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="sessionTraitTypeId"> Identifies the
    '''                                       <see cref="SessionTraitTypeEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal sessionTraitTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SessionTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionTraitNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = sessionAutoId})
        sqlBuilder.Where($"{NameOf(SessionTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sessionTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches Session traits by unique index; expected single or none. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId">      Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="sessionTraitTypeId"> Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal sessionTraitTypeId As Integer) As IEnumerable(Of SessionTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SessionTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionTraitNub.PrimaryId)} = @primaryId", New With {.primaryId = sessionAutoId})
        sqlBuilder.Where($"{NameOf(SessionTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sessionTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SessionTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="SessionEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId">      Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="sessionTraitTypeId"> Identifies the
    '''                                       <see cref="SessionTraitTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal sessionTraitTypeId As Integer) As Boolean
        Return 1 = SessionTraitEntity.CountEntities(connection, sessionAutoId, sessionTraitTypeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count values associated with this session. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of specification values. </returns>
    Public Function CountSessionValues(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SessionTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionTraitNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches session Natural(Integer)-value Values by Part auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">          The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchSessionValues(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of SessionTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SessionTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionTraitNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SessionTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets or sets the <see cref="SessionEntity"/>. </summary>
    ''' <value> the <see cref="SessionEntity"/>. </value>
    Public ReadOnly Property SessionEntity As SessionEntity

    ''' <summary> Fetches a <see cref="SessionEntity"/>. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchSessionEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._SessionEntity = New SessionEntity()
        Return Me.SessionEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

    ''' <summary> Gets or sets the <see cref="SessionEntity"/>. </summary>
    ''' <value> the <see cref="SessionEntity"/>. </value>
    Public ReadOnly Property SessionNaturalTypeEntity As SessionTraitTypeEntity

    ''' <summary> Fetches a <see cref="SessionEntity"/>. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchSessionNaturalTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._SessionNaturalTypeEntity = New SessionTraitTypeEntity()
        Return Me.SessionNaturalTypeEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToManyNatural.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SessionAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="SessionEntity"/> record. </summary>
    ''' <value> Identifies the <see cref="SessionEntity"/> record. </value>
    Public Property SessionAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToManyNatural.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SessionTraitTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identity of the <see cref="SessionEntity"/> <see cref="SessionTraitTypeEntity"/>. </summary>
    ''' <value> The identity of the <see cref="SessionEntity"/> <see cref="SessionTraitTypeEntity"/>. </value>
    Public Property SessionTraitTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property


    ''' <summary> Gets or sets a Natural(Integer)-value assigned to the <see cref="SessionEntity"/> for the specific <see cref="SessionTraitTypeEntity"/>. </summary>
    ''' <value> The Natural(Integer)-value assigned to the <see cref="SessionEntity"/> for the specific <see cref="SessionTraitTypeEntity"/>. </value>
    Public Property Amount As Integer Implements IOneToManyNatural.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(ByVal value As Integer)
            If Not Double.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of <see cref="SessionTraitEntity"/>'s. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class SessionTraitEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToManyNatural, SessionTraitNub, SessionTraitEntity)

    Protected Overrides Function GetKeyForItem(item As SessionTraitEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As SessionTraitEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all Sessions from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of SessionTraitEntity))
        If entities?.Any Then
            For Each entity As SessionTraitEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return SessionTraitBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary> Collection of <see cref="SessionEntity"/> unique <see cref="SessionTraitEntity"/>'s. </summary>
''' <remarks> David, 2020-05-05. </remarks>
''' 
Public Class SessionUniqueTraitEntityCollection
    Inherits SessionTraitEntityCollection
    Implements isr.Core.Constructs.IGetterSetter(Of Integer)

#Region " CONSTRUTION "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
        Me.SessionTrait = New SessionTrait(Me)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    Public Sub New(ByVal sessionAutoId As Integer)
        Me.New
        Me.SessionAutoId = sessionAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value
    '''                       can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As SessionTraitEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.SessionTraitTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.SessionTraitTypeId)
        Me.NotifyPropertyChanged(SessionTraitTypeEntity.EntityLookupDictionary(entity.SessionTraitTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all Sessions from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains 'NaturalType' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="sessionTraitTypeId"> Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal sessionTraitTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(sessionTraitTypeId)
    End Function

    ''' <summary>
    ''' Converts a name to a key using the
    ''' <see cref="SessionTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
    ''' extend the element Nominal Traits beyond the values of the enumeration type that is used to
    ''' populate this table.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return SessionTraitTypeEntity.KeyLookupDictionary(name)
    End Function

#End Region

#Region " GETTER/SETTER "

    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        Get
            Return Me.Getter(name)
        End Get
        Set(value As Integer?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.Setter(value.Value, name)
            End If
        End Set
    End Property

    ''' <summary> Gets the Real(Double)-value for the given <see cref="NomTraitTypeEntity.Label"/>. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable Double. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer? Implements isr.Core.Constructs.IGetterSetter(Of Integer).Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary> Set the specified element value for the given <see cref="NomTraitTypeEntity.Label"/>. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Double. </returns>
    Protected Function Setter(ByVal value As Integer, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer Implements isr.Core.Constructs.IGetterSetter(Of Integer).Setter
        Me.Setter(Me.ToKey(name), value)
        Me.NotifyPropertyChanged(name)
        Return value
    End Function

    ''' <summary> Gets or sets the session attribute. </summary>
    ''' <value> The session attribute. </value>
    Public ReadOnly Property SessionTrait As SessionTrait

#End Region

#Region " TRAIT SELECTION "

    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="sessionTraitTypeId"> Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
    ''' <returns> An SessionNaturalEntity. </returns>
    Public Function Entity(ByVal sessionTraitTypeId As Integer) As SessionTraitEntity
        Return If(Me.ContainsKey(sessionTraitTypeId), Me(Me._PrimaryKeyDictionary(sessionTraitTypeId)), New SessionTraitEntity)
    End Function

    ''' <summary>
    ''' Gets the Natural(Integer)-value for the given <see cref="SessionTraitTypeEntity.Id"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="sessionTraitTypeId"> Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
    ''' <returns> An Integer? </returns>
    Public Function Getter(ByVal sessionTraitTypeId As Integer) As Integer?
        Return If(Me.ContainsKey(sessionTraitTypeId), Me(Me._PrimaryKeyDictionary(sessionTraitTypeId)).Amount, New Integer?)
    End Function

    ''' <summary>
    ''' Set the specified element value for the given <see cref="SessionTraitTypeEntity.Id"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="sessionTraitTypeId"> Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
    ''' <param name="value">           The value. </param>
    Public Sub Setter(ByVal sessionTraitTypeId As Integer, ByVal value As Integer)
        If Me.ContainsKey(sessionTraitTypeId) Then
            Me(Me._PrimaryKeyDictionary(sessionTraitTypeId)).Amount = value
        Else
            Me.Add(New SessionTraitEntity With {.SessionAutoId = Me.SessionAutoId, .SessionTraitTypeId = sessionTraitTypeId, .Amount = value})
        End If
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="SessionEntity"/>. </summary>
    ''' <value> Identifies the <see cref="SessionEntity"/>. </value>
    Public ReadOnly Property SessionAutoId As Integer

#End Region

End Class


