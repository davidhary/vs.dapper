Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Station-Session builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class StationSessionBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return StationSessionBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(StationSessionBuilder._TableName) Then
                StationSessionBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(StationSessionNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = StationBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(StationNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = SessionBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(SessionNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(StationSessionEntity.StationAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(StationSessionEntity.SessionAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As StationSessionBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As StationSessionBuilder
        If StationSessionBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                StationSessionBuilder._Instance = New StationSessionBuilder()
            End SyncLock
        End If
        Return StationSessionBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Station Session Nub based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("StationSession")>
Public Class StationSessionNub
    Inherits OneToManyNub
    Implements IOneToMany

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToMany
        Return New StationSessionNub
    End Function

End Class

''' <summary>
''' The Station-Session Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class StationSessionEntity
    Inherits EntityBase(Of IOneToMany, StationSessionNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New StationSessionNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Station-Session interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New StationSessionNub, cache, store)
        Me.UsingNativeTracking = String.Equals(StationSessionBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToMany
        Return New StationSessionNub
    End Function


    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        StationSessionNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        StationSessionNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Station-Session interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        StationSessionNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <param name="sessionAutoId">      Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer, ByVal sessionAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As StationSessionNub = StationSessionEntity.FetchNubs(connection, stationAutoId, sessionAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.StationAutoId, Me.SessionAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.StationAutoId, Me.SessionAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer, ByVal sessionAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, stationAutoId, sessionAutoId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If StationSessionEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId">     Identifies the <see cref="StationEntity"/>. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer, ByVal sessionAutoId As Integer) As Boolean
        Return connection.Delete(Of StationSessionNub)(New StationSessionNub With {.PrimaryId = stationAutoId, .SecondaryId = sessionAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Station-Session entities. </summary>
    ''' <value> The Station-Session entities. </value>
    Public ReadOnly Property StationSessions As IEnumerable(Of StationSessionEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of StationSessionEntity)
        Return If(usingNativeTracking, StationSessionEntity.Populate(connection.GetAll(Of IOneToMany)), StationSessionEntity.Populate(connection.GetAll(Of StationSessionNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._StationSessions = StationSessionEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(StationSessionEntity.StationSessions))
        Return If(Me.StationSessions?.Any, Me.StationSessions.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of StationSessionNub)) As IEnumerable(Of StationSessionEntity)
        Dim l As New List(Of StationSessionEntity)
        If nubs?.Any Then
            For Each nub As StationSessionNub In nubs
                l.Add(New StationSessionEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of StationSessionEntity)
        Dim l As New List(Of StationSessionEntity)
        If interfaces?.Any Then
            Dim nub As New StationSessionNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New StationSessionEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to session entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StationSessionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StationSessionNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="stationAutoId">  Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As IEnumerable(Of StationSessionEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StationSessionBuilder.TableName}] WHERE {NameOf(StationSessionNub.PrimaryId)} = @Id", New With {Key .Id = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return StationSessionEntity.Populate(connection.Query(Of StationSessionNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by session. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> The total number of entities by session. </returns>
    Public Shared Function CountEntitiesBySession(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StationSessionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StationSessionNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by sessions in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by sessions in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesBySession(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As IEnumerable(Of StationSessionEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StationSessionBuilder.TableName}] WHERE {NameOf(StationSessionNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return StationSessionEntity.Populate(connection.Query(Of StationSessionNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <param name="sessionAutoId">       Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StationSessionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StationSessionNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = stationAutoId})
        sqlBuilder.Where($"{NameOf(StationSessionNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <param name="sessionAutoId">       Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer, ByVal sessionAutoId As Integer) As IEnumerable(Of StationSessionNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{StationSessionBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StationSessionNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = stationAutoId})
        sqlBuilder.Where($"{NameOf(StationSessionNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of StationSessionNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Station Session exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <param name="sessionAutoId">       Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer, ByVal sessionAutoId As Integer) As Boolean
        Return 1 = StationSessionEntity.CountEntities(connection, stationAutoId, sessionAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Station entity. </summary>
    ''' <value> The Station entity. </value>
    Public ReadOnly Property StationEntity As StationEntity

    ''' <summary> Fetches Station Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Station Entity. </returns>
    Public Function FetchStationEntity(ByVal connection As System.Data.IDbConnection) As StationEntity
        Dim entity As New StationEntity()
        entity.FetchUsingKey(connection, Me.StationAutoId)
        Me._StationEntity = entity
        Return entity
    End Function

    ''' <summary> Count Stations associated with the specified <paramref name="sessionAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> The total number of Stations. </returns>
    Public Shared Function CountStations(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{StationSessionBuilder.TableName}] WHERE {NameOf(StationSessionNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Stations associated with the specified <paramref name="sessionAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Stations in this collection.
    ''' </returns>
    Public Shared Function FetchStations(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As IEnumerable(Of SessionEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StationSessionBuilder.TableName}] WHERE {NameOf(StationSessionNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of SessionEntity)
        For Each nub As StationSessionNub In connection.Query(Of StationSessionNub)(selector.RawSql, selector.Parameters)
            Dim entity As New StationSessionEntity(nub)
            l.Add(entity.FetchSessionEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Stations associated with the specified <paramref name="sessionAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteStations(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{StationSessionBuilder.TableName}] WHERE {NameOf(StationSessionNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sessionAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the session entity. </summary>
    ''' <value> The session entity. </value>
    Public ReadOnly Property SessionEntity As SessionEntity

    ''' <summary> Fetches a session Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The session Entity. </returns>
    Public Function FetchSessionEntity(ByVal connection As System.Data.IDbConnection) As SessionEntity
        Dim entity As New SessionEntity()
        entity.FetchUsingKey(connection, Me.SessionAutoId)
        Me._SessionEntity = entity
        Return entity
    End Function

    Public Shared Function CountSessions(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{StationSessionBuilder.TableName}] WHERE {NameOf(StationSessionNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the sessions in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="stationAutoId"> Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the sessions in this collection.
    ''' </returns>
    Public Shared Function FetchSessions(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As IEnumerable(Of SessionEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StationSessionBuilder.TableName}] WHERE {NameOf(StationSessionNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = stationAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of SessionEntity)
        For Each nub As StationSessionNub In connection.Query(Of StationSessionNub)(selector.RawSql, selector.Parameters)
            Dim entity As New StationSessionEntity(nub)
            l.Add(entity.FetchSessionEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all session related to the specified Station. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="stationAutoId">  Identifies the <see cref="StationEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteSessions(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{StationSessionBuilder.TableName}] WHERE {NameOf(StationSessionNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = stationAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(StationSessionEntity.StationAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(StationSessionEntity.SessionAutoId))
            End If
        End Set
    End Property

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Station. </summary>
    ''' <value> Identifies the Station. </value>
    Public Property StationAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the session. </summary>
    ''' <value> Identifies the session. </value>
    Public Property SessionAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
