Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Core

''' <summary> A Lot-Meter-Element-Nominal-Sample builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class LotSampleBuilder
    Inherits ThreeToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return LotSampleBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(LotSampleBuilder._TableName) Then
                LotSampleBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(LotSampleNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = LotBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(LotNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = MultimeterBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(MultimeterNub.Id)

    ''' <summary> Gets or sets the name of the Ternary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Ternary table. </value>
    Public Overrides Property TernaryTableName As String = ElementBuilder.TableName

    ''' <summary> Gets or sets the name of the Ternary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Ternary table key. </value>
    Public Overrides Property TernaryTableKeyName As String = NameOf(ElementNub.AutoId)

    ''' <summary> Gets or sets the name of the Quaternary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Quaternary table. </value>
    Public Overrides Property QuaternaryTableName As String = SampleTraitBuilder.TableName

    ''' <summary> Gets or sets the name of the Quaternary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Quaternary table key. </value>
    Public Overrides Property QuaternaryTableKeyName As String = NameOf(SampleTraitNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(LotSampleEntity.LotAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(LotSampleEntity.MultimeterId)

    ''' <summary> Gets or sets the name of the ternary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the ternary identifier field. </value>
    Public Overrides Property TernaryIdFieldName As String = NameOf(LotSampleEntity.ElementAutoId)

    ''' <summary> Gets or sets the name of the quaternary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the quaternary identifier field. </value>
    Public Overrides Property QuaternaryIdFieldName As String = NameOf(LotSampleEntity.SampleTraitAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As LotSampleBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As LotSampleBuilder
        If LotSampleBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                LotSampleBuilder._Instance = New LotSampleBuilder()
            End SyncLock
        End If
        Return LotSampleBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="LotSampleNub"/> based on the
''' <see cref="IThreeToMany">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
<Table("LotSample")>
Public Class LotSampleNub
    Inherits ThreeToManyNub
    Implements IThreeToMany

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IThreeToMany
        Return New LotSampleNub
    End Function

End Class

''' <summary>
''' The <see cref="LotSampleEntity"/> based on the
''' <see cref="IThreeToMany">interface</see>.
''' </summary>
''' <remarks> David, 6/16/2020. </remarks>
Public Class LotSampleEntity
    Inherits EntityBase(Of IThreeToMany, LotSampleNub)
    Implements IThreeToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New LotSampleNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Lot-Element interface. </param>
    Public Sub New(ByVal value As IThreeToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IThreeToMany, ByVal store As IThreeToMany)
        MyBase.New(New LotSampleNub, cache, store)
        Me.UsingNativeTracking = String.Equals(LotSampleBuilder.TableName, NameOf(IThreeToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IThreeToMany
        Return New LotSampleNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IThreeToMany
        Dim destination As IThreeToMany = Me.CreateNew
        LotSampleNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IThreeToMany)
        LotSampleNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Lot-Element interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IThreeToMany)
        ' first make the copy to notify of any property change.
        LotSampleNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches a <see cref="LotSampleEntity"/> using the entity key. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection">            The connection. </param>
    ''' <param name="lotAutoId">             Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="elementAutoId">         Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="sampleAttributeAutoId"> Identifies the <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                            ByVal elementAutoId As Integer, ByVal sampleAttributeAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As LotSampleNub = LotSampleEntity.FetchNubs(connection, lotAutoId, elementAutoId, sampleAttributeAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Fetches a <see cref="LotSampleEntity"/> using the entity key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.LotAutoId, Me.ElementAutoId, Me.SampleTraitAutoId)
    End Function

    ''' <summary>
    ''' Fetches an existing <see cref="LotSampleEntity"/> using the entity unique index.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.LotAutoId, Me.ElementAutoId, Me.SampleTraitAutoId)
    End Function

    ''' <summary>
    ''' Fetches an existing <see cref="LotSampleEntity"/> using the entity unique index.
    ''' </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">            The connection. </param>
    ''' <param name="lotAutoId">             Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="elementAutoId">         Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="sampleAttributeAutoId"> Identifies the <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                                    ByVal elementAutoId As Integer, ByVal sampleAttributeAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, lotAutoId, elementAutoId, sampleAttributeAutoId)
    End Function

    ''' <summary>
    ''' Tries to fetch an existing <see cref="Entities.SampleTraitEntity"/> associated with a
    ''' <see cref="Entities.LotEntity"/> and <see cref="Entities.ElementEntity"/>;
    ''' otherwise, inserts a new <see cref="Entities.SampleTraitEntity"/>. Then tries to fetch an
    ''' existing or insert a new
    ''' <see cref="LotSampleEntity"/>.
    ''' </summary>
    ''' <remarks>
    ''' Assumes that a <see cref="Entities.LotEntity"/> exists for the specified
    ''' <paramref name="lotAutoId"/>
    ''' Assumes that a <see cref="Entities.ElementEntity"/> exists for the specified
    ''' <paramref name="elementAutoId"/>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="lotAutoId">         Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">      Identifies the
    '''                                  <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">         Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="sampleTraitTypeId"> Identifies the <see cref="Entities.SampleTraitType"/>. </param>
    ''' <param name="amount">            The amount. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainSampleTrait(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                             ByVal multimeterId As Integer, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer,
                                             ByVal sampleTraitTypeId As Integer, ByVal amount As Double) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._SampleTraitEntity = LotSampleEntity.FetchSampleTrait(connection, lotAutoId, multimeterId, elementAutoId, nomTypeId, sampleTraitTypeId)
        If Not Me._SampleTraitEntity.IsClean Then
            Me._SampleTraitEntity = New SampleTraitEntity With {.SampleTraitTypeId = sampleTraitTypeId, .NomTypeId = nomTypeId, .Amount = amount}
            If Not Me.SampleTraitEntity.Insert(connection) Then
                result = (False, $"Failed inserting {NameOf(Entities.SampleTraitEntity)} with {NameOf(Entities.SampleTraitEntity.SampleTraitTypeId)} of {sampleTraitTypeId}")
            End If
        End If
        If result.Success Then
            Me.PrimaryId = lotAutoId
            Me.SecondaryId = elementAutoId
            Me.TernaryId = Me.SampleTraitEntity.AutoId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(LotSampleEntity.SampleTraitEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.LotSampleEntity)} with {NameOf(Entities.LotSampleEntity.LotAutoId)} of {lotAutoId} and {NameOf(Entities.LotSampleEntity.ElementAutoId)} of {elementAutoId} and {NameOf(Entities.LotSampleEntity.SampleTraitAutoId)} of {Me.SampleTraitEntity.AutoId}")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing <see cref="Entities.LotEntity"/> and
    ''' <see cref="Entities.ElementEntity"/> and fetches or inserts a new
    ''' <see cref="Entities.SampleTraitEntity"/> and fetches an existing or inserts a new
    ''' <see cref="LotSampleEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="lotAutoId">         Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">      Identifies the
    '''                                  <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">         Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="sampleTraitTypeId"> Identifies the <see cref="Entities.SampleTraitType"/>. </param>
    ''' <param name="amount">            The amount. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal multimeterId As Integer, ByVal elementAutoId As Integer,
                              ByVal nomTypeId As Integer, ByVal sampleTraitTypeId As Integer, ByVal amount As Double) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.LotEntity Is Nothing OrElse Me.LotEntity.AutoId <> lotAutoId Then
            ' make sure we have a lot associated with the lot auto ID.
            Me._LotEntity = New LotEntity With {.AutoId = lotAutoId}
            If Not Me._LotEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.AutoId)} of {lotAutoId}")
            End If
        End If
        If Me.ElementEntity Is Nothing OrElse Me.ElementEntity.AutoId <> elementAutoId Then
            ' make sure we have an element associated with the element auto ID.
            Me._ElementEntity = New ElementEntity With {.AutoId = elementAutoId}
            If Not Me._ElementEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.ElementEntity)} with {NameOf(Entities.ElementEntity.AutoId)} of {elementAutoId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainSampleTrait(connection, lotAutoId, multimeterId, elementAutoId, nomTypeId, sampleTraitTypeId, amount)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(LotSampleEntity.LotEntity))
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing <see cref="Entities.LotEntity"/> and
    ''' <see cref="Entities.ElementEntity"/> and fetches or inserts a new
    ''' <see cref="Entities.SampleTraitEntity"/> and fetches an existing or inserts a new
    ''' <see cref="LotSampleEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="nomTypeId">         Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="sampleTraitTypeId"> Identifies the <see cref="Entities.SampleTraitType"/>. </param>
    ''' <param name="amount">            The amount. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal nomTypeId As Integer, ByVal sampleTraitTypeId As Integer,
                              ByVal amount As Double) As (Success As Boolean, Details As String)
        Return Me.TryObtain(connection, Me.LotAutoId, Me.MultimeterId, Me.ElementAutoId, nomTypeId, sampleTraitTypeId, amount)
    End Function

    ''' <summary>
    ''' Tries to fetch existing <see cref="Entities.LotEntity"/> and
    ''' <see cref="Entities.ElementEntity"/> and fetches or inserts a new
    ''' <see cref="Entities.SampleTraitEntity"/> and fetches an existing or inserts a new
    ''' <see cref="LotSampleEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="lotAutoId">         Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">      Identifies the
    '''                                  <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">         Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="sampleTraitTypeId"> Identifies the <see cref="Entities.SampleTraitType"/>. </param>
    ''' <param name="amount">            The amount. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Function Obtain(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal multimeterId As Integer,
                                     ByVal elementAutoId As Integer, ByVal nomTypeId As Integer, ByVal sampleTraitTypeId As Integer, ByVal amount As Double) As Boolean
        Dim r As (Success As Boolean, Details As String) = Me.TryObtain(connection, lotAutoId, multimeterId, elementAutoId, nomTypeId, sampleTraitTypeId, amount)
        If Not r.Success Then Throw New OperationFailedException(r.Details)
        Return r.Success
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IThreeToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If LotSampleEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId, entity.TernaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId">     Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal elementAutoId As Integer) As Boolean
        Return connection.Delete(Of LotSampleNub)(New LotSampleNub With {.PrimaryId = lotAutoId, .SecondaryId = elementAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="LotSampleEntity"/>'s. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Lot-Element entities. </value>
    Public ReadOnly Property LotMeterElementSamples As IEnumerable(Of LotSampleEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of LotSampleEntity)
        Return If(usingNativeTracking, LotSampleEntity.Populate(connection.GetAll(Of IThreeToMany)), LotSampleEntity.Populate(connection.GetAll(Of LotSampleNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._LotMeterElementSamples = LotSampleEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(LotSampleEntity.LotMeterElementSamples))
        Return If(Me.LotMeterElementSamples?.Any, Me.LotMeterElementSamples.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of LotSampleNub)) As IEnumerable(Of LotSampleEntity)
        Dim l As New List(Of LotSampleEntity)
        If nubs?.Any Then
            For Each nub As LotSampleNub In nubs
                l.Add(New LotSampleEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IThreeToMany)) As IEnumerable(Of LotSampleEntity)
        Dim l As New List(Of LotSampleEntity)
        If interfaces?.Any Then
            Dim nub As New LotSampleNub
            For Each iFace As IThreeToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New LotSampleEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>
    ''' Count <see cref="LotSampleEntity"/>'s by <see cref="Entities.LotEntity"/>; returns up to
    ''' Element entities count.
    ''' </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotSampleBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSampleNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the <see cref="LotSampleEntity"/>'s by <see cref="Entities.LotEntity"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of LotSampleEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSampleBuilder.TableName}] WHERE {NameOf(LotSampleNub.PrimaryId)} = @lotAutoId", New With {Key lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotSampleEntity.Populate(connection.Query(Of LotSampleNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Count <see cref="LotSampleEntity"/>'s by <see cref="Entities.ElementEntity"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of entities by Element. </returns>
    Public Shared Function CountEntitiesByElement(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotSampleBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSampleNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the <see cref="LotSampleEntity"/>'s by Elements in this collection.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Elements in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByElement(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of LotSampleEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSampleBuilder.TableName}] WHERE {NameOf(LotSampleNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotSampleEntity.Populate(connection.Query(Of LotSampleNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count <see cref="LotSampleEntity"/>'s; returns 1 or 0. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">            The connection. </param>
    ''' <param name="lotAutoId">             Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="elementAutoId">         Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="sampleAttributeAutoId"> Identifies the <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                         ByVal elementAutoId As Integer, ByVal sampleAttributeAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotSampleBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSampleNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotSampleNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(LotSampleNub.TernaryId)} = @TernaryId", New With {.TernaryId = sampleAttributeAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches <see cref="LotSampleNub"/>'s; expects single entity or none. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">            The connection. </param>
    ''' <param name="lotAutoId">             Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="elementAutoId">         Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="sampleAttributeAutoId"> Identifies the <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                     ByVal elementAutoId As Integer, ByVal sampleAttributeAutoId As Integer) As IEnumerable(Of LotSampleNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotSampleBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSampleNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotSampleNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = elementAutoId})
        sqlBuilder.Where($"{NameOf(LotSampleNub.TernaryId)} = @TernaryId", New With {.TernaryId = sampleAttributeAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotSampleNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="LotSampleEntity"/> exists. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <param name="connection">            The connection. </param>
    ''' <param name="lotAutoId">             Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="elementAutoId">         Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="sampleAttributeAutoId"> Identifies the <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                    ByVal elementAutoId As Integer, ByVal sampleAttributeAutoId As Integer) As Boolean
        Return 1 = LotSampleEntity.CountEntities(connection, lotAutoId, elementAutoId, sampleAttributeAutoId)
    End Function

#End Region

#Region " RELATIONS: LOT "

    ''' <summary> Gets or sets the <see cref="Entities.LotEntity"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Lot entity. </value>
    Public ReadOnly Property LotEntity As LotEntity

    ''' <summary> Fetches  <see cref="Entities.LotEntity"/>. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Lot Entity. </returns>
    Public Function FetchLotEntity(ByVal connection As System.Data.IDbConnection) As LotEntity
        Dim entity As New LotEntity()
        entity.FetchUsingKey(connection, Me.LotAutoId)
        Me._LotEntity = entity
        Return entity
    End Function

    ''' <summary>
    ''' Count <see cref="Entities.LotEntity"/>'s associated with the specified
    ''' <paramref name="elementAutoId"/>; expected 1.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of Lots. </returns>
    Public Shared Function CountLots(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{LotSampleBuilder.TableName}] WHERE {NameOf(LotSampleNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the <see cref="Entities.LotEntity"/>'s associated with the specified
    ''' <paramref name="elementAutoId"/>; expected a single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Lots in this collection.
    ''' </returns>
    Public Shared Function FetchLots(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSampleBuilder.TableName}] WHERE {NameOf(LotSampleNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ElementEntity)
        For Each nub As LotSampleNub In connection.Query(Of LotSampleNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotSampleEntity(nub)
            l.Add(entity.FetchElementEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Lots associated with the specified <paramref name="elementAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteLots(ByVal connection As System.Data.IDbConnection, ByVal elementAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotSampleBuilder.TableName}] WHERE {NameOf(LotSampleNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = elementAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

#End Region

#Region " RELATIONS: MULTIMETER "

    ''' <summary> Gets or sets the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="Entities.MultimeterEntity"/>. </value>
    Public ReadOnly Property MultimeterEntity As MultimeterEntity

    ''' <summary> Fetches a Multimeter Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The <see cref="Entities.MultimeterEntity"/>. </returns>
    Public Function FetchMultimeterEntity(ByVal connection As System.Data.IDbConnection) As MultimeterEntity
        Dim entity As New MultimeterEntity()
        entity.FetchUsingKey(connection, Me.MultimeterId)
        Me._MultimeterEntity = entity
        Return entity
    End Function

    ''' <summary> Count Multimeters. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The total number of Multimeters. </returns>
    Public Shared Function CountMultimeters(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{LotSampleBuilder.TableName}] WHERE {NameOf(LotSampleNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Multimeters in this collection. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Multimeters in this collection.
    ''' </returns>
    Public Shared Function FetchMultimeters(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of MultimeterEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSampleBuilder.TableName}] WHERE {NameOf(LotSampleNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of MultimeterEntity)
        For Each nub As LotSampleNub In connection.Query(Of LotSampleNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotSampleEntity(nub)
            l.Add(entity.FetchMultimeterEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Multimeter related to the specified Lot. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteMultimeters(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotSampleBuilder.TableName}] WHERE {NameOf(LotSampleNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.MultimeterEntity"/>'s. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered Multimeters in this
    ''' collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchOrderedMultimeters(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer) As IEnumerable(Of MultimeterEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .LotAutoId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return MultimeterEntity.Populate(connection.Query(Of MultimeterNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.MultimeterEntity"/>'s. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered Multimeters in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchOrderedMultimeters(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of MultimeterEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotMeterElementSample] on [LotMeterElementSample].SecondaryId = [Multimeter].AutoId where [LotMeterElementSample].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{MultimeterBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{MultimeterBuilder.TableName}] Inner Join [{LotSampleBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotSampleBuilder.TableName}].{NameOf(LotSampleNub.SecondaryId)} = [{MultimeterBuilder.TableName}].{NameOf(MultimeterNub.Id)}")
        queryBuilder.AppendLine($"WHERE [{LotSampleBuilder.TableName}].{NameOf(LotSampleNub.PrimaryId)} = @LotAutoId")
        queryBuilder.AppendLine($"ORDER BY [{MultimeterBuilder.TableName}].{NameOf(MultimeterNub.Id)} ASC; ")
        Return LotSampleEntity.FetchOrderedMultimeters(connection, queryBuilder.ToString, lotAutoId)
    End Function

    ''' <summary> Fetches an <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="selectQuery">      The select query. </param>
    ''' <param name="lotAutoId">        Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterNumber"> The <see cref="Entities.MultimeterEntity"/> number. </param>
    ''' <returns> The Multimeter. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchMultimeter(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer, ByVal multimeterNumber As Integer) As MultimeterEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.LotAutoId = lotAutoId, .MultimeterLabel = multimeterNumber})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As MultimeterNub = connection.Query(Of MultimeterNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New MultimeterEntity, New MultimeterEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <remarks>
    ''' David, 5/19/2020.
    ''' <code>
    ''' SELECT [Multimeter].*
    ''' FROM [Multimeter] Inner Join [LotMeterElementSample]
    ''' ON [LotMeterElementSample].[SecondaryId] = [Multimeter].[Id]
    ''' WHERE ([LotMeterElementSample].[PrimaryId] = 8 AND [Multimeter].[Number] = 1)
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="lotAutoId">        Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterNumber"> The <see cref="Entities.MultimeterEntity"/>number. </param>
    ''' <returns> The Multimeter. </returns>
    Public Shared Function FetchMultimeter(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal multimeterNumber As Integer) As MultimeterEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotMeterElementSample] on [LotMeterElementSample].SecondaryId = [Multimeter].AutoId where [LotMeterElementSample].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{MultimeterBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{MultimeterBuilder.TableName}] Inner Join [{LotSampleBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotSampleBuilder.TableName}].[{NameOf(LotSampleNub.SecondaryId)}] = [{MultimeterBuilder.TableName}].[{NameOf(MultimeterNub.Id)}]")
        queryBuilder.AppendLine($"WHERE ([{LotSampleBuilder.TableName}].[{NameOf(LotSampleNub.PrimaryId)}] = @LotAutoId ")
        queryBuilder.AppendLine($"AND [{MultimeterBuilder.TableName}].[{NameOf(MultimeterNub.Amount)}] = @MultimeterNumber); ")
        Return LotSampleEntity.FetchMultimeter(connection, queryBuilder.ToString, lotAutoId, multimeterNumber)
    End Function

#End Region

#Region " RELATIONS: ELEMENT "

    ''' <summary> Gets or sets the Element entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Element entity. </value>
    Public ReadOnly Property ElementEntity As ElementEntity

    ''' <summary> Fetches a Element Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Element Entity. </returns>
    Public Function FetchElementEntity(ByVal connection As System.Data.IDbConnection) As ElementEntity
        Dim entity As New ElementEntity()
        entity.FetchUsingKey(connection, Me.ElementAutoId)
        Me._ElementEntity = entity
        Return entity
    End Function

    ''' <summary> Count elements. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The total number of elements. </returns>
    Public Shared Function CountElements(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{LotSampleBuilder.TableName}] WHERE {NameOf(LotSampleNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Elements in this collection. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Elements in this collection.
    ''' </returns>
    Public Shared Function FetchElements(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSampleBuilder.TableName}] WHERE {NameOf(LotSampleNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ElementEntity)
        For Each nub As LotSampleNub In connection.Query(Of LotSampleNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotSampleEntity(nub)
            l.Add(entity.FetchElementEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Element related to the specified Lot. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteElements(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotSampleBuilder.TableName}] WHERE {NameOf(LotSampleNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.ElementEntity"/>'s. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered elements in this
    ''' collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchOrderedElements(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .LotAutoId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return ElementEntity.Populate(connection.Query(Of ElementNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.ElementEntity"/>'s. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered elements in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchOrderedElements(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotMeterElementSample] on [LotMeterElementSample].SecondaryId = [Element].AutoId where [LotMeterElementSample].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{ElementBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{ElementBuilder.TableName}] Inner Join [{LotSampleBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotSampleBuilder.TableName}].{NameOf(LotSampleNub.SecondaryId)} = [{ElementBuilder.TableName}].{NameOf(ElementNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{LotSampleBuilder.TableName}].{NameOf(LotSampleNub.PrimaryId)} = @LotAutoId")
        queryBuilder.AppendLine($"ORDER BY [{ElementBuilder.TableName}].{NameOf(ElementNub.Label)} ASC; ")
        Return LotSampleEntity.FetchOrderedElements(connection, queryBuilder.ToString, lotAutoId)
    End Function

    ''' <summary> Fetches an <see cref="Entities.ElementEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="selectQuery">  The select query. </param>
    ''' <param name="lotAutoId">    Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="elementLabel"> The <see cref="Entities.ElementEntity"/>label. </param>
    ''' <returns> The element. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchElement(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer, ByVal elementLabel As String) As ElementEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.LotAutoId = lotAutoId, .ElementLabel = elementLabel})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As ElementNub = connection.Query(Of ElementNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New ElementEntity, New ElementEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an <see cref="Entities.ElementEntity"/>. </summary>
    ''' <remarks>
    ''' David, 5/19/2020.
    ''' <code>
    ''' SELECT [Element].*
    ''' FROM [Element] Inner Join [LotMeterElementSample]
    ''' ON [LotMeterElementSample].[SecondaryId] = [Element].[AutoId]
    ''' WHERE ([LotMeterElementSample].[PrimaryId] = 8 AND [Element].[Label] = 'R1')
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="lotAutoId">    Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="elementLabel"> The <see cref="Entities.ElementEntity"/>label. </param>
    ''' <returns> The element. </returns>
    Public Shared Function FetchElement(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal elementLabel As String) As ElementEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotMeterElementSample] on [LotMeterElementSample].SecondaryId = [Element].AutoId where [LotMeterElementSample].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{ElementBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{ElementBuilder.TableName}] Inner Join [{LotSampleBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotSampleBuilder.TableName}].[{NameOf(LotSampleNub.SecondaryId)}] = [{ElementBuilder.TableName}].[{NameOf(ElementNub.AutoId)}]")
        queryBuilder.AppendLine($"WHERE ([{LotSampleBuilder.TableName}].[{NameOf(LotSampleNub.PrimaryId)}] = @LotAutoId ")
        queryBuilder.AppendLine($"AND [{ElementBuilder.TableName}].[{NameOf(ElementNub.Label)}] = @ElementLabel); ")
        Return LotSampleEntity.FetchElement(connection, queryBuilder.ToString, lotAutoId, elementLabel)
    End Function

#End Region

#Region " RELATIONS: SAMPLE TRAITS "

    ''' <summary> Gets or sets the <see cref="Entities.SampleTraitEntity"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="Entities.SampleTraitEntity"/>. </value>
    Public ReadOnly Property SampleTraitEntity As SampleTraitEntity

    ''' <summary> Fetches a <see cref="Entities.SampleTraitEntity"/>. </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The <see cref="Entities.SampleTraitEntity"/>. </returns>
    Public Function FetchSampleTraitEntity(ByVal connection As System.Data.IDbConnection) As SampleTraitEntity
        Me._SampleTraitEntity = New SampleTraitEntity With {.AutoId = Me.SampleTraitAutoId}
        Me._SampleTraitEntity.FetchUsingKey(connection)
        Return Me._SampleTraitEntity
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.SampleTraitEntity"/>'s. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="selectQuery">   The select query. </param>
    ''' <param name="lotAutoId">     Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered
    ''' <see cref="Entities.SampleTraitEntity"/>'s in this collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchOrderedSampleTraits(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String,
                                                    ByVal lotAutoId As Integer, ByVal multimeterId As Integer, ByVal elementAutoId As Integer) As IEnumerable(Of SampleTraitEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.LotAutoId = lotAutoId,
                                                                                                     .MultimeterId = multimeterId,
                                                                                                     .ElementAutoId = elementAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return SampleTraitEntity.Populate(connection.Query(Of SampleTraitNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.SampleTraitEntity"/>'s. </summary>
    ''' <remarks>
    ''' David, 5/9/2020.
    ''' <code>
    ''' SELECT [SampleTrait].*
    ''' FROM [SampleTrait] Inner Join [LotMeterElementSample]
    ''' ON [LotMeterElementSample].[TernaryId] = [SampleTrait].[AutoId]
    ''' WHERE ([LotMeterElementSample].[PrimaryId] = 8 AND [LotMeterElementSample].[SecondaryId] = 1)
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId">     Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered
    ''' <see cref="Entities.SampleTraitEntity"/>'s in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedSampleTraits(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                                    ByVal multimeterId As Integer, ByVal elementAutoId As Integer) As IEnumerable(Of SampleTraitEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotMeterElementSample] on [LotMeterElementSample].SecondaryId = [SampleAttribute].AutoId where [LotMeterElementSample].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{SampleTraitBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{SampleTraitBuilder.TableName}] Inner Join [{LotSampleBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotSampleBuilder.TableName}].{NameOf(LotSampleNub.TernaryId)} = [{SampleTraitBuilder.TableName}].{NameOf(SampleTraitNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{LotSampleBuilder.TableName}].[{NameOf(LotSampleNub.PrimaryId)}] = @{NameOf(lotAutoId)} ")
        queryBuilder.AppendLine($"AND [{LotSampleBuilder.TableName}].[{NameOf(LotSampleNub.SecondaryId)}] = @{NameOf(multimeterId)} ")
        queryBuilder.AppendLine($"AND [{LotSampleBuilder.TableName}].[{NameOf(LotSampleNub.TernaryId)}] = @{NameOf(elementAutoId)}) ")
        queryBuilder.AppendLine($"ORDER BY [{SampleTraitBuilder.TableName}].{NameOf(SampleTraitNub.FirstForeignId)} ASC ")
        queryBuilder.AppendLine($" [{SampleTraitBuilder.TableName}].{NameOf(SampleTraitNub.SecondForeignId)} ASC; ")
        Return LotSampleEntity.FetchOrderedSampleTraits(connection, queryBuilder.ToString, lotAutoId, multimeterId, elementAutoId)
    End Function

    ''' <summary> Fetches an <see cref="Entities.SampleTraitEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="selectQuery">   The select query. </param>
    ''' <param name="lotAutoId">     Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="sampleTraitId"> Identifies the sample
    '''                              <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns> The sampleAttribute. </returns>
    Public Shared Function FetchSampleTrait(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer,
                                            ByVal multimeterId As Integer, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer,
                                            ByVal sampleTraitId As Integer) As SampleTraitEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {lotAutoId, multimeterId, elementAutoId,
                                                                                                     nomTypeId, sampleTraitId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As SampleTraitNub = connection.Query(Of SampleTraitNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New SampleTraitEntity, New SampleTraitEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an <see cref="Entities.SampleTraitEntity"/>. </summary>
    ''' <remarks>
    ''' David, 6/16/2020.
    ''' <code>
    ''' SELECT [SampleTrait].*
    ''' FROM [SampleTrait] Inner Join [LotMeterElementSample]
    ''' ON [LotMeterElementSample].[TernaryId] = [SampleTrait].[AutoId]
    ''' WHERE ([LotMeterElementSample].[PrimaryId] = 8 AND [SampleTrait].[FirstForeignId] = 1 AND [SampleTrait].[SecondForeignId] = 1 )
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="lotAutoId">         Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">      Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">         Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="sampleTraitTypeId"> Identifies the
    '''                                      <see cref="SampleTraitTypeEntity"/>. </param>
    ''' <returns> The sampleAttribute. </returns>
    Public Shared Function FetchSampleTrait(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                            ByVal multimeterId As Integer, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer,
                                            ByVal sampleTraitTypeId As Integer) As SampleTraitEntity
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"SELECT [{SampleTraitBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{SampleTraitBuilder.TableName}] Inner Join [{LotSampleBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotSampleBuilder.TableName}].[{NameOf(LotSampleNub.TernaryId)}] = [{SampleTraitBuilder.TableName}].[{NameOf(SampleTraitNub.AutoId)}]")
        queryBuilder.AppendLine($"WHERE ([{LotSampleBuilder.TableName}].[{NameOf(LotSampleNub.PrimaryId)}] = @L{NameOf(lotAutoId)} ")
        queryBuilder.AppendLine($"AND [{LotSampleBuilder.TableName}].[{NameOf(LotSampleNub.SecondaryId)}] = @{NameOf(multimeterId)} ")
        queryBuilder.AppendLine($"AND [{LotSampleBuilder.TableName}].[{NameOf(LotSampleNub.TernaryId)}] = @{NameOf(elementAutoId)} ")
        queryBuilder.AppendLine($"AND [{SampleTraitBuilder.TableName}].[{NameOf(SampleTraitNub.FirstForeignId)}] = @{NameOf(nomTypeId)} ")
        queryBuilder.AppendLine($"AND [{SampleTraitBuilder.TableName}].[{NameOf(SampleTraitNub.SecondForeignId)}] = @{NameOf(sampleTraitTypeId)}); ")
        Return LotSampleEntity.FetchSampleTrait(connection, queryBuilder.ToString, lotAutoId, multimeterId, elementAutoId, nomTypeId, sampleTraitTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IThreeToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotSampleEntity.LotAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.LotEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/>. </value>
    Public Property LotAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IThreeToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotSampleEntity.MultimeterId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/>.
    ''' </summary>
    ''' <value> Identifies the "Entities.MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Ternary reference. </summary>
    ''' <value> The identifier of Ternary reference. </value>
    Public Property TernaryId As Integer Implements IThreeToMany.TernaryId
        Get
            Return Me.ICache.TernaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.TernaryId, value) Then
                Me.ICache.TernaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotSampleEntity.ElementAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the "Entities.ElementEntity"/>. </value>
    Public Property ElementAutoId As Integer
        Get
            Return Me.TernaryId
        End Get
        Set(value As Integer)
            Me.TernaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Quaternary reference. </summary>
    ''' <value> The identifier of Quaternary reference. </value>
    Public Property QuaternaryId As Integer Implements IThreeToMany.QuaternaryId
        Get
            Return Me.ICache.QuaternaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.QuaternaryId, value) Then
                Me.ICache.QuaternaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotSampleEntity.SampleTraitAutoId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.SampleTraitEntity"/>.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.SampleTraitEntity"/>. </value>
    Public Property SampleTraitAutoId As Integer
        Get
            Return Me.QuaternaryId
        End Get
        Set(value As Integer)
            Me.QuaternaryId = value
        End Set
    End Property

#End Region

End Class

''' <summary>
''' Collection of unique <see cref="Entities.ElementEntity"/> +
''' <see cref="Entities.MultimeterEntity"/> +
''' <see cref="Entities.NomTypeEntity"/> <see cref="Entities.SampleTraitEntity"/>'s.
''' </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class LotSampleTraitEntityCollection
    Inherits SampleTraitEntityCollection
    Implements isr.Core.Constructs.IGetterSetter(Of Double)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 6/10/2020. </remarks>
    ''' <param name="lot">        The <see cref="LotEntity"/>. </param>
    ''' <param name="element">    The <see cref="ElementEntity"/>. </param>
    ''' <param name="nomTypeId">  Identifier for the <see cref="NomTypeEntity"/>. </param>
    ''' <param name="multimeter"> The <see cref="MultimeterEntity"/>. </param>
    Public Sub New(ByVal lot As LotEntity, ByVal element As ElementEntity, ByVal nomTypeId As Integer, ByVal multimeter As MultimeterEntity)
        MyBase.New()
        Me.LotAutoId = lot.AutoId
        Me._UniqueIndexDictionary = New Dictionary(Of ThreeKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, ThreeKeySelector)
        Me._SampleTrait = New SampleTrait(Me, element, nomTypeId, multimeter, lot.Traits.LotTrait.CertifiedQuantity)
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of ThreeKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, ThreeKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As SampleTraitEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.SampleTraitTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.SampleTraitTypeId)
        Me.NotifyPropertyChanged(SampleTraitTypeEntity.EntityLookupDictionary(entity.SampleTraitTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains 'TraitType' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="sampleTraitTypeId"> Identifies the <see cref="SampleTraitTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsAttribute(ByVal sampleTraitTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(sampleTraitTypeId)
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.LotEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/>. </value>
    Public ReadOnly Property LotAutoId As Integer

#End Region

#Region " GETTER SETTER "

    ''' <summary>
    ''' Gets the Real(Double)-value for the given <see cref="SampleTraitTypeEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable Double. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double? Implements Core.Constructs.IGetterSetter(Of Double).Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary>
    ''' Set the specified element value for the given <see cref="SampleTraitTypeEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value">                                      value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Double. </returns>
    Protected Function Setter(ByVal value As Double, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double Implements Core.Constructs.IGetterSetter(Of Double).Setter
        Me.Setter(Me.ToKey(name), value)
        Me.NotifyPropertyChanged(name)
        Return value
    End Function

    ''' <summary> Gets or sets the Sample Trait. </summary>
    ''' <value> The Sample Trait. </value>
    Public ReadOnly Property SampleTrait As SampleTrait

#End Region

#Region " TRAIT SELECTION "

    ''' <summary>
    ''' Converts a name to a key using the
    ''' <see cref="SampleTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
    ''' extend the element Sample Traits beyond the values of the enumeration type that is used to
    ''' populate this table.
    ''' </summary>
    ''' <remarks> David, 5/11/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return SampleTraitTypeEntity.KeyLookupDictionary(name)
    End Function

    ''' <summary> Sets the trait value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Double. </returns>
    Public Function Setter(ByVal value As Double?, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        If value.HasValue AndAlso Not Nullable.Equals(value, Me.Getter(name)) Then
            Me.Setter(value.Value, name)
            Me.NotifyPropertyChanged(name)
        End If
        Return value
    End Function

#If False Then
    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.Getter(Me.ToKey(name))
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.Setter(Me.ToKey(name), value.Value)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property
#End If

    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="sampleTraitTypeId"> Identifies the <see cref="SampleTraitTypeEntity"/>. </param>
    ''' <returns> An elementReadingRealEntity. </returns>
    Public Function Entity(ByVal sampleTraitTypeId As Integer) As SampleTraitEntity
        Return If(Me.ContainsAttribute(sampleTraitTypeId), Me(Me._PrimaryKeyDictionary(sampleTraitTypeId)), New SampleTraitEntity)
    End Function

    ''' <summary> Gets the Real(Double)-value for the given Trait Type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="sampleTraitTypeId"> Identifies the <see cref="SampleTraitTypeEntity"/>. </param>
    ''' <returns> A Double? </returns>
    Public Function Getter(ByVal sampleTraitTypeId As Integer) As Double?
        Return If(Me.ContainsAttribute(sampleTraitTypeId), Me(Me._PrimaryKeyDictionary(sampleTraitTypeId)).Amount, New Double?)
    End Function

    ''' <summary> Set the specified element value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="sampleTraitTypeId"> Identifies the <see cref="SampleTraitTypeEntity"/>. </param>
    ''' <param name="value">             The value. </param>
    Public Sub Setter(ByVal sampleTraitTypeId As Integer, ByVal value As Double)
        If Me.ContainsAttribute(sampleTraitTypeId) Then
            Me(Me._PrimaryKeyDictionary(sampleTraitTypeId)).Amount = value
        Else
            Me.Add(New SampleTraitEntity With {.SampleTraitTypeId = sampleTraitTypeId, .Amount = value})
        End If
    End Sub

#End Region

#Region " UPSERT "

    ''' <summary> Inserts or updates all entities using the given connection. </summary>
    ''' <remarks>
    ''' David, 5/13/2020. Entities that failed to save are enumerated in
    ''' <see cref="P:isr.Dapper.Entity.EntityKeyedCollection`4.UnsavedKeys" />
    ''' </remarks>
    ''' <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <CLSCompliant(False)>
    Public Overrides Function Upsert(ByVal transcactedConnection As TransactedConnection) As Boolean
        Me.ClearUnsavedKeys()
        ' dictionary is instantiated only after the collection has values.
        If Not Me.Any Then Return True
        Dim lotMeterElementSample As LotSampleEntity
        For Each keyValue As KeyValuePair(Of ThreeKeySelector, SampleTraitEntity) In Me.Dictionary
            If keyValue.Value.Upsert(transcactedConnection) Then
                lotMeterElementSample = New LotSampleEntity() With {.LotAutoId = Me.LotAutoId,
                                                                    .ElementAutoId = Me.SampleTrait.ElementAutoId,
                                                                    .SampleTraitAutoId = keyValue.Value.AutoId}
                If Not lotMeterElementSample.Obtain(transcactedConnection) Then
                    Me.AddUnsavedKey(keyValue.Key)
                End If
            Else
                Me.AddUnsavedKey(keyValue.Key)
            End If
        Next
        ' success if no unsaved keys
        Return Not Me.UnsavedKeys.Any
    End Function

#End Region

End Class
