
Partial Public Class LotEntity

    ''' <summary> Gets or sets the <see cref="IEnumerable(Of OhmMeterSetupEntity)"/>. </summary>
    ''' <value> The <see cref="IEnumerable(Of OhmMeterSetupEntity)"/>. </value>
    Public ReadOnly Property OhmMeterSetups As OhmMeterSetupEntityCollection

    ''' <summary> Fetches multimeter ohm meter setups. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Sub FetchOhmMeterSetups(ByVal connection As System.Data.IDbConnection)
        Me.OhmMeterSetups.Clear()
        Me.OhmMeterSetups.Populate(LotOhmMeterSetupEntity.FetchOhmMeterSetups(connection, Me.AutoId))
    End Sub

    ''' <summary> Gets or sets the <see cref="IEnumerable(Of OhmMeterSetupEntity)"/>. </summary>
    ''' <value> The <see cref="IEnumerable(Of OhmMeterSetupEntity)"/>. </value>
    Public ReadOnly Property MultimeterIdOhmMeterSetups As MultimeterIdOhmMeterSetupCollection

    ''' <summary> Gets or sets the multimeter number ohm meter setups. </summary>
    ''' <value> The multimeter number ohm meter setups. </value>
    Public ReadOnly Property MultimeterNumberOhmMeterSetups As MultimeterNumberOhmMeterSetupCollection

    ''' <summary> Fetches multimeter ohm meter setups. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Sub FetchMultimeterOhmMeterSetups(ByVal connection As System.Data.IDbConnection)
        Me.MultimeterIdOhmMeterSetups.Clear()
        Me.MultimeterNumberOhmMeterSetups.Clear()
        If Not MultimeterEntity.IsEnumerated Then MultimeterEntity.TryFetchAll(connection)
        Me.FetchOhmMeterSetups(connection)
        Dim lotOhmMeterSetup As New LotOhmMeterSetupEntity With {.LotAutoId = Me.AutoId}
        For Each entity As LotOhmMeterSetupNub In lotOhmMeterSetup.FetchLotOhmMeterSetups(connection)
            Me._MultimeterIdOhmMeterSetups.Add(entity.SecondaryId, Me.OhmMeterSetups(entity.ForeignId))
            Me._MultimeterNumberOhmMeterSetups.Add(MultimeterEntity.EntityLookupDictionary(entity.SecondaryId).MultimeterNumber, Me.OhmMeterSetups(entity.ForeignId))
        Next
    End Sub

    ''' <summary> Adds lot ohm meter setups. </summary>
    ''' <remarks> David, 6/15/2020. </remarks>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="toleranceCode">        The tolerance code. </param>
    ''' <param name="nominalValue">         The nominal value. </param>
    ''' <param name="multimeterIdentities"> The multimeter identities. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function AddLotOhmMeterSetups(ByVal connection As System.Data.IDbConnection,
                                         ByVal toleranceCode As String, ByVal nominalValue As Double,
                                         ByVal multimeterIdentities As IEnumerable(Of Integer)) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Not MultimeterEntity.IsEnumerated Then
            Dim r As (Success As Boolean, details As String, Entities As IEnumerable(Of MultimeterEntity)) = MultimeterEntity.TryFetchAll(connection)
            result = (r.Success, r.details)
        End If
        If Not result.Success Then Return result
        Me.FetchMultimeterOhmMeterSetups(connection)
        Dim r2 As (Success As Boolean, Details As String, OhmMeterSetup As OhmMeterSetupEntity)
        For Each multimeterId As Integer In multimeterIdentities
            If Not Me.MultimeterIdOhmMeterSetups.ContainsKey(multimeterId) Then
                r2 = LotOhmMeterSetupEntity.AddLotOhmMeterSetupEntity(connection, Me.AutoId, toleranceCode, nominalValue, multimeterId)
                If r2.Success Then
                    Me.OhmMeterSetups.Add(r2.OhmMeterSetup)
                    Me.MultimeterIdOhmMeterSetups.Add(multimeterId, r2.OhmMeterSetup)
                    Me.MultimeterNumberOhmMeterSetups.Add(MultimeterEntity.EntityLookupDictionary(multimeterId).MultimeterNumber, r2.OhmMeterSetup)
                Else
                    result = (r2.Success, r2.Details)
                End If
            End If
            If Not result.Success Then
                Exit For
            End If
        Next
        Return result
    End Function

End Class
