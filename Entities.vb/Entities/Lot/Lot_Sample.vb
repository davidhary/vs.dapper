Imports System.Collections.ObjectModel

Imports Dapper

Partial Public Class LotEntity

    ''' <summary> Gets or sets the <see cref="LotYieldTraitEntityCollection"/>. </summary>
    ''' <value> The <see cref="LotYieldTraitEntityCollection"/>. </value>
    Public ReadOnly Property YieldTraits As YieldTraitConnection

    ''' <summary> Gets or sets the <see cref="LotSampleTraitEntityCollection"/>. </summary>
    ''' <value> The <see cref="LotSampleTraitEntityCollection"/>. </value>
    Public ReadOnly Property SampleTraits As SampleTraitCollection

    ''' <summary> Initializes the sample yield traits. </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    ''' <param name="multimeters"> The multimeters. </param>
    ''' <param name="elements">    The elements. </param>
    Public Sub InitializeSampleYieldTraits(ByVal multimeters As IEnumerable(Of MultimeterEntity), ByVal elements As IEnumerable(Of ElementEntity))
        Me._YieldTraits = New YieldTraitConnection()
        Me._SampleTraits = New SampleTraitCollection
        Dim lotYieldTraits As LotYieldTraitEntityCollection
        Dim lotSampleTraits As LotSampleTraitEntityCollection
        For Each multimeter As MultimeterEntity In multimeters
            lotYieldTraits = New LotYieldTraitEntityCollection(Me, multimeter)
            Me.YieldTraits.Add(lotYieldTraits)
            Me.NomReadings.Add(lotYieldTraits)
            For Each element As ElementEntity In elements
                For Each nomType As NomTypeEntity In element.NomTypes
                    lotSampleTraits = New LotSampleTraitEntityCollection(Me, element, nomType.Id, multimeter)
                    Me.SampleTraits.Add(lotSampleTraits)
                    Me.NomReadings.Add(lotSampleTraits)
                Next
            Next
        Next
    End Sub

    ''' <summary> Fetches the Samples. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="multimeters"> The multimeters. </param>
    ''' <param name="elements">    The elements. </param>
    ''' <returns> The Samples. </returns>
    Public Function FetchSamples(ByVal connection As System.Data.IDbConnection, ByVal multimeters As IEnumerable(Of MultimeterEntity),
                                 ByVal elements As IEnumerable(Of ElementEntity)) As LotNomReadingCollection
        If Not Entities.SampleTraitTypeEntity.IsEnumerated Then Entities.SampleTraitTypeEntity.TryFetchAll(connection)
        Dim sampleTraits As LotSampleTraitEntityCollection
        For Each multimeter As MultimeterEntity In multimeters
            Me.YieldTraits(multimeter.Id).Populate(LotYieldEntity.FetchOrderedYieldTraits(connection, Me.AutoId, multimeter.Id))
            Me.NomReadings.Add(Me.YieldTraits(multimeter.Id))
            For Each element As ElementEntity In elements
                For Each nomType As NomTypeEntity In element.NomTypes
                    sampleTraits = Me.SampleTraits(New MeterElementNomTypeSelector(multimeter.Id, element.AutoId, nomType.Id))
                    sampleTraits.Populate(LotSampleEntity.FetchOrderedSampleTraits(connection, Me.AutoId, multimeter.Id, element.AutoId))
                    Me.NomReadings.Add(sampleTraits)
                Next
            Next
        Next
        Return Me.NomReadings
    End Function

End Class

''' <summary>
''' A yield trait connection of <see cref="LotYieldTraitEntityCollection"/> keyed by multimeter
''' id.
''' </summary>
''' <remarks> David, 7/3/2020. </remarks>
Public Class YieldTraitConnection
    Inherits KeyedCollection(Of Integer, LotYieldTraitEntityCollection)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As LotYieldTraitEntityCollection) As Integer
        Return item.MultimeterId
    End Function

    ''' <summary> Updates or inserts entities using the given connection. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    <CLSCompliant(False)>
    Public Sub Upsert(ByVal connection As TransactedConnection)
        For Each item As LotYieldTraitEntityCollection In Me
            item.Upsert(connection)
        Next
    End Sub

    ''' <summary> Updates or inserts entities using the given connection. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Sub Upsert(ByVal connection As System.Data.IDbConnection)
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        Me.Upsert(New TransactedConnection(connection, transaction))
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            Me.Upsert(transactedConnection)
        End If
    End Sub

End Class

''' <summary> Collection of sample traits. </summary>
''' <remarks> David, 7/3/2020. </remarks>
Public Class SampleTraitCollection
    Inherits KeyedCollection(Of MeterElementNomTypeSelector, LotSampleTraitEntityCollection)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As LotSampleTraitEntityCollection) As MeterElementNomTypeSelector
        Return item.SampleTrait.Selector
    End Function

    ''' <summary> Evaluates all elements. </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    Public Sub Evaluate()
        For Each item As LotSampleTraitEntityCollection In Me
            item.SampleTrait.Evaluate()
        Next
    End Sub

    ''' <summary> Updates or inserts entities using the given connection. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    <CLSCompliant(False)>
    Public Sub Upsert(ByVal connection As TransactedConnection)
        For Each item As LotSampleTraitEntityCollection In Me
            item.Upsert(connection)
        Next
    End Sub

    ''' <summary> Updates or inserts entities using the given connection. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Sub Upsert(ByVal connection As System.Data.IDbConnection)
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        Me.Upsert(New TransactedConnection(connection, transaction))
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            Me.Upsert(transactedConnection)
        End If
    End Sub

End Class
