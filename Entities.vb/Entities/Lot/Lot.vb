Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary>
''' A builder for a Nominal lot identified by a lot number based on the
''' <see cref="IKeyLabelTime">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class LotBuilder
    Inherits KeyLabelTimeBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return LotBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(LotBuilder._TableName) Then
                LotBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(LotNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the size of the label field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The size of the label field. </value>
    Public Overrides Property LabelFieldSize() As Integer = 32

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(LotEntity.AutoId)

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the label field. </value>
    Public Overrides Property LabelFieldName() As String = NameOf(LotEntity.LotNumber)

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the timestamp field. </value>
    Public Overrides Property TimestampFieldName() As String = NameOf(UutEntity.Timestamp)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As LotBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As LotBuilder
        If LotBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                LotBuilder._Instance = New LotBuilder()
            End SyncLock
        End If
        Return LotBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Lot table based on the <see cref="IKeyLabelTime">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("Lot")>
Public Class LotNub
    Inherits KeyLabelTimeNub
    Implements IKeyLabelTime

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IKeyLabelTime
        Return New LotNub
    End Function

End Class

''' <summary> The Lot Entity based on the <see cref="IKeyLabelTime">interface</see>. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class LotEntity
    Inherits EntityBase(Of IKeyLabelTime, LotNub)
    Implements IKeyLabelTime

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New LotNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Lot interface. </param>
    Public Sub New(ByVal value As IKeyLabelTime)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyLabelTime, ByVal store As IKeyLabelTime)
        MyBase.New(New LotNub, cache, store)
        Me.UsingNativeTracking = String.Equals(LotBuilder.TableName, NameOf(IKeyLabelTime).TrimStart("I"c))
        Me.MultimeterIdOhmMeterSetups = New MultimeterIdOhmMeterSetupCollection
        Me.MultimeterNumberOhmMeterSetups = New MultimeterNumberOhmMeterSetupCollection
        Me.OhmMeterSetups = New OhmMeterSetupEntityCollection
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IKeyLabelTime
        Return New LotNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IKeyLabelTime
        Dim destination As IKeyLabelTime = Me.CreateNew
        LotNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IKeyLabelTime)
        LotNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Lot interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyLabelTime)
        ' first make the copy to notify of any property change.
        LotNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Lot table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyLabelTime)(key), connection.Get(Of LotNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.LotNumber)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As Boolean
        Me.ClearStore()
        Dim nub As LotNub = LotEntity.FetchNubs(connection, lotNumber).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyLabelTime) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If LotEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(LotBuilder.Get.UsingUniqueLabel(connection),
                                        Me.FetchUsingUniqueIndex(connection, entity.Label),
                                        Me.FetchUsingKey(connection, entity.AutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of LotNub)(New LotNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Lot entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Lot entities. </value>
    Public ReadOnly Property Lots As IEnumerable(Of LotEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of LotEntity)
        Return If(usingNativeTracking, LotEntity.Populate(connection.GetAll(Of IKeyLabelTime)), LotEntity.Populate(connection.GetAll(Of LotNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Lots = LotEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(LotEntity.Lots))
        Return If(Me.Lots?.Any, Me.Lots.Count, 0)
    End Function

    ''' <summary> Populates a list of Lot entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The Lot nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal nubs As IEnumerable(Of LotNub)) As IEnumerable(Of LotEntity)
        Dim l As New List(Of LotEntity)
        If nubs?.Any Then
            For Each nub As LotNub In nubs
                l.Add(New LotEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Lot entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The Lot interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyLabelTime)) As IEnumerable(Of LotEntity)
        Dim l As New List(Of LotEntity)
        If interfaces?.Any Then
            Dim nub As New LotNub
            For Each iFace As IKeyLabelTime In interfaces
                nub.CopyFrom(iFace)
                l.Add(New LotEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotNub.Label)} = @lotNumber", New With {lotNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{LotBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(LotNub.Label)} IS NULL OR {NameOf(LotNub.Label)} = @LotNumber)")
        selectSql.Append("; ")

        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {lotNumber})

    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As IEnumerable(Of LotNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotNub.Label)} = @lotNumber", New With {lotNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the lot exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As Boolean
        Return 1 = LotEntity.CountEntities(connection, lotNumber)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.LotEntity"/> </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/> </value>
    Public Property AutoId As Integer Implements IKeyLabelTime.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Lot number. </summary>
    ''' <value> The Lot number. </value>
    Public Property Label As String Implements IKeyLabelTime.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotEntity.LotNumber))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements IKeyLabelTime.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Lot number. </summary>
    ''' <value> The Lot number. </value>
    Public Property LotNumber As String
        Get
            Return Me.Label
        End Get
        Set(value As String)
            Me.Label = value
        End Set
    End Property


#End Region

#Region " TIMESTAMP "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    ''' <summary> The timestamp caption format. </summary>
    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

#End Region

End Class

