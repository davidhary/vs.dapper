Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Lot OhmMeterSetup builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class LotOhmMeterSetupBuilder
    Inherits OneToManyIdBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return LotOhmMeterSetupBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(LotOhmMeterSetupBuilder._TableName) Then
                LotOhmMeterSetupBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(LotOhmMeterSetupNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = LotBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(LotNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = MultimeterBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(MultimeterNub.Id)

    ''' <summary> Gets the name of the Ternary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Ternary table. </value>
    Public Overrides Property ForeignIdTableName As String = OhmMeterSetupBuilder.TableName

    ''' <summary> Gets the name of the Ternary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Ternary table key. </value>
    Public Overrides Property ForeignIdTableKeyName As String = NameOf(OhmMeterSetupNub.AutoId)

    ''' <summary> Gets the name of the primary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(LotOhmMeterSetupEntity.LotAutoId)

    ''' <summary> Gets the name of the secondary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(LotOhmMeterSetupEntity.MultimeterId)

    ''' <summary> Gets the name of the foreign identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName As String = NameOf(LotOhmMeterSetupEntity.OhmMeterSetupAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As LotOhmMeterSetupBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As LotOhmMeterSetupBuilder
        If LotOhmMeterSetupBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                LotOhmMeterSetupBuilder._Instance = New LotOhmMeterSetupBuilder()
            End SyncLock
        End If
        Return LotOhmMeterSetupBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="Entities.LotEntity"/> OhmMeterSetup table
''' <see cref="IOneToManyId">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("LotOhmMeterSetup")>
Public Class LotOhmMeterSetupNub
    Inherits OneToManyIdNub
    Implements IOneToManyId

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToManyId
        Return New LotOhmMeterSetupNub
    End Function

End Class

''' <summary>
''' The <see cref="Entities.LotEntity"/>OhmMeterSetup Entity. Implements access to the database
''' using Dapper.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class LotOhmMeterSetupEntity
    Inherits EntityBase(Of IOneToManyId, LotOhmMeterSetupNub)
    Implements IOneToManyId

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New LotOhmMeterSetupNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.LotEntity"/>OhmMeterSetup interface. </param>
    Public Sub New(ByVal value As IOneToManyId)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToManyId, ByVal store As IOneToManyId)
        MyBase.New(New LotOhmMeterSetupNub, cache, store)
        Me.UsingNativeTracking = String.Equals(LotOhmMeterSetupBuilder.TableName, NameOf(IOneToManyId).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToManyId
        Return New LotOhmMeterSetupNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOneToManyId
        Dim destination As IOneToManyId = Me.CreateNew
        LotOhmMeterSetupNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="LotOhmMeterSetupEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToManyId)
        LotOhmMeterSetupNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached OhmMeterSetup, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.LotEntity"/>OhmMeterSetup interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToManyId)
        ' first make the copy to notify of any property change.
        LotOhmMeterSetupNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="meterId">    Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal meterId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As LotOhmMeterSetupNub = LotOhmMeterSetupEntity.FetchNubs(connection, lotAutoId, meterId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="meterId">    Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal meterId As Integer) As Boolean
        Me.ClearStore()
        Return Me.FetchUsingKey(connection, lotAutoId, meterId)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToManyId) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If LotOhmMeterSetupEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="meterId">    Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal meterId As Integer) As Boolean
        Return connection.Delete(Of LotOhmMeterSetupNub)(New LotOhmMeterSetupNub With {.PrimaryId = lotAutoId, .SecondaryId = meterId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets the <see cref="Entities.LotEntity"/>OhmMeterSetup entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The lot ohm meter setups. </value>
    Public ReadOnly Property LotOhmMeterSetups As IEnumerable(Of LotOhmMeterSetupEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of LotOhmMeterSetupEntity)
        Return If(usingNativeTracking, LotOhmMeterSetupEntity.Populate(connection.GetAll(Of IOneToManyId)),
                                       LotOhmMeterSetupEntity.Populate(connection.GetAll(Of LotOhmMeterSetupNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._LotOhmMeterSetups = LotOhmMeterSetupEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(LotOhmMeterSetupEntity.LotOhmMeterSetups))
        Return If(Me.LotOhmMeterSetups?.Any, Me.LotOhmMeterSetups.Count, 0)
    End Function

    ''' <summary> Count LotOhmMeterSetups. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{LotOhmMeterSetupBuilder.TableName}] WHERE {NameOf(LotOhmMeterSetupNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of LotOhmMeterSetupEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotOhmMeterSetupBuilder.TableName}] WHERE {NameOf(LotOhmMeterSetupNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotOhmMeterSetupEntity.Populate(connection.Query(Of LotOhmMeterSetupNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches Lot ohm meter setup by Lot auto id; expected single or none. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of LotOhmMeterSetupNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotOhmMeterSetupBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotOhmMeterSetupEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotOhmMeterSetupNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of LotOhmMeterSetup entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> the <see cref="Entities.LotEntity"/>OhmMeterSetup nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of LotOhmMeterSetupNub)) As IEnumerable(Of LotOhmMeterSetupEntity)
        Dim l As New List(Of LotOhmMeterSetupEntity)
        If nubs?.Any Then
            For Each nub As LotOhmMeterSetupNub In nubs
                l.Add(New LotOhmMeterSetupEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of LotOhmMeterSetup entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> the <see cref="Entities.LotEntity"/>OhmMeterSetup interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToManyId)) As IEnumerable(Of LotOhmMeterSetupEntity)
        Dim l As New List(Of LotOhmMeterSetupEntity)
        If interfaces?.Any Then
            Dim nub As New LotOhmMeterSetupNub
            For Each iFace As IOneToManyId In interfaces
                nub.CopyFrom(iFace)
                l.Add(New LotOhmMeterSetupEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Lot Traits; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="meterId">    Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal meterId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotOhmMeterSetupBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotOhmMeterSetupNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotOhmMeterSetupNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = meterId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Lot Ohm Meter Setup by Lot Auto ID and Meter Id;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="meterId">    Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal meterId As Integer) As IEnumerable(Of LotOhmMeterSetupNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotOhmMeterSetupBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotOhmMeterSetupNub.PrimaryId)} = @primaryId", New With {.primaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotOhmMeterSetupNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = meterId})

        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotOhmMeterSetupNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.LotEntity"/>OhmMeterSetup exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="meterId">    Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal meterId As Integer) As Boolean
        Return 1 = LotOhmMeterSetupEntity.CountEntities(connection, lotAutoId, meterId)
    End Function

#End Region

#Region " RELATIONS: LOT "

    ''' <summary> Count OhmMeterSetups associated with this Lot. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of OhmMeterSetup OhmMeterSetups. </returns>
    Public Function CountLotOhmMeterSetups(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotOhmMeterSetupBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotOhmMeterSetupNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Lot OhmMeterSetup OhmMeterSetups by <see cref="LotOhmMeterSetupEntity.LotAutoId"/>;
    ''' expected as manger as the number of meters.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchLotOhmMeterSetups(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of LotOhmMeterSetupNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotOhmMeterSetupBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotOhmMeterSetupNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotOhmMeterSetupNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets the <see cref="Entities.LotEntity"/> Entity. </summary>
    ''' <value> The lot entity. </value>
    Public ReadOnly Property LotEntity As LotEntity

    ''' <summary> Fetches a Lot Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchLotEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._LotEntity = New LotEntity()
        Return Me.LotEntity.FetchUsingKey(connection, Me.LotAutoId)
    End Function

#End Region

#Region " RELATIONS: MULTIMETER "

    ''' <summary> The multimeter entity. </summary>
    Private _MultimeterEntity As MultimeterEntity

    ''' <summary> Gets the meter model entity. </summary>
    ''' <value> The meter model entity. </value>
    Public ReadOnly Property MultimeterEntity As MultimeterEntity
        Get
            If Me._MultimeterEntity Is Nothing Then
                Me._MultimeterEntity = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
            End If
            Return Me._MultimeterEntity
        End Get
    End Property

    ''' <summary> Fetches the meter. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchMultimeterEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not MultimeterEntity.IsEnumerated Then MultimeterEntity.TryFetchAll(connection)
        Me._MultimeterEntity = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.MultimeterEntity))
        Return Me._MultimeterEntity IsNot Nothing
    End Function

#End Region

#Region " RELATIONS: METER MODEL "

    ''' <summary> The Meter Model entity. </summary>
    Private _MultimeterModelEntity As MultimeterModelEntity

    ''' <summary> Gets the Meter Model entity. </summary>
    ''' <value> The meter model entity. </value>
    Public ReadOnly Property MultimeterModelEntity As MultimeterModelEntity
        Get
            If Me._MultimeterModelEntity Is Nothing Then
                Me._MultimeterModelEntity = Me.MultimeterEntity.MultimeterModelEntity
            End If
            Return Me._MultimeterModelEntity
        End Get
    End Property

    ''' <summary> Fetches the Meter Model entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchMultimeterModel(ByVal connection As System.Data.IDbConnection) As Boolean
        Me.FetchMultimeterEntity(connection)
        Me.MultimeterEntity.FetchMultimeterModel(connection)
        Me._MultimeterModelEntity = Me.MultimeterEntity.MultimeterModelEntity
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.MeterModelEntity))
        Return Me.MultimeterModelEntity IsNot Nothing
    End Function

#End Region

#Region " RELATIONS: OHM METER SETUP "

    ''' <summary> Gets or sets the ohm meter setup entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The ohm meter setup entity. </value>
    Public ReadOnly Property OhmMeterSetupEntity As OhmMeterSetupEntity

    ''' <summary> Fetches ohm meter setup entity. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function FetchOhmMeterSetupEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._OhmMeterSetupEntity = New OhmMeterSetupEntity()
        Return Me.OhmMeterSetupEntity.FetchUsingKey(connection, Me.OhmMeterSetupAutoId)
    End Function

    ''' <summary> Fetches a <see cref="Entities.OhmMeterSetupEntity"/>'s. </summary>
    ''' <remarks> David, 6/23/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The OhmMeterSetup. </returns>
    Public Shared Function FetchOhmMeterSetups(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String,
                                               ByVal lotAutoId As Integer) As IEnumerable(Of OhmMeterSetupEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {lotAutoId})
        ' Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.lotAutoId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return OhmMeterSetupEntity.Populate(connection.Query(Of OhmMeterSetupNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches <see cref="Entities.OhmMeterSetupEntity"/>'s. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The <see cref="IEnumerable(Of OhmMeterSetupEntity)"/>. </returns>
    Public Shared Function FetchOhmMeterSetups(ByVal connection As System.Data.IDbConnection,
                                               ByVal lotAutoId As Integer) As IEnumerable(Of OhmMeterSetupEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotOhmMeterSetup] on [LotOhmMeterSetup].SecondaryId = [OhmMeterSetup].AutoId where [LotOhmMeterSetup].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{OhmMeterSetupBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{OhmMeterSetupBuilder.TableName}] Inner Join [{LotOhmMeterSetupBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotOhmMeterSetupBuilder.TableName}].{NameOf(LotOhmMeterSetupNub.ForeignId)} = [{OhmMeterSetupBuilder.TableName}].{NameOf(OhmMeterSetupNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{LotOhmMeterSetupBuilder.TableName}].{NameOf(LotOhmMeterSetupNub.PrimaryId)} = @{NameOf(lotAutoId)}; ")
        Return LotOhmMeterSetupEntity.FetchOhmMeterSetups(connection, queryBuilder.ToString, lotAutoId)
    End Function

    ''' <summary> Adds a Lot ohm meter setup entity. </summary>
    ''' <remarks> David, 6/14/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId">     Identifies the Lot identity. </param>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="nominalValue">  The nominal value. </param>
    ''' <param name="multimeterId">  The identifier of the multimeter. </param>
    ''' <returns> An OhmMeterSetupEntity. </returns>
    Public Shared Function AddLotOhmMeterSetupEntity(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                                     ByVal toleranceCode As String, ByVal nominalValue As Double,
                                                     ByVal multimeterId As Integer) As (Success As Boolean, Details As String, OhmMeterSetup As OhmMeterSetupEntity)
        Dim ohmMeterSettingLookupId As Integer = 0
        Dim ohmMeterSettingLookup As New OhmMeterSettingLookupEntity
        Dim ohmMeterSettingEntity As New OhmMeterSettingEntity
        Dim ohmMeterSetupEntity As New OhmMeterSetupEntity
        Dim lotOhmMeterSetupEntity As New LotOhmMeterSetupEntity
        Dim result As (Success As Boolean, Details As String, OhmMeterSetup As OhmMeterSetupEntity) = (True, String.Empty, ohmMeterSetupEntity)
        ohmMeterSettingLookup = OhmMeterSettingLookupEntity.SelectOhmMeterSettingLookupEntity(toleranceCode, multimeterId, nominalValue)
        Dim r As (Success As Boolean, Details As String)
        r = ohmMeterSettingLookup.ValidateStoredEntity($"Selected {NameOf(Dapper.Entities.OhmMeterSettingLookupEntity)} for tolerance '{toleranceCode}' and nominal value of {nominalValue}")
        If Not r.Success Then result = (r.Success, r.Details, ohmMeterSetupEntity)
        If result.Success Then
            ohmMeterSettingEntity = New OhmMeterSettingEntity With {.Id = ohmMeterSettingLookup.OhmMeterSettingId}
            ohmMeterSettingEntity.FetchUsingUniqueIndex(connection)
            r = ohmMeterSettingEntity.ValidateStoredEntity($"Fetched {NameOf(Dapper.Entities.OhmMeterSettingEntity)} with {NameOf(Dapper.Entities.OhmMeterSettingLookupEntity.OhmMeterSettingId)} of {ohmMeterSettingLookup.OhmMeterSettingId}")
            If Not r.Success Then result = (r.Success, r.Details, ohmMeterSetupEntity)
        End If
        If result.Success Then
            ohmMeterSetupEntity.Copy(ohmMeterSettingEntity)
            ohmMeterSetupEntity.Insert(connection)
            r = ohmMeterSetupEntity.ValidateStoredEntity($"Inserted {NameOf(Dapper.Entities.OhmMeterSetupEntity)}")
            If Not r.Success Then result = (r.Success, r.Details, ohmMeterSetupEntity)
        End If
        If result.Success Then
            lotOhmMeterSetupEntity = New LotOhmMeterSetupEntity With {.LotAutoId = lotAutoId, .OhmMeterSetupAutoId = ohmMeterSetupEntity.AutoId,
                                                                      .MultimeterId = multimeterId}
            lotOhmMeterSetupEntity.Upsert(connection)
            r = lotOhmMeterSetupEntity.ValidateStoredEntity($"Upserted {NameOf(Dapper.Entities.LotOhmMeterSetupEntity)} ")
            If Not r.Success Then result = (r.Success, r.Details, ohmMeterSetupEntity)
        End If
        Return result
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToManyId.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotOhmMeterSetupEntity.LotAutoId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.LotEntity"/> record.
    ''' </summary>
    ''' <value> The identifier of the lot automatic. </value>
    Public Property LotAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToManyId.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotOhmMeterSetupEntity.MultimeterId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identity of the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <value> The identifier of the multimeter. </value>
    Public Property MultimeterId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the Three-to-Many referenced table identifier. </summary>
    ''' <value> The identifier of Three-to-Many referenced table. </value>
    Public Property ForeignId As Integer Implements IOneToManyId.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotOhmMeterSetupEntity.OhmMeterSetupAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identity of the <see cref="OhmMeterSetupEntity"/>. </summary>
    ''' <value> The identifier of the ohm meter setup automatic. </value>
    Public Property OhmMeterSetupAutoId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(ByVal value As Integer)
            Me.ForeignId = value
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of Lot Trait Natural (Integer-value) entities. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class LotOhmMeterSetupEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToManyId, LotOhmMeterSetupNub, LotOhmMeterSetupEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As LotOhmMeterSetupEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As LotOhmMeterSetupEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all Lots from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of LotOhmMeterSetupEntity))
        If entities?.Any Then
            For Each entity As LotOhmMeterSetupEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return LotOhmMeterSetupBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary>
''' Collection of Lot-Unique Lot Trait Natural (Integer-value) Entities unique to the Lot.
''' </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class LotUniqueOhmMeterSetupEntityCollection
    Inherits LotOhmMeterSetupEntityCollection

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    Public Sub New(ByVal lotAutoId As Integer)
        Me.New
        Me.LotAutoId = lotAutoId
    End Sub


    ''' <summary> Dictionary of unique indexes for selecting the ohm meter setup by multimeter. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys for selecting the ohm meter setup by multimeter. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As LotOhmMeterSetupEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.MultimeterId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.MultimeterId)
    End Sub

    ''' <summary>
    ''' Removes all Lots from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains 'ohmMeterSetupAutoId' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal multimeterId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(multimeterId)
    End Function

    ''' <summary>
    ''' Gets the setup for the specified <see cref="Entities.MultimeterEntity"/> Id.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> A LotOhmMeterSetupEntity. </returns>
    Public Function OhmMeterSetup(ByVal multimeterId As Integer) As LotOhmMeterSetupEntity
        Return If(Me.ContainsKey(multimeterId), Me(Me._PrimaryKeyDictionary(multimeterId)), New LotOhmMeterSetupEntity)
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.LotEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/>. </value>
    Public ReadOnly Property LotAutoId As Integer

End Class

