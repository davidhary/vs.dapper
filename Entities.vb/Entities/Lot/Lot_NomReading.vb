Imports isr.Core.Engineering
Imports isr.Core.RandomExtensions

Partial Public Class LotEntity

    ''' <summary> Fetches nom readings. </summary>
    ''' <remarks> David, 7/2/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The nom readings. </returns>
    Public Function FetchNomReadings(ByVal connection As System.Data.IDbConnection) As Integer
        Return Me.Uuts.FetchNomReadings(connection)
    End Function

    ''' <summary> Initializes the nom readings. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="multimeters"> The multimeters. </param>
    ''' <param name="elements">    The elements. </param>
    Public Sub InitializeNomReadings(ByVal multimeters As IEnumerable(Of MultimeterEntity), ByVal elements As ElementEntityCollection)
        Me._NomReadings = New LotNomReadingCollection(multimeters, elements, -3, 3, 31)
        Me.Uuts.InitializeNomReadings(elements)
    End Sub

    ''' <summary>
    ''' Populates the <see cref="Entities.UutEntity"/> <see cref="UutEntity.NomReadings"/> for all
    ''' <see cref="Entities.NutEntity"/>'s.
    ''' </summary>
    ''' <remarks> David, 6/11/2020. </remarks>
    ''' <returns> The number of <see cref="NomReading"/>'s . </returns>
    Public Function PopulateNomReadings() As Integer
        Me.NomReadings.Clear()
        For Each uut As UutEntity In Me.Uuts
            uut.PopulateNomReadings()
            Me.NomReadings.AddRange(uut.NomReadings)
        Next
        Return Me.NomReadings.Count
    End Function

    ''' <summary>
    ''' Gets or sets the nominal readings initializing the histogram over 6 sigma with 0.2 sigma bin
    ''' width.
    ''' </summary>
    ''' <value> The nominal readings. </value>
    Public ReadOnly Property NomReadings As LotNomReadingCollection

End Class

''' <summary> Dictionary of nominal readings keyed by the multimeter. </summary>
''' <remarks> David, 2020-06-27. </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class LotNomReadingCollection
    Inherits NutNomReadingCollection

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="multimeters"> The multimeters. </param>
    ''' <param name="elements">    The elements. </param>
    ''' <param name="lowerBound">  The lower bound. </param>
    ''' <param name="upperBound">  The upper bound. </param>
    ''' <param name="binCount">    The number of bins. </param>
    Public Sub New(ByVal multimeters As IEnumerable(Of MultimeterEntity), ByVal elements As IEnumerable(Of ElementEntity),
                   ByVal lowerBound As Double, ByVal upperBound As Double, ByVal binCount As Integer)
        MyBase.New(multimeters, elements)
        Me.YieldDictionary = New Dictionary(Of Integer, YieldTrait)
        Me.SampleDictionary = New Dictionary(Of MeterElementNomTypeSelector, SampleTrait)
        Me.HistogramDictionary = New Dictionary(Of MeterElementNomTypeSelector, isr.Core.Engineering.HistogramBindingList)
        Me.BinCount = binCount
        Me.LowerBound = lowerBound
        Me.UpperBound = upperBound
        Dim selector As MeterElementNomTypeSelector
        Dim histogram As isr.Core.Engineering.HistogramBindingList
        For Each multimeter As MultimeterEntity In multimeters
            For Each element As ElementEntity In elements
                For Each nomType As NomTypeEntity In element.NomTypes
                    histogram = New isr.Core.Engineering.HistogramBindingList(Me.LowerBound, Me.UpperBound, Me.BinCount)
                    histogram.Initialize()
                    selector = New MeterElementNomTypeSelector(multimeter.Id, element.AutoId, nomType.Id)
                    Me.HistogramDictionary.Add(selector, histogram)
                Next
            Next
        Next
    End Sub

    ''' <summary> Gets or sets the item count limit. </summary>
    ''' <value> The item count limit. </value>
    Public Property ItemCountLimit As Integer

    ''' <summary> Adds a nom readings. </summary>
    ''' <remarks> David, 7/16/2020. </remarks>
    ''' <param name="uut"> The <see cref="UutEntity"/>. </param>
    Public Overloads Sub AddNomReadings(ByVal uut As UutEntity)
        For Each nut As NutEntity In uut.Nuts
            Me.Add(nut.NomReadings)
        Next
    End Sub

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal item As NutUniqueNomReadingEntityCollection)
        MyBase.Add(item)
        Dim selector As New MeterElementNomTypeSelector(item.NomReading)
        If Me.BinCount > 0 Then Me.HistogramDictionary(selector).Update(item.NomReading.Delta)
        If Me.ItemCountLimit > 0 Then
            Do Until Me.Count <= Me.ItemCountLimit
                Me.RemoveFirst()
            Loop
        End If
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/30/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        For Each histogram As HistogramBindingList In Me.HistogramDictionary.Values
            histogram.Initialize()
        Next
        If Me.SampleDictionary IsNot Nothing Then
            For Each sampleTrait As SampleTrait In Me.SampleDictionary.Values
                sampleTrait.Clear()
            Next
        End If
        If Me.YieldDictionary IsNot Nothing Then
            For Each yieldTrait As YieldTrait In Me.YieldDictionary.Values
                yieldTrait.Clear()
            Next
        End If
    End Sub


#End Region

#Region " SAMPLE "

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overloads Sub Add(ByVal item As LotSampleTraitEntityCollection)
        Dim selector As New MeterElementNomTypeSelector(item.SampleTrait)
        If Not Me.SampleDictionary.ContainsKey(selector) Then
            Me._SampleDictionary.Add(selector, item.SampleTrait)
        End If
    End Sub

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 7/7/2020. </remarks>
    ''' <param name="uut"> The <see cref="UutEntity"/>. </param>
    ''' <param name="nut"> The nut. </param>
    Public Overloads Sub Add(ByVal uut As UutEntity, ByVal nut As NutEntity)
        Dim selector As New MeterElementNomTypeSelector(uut.Traits.UutTrait.MultimeterId, nut.ElementAutoId, nut.NomTypeId)
        If Me.SampleDictionary.ContainsKey(selector) Then
            Dim sampleTrait As SampleTrait = Me.SampleDictionary(selector)
            sampleTrait.AddItem(nut.NomReadings.NomReading.Delta)
        End If
    End Sub

    ''' <summary> Evaluates this sample statistics. </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    Public Sub Evaluate()
        For Each sampleTrait As SampleTrait In Me.SampleDictionary.Values
            sampleTrait.Evaluate()
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of samples. </summary>
    ''' <value> A dictionary of samples. </value>
    Public ReadOnly Property SampleDictionary As IDictionary(Of MeterElementNomTypeSelector, SampleTrait)

    ''' <summary> Select <see cref="SampleTrait"/>. </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> A <see cref="SampleTrait"/>. </returns>
    Public Function SelectSampleTrait(ByVal multimeterId As Integer, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As SampleTrait
        Return Me.SampleDictionary(New MeterElementNomTypeSelector(multimeterId, elementAutoId, nomTypeId))
    End Function

#End Region

#Region " YIELD "

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/30/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overloads Sub Add(ByVal item As LotYieldTraitEntityCollection)
        If Not Me.YieldDictionary.ContainsKey(item.YieldTrait.MultimeterId) Then
            Me.YieldDictionary.Add(item.YieldTrait.MultimeterId, item.YieldTrait)
        End If
    End Sub

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 7/7/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overloads Sub Add(ByVal item As UutEntity)
        If Me.YieldDictionary?.ContainsKey(item.Traits.UutTrait.MultimeterId) AndAlso item.ProductSortEntity.IsClean Then
            Dim yieldTrait As YieldTrait = Me.YieldDictionary(item.Traits.UutTrait.MultimeterId)
            yieldTrait.AddItem(item)
        End If
    End Sub

    ''' <summary> Gets or sets a dictionary of Yields. </summary>
    ''' <value> A dictionary of Yields. </value>
    Public ReadOnly Property YieldDictionary As IDictionary(Of Integer, YieldTrait)

    ''' <summary> Select Yield Trait. </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns> A <see cref="YieldTrait"/>. </returns>
    Public Function SelectYieldTrait(ByVal multimeterId As Integer) As YieldTrait
        Return Me.YieldDictionary(multimeterId)
    End Function

#End Region

#Region " HISTOGRAM "

    ''' <summary> Simulates this the data. </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="part">      The <see cref="PartEntity"/>. </param>
    ''' <param name="element">   The <see cref="ElementEntity"/>. </param>
    ''' <param name="nomTypeId"> Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="uut">       The <see cref="UutEntity"/>. </param>
    Public Sub Simulate(ByVal part As PartEntity, ByVal element As ElementEntity, ByVal nomTypeId As Integer, ByVal uut As UutEntity)
        Me.Clear()
        Dim rnd As New Random
        Dim nominalValue As Double = part.NomTraits(New MeterElementNomTypeSelector(uut.Traits.UutTrait.MultimeterId, element.AutoId, nomTypeId)).NomTrait.NominalValue.Value
        Dim item As NutUniqueNomReadingEntityCollection
        For i As Integer = 1 To 20000
            item = New NutUniqueNomReadingEntityCollection(0, element, nomTypeId, uut)
            item.NomReading.Reading = 1 + nominalValue * rnd.NextNormal
            Me.Add(item)
        Next
    End Sub

    ''' <summary> Gets or sets the number of bins. </summary>
    ''' <value> The number of bins. </value>
    Public Property BinCount As Integer

    ''' <summary> Gets or sets the lower bound. </summary>
    ''' <value> The lower bound. </value>
    Public Property LowerBound As Double

    ''' <summary> Gets or sets the upper bound. </summary>
    ''' <value> The upper bound. </value>
    Public Property UpperBound As Double

    ''' <summary>
    ''' Dictionary of <see cref="BindingList"/> of
    ''' <see cref="isr.Core.Engineering.HistogramBindingList"/>.
    ''' </summary>
    ''' <value> A dictionary of histograms. </value>
    Public ReadOnly Property HistogramDictionary As IDictionary(Of MeterElementNomTypeSelector, isr.Core.Engineering.HistogramBindingList)

    ''' <summary> Select <see cref="isr.Core.Engineering.HistogramBindingList"/>. </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> An isr.Core.Engineering.Histogram. </returns>
    Public Function SelectHistogram(ByVal multimeterId As Integer, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As isr.Core.Engineering.HistogramBindingList
        Return Me.HistogramDictionary(New MeterElementNomTypeSelector(elementAutoId, multimeterId, nomTypeId))
    End Function

#End Region

End Class

