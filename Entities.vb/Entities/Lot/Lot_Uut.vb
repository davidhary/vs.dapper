
Imports Dapper

Partial Public Class LotEntity

    ''' <summary> Gets or sets the <see cref="LotUniqueUutEntityCollection">Lot Uuts</see>. </summary>
    ''' <value> The Lot Uuts entities. </value>
    Public ReadOnly Property Uuts As LotUniqueUutEntityCollection

    ''' <summary> Fetches the <see cref="LotUniqueUutEntityCollection">Lot Uuts</see>. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of Uuts. </returns>
    Public Function FetchUuts(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Uuts = LotEntity.FetchUuts(connection, Me.AutoId)
        For Each uut As UutEntity In Me.Uuts
            uut.FetchTraits(connection)
        Next
        Return If(Me.Uuts?.Any, Me.Uuts.Count, 0)
    End Function

    ''' <summary> Fetches the <see cref="LotUniqueUutEntityCollection">Lot Uuts</see>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The Uuts. </returns>
    Public Shared Function FetchUuts(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As LotUniqueUutEntityCollection
        Dim result As LotUniqueUutEntityCollection
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        result = LotEntity.FetchUuts(New TransactedConnection(connection, transaction), lotAutoId)
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            result = LotEntity.FetchUuts(transactedConnection, lotAutoId)
        End If
        Return result
    End Function

    ''' <summary> Fetches the <see cref="LotUniqueUutEntityCollection">Lot Uuts</see>. </summary>
    ''' <remarks> David, 7/18/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The Uuts. </returns>
    <CLSCompliant(False)>
    Public Shared Function FetchUuts(ByVal connection As TransactedConnection, ByVal lotAutoId As Integer) As LotUniqueUutEntityCollection
        If Not UutTypeEntity.IsEnumerated Then UutTypeEntity.TryFetchAll(connection)
        Dim part As PartEntity = PartLotEntity.FetchParts(connection, lotAutoId).First
        part.FetchElementsOnly(connection)
        Dim uuts As New LotUniqueUutEntityCollection(lotAutoId)
        uuts.Populate(LotUutEntity.FetchOrderedUuts(connection, lotAutoId))
        uuts.FetchNuts(connection)
        Return uuts
    End Function

End Class
