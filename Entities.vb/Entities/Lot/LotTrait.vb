''' <summary> The Lot Trait class holing the Lot Trait values. </summary>
''' <remarks> David, 2020-05-29. </remarks>
Public Class LotTrait
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSestter"> The getter setter. </param>
    Public Sub New(ByVal getterSestter As isr.Core.Constructs.IGetterSetter(Of Integer))
        MyBase.New()
        Me.GetterSetter = getterSestter
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter(Of Integer)

    ''' <summary> Gets the trait value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Integer. </returns>
    Public Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        Return Me.GetterSetter.Getter(name)
    End Function

    ''' <summary> Sets the trait value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Integer. </returns>
    Public Function Setter(ByVal value As Integer?, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        If value.HasValue AndAlso Not Nullable.Equals(value, Me.Getter(name)) Then
            Me.GetterSetter.Setter(value.Value, name)
            Me.NotifyPropertyChanged(name)
        End If
        Return value
    End Function

#If False Then
    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As Integer?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.GetterSetter.Setter(value.Value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property
#End If

#End Region

#Region " TRAITS "

    ''' <summary> Gets or sets the certified quantity. </summary>
    ''' <value> The certified quantity. </value>
    Public Property CertifiedQuantity As Integer
        Get
            Return CType(Me.Getter(), Integer)
        End Get
        Set(value As Integer)
            Me.Setter(value)
        End Set
    End Property

#End Region

End Class
