Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Lot Natural(Integer)-value trait builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class LotTraitBuilder
    Inherits OneToManyNaturalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return LotTraitBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(LotTraitBuilder._TableName) Then
                LotTraitBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(LotTraitNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = LotBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(LotNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = LotTraitTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(LotTraitTypeNub.Id)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(LotTraitEntity.LotAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(LotTraitEntity.LotTraitTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName As String = NameOf(LotTraitEntity.Amount)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As LotTraitBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As LotTraitBuilder
        If LotTraitBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                LotTraitBuilder._Instance = New LotTraitBuilder()
            End SyncLock
        End If
        Return LotTraitBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="LotTraitEntity"/> value table
''' <see cref="IOneToManyNatural">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("LotTrait")>
Public Class LotTraitNub
    Inherits OneToManyNaturalNub
    Implements IOneToManyNatural

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToManyNatural
        Return New LotTraitNub
    End Function

End Class

''' <summary>
''' The <see cref="LotTraitEntity"/> stores <see cref="Entities.LotEntity"/> traits .
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class LotTraitEntity
    Inherits EntityBase(Of IOneToManyNatural, LotTraitNub)
    Implements IOneToManyNatural

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New LotTraitNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.LotEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IOneToManyNatural)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToManyNatural, ByVal store As IOneToManyNatural)
        MyBase.New(New LotTraitNub, cache, store)
        Me.UsingNativeTracking = String.Equals(LotTraitBuilder.TableName, NameOf(IOneToManyNatural).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOneToManyNatural
        Return New LotTraitNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOneToManyNatural
        Dim destination As IOneToManyNatural = Me.CreateNew
        LotTraitNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="LotTraitEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOneToManyNatural)
        LotTraitNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.LotEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToManyNatural)
        ' first make the copy to notify of any property change.
        LotTraitNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="lotAutoId">      Identifies the <see cref="Entities.LotEntity"/> record. </param>
    ''' <param name="lotTraitTypeId"> Identifies the <see cref="LotTraitTypeEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal lotTraitTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotTraitNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = lotTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of LotTraitNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="lotAutoId">      Identifies the <see cref="Entities.LotEntity"/> record. </param>
    ''' <param name="lotTraitTypeId"> Identifies the <see cref="LotTraitTypeEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal lotTraitTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As LotTraitNub = LotTraitEntity.FetchEntities(connection, lotAutoId, lotTraitTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PrimaryId, Me.SecondaryId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToManyNatural) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If LotTraitEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="lotAutoId">      Identifies the <see cref="Entities.LotEntity"/> record. </param>
    ''' <param name="lotTraitTypeId"> Identifies the <see cref="LotTraitTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal lotTraitTypeId As Integer) As Boolean
        Return connection.Delete(Of LotTraitNub)(New LotTraitNub With {.PrimaryId = lotAutoId, .SecondaryId = lotTraitTypeId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary>
    ''' Gets or sets the <see cref="Entities.LotEntity"/> Attribute Natural(Integer)-Value entities.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value>
    ''' the <see cref="Entities.LotEntity"/> Attribute Natural(Integer)-Value entities.
    ''' </value>
    Public ReadOnly Property LotNaturals As IEnumerable(Of LotTraitEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of LotTraitEntity)
        Return If(usingNativeTracking, LotTraitEntity.Populate(connection.GetAll(Of IOneToManyNatural)),
                                       LotTraitEntity.Populate(connection.GetAll(Of LotTraitNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._LotNaturals = LotTraitEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(LotTraitEntity.LotNaturals))
        Return If(Me.LotNaturals?.Any, Me.LotNaturals.Count, 0)
    End Function

    ''' <summary> Count LotValues. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> record. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{LotTraitBuilder.TableName}] WHERE {NameOf(LotTraitNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> record. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of LotTraitEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotTraitBuilder.TableName}] WHERE {NameOf(LotTraitNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotTraitEntity.Populate(connection.Query(Of LotTraitNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches Lot Traits by Lot auto id. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/> record. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of LotTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotTraitEntity.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Populates a list of LotValue entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> the <see cref="Entities.LotEntity"/>Value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of LotTraitNub)) As IEnumerable(Of LotTraitEntity)
        Dim l As New List(Of LotTraitEntity)
        If nubs?.Any Then
            For Each nub As LotTraitNub In nubs
                l.Add(New LotTraitEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of LotValue entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> the <see cref="Entities.LotEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToManyNatural)) As IEnumerable(Of LotTraitEntity)
        Dim l As New List(Of LotTraitEntity)
        If interfaces?.Any Then
            Dim nub As New LotTraitNub
            For Each iFace As IOneToManyNatural In interfaces
                nub.CopyFrom(iFace)
                l.Add(New LotTraitEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count Lot Traits buy unique index; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="lotAutoId">      Identifies the <see cref="Entities.LotEntity"/> record. </param>
    ''' <param name="lotTraitTypeId"> Identifies the <see cref="LotTraitTypeEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal lotTraitTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotTraitNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = lotTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches Lot Traits by unique index; expected single or none. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="lotAutoId">      Identifies the <see cref="Entities.LotEntity"/> record. </param>
    ''' <param name="lotTraitTypeId"> Identifies the <see cref="LotTraitTypeEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal lotTraitTypeId As Integer) As IEnumerable(Of LotTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotTraitNub.PrimaryId)} = @primaryId", New With {.primaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotTraitNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = lotTraitTypeId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.LotEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="lotAutoId">      Identifies the <see cref="Entities.LotEntity"/> record. </param>
    ''' <param name="lotTraitTypeId"> Identifies the <see cref="LotTraitTypeEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal lotTraitTypeId As Integer) As Boolean
        Return 1 = LotTraitEntity.CountEntities(connection, lotAutoId, lotTraitTypeId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Count values associated with this Lot. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of specification values. </returns>
    Public Function CountLotValues(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotTraitNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Lot Natural(Integer)-value Values by Part auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchLotValues(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of LotTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotTraitNub.PrimaryId)} = @PrimaryId", New With {Me.PrimaryId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.LotEntity"/>. </summary>
    ''' <value> the <see cref="Entities.LotEntity"/>. </value>
    Public ReadOnly Property LotEntity As LotEntity

    ''' <summary> Fetches a <see cref="Entities.LotEntity"/>. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchLotEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._LotEntity = New LotEntity()
        Return Me.LotEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

    ''' <summary> Gets or sets the <see cref="Entities.LotEntity"/>. </summary>
    ''' <value> the <see cref="Entities.LotEntity"/>. </value>
    Public ReadOnly Property LotNaturalTypeEntity As LotTraitTypeEntity

    ''' <summary> Fetches a <see cref="Entities.LotEntity"/>. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchLotNaturalTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._LotNaturalTypeEntity = New LotTraitTypeEntity()
        Return Me.LotNaturalTypeEntity.FetchUsingKey(connection, Me.PrimaryId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToManyNatural.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotAutoId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.LotEntity"/> record.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/> record. </value>
    Public Property LotAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToManyNatural.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotTraitTypeId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identity of the <see cref="Entities.LotEntity"/>
    ''' <see cref="LotTraitTypeEntity"/>.
    ''' </summary>
    ''' <value>
    ''' The identity of the <see cref="Entities.LotEntity"/> <see cref="LotTraitTypeEntity"/>.
    ''' </value>
    Public Property LotTraitTypeId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(ByVal value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets a Natural(Integer)-value assigned to the <see cref="Entities.LotEntity"/> for
    ''' the specific <see cref="LotTraitTypeEntity"/>.
    ''' </summary>
    ''' <value>
    ''' The Natural(Integer)-value assigned to the <see cref="Entities.LotEntity"/> for the specific
    ''' <see cref="LotTraitTypeEntity"/>.
    ''' </value>
    Public Property Amount As Integer Implements IOneToManyNatural.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(ByVal value As Integer)
            If Not Double.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of <see cref="LotTraitEntity"/>'s. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class LotTraitEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IOneToManyNatural, LotTraitNub, LotTraitEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As LotTraitEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As LotTraitEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all Lots from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of LotTraitEntity))
        If entities?.Any Then
            For Each entity As LotTraitEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return LotTraitBuilder.Get.Upsert(connection, Me)
    End Function

End Class

''' <summary>
''' Collection of <see cref="Entities.LotEntity"/> unique <see cref="LotTraitEntity"/>'s.
''' </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class LotUniqueTraitEntityCollection
    Inherits LotTraitEntityCollection
    Implements isr.Core.Constructs.IGetterSetter(Of Integer)

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
        Me.LotTrait = New LotTrait(Me)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    Public Sub New(ByVal lotAutoId As Integer)
        Me.New
        Me.LotAutoId = lotAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As LotTraitEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.LotTraitTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.LotTraitTypeId)
        Me.NotifyPropertyChanged(LotTraitTypeEntity.EntityLookupDictionary(entity.LotTraitTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all Lots from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains 'NaturalType' key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="lotTraitTypeId"> Identifies the <see cref="LotTraitTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal lotTraitTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(lotTraitTypeId)
    End Function

#End Region

#Region " GETTER/SETTER "

    ''' <summary>
    ''' Converts a name to a key using the
    ''' <see cref="LotTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
    ''' extend the element Nominal Traits beyond the values of the enumeration type that is used to
    ''' populate this table.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return LotTraitTypeEntity.KeyLookupDictionary(name)
    End Function

    ''' <summary> Sets the trait value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Integer. </returns>
    Public Function Setter(ByVal value As Integer?, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        If value.HasValue AndAlso Not Nullable.Equals(value, Me.Getter(name)) Then
            Me.Setter(value.Value, name)
            Me.NotifyPropertyChanged(name)
        End If
        Return value
    End Function

#If False Then
    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        Get
            Return Me.Getter(name)
        End Get
        Set(value As Integer?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.Setter(value.Value, name)
            End If
        End Set
    End Property
#End If

    ''' <summary>
    ''' Gets the Integer-value for the given <see cref="LotTraitType"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable Double. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer? Implements isr.Core.Constructs.IGetterSetter(Of Integer).Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary>
    ''' Set the specified element value for the given <see cref="LotTraitType"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value">                                      value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Double. </returns>
    Protected Function Setter(ByVal value As Integer, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer Implements isr.Core.Constructs.IGetterSetter(Of Integer).Setter
        Me.Setter(Me.ToKey(name), value)
        Me.NotifyPropertyChanged(name)
        Return value
    End Function

    ''' <summary> Gets or sets the Lot Trait. </summary>
    ''' <value> The Lot Trait. </value>
    Public ReadOnly Property LotTrait As LotTrait

#End Region

#Region " ATTRIBUTE  SELECTION "

    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="lotTraitTypeId"> Identifies the <see cref="LotTraitTypeEntity"/>. </param>
    ''' <returns> An LotNaturalEntity. </returns>
    Public Function Entity(ByVal lotTraitTypeId As Integer) As LotTraitEntity
        Return If(Me.ContainsKey(lotTraitTypeId), Me(Me._PrimaryKeyDictionary(lotTraitTypeId)), New LotTraitEntity)
    End Function

    ''' <summary>
    ''' Gets the Natural(Integer)-value for the given <see cref="LotTraitTypeEntity.Id"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="lotTraitTypeId"> Identifies the <see cref="LotTraitTypeEntity"/>. </param>
    ''' <returns> An Integer? </returns>
    Public Function Getter(ByVal lotTraitTypeId As Integer) As Integer?
        Return If(Me.ContainsKey(lotTraitTypeId), Me(Me._PrimaryKeyDictionary(lotTraitTypeId)).Amount, New Integer?)
    End Function

    ''' <summary>
    ''' Set the specified element value for the given <see cref="LotTraitTypeEntity.Id"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="lotTraitTypeId"> Identifies the <see cref="LotTraitTypeEntity"/>. </param>
    ''' <param name="value">          The value. </param>
    Public Sub Setter(ByVal lotTraitTypeId As Integer, ByVal value As Integer)
        If Me.ContainsKey(lotTraitTypeId) Then
            Me(Me._PrimaryKeyDictionary(lotTraitTypeId)).Amount = value
        Else
            Me.Add(New LotTraitEntity With {.LotAutoId = Me.LotAutoId, .LotTraitTypeId = lotTraitTypeId, .Amount = value})
        End If
    End Sub

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.LotEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/>. </value>
    Public ReadOnly Property LotAutoId As Integer

#End Region

End Class


