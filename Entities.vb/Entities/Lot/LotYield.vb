Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Core

''' <summary> A Lot-Meter-Yield builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class LotYieldBuilder
    Inherits TwoToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return LotYieldBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(LotYieldBuilder._TableName) Then
                LotYieldBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(LotYieldNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = LotBuilder.TableName

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(LotNub.AutoId)

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = MultimeterBuilder.TableName

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(MultimeterNub.Id)

    ''' <summary> Gets or sets the name of the Ternary table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Ternary table. </value>
    Public Overrides Property TernaryTableName As String = YieldTraitBuilder.TableName

    ''' <summary> Gets or sets the name of the Ternary table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Ternary table key. </value>
    Public Overrides Property TernaryTableKeyName As String = NameOf(YieldTraitNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(LotYieldEntity.LotAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(LotYieldEntity.MultimeterId)

    ''' <summary> Gets or sets the name of the ternary identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException">  Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the ternary identifier field. </value>
    Public Overrides Property TernaryIdFieldName As String = NameOf(LotYieldEntity.YieldTraitAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As LotYieldBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As LotYieldBuilder
        If LotYieldBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                LotYieldBuilder._Instance = New LotYieldBuilder()
            End SyncLock
        End If
        Return LotYieldBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="LotYieldNub"/> based on the <see cref="ITwoToMany">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
<Table("LotYield")>
Public Class LotYieldNub
    Inherits TwoToManyNub
    Implements ITwoToMany

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ITwoToMany
        Return New LotYieldNub
    End Function

End Class

''' <summary>
''' The <see cref="LotYieldEntity"/> based on the
''' <see cref="ITwoToMany">interface</see>.
''' </summary>
''' <remarks> David, 6/16/2020. </remarks>
Public Class LotYieldEntity
    Inherits EntityBase(Of ITwoToMany, LotYieldNub)
    Implements ITwoToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New LotYieldNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Lot-Element interface. </param>
    Public Sub New(ByVal value As ITwoToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ITwoToMany, ByVal store As ITwoToMany)
        MyBase.New(New LotYieldNub, cache, store)
        Me.UsingNativeTracking = String.Equals(LotYieldBuilder.TableName, NameOf(ITwoToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ITwoToMany
        Return New LotYieldNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ITwoToMany
        Dim destination As ITwoToMany = Me.CreateNew
        LotYieldNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ITwoToMany)
        LotYieldNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Lot-Element interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ITwoToMany)
        ' first make the copy to notify of any property change.
        LotYieldNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches a <see cref="LotYieldEntity"/> using the entity key. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="lotAutoId">            Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">         Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="yieldAttributeAutoId"> Identifies the <see cref="YieldTraitEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                            ByVal multimeterId As Integer, ByVal yieldAttributeAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As LotYieldNub = LotYieldEntity.FetchNubs(connection, lotAutoId, multimeterId, yieldAttributeAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Fetches a <see cref="LotYieldEntity"/> using the entity key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.LotAutoId, Me.MultimeterId, Me.YieldTraitAutoId)
    End Function

    ''' <summary>
    ''' Fetches an existing <see cref="LotYieldEntity"/> using the entity unique index.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.LotAutoId, Me.MultimeterId, Me.YieldTraitAutoId)
    End Function

    ''' <summary>
    ''' Fetches an existing <see cref="LotYieldEntity"/> using the entity unique index.
    ''' </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="lotAutoId">            Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">         Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="yieldAttributeAutoId"> Identifies the <see cref="YieldTraitEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                                    ByVal multimeterId As Integer, ByVal yieldAttributeAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, lotAutoId, multimeterId, yieldAttributeAutoId)
    End Function

    ''' <summary>
    ''' Tries to fetch an existing <see cref="YieldTraitEntity"/> associated with a
    ''' <see cref="Entities.LotEntity"/> and <see cref="Entities.ElementEntity"/>;
    ''' otherwise, inserts a new <see cref="YieldTraitEntity"/>. Then tries to fetch an existing or
    ''' insert a new
    ''' <see cref="LotYieldEntity"/>.
    ''' </summary>
    ''' <remarks>
    ''' Assumes that a <see cref="Entities.LotEntity"/> exists for the specified
    ''' <paramref name="lotAutoId"/>
    ''' Assumes that a <see cref="Entities.ElementEntity"/> exists for the specified
    ''' <paramref name="MultimeterId"/>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="lotAutoId">        Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitType"/>. </param>
    ''' <param name="amount">           The amount. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainYieldAttribute(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal multimeterId As Integer,
                                              ByVal yieldTraitTypeId As Integer, ByVal amount As Double) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._YieldTraitEntity = LotYieldEntity.FetchYieldTrait(connection, lotAutoId, multimeterId, yieldTraitTypeId)
        If Not Me._YieldTraitEntity.IsClean Then
            Me._YieldTraitEntity = New YieldTraitEntity With {.YieldTraitTypeId = yieldTraitTypeId, .Amount = amount}
            If Not Me.YieldTraitEntity.Insert(connection) Then
                result = (False, $"Failed inserting {NameOf(Entities.YieldTraitEntity)} with {NameOf(Entities.YieldTraitEntity.YieldTraitTypeId)} of {yieldTraitTypeId}")
            End If
        End If
        If result.Success Then
            Me.PrimaryId = lotAutoId
            Me.SecondaryId = multimeterId
            Me.TernaryId = Me.YieldTraitEntity.AutoId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(LotYieldEntity.YieldTraitEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.LotYieldEntity)} with {NameOf(Entities.LotYieldEntity.LotAutoId)} of {lotAutoId} and {NameOf(Entities.LotYieldEntity.MultimeterId)} of {multimeterId} and {NameOf(Entities.LotYieldEntity.YieldTraitAutoId)} of {Me.YieldTraitEntity.AutoId}")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing <see cref="Entities.LotEntity"/> and
    ''' <see cref="Entities.ElementEntity"/> and fetches or inserts a new
    ''' <see cref="YieldTraitEntity"/> and fetches an existing or inserts a new
    ''' <see cref="LotYieldEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="lotAutoId">        Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitType"/>. </param>
    ''' <param name="amount">           The amount. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal multimeterId As Integer,
                              ByVal yieldTraitTypeId As Integer, ByVal amount As Double) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.LotEntity Is Nothing OrElse Me.LotEntity.AutoId <> lotAutoId Then
            ' make sure we have a lot associated with the lot auto ID.
            Me._LotEntity = New LotEntity With {.AutoId = lotAutoId}
            If Not Me._LotEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.AutoId)} of {lotAutoId}")
            End If
        End If
        If Me.ElementEntity Is Nothing OrElse Me.ElementEntity.AutoId <> multimeterId Then
            ' make sure we have an element associated with the element auto ID.
            Me._ElementEntity = New ElementEntity With {.AutoId = multimeterId}
            If Not Me._ElementEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed fetching {NameOf(Entities.ElementEntity)} with {NameOf(Entities.ElementEntity.AutoId)} of {multimeterId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainYieldAttribute(connection, lotAutoId, multimeterId, yieldTraitTypeId, amount)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(LotYieldEntity.LotEntity))
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing <see cref="Entities.LotEntity"/> and
    ''' <see cref="Entities.ElementEntity"/> and fetches or inserts a new
    ''' <see cref="YieldTraitEntity"/> and fetches an existing or inserts a new
    ''' <see cref="LotYieldEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitType"/>. </param>
    ''' <param name="amount">           The amount. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal yieldTraitTypeId As Integer,
                              ByVal amount As Double) As (Success As Boolean, Details As String)
        Return Me.TryObtain(connection, Me.LotAutoId, Me.MultimeterId, yieldTraitTypeId, amount)
    End Function

    ''' <summary>
    ''' Tries to fetch existing <see cref="Entities.LotEntity"/> and
    ''' <see cref="Entities.ElementEntity"/> and fetches or inserts a new
    ''' <see cref="YieldTraitEntity"/> and fetches an existing or inserts a new
    ''' <see cref="LotYieldEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="lotAutoId">        Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">     Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitType"/>. </param>
    ''' <param name="amount">           The amount. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Function Obtain(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal multimeterId As Integer,
                                     ByVal yieldTraitTypeId As Integer, ByVal amount As Double) As Boolean
        Dim r As (Success As Boolean, Details As String)
        r = Me.TryObtain(connection, lotAutoId, multimeterId, yieldTraitTypeId, amount)
        If Not r.Success Then Throw New OperationFailedException(r.Details)
        Return r.Success
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ITwoToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If LotYieldEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId, entity.TernaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="lotAutoId">    Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal multimeterId As Integer) As Boolean
        Return connection.Delete(Of LotYieldNub)(New LotYieldNub With {.PrimaryId = lotAutoId, .SecondaryId = multimeterId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the <see cref="LotYieldEntity"/>'s. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Lot-Element entities. </value>
    Public ReadOnly Property LotMeterYields As IEnumerable(Of LotYieldEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of LotYieldEntity)
        Return If(usingNativeTracking, LotYieldEntity.Populate(connection.GetAll(Of ITwoToMany)), LotYieldEntity.Populate(connection.GetAll(Of LotYieldNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._LotMeterYields = LotYieldEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(LotYieldEntity.LotMeterYields))
        Return If(Me.LotMeterYields?.Any, Me.LotMeterYields.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of LotYieldNub)) As IEnumerable(Of LotYieldEntity)
        Dim l As New List(Of LotYieldEntity)
        If nubs?.Any Then
            For Each nub As LotYieldNub In nubs
                l.Add(New LotYieldEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of ITwoToMany)) As IEnumerable(Of LotYieldEntity)
        Dim l As New List(Of LotYieldEntity)
        If interfaces?.Any Then
            Dim nub As New LotYieldNub
            For Each iFace As ITwoToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New LotYieldEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>
    ''' Count <see cref="LotYieldEntity"/>'s by <see cref="Entities.LotEntity"/>; returns up to
    ''' Element entities count.
    ''' </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotYieldBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotYieldNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the <see cref="LotYieldEntity"/>'s by <see cref="Entities.LotEntity"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of LotYieldEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotYieldBuilder.TableName}] WHERE {NameOf(LotYieldNub.PrimaryId)} = @lotAutoId", New With {Key lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotYieldEntity.Populate(connection.Query(Of LotYieldNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>
    ''' Count <see cref="LotYieldEntity"/>'s by <see cref="Entities.ElementEntity"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of entities by Element. </returns>
    Public Shared Function CountEntitiesByElement(ByVal connection As System.Data.IDbConnection, ByVal multimeterId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotYieldBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotYieldNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = multimeterId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the <see cref="LotYieldEntity"/>'s by Elements in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Elements in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByElement(ByVal connection As System.Data.IDbConnection, ByVal multimeterId As Integer) As IEnumerable(Of LotYieldEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotYieldBuilder.TableName}] WHERE {NameOf(LotYieldNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = multimeterId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotYieldEntity.Populate(connection.Query(Of LotYieldNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count <see cref="LotYieldEntity"/>'s; returns 1 or 0. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="lotAutoId">            Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">         Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="yieldAttributeAutoId"> Identifies the <see cref="YieldTraitEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                         ByVal multimeterId As Integer, ByVal yieldAttributeAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotYieldBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotYieldNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotYieldNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = multimeterId})
        sqlBuilder.Where($"{NameOf(LotYieldNub.TernaryId)} = @TernaryId", New With {.TernaryId = yieldAttributeAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches <see cref="LotYieldNub"/>'s; expects single entity or none. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="lotAutoId">            Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">         Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="yieldAttributeAutoId"> Identifies the <see cref="YieldTraitEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                     ByVal multimeterId As Integer, ByVal yieldAttributeAutoId As Integer) As IEnumerable(Of LotYieldNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotYieldBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotYieldNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotYieldNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = multimeterId})
        sqlBuilder.Where($"{NameOf(LotYieldNub.TernaryId)} = @TernaryId", New With {.TernaryId = yieldAttributeAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotYieldNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="LotYieldEntity"/> exists. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <param name="connection">           The connection. </param>
    ''' <param name="lotAutoId">            Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">         Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="yieldAttributeAutoId"> Identifies the <see cref="YieldTraitEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                    ByVal multimeterId As Integer, ByVal yieldAttributeAutoId As Integer) As Boolean
        Return 1 = LotYieldEntity.CountEntities(connection, lotAutoId, multimeterId, yieldAttributeAutoId)
    End Function

#End Region

#Region " RELATIONS: LOT "

    ''' <summary> Gets or sets the <see cref="Entities.LotEntity"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Lot entity. </value>
    Public ReadOnly Property LotEntity As LotEntity

    ''' <summary> Fetches  <see cref="Entities.LotEntity"/>. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Lot Entity. </returns>
    Public Function FetchLotEntity(ByVal connection As System.Data.IDbConnection) As LotEntity
        Dim entity As New LotEntity()
        entity.FetchUsingKey(connection, Me.LotAutoId)
        Me._LotEntity = entity
        Return entity
    End Function

    ''' <summary>
    ''' Count <see cref="Entities.LotEntity"/>'s associated with the specified
    ''' <paramref name="MultimeterId"/>; expected 1.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> The total number of Lots. </returns>
    Public Shared Function CountLots(ByVal connection As System.Data.IDbConnection, ByVal multimeterId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{LotYieldBuilder.TableName}] WHERE {NameOf(LotYieldNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = multimeterId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the <see cref="Entities.LotEntity"/>'s associated with the specified
    ''' <paramref name="MultimeterId"/>; expected a single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Lots in this collection.
    ''' </returns>
    Public Shared Function FetchLots(ByVal connection As System.Data.IDbConnection, ByVal multimeterId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotYieldBuilder.TableName}] WHERE {NameOf(LotYieldNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = multimeterId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ElementEntity)
        For Each nub As LotYieldNub In connection.Query(Of LotYieldNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotYieldEntity(nub)
            l.Add(entity.FetchElementEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Lots associated with the specified <paramref name="MultimeterId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteLots(ByVal connection As System.Data.IDbConnection, ByVal multimeterId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotYieldBuilder.TableName}] WHERE {NameOf(LotYieldNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = multimeterId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

#End Region

#Region " RELATIONS: Multimeter "

    ''' <summary> Gets or sets the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="Entities.MultimeterEntity"/>. </value>
    Public ReadOnly Property MultimeterEntity As MultimeterEntity

    ''' <summary> Fetches a Multimeter Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The <see cref="Entities.MultimeterEntity"/>. </returns>
    Public Function FetchMultimeterEntity(ByVal connection As System.Data.IDbConnection) As MultimeterEntity
        Dim entity As New MultimeterEntity()
        entity.FetchUsingKey(connection, Me.MultimeterId)
        Me._MultimeterEntity = entity
        Return entity
    End Function

    ''' <summary> Count Multimeters. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The total number of Multimeters. </returns>
    Public Shared Function CountMultimeters(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{LotYieldBuilder.TableName}] WHERE {NameOf(LotYieldNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Multimeters in this collection. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Multimeters in this collection.
    ''' </returns>
    Public Shared Function FetchMultimeters(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of MultimeterEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
             $"SELECT * FROM [{LotYieldBuilder.TableName}] WHERE {NameOf(LotYieldNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of MultimeterEntity)
        For Each nub As LotYieldNub In connection.Query(Of LotYieldNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotYieldEntity(nub)
            l.Add(entity.FetchMultimeterEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Multimeter related to the specified Lot. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteMultimeters(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotYieldBuilder.TableName}] WHERE {NameOf(LotYieldNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.MultimeterEntity"/>'s. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered Multimeters in this
    ''' collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchOrderedMultimeters(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer) As IEnumerable(Of MultimeterEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .LotAutoId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return MultimeterEntity.Populate(connection.Query(Of MultimeterNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.MultimeterEntity"/>'s. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered Multimeters in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchOrderedMultimeters(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of MultimeterEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotMeterYield] on [LotMeterYield].SecondaryId = [Multimeter].AutoId where [LotMeterYield].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{MultimeterBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{MultimeterBuilder.TableName}] Inner Join [{LotYieldBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotYieldBuilder.TableName}].{NameOf(LotYieldNub.SecondaryId)} = [{MultimeterBuilder.TableName}].{NameOf(MultimeterNub.Id)}")
        queryBuilder.AppendLine($"WHERE [{LotYieldBuilder.TableName}].{NameOf(LotYieldNub.PrimaryId)} = @LotAutoId")
        queryBuilder.AppendLine($"ORDER BY [{MultimeterBuilder.TableName}].{NameOf(MultimeterNub.Id)} ASC; ")
        Return LotYieldEntity.FetchOrderedMultimeters(connection, queryBuilder.ToString, lotAutoId)
    End Function

    ''' <summary> Fetches an <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="selectQuery">      The select query. </param>
    ''' <param name="lotAutoId">        Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterNumber"> The <see cref="Entities.MultimeterEntity"/> number. </param>
    ''' <returns> The Multimeter. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchMultimeter(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer, ByVal multimeterNumber As Integer) As MultimeterEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.LotAutoId = lotAutoId, .MultimeterLabel = multimeterNumber})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As MultimeterNub = connection.Query(Of MultimeterNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New MultimeterEntity, New MultimeterEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <remarks>
    ''' David, 5/19/2020.
    ''' <code>
    ''' SELECT [Multimeter].*
    ''' FROM [Multimeter] Inner Join [LotMeterYield]
    ''' ON [LotMeterYield].[SecondaryId] = [Multimeter].[Id]
    ''' WHERE ([LotMeterYield].[PrimaryId] = 8 AND [Multimeter].[Number] = 1)
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="lotAutoId">        Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterNumber"> The <see cref="Entities.MultimeterEntity"/>number. </param>
    ''' <returns> The Multimeter. </returns>
    Public Shared Function FetchMultimeter(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal multimeterNumber As Integer) As MultimeterEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotMeterYield] on [LotMeterYield].SecondaryId = [Multimeter].AutoId where [LotMeterYield].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{MultimeterBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{MultimeterBuilder.TableName}] Inner Join [{LotYieldBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotYieldBuilder.TableName}].[{NameOf(LotYieldNub.SecondaryId)}] = [{MultimeterBuilder.TableName}].[{NameOf(MultimeterNub.Id)}]")
        queryBuilder.AppendLine($"WHERE ([{LotYieldBuilder.TableName}].[{NameOf(LotYieldNub.PrimaryId)}] = @LotAutoId ")
        queryBuilder.AppendLine($"AND [{MultimeterBuilder.TableName}].[{NameOf(MultimeterNub.Amount)}] = @MultimeterNumber); ")
        Return LotYieldEntity.FetchMultimeter(connection, queryBuilder.ToString, lotAutoId, multimeterNumber)
    End Function

#End Region

#Region " RELATIONS: ELEMENT "

    ''' <summary> Gets or sets the Element entity. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Element entity. </value>
    Public ReadOnly Property ElementEntity As ElementEntity

    ''' <summary> Fetches a Element Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Element Entity. </returns>
    Public Function FetchElementEntity(ByVal connection As System.Data.IDbConnection) As ElementEntity
        Dim entity As New ElementEntity()
        entity.FetchUsingKey(connection, Me.MultimeterId)
        Me._ElementEntity = entity
        Return entity
    End Function

    ''' <summary> Count elements. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The total number of elements. </returns>
    Public Shared Function CountElements(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{LotYieldBuilder.TableName}] WHERE {NameOf(LotYieldNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Elements in this collection. </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Elements in this collection.
    ''' </returns>
    Public Shared Function FetchElements(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotYieldBuilder.TableName}] WHERE {NameOf(LotYieldNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ElementEntity)
        For Each nub As LotYieldNub In connection.Query(Of LotYieldNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotYieldEntity(nub)
            l.Add(entity.FetchElementEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Element related to the specified Lot. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteElements(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotYieldBuilder.TableName}] WHERE {NameOf(LotYieldNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.ElementEntity"/>'s. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered elements in this
    ''' collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchOrderedElements(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .LotAutoId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return ElementEntity.Populate(connection.Query(Of ElementNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered <see cref="Entities.ElementEntity"/>'s. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered elements in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchOrderedElements(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of ElementEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotMeterYield] on [LotMeterYield].SecondaryId = [Element].AutoId where [LotMeterYield].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{ElementBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{ElementBuilder.TableName}] Inner Join [{LotYieldBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotYieldBuilder.TableName}].{NameOf(LotYieldNub.SecondaryId)} = [{ElementBuilder.TableName}].{NameOf(ElementNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{LotYieldBuilder.TableName}].{NameOf(LotYieldNub.PrimaryId)} = @LotAutoId")
        queryBuilder.AppendLine($"ORDER BY [{ElementBuilder.TableName}].{NameOf(ElementNub.Label)} ASC; ")
        Return LotYieldEntity.FetchOrderedElements(connection, queryBuilder.ToString, lotAutoId)
    End Function

    ''' <summary> Fetches an <see cref="Entities.ElementEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="selectQuery">  The select query. </param>
    ''' <param name="lotAutoId">    Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="elementLabel"> The <see cref="Entities.ElementEntity"/>label. </param>
    ''' <returns> The element. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchElement(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer, ByVal elementLabel As String) As ElementEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.LotAutoId = lotAutoId, .ElementLabel = elementLabel})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As ElementNub = connection.Query(Of ElementNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New ElementEntity, New ElementEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an <see cref="Entities.ElementEntity"/>. </summary>
    ''' <remarks>
    ''' David, 5/19/2020.
    ''' <code>
    ''' SELECT [Element].*
    ''' FROM [Element] Inner Join [LotMeterYield]
    ''' ON [LotMeterYield].[SecondaryId] = [Element].[AutoId]
    ''' WHERE ([LotMeterYield].[PrimaryId] = 8 AND [Element].[Label] = 'R1')
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="lotAutoId">    Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="elementLabel"> The <see cref="Entities.ElementEntity"/>label. </param>
    ''' <returns> The element. </returns>
    Public Shared Function FetchElement(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal elementLabel As String) As ElementEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotMeterYield] on [LotMeterYield].SecondaryId = [Element].AutoId where [LotMeterYield].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{ElementBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{ElementBuilder.TableName}] Inner Join [{LotYieldBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotYieldBuilder.TableName}].[{NameOf(LotYieldNub.SecondaryId)}] = [{ElementBuilder.TableName}].[{NameOf(ElementNub.AutoId)}]")
        queryBuilder.AppendLine($"WHERE ([{LotYieldBuilder.TableName}].[{NameOf(LotYieldNub.PrimaryId)}] = @LotAutoId ")
        queryBuilder.AppendLine($"AND [{ElementBuilder.TableName}].[{NameOf(ElementNub.Label)}] = @ElementLabel); ")
        Return LotYieldEntity.FetchElement(connection, queryBuilder.ToString, lotAutoId, elementLabel)
    End Function

#End Region

#Region " RELATIONS: YIELD TRAITS "

    ''' <summary> Gets or sets the <see cref="YieldTraitEntity"/>. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="YieldTraitEntity"/>. </value>
    Public ReadOnly Property YieldTraitEntity As YieldTraitEntity

    ''' <summary> Fetches a <see cref="YieldTraitEntity"/>. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The <see cref="YieldTraitEntity"/>. </returns>
    Public Function FetchYieldAttributeEntity(ByVal connection As System.Data.IDbConnection) As YieldTraitEntity
        Me._YieldTraitEntity = LotYieldEntity.FetchYieldTrait(connection, Me.LotAutoId, Me.MultimeterId, Me.YieldTraitAutoId)
        Return Me._YieldTraitEntity
    End Function

    ''' <summary> Fetches the ordered <see cref="YieldTraitEntity"/>'s. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="selectQuery">  The select query. </param>
    ''' <param name="lotAutoId">    Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered
    ''' <see cref="YieldTraitEntity"/>'s in this collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchOrderedYieldTraits(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String,
                                                   ByVal lotAutoId As Integer, ByVal multimeterId As Integer) As IEnumerable(Of YieldTraitEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {lotAutoId, multimeterId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return YieldTraitEntity.Populate(connection.Query(Of YieldTraitNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered <see cref="YieldTraitEntity"/>'s. </summary>
    ''' <remarks>
    ''' David, 5/9/2020.
    ''' <code>
    ''' SELECT [YieldTrait].*
    ''' FROM [YieldTrait] Inner Join [LotMeterYield]
    ''' ON [LotMeterYield].[TernaryId] = [YieldTrait].[AutoId]
    ''' WHERE ([LotMeterYield].[PrimaryId] = 8 AND [LotMeterYield].[SecondaryId] = 1)
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="lotAutoId">    Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId"> Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered
    ''' <see cref="YieldTraitEntity"/>'s in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedYieldTraits(ByVal connection As System.Data.IDbConnection,
                                                         ByVal lotAutoId As Integer, ByVal multimeterId As Integer) As IEnumerable(Of YieldTraitEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotMeterYield] on [LotMeterYield].SecondaryId = [YieldAttribute].AutoId where [LotMeterYield].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{YieldTraitBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{YieldTraitBuilder.TableName}] Inner Join [{LotYieldBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotYieldBuilder.TableName}].{NameOf(LotYieldNub.TernaryId)} = [{YieldTraitBuilder.TableName}].{NameOf(YieldTraitNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{LotYieldBuilder.TableName}].[{NameOf(LotYieldNub.PrimaryId)}] = @LotAutoId ")
        queryBuilder.AppendLine($"AND [{LotYieldBuilder.TableName}].[{NameOf(LotYieldNub.SecondaryId)}] = @MultimeterId) ")
        queryBuilder.AppendLine($"ORDER BY [{YieldTraitBuilder.TableName}].{NameOf(YieldTraitNub.ForeignId)} ASC; ")
        Return LotYieldEntity.FetchOrderedYieldTraits(connection, queryBuilder.ToString, lotAutoId, multimeterId)
    End Function

    ''' <summary> Fetches an <see cref="Entities.YieldTraitEntity"/>. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="selectQuery">      The select query. </param>
    ''' <param name="lotAutoId">        Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">     Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="yieldTraitTypeId"> Identifies the
    '''                                       <see cref="YieldTraitTypeEntity"/>. </param>
    ''' <returns> The YieldAttribute. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchYieldTrait(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer,
                                                 ByVal multimeterId As Integer, ByVal yieldTraitTypeId As Integer) As YieldTraitEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.LotAutoId = lotAutoId,
                                                                                                     .MultimeterId = multimeterId,
                                                                                                     .YieldTraitTypeId = yieldTraitTypeId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As YieldTraitNub = connection.Query(Of YieldTraitNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New YieldTraitEntity, New YieldTraitEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an <see cref="Entities.YieldTraitEntity"/>. </summary>
    ''' <remarks>
    ''' David, 6/16/2020.
    ''' <code>
    ''' SELECT [YieldTrait].*
    ''' FROM [YieldTrait] Inner Join [LotMeterYield]
    ''' ON [LotMeterYield].[TernaryId] = [YieldTrait].[AutoId]
    ''' WHERE ([LotMeterYield].[PrimaryId] = 8 AND [YieldTrait].[FirstForeignId] = 1 AND [YieldTrait].[SecondForeignId] = 1 )
    ''' </code>
    ''' </remarks>
    ''' <param name="connection">       The connection. </param>
    ''' <param name="lotAutoId">        Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeterId">     Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitTypeEntity"/>. </param>
    ''' <returns> The YieldAttribute. </returns>
    Public Shared Function FetchYieldTrait(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer,
                                               ByVal multimeterId As Integer,
                                               ByVal yieldTraitTypeId As Integer) As YieldTraitEntity
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"SELECT [{YieldTraitBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{YieldTraitBuilder.TableName}] Inner Join [{LotYieldBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotYieldBuilder.TableName}].[{NameOf(LotYieldNub.TernaryId)}] = [{YieldTraitBuilder.TableName}].[{NameOf(YieldTraitNub.AutoId)}]")
        queryBuilder.AppendLine($"WHERE ([{LotYieldBuilder.TableName}].[{NameOf(LotYieldNub.PrimaryId)}] = @{NameOf(lotAutoId)} ")
        queryBuilder.AppendLine($"AND [{LotYieldBuilder.TableName}].[{NameOf(LotYieldNub.SecondaryId)}] = @{NameOf(multimeterId)} ")
        queryBuilder.AppendLine($"AND [{YieldTraitBuilder.TableName}].[{NameOf(YieldTraitNub.ForeignId)}] = @{NameOf(yieldTraitTypeId)}); ")
        Return LotYieldEntity.FetchYieldTrait(connection, queryBuilder.ToString, lotAutoId, multimeterId, yieldTraitTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements ITwoToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotYieldEntity.LotAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.LotEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/>. </value>
    Public Property LotAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements ITwoToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotYieldEntity.MultimeterId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/>.
    ''' </summary>
    ''' <value> Identifies the "Entities.MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Ternary reference. </summary>
    ''' <value> The identifier of Ternary reference. </value>
    Public Property TernaryId As Integer Implements ITwoToMany.TernaryId
        Get
            Return Me.ICache.TernaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.TernaryId, value) Then
                Me.ICache.TernaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotYieldEntity.YieldTraitAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="YieldTraitEntity"/>. </summary>
    ''' <value> Identifies the <see cref="YieldTraitEntity"/>. </value>
    Public Property YieldTraitAutoId As Integer
        Get
            Return Me.TernaryId
        End Get
        Set(value As Integer)
            Me.TernaryId = value
        End Set
    End Property

#End Region

End Class

''' <summary>
''' Collection of unique <see cref="Entities.ElementEntity"/> +
''' <see cref="Entities.MultimeterEntity"/> +
''' <see cref="Entities.NomTypeEntity"/> <see cref="YieldTraitEntity"/>'s.
''' </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class LotYieldTraitEntityCollection
    Inherits YieldTraitEntityCollection
    Implements isr.Core.Constructs.IGetterSetter(Of Double)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="lot">        The <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="multimeter"> The <see cref="Entities.MultimeterEntity"/>. </param>
    Public Sub New(ByVal lot As LotEntity, ByVal multimeter As MultimeterEntity)
        MyBase.New()
        Me.LotAutoId = lot.AutoId
        Me.MultimeterId = multimeter.Id
        Me._UniqueIndexDictionary = New Dictionary(Of DualKeySelector, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, DualKeySelector)
        Me._YieldTrait = New YieldTrait(Me, multimeter, lot.Traits.LotTrait.CertifiedQuantity)
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of DualKeySelector, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, DualKeySelector)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/16/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As YieldTraitEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.YieldTraitTypeId, entity.EntitySelector)
        Me._UniqueIndexDictionary.Add(entity.EntitySelector, entity.YieldTraitTypeId)
        Me.NotifyPropertyChanged(YieldTraitTypeEntity.EntityLookupDictionary(entity.YieldTraitTypeId).Label)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Queries if collection contains a key. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitTypeEntity"/>. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsAttribute(ByVal yieldTraitTypeId As Integer) As Boolean
        Return Me._PrimaryKeyDictionary.ContainsKey(yieldTraitTypeId)
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.LotEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/>. </value>
    Public ReadOnly Property LotAutoId As Integer

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public ReadOnly Property MultimeterId As Integer


#End Region

#Region " GETTER SETTER "

    ''' <summary>
    ''' Gets the Real(Double)-value for the given <see cref="YieldTraitTypeEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Nullable Double. </returns>
    Protected Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double? Implements Core.Constructs.IGetterSetter(Of Double).Getter
        Return Me.Getter(Me.ToKey(name))
    End Function

    ''' <summary>
    ''' Set the specified element value for the given <see cref="YieldTraitTypeEntity.Label"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value">                                      value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller member. </param>
    ''' <returns> A Double. </returns>
    Protected Function Setter(ByVal value As Double, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double Implements Core.Constructs.IGetterSetter(Of Double).Setter
        Me.Setter(Me.ToKey(name), value)
        Me.NotifyPropertyChanged(name)
        Return value
    End Function

    ''' <summary> Gets or sets the Yield Trait. </summary>
    ''' <value> The Yield Trait. </value>
    Public ReadOnly Property YieldTrait As YieldTrait

#End Region

#Region " TRAIT SELECTION "

    ''' <summary>
    ''' Converts a name to a key using the
    ''' <see cref="YieldTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
    ''' extend the element Yield Traits beyond the values of the enumeration type that is used to
    ''' populate this table.
    ''' </summary>
    ''' <remarks> David, 5/11/2020. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Protected Overridable Function ToKey(ByVal name As String) As Integer
        Return YieldTraitTypeEntity.KeyLookupDictionary(name)
    End Function

    ''' <summary> Sets the trait value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Double. </returns>
    Public Function Setter(ByVal value As Double?, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        If value.HasValue AndAlso Not Nullable.Equals(value, Me.Getter(name)) Then
            Me.Setter(value.Value, name)
            Me.NotifyPropertyChanged(name)
        End If
        Return value
    End Function


#If False Then
    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.Getter(Me.ToKey(name))
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.Setter(Me.ToKey(name), value.Value)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property
#End If

    ''' <summary> gets the entity associated with the specified type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitTypeEntity"/>. </param>
    ''' <returns> An elementReadingRealEntity. </returns>
    Public Function Entity(ByVal yieldTraitTypeId As Integer) As YieldTraitEntity
        Return If(Me.ContainsAttribute(yieldTraitTypeId), Me(Me._PrimaryKeyDictionary(yieldTraitTypeId)), New YieldTraitEntity)
    End Function

    ''' <summary> Gets the Real(Double)-value for the given Trait Type. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitTypeEntity"/>. </param>
    ''' <returns> A Double? </returns>
    Public Function Getter(ByVal yieldTraitTypeId As Integer) As Double?
        Return If(Me.ContainsAttribute(yieldTraitTypeId), Me(Me._PrimaryKeyDictionary(yieldTraitTypeId)).Amount, New Double?)
    End Function

    ''' <summary> Set the specified element value. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitTypeEntity"/>. </param>
    ''' <param name="value">            The value. </param>
    Public Sub Setter(ByVal yieldTraitTypeId As Integer, ByVal value As Double)
        If Me.ContainsAttribute(yieldTraitTypeId) Then
            Me(Me._PrimaryKeyDictionary(yieldTraitTypeId)).Amount = value
        Else
            Me.Add(New YieldTraitEntity With {.YieldTraitTypeId = yieldTraitTypeId, .Amount = value})
        End If
    End Sub

#End Region

#Region " UPSERT "

    ''' <summary> Inserts or updates all entities using the given connection. </summary>
    ''' <remarks>
    ''' David, 5/13/2020. Entities that failed to save are enumerated in
    ''' <see cref="P:isr.Dapper.Entity.EntityKeyedCollection`4.UnsavedKeys" />
    ''' </remarks>
    ''' <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <CLSCompliant(False)>
    Public Overrides Function Upsert(ByVal transcactedConnection As TransactedConnection) As Boolean
        Me.ClearUnsavedKeys()
        ' dictionary is instantiated only after the collection has values
        If Not Me.Any Then Return True
        Dim lotMeterYield As LotYieldEntity
        For Each keyValue As KeyValuePair(Of DualKeySelector, YieldTraitEntity) In Me.Dictionary
            If keyValue.Value.Upsert(transcactedConnection) Then
                lotMeterYield = New LotYieldEntity() With {.LotAutoId = Me.LotAutoId,
                                                           .MultimeterId = Me.MultimeterId,
                                                           .YieldTraitAutoId = keyValue.Value.AutoId}
                If Not lotMeterYield.Obtain(transcactedConnection) Then
                    Me.AddUnsavedKey(keyValue.Key)
                End If
            Else
                Me.AddUnsavedKey(keyValue.Key)
            End If
        Next
        ' success if no unsaved keys
        Return Not Me.UnsavedKeys.Any
    End Function

#End Region

End Class
