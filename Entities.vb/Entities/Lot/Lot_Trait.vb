
Partial Public Class LotEntity

    ''' <summary> Gets or sets the <see cref="LotTraitEntity"/> collection. </summary>
    ''' <value> The traits. </value>
    Public ReadOnly Property Traits As LotUniqueTraitEntityCollection

    ''' <summary>
    ''' Fetches the <see cref="LotUniqueTraitEntityCollection"/> Natural(Integer)-value type trait
    ''' entity collection.
    ''' </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of trait. </returns>
    Public Function FetchTraits(ByVal connection As System.Data.IDbConnection) As Integer
        If Not LotTraitTypeEntity.IsEnumerated() Then LotTraitTypeEntity.TryFetchAll(connection)
        Me._Traits = New LotUniqueTraitEntityCollection(Me.AutoId)
        Me._Traits.Populate(LotTraitEntity.FetchEntities(connection, Me.AutoId))
        Return Me.Traits.Count
    End Function

End Class
