

Imports System.ComponentModel

Imports Dapper

Imports isr.Core

''' <summary> Monitors the active UUT. </summary>
''' <remarks> David, 5/28/2020. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Class UutMonitor
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <param name="multimeter"> The <see cref="MultimeterEntity"/>. </param>
    Public Sub New(ByVal multimeter As MultimeterEntity)
        MyBase.New
        Me.Multimeter = multimeter
        Me.ActiveUUTEntity = New UutEntity
        Me.NomReadingsBindingList = New isr.Core.Constructs.InvokingBindingList(Of NomReading)
        ' AddHandler Me.ActiveUUTEntity.PropertyChanged, AddressOf Me.ActiveUUTEntityPropertyChanged
    End Sub

#End Region

#Region " MULTIMETER IDENTITY "

    Private _Multimeter As MultimeterEntity

    ''' <summary> Gets or sets the multimeter. </summary>
    ''' <value> The multimeter. </value>
    Public Property Multimeter As MultimeterEntity
        Get
            Return Me._Multimeter
        End Get
        Set(value As MultimeterEntity)
            If Not MultimeterEntity.Equals(value, Me.Multimeter) Then
                Me._Multimeter = value
                Me.NotifyPropertyChanged()
            End If

        End Set
    End Property

    ''' <summary> Gets the multimeter number. </summary>
    ''' <value> The multimeter number. </value>
    Public ReadOnly Property MultimeterNumber As Integer
        Get
            Return Me.Multimeter.MultimeterNumber
        End Get
    End Property

    ''' <summary> Gets the identifier of the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public ReadOnly Property MultimeterId As Integer
        Get
            Return Me.Multimeter.Id
        End Get
    End Property

#End Region

#Region " NUT's "

    ''' <summary> Adds a nut entity. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifier for the uut automatic. </param>
    ''' <param name="elementId">  Identifier for the element. </param>
    ''' <param name="nomTypeId">  Identifier for the nom type. </param>
    ''' <returns> A Dapper.Entities.UutNutEntity. </returns>
    Public Shared Function AddNutEntity(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer,
                                        ByVal elementId As Integer, ByVal nomTypeId As Integer) As Dapper.Entities.UutNutEntity
        Dim uutNut As Dapper.Entities.UutNutEntity
        uutNut = New Dapper.Entities.UutNutEntity()
        Dim r As (Success As Boolean, Details As String) = uutNut.TryObtainNut(connection, uutAutoId, elementId, nomTypeId)
        If Not r.Success Then
            Throw New OperationFailedException($"{NameOf(Dapper.Entities.UutNutEntity)} with '{NameOf(Dapper.Entities.UutNutEntity.UutAutoId)}' of {uutAutoId} should have been inserted; { r.Details}")
        End If
        Return uutNut
    End Function

    ''' <summary> Adds a nut entities. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="elements">   The elements. </param>
    ''' <param name="uut">        The uut. </param>
    ''' <returns> A Dapper.Entities.UutEntity. </returns>
    Public Shared Function AddNutEntities(ByVal connection As System.Data.IDbConnection,
                                          ByVal elements As Dapper.Entities.ProductUniqueElementEntityCollection,
                                          ByVal uut As Dapper.Entities.UutEntity) As Dapper.Entities.UutEntity
        Dim nut As Dapper.Entities.NutEntity
        Dim expectedCount As Integer = 0
        For Each element As Dapper.Entities.ElementEntity In elements
            For Each nomTypeEntity As Dapper.Entities.NomTypeEntity In element.NomTypes
                nut = UutMonitor.AddNutEntity(connection, uut.AutoId, element.AutoId, nomTypeEntity.Id).NutEntity
                nut.InitializeNomReadings(uut, element)
                nut.FetchNomReadings(connection)
                expectedCount += 1
            Next
        Next
        If expectedCount <> uut.FetchNuts(connection) Then Throw New OperationFailedException($"{NameOf(Dapper.Entities.UutEntity.Nuts)} count {uut.Nuts.Count} should be {expectedCount}")
        uut.InitializeNomReadings(elements)
        uut.FetchNomReadings(connection)
        uut.UutState = If((uut.Nuts?.Any).GetValueOrDefault(False), Dapper.Entities.UutState.Ready, Dapper.Entities.UutState.Undefined)
        For Each nut In uut.Nuts
            If nut.NomReadings.Any Then
                uut.UutState = Dapper.Entities.UutState.Measured
                Exit For
            End If
        Next
        Return uut
    End Function

#End Region

#Region " ACTIVE UUT "

    ''' <summary> Releases the active uut. </summary>
    ''' <remarks> David, 7/8/2020. </remarks>
    Public Sub ReleaseActiveUut()
        If Me.ActiveUUTEntity IsNot Nothing Then
            ' RemoveHandler Me.ActiveUUTEntity.PropertyChanged, AddressOf Me.ActiveUUTEntityPropertyChanged
            Me._ActiveUUTEntity = Nothing
        End If
    End Sub

    ''' <summary> Prepare next uut. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="elements">   The <see cref="ElementEntity"/>'s. </param>
    ''' <param name="lotAutoId">  Identifier for the <see cref="LotEntity"/>. </param>
    ''' <param name="uutType">    Type of the uut. </param>
    Public Sub PrepareNextUut(ByVal connection As System.Data.IDbConnection, ByVal elements As Dapper.Entities.ProductUniqueElementEntityCollection,
                              ByVal lotAutoId As Integer, ByVal uutType As Dapper.Entities.UutType)
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        Me.PrepareNextUut(New TransactedConnection(connection, transaction), elements, lotAutoId, uutType)
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            Me.PrepareNextUut(transactedConnection, elements, lotAutoId, uutType)
        End If
    End Sub

    ''' <summary> Prepare next uut. </summary>
    ''' <remarks> David, 7/11/2020. <para>
    ''' SQLite elapsed time: less than 20 ms </para><para>
    ''' SQL Server elapsed time: Less than 10 ms.</para><para>
    ''' First elapsed time: ~70 ms, which occurs before test starts.
    ''' </para> </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="elements">   The <see cref="ElementEntity"/>'s. </param>
    ''' <param name="lotAutoId">  Identifier for the <see cref="LotEntity"/>. </param>
    ''' <param name="uutType">    Type of the uut. </param>
    <CLSCompliant(False)>
    Public Sub PrepareNextUut(ByVal connection As TransactedConnection, ByVal elements As Dapper.Entities.ProductUniqueElementEntityCollection,
                              ByVal lotAutoId As Integer, ByVal uutType As Dapper.Entities.UutType)
        Dim lotUut As New Dapper.Entities.LotUutEntity With {.LotAutoId = lotAutoId}
        Me.ActiveUUTEntity.UutState = UutState.Undefined
        Dim r As (Success As Boolean, Details As String) = lotUut.TryObtainUut(connection, lotAutoId, -1, uutType)
        If r.Success Then
            Me.PrepareUut(connection, elements, lotUut.UutAutoId)
        Else
            Throw New OperationFailedException(r.Details)
        End If
    End Sub

    ''' <summary>
    ''' Fetches the <see cref="UutMonitor.ActiveUUTEntity"/>, its <see cref="NutEntity"/>'s and
    ''' <see cref="ProductSortEntity"/> with readings and traits.
    ''' </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="elements">   The <see cref="ElementEntity"/>'s. </param>
    ''' <param name="uutAutoId">  Identifier for the <see cref="UutEntity"/>. </param>
    Public Sub PrepareUut(ByVal connection As System.Data.IDbConnection, ByVal elements As Dapper.Entities.ProductUniqueElementEntityCollection, ByVal uutAutoId As Integer)
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        Me.PrepareUut(New TransactedConnection(connection, transaction), elements, uutAutoId)
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            Me.PrepareUut(transactedConnection, elements, uutAutoId)
        End If
    End Sub

    ''' <summary>
    ''' Fetches the <see cref="UutMonitor.ActiveUUTEntity"/>, its <see cref="NutEntity"/>'s and
    ''' <see cref="ProductSortEntity"/> with readings and traits.
    ''' </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="elements">   The <see cref="ElementEntity"/>'s. </param>
    ''' <param name="uutAutoId">  Identifier for the <see cref="UutEntity"/>. </param>
    <CLSCompliant(False)>
    Public Sub PrepareUut(ByVal connection As TransactedConnection, ByVal elements As Dapper.Entities.ProductUniqueElementEntityCollection, ByVal uutAutoId As Integer)
        Me.ActiveUUTEntity.Clear()
        Me.ActiveUUTEntity.ProductSortEntity = Nothing
        Me.ActiveUUTEntity.AutoId = uutAutoId
        If Me.ActiveUUTEntity.FetchUsingKey(connection) Then
            Me.ActiveUUTEntity.FetchTraits(connection)
            Me.ActiveUUTEntity.Traits.UutTrait.MultimeterId = Me.MultimeterId
            Me.ActiveUUTEntity.Traits.Upsert(connection)
            UutMonitor.AddNutEntities(connection, elements, Me.ActiveUUTEntity)
            Me.ActiveUUTEntity.ProductSortEntity = UutProductSortEntity.FetchSortEntity(connection, uutAutoId)
            Me.ActiveUUTEntity.UutState = If(Me.ActiveUUTEntity.ProductSortEntity?.IsClean, Dapper.Entities.UutState.Complete, Me.ActiveUUTEntity.UutState)
        Else
            Throw New OperationFailedException($"Failed fetching {NameOf(UutEntity)} with {NameOf(UutEntity.AutoId)} of {uutAutoId}")
        End If
    End Sub

    ''' <summary> Gets or sets the active uut entity. </summary>
    ''' <value> The active uut entity. </value>
    Public ReadOnly Property ActiveUUTEntity As Entities.UutEntity

    ''' <summary> Gets or sets the exception. </summary>
    ''' <value> The exception. </value>
    Public ReadOnly Property Exception As Exception

    ''' <summary>
    ''' Sorts the Active UUT and sets the <see cref="Dapper.Entities.UutEntity.UutState"/>.
    ''' </summary>
    ''' <remarks> David, 7/16/2020. </remarks>
    ''' <param name="binnings"> The <see cref="ProductUniqueBinningEntityCollection"/>. </param>
    ''' <param name="sorts">    The <see cref="ProductUniqueProductSortEntityCollection"/>. </param>
    ''' <returns> An <see cref="Dapper.Entities.UutProductSortEntity"/>. </returns>
    Public Function SortUut(ByVal binnings As ProductUniqueBinningEntityCollection,
                            ByVal sorts As ProductUniqueProductSortEntityCollection) As Dapper.Entities.ProductSortEntity
        Dim result As Dapper.Entities.ProductSortEntity = If(Me.ActiveUUTEntity.UutState = UutState.Measured,
                                                                    Me.ActiveUUTEntity.DoSort(binnings, sorts),
                                                                    New Dapper.Entities.ProductSortEntity)
        Me.UpdateContinuousFailures(result)
        Return result
    End Function

    ''' <summary> Deletes the incomplete uut described by connection. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    <CLSCompliant(False)>
    Public Sub DeleteIncompleteUut(connection As TransactedConnection)
        ' remove any incomplete DUTs.
        If Me.ActiveUUTEntity.UutNumber > 0 AndAlso Me.ActiveUUTEntity.UutState <> Dapper.Entities.UutState.Complete Then
            Me.ActiveUUTEntity.Delete(connection)
        End If
    End Sub

#End Region

#Region " CONTINUOUS FAILURE MANAGEMENT "

    ''' <summary> Updates the continuous failures described by productSortEntity. </summary>
    ''' <remarks> David, 5/27/2020. </remarks>
    ''' <param name="productSortEntity"> The product sort entity. </param>
    Public Sub UpdateContinuousFailures(ByVal productSortEntity As Entities.ProductSortEntity)
        If productSortEntity Is Nothing Then
            ' if sort is nothing, ignore as this represents an intermediate state of getting a new UUT
        ElseIf Entities.ProductSortEntity.Equals(Me.LastSortEntity, productSortEntity) Then
            If Me.ActiveUUTEntity.BucketBin <> BucketBin.Good Then
                Me.ContinuousFailureCount += 1
                ' assumes that the product sort naturals were fetches when setting the product sort.
                Me.ContinuousFailureLimit = productSortEntity.Traits.ProductSortTrait?.ContinuousFailureCountLimit
                Me.ContinuousFailureAlert = Me.ContinuousFailureLimit.HasValue AndAlso Me.ContinuousFailureLimit.Value > 0 AndAlso
                    Me.ContinuousFailureLimit.Value <= Me.ContinuousFailureCount
                If Me._ContinuousFailureAlert Then
                    Dim kvp As New KeyValuePair(Of String, Integer)(productSortEntity.Label, Me.ContinuousFailureCount)
                    If Me.ContinuousFailuresStack.Any AndAlso String.Equals(Me.ContinuousFailuresStack.Peek.Key, productSortEntity.Label) Then
                        ' if this is the same sort as the previous continuous failure sort, pop the last entry to be replaced with a new one
                        Me.ContinuousFailuresStack.Pop()
                    Else
                    End If
                    Me.ContinuousFailuresStack.Push(kvp)
                End If
                Me.NotifyPropertyChanged(NameOf(UutMonitor.ContinuousFailureCount))
            Else
                Me.ContinuousFailureAlert = False
            End If
        Else
            ' if a new sort, initialize the Continuous Failure Count and clear the alert
            Me.ContinuousFailureAlert = False
            Me.ContinuousFailureCount = If(Me.ActiveUUTEntity.BucketBin <> BucketBin.Good, 1, 0)
        End If
        If productSortEntity IsNot Nothing Then Me.LastSortEntity = productSortEntity
    End Sub

    Private _ContinuousFailureAlert As Boolean

    ''' <summary> Gets or sets the continuous failure alert. </summary>
    ''' <value> The continuous failure alert. </value>
    Public Property ContinuousFailureAlert As Boolean
        Get
            Return Me._ContinuousFailureAlert
        End Get
        Set(value As Boolean)
            If value <> Me.ContinuousFailureAlert Then
                Me._ContinuousFailureAlert = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _LastSortEntity As ProductSortEntity

    ''' <summary> Gets or sets the last sort entity. </summary>
    ''' <value> The last sort entity. </value>
    Public Property LastSortEntity As ProductSortEntity
        Get
            Return Me._LastSortEntity
        End Get
        Set(value As ProductSortEntity)
            If Not ProductSortEntity.Equals(value, Me.LastSortEntity) Then
                Me._LastSortEntity = value
                Me.NotifyPropertyChanged()
            End If

        End Set
    End Property

    Private _ContinuousFailureCount As Integer

    ''' <summary> Gets or sets the number of continuous failures. </summary>
    ''' <value> The number of continuous failures. </value>
    Public Property ContinuousFailureCount As Integer
        Get
            Return Me._ContinuousFailureCount
        End Get
        Set(value As Integer)
            If value <> Me.ContinuousFailureCount Then
                Me._ContinuousFailureCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _ContinuousFailureLimit As Integer?

    ''' <summary> Gets or sets the continuous failure limit. </summary>
    ''' <value> The continuous failure limit. </value>
    Public Property ContinuousFailureLimit As Integer?
        Get
            Return Me._ContinuousFailureLimit
        End Get
        Set(value As Integer?)
            If Not Nullable.Equals(value, Me.ContinuousFailureLimit) Then
                Me._ContinuousFailureLimit = value
                Me.NotifyPropertyChanged()
            End If

        End Set
    End Property


    ''' <summary> Clears the continuous failures. </summary>
    ''' <remarks> David, 5/27/2020. </remarks>
    Public Sub ClearContinuousFailures()
        Me.ContinuousFailuresStack.Clear()
    End Sub

    ''' <summary> Gets or sets a stack of continuous failures. </summary>
    ''' <value> A stack of continuous failures. </value>
    Public ReadOnly Property ContinuousFailuresStack As New Stack(Of KeyValuePair(Of String, Integer))

#End Region

#Region " COMPLETED UUT INFO "

    Private _BucketColor As Integer

    ''' <summary> Gets or sets the bucket Color. </summary>
    ''' <value> The bucket Color. </value>
    Public Property BucketColor As Integer
        Get
            Return Me._BucketColor
        End Get
        Set
            If Value <> Me.BucketColor Then
                Me._BucketColor = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _CompletedUutNumber As Integer

    ''' <summary> Gets or sets the completed uut number. </summary>
    ''' <value> The ordinal number. </value>
    Public Property CompletedUutNumber As Integer
        Get
            Return Me._CompletedUutNumber
        End Get
        Set
            If Value <> Me.CompletedUutNumber Then
                Me._CompletedUutNumber = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _SortLabel As String

    ''' <summary> Gets or sets the sort label. </summary>
    ''' <value> The sort label. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property SortLabel As String
        Get
            Return Me._SortLabel
        End Get
        Set
            If Not String.Equals(Value, Me.SortLabel) Then
                Me._SortLabel = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> A <see cref="isr.Core.Constructs.InvokingBindingList"/> of <see cref="NomReading"/>. </summary>
    Public ReadOnly Property NomReadingsBindingList As isr.Core.Constructs.InvokingBindingList(Of NomReading)

    ''' <summary> Updates the readings described by nomReadings. </summary>
    ''' <remarks> David, 8/3/2020. </remarks>
    ''' <param name="nomReadings"> The nom readings. </param>
    Public Sub UpdateReadings(ByVal nomReadings As IEnumerable(Of NomReading))
        Me.NomReadingsBindingList.RaiseListChangedEvents = False
        Me.NomReadingsBindingList.Clear()
        For Each nomReading As NomReading In nomReadings
            Me.NomReadingsBindingList.Add(nomReading)
        Next
        Me.NomReadingsBindingList.RaiseListChangedEvents = True
        Me.NomReadingsBindingList.ResetBindings()
    End Sub

#End Region

#Region " MESSAGES "

    Private _ProgressMessage As String

    ''' <summary> Gets or sets a message describing the progress. </summary>
    ''' <value> A message describing the progress. </value>
    Public Property ProgressMessage As String
        Get
            Return Me._ProgressMessage
        End Get
        Set(value As String)
            If value <> Me.ProgressMessage Then
                Me._ProgressMessage = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class

''' <summary> Collection of <see cref="UutMonitor"/>'s keyed by the meter number. </summary>
''' <remarks> David, 6/12/2020. </remarks>
Public Class UutMonitorCollection
    Inherits ObjectModel.KeyedCollection(Of Integer, UutMonitor)
    Protected Overrides Function GetKeyForItem(item As UutMonitor) As Integer
        Return item.MultimeterNumber
    End Function
End Class

