Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Structure-Uut builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class StructureUutBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return StructureUutBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(StructureUutBuilder._TableName) Then
                StructureUutBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(StructureUutNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = StructureBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(StructureNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = UutBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(UutNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(StructureUutEntity.StructureAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(StructureUutEntity.UutAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As StructureUutBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As StructureUutBuilder
        If StructureUutBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                StructureUutBuilder._Instance = New StructureUutBuilder()
            End SyncLock
        End If
        Return StructureUutBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Structure Uut Nub based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("StructureUut")>
Public Class StructureUutNub
    Inherits OneToManyNub
    Implements IOneToMany

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToMany
        Return New StructureUutNub
    End Function

End Class

''' <summary>
''' The Structure-Uut Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class StructureUutEntity
    Inherits EntityBase(Of IOneToMany, StructureUutNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New StructureUutNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Structure-Uut interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New StructureUutNub, cache, store)
        Me.UsingNativeTracking = String.Equals(StructureUutBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToMany
        Return New StructureUutNub
    End Function

    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        StructureUutNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        StructureUutNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Structure-Uut interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        StructureUutNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="uutAutoId">       Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal uutAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As StructureUutNub = StructureUutEntity.FetchNubs(connection, structureAutoId, uutAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.StructureAutoId, Me.UutAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.StructureAutoId, Me.UutAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="uutAutoId">       Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal uutAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, structureAutoId, uutAutoId)
    End Function

    ''' <summary>
    ''' Attempts to retrieve an existing or insert a new <see cref="StructureUutEntity"/> from the
    ''' given data. Specifying new <paramref name="structureNumber"/> adds this Structure;
    ''' Specifying a negative
    ''' <paramref name="uutNumber"/> adds a new Uut with the next Uut number for
    ''' this Structure. Updates the <see cref="StructureUutEntity.StructureEntity"/> and
    ''' <see cref="StructureUutEntity.UutEntity"/>
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureNumber"> The Structure number. </param>
    ''' <param name="uutNumber">       The Uut number. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal structureNumber As Integer, ByVal uutNumber As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._StructureEntity = New StructureEntity With {.StructureNumber = structureNumber}
        Me._UutEntity = New UutEntity With {.UutNumber = uutNumber}
        If Me.StructureEntity.Obtain(connection) Then
            If 0 < uutNumber Then
                If Not Me.UutEntity.FetchUsingUniqueIndex(connection) Then
                    result = (False, $"Failed fetching existing {NameOf(Entities.UutEntity)} with {NameOf(Entities.UutEntity.UutNumber)} of {uutNumber}")
                End If
            Else
                Me._UutEntity = StructureUutEntity.FetchLastUut(connection, Me.StructureEntity.AutoId)
                uutNumber = If(Me.UutEntity.IsClean, Me.UutEntity.UutNumber + 1, 1)
                Me._UutEntity = New UutEntity() With {.UutNumber = uutNumber}
                If Not Me.UutEntity.Obtain(connection) Then
                    result = (False, $"Failed obtaining {NameOf(Entities.UutEntity)} with {NameOf(Entities.UutEntity.UutNumber)} of {uutNumber}")
                End If
            End If
        Else
            result = (False, $"Failed obtaining {NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.StructureNumber)} of {structureNumber}")
        End If
        If result.Success Then
            Me.StructureAutoId = Me.StructureEntity.AutoId
            Me.UutAutoId = Me.UutEntity.AutoId
            If Not Me.Obtain(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.StructureUutEntity)} for [{NameOf(Entities.StructureEntity.StructureNumber)}, {NameOf(Entities.UutEntity.UutNumber)}] of [{structureNumber},{uutNumber}]")
            End If
        End If
        Me.NotifyPropertyChanged(NameOf(StructureUutEntity.UutEntity))
        Me.NotifyPropertyChanged(NameOf(StructureUutEntity.StructureEntity))
        Return result
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If StructureUutEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="uutAutoId">       Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal uutAutoId As Integer) As Boolean
        Return connection.Delete(Of StructureUutNub)(New StructureUutNub With {.PrimaryId = structureAutoId, .SecondaryId = uutAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Structure-Uut entities. </summary>
    ''' <value> The Structure-Uut entities. </value>
    Public ReadOnly Property StructureUuts As IEnumerable(Of StructureUutEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of StructureUutEntity)
        Return If(usingNativeTracking, StructureUutEntity.Populate(connection.GetAll(Of IOneToMany)), StructureUutEntity.Populate(connection.GetAll(Of StructureUutNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._StructureUuts = StructureUutEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(StructureUutEntity.StructureUuts))
        Return If(Me.StructureUuts?.Any, Me.StructureUuts.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of StructureUutNub)) As IEnumerable(Of StructureUutEntity)
        Dim l As New List(Of StructureUutEntity)
        If nubs?.Any Then
            For Each nub As StructureUutNub In nubs
                l.Add(New StructureUutEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of StructureUutEntity)
        Dim l As New List(Of StructureUutEntity)
        If interfaces?.Any Then
            Dim nub As New StructureUutNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New StructureUutEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns up to Uut entities count. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StructureUutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureUutNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of StructureUutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureUutBuilder.TableName}] WHERE {NameOf(StructureUutNub.PrimaryId)} = @Id", New With {Key .Id = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return StructureUutEntity.Populate(connection.Query(Of StructureUutNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Uut. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> The total number of entities by Uut. </returns>
    Public Shared Function CountEntitiesByUut(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StructureUutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureUutNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Uuts in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Uuts in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByUut(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As IEnumerable(Of StructureUutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureUutBuilder.TableName}] WHERE {NameOf(StructureUutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return StructureUutEntity.Populate(connection.Query(Of StructureUutNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="uutAutoId">       Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StructureUutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureUutNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = structureAutoId})
        sqlBuilder.Where($"{NameOf(StructureUutNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="uutAutoId">       Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal uutAutoId As Integer) As IEnumerable(Of StructureUutNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{StructureUutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureUutNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = structureAutoId})
        sqlBuilder.Where($"{NameOf(StructureUutNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of StructureUutNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Structure Uut exists. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="uutAutoId">       Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal uutAutoId As Integer) As Boolean
        Return 1 = StructureUutEntity.CountEntities(connection, structureAutoId, uutAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Structure entity. </summary>
    ''' <value> The Structure entity. </value>
    Public ReadOnly Property StructureEntity As StructureEntity

    ''' <summary> Fetches Structure Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Structure Entity. </returns>
    Public Function FetchStructureEntity(ByVal connection As System.Data.IDbConnection) As StructureEntity
        Dim entity As New StructureEntity()
        entity.FetchUsingKey(connection, Me.StructureAutoId)
        Me._StructureEntity = entity
        Return entity
    End Function

    ''' <summary> Count Structures associated with the specified <paramref name="uutAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> The total number of Structures. </returns>
    Public Shared Function CountStructures(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{StructureUutBuilder.TableName}] WHERE {NameOf(StructureUutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Structures associated with the specified <paramref name="uutAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Structures in this collection.
    ''' </returns>
    Public Shared Function FetchStructures(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As IEnumerable(Of UutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureUutBuilder.TableName}] WHERE {NameOf(StructureUutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of UutEntity)
        For Each nub As StructureUutNub In connection.Query(Of StructureUutNub)(selector.RawSql, selector.Parameters)
            Dim entity As New StructureUutEntity(nub)
            l.Add(entity.FetchUutEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Structures associated with the specified <paramref name="uutAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteStructures(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{StructureUutBuilder.TableName}] WHERE {NameOf(StructureUutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = uutAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the Uut entity. </summary>
    ''' <value> The Uut entity. </value>
    Public ReadOnly Property UutEntity As UutEntity

    ''' <summary> Fetches a Uut Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Uut Entity. </returns>
    Public Function FetchUutEntity(ByVal connection As System.Data.IDbConnection) As UutEntity
        Dim entity As New UutEntity()
        entity.FetchUsingKey(connection, Me.UutAutoId)
        Me._UutEntity = entity
        Return entity
    End Function

    ''' <summary> Count uuts. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> The total number of uuts. </returns>
    Public Shared Function CountUuts(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{StructureUutBuilder.TableName}] WHERE {NameOf(StructureUutNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Uuts in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Uuts in this collection.
    ''' </returns>
    Public Shared Function FetchUuts(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of UutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureUutBuilder.TableName}] WHERE {NameOf(StructureUutNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of UutEntity)
        For Each nub As StructureUutNub In connection.Query(Of StructureUutNub)(selector.RawSql, selector.Parameters)
            Dim entity As New StructureUutEntity(nub)
            l.Add(entity.FetchUutEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Uut related to the specified Structure. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteUuts(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{StructureUutBuilder.TableName}] WHERE {NameOf(StructureUutNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = structureAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered uuts in this collection. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="structureAutoId">   Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered uuts in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedUuts(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal structureAutoId As Integer) As IEnumerable(Of UutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .Id = structureAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return UutEntity.Populate(connection.Query(Of UutNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered uuts in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered uuts in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedUuts(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of UutEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [StructureUut] on [StructureUut].SecondaryId = [Uut].AutoId where [StructureUut].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{UutBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{UutBuilder.TableName}] Inner Join [{StructureUutBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{StructureUutBuilder.TableName}].{NameOf(StructureUutNub.SecondaryId)} = [{UutBuilder.TableName}].{NameOf(UutNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{StructureUutBuilder.TableName}].{NameOf(StructureUutNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{UutBuilder.TableName}].{NameOf(UutNub.Amount)} ASC; ")
        Return StructureUutEntity.FetchOrderedUuts(connection, queryBuilder.ToString, structureAutoId)
    End Function

    ''' <summary> Fetches the last Uut in this collection. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="structureAutoId">   Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Uuts in this collection.
    ''' </returns>
    Public Shared Function FetchFirstUut(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal structureAutoId As Integer) As UutEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery, New With {Key .Id = structureAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As UutNub = connection.Query(Of UutNub)(template.RawSql, template.Parameters).FirstOrDefault
        Return If(nub Is Nothing, New UutEntity, New UutEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches the last Uut in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Uuts in this collection.
    ''' </returns>
    Public Shared Function FetchLastUut(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As UutEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [StructureUut] on [StructureUut].SecondaryId = [Uut].AutoId where [StructureUut].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{UutBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{UutBuilder.TableName}] Inner Join [{StructureUutBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{StructureUutBuilder.TableName}].{NameOf(StructureUutNub.SecondaryId)} = [{UutBuilder.TableName}].{NameOf(UutNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{StructureUutBuilder.TableName}].{NameOf(StructureUutNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{UutBuilder.TableName}].{NameOf(UutNub.Amount)} DESC; ")
        Return StructureUutEntity.FetchFirstUut(connection, queryBuilder.ToString, structureAutoId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(StructureUutEntity.StructureAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="StructureEntity"/>. </summary>
    ''' <value> Identifies the <see cref="StructureEntity"/>. </value>
    Public Property StructureAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(StructureUutEntity.UutAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Uut. </summary>
    ''' <value> Identifies the Uut. </value>
    Public Property UutAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
