
Imports Dapper

Imports isr.Core

Partial Public Class UutEntity

    ''' <summary> Gets or sets the <see cref="UutUniqueNutEntityCollection">uut Nuts</see>. </summary>
    ''' <value> The Uut Nuts entities. </value>
    Public ReadOnly Property Nuts As UutUniqueNutEntityCollection

    ''' <summary>
    ''' Fetches the <see cref="UutUniqueNutEntityCollection">Uut Nuts</see>. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of <see cref="Entities.NutEntity"/>'s in <see cref="Uutentity.Nuts"/>. </returns>
    Public Function FetchNuts(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Nuts = UutEntity.FetchNuts(connection, Me.AutoId)
        Return (Me.Nuts?.Count).GetValueOrDefault(0)
    End Function

    ''' <summary> Fetches the <see cref="UutUniqueNutEntityCollection">Uut Nuts</see>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> The <see cref="UutUniqueNutEntityCollection"/>. </returns>
    Public Shared Function FetchNuts(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As UutUniqueNutEntityCollection
        If Not ReadingBinEntity.IsEnumerated() Then ReadingBinEntity.TryFetchAll(connection)
        Dim nuts As New UutUniqueNutEntityCollection(uutAutoId)
        nuts.Populate(UutNutEntity.FetchOrderedNuts(connection, uutAutoId))
        Return nuts
    End Function

    Private _FocusedElementLabel As String

    ''' <summary> Gets or sets the focused element label. </summary>
    ''' <value> The focused element label. </value>
    Public Property FocusedElementLabel As String
        Get
            Return Me._FocusedElementLabel
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FocusedElementLabel) Then
                Me._FocusedElementLabel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _FocusedNutNomType As NomType

    Public Property FocusedNutNomType As NomType
        Get
            Return Me._FocusedNutNomType
        End Get
        Set(value As NomType)
            If Not Integer.Equals(value, Me.FocusedNutNomType) Then
                Me._FocusedNutNomType = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _FocusedNutReadingBin As ReadingBin

    Public Property FocusedNutReadingBin As ReadingBin
        Get
            Return Me._FocusedNutReadingBin
        End Get
        Set(value As ReadingBin)
            If Not Integer.Equals(value, Me.FocusedNutReadingBin) Then
                Me._FocusedNutReadingBin = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _FocusedNutAmount As Double

    Public Property FocusedNutAmount As Double
        Get
            Return Me._FocusedNutAmount
        End Get
        Set(value As Double)
            If Not Integer.Equals(value, Me.FocusedNutAmount) Then
                Me._FocusedNutAmount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Adds a nut reading. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="nut">           The <see cref="NutEntity"/>. </param>
    ''' <param name="readingValue">  The reading value. </param>
    ''' <param name="readingStatus"> The reading status. </param>
    ''' <returns> A NutEntity. </returns>
    Public Function AddNutReading(ByVal nut As NutEntity, ByVal readingValue As Double, ByVal readingStatus As Integer) As NutEntity
        ' at this time we have only a single nominal type.
        nut.NomReadings.NomReading.Reading = readingValue
        nut.NomReadings.NomReading.Status = readingStatus
        Me.FocusedElementLabel = nut.NomReadings.NomReading.ElementLabel
        Me.FocusedNutNomType = nut.NomReadings.NomReading.NomType
        Me.FocusedNutAmount = readingValue
        Me.NomReadings.Add(nut.NomReadings)
        Return nut
    End Function

    ''' <summary> Adds a nut bin. </summary>
    ''' <remarks> David, 7/2/2020. </remarks>
    ''' <param name="nut"> The <see cref="NutEntity"/>. </param>
    ''' <param name="bin"> The <see cref="ReadingBin"/>. </param>
    ''' <returns> A NutEntity. </returns>
    Public Function AddNutBin(ByVal nut As NutEntity, ByVal bin As ReadingBin) As NutEntity
        nut.NomReadings.NomReading.BinNumber = bin
        Me.Nuts.UpaddReadingBin(nut, ReadingBinEntity.EntityLookupDictionary(bin))
        Me.FocusedElementLabel = nut.NomReadings.NomReading.ElementLabel
        Me.FocusedNutNomType = nut.NomReadings.NomReading.NomType
        Me.FocusedNutReadingBin = bin
        Return nut
    End Function

    ''' <summary> Stores nut reading and bin. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    <CLSCompliant(False)>
    Public Sub StoreNutReadingsAndBins(ByVal connection As TransactedConnection)
        Dim readingBin As ReadingBinEntity
        For Each nut As NutEntity In Me.Nuts
            nut.NomReadings.Upsert(connection)
            readingBin = Me.Nuts.NutIdentityReadingBinEntityDictionary(nut.AutoId)
            UutEntity.StoreNutReadingAndBin(connection, nut, readingBin.Id)
        Next
    End Sub

    ''' <summary> Stores nut reading and bin. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Sub StoreNutReadingsAndBins(ByVal connection As System.Data.IDbConnection)
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        Me.StoreNutReadingsAndBins(New TransactedConnection(connection, transaction))
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            Me.StoreNutReadingsAndBins(transactedConnection)
        End If
    End Sub

    ''' <summary> Stores nut reading and bin. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nut">        The <see cref="NutEntity"/>. </param>
    ''' <param name="binId">      Identifier for the bin. </param>
    ''' <returns> A NutReadingBinEntity. </returns>
    <CLSCompliant(False)>
    Public Shared Function StoreNutReadingAndBin(ByVal connection As TransactedConnection, ByVal nut As NutEntity, ByVal binId As Integer) As NutReadingBinEntity
        nut.NomReadings.Upsert(connection)
        Dim nutReadingBin As New NutReadingBinEntity With {.NutAutoId = nut.AutoId, .ReadingBinId = binId}
        Dim r As (Success As Boolean, Details As String) = nutReadingBin.TryObtain(connection)
        If Not r.Success Then Throw New OperationFailedException(r.Details)
        Return nutReadingBin
    End Function

    ''' <summary> Stores nut reading and bin. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nut">        The <see cref="NutEntity"/>. </param>
    ''' <param name="binId">      Identifier for the bin. </param>
    ''' <returns> A NutReadingBinEntity. </returns>
    Public Shared Function StoreNutReadingAndBin(ByVal connection As System.Data.IDbConnection, ByVal nut As NutEntity, ByVal binId As Integer) As NutReadingBinEntity
        Dim result As NutReadingBinEntity
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        result = UutEntity.StoreNutReadingAndBin(New TransactedConnection(connection, transaction), nut, binId)
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            result = UutEntity.StoreNutReadingAndBin(transactedConnection, nut, binId)
        End If
        Return result
    End Function

    ''' <summary> Adds a nut reading. </summary>
    ''' <remarks> David, 7/2/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nut">        The <see cref="NutEntity"/>. </param>
    ''' <param name="amount">     The amount. </param>
    ''' <param name="upsert">     True to upsert. </param>
    ''' <returns> A NutEntity. </returns>
    Public Function AddNutReading(ByVal connection As System.Data.IDbConnection, ByVal nut As NutEntity,
                                  ByVal amount As Double, ByVal upsert As Boolean) As NutEntity
        ' at this time we have only a single nominal type.
        nut.NomReadings.NomReading.Reading = amount
        Me.FocusedElementLabel = nut.NomReadings.NomReading.ElementLabel
        Me.FocusedNutNomType = nut.NomReadings.NomReading.NomType
        Me.FocusedNutAmount = amount
        If upsert Then Dim unused As Boolean = nut.NomReadings.Upsert(connection)
        Me.NomReadings.Add(nut.NomReadings)
        Return nut
    End Function

    ''' <summary> Adds a nut bin. </summary>
    ''' <remarks> David, 7/2/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nut">        The <see cref="NutEntity"/>. </param>
    ''' <param name="bin">        The <see cref="ReadingBin"/>. </param>
    ''' <param name="upsert">     True to upsert. </param>
    ''' <returns> A NutReadingBinEntity. </returns>
    Public Function AddNutBin(ByVal connection As System.Data.IDbConnection, ByVal nut As NutEntity,
                              ByVal bin As ReadingBin, ByVal upsert As Boolean) As NutReadingBinEntity
        nut.NomReadings.NomReading.BinNumber = bin
        Me.Nuts.UpaddReadingBin(nut, ReadingBinEntity.EntityLookupDictionary(bin))
        Me.FocusedElementLabel = nut.NomReadings.NomReading.ElementLabel
        Me.FocusedNutNomType = nut.NomReadings.NomReading.NomType
        Me.FocusedNutReadingBin = bin
        If upsert Then nut.NomReadings.Upsert(connection)
        Return UutEntity.StoreNutBin(connection, nut.AutoId, bin)
    End Function

    ''' <summary> Stores nut bin. </summary>
    ''' <remarks> David, 7/2/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="nutId">      Identifier for the nut. </param>
    ''' <param name="binId">      Identifier for the bin. </param>
    ''' <returns> A NutReadingBinEntity. </returns>
    Public Shared Function StoreNutBin(ByVal connection As System.Data.IDbConnection, ByVal nutId As Integer, ByVal binId As Integer) As NutReadingBinEntity
        Dim nutReadingBin As New NutReadingBinEntity With {.NutAutoId = nutId, .ReadingBinId = binId}
        Dim r As (Success As Boolean, Details As String) = nutReadingBin.TryObtain(connection)
        If Not r.Success Then Throw New OperationFailedException(r.Details)
        Return nutReadingBin
    End Function

End Class

