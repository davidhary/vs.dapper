Imports System.Collections.ObjectModel
Imports System.ComponentModel

Imports Dapper

Partial Public Class UutEntity

    ''' <summary> Fetches nom readings. </summary>
    ''' <remarks> David, 7/2/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The nom readings. </returns>
    Public Function FetchNomReadings(ByVal connection As System.Data.IDbConnection) As Integer
        Return Me.Nuts.FetchNomReadings(connection)
    End Function

    ''' <summary> Populates the <see cref="Entities.UutEntity"/> <see cref="UutEntity.NomReadings"/> for all <see cref="Entities.NutEntity"/>'s. </summary>
    ''' <remarks> David, 6/11/2020. </remarks>
    ''' <returns> The number of <see cref="NomReading"/>'s in <see cref="Uutentity.NomReadings"/>. </returns>
    Public Function PopulateNomReadings() As Integer
        Me.NomReadings.Clear()
        For Each nut As NutEntity In Me.Nuts
            Me.NomReadings.Add(nut.NomReadings)
        Next
        Return Me.NomReadings.Count
    End Function

    ''' <summary> Initializes the nom readings. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="elements">    The elements. </param>
    Public Sub InitializeNomReadings(ByVal elements As ElementEntityCollection)
        Me._NomReadings = New NutNomReadingCollection(New MultimeterEntity() {Me.Traits.UutTrait.Multimeter}, elements)
        Me.Nuts.InitializeNomReadings(Me, elements)
    End Sub

    ''' <summary> Gets the nominal readings. </summary>
    ''' <value> The nominal readings. </value>
    Public ReadOnly Property NomReadings As NutNomReadingCollection

End Class

''' <summary>
''' Keyed collection of <see cref="NutUniqueNomReadingEntityCollection"/> keyed by
''' <see cref="NutMultimeterNomTypeSelector"/> of <see cref="Entities.NutEntity"/> and <see cref="Entities.MultimeterEntity"/> and
''' <see cref="Entities.NomTypeEntity"/>.
''' </summary>
''' <remarks> David, 6/16/2020. </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class NutNomReadingCollection
    Inherits KeyedCollection(Of NutMultimeterNomTypeSelector, NutUniqueNomReadingEntityCollection)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="multimeters"> The multimeters. </param>
    ''' <param name="elements">    The elements. </param>
    Public Sub New(ByVal multimeters As IEnumerable(Of MultimeterEntity), ByVal elements As IEnumerable(Of ElementEntity))
        MyBase.New()
        Me.MeterBindingListDictionary = New Dictionary(Of Integer, isr.Core.Constructs.InvokingBindingList(Of NomReading))
        Me.MeterElementNomTypeBindingListDictionary = New Dictionary(Of MeterElementNomTypeSelector, isr.Core.Constructs.InvokingBindingList(Of NomReading))
        Dim selector As MeterElementNomTypeSelector
        Me.BindingList = New isr.Core.Constructs.InvokingBindingList(Of NomReading)
        Dim meterBindingList As isr.Core.Constructs.InvokingBindingList(Of NomReading)
        Dim meterElementNomTypeBindingList As isr.Core.Constructs.InvokingBindingList(Of NomReading)
        For Each multimeter As MultimeterEntity In multimeters
            meterBindingList = New isr.Core.Constructs.InvokingBindingList(Of NomReading)
            Me.MeterBindingListDictionary.Add(multimeter.Id, meterBindingList)
            For Each element As ElementEntity In elements
                For Each nomTypeEntity As NomTypeEntity In element.NomTypes
                    meterElementNomTypeBindingList = New isr.Core.Constructs.InvokingBindingList(Of NomReading)
                    selector = New MeterElementNomTypeSelector(multimeter.Id, element.AutoId, nomTypeEntity.Id)
                    Me.MeterElementNomTypeBindingListDictionary.Add(selector, meterElementNomTypeBindingList)
                Next
            Next
        Next
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 6/22/2020. </remarks>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As NutUniqueNomReadingEntityCollection) As NutMultimeterNomTypeSelector
        Return New NutMultimeterNomTypeSelector(item)
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As NutUniqueNomReadingEntityCollection)
        MyBase.Add(item)
        Dim selector As New MeterElementNomTypeSelector(item.NomReading)
        Me.MeterElementNomTypeBindingListDictionary(selector).Add(item.NomReading)
        Me.MeterBindingListDictionary(item.NomReading.MultimeterId).Add(item.NomReading)
        Me.BindingList.Add(item.NomReading)
    End Sub

    ''' <summary> Removes the first. </summary>
    ''' <remarks> David, 7/3/2020. </remarks>
    Public Sub RemoveFirst()
        If Me.Any Then
            Dim selector As New MeterElementNomTypeSelector(Me._BindingList.First)
            Me.MeterElementNomTypeBindingListDictionary(selector)?.RemoveAt(0)
            Me.MeterBindingListDictionary(selector.MultimeterId)?.RemoveAt(0)
            Me.RemoveAt(0)
            Me.BindingList.RemoveAt(0)
        End If
    End Sub

    ''' <summary> Toggles binding list change events. </summary>
    ''' <remarks> David, 8/11/2020. </remarks>
    ''' <param name="enabled"> True to enable, false to disable. </param>
    Public Sub ToggleBindingListChangeEvents(ByVal enabled As Boolean)
        Me.BindingList.RaiseListChangedEvents = enabled
        For Each bl As isr.Core.Constructs.InvokingBindingList(Of NomReading) In Me.MeterBindingListDictionary.Values
            bl.RaiseListChangedEvents = enabled
            If enabled Then bl.ResetBindings()
        Next
        For Each bl As isr.Core.Constructs.InvokingBindingList(Of NomReading) In Me.MeterElementNomTypeBindingListDictionary.Values
            bl.RaiseListChangedEvents = enabled
            If enabled Then bl.ResetBindings()
        Next
    End Sub

    ''' <summary> Adds a range. </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="items"> <see cref="NutNomReadingCollection"/> of items to append to this. </param>
    Public Overridable Sub AddRange(ByVal items As NutNomReadingCollection)
        Try
            Me.ToggleBindingListChangeEvents(False)
            For Each item As NutUniqueNomReadingEntityCollection In items
                Me.Add(item)
            Next
        Catch
            Throw
        Finally
            Me.ToggleBindingListChangeEvents(True)
        End Try
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
        Me.BindingList.Clear()
        For Each bindingList As BindingList(Of NomReading) In Me._MeterBindingListDictionary.Values
            bindingList.Clear()
        Next
        For Each bindingList As BindingList(Of NomReading) In Me._MeterElementNomTypeBindingListDictionary.Values
            bindingList.Clear()
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of meter identity binding lists. </summary>
    ''' <value> A dictionary of meter identity binding lists. </value>
    Public ReadOnly Property MeterBindingListDictionary As IDictionary(Of Integer, isr.Core.Constructs.InvokingBindingList(Of NomReading))

    ''' <summary> Dictionary of <see cref="BindingList"/> of <see cref="NomReading"/>. </summary>
    Public ReadOnly Property MeterElementNomTypeBindingListDictionary As IDictionary(Of MeterElementNomTypeSelector, isr.Core.Constructs.InvokingBindingList(Of NomReading))

    ''' <summary> Selects <see cref="BindingList"/> of <see cref="NomReading"/>. </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifies the <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <returns> An <see cref="isr.Core.Constructs.InvokingBindingList"/>. </returns>
    Public Function SelectBindingList(ByVal multimeterId As Integer, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer) As isr.Core.Constructs.InvokingBindingList(Of NomReading)
        Return Me.MeterElementNomTypeBindingListDictionary(New MeterElementNomTypeSelector(multimeterId, elementAutoId, nomTypeId))
    End Function

    ''' <summary> A <see cref="BindingList"/> of <see cref="NomReading"/>. </summary>
    Public ReadOnly Property BindingList As isr.Core.Constructs.InvokingBindingList(Of NomReading)

    ''' <summary> Updates or inserts entities using the given connection. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    <CLSCompliant(False)>
    Public Sub Upsert(ByVal connection As TransactedConnection)
        For Each nomReadings As NutUniqueNomReadingEntityCollection In Me
            nomReadings.Upsert(connection)
        Next
    End Sub

    ''' <summary> Updates or inserts entities using the given connection. </summary>
    ''' <remarks> David, 7/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Sub Upsert(ByVal connection As System.Data.IDbConnection)
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        Me.Upsert(New TransactedConnection(connection, transaction))
                        transaction.Commit()
                    Catch
                        transaction?.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            Me.Upsert(transactedConnection)
        End If
    End Sub

End Class

