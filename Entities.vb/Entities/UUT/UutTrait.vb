''' <summary> The Uut Trait class holing the Uut trait values. </summary>
''' <remarks> David, 2020-05-29. </remarks>
Public Class UutTrait
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSestter"> The getter setter. </param>
    Public Sub New(ByVal getterSestter As isr.Core.Constructs.IGetterSetter(Of Integer))
        MyBase.New()
        Me.GetterSetter = getterSestter
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter(Of Integer)

    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As Integer?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.GetterSetter.Setter(value.Value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

#End Region

#Region " TRAITS "

    ''' <summary> Gets or sets the identifier of the multimeter. </summary>
    ''' <value> Identifies the multimeter. </value>
    Public Property MultimeterId As Integer
        Get
            Return CType(Me.TraitValue(), Integer)
        End Get
        Set(value As Integer)
            Me.TraitValue() = (value)
        End Set
    End Property

#End Region

#Region " DERIVED TRAITS "

    Private _Multimeter As MultimeterEntity

    ''' <summary> Gets the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <value> The <see cref="Entities.MultimeterEntity"/>. </value>
    Public ReadOnly Property Multimeter As MultimeterEntity
        Get
            If Not (Me._Multimeter?.IsClean()).GetValueOrDefault(False) Then
                Me._Multimeter = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
            End If
            Return Me._Multimeter
        End Get
    End Property

    Private _MultimeterModel As MultimeterModelEntity

    ''' <summary> Gets the Meter Model entity. </summary>
    ''' <value> The Meter Model entity. </value>
    Public ReadOnly Property MultimeterModel As MultimeterModelEntity
        Get
            If Not (Me._MultimeterModel?.IsClean()).GetValueOrDefault(False) Then
                Me._MultimeterModel = MultimeterModelEntity.EntityLookupDictionary(Me.Multimeter.MultimeterModelId)
            End If
            Return Me._MultimeterModel
        End Get
    End Property

#End Region

End Class
