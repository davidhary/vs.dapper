Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Lot-Uut builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class LotUutBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return LotUutBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(LotUutBuilder._TableName) Then
                LotUutBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(LotUutNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = LotBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(LotNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = UutBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(UutNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(LotUutEntity.LotAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(LotUutEntity.UutAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As LotUutBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As LotUutBuilder
        If LotUutBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                LotUutBuilder._Instance = New LotUutBuilder()
            End SyncLock
        End If
        Return LotUutBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Lot Uut Nub based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("LotUut")>
Public Class LotUutNub
    Inherits OneToManyNub
    Implements IOneToMany

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToMany
        Return New LotUutNub
    End Function

End Class

''' <summary>
''' The Lot-Uut Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class LotUutEntity
    Inherits EntityBase(Of IOneToMany, LotUutNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New LotUutNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Lot-Uut interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New LotUutNub, cache, store)
        Me.UsingNativeTracking = String.Equals(LotUutBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToMany
        Return New LotUutNub
    End Function

    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        LotUutNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        LotUutNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Lot-Uut interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        LotUutNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="uutAutoId">      Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal uutAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As LotUutNub = LotUutEntity.FetchNubs(connection, lotAutoId, uutAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.LotAutoId, Me.UutAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.LotAutoId, Me.UutAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="uutAutoId">      Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal uutAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, lotAutoId, uutAutoId)
    End Function

    ''' <summary> Gets the slim locker for managing thread save addition of DUT tests. </summary>
    ''' <value> The locker. </value>
    Protected ReadOnly Property SlimLocker As New Threading.ReaderWriterLockSlim()

    Public Function TryObtainUut(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal uutNumber As Integer,
                                 ByVal uutTypeId As Integer) As (Success As Boolean, Details As String)
        Me.SlimLocker.EnterReadLock()
        Try
            Return Me.TryObtainUutThis(connection, lotAutoId, uutNumber, uutTypeId)
        Catch
            Throw
        Finally
            Me.SlimLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary>
    ''' Attempts to retrieve an existing or insert a new <see cref="Entities.UutEntity"/> associated with an
    ''' existing <see cref="Entities.LotEntity"/> with <paramref name="lotAutoId"/>. Specifying a negative
    ''' <paramref name="uutNumber"/> adds a new Uut with the next Uut number for this Lot. Thereafter
    ''' a <see cref="LotUutEntity"/> is updated or inserted.
    ''' </summary>
    ''' <remarks>
    ''' Using this method assumes the UUT has a non-unique <see cref="UutEntity.UutNumber"/>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="uutNumber">  The UUT Number. </param>
    ''' <param name="uutTypeId">  Identifies the <see cref="Entities.UutEntity"/> type. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Private Function TryObtainUutThis(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal uutNumber As Integer,
                                      ByVal uutTypeId As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If 0 < uutNumber Then
            Me._UutEntity = LotUutEntity.FetchUut(connection, lotAutoId, uutNumber)
            If Not Me.UutEntity.IsClean Then
                result = (False, $"Failed fetching existing {NameOf(Entities.UutEntity)} with {NameOf(Entities.UutEntity.UutNumber)} of {uutNumber}")
            End If
        Else
            Me._UutEntity = LotUutEntity.FetchLastUut(connection, lotAutoId)
            uutNumber = If(Me.UutEntity.IsClean, Me.UutEntity.UutNumber + 1, 1)
            Me._UutEntity = New UutEntity() With {.UutNumber = uutNumber, .UutTypeId = uutTypeId}
            If Not Me.UutEntity.Insert(connection) Then
                result = (False, $"Failed inserting {NameOf(Entities.UutEntity)} with {NameOf(Entities.UutEntity.UutNumber)} of {uutNumber}")
            End If
        End If
        If result.Success Then
            Me.LotAutoId = lotAutoId
            Me.UutAutoId = Me.UutEntity.AutoId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(LotUutEntity.UutEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.LotUutEntity)} for [{NameOf(Entities.LotEntity.AutoId)}, {NameOf(Entities.UutEntity.UutNumber)}] of [{lotAutoId},{uutNumber}]")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Attempts to retrieve an existing or insert a new <see cref="LotUutEntity"/> from the given
    ''' data. Specifying a negative <paramref name="uutNumber"/> adds a new Uut with the next Uut
    ''' number for this Lot. Updates the <see cref="LotUutEntity.LotEntity"/> and
    ''' <see cref="LotUutEntity.UutEntity"/>
    ''' </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="uutNumber">  UUT Number. </param>
    ''' <param name="uutTypeId">  Identifies the <see cref="Entities.UutEntity"/> type. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal uutNumber As Integer,
                              ByVal uutTypeId As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.LotEntity Is Nothing OrElse Me.LotEntity.AutoId <> lotAutoId Then
            Me._LotEntity = New LotEntity With {.AutoId = lotAutoId}
            If Not Me.LotEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.AutoId)} of {lotAutoId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainUut(connection, lotAutoId, uutNumber, uutTypeId)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(LotUutEntity.LotEntity))
        Return result
    End Function

    ''' <summary>
    ''' Attempts to retrieve an existing or insert a new <see cref="LotUutEntity"/> from the given
    ''' data. Specifying new <paramref name="lotLabel"/> adds this Lot;
    ''' Specifying a negative
    ''' <paramref name="uutNumber"/> adds a new Uut with the next Uut number for
    ''' this Lot. Updates the <see cref="LotUutEntity.LotEntity"/> and
    ''' <see cref="LotUutEntity.UutEntity"/>
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotLabel">   The lot label. </param>
    ''' <param name="uutNumber">  The Uut number. </param>
    ''' <param name="uutTypeId">  Identifies the <see cref="Entities.UutEntity"/> type. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal lotLabel As String, ByVal uutNumber As Integer,
                              ByVal uutTypeId As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.LotEntity Is Nothing OrElse Me.LotEntity.LotNumber <> lotLabel Then
            Me._LotEntity = New LotEntity With {.Label = lotLabel}
            If Not Me.LotEntity.Obtain(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.Label)} of {lotLabel}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtain(connection, Me.LotEntity.AutoId, uutNumber, uutTypeId)
        End If
        Return result
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If LotUutEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId">     Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal uutAutoId As Integer) As Boolean
        Return connection.Delete(Of LotUutNub)(New LotUutNub With {.PrimaryId = lotAutoId, .SecondaryId = uutAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Lot-Uut entities. </summary>
    ''' <value> The Lot-Uut entities. </value>
    Public ReadOnly Property LotUuts As IEnumerable(Of LotUutEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of LotUutEntity)
        Return If(usingNativeTracking, LotUutEntity.Populate(connection.GetAll(Of IOneToMany)), LotUutEntity.Populate(connection.GetAll(Of LotUutNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._LotUuts = LotUutEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(LotUutEntity.LotUuts))
        Return If(Me.LotUuts?.Any, Me.LotUuts.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of LotUutNub)) As IEnumerable(Of LotUutEntity)
        Dim l As New List(Of LotUutEntity)
        If nubs?.Any Then
            For Each nub As LotUutNub In nubs
                l.Add(New LotUutEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of LotUutEntity)
        Dim l As New List(Of LotUutEntity)
        If interfaces?.Any Then
            Dim nub As New LotUutNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New LotUutEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to Uut entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotUutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotUutNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of LotUutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotUutBuilder.TableName}] WHERE {NameOf(LotUutNub.PrimaryId)} = @Id", New With {Key .Id = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotUutEntity.Populate(connection.Query(Of LotUutNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Uut. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> The total number of entities by Uut. </returns>
    Public Shared Function CountEntitiesByUut(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotUutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotUutNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Uuts in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Uuts in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByUut(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As IEnumerable(Of LotUutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotUutBuilder.TableName}] WHERE {NameOf(LotUutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotUutEntity.Populate(connection.Query(Of LotUutNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="uutAutoId">       Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotUutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotUutNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotUutNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="uutAutoId">       Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal uutAutoId As Integer) As IEnumerable(Of LotUutNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotUutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotUutNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotUutNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotUutNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Lot Uut exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="uutAutoId">       Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal uutAutoId As Integer) As Boolean
        Return 1 = LotUutEntity.CountEntities(connection, lotAutoId, uutAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Lot entity. </summary>
    ''' <value> The Lot entity. </value>
    Public ReadOnly Property LotEntity As LotEntity

    ''' <summary> Fetches Lot Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Lot Entity. </returns>
    Public Function FetchLotEntity(ByVal connection As System.Data.IDbConnection) As LotEntity
        Dim entity As New LotEntity()
        entity.FetchUsingKey(connection, Me.LotAutoId)
        Me._LotEntity = entity
        Return entity
    End Function

    ''' <summary> Count Lots associated with the specified <paramref name="uutAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> The total number of Lots. </returns>
    Public Shared Function CountLots(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{LotUutBuilder.TableName}] WHERE {NameOf(LotUutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Lots associated with the specified <paramref name="uutAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Lots in this collection.
    ''' </returns>
    Public Shared Function FetchLots(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As IEnumerable(Of UutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotUutBuilder.TableName}] WHERE {NameOf(LotUutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of UutEntity)
        For Each nub As LotUutNub In connection.Query(Of LotUutNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotUutEntity(nub)
            l.Add(entity.FetchUutEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Lots associated with the specified <paramref name="uutAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteLots(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotUutBuilder.TableName}] WHERE {NameOf(LotUutNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = uutAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the Uut entity. </summary>
    ''' <value> The Uut entity. </value>
    Public ReadOnly Property UutEntity As UutEntity

    ''' <summary> Fetches a Uut Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Uut Entity. </returns>
    Public Function FetchUutEntity(ByVal connection As System.Data.IDbConnection) As UutEntity
        Dim entity As New UutEntity()
        entity.FetchUsingKey(connection, Me.UutAutoId)
        Me._UutEntity = entity
        Return entity
    End Function

    Public Shared Function CountUuts(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{LotUutBuilder.TableName}] WHERE {NameOf(LotUutNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Uuts in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Uuts in this collection.
    ''' </returns>
    Public Shared Function FetchUuts(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of UutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotUutBuilder.TableName}] WHERE {NameOf(LotUutNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of UutEntity)
        For Each nub As LotUutNub In connection.Query(Of LotUutNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotUutEntity(nub)
            l.Add(entity.FetchUutEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Uut related to the specified Lot. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteUuts(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotUutBuilder.TableName}] WHERE {NameOf(LotUutNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered uuts in this collection. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered uuts in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedUuts(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer) As IEnumerable(Of UutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .Id = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return UutEntity.Populate(connection.Query(Of UutNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered uuts in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered uuts in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedUuts(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of UutEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotUut] on [LotUut].SecondaryId = [Uut].AutoId where [LotUut].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{UutBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{UutBuilder.TableName}] Inner Join [{LotUutBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotUutBuilder.TableName}].{NameOf(LotUutNub.SecondaryId)} = [{UutBuilder.TableName}].{NameOf(UutNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{LotUutBuilder.TableName}].{NameOf(LotUutNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{UutBuilder.TableName}].{NameOf(UutNub.Amount)} ASC; ")
        Return LotUutEntity.FetchOrderedUuts(connection, queryBuilder.ToString, lotAutoId)
    End Function

    ''' <summary>
    ''' Fetches the first Uut from the ordered list; this would be the last ordinal number entity if
    ''' using descending order.
    ''' </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Uuts in this collection.
    ''' </returns>
    Public Shared Function FetchFirstUut(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer) As UutEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery, New With {Key .Id = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As UutNub = connection.Query(Of UutNub)(template.RawSql, template.Parameters).FirstOrDefault
        Return If(nub Is Nothing, New UutEntity, New UutEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches the last Uut in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Uuts in this collection.
    ''' </returns>
    Public Shared Function FetchLastUut(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As UutEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotUut] on [LotUut].SecondaryId = [Uut].AutoId where [LotUut].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{UutBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{UutBuilder.TableName}] Inner Join [{LotUutBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotUutBuilder.TableName}].{NameOf(LotUutNub.SecondaryId)} = [{UutBuilder.TableName}].{NameOf(UutNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{LotUutBuilder.TableName}].{NameOf(LotUutNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{UutBuilder.TableName}].{NameOf(UutNub.Amount)} DESC; ")
        Return LotUutEntity.FetchFirstUut(connection, queryBuilder.ToString, lotAutoId)
    End Function

    ''' <summary> Fetches an Uut. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="selectQuery">  The select query. </param>
    ''' <param name="lotAutoId">    Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> The Uut. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchUut(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer, ByVal uutNumber As Integer) As UutEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {.Id = lotAutoId, .UutNumber = uutNumber})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As UutNub = connection.Query(Of UutNub)(template.RawSql, template.Parameters).SingleOrDefault
        Return If(nub Is Nothing, New UutEntity, New UutEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches an Uut. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="uutNumber">  The UUT Number. </param>
    ''' <returns> The Uut. </returns>
    Public Shared Function FetchUut(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal uutNumber As Integer) As UutEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotUut] on [LotUut].SecondaryId = [Uut].AutoId where [LotUut].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{UutBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{UutBuilder.TableName}] Inner Join [{LotUutBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotUutBuilder.TableName}].{NameOf(LotUutNub.SecondaryId)} = [{UutBuilder.TableName}].{NameOf(UutNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE ([{LotUutBuilder.TableName}].{NameOf(LotUutNub.PrimaryId)} = @Id AND [{UutBuilder.TableName}].{NameOf(UutNub.Amount)} = @UutNumber); ")
        Return LotUutEntity.FetchUut(connection, queryBuilder.ToString, lotAutoId, uutNumber)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotUutEntity.LotAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.LotEntity"/> </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/> </value>
    Public Property LotAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotUutEntity.UutAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Uut. </summary>
    ''' <value> Identifies the Uut. </value>
    Public Property UutAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
