Imports System.ComponentModel

Imports Dapper

Imports isr.Core

Partial Public Class UutEntity

    ''' <summary>
    ''' Enumerate the sort identities for all the binned Nuts measured for the UUT.
    ''' </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="binnings"> The binning entities. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process select sort identities in this
    ''' collection.
    ''' </returns>
    Public Function SelectUutSortIdentities(ByVal binnings As ProductUniqueBinningEntityCollection) As IEnumerable(Of Integer)
        Dim result As New List(Of Integer)
        For Each elementBin As KeyValuePair(Of NutUniqueKey, ReadingBinEntity) In Me.Nuts.NutUniqueKeyReadingBinEntityDictionary
            Dim sortId As Integer? = binnings.Getter(elementBin.Key.ElementId, elementBin.Key.NomTypeId, elementBin.Value.Id)
            If sortId.HasValue Then result.Add(sortId.Value)
        Next
        Return result
    End Function

    ''' <summary> Executes the sort operation. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <param name="product"> The product. </param>
    ''' <returns> A ProductSortEntity. </returns>
    Public Function DoSort(ByVal product As ProductEntity) As ProductSortEntity
        Return Me.DoSort(product.ProductBinnings, product.ProductSorts)
    End Function

    ''' <summary> Executes the sort operation. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="binnings"> The binning entities. </param>
    ''' <param name="sorts">    The sorts. </param>
    ''' <returns> A ProductSortEntity. </returns>
    Public Function DoSort(ByVal binnings As ProductUniqueBinningEntityCollection,
                           ByVal sorts As ProductUniqueProductSortEntityCollection) As ProductSortEntity
        Dim sortedFailedSortEntityDix As New SortedDictionary(Of Integer, ProductSortEntity)
        Dim sortIdentites As IEnumerable(Of Integer) = Me.SelectUutSortIdentities(binnings)
        Dim candidateEntity As ProductSortEntity = New ProductSortEntity
        Dim candidateBucketBin As Integer
        For Each sortId As Integer In sortIdentites
            candidateEntity = sorts(sortId)
            candidateBucketBin = candidateEntity.Traits.ProductSortTrait.BucketBin.Value
            If candidateBucketBin <> BucketBin.Good Then
                If Not sortedFailedSortEntityDix.ContainsKey(candidateEntity.SortOrder) Then
                    sortedFailedSortEntityDix.Add(candidateEntity.SortOrder, candidateEntity)
                End If
            End If
        Next
        ' if any sorts failed, the first item is the sort entity; otherwise, the candidate already is the good sort entity.
        If sortedFailedSortEntityDix.Any Then candidateEntity = sortedFailedSortEntityDix.First.Value
        Return Me.ApplyProductSortEntity(candidateEntity)
    End Function

    ''' <summary> Applies the product sort entity described by candidateEntity. </summary>
    ''' <remarks> David, 7/8/2020. </remarks>
    ''' <param name="candidateEntity"> The candidate entity. </param>
    ''' <returns> A ProductSortEntity. </returns>
    Private Function ApplyProductSortEntity(ByVal candidateEntity As ProductSortEntity) As ProductSortEntity
        If (candidateEntity?.IsClean).GetValueOrDefault(False) Then
            Dim candidateBucketBin As Integer = candidateEntity.Traits.ProductSortTrait.BucketBin.Value
            Me._ProductSortEntity = candidateEntity
            Me.BucketBin = BucketBinEntity.ToBucketBin(candidateBucketBin)
            Me.UutState = If(BucketBinEntity.IsPresent(Me.BucketBin), UutState.Present, UutState.Absent)
        Else
            Me.UutState = If((Me.Nuts?.Any).GetValueOrDefault(False), Dapper.Entities.UutState.Ready, Dapper.Entities.UutState.Undefined)
            Me.BucketBin = BucketBin.Unknown
            Me._ProductSortEntity = Nothing
        End If
        Me.SyncNotifyPropertyChanged(NameOf(UutEntity.ProductSortEntity))
        Return candidateEntity
    End Function

    Private _ProductSortEntity As ProductSortEntity

    ''' <summary> Gets or sets the product sort entity. </summary>
    ''' <value> The product sort entity. </value>
    Public Property ProductSortEntity As ProductSortEntity
        Get
            Return Me._ProductSortEntity
        End Get
        Set(value As ProductSortEntity)
            If Not ProductSortEntity.Equals(value, Me.ProductSortEntity) Then
                Me.ApplyProductSortEntity(value)
            End If
        End Set
    End Property

    Private _BucketBin As BucketBin

    ''' <summary> Gets the bucket bin. </summary>
    ''' <value> The bucket bin. </value>
    Public Property BucketBin As BucketBin
        Get
            Return Me._BucketBin
        End Get
        Set(value As BucketBin)
            If value <> Me.BucketBin Then
                Me._BucketBin = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _UutState As UutState

    ''' <summary> Gets or sets the state of the uut. </summary>
    ''' <value> The uut state. </value>
    Public Property UutState As UutState
        Get
            Return Me._UutState
        End Get
        Set(value As UutState)
            If Not UutState.Equals(value, Me.UutState) Then
                Me._UutState = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Stores the sort of a sorted and <see cref="entities.UutState.present"/> UUT.
    ''' </summary>
    ''' <remarks> David, 6/14/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An <see cref="Dapper.Entities.UutProductSortEntity"/>. </returns>
    <CLSCompliant(False)>
    Public Function StoreUutSort(ByVal connection As TransactedConnection) As Dapper.Entities.UutProductSortEntity
        Dim uutSort As New Dapper.Entities.UutProductSortEntity
        If Me.UutState = Dapper.Entities.UutState.Present Then
            ' move uut to the complete state only if it is present; which means the uut must be sorted first.
            uutSort = New Dapper.Entities.UutProductSortEntity() With {.UutAutoId = Me.AutoId, .ProductSortAutoId = Me.ProductSortEntity.AutoId}
            uutSort.Upsert(connection)
            Dim r As (Success As Boolean, Details As String) = uutSort.ValidateStoredEntity($"{NameOf(Dapper.Entities.UutProductSortEntity)}")
            If Not r.Success Then Throw New OperationFailedException(r.Details)
            Me.UutState = Dapper.Entities.UutState.Complete
        End If
        Return uutSort
    End Function

End Class


Public Enum UutState

    ''' <summary> An enum constant representing the Idle State; uut is not defined. </summary>
    <Description("Undefined")> Undefined

    ''' <summary> An enum constant representing the Ready State:
    '''           UUT has NUT defined and ready to receive readings. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the Measured State:
    '''           UUT NUT's have readings. </summary>
    <Description("Measured")> Measured

    ''' <summary> An enum constant representing the Present State:
    '''           UUT was sorted and determined to be present. </summary>
    <Description("Present")> Present

    ''' <summary> An enum constant representing the Absent State:
    '''           UUT was sorted and determined to be absent. </summary>
    <Description("Absent")> Absent

    ''' <summary> An enum constant representing the Completed State:
    '''           UUT and NUT data were saved. </summary>
    <Description("Complete")> Complete
End Enum

