Partial Public Class UutEntity

    ''' <summary> Gets or sets the <see cref="UutTraitEntity"/> collection. </summary>
    ''' <value> The Traits. </value>
    Public ReadOnly Property Traits As UutUniqueTraitEntityCollection

    ''' <summary>
    ''' Fetches the <see cref="UutUniqueTraitEntityCollection"/> Natural(Integer)-value type
    ''' trait entity collection.
    ''' </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of Traits. </returns>
    Public Function FetchTraits(ByVal connection As System.Data.IDbConnection) As Integer
        If Not UutTraitTypeEntity.IsEnumerated() Then UutTraitTypeEntity.TryFetchAll(connection)
        Me._Traits = New UutUniqueTraitEntityCollection(Me.AutoId)
        Me._Traits.Populate(UutTraitEntity.FetchEntities(connection, Me.AutoId))
        Return Me.Traits.Count
    End Function

End Class
