Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Uut-Product-Sort builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class UutProductSortBuilder
    Inherits OneToOneBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return UutProductSortBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(UutProductSortBuilder._TableName) Then
                UutProductSortBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(UutProductSortNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = UutBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(UutNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = ProductSortBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(ProductSortNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(UutProductSortEntity.UutAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(UutProductSortEntity.ProductSortAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As UutProductSortBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As UutProductSortBuilder
        If UutProductSortBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                UutProductSortBuilder._Instance = New UutProductSortBuilder()
            End SyncLock
        End If
        Return UutProductSortBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Uut Sort Nub based on the <see cref="IOneToOne">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("UutSort")>
Public Class UutProductSortNub
    Inherits OneToOneNub
    Implements IOneToOne

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToOne
        Return New UutProductSortNub
    End Function

End Class

''' <summary>
''' Sorts the UUT Product Sort based on the <see cref="IOnetoone"/> interface.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class UutProductSortEntity
    Inherits EntityBase(Of IOneToOne, UutProductSortNub)
    Implements IOneToOne

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New UutProductSortNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Uut-Sort interface. </param>
    Public Sub New(ByVal value As IOneToOne)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToOne, ByVal store As IOneToOne)
        MyBase.New(New UutProductSortNub, cache, store)
        Me.UsingNativeTracking = String.Equals(UutProductSortBuilder.TableName, NameOf(IOneToOne).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToOne
        Return New UutProductSortNub
    End Function

    Public Overrides Function CreateCopy() As IOneToOne
        Dim destination As IOneToOne = Me.CreateNew
        UutProductSortNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToOne)
        UutProductSortNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Uut-Sort interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToOne)
        ' first make the copy to notify of any property change.
        UutProductSortNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IOneToOne)(key), connection.Get(Of UutProductSortNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.UutAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.UutAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, uutAutoId)
    End Function

    ''' <summary>
    ''' Tries to fetch and existing or insert a new <see cref="ProductSortEntity"/> and update or insert a new <see cref="UutProductSortEntity"/>.
    ''' </summary>
    ''' <remarks> Assumes that a <see cref="Entities.UutEntity"/> exists for the specified <paramref name="uutAutoId"/> </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="productSortId">      Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtainSort(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal productSortId As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._ProductSortEntity = New ProductSortEntity With {.AutoId = productSortId}
        If Not Me.ProductSortEntity.Obtain(connection) Then
            result = (False, $"Failed obtaining {NameOf(Entities.ProductSortEntity)} with {NameOf(Entities.ProductSortEntity.AutoId)} of {productSortId}")
        End If
        If result.Success Then
            Me.UutAutoId = Me.UutEntity.AutoId
            Me.ProductSortAutoId = Me.ProductSortEntity.AutoId
            If Me.Obtain(connection) Then
                Me.NotifyPropertyChanged(NameOf(UutProductSortEntity.ProductSortEntity))
            Else
                result = (False, $"Failed obtaining {NameOf(Entities.UutProductSortEntity)} for [{NameOf(Entities.UutEntity.AutoId)},{NameOf(Entities.ProductSortEntity.AutoId)}] of [{uutAutoId},{productSortId}]")
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.UutEntity"/> and <see cref="ProductSortEntity"/> entities
    ''' and fetches an existing or inserts a new <see cref="UutProductSortEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="sortId">     Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal sortId As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.UutEntity Is Nothing OrElse Me.UutEntity.AutoId <> uutAutoId Then
            Me._UutEntity = New UutEntity With {.AutoId = uutAutoId}
            If Not Me.UutEntity.FetchUsingKey(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.UutEntity)} with {NameOf(Entities.UutEntity.AutoId)} of {uutAutoId}")
            End If
        End If
        If result.Success Then
            result = Me.TryObtainSort(connection, uutAutoId, sortId)
        End If
        If result.Success Then Me.NotifyPropertyChanged(NameOf(UutProductSortEntity.UutEntity))
        Return result
    End Function

    ''' <summary>
    ''' Tries to fetch existing or insert new <see cref="Entities.UutEntity"/> and <see cref="ProductSortEntity"/> entities
    ''' and fetches an existing or inserts a new <see cref="UutProductSortEntity"/>.
    ''' </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String)
        Return Me.TryObtain(connection, Me.UutAutoId, Me.ProductSortAutoId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToOne) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If UutProductSortEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId">     Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="sortAutoId"> Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal sortAutoId As Integer) As Boolean
        Return connection.Delete(Of UutProductSortNub)(New UutProductSortNub With {.PrimaryId = uutAutoId, .SecondaryId = sortAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Uut-Sort entities. </summary>
    ''' <value> The Uut-Sort entities. </value>
    Public ReadOnly Property UutSorts As IEnumerable(Of UutProductSortEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of UutProductSortEntity)
        Return If(usingNativeTracking, UutProductSortEntity.Populate(connection.GetAll(Of IOneToOne)), UutProductSortEntity.Populate(connection.GetAll(Of UutProductSortNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._UutSorts = UutProductSortEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(UutProductSortEntity.UutSorts))
        Return If(Me.UutSorts?.Any, Me.UutSorts.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of UutProductSortNub)) As IEnumerable(Of UutProductSortEntity)
        Dim l As New List(Of UutProductSortEntity)
        If nubs?.Any Then
            For Each nub As UutProductSortNub In nubs
                l.Add(New UutProductSortEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToOne)) As IEnumerable(Of UutProductSortEntity)
        Dim l As New List(Of UutProductSortEntity)
        If interfaces?.Any Then
            Dim nub As New UutProductSortNub
            For Each iFace As IOneToOne In interfaces
                nub.CopyFrom(iFace)
                l.Add(New UutProductSortEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to Sort entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{UutProductSortBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(UutProductSortNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As IEnumerable(Of UutProductSortEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{UutProductSortBuilder.TableName}] WHERE {NameOf(UutProductSortNub.PrimaryId)} = @Id", New With {Key .Id = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return UutProductSortEntity.Populate(connection.Query(Of UutProductSortNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Sort. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sortAutoId"> Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns> The total number of entities by Sort. </returns>
    Public Shared Function CountEntitiesBySort(ByVal connection As System.Data.IDbConnection, ByVal sortAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{UutProductSortBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(UutProductSortNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Sorts in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sortAutoId"> Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Sorts in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesBySort(ByVal connection As System.Data.IDbConnection, ByVal sortAutoId As Integer) As IEnumerable(Of UutProductSortEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{UutProductSortBuilder.TableName}] WHERE {NameOf(UutProductSortNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return UutProductSortEntity.Populate(connection.Query(Of UutProductSortNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="sortAutoId">       Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal sortAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{UutProductSortBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(UutProductSortNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = uutAutoId})
        sqlBuilder.Where($"{NameOf(UutProductSortNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="sortAutoId">       Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal sortAutoId As Integer) As IEnumerable(Of UutProductSortNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{UutProductSortBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(UutProductSortNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = uutAutoId})
        sqlBuilder.Where($"{NameOf(UutProductSortNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = sortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of UutProductSortNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Uut Sort exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <param name="sortAutoId">       Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer, ByVal sortAutoId As Integer) As Boolean
        Return 1 = UutProductSortEntity.CountEntities(connection, uutAutoId, sortAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Uut entity. </summary>
    ''' <value> The Uut entity. </value>
    Public ReadOnly Property UutEntity As UutEntity

    ''' <summary> Fetches Uut Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Uut Entity. </returns>
    Public Function FetchUutEntity(ByVal connection As System.Data.IDbConnection) As UutEntity
        Dim entity As New UutEntity()
        entity.FetchUsingKey(connection, Me.UutAutoId)
        Me._UutEntity = entity
        Return entity
    End Function

    ''' <summary> Count Uuts associated with the specified <paramref name="sortAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sortAutoId"> Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns> The total number of Uuts. </returns>
    Public Shared Function CountUuts(ByVal connection As System.Data.IDbConnection, ByVal sortAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{UutProductSortBuilder.TableName}] WHERE {NameOf(UutProductSortNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Uuts associated with the specified <paramref name="sortAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sortAutoId"> Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Uuts in this collection.
    ''' </returns>
    Public Shared Function FetchUuts(ByVal connection As System.Data.IDbConnection, ByVal sortAutoId As Integer) As IEnumerable(Of UutEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{UutProductSortBuilder.TableName}] WHERE {NameOf(UutProductSortNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of UutEntity)
        For Each nub As UutProductSortNub In connection.Query(Of UutProductSortNub)(selector.RawSql, selector.Parameters)
            Dim entity As New UutProductSortEntity(nub)
            l.Add(entity.FetchUutEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Uuts associated with the specified <paramref name="sortAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sortAutoId"> Identifies the <see cref="ProductSortEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteUuts(ByVal connection As System.Data.IDbConnection, ByVal sortAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{UutProductSortBuilder.TableName}] WHERE {NameOf(UutProductSortNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = sortAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the Sort entity. </summary>
    ''' <value> The Sort entity. </value>
    Public ReadOnly Property ProductSortEntity As ProductSortEntity

    ''' <summary> Fetches a Sort Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Sort Entity. </returns>
    Public Function FetchSortEntity(ByVal connection As System.Data.IDbConnection) As ProductSortEntity
        Dim entity As New ProductSortEntity()
        entity.FetchUsingKey(connection, Me.ProductSortAutoId)
        Me._ProductSortEntity = entity
        Return entity
    End Function

    ''' <summary> Fetches a Sort Entity. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> The Sort Entity. </returns>
    Public Shared Function FetchSortEntity(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As ProductSortEntity
        Dim entity As ProductSortEntity = UutProductSortEntity.FetchSorts(connection, uutAutoId).FirstOrDefault
        If (entity?.IsClean).GetValueOrDefault(False) Then
            entity.FetchTraits(connection)
        End If
        Return entity
    End Function

    Public Shared Function CountSorts(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{UutProductSortBuilder.TableName}] WHERE {NameOf(UutProductSortNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Sorts in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Sorts in this collection.
    ''' </returns>
    Public Shared Function FetchSorts(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As IEnumerable(Of ProductSortEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{UutProductSortBuilder.TableName}] WHERE {NameOf(UutProductSortNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = uutAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ProductSortEntity)
        For Each nub As UutProductSortNub In connection.Query(Of UutProductSortNub)(selector.RawSql, selector.Parameters)
            Dim entity As New UutProductSortEntity(nub)
            l.Add(entity.FetchSortEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Sort related to the specified Uut. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutAutoId">  Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteSorts(ByVal connection As System.Data.IDbConnection, ByVal uutAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{UutProductSortBuilder.TableName}] WHERE {NameOf(UutProductSortNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = uutAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the lot uut product sorts in this collection. </summary>
    ''' <remarks> David, 7/9/2020</remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="lotAutoId">   Identifier for the lot automatic. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the lot uut product sorts in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchLotUutProductSorts(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer) As IEnumerable(Of UutProductSortEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return UutProductSortEntity.Populate(connection.Query(Of UutProductSortEntity)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the lot uut product sorts in this collection. </summary>
    ''' <remarks> David, 7/9/2020
    ''' Inner join for three tables:          
    ''' <code>
    ''' select * from tableA a
    '''    inner join tableB b on a.common = b.common
    '''    inner join TableC c on b.common = c.common
    ''' </code> 
    ''' Working query:
    ''' <code>
    ''' select [UUtSort].* from [UUtSort]
    '''	   inner join [Uut] on [UutSort].PrimaryId = [Uut].AutoId
    '''    inner join [LotUut] on [Uut].AutoId = [LotUut].SecondaryId
    ''' Where [LotUut].PrimaryId = 3
    ''' </code>. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifier for the lot automatic. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the lot uut product sorts in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchLotUutProductSorts(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of UutProductSortEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UutProductSort].* From [UutProductSort] Inner Join [UutProductSort] on [Uut].PrimaryId 
        ' Inner Join [LotUut] on [LotUut].SecondaryId = [Uut].AutoId where [LotUut].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{UutProductSortBuilder.TableName}].* FROM [{UutProductSortBuilder.TableName}] ")
        queryBuilder.AppendLine($"Inner Join [{UutBuilder.TableName}] ")
        queryBuilder.AppendLine($"   ON [{UutProductSortBuilder.TableName}].{NameOf(UutProductSortNub.PrimaryId)} = [{UutBuilder.TableName}].{NameOf(UutNub.AutoId)}")
        queryBuilder.AppendLine($"Inner Join [{LotUutBuilder.TableName}]")
        queryBuilder.AppendLine($"   ON [{UutBuilder.TableName}].{NameOf(UutNub.AutoId)} = [{LotUutBuilder.TableName}].{NameOf(LotUutNub.SecondaryId)} ")
        queryBuilder.AppendLine($"WHERE [{LotUutBuilder.TableName}].{NameOf(LotUutNub.PrimaryId)} = @{NameOf(lotAutoId)}")
        queryBuilder.AppendLine($"ORDER BY [{UutProductSortBuilder.TableName}].{NameOf(UutProductSortNub.PrimaryId)} ASC; ")
        Return UutProductSortEntity.FetchLotUutProductSorts(connection, queryBuilder.ToString, lotAutoId)
    End Function

    ''' <summary> Deletes the unsorted uuts. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lot">        The <see cref="LotEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteUnsortedUuts(ByVal connection As System.Data.IDbConnection, ByVal lot As LotEntity) As Integer
        Dim result As Integer
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            Dim wasOpen As Boolean = connection.IsOpen
            Try
                If Not wasOpen Then connection.Open()
                Using transaction As Data.IDbTransaction = connection.BeginTransaction
                    Try
                        result = UutProductSortEntity.DeleteUnsortedUuts(New TransactedConnection(connection, transaction), lot)
                        transaction.Commit()
                    Catch
                        transaction.Rollback()
                        Throw
                    Finally
                    End Try
                End Using
            Catch
                Throw
            Finally
                If Not wasOpen Then connection.Close()
            End Try
        Else
            result = UutProductSortEntity.DeleteUnsortedUuts(transactedConnection, lot)
        End If
        Return result
    End Function

    ''' <summary> Deletes the unsorted uuts. </summary>
    ''' <remarks> David, 7/9/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lot">        The <see cref="LotEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    <CLSCompliant(False)>
    Public Shared Function DeleteUnsortedUuts(ByVal connection As TransactedConnection, ByVal lot As LotEntity) As Integer
        If lot.Uuts Is Nothing Then lot.FetchUuts(connection)
        Dim count As Integer
        If lot.Uuts.Any Then
            Dim uutProductSorts As New UutProductSortCollection
            uutProductSorts.Populate(UutProductSortEntity.FetchLotUutProductSorts(connection, lot.AutoId))
            If uutProductSorts.Any Then
                For Each uut As UutEntity In lot.Uuts
                    If Not uutProductSorts.Contains(uut.AutoId) Then
                        ' remove uut from the database if it does not have a sort
                        uut.Delete(connection)
                        count += 1
                    End If
                Next
            Else
                ' all UUTs are unsorted
                For Each uut As UutEntity In lot.Uuts
                    uut.Delete(connection)
                    count += 1
                Next
            End If
            If count > 0 Then
                ' refetch if any uut was removed.
                lot.FetchUuts(connection)
            End If
        Else
            Return count
        End If
        Return count
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToOne.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(UutProductSortEntity.UutAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.UutEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.UutEntity"/>. </value>
    Public Property UutAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToOne.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(UutProductSortEntity.ProductSortAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ProductSortEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ProductSortEntity"/>. </value>
    Public Property ProductSortAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class

''' <summary> Collection of uut product sorts. </summary>
''' <remarks> David, 7/9/2020. </remarks>
Public Class UutProductSortCollection
    Inherits EntityKeyedCollection(Of Integer, IOneToOne, UutProductSortNub, UutProductSortEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 7/9/2020. </remarks>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As UutProductSortEntity) As Integer
        Return item.UutAutoId
    End Function

    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Throw New NotImplementedException()
    End Function

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 7/9/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of UutProductSortEntity))
        For Each entity As UutProductSortEntity In entities
            Me.Add(entity)
        Next
    End Sub

End Class
