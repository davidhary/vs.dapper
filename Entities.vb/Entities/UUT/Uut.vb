Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A builder for a Natural UUT based on the <see cref="IKeyForeignNaturalTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class UutBuilder
    Inherits KeyForeignNaturalTimeBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return UutBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(UutBuilder._TableName) Then
                UutBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(UutNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the foreign table . </summary>
    ''' <value> The name of the foreign table. </value>
    Public Overrides Property ForeignTableName As String = UutTypeBuilder.TableName

    ''' <summary> Gets the name of the foreign table key. </summary>
    ''' <value> The name of the foreign table key. </value>
    Public Overrides Property ForeignTableKeyName As String = NameOf(UutTypeNub.Id)

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(UutEntity.AutoId)

    ''' <summary> Gets or sets the name of the foreign identifier field. </summary>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName() As String = NameOf(UutEntity.UutTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName() As String = NameOf(UutEntity.UutNumber)

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <value> The name of the timestamp field. </value>
    Public Overrides Property TimestampFieldName() As String = NameOf(UutEntity.Timestamp)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As UutBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As UutBuilder
        If UutBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                UutBuilder._Instance = New UutBuilder()
            End SyncLock
        End If
        Return UutBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the natural Uut table based on the <see cref="IKeyForeignNaturalTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("Uut")>
Public Class UutNub
    Inherits KeyForeignNaturalTimeNub
    Implements IKeyForeignNaturalTime

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IKeyForeignNaturalTime
        Return New UutNub
    End Function

End Class


''' <summary> The natural Uut Entity based on the <see cref="IKeyForeignNaturalTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class UutEntity
    Inherits EntityBase(Of IKeyForeignNaturalTime, UutNub)
    Implements IKeyForeignNaturalTime

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New UutNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Uut interface. </param>
    Public Sub New(ByVal value As IKeyForeignNaturalTime)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyForeignNaturalTime, ByVal store As IKeyForeignNaturalTime)
        MyBase.New(New UutNub, cache, store)
        Me.UsingNativeTracking = String.Equals(UutBuilder.TableName, NameOf(IKeyForeignNaturalTime).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IKeyForeignNaturalTime
        Return New UutNub
    End Function


    Public Overrides Function CreateCopy() As IKeyForeignNaturalTime
        Dim destination As IKeyForeignNaturalTime = Me.CreateNew
        UutNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IKeyForeignNaturalTime)
        UutNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Uut interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyForeignNaturalTime)
        ' first make the copy to notify of any property change.
        UutNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Uut table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyForeignNaturalTime)(key), connection.Get(Of UutNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.UutNumber)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutNumber">  The uut number. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal uutNumber As Integer) As Boolean
        Me.ClearStore()
        Dim nub As UutNub = UutEntity.FetchNubs(connection, uutNumber).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyForeignNaturalTime) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If UutEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(UniqueIndexOptions.Amount = (UutBuilder.Get.QueryUniqueIndexOptions(connection) And UniqueIndexOptions.Amount),
                                        Me.FetchUsingUniqueIndex(connection, entity.Amount),
                                        Me.FetchUsingKey(connection, entity.AutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of UutNub)(New UutNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Uut entities. </summary>
    ''' <value> The Uut entities. </value>
    Public ReadOnly Property Uuts As IEnumerable(Of UutEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of UutEntity)
        Return If(usingNativeTracking, UutEntity.Populate(connection.GetAll(Of IKeyForeignNaturalTime)), UutEntity.Populate(connection.GetAll(Of UutNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Uuts = UutEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(UutEntity.Uuts))
        Return If(Me.Uuts?.Any, Me.Uuts.Count, 0)
    End Function

    ''' <summary> Populates a list of Uut entities. </summary>
    ''' <param name="nubs"> The Uut nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of UutNub)) As IEnumerable(Of UutEntity)
        Dim l As New List(Of UutEntity)
        If nubs?.Any Then
            For Each nub As UutNub In nubs
                l.Add(New UutEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Uut entities. </summary>
    ''' <param name="interfaces"> The Uut interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyForeignNaturalTime)) As IEnumerable(Of UutEntity)
        Dim l As New List(Of UutEntity)
        If interfaces?.Any Then
            Dim nub As New UutNub
            For Each iFace As IKeyForeignNaturalTime In interfaces
                nub.CopyFrom(iFace)
                l.Add(New UutEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutNumber">  The uut number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal uutNumber As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{UutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(UutNub.Amount)} = @UutNumber", New With {uutNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutNumber">  The uut number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal uutNumber As Integer) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{UutBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(UutNub.Amount)} IS NULL OR {NameOf(UutNub.Amount)} = @UutNumber)")
        selectSql.Append("; ")
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {uutNumber})
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutNumber">  The uut number. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal uutNumber As Integer) As IEnumerable(Of UutNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{UutBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(UutNub.Amount)} = @UutNumber", New With {uutNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of UutNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the uut exists. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="uutNumber">  The uut number. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal uutNumber As Integer) As Boolean
        Return 1 = UutEntity.CountEntities(connection, uutNumber)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Uut. </summary>
    ''' <value> Identifies the Uut. </value>
    Public Property AutoId As Integer Implements IKeyForeignNaturalTime.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="UutTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="UutTypeEntity"/>. </value>
    Public Property ForeignId As Integer Implements IKeyForeignNaturalTime.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(UutEntity.UutTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="UutTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="UutTypeEntity"/>. </value>
    Public Property UutTypeId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(value As Integer)
            Me.ForeignId = value
        End Set
    End Property


    ''' <summary> Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value. </summary>
    ''' <value> The natural amount. </value>
    Public Property Amount As Integer Implements IKeyForeignNaturalTime.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(value As Integer)
            If Me.Amount <> value Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(UutEntity.UutNumber))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the uut number. </summary>
    ''' <value> The uut number. </value>
    Public Property UutNumber As Integer
        Get
            Return Me.Amount
        End Get
        Set(value As Integer)
            Me.Amount = value
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks> Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to convert to local time. </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements IKeyForeignNaturalTime.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " TIMESTAMP "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of uut entities. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class UutEntityCollection
    Inherits EntityKeyedCollection(Of Integer, IKeyForeignNaturalTime, UutNub, UutEntity)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(ByVal item As UutEntity) As Integer
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.AutoId
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As UutEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of UutEntity))
        If entities?.Any Then
            For Each entity As UutEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Fetches nom readings. </summary>
    ''' <remarks> David, 7/2/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The nom readings. </returns>
    Public Function FetchNomReadings(ByVal connection As System.Data.IDbConnection) As Integer
        Dim count As Integer = 0
        For Each uut As UutEntity In Me
            count += uut.FetchNomReadings(connection)
        Next
        Return count
    End Function

    ''' <summary> Initializes the nom readings. </summary>
    ''' <remarks> David, 7/2/2020. </remarks>
    ''' <param name="elements">    The elements. </param>
    Public Sub InitializeNomReadings(ByVal elements As ElementEntityCollection)
        For Each uut As UutEntity In Me
            uut.InitializeNomReadings(elements)
        Next
    End Sub

    ''' <summary> Fetches the Nuts. </summary>
    ''' <remarks> David, 7/2/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Sub FetchNuts(ByVal connection As System.Data.IDbConnection)
        For Each uut As UutEntity In Me
            uut.FetchNuts(connection)
        Next
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Throw New NotImplementedException()
    End Function

End Class

''' <summary> Collection of uut entities uniquely associated with a lot. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class LotUniqueUutEntityCollection
    Inherits UutEntityCollection

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
        Me._UniqueIndexDictionary = New Dictionary(Of Integer, Integer)
        Me._PrimaryKeyDictionary = New Dictionary(Of Integer, Integer)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    Public Sub New(ByVal lotAutoId As Integer)
        Me.New
        Me.LotAutoId = lotAutoId
    End Sub

    ''' <summary> Dictionary of unique indexes. </summary>
    Private ReadOnly _UniqueIndexDictionary As IDictionary(Of Integer, Integer)

    ''' <summary> Dictionary of primary keys. </summary>
    Private ReadOnly _PrimaryKeyDictionary As IDictionary(Of Integer, Integer)

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="entity"> The object to be added to the end of the
    '''                       <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
    '''                       value can be <see langword="null" /> for reference types. </param>
    Public Overrides Sub Add(ByVal entity As UutEntity)
        MyBase.Add(entity)
        Me._PrimaryKeyDictionary.Add(entity.Amount, entity.AutoId)
        Me._UniqueIndexDictionary.Add(entity.AutoId, entity.Amount)
    End Sub

    ''' <summary>
    ''' Removes all uuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._UniqueIndexDictionary.Clear()
        Me._PrimaryKeyDictionary.Clear()
    End Sub

    ''' <summary> Selects an uut from the collection. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="uutNumber"> The uut number. </param>
    ''' <returns> An UutEntity. </returns>
    Public Function Uut(ByVal uutNumber As Integer) As UutEntity
        Dim id As Integer = Me._PrimaryKeyDictionary(uutNumber)
        Return If(Me.Contains(id), Me(id), New UutEntity)
    End Function

    ''' <summary> Uut number. </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="uutAutoId"> Identifies the <see cref="Entities.UutEntity"/>. </param>
    ''' <returns> The UUT number or -1 if not found. </returns>
    Public Function UutNumber(ByVal uutAutoId As Integer) As Integer
        Return If(Me._UniqueIndexDictionary.ContainsKey(uutAutoId), Me._UniqueIndexDictionary(uutAutoId), -1)
    End Function

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.UutEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.UutEntity"/>. </value>
    Public ReadOnly Property LotAutoId As Integer


End Class
