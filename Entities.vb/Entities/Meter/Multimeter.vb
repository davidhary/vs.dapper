Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary>
''' A Multimeter builder based on the <see cref="IExplicitKeyForeignNatural">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class MultimeterBuilder
    Inherits ExplicitKeyForeignNaturalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return MultimeterBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(MultimeterBuilder._TableName) Then
                MultimeterBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(MultimeterNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the foreign table . </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the foreign table. </value>
    Public Overrides Property ForeignTableName As String = MultimeterModelBuilder.TableName

    ''' <summary> Gets the name of the foreign table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the foreign table key. </value>
    Public Overrides Property ForeignTableKeyName As String = NameOf(MultimeterModelNub.Id)

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValues(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return InsertValues(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return InsertValues(sqlite)
        Return 0
    End Function

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValues(ByVal connection As System.Data.SQLite.SQLiteConnection) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim builder As New Text.StringBuilder()
        builder.AppendLine($"INSERT OR IGNORE INTO [{MultimeterBuilder.TableName}] VALUES (1,1,1); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{MultimeterBuilder.TableName}] VALUES (2,1,2); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{MultimeterBuilder.TableName}] VALUES (3,2,1); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{MultimeterBuilder.TableName}] VALUES (4,2,2); ")
        Return connection.Execute(builder.ToString)
    End Function

    ''' <summary> Inserts or ignores the records using an SQL client connection. </summary>
    ''' <remarks> David, 3/14/2020. Supports SQL Client only library. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValuesSqlClient(ByVal connection As System.Data.IDbConnection) As Integer
        Return MultimeterBuilder.InsertValues(TryCast(connection, System.Data.SqlClient.SqlConnection))
    End Function

    ''' <summary> Inserts the values SQL lite described by connection. </summary>
    ''' <remarks> David, 6/18/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValuesSQLite(ByVal connection As System.Data.IDbConnection) As Integer
        Return MultimeterBuilder.InsertValues(TryCast(connection, System.Data.SQLite.SQLiteConnection))
    End Function

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValues(ByVal connection As System.Data.SqlClient.SqlConnection) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim builder As New Text.StringBuilder()
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{MultimeterBuilder.TableName}] WHERE [{NameOf(MultimeterNub.Id)}] = 1) INSERT INTO [{MultimeterBuilder.TableName}] VALUES (1,1,1); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{MultimeterBuilder.TableName}] WHERE [{NameOf(MultimeterNub.Id)}] = 2) INSERT INTO [{MultimeterBuilder.TableName}] VALUES (2,1,2); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{MultimeterBuilder.TableName}] WHERE [{NameOf(MultimeterNub.Id)}] = 3) INSERT INTO [{MultimeterBuilder.TableName}] VALUES (3,2,1); ")
        builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{MultimeterBuilder.TableName}] WHERE [{NameOf(MultimeterNub.Id)}] = 4) INSERT INTO [{MultimeterBuilder.TableName}] VALUES (4,2,2); ")
        Return connection.Execute(builder.ToString)
    End Function

    ''' <summary> Gets the name of the automatic identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property IdFieldName() As String = NameOf(MultimeterEntity.Id)

    ''' <summary> Gets the name of the foreign identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName() As String = NameOf(MultimeterEntity.MultimeterModelId)

    ''' <summary> Gets the name of the amount field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName() As String = NameOf(MultimeterEntity.MultimeterNumber)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As MultimeterBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As MultimeterBuilder
        If MultimeterBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                MultimeterBuilder._Instance = New MultimeterBuilder()
            End SyncLock
        End If
        Return MultimeterBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Multimeter table based on the
''' <see cref="IExplicitKeyForeignNatural">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("Multimeter")>
Public Class MultimeterNub
    Inherits ExplicitKeyForeignNaturalNub
    Implements IExplicitKeyForeignNatural

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IExplicitKeyForeignNatural
        Return New MultimeterNub
    End Function

End Class

''' <summary>
''' The <see cref="Entities.MultimeterEntity"/>. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class MultimeterEntity
    Inherits EntityBase(Of IExplicitKeyForeignNatural, MultimeterNub)
    Implements IExplicitKeyForeignNatural

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New MultimeterNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Multimeter interface. </param>
    Public Sub New(ByVal value As IExplicitKeyForeignNatural)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IExplicitKeyForeignNatural, ByVal store As IExplicitKeyForeignNatural)
        MyBase.New(New MultimeterNub, cache, store)
        Me.UsingNativeTracking = String.Equals(MultimeterBuilder.TableName, NameOf(IExplicitKeyForeignNatural).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IExplicitKeyForeignNatural
        Return New MultimeterNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IExplicitKeyForeignNatural
        Dim destination As IExplicitKeyForeignNatural = Me.CreateNew
        MultimeterNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IExplicitKeyForeignNatural)
        MultimeterNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Multimeter interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IExplicitKeyForeignNatural)
        ' first make the copy to notify of any property change.
        MultimeterNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Multimeter table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IExplicitKeyForeignNatural)(key), connection.Get(Of MultimeterNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.MultimeterModelId, Me.MultimeterNumber)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="meterModelId"> Identifier for the meter model. </param>
    ''' <param name="meterNumber">  The meter number. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal meterModelId As Integer, ByVal meterNumber As Integer) As Boolean
        Me.ClearStore()
        Dim nub As MultimeterNub = MultimeterEntity.FetchNubs(connection, meterModelId, meterNumber).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IExplicitKeyForeignNatural) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If MultimeterEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Id, entity.ForeignId) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of MultimeterNub)(New MultimeterNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets the Multimeter entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Multimeter entities. </value>
    Public Shared ReadOnly Property Multimeters As IEnumerable(Of MultimeterEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of MultimeterEntity)
        Return If(usingNativeTracking, MultimeterEntity.Populate(connection.GetAll(Of IExplicitKeyForeignNatural)), MultimeterEntity.Populate(connection.GetAll(Of MultimeterNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        MultimeterEntity._Multimeters = MultimeterEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(MultimeterEntity.Multimeters))
        Return If(MultimeterEntity.Multimeters?.Any, MultimeterEntity.Multimeters.Count, 0)
    End Function

    ''' <summary> Populates a list of Multimeter entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The Multimeter nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of MultimeterNub)) As IEnumerable(Of MultimeterEntity)
        Dim l As New List(Of MultimeterEntity)
        If nubs?.Any Then
            For Each nub As MultimeterNub In nubs
                l.Add(New MultimeterEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Multimeter entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The Multimeter interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IExplicitKeyForeignNatural)) As IEnumerable(Of MultimeterEntity)
        Dim l As New List(Of MultimeterEntity)
        If interfaces?.Any Then
            Dim nub As New MultimeterNub
            For Each iFace As IExplicitKeyForeignNatural In interfaces
                nub.CopyFrom(iFace)
                l.Add(New MultimeterEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Dictionary of entity lookups. </summary>
    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, MultimeterEntity)

    ''' <summary> The entity lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of Integer, MultimeterEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, MultimeterEntity)
        If Not (MultimeterEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            MultimeterEntity._EntityLookupDictionary = New Dictionary(Of Integer, MultimeterEntity)
            For Each entity As MultimeterEntity In MultimeterEntity.Multimeters
                MultimeterEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return MultimeterEntity._EntityLookupDictionary
    End Function

    ''' <summary> Dictionary of key lookups. </summary>
    Private Shared _KeyLookupDictionary As IDictionary(Of KeyValuePair(Of Integer, Integer), Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of KeyValuePair(Of Integer, Integer), Integer)
        If Not (MultimeterEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            MultimeterEntity._KeyLookupDictionary = New Dictionary(Of KeyValuePair(Of Integer, Integer), Integer)
            For Each entity As MultimeterEntity In MultimeterEntity.Multimeters
                MultimeterEntity._KeyLookupDictionary.Add(New KeyValuePair(Of Integer, Integer)(entity.MultimeterModelId, entity.MultimeterNumber), entity.Id)
            Next
        End If
        Return MultimeterEntity._KeyLookupDictionary
    End Function

    ''' <summary> Select multimeter entity. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <param name="multimeterModelId"> Identifies the <see cref="Entities.MultimeterEntity"/> model. </param>
    ''' <param name="meterNumber">       The meter number. </param>
    ''' <returns> A MultimeterEntity. </returns>
    Public Shared Function SelectMultimeterEntity(ByVal multimeterModelId As Integer, ByVal meterNumber As Integer) As MultimeterEntity
        Return EntityLookupDictionary(MultimeterEntity.KeyLookupDictionary(New KeyValuePair(Of Integer, Integer)(multimeterModelId, meterNumber)))
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (MultimeterEntity.Multimeters?.Any AndAlso
                MultimeterEntity._EntityLookupDictionary?.Any AndAlso
                MultimeterEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns up to Bin entities count. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="multimeterModelId"> Identifies the <see cref="Entities.MultimeterEntity"/> model. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal multimeterModelId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{MultimeterBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(MultimeterNub.ForeignId)} = @TypeId", New With {.TypeId = multimeterModelId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="multimeterModelId"> Identifies the <see cref="Entities.MultimeterEntity"/> model. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal multimeterModelId As Integer) As IEnumerable(Of MultimeterEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{MultimeterBuilder.TableName}] WHERE {NameOf(MultimeterNub.ForeignId)} = @Id", New With {Key .Id = multimeterModelId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return MultimeterEntity.Populate(connection.Query(Of MultimeterNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by meter number. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="meterNumber"> The meter number. </param>
    ''' <returns> The total number of entities by meter number. </returns>
    Public Shared Function CountEntitiesByMeterNumber(ByVal connection As System.Data.IDbConnection, ByVal meterNumber As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{MultimeterBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(MultimeterNub.Amount)} = @Amount", New With {.Amount = meterNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by meter number in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="meterNumber"> The meter number. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Bins in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByMeterNumber(ByVal connection As System.Data.IDbConnection, ByVal meterNumber As Integer) As IEnumerable(Of MultimeterEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{MultimeterBuilder.TableName}] WHERE {NameOf(MultimeterNub.Amount)} = @Amount", New With {Key .Amount = meterNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return MultimeterEntity.Populate(connection.Query(Of MultimeterNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="multimeterModelId"> Identifies the <see cref="Entities.MultimeterEntity"/> model. </param>
    ''' <param name="multimeterNumber">  The multimeter number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal multimeterModelId As Integer, ByVal multimeterNumber As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{MultimeterBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(MultimeterNub.ForeignId)} = @TypeId", New With {.TypeId = multimeterModelId})
        sqlBuilder.Where($"{NameOf(MultimeterNub.Amount)} = @Amount", New With {.Amount = multimeterNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="multimeterModelId"> Identifies the <see cref="Entities.MultimeterEntity"/> model. </param>
    ''' <param name="multimeterNumber">  The multimeter number. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal multimeterModelId As Integer, ByVal multimeterNumber As Integer) As IEnumerable(Of MultimeterNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{MultimeterBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(MultimeterNub.ForeignId)} = @TypeId", New With {.TypeId = multimeterModelId})
        sqlBuilder.Where($"{NameOf(MultimeterNub.Amount)} = @Amount", New With {.Amount = multimeterNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of MultimeterNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Sort Bin exists. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="multimeterModelId"> Identifies the <see cref="Entities.MultimeterEntity"/> model. </param>
    ''' <param name="multimeterNumber">  The multimeter number. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal multimeterModelId As Integer, ByVal multimeterNumber As Integer) As Boolean
        Return 1 = MultimeterEntity.CountEntities(connection, multimeterModelId, multimeterNumber)
    End Function

#End Region

#Region " RELATIONS "

#Region " METER "

    ''' <summary> The Meter Model entity. </summary>
    Private _MultimeterModelEntity As MultimeterModelEntity

    ''' <summary> Gets the Meter Model entity. </summary>
    ''' <value> The Meter Model entity. </value>
    Public ReadOnly Property MultimeterModelEntity As MultimeterModelEntity
        Get
            If Me._MultimeterModelEntity Is Nothing Then
                Me._MultimeterModelEntity = MultimeterModelEntity.EntityLookupDictionary(Me.MultimeterModelId)
            End If
            Return Me._MultimeterModelEntity
        End Get
    End Property

    ''' <summary> Fetches the Meter Model entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchMultimeterModel(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not MultimeterModelEntity.IsEnumerated Then MultimeterModelEntity.TryFetchAll(connection)
        Me._MultimeterModelEntity = MultimeterModelEntity.EntityLookupDictionary(Me.MultimeterModelId)
        Me.NotifyPropertyChanged(NameOf(MultimeterEntity.MultimeterModelEntity))
        Return Me.MultimeterModelEntity IsNot Nothing
    End Function

#End Region

#End Region

#Region " FIELDS "

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/>.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property Id As Integer Implements IExplicitKeyForeignNatural.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/> type.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/> type. </value>
    Public Property ForeignId As Integer Implements IExplicitKeyForeignNatural.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(MultimeterEntity.MultimeterModelId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/> model.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/> model. </value>
    Public Property MultimeterModelId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(value As Integer)
            Me.ForeignId = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
    ''' </summary>
    ''' <value> The natural amount. </value>
    Public Property Amount As Integer Implements IExplicitKeyForeignNatural.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(value As Integer)
            If Me.Amount <> value Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(MultimeterEntity.MultimeterNumber))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the multimeter number. </summary>
    ''' <value> The multimeter number. </value>
    Public Property MultimeterNumber As Integer
        Get
            Return Me.Amount
        End Get
        Set(value As Integer)
            Me.Amount = value
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="multimeterId"> The entity unique key. </param>
    ''' <returns> A <see cref="Entities.MultimeterEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal multimeterId As Integer) As (Success As Boolean, Details As String, Entity As MultimeterEntity)
        Dim activity As String = String.Empty
        Dim entity As New MultimeterEntity
        Try
            activity = $"Fetching {NameOf(MultimeterEntity)} by {NameOf(MultimeterNub.Id)} of {multimeterId}"
            Return If(entity.FetchUsingKey(connection, multimeterId),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 4/28/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="meterModelId"> Identifier for the meter model. </param>
    ''' <param name="meterNumber">  The multimeter label. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As MultimeterEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal meterModelId As Integer,
                                                    ByVal meterNumber As Integer) As (Success As Boolean, Details As String, Entity As MultimeterEntity)
        Dim activity As String = String.Empty
        Dim entity As New MultimeterEntity
        Try
            activity = $"Fetching {NameOf(MultimeterEntity)} by [{NameOf(MultimeterNub.ForeignId)},{NameOf(MultimeterNub.Amount)}] of [{meterModelId},{meterNumber}]"
            Return If(entity.FetchUsingUniqueIndex(connection, meterModelId, meterNumber),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, details As String, Entities As IEnumerable(Of MultimeterEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, details As String, Entities As IEnumerable(Of MultimeterEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New MultimeterEntity()
            activity = $"fetching all {NameOf(MultimeterEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> MultimeterEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(MultimeterEntity.EntityLookupDictionary)} count must equal {NameOf(MultimeterEntity.Multimeters)} count ")
            ElseIf elementCount <> MultimeterEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(MultimeterEntity.KeyLookupDictionary)} count must equal {NameOf(MultimeterEntity.Multimeters)} count ")
            End If
            If Not MultimeterModelEntity.IsEnumerated() Then MultimeterModelEntity.TryFetchAll(connection)
            Return (True, String.Empty, MultimeterEntity.Multimeters)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of MultimeterEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Collection of Multimeter entities. </summary>
''' <remarks> David, 5/19/2020. </remarks>
Public Class MultimeterEntityCollection
    Inherits EntityKeyedCollection(Of Integer, IExplicitKeyForeignNatural, MultimeterNub, MultimeterEntity)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(ByVal item As MultimeterEntity) As Integer
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.Id
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As MultimeterEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of MultimeterEntity))
        If entities?.Any Then
            For Each entity As MultimeterEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Throw New NotImplementedException()
    End Function

End Class

