Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary>
''' Interface for the Ohm Meter Setting Lookup nub and entity. Includes the fields as kept in the
''' data table. Allows tracking of property changes.
''' </summary>
''' <remarks>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value. <para>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IOhmMeterSettingLookup

    ''' <summary> Gets the identifier of the Ohm Meter Setting Lookup. </summary>
    ''' <value> Identifies the Ohm Meter Setting Lookup. </value>
    <ExplicitKey>
    Property Id As Integer

    ''' <summary> Gets the tolerance code. </summary>
    ''' <value> The tolerance code. </value>
    Property ToleranceCode As String

    ''' <summary> Gets Identifies the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Property MultimeterId As Integer

    ''' <summary> Gets the minimum inclusive value. </summary>
    ''' <value> The minimum inclusive value. </value>
    Property MinimumInclusiveValue As Double?

    ''' <summary> Gets the maximum exclusive value. </summary>
    ''' <value> The maximum exclusive value. </value>
    Property MaximumExclusiveValue As Double?

    ''' <summary> Gets Identifies the selected Ohm Meter Setting. </summary>
    ''' <value> Identifies the selected Ohm Meter Setting. </value>
    Property OhmMeterSettingId As Integer

End Interface

''' <summary> The ohm meter setting lookup builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class OhmMeterSettingLookupBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(OhmMeterSettingLookupBuilder._TableName) Then
                OhmMeterSettingLookupBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(OhmMeterSettingLookupNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the multimeter table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the multimeter table. </value>
    Public Shared Property MultimeterTableName As String = MultimeterModelBuilder.TableName

    ''' <summary> Gets the name of the multimeter table foreign key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the multimeter table foreign key. </value>
    Public Shared Property MultimeterTableKeyName As String = NameOf(MultimeterModelNub.Id)

    ''' <summary> Inserts values from file. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="fileName">   Filename of the file. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertFileValues(ByVal connection As System.Data.IDbConnection, ByVal fileName As String) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        If fileName Is Nothing Then Throw New ArgumentNullException(NameOf(fileName))
        Return connection.Execute(System.IO.File.ReadAllText(fileName))
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{OhmMeterSettingLookupBuilder.TableName}] (
[{NameOf(OhmMeterSettingLookupNub.Id)}] integer NOT NULL PRIMARY KEY, 
[{NameOf(OhmMeterSettingLookupNub.ToleranceCode)}] [nchar](1) NULL, 
[{NameOf(OhmMeterSettingLookupNub.MultimeterId)}] integer NULL,
[{NameOf(OhmMeterSettingLookupNub.MinimumInclusiveValue)}] [float] NULL,
[{NameOf(OhmMeterSettingLookupNub.MaximumExclusiveValue)}] [float] NULL,
[{NameOf(OhmMeterSettingLookupNub.OhmMeterSettingId)}] [integer] NOT NULL,
FOREIGN KEY ([{NameOf(OhmMeterSettingLookupNub.ToleranceCode)}]) REFERENCES [{ToleranceBuilder.TableName}] ([{NameOf(ToleranceNub.ToleranceCode)}]) ON UPDATE CASCADE ON DELETE CASCADE, 
FOREIGN KEY ([{NameOf(OhmMeterSettingLookupNub.OhmMeterSettingId)}]) REFERENCES [{OhmMeterSettingBuilder.TableName}] ([{NameOf(OhmMeterSettingNub.Id)}]) ON UPDATE CASCADE ON DELETE CASCADE, 
FOREIGN KEY ([{NameOf(OhmMeterSettingLookupNub.MultimeterId)}]) REFERENCES [{OhmMeterSettingLookupBuilder.MultimeterTableName}] ([{OhmMeterSettingLookupBuilder.MultimeterTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmMeterSettingLookupBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{OhmMeterSettingLookupBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{OhmMeterSettingLookupBuilder.TableName}](
	[{NameOf(OhmMeterSettingLookupNub.Id)}] [int] NOT NULL,
	[{NameOf(OhmMeterSettingLookupNub.ToleranceCode)}] [nchar](1) NULL,
	[{NameOf(OhmMeterSettingLookupNub.MultimeterId)}] [int] NULL,
	[{NameOf(OhmMeterSettingLookupNub.MinimumInclusiveValue)}] [float] NULL,
	[{NameOf(OhmMeterSettingLookupNub.MaximumExclusiveValue)}] [float] NULL,
	[{NameOf(OhmMeterSettingLookupNub.OhmMeterSettingId)}] [int] NOT NULL,
 CONSTRAINT [PK_{OhmMeterSettingLookupBuilder.TableName}] PRIMARY KEY CLUSTERED 
(
	[{NameOf(OhmMeterSettingLookupNub.Id)}] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END;

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmMeterSettingLookupBuilder.TableName}_{OhmMeterSettingLookupBuilder.MultimeterTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmMeterSettingLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmMeterSettingLookupBuilder.TableName}]  WITH CHECK ADD CONSTRAINT [FK_{OhmMeterSettingLookupBuilder.TableName}_{OhmMeterSettingLookupBuilder.MultimeterTableName}] FOREIGN KEY([{NameOf(OhmMeterSettingLookupNub.MultimeterId)}])
REFERENCES [dbo].[{OhmMeterSettingLookupBuilder.MultimeterTableName}] ([{OhmMeterSettingLookupBuilder.MultimeterTableKeyName}])
ON UPDATE CASCADE
ON DELETE CASCADE;

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmMeterSettingLookupBuilder.TableName}_{OhmMeterSettingLookupBuilder.MultimeterTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmMeterSettingLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmMeterSettingLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{OhmMeterSettingLookupBuilder.TableName}_{OhmMeterSettingLookupBuilder.MultimeterTableName}];

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmMeterSettingLookupBuilder.TableName}_{OhmMeterSettingBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmMeterSettingLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmMeterSettingLookupBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{OhmMeterSettingLookupBuilder.TableName}_{OhmMeterSettingBuilder.TableName}] FOREIGN KEY([{NameOf(OhmMeterSettingLookupNub.OhmMeterSettingId)}])
REFERENCES [dbo].[{OhmMeterSettingBuilder.TableName}] ([{NameOf(OhmMeterSettingNub.Id)}])
ON UPDATE CASCADE
ON DELETE CASCADE;

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmMeterSettingLookupBuilder.TableName}_{OhmMeterSettingBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmMeterSettingLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmMeterSettingLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{OhmMeterSettingLookupBuilder.TableName}_{OhmMeterSettingBuilder.TableName}];

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmMeterSettingLookupBuilder.TableName}_{ToleranceBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmMeterSettingLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmMeterSettingLookupBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{OhmMeterSettingLookupBuilder.TableName}_{ToleranceBuilder.TableName}] FOREIGN KEY([{NameOf(OhmMeterSettingLookupNub.ToleranceCode)}])
REFERENCES [dbo].[{ToleranceBuilder.TableName}] ([{NameOf(ToleranceNub.ToleranceCode)}])
ON UPDATE CASCADE
ON DELETE CASCADE;

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmMeterSettingLookupBuilder.TableName}_{ToleranceBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmMeterSettingLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmMeterSettingLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{OhmMeterSettingLookupBuilder.TableName}_{ToleranceBuilder.TableName}]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmMeterSettingLookupBuilder.TableName
    End Function

End Class

''' <summary>
''' Implements the Ohm Meter Setting Lookup table
''' <see cref="IOhmMeterSettingLookup">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2019 </para>
''' </remarks>
<Table("OhmMeterSettingLookup")>
Public Class OhmMeterSettingLookupNub
    Inherits EntityNubBase(Of IOhmMeterSettingLookup)
    Implements IOhmMeterSettingLookup

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmMeterSettingLookup
        Return New OhmMeterSettingLookupNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmMeterSettingLookup
        Dim destination As IOhmMeterSettingLookup = Me.CreateNew
        OhmMeterSettingLookupNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmMeterSettingLookup)
        OhmMeterSettingLookupNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IOhmMeterSettingLookup, ByVal destination As IOhmMeterSettingLookup)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.MaximumExclusiveValue = source.MaximumExclusiveValue
        destination.MultimeterId = source.MultimeterId
        destination.MinimumInclusiveValue = source.MinimumInclusiveValue
        destination.OhmMeterSettingId = source.OhmMeterSettingId
        destination.Id = source.Id
        destination.ToleranceCode = source.ToleranceCode
    End Sub

#End Region

#Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IOhmMeterSettingLookup))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IOhmMeterSettingLookup) As Boolean
        Return other IsNot Nothing AndAlso OhmMeterSettingLookupNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IOhmMeterSettingLookup, ByVal right As IOhmMeterSettingLookup) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Double?.Equals(left.MaximumExclusiveValue, right.MaximumExclusiveValue)
            result = result AndAlso Integer.Equals(left.MultimeterId, right.MultimeterId)
            result = result AndAlso Double?.Equals(left.MinimumInclusiveValue, right.MinimumInclusiveValue)
            result = result AndAlso Integer.Equals(left.OhmMeterSettingId, right.OhmMeterSettingId)
            result = result AndAlso Integer.Equals(left.Id, right.Id)
            result = result AndAlso String.Equals(left.ToleranceCode, right.ToleranceCode)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.MaximumExclusiveValue.GetHashCode() Xor Me.MultimeterId Xor Me.MinimumInclusiveValue.GetHashCode() Xor Me.OhmMeterSettingId Xor Me.Id Xor Me.ToleranceCode.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets the identifier of the Ohm Meter Setting Lookup. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the Ohm Meter Setting Lookup. </value>
    <ExplicitKey>
    Public Property Id As Integer Implements IOhmMeterSettingLookup.Id

    ''' <summary> Gets the tolerance code. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The tolerance code. </value>
    Public Property ToleranceCode As String Implements IOhmMeterSettingLookup.ToleranceCode

    ''' <summary> Gets Identifies the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer Implements IOhmMeterSettingLookup.MultimeterId

    ''' <summary> Gets the minimum inclusive value. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The minimum inclusive value. </value>
    Public Property MinimumInclusiveValue As Double? Implements IOhmMeterSettingLookup.MinimumInclusiveValue

    ''' <summary> Gets the maximum exclusive value. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The maximum exclusive value. </value>
    Public Property MaximumExclusiveValue As Double? Implements IOhmMeterSettingLookup.MaximumExclusiveValue

    ''' <summary> Gets Identifies the selected Ohm Meter Setting. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the selected Ohm Meter Setting. </value>
    Public Property OhmMeterSettingId As Integer Implements IOhmMeterSettingLookup.OhmMeterSettingId

#End Region

End Class

''' <summary>
''' The Ohm Meter Setting Lookup Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2019 </para>
''' </remarks>
Public Class OhmMeterSettingLookupEntity
    Inherits EntityBase(Of IOhmMeterSettingLookup, OhmMeterSettingLookupNub)
    Implements IOhmMeterSettingLookup

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New OhmMeterSettingLookupNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As IOhmMeterSettingLookup)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOhmMeterSettingLookup, ByVal store As IOhmMeterSettingLookup)
        MyBase.New(New OhmMeterSettingLookupNub, cache, store)
        Me.UsingNativeTracking = String.Equals(OhmMeterSettingLookupBuilder.TableName, NameOf(IOhmMeterSettingLookup).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmMeterSettingLookup
        Return New OhmMeterSettingLookupNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmMeterSettingLookup
        Dim destination As IOhmMeterSettingLookup = Me.CreateNew
        OhmMeterSettingLookupNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmMeterSettingLookup)
        OhmMeterSettingLookupNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOhmMeterSettingLookup)
        ' first make the copy to notify of any property change.
        OhmMeterSettingLookupNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        the Meter Model table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IOhmMeterSettingLookup)(key), connection.Get(Of OhmMeterSettingLookupNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 3/10/2020. </remarks>
    ''' <param name="connection">              The connection. </param>
    ''' <param name="ohmMeterSettingLookupId"> Identifies the
    '''                                        <see cref="OhmMeterSettingLookupEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal ohmMeterSettingLookupId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, ohmMeterSettingLookupId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 5/16/2020. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOhmMeterSettingLookup) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If OhmMeterSettingLookupEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.Id) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of OhmMeterSettingLookupNub)(New OhmMeterSettingLookupNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets the Trait Type entities. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Trait Type entities. </value>
    Public Shared ReadOnly Property OhmMeterSettingLookups As IEnumerable(Of OhmMeterSettingLookupEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of OhmMeterSettingLookupEntity)
        Return If(usingNativeTracking, OhmMeterSettingLookupEntity.Populate(connection.GetAll(Of IOhmMeterSettingLookup)), OhmMeterSettingLookupEntity.Populate(connection.GetAll(Of OhmMeterSettingLookupNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        OhmMeterSettingLookupEntity._OhmMeterSettingLookups = OhmMeterSettingLookupEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(OhmMeterSettingLookupEntity.OhmMeterSettingLookups))
        Return If(OhmMeterSettingLookupEntity.OhmMeterSettingLookups?.Any, OhmMeterSettingLookupEntity.OhmMeterSettingLookups.Count, 0)
    End Function

    ''' <summary> Populates a list of MeterModel entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> the Meter Model nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of OhmMeterSettingLookupNub)) As IEnumerable(Of OhmMeterSettingLookupEntity)
        Dim l As New List(Of OhmMeterSettingLookupEntity)
        If nubs?.Any Then
            For Each nub As OhmMeterSettingLookupNub In nubs
                l.Add(New OhmMeterSettingLookupEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of MeterModel entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> the Meter Model interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOhmMeterSettingLookup)) As IEnumerable(Of OhmMeterSettingLookupEntity)
        Dim l As New List(Of OhmMeterSettingLookupEntity)
        If interfaces?.Any Then
            Dim nub As New OhmMeterSettingLookupNub
            For Each iFace As IOhmMeterSettingLookup In interfaces
                nub.CopyFrom(iFace)
                l.Add(New OhmMeterSettingLookupEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="meterModelId"> Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal meterModelId As Integer) As IEnumerable(Of OhmMeterSettingLookupEntity)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{OhmMeterSettingLookupBuilder.TableName}] WHERE {NameOf(OhmMeterSettingLookupNub.MultimeterId)} = @Id", New With {Key .Id = meterModelId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return OhmMeterSettingLookupEntity.Populate(connection.Query(Of OhmMeterSettingLookupNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Dictionary of entity lookups. </summary>
    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, OhmMeterSettingLookupEntity)

    ''' <summary> The entity lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of Integer, OhmMeterSettingLookupEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, OhmMeterSettingLookupEntity)
        If Not (OhmMeterSettingLookupEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            OhmMeterSettingLookupEntity._EntityLookupDictionary = New Dictionary(Of Integer, OhmMeterSettingLookupEntity)
            For Each entity As OhmMeterSettingLookupEntity In OhmMeterSettingLookupEntity.OhmMeterSettingLookups
                OhmMeterSettingLookupEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return OhmMeterSettingLookupEntity._EntityLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (OhmMeterSettingLookupEntity.OhmMeterSettingLookups?.Any AndAlso
                OhmMeterSettingLookupEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False)
    End Function


#End Region

#Region " FIND "

    ''' <summary> Select ohm meter setting lookup entity. </summary>
    ''' <remarks> David, 5/31/2020. </remarks>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="nominalValue">  The nominal value. </param>
    ''' <returns> An OhmMeterSettingLookupEntity. </returns>
    Public Shared Function SelectOhmMeterSettingLookupEntity(ByVal toleranceCode As String, ByVal multimeterId As Integer, ByVal nominalValue As Double) As OhmMeterSettingLookupEntity
        Dim entity As OhmMeterSettingLookupEntity = New OhmMeterSettingLookupEntity
        For Each entity In OhmMeterSettingLookupEntity.OhmMeterSettingLookups
            If String.Equals(toleranceCode, entity.ToleranceCode) AndAlso multimeterId = entity.MultimeterId Then
                If entity.MinimumInclusiveValue.HasValue AndAlso entity.MaximumExclusiveValue.HasValue AndAlso
                        (entity.MaximumExclusiveValue.Value > nominalValue) AndAlso (entity.MinimumInclusiveValue.Value <= nominalValue) Then
                    Exit For
                ElseIf Not entity.MinimumInclusiveValue.HasValue AndAlso (entity.MaximumExclusiveValue.Value > nominalValue) Then
                    Exit For
                ElseIf Not entity.MaximumExclusiveValue.HasValue AndAlso (entity.MinimumInclusiveValue.Value <= nominalValue) Then
                    Exit For
                End If
            End If
        Next
        Return entity
    End Function

    ''' <summary> Builds where clause. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="value">         the nominal value. </param>
    ''' <returns> A String. </returns>
    Private Shared Function BuildWhereClause(ByVal toleranceCode As String, ByVal multimeterId As Integer, ByVal value As Double) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"WHERE ({NameOf(OhmMeterSettingLookupEntity.ToleranceCode)} = @{NameOf(toleranceCode)} AND ")
        queryBuilder.AppendLine($"{NameOf(OhmMeterSettingLookupEntity.MultimeterId)} = @{NameOf(multimeterId)} AND ")
        queryBuilder.AppendLine($"(({NameOf(OhmMeterSettingLookupEntity.MinimumInclusiveValue)} IS NULL AND {NameOf(OhmMeterSettingLookupEntity.MaximumExclusiveValue)} > @{NameOf(value)}) OR ")
        queryBuilder.AppendLine($"({NameOf(OhmMeterSettingLookupEntity.MinimumInclusiveValue)} <= @{NameOf(value)} AND {NameOf(OhmMeterSettingLookupEntity.MaximumExclusiveValue)} > @{NameOf(value)}) OR ")
        queryBuilder.AppendLine($"({NameOf(OhmMeterSettingLookupEntity.MinimumInclusiveValue)} <= @{NameOf(value)} AND {NameOf(OhmMeterSettingLookupEntity.MaximumExclusiveValue)} IS NULL) ) ); ")
        Return queryBuilder.ToString
    End Function

    ''' <summary> Count entities; returns number of records with the specified values. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="value">         the nominal value. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal toleranceCode As String, ByVal multimeterId As Integer,
                                         ByVal value As Double) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select count(*) from [{OhmMeterSettingLookupBuilder.TableName}] ")
        queryBuilder.Append(OhmMeterSettingLookupEntity.BuildWhereClause(toleranceCode, multimeterId, value))
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(queryBuilder.ToString, New With {toleranceCode, multimeterId, value})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="value">         the nominal value. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal toleranceCode As String, ByVal multimeterId As Integer,
                                     ByVal value As Double) As IEnumerable(Of OhmMeterSettingLookupNub)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select * from [{OhmMeterSettingLookupBuilder.TableName}] ")
        queryBuilder.Append(OhmMeterSettingLookupEntity.BuildWhereClause(toleranceCode, multimeterId, value))
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(queryBuilder.ToString, New With {toleranceCode, multimeterId, value})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of OhmMeterSettingLookupNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Meter exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="value">         the nominal value. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal toleranceCode As String,
                                    ByVal multimeterId As Integer, ByVal value As Double) As Boolean
        Return 1 = OhmMeterSettingLookupEntity.CountEntities(connection, toleranceCode, multimeterId, value)
    End Function

#End Region

#Region " RELATIONS "

#Region " TOLERANCE "

    ''' <summary> The tolerance entity. </summary>
    Private _ToleranceEntity As ToleranceEntity

    ''' <summary> Gets the meter model entity. </summary>
    ''' <value> The meter model entity. </value>
    Public ReadOnly Property ToleranceEntity As ToleranceEntity
        Get
            If Me._ToleranceEntity Is Nothing Then
                Me._ToleranceEntity = ToleranceEntity.EntityLookupDictionary(Me.ToleranceCode)
            End If
            Return Me._ToleranceEntity
        End Get
    End Property

    ''' <summary> Fetches the Tolerance entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchToleranceEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not ToleranceEntity.IsEnumerated Then ToleranceEntity.TryFetchAll(connection)
        Me._ToleranceEntity = ToleranceEntity.EntityLookupDictionary(Me.ToleranceCode)
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.ToleranceEntity))
        Return Me.ToleranceEntity IsNot Nothing
    End Function

#End Region

#Region " OHM METER SETTING "

    ''' <summary> Gets the selected Ohm Meter Setting entity. </summary>
    ''' <value> The selected Ohm Meter Settings entity. </value>
    Public ReadOnly Property OhmMeterSetting As OhmMeterSettingEntity

    ''' <summary> Fetches the Ohm Meter Setting. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchOhmMeterSetting(ByVal connection As System.Data.IDbConnection) As Boolean
        Dim entity As New OhmMeterSettingEntity()
        Dim result As Boolean = entity.FetchUsingKey(connection, Me.OhmMeterSettingId)
        Me._OhmMeterSetting = entity
        Me.NotifyPropertyChanged(NameOf(OhmMeterSettingLookupEntity.OhmMeterSetting))
        Return result
    End Function

#End Region

#Region " MULTIMETER "

    ''' <summary> The multimeter entity. </summary>
    Private _MultimeterEntity As MultimeterEntity

    ''' <summary> Gets the meter model entity. </summary>
    ''' <value> The meter model entity. </value>
    Public ReadOnly Property MultimeterEntity As MultimeterEntity
        Get
            If Me._MultimeterEntity Is Nothing Then
                Me._MultimeterEntity = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
            End If
            Return Me._MultimeterEntity
        End Get
    End Property

    ''' <summary> Fetches the meter. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchMultimeterEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not MultimeterEntity.IsEnumerated Then MultimeterEntity.TryFetchAll(connection)
        Me._MultimeterEntity = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.MultimeterEntity))
        Return Me._MultimeterEntity IsNot Nothing
    End Function

#End Region

#Region " METER MODEL "

    ''' <summary> The Meter Model entity. </summary>
    Private _MultimeterModelEntity As MultimeterModelEntity

    ''' <summary> Gets the Meter Model entity. </summary>
    ''' <value> The meter model entity. </value>
    Public ReadOnly Property MultimeterModelEntity As MultimeterModelEntity
        Get
            If Me._MultimeterModelEntity Is Nothing Then
                Me._MultimeterModelEntity = Me.MultimeterEntity.MultimeterModelEntity
            End If
            Return Me._MultimeterModelEntity
        End Get
    End Property

    ''' <summary> Fetches the Meter Model entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchMultimeterModel(ByVal connection As System.Data.IDbConnection) As Boolean
        Me.FetchMultimeterEntity(connection)
        Me.MultimeterEntity.FetchMultimeterModel(connection)
        Me._MultimeterModelEntity = Me.MultimeterEntity.MultimeterModelEntity
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.MeterModelEntity))
        Return Me.MultimeterModelEntity IsNot Nothing
    End Function

#End Region

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Ohm Meter Setting Lookup. </summary>
    ''' <value> Identifies the Ohm Meter Setting Lookup. </value>
    Public Property Id As Integer Implements IOhmMeterSettingLookup.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the tolerance code. </summary>
    ''' <value> The tolerance code. </value>
    Public Property ToleranceCode As String Implements IOhmMeterSettingLookup.ToleranceCode
        Get
            Return Me.ICache.ToleranceCode
        End Get
        Set(value As String)
            If Not String.Equals(Me.ToleranceCode, value) Then
                Me.ICache.ToleranceCode = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/>.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer Implements IOhmMeterSettingLookup.MultimeterId
        Get
            Return Me.ICache.MultimeterId
        End Get
        Set(value As Integer)
            If Not Integer?.Equals(Me.MultimeterId, value) Then
                Me.ICache.MultimeterId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the minimum inclusive value. </summary>
    ''' <value> The minimum inclusive value. </value>
    Public Property MinimumInclusiveValue As Double? Implements IOhmMeterSettingLookup.MinimumInclusiveValue
        Get
            Return Me.ICache.MinimumInclusiveValue
        End Get
        Set(value As Double?)
            If Not Double?.Equals(Me.MinimumInclusiveValue, value) Then
                Me.ICache.MinimumInclusiveValue = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the maximum exclusive value. </summary>
    ''' <value> The maximum exclusive value. </value>
    Public Property MaximumExclusiveValue As Double? Implements IOhmMeterSettingLookup.MaximumExclusiveValue
        Get
            Return Me.ICache.MaximumExclusiveValue
        End Get
        Set(value As Double?)
            If Not Double?.Equals(Me.MaximumExclusiveValue, value) Then
                Me.ICache.MaximumExclusiveValue = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the selected Ohm Meter Setting. </summary>
    ''' <value> Identifies the selected Ohm Meter Setting. </value>
    Public Property OhmMeterSettingId As Integer Implements IOhmMeterSettingLookup.OhmMeterSettingId
        Get
            Return Me.ICache.OhmMeterSettingId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.OhmMeterSettingId, value) Then
                Me.ICache.OhmMeterSettingId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">              The connection. </param>
    ''' <param name="ohmMeterSettingLookupId"> The entity unique key. </param>
    ''' <returns> A <see cref="OhmMeterSettingLookupEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal ohmMeterSettingLookupId As Integer) As (Success As Boolean, Details As String, Entity As OhmMeterSettingLookupEntity)
        Dim activity As String = String.Empty
        Dim entity As New OhmMeterSettingLookupEntity
        Try
            activity = $"Fetching {NameOf(OhmMeterSettingLookupEntity)} by {NameOf(OhmMeterSettingLookupNub.Id)} of {ohmMeterSettingLookupId}"
            Return If(entity.FetchUsingKey(connection, ohmMeterSettingLookupId),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, details As String, Entities As IEnumerable(Of
    ''' OhmMeterSettingLookupEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, details As String, Entities As IEnumerable(Of OhmMeterSettingLookupEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New OhmMeterSettingLookupEntity()
            activity = $"fetching all {NameOf(OhmMeterSettingLookupEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> OhmMeterSettingLookupEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(OhmMeterSettingLookupEntity.EntityLookupDictionary)} count must equal {NameOf(OhmMeterSettingLookupEntity.OhmMeterSettingLookups)} count ")
            End If
            Return (True, String.Empty, OhmMeterSettingLookupEntity.OhmMeterSettingLookups)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of OhmMeterSettingLookupEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

