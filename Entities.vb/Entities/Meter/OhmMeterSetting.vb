Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Core

Imports isr.Dapper.Entities.ExceptionExtensions
Imports isr.Dapper.Entities.ConnectionExtensions

''' <summary>
''' Interface for the Ohm Meter Setting nub and entity. Includes the fields as kept in the data
''' table. Allows tracking of property changes.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IOhmMeterSetting

    ''' <summary> Gets Identifies the Ohm Meter Setting. </summary>
    ''' <value> Identifies the Ohm Meter Setting. </value>
    <ExplicitKey>
    Property Id As Integer

    ''' <summary> Gets the Nplc. </summary>
    ''' <value> The Nplc. </value>
    Property Nplc As Double

    ''' <summary> Gets the number of averaging filters. </summary>
    ''' <value> The number of averaging filters. </value>
    Property AveragingFilterCount As Integer

    ''' <summary> Gets the averaging filter window. </summary>
    ''' <value> The averaging filter window. </value>
    Property AveragingFilterWindow As Double

    ''' <summary> Gets the start delay. </summary>
    ''' <value> The start delay. </value>
    Property StartDelay As Double

    ''' <summary> Gets the end delay. </summary>
    ''' <value> The end delay. </value>
    Property EndDelay As Double

End Interface

''' <summary> An ohm meter setting builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class OhmMeterSettingBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(OhmMeterSettingBuilder._TableName) Then
                OhmMeterSettingBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(OhmMeterSettingNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValues(ByVal connection As System.Data.IDbConnection) As Integer
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return InsertValues(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return InsertValues(sqlite)
        Return 0
    End Function

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValues(ByVal connection As System.Data.SQLite.SQLiteConnection) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim builder As New Text.StringBuilder()
        '                                                                                       Id 
        '                                                                                           Nplc
        '                                                                                              AveragingFilterCount 
        '                                                                                                  AveragingFilterWindow
        '                                                                                                         StartDelay 
        '                                                                                                               EndDelay
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (1,  1, 10, 0.001, 0.02, 0); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (2,  1,  1, 0    , 0.02, 0); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (3,  1,  1, 0    , 0.05, 0); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (4,  1,  1, 0    , 0.1 , 0); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (5,  1,  1, 0    , 0.2 , 0); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (6,  1,  1, 0    , 0.5 , 0); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (7,  1,  5, 0.001, 0.001, 0); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (8,  1,  1, 0    , 0.001, 0); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (9,  1,  1, 0    , 0.001, 0); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (10, 1,  1, 0    , 0.001, 0); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (11, 1,  1, 0    , 0.001, 0); ")
        builder.AppendLine($"INSERT OR IGNORE INTO [{OhmMeterSettingBuilder.TableName}] VALUES (12, 1,  1, 0    , 0.001, 0); ")
        Return connection.Execute(builder.ToString)
    End Function

    ''' <summary> Inserts or ignores the records using an SQL client connection. </summary>
    ''' <remarks> David, 3/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValuesSqlClient(ByVal connection As System.Data.IDbConnection) As Integer
        Return OhmMeterSettingBuilder.InsertValues(TryCast(connection, System.Data.SqlClient.SqlConnection))
    End Function

    ''' <summary> Inserts the values sq lite described by connection. </summary>
    ''' <remarks> David, 6/18/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValuesSQLite(ByVal connection As System.Data.IDbConnection) As Integer
        Return OhmMeterSettingBuilder.InsertValues(TryCast(connection, System.Data.SQLite.SQLiteConnection))
    End Function

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertValues(ByVal connection As System.Data.SqlClient.SqlConnection) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim builder As New Text.StringBuilder()
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=1) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (1,10,0.001,0.02,0,0); ")
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=2) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (2,1,0,0.02,0,0); ")
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=3) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (3,1,0,0.05,0,0); ")
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=4) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (4,1,0,0.1,0,0); ")
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=5) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (5,1,0,0.2,0,0); ")
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=6) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (6,1,0,0.5,0,0); ")
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=7) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (7,10,0.001,0.02,0,0); ")
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=8) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (8,1,0,0.02,0,0); ")
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=9) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (9,1,0,0.05,0,0); ")
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=10) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (10,1,0,0.1,0,0); ")
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=11) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (11,1,0,0.2,0,0); ")
        builder.AppendLine($"IF Not EXISTS(select * from [dbo].[{OhmMeterSettingBuilder.TableName}] where [{NameOf(OhmMeterSettingNub.Id)}]=12) INSERT INTO [{OhmMeterSettingBuilder.TableName}] VALUES (12,1,0,0.5,0,0); ")
        Return connection.Execute(builder.ToString)
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{OhmMeterSettingBuilder.TableName}] (
[{NameOf(OhmMeterSettingNub.Id)}] integer NOT NULL PRIMARY KEY, 
[{NameOf(OhmMeterSettingNub.Nplc)}] float NOT NULL, 
[{NameOf(OhmMeterSettingNub.AveragingFilterCount)}] integer NOT NULL, 
[{NameOf(OhmMeterSettingNub.AveragingFilterWindow)}] float NOT NULL, 
[{NameOf(OhmMeterSettingNub.StartDelay)}] float NOT NULL, 
[{NameOf(OhmMeterSettingNub.EndDelay)}] float NOT NULL); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmMeterSettingBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].{OhmMeterSettingBuilder.TableName}') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{OhmMeterSettingBuilder.TableName}](
	[{NameOf(OhmMeterSettingNub.Id)}] [int] NOT NULL,
	[{NameOf(OhmMeterSettingNub.Nplc)}] [float] NOT NULL,
	[{NameOf(OhmMeterSettingNub.AveragingFilterCount)}] [int] NOT NULL,
	[{NameOf(OhmMeterSettingNub.AveragingFilterWindow)}] [float] NOT NULL,
	[{NameOf(OhmMeterSettingNub.StartDelay)}] [float] NOT NULL,
	[{NameOf(OhmMeterSettingNub.EndDelay)}] [float] NOT NULL,
 CONSTRAINT [PK_{OhmMeterSettingBuilder.TableName})] PRIMARY KEY CLUSTERED 
([{NameOf(OhmMeterSettingNub.Id)}] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmMeterSettingBuilder.TableName
    End Function

End Class

''' <summary>
''' Implements the Ohm Meter Setting table <see cref="IOhmMeterSetting">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2019 </para>
''' </remarks>
<Table("OhmMeterSetting")>
Public Class OhmMeterSettingNub
    Inherits EntityNubBase(Of IOhmMeterSetting)
    Implements IOhmMeterSetting

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmMeterSetting
        Return New OhmMeterSettingNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmMeterSetting
        Dim destination As IOhmMeterSetting = Me.CreateNew
        OhmMeterSettingNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmMeterSetting)
        OhmMeterSettingNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IOhmMeterSetting, ByVal destination As IOhmMeterSetting)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.Nplc = source.Nplc
        destination.AveragingFilterCount = source.AveragingFilterCount
        destination.AveragingFilterWindow = source.AveragingFilterWindow
        destination.EndDelay = source.EndDelay
        destination.Id = source.Id
        destination.StartDelay = source.StartDelay
    End Sub

#End Region

#Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IOhmMeterSetting))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IOhmMeterSetting) As Boolean
        Return other IsNot Nothing AndAlso OhmMeterSettingNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IOhmMeterSetting, ByVal right As IOhmMeterSetting) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Double.Equals(left.Nplc, right.Nplc)
            result = result AndAlso Integer.Equals(left.AveragingFilterCount, right.AveragingFilterCount)
            result = result AndAlso Double.Equals(left.AveragingFilterWindow, right.AveragingFilterWindow)
            result = result AndAlso Double.Equals(left.EndDelay, right.EndDelay)
            result = result AndAlso Integer.Equals(left.Id, right.Id)
            result = result AndAlso Double.Equals(left.StartDelay, right.StartDelay)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.Nplc.GetHashCode() Xor Me.AveragingFilterCount Xor Me.AveragingFilterWindow.GetHashCode() Xor EndDelay.GetHashCode() Xor Me.Id Xor Me.StartDelay.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets Identifies the Ohm Meter Setting. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the Ohm Meter Setting. </value>
    <ExplicitKey>
    Public Property Id As Integer Implements IOhmMeterSetting.Id

    ''' <summary> Gets or sets the Nplc. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Nplc. </value>
    Public Property Nplc As Double Implements IOhmMeterSetting.Nplc

    ''' <summary> Gets or sets the number of averaging filters. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The number of averaging filters. </value>
    Public Property AveragingFilterCount As Integer Implements IOhmMeterSetting.AveragingFilterCount

    ''' <summary> Gets or sets the averaging filter window. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The averaging filter window. </value>
    Public Property AveragingFilterWindow As Double Implements IOhmMeterSetting.AveragingFilterWindow

    ''' <summary> Gets or sets the start delay. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The start delay. </value>
    Public Property StartDelay As Double Implements IOhmMeterSetting.StartDelay

    ''' <summary> Gets or sets the end delay. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The end delay. </value>
    Public Property EndDelay As Double Implements IOhmMeterSetting.EndDelay

#End Region

End Class

''' <summary>
''' the Ohm Meter Setting Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2019 </para>
''' </remarks>
Public Class OhmMeterSettingEntity
    Inherits EntityBase(Of IOhmMeterSetting, OhmMeterSettingNub)
    Implements IOhmMeterSetting

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New OhmMeterSettingNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The OhmMeterSetting interface. </param>
    Public Sub New(ByVal value As IOhmMeterSetting)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOhmMeterSetting, ByVal store As IOhmMeterSetting)
        MyBase.New(New OhmMeterSettingNub, cache, store)
        Me.UsingNativeTracking = String.Equals(OhmMeterSettingBuilder.TableName, NameOf(IOhmMeterSetting).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmMeterSetting
        Return New OhmMeterSettingNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmMeterSetting
        Dim destination As IOhmMeterSetting = Me.CreateNew
        OhmMeterSettingNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmMeterSetting)
        OhmMeterSettingNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The OhmMeterSetting interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOhmMeterSetting)
        ' first make the copy to notify of any property change.
        OhmMeterSettingNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The OhmMeterSetting table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IOhmMeterSetting)(key), connection.Get(Of OhmMeterSettingNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="ohmMeterSettingId"> Identifies the Ohm Meter Setting. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal ohmMeterSettingId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, ohmMeterSettingId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOhmMeterSetting) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If OhmMeterSettingEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.Id) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of OhmMeterSettingNub)(New OhmMeterSettingNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the Guard Band Lookup entities. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> the Guard Band Lookup entities. </value>
    Public Shared ReadOnly Property OhmMeterSettings As IEnumerable(Of OhmMeterSettingEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of OhmMeterSettingEntity)
        Return If(usingNativeTracking, OhmMeterSettingEntity.Populate(connection.GetAll(Of IOhmMeterSetting)), OhmMeterSettingEntity.Populate(connection.GetAll(Of OhmMeterSettingNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        OhmMeterSettingEntity._OhmMeterSettings = OhmMeterSettingEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(OhmMeterSettingEntity.OhmMeterSettings))
        Return If(OhmMeterSettingEntity.OhmMeterSettings?.Any, OhmMeterSettingEntity.OhmMeterSettings.Count, 0)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 4/28/2020. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="ohmMeterSettingId"> Identifies the Ohm Meter Setting. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal ohmMeterSettingId As Integer) As IEnumerable(Of OhmMeterSettingEntity)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{OhmMeterSettingBuilder.TableName}] WHERE {NameOf(OhmMeterSettingNub.Id)} = @Id", New With {Key .Id = ohmMeterSettingId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return OhmMeterSettingEntity.Populate(connection.Query(Of OhmMeterSettingNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Populates a list of OhmMeterSetting entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The OhmMeterSetting nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of OhmMeterSettingNub)) As IEnumerable(Of OhmMeterSettingEntity)
        Dim l As New List(Of OhmMeterSettingEntity)
        If nubs?.Any Then
            For Each nub As OhmMeterSettingNub In nubs
                l.Add(New OhmMeterSettingEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of OhmMeterSetting entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The OhmMeterSetting interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOhmMeterSetting)) As IEnumerable(Of OhmMeterSettingEntity)
        Dim l As New List(Of OhmMeterSettingEntity)
        If interfaces?.Any Then
            Dim nub As New OhmMeterSettingNub
            For Each iFace As IOhmMeterSetting In interfaces
                nub.CopyFrom(iFace)
                l.Add(New OhmMeterSettingEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Count entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection.CountEntities(OhmMeterSettingBuilder.TableName)
    End Function

    ''' <summary> Dictionary of entity lookups. </summary>
    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, OhmMeterSettingEntity)

    ''' <summary> The entity lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of Integer, OhmMeterSettingEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, OhmMeterSettingEntity)
        If Not (OhmMeterSettingEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            OhmMeterSettingEntity._EntityLookupDictionary = New Dictionary(Of Integer, OhmMeterSettingEntity)
            For Each entity As OhmMeterSettingEntity In OhmMeterSettingEntity.OhmMeterSettings
                OhmMeterSettingEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return OhmMeterSettingEntity._EntityLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (OhmMeterSettingEntity.OhmMeterSettings?.Any AndAlso
                OhmMeterSettingEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " OHM METER SETTING "

    ''' <summary> Gets or sets the Ohm Meter Setting entity. </summary>
    ''' <value> The Ohm Meter Setting entity. </value>
    Public ReadOnly Property OhmMeterSetting As OhmMeterSettingEntity

    ''' <summary> Fetches the Ohm Meter Setting entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchOhmMeterSetting(ByVal connection As System.Data.IDbConnection) As Boolean
        Dim entity As New OhmMeterSettingEntity()
        Dim result As Boolean = entity.FetchUsingKey(connection, Me.Id)
        Me._OhmMeterSetting = entity
        Me.NotifyPropertyChanged(NameOf(OhmMeterSettingEntity.OhmMeterSetting))
        Return result
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Ohm Meter Setting. </summary>
    ''' <value> Identifies the Ohm Meter Setting. </value>
    Public Property Id As Integer Implements IOhmMeterSetting.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Nplc. </summary>
    ''' <value> The Nplc. </value>
    Public Property Nplc As Double Implements IOhmMeterSetting.Nplc
        Get
            Return Me.ICache.Nplc
        End Get
        Set(value As Double)
            If Not Double.Equals(Me.Nplc, value) Then
                Me.ICache.Nplc = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the number of averaging filters. </summary>
    ''' <value> The number of averaging filters. </value>
    Public Property AveragingFilterCount As Integer Implements IOhmMeterSetting.AveragingFilterCount
        Get
            Return Me.ICache.AveragingFilterCount
        End Get
        Set(value As Integer)
            If Not Double.Equals(Me.AveragingFilterCount, value) Then
                Me.ICache.AveragingFilterCount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the averaging filter window. </summary>
    ''' <value> The averaging filter window. </value>
    Public Property AveragingFilterWindow As Double Implements IOhmMeterSetting.AveragingFilterWindow
        Get
            Return Me.ICache.AveragingFilterWindow
        End Get
        Set(value As Double)
            If Not Double.Equals(Me.AveragingFilterWindow, value) Then
                Me.ICache.AveragingFilterWindow = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the start delay. </summary>
    ''' <value> The start delay. </value>
    Public Property StartDelay As Double Implements IOhmMeterSetting.StartDelay
        Get
            Return Me.ICache.StartDelay
        End Get
        Set(value As Double)
            If Not Double.Equals(Me.StartDelay, value) Then
                Me.ICache.StartDelay = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the end delay. </summary>
    ''' <value> The end delay. </value>
    Public Property EndDelay As Double Implements IOhmMeterSetting.EndDelay
        Get
            Return Me.ICache.EndDelay
        End Get
        Set(value As Double)
            If Not Double.Equals(Me.EndDelay, value) Then
                Me.ICache.EndDelay = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="ohmMeterSettingId"> The entity unique key. </param>
    ''' <returns> A <see cref="OhmMeterSettingEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal ohmMeterSettingId As Integer) As (Success As Boolean, Details As String, Entity As OhmMeterSettingEntity)
        Dim activity As String = String.Empty
        Dim entity As New OhmMeterSettingEntity
        Try
            activity = $"Fetching {NameOf(OhmMeterSettingEntity)} by {NameOf(OhmMeterSettingNub.Id)} of {ohmMeterSettingId}"
            Return If(entity.FetchUsingKey(connection, ohmMeterSettingId),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, details As String, Entities As IEnumerable(Of OhmMeterSettingEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, details As String, Entities As IEnumerable(Of OhmMeterSettingEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New OhmMeterSettingEntity()
            activity = $"fetching all {NameOf(OhmMeterSettingEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> OhmMeterSettingEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(OhmMeterSettingEntity.EntityLookupDictionary)} count must equal {NameOf(OhmMeterSettingEntity.OhmMeterSettings)} count ")
            End If
            Return (True, String.Empty, OhmMeterSettingEntity.OhmMeterSettings)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of OhmMeterSettingEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Collection of ohm meter setting entities. </summary>
''' <remarks> David, 6/2/2020. </remarks>
Public Class OhmMeterSettingEntityCollection
    Inherits EntityKeyedCollection(Of Integer, IOhmMeterSetting, OhmMeterSettingNub, OhmMeterSettingEntity)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified OhmMeterSetting.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The OhmMeterSetting from which to extract the key. </param>
    ''' <returns> The key for the specified OhmMeterSetting. </returns>
    Protected Overrides Function GetKeyForItem(ByVal item As OhmMeterSettingEntity) As Integer
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.Id
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As OhmMeterSettingEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all OhmMeterSettings from the
    ''' <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of OhmMeterSettingEntity))
        If entities?.Any Then
            For Each entity As OhmMeterSettingEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Throw New NotImplementedException()
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/2/2020. </remarks>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="nominalValue">  The nominal value. </param>
    Public Overloads Sub Add(ByVal toleranceCode As String, ByVal multimeterId As Integer, ByVal nominalValue As Double)
        ' assume the setting lookup entities were fetched
        Me.Add(OhmMeterSettingEntity.EntityLookupDictionary(OhmMeterSettingLookupEntity.SelectOhmMeterSettingLookupEntity(toleranceCode, multimeterId,
                                                                                                         nominalValue).OhmMeterSettingId))
    End Sub

End Class

