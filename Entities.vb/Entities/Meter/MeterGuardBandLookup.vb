Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary>
''' Interface for the Meter Guard Band Lookup nub and entity. Includes the fields as kept in the
''' data table. Allows tracking of property changes.
''' </summary>
''' <remarks>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value. <para>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IMeterGuardBandLookup

    ''' <summary> Gets the identifier of the GuardBandLookup. </summary>
    ''' <value> Identifies the GuardBandLookup. </value>
    <ExplicitKey>
    Property Id As Integer

    ''' <summary> Gets the tolerance code. </summary>
    ''' <value> The tolerance code. </value>
    Property ToleranceCode As String

    ''' <summary> Gets Identifies the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Property MultimeterId As Integer

    ''' <summary> Gets Identifies the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NomTypeEntity"/>. </value>
    Property NomTypeId As Integer

    ''' <summary> Gets the minimum inclusive value. </summary>
    ''' <value> The minimum inclusive value. </value>
    Property MinimumInclusiveValue As Double?

    ''' <summary> Gets the maximum exclusive value. </summary>
    ''' <value> The maximum exclusive value. </value>
    Property MaximumExclusiveValue As Double?

    ''' <summary> Gets the guard band factor. </summary>
    ''' <value> The guard band factor. </value>
    Property GuardBandFactor As Double

End Interface

''' <summary> A meter guard band lookup builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class MeterGuardBandLookupBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(MeterGuardBandLookupBuilder._TableName) Then
                MeterGuardBandLookupBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(MeterGuardBandLookupNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the Multimeter table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Multimeter table. </value>
    Public Shared Property MultimeterTableName As String = MultimeterBuilder.TableName

    ''' <summary> Gets the name of the Multimeter table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Multimeter table key. </value>
    Public Shared Property MultimeterTableKeyName As String = NameOf(MultimeterNub.Id)

    ''' <summary> Gets the name of the Nominal (measurement) type table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Nominal type table. </value>
    Public Shared Property NomTypeTableName As String = NomTypeBuilder.TableName

    ''' <summary> Gets the name of the Nominal (measurement) type table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Nominal type table key. </value>
    Public Shared Property NomTypeTableKeyName As String = NameOf(NomTypeNub.Id)

    ''' <summary> Inserts values from file. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="fileName">   Filename of the file. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertFileValues(ByVal connection As System.Data.IDbConnection, ByVal fileName As String) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        If fileName Is Nothing Then Throw New ArgumentNullException(NameOf(fileName))
        Return connection.Execute(System.IO.File.ReadAllText(fileName))
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{MeterGuardBandLookupBuilder.TableName}] (
[{NameOf(MeterGuardBandLookupNub.Id)}] integer NOT NULL PRIMARY KEY, 
[{NameOf(MeterGuardBandLookupNub.ToleranceCode)}] [nchar](1) NOT NULL,
[{NameOf(MeterGuardBandLookupNub.MultimeterId)}] integer NOT NULL DEFAULT (1),
[{NameOf(MeterGuardBandLookupNub.NomTypeId)}] integer NOT NULL DEFAULT (1), 
[{NameOf(MeterGuardBandLookupNub.MinimumInclusiveValue)}] [float] NULL,
[{NameOf(MeterGuardBandLookupNub.MaximumExclusiveValue)}] [float] NULL,
[{NameOf(MeterGuardBandLookupNub.GuardBandFactor)}] [float] NOT NULL,
FOREIGN KEY ([{NameOf(MeterGuardBandLookupNub.NomTypeId)}]) REFERENCES [{MeterGuardBandLookupBuilder.NomTypeTableName}] ([{MeterGuardBandLookupBuilder.NomTypeTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE, 
FOREIGN KEY ([{NameOf(MeterGuardBandLookupNub.ToleranceCode)}]) REFERENCES [{ToleranceBuilder.TableName}] ([{NameOf(ToleranceNub.ToleranceCode)}]) ON UPDATE CASCADE ON DELETE CASCADE, 
FOREIGN KEY ([{NameOf(MeterGuardBandLookupNub.MultimeterId)}]) REFERENCES [{MeterGuardBandLookupBuilder.MultimeterTableName}] ([{MeterGuardBandLookupBuilder.MultimeterTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return MeterGuardBandLookupBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{MeterGuardBandLookupBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{MeterGuardBandLookupBuilder.TableName}](
	[{NameOf(MeterGuardBandLookupNub.Id)}] [int] NOT NULL,
	[{NameOf(MeterGuardBandLookupNub.ToleranceCode)}] [nchar](1) NOT NULL,
	[{NameOf(MeterGuardBandLookupNub.MultimeterId)}] [int] NOT NULL,
	[{NameOf(MeterGuardBandLookupNub.NomTypeId)}] [int] NOT NULL,
	[{NameOf(MeterGuardBandLookupNub.MinimumInclusiveValue)}] [float] NULL,
	[{NameOf(MeterGuardBandLookupNub.MaximumExclusiveValue)}] [float] NULL,
	[{NameOf(MeterGuardBandLookupNub.GuardBandFactor)}] [float] NOT NULL,
 CONSTRAINT [PK_{MeterGuardBandLookupBuilder.TableName}] PRIMARY KEY CLUSTERED 
(
	[{NameOf(MeterGuardBandLookupNub.Id)}] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_{MeterGuardBandLookupBuilder.TableName}_{NameOf(MeterGuardBandLookupNub.MultimeterId)}]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[{MeterGuardBandLookupBuilder.TableName}] ADD  CONSTRAINT [DF_{MeterGuardBandLookupBuilder.TableName}_{NameOf(MeterGuardBandLookupNub.MultimeterId)}]  DEFAULT ((1)) FOR [{NameOf(MeterGuardBandLookupNub.MultimeterId)}]
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_{MeterGuardBandLookupBuilder.TableName}_{NameOf(MeterGuardBandLookupNub.NomTypeId)}]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[{MeterGuardBandLookupBuilder.TableName}] ADD  CONSTRAINT [DF_{MeterGuardBandLookupBuilder.TableName}_{NameOf(MeterGuardBandLookupNub.NomTypeId)}]  DEFAULT ((1)) FOR [{NameOf(MeterGuardBandLookupNub.NomTypeId)}]
END;

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{MeterGuardBandLookupBuilder.TableName}_{MeterGuardBandLookupBuilder.NomTypeTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{MeterGuardBandLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{MeterGuardBandLookupBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{MeterGuardBandLookupBuilder.TableName}_{MeterGuardBandLookupBuilder.NomTypeTableName}] FOREIGN KEY([{NameOf(MeterGuardBandLookupNub.NomTypeId)}])
REFERENCES [dbo].[{MeterGuardBandLookupBuilder.NomTypeTableName}] ([{MeterGuardBandLookupBuilder.NomTypeTableKeyName}])
ON UPDATE CASCADE
ON DELETE CASCADE;

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{MeterGuardBandLookupBuilder.TableName}_{MeterGuardBandLookupBuilder.NomTypeTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{MeterGuardBandLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{MeterGuardBandLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{MeterGuardBandLookupBuilder.TableName}_{MeterGuardBandLookupBuilder.NomTypeTableName}];

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{MeterGuardBandLookupBuilder.TableName}_{MeterGuardBandLookupBuilder.MultimeterTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{MeterGuardBandLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{MeterGuardBandLookupBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{MeterGuardBandLookupBuilder.TableName}_{MeterGuardBandLookupBuilder.MultimeterTableName}] FOREIGN KEY([{NameOf(MeterGuardBandLookupNub.MultimeterId)}])
REFERENCES [dbo].[{MeterGuardBandLookupBuilder.MultimeterTableName}] ([{MeterGuardBandLookupBuilder.MultimeterTableKeyName}]);

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{MeterGuardBandLookupBuilder.TableName}_{MeterGuardBandLookupBuilder.MultimeterTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{MeterGuardBandLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{MeterGuardBandLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{MeterGuardBandLookupBuilder.TableName}_{MeterGuardBandLookupBuilder.MultimeterTableName}];

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{MeterGuardBandLookupBuilder.TableName}_{ToleranceBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{MeterGuardBandLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{MeterGuardBandLookupBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{MeterGuardBandLookupBuilder.TableName}_{ToleranceBuilder.TableName}] FOREIGN KEY([{NameOf(MeterGuardBandLookupNub.ToleranceCode)}])
REFERENCES [dbo].[{ToleranceBuilder.TableName}] ([{NameOf(MeterGuardBandLookupNub.ToleranceCode)}])
ON UPDATE CASCADE
ON DELETE CASCADE;

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{MeterGuardBandLookupBuilder.TableName}_{ToleranceBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{MeterGuardBandLookupBuilder.TableName}]'))
ALTER TABLE [dbo].[{MeterGuardBandLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{MeterGuardBandLookupBuilder.TableName}_{ToleranceBuilder.TableName}]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return MeterGuardBandLookupBuilder.TableName
    End Function

End Class

''' <summary>
''' Implements the Meter Guard Band Lookup table
''' <see cref="IMeterGuardBandLookup">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2019 </para>
''' </remarks>
<Table("MeterGuardBandLookup")>
Public Class MeterGuardBandLookupNub
    Inherits EntityNubBase(Of IMeterGuardBandLookup)
    Implements IMeterGuardBandLookup

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IMeterGuardBandLookup
        Return New MeterGuardBandLookupNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IMeterGuardBandLookup
        Dim destination As IMeterGuardBandLookup = Me.CreateNew
        MeterGuardBandLookupNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IMeterGuardBandLookup)
        MeterGuardBandLookupNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IMeterGuardBandLookup, ByVal destination As IMeterGuardBandLookup)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.GuardBandFactor = source.GuardBandFactor
        destination.Id = source.Id
        destination.MaximumExclusiveValue = source.MaximumExclusiveValue
        destination.NomTypeId = source.NomTypeId
        destination.MultimeterId = source.MultimeterId
        destination.MinimumInclusiveValue = source.MinimumInclusiveValue
        destination.ToleranceCode = source.ToleranceCode
    End Sub

#End Region

#Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IMeterGuardBandLookup))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IMeterGuardBandLookup) As Boolean
        Return other IsNot Nothing AndAlso MeterGuardBandLookupNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IMeterGuardBandLookup, ByVal right As IMeterGuardBandLookup) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Double.Equals(left.GuardBandFactor, right.GuardBandFactor)
            result = result AndAlso Integer.Equals(left.Id, right.Id)
            result = result AndAlso String.Equals(left.ToleranceCode, right.ToleranceCode)
            result = result AndAlso Integer.Equals(left.NomTypeId, right.NomTypeId)
            result = result AndAlso Double?.Equals(left.MinimumInclusiveValue, right.MinimumInclusiveValue)
            result = result AndAlso Double?.Equals(left.MaximumExclusiveValue, right.MaximumExclusiveValue)
            result = result AndAlso Integer.Equals(left.MultimeterId, right.MultimeterId)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.GuardBandFactor.GetHashCode() Xor Me.Id Xor Me.ToleranceCode.GetHashCode() Xor Me.NomTypeId Xor Me.MinimumInclusiveValue.GetHashCode() Xor Me.MaximumExclusiveValue.GetHashCode() Xor Me.MultimeterId
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets the identifier of the GuardBandLookup. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the GuardBandLookup. </value>
    <ExplicitKey>
    Public Property Id As Integer Implements IMeterGuardBandLookup.Id

    ''' <summary> Gets the tolerance code. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The tolerance code. </value>
    Public Property ToleranceCode As String Implements IMeterGuardBandLookup.ToleranceCode

    ''' <summary> Gets Identifies the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer Implements IMeterGuardBandLookup.MultimeterId

    ''' <summary> Gets Identifies the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the <see cref="Entities.NomTypeEntity"/>. </value>
    Public Property NomTypeId As Integer Implements IMeterGuardBandLookup.NomTypeId

    ''' <summary> Gets the minimum inclusive value. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The minimum inclusive value. </value>
    Public Property MinimumInclusiveValue As Double? Implements IMeterGuardBandLookup.MinimumInclusiveValue

    ''' <summary> Gets the maximum exclusive value. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The maximum exclusive value. </value>
    Public Property MaximumExclusiveValue As Double? Implements IMeterGuardBandLookup.MaximumExclusiveValue

    ''' <summary> Gets the guard band factor. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The guard band factor. </value>
    Public Property GuardBandFactor As Double Implements IMeterGuardBandLookup.GuardBandFactor

#End Region

End Class

''' <summary>
''' The Meter Guard Band Lookup Entity implementing the <see cref="IMeterGuardBandLookup"/>
''' interface.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2019 </para>
''' </remarks>
Public Class MeterGuardBandLookupEntity
    Inherits EntityBase(Of IMeterGuardBandLookup, MeterGuardBandLookupNub)
    Implements IMeterGuardBandLookup

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New MeterGuardBandLookupNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The GuardBandLookup interface. </param>
    Public Sub New(ByVal value As IMeterGuardBandLookup)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IMeterGuardBandLookup, ByVal store As IMeterGuardBandLookup)
        MyBase.New(New MeterGuardBandLookupNub, cache, store)
        Me.UsingNativeTracking = String.Equals(MeterGuardBandLookupBuilder.TableName, NameOf(IMeterGuardBandLookup).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IMeterGuardBandLookup
        Return New MeterGuardBandLookupNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IMeterGuardBandLookup
        Dim destination As IMeterGuardBandLookup = Me.CreateNew
        MeterGuardBandLookupNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IMeterGuardBandLookup)
        MeterGuardBandLookupNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The GuardBandLookup interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IMeterGuardBandLookup)
        ' first make the copy to notify of any property change.
        MeterGuardBandLookupNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The GuardBandLookup table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IMeterGuardBandLookup)(key), connection.Get(Of MeterGuardBandLookupNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="guardBandLookupId"> Identifier for the guard band lookup. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal guardBandLookupId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, guardBandLookupId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IMeterGuardBandLookup) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If MeterGuardBandLookupEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.Id) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of MeterGuardBandLookupNub)(New MeterGuardBandLookupNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets the Meter Guard Band Lookup entities. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> the Meter Guard Band Lookup entities. </value>
    Public Shared ReadOnly Property MeterGuardBandLookups As IEnumerable(Of MeterGuardBandLookupEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of MeterGuardBandLookupEntity)
        Return If(usingNativeTracking, MeterGuardBandLookupEntity.Populate(connection.GetAll(Of IMeterGuardBandLookup)), MeterGuardBandLookupEntity.Populate(connection.GetAll(Of MeterGuardBandLookupNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        MeterGuardBandLookupEntity._MeterGuardBandLookups = MeterGuardBandLookupEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.MeterGuardBandLookups))
        Return If(MeterGuardBandLookupEntity.MeterGuardBandLookups?.Any, MeterGuardBandLookupEntity.MeterGuardBandLookups.Count, 0)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="meterModelId"> Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal meterModelId As Integer) As IEnumerable(Of MeterGuardBandLookupEntity)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{MeterGuardBandLookupBuilder.TableName}] WHERE {NameOf(MeterGuardBandLookupNub.MultimeterId)} = @Id", New With {Key .Id = meterModelId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return MeterGuardBandLookupEntity.Populate(connection.Query(Of MeterGuardBandLookupNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Populates a list of GuardBandLookup entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The GuardBandLookup nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of MeterGuardBandLookupNub)) As IEnumerable(Of MeterGuardBandLookupEntity)
        Dim l As New List(Of MeterGuardBandLookupEntity)
        If nubs?.Any Then
            For Each nub As MeterGuardBandLookupNub In nubs
                l.Add(New MeterGuardBandLookupEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of GuardBandLookup entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The GuardBandLookup interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IMeterGuardBandLookup)) As IEnumerable(Of MeterGuardBandLookupEntity)
        Dim l As New List(Of MeterGuardBandLookupEntity)
        If interfaces?.Any Then
            Dim nub As New MeterGuardBandLookupNub
            For Each iFace As IMeterGuardBandLookup In interfaces
                nub.CopyFrom(iFace)
                l.Add(New MeterGuardBandLookupEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Dictionary of entity lookups. </summary>
    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, MeterGuardBandLookupEntity)

    ''' <summary> The entity lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of Integer, MeterGuardBandLookupEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, MeterGuardBandLookupEntity)
        If Not (MeterGuardBandLookupEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            MeterGuardBandLookupEntity._EntityLookupDictionary = New Dictionary(Of Integer, MeterGuardBandLookupEntity)
            For Each entity As MeterGuardBandLookupEntity In MeterGuardBandLookupEntity.MeterGuardBandLookups
                MeterGuardBandLookupEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return MeterGuardBandLookupEntity._EntityLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (MeterGuardBandLookupEntity.MeterGuardBandLookups?.Any AndAlso
                MeterGuardBandLookupEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Select meter guard band. </summary>
    ''' <remarks> David, 5/31/2020. </remarks>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multmeterId">   Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="nominalValue">  The nominal value. </param>
    ''' <returns> A MeterGuardBandLookupEntity matching the provided arguments. </returns>
    Public Shared Function SelectMeterGuardBandEntity(ByVal toleranceCode As String, ByVal multmeterId As Integer,
                                                      ByVal nomTypeId As Integer, ByVal nominalValue As Double) As MeterGuardBandLookupEntity
        Dim entity As MeterGuardBandLookupEntity = New MeterGuardBandLookupEntity
        For Each entity In MeterGuardBandLookupEntity.MeterGuardBandLookups
            If String.Equals(toleranceCode, entity.ToleranceCode) AndAlso multmeterId = entity.MultimeterId AndAlso nomTypeId = entity.NomTypeId Then
                If entity.MinimumInclusiveValue.HasValue AndAlso entity.MaximumExclusiveValue.HasValue AndAlso
                        (entity.MaximumExclusiveValue.Value < nominalValue) AndAlso (entity.MinimumInclusiveValue.Value >= nominalValue) Then
                    Exit For
                ElseIf entity.MinimumInclusiveValue.HasValue AndAlso (entity.MinimumInclusiveValue.Value <= nominalValue) Then
                    Exit For
                ElseIf entity.MaximumExclusiveValue.HasValue AndAlso (entity.MaximumExclusiveValue.Value >= nominalValue) Then
                    Exit For
                End If
            End If
        Next
        Return entity
    End Function

    ''' <summary> Builds where clause. </summary>
    ''' <remarks> David, 6/17/2020. </remarks>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="value">         the Meter Guard Band Lookup interface. </param>
    ''' <returns> A String. </returns>
    Private Shared Function BuildWhereClause(ByVal toleranceCode As String, ByVal multimeterId As Integer,
                                             ByVal nomTypeId As Integer, ByVal value As Double) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"WHERE ({NameOf(MeterGuardBandLookupEntity.ToleranceCode)} = @{NameOf(toleranceCode)} AND ")
        queryBuilder.AppendLine($"{NameOf(MeterGuardBandLookupEntity.MultimeterId)} = @{NameOf(multimeterId)} AND ")
        queryBuilder.AppendLine($"{NameOf(MeterGuardBandLookupEntity.NomTypeId)} = @{NameOf(nomTypeId)} AND ")
        queryBuilder.AppendLine($"(({NameOf(MeterGuardBandLookupEntity.MinimumInclusiveValue)} IS NULL AND {NameOf(MeterGuardBandLookupEntity.MaximumExclusiveValue)} > @{NameOf(value)}) OR ")
        queryBuilder.AppendLine($"({NameOf(MeterGuardBandLookupEntity.MinimumInclusiveValue)} <= @{NameOf(value)} AND {NameOf(MeterGuardBandLookupEntity.MaximumExclusiveValue)} > @{NameOf(value)}) OR ")
        queryBuilder.AppendLine($"({NameOf(MeterGuardBandLookupEntity.MinimumInclusiveValue)} <= @{NameOf(value)} AND {NameOf(MeterGuardBandLookupEntity.MaximumExclusiveValue)} IS NULL) ) ); ")
        Return queryBuilder.ToString
    End Function

    ''' <summary> Count entities; returns number of records with the specified values. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="value">         the Meter Guard Band Lookup interface. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal toleranceCode As String,
                                         ByVal multimeterId As Integer, ByVal nomTypeId As Integer, ByVal value As Double) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select count(*) from [{MeterGuardBandLookupBuilder.TableName}] ")
        queryBuilder.Append(MeterGuardBandLookupEntity.BuildWhereClause(toleranceCode, multimeterId, nomTypeId, value))
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(queryBuilder.ToString,
                                                                     New With {toleranceCode, multimeterId, nomTypeId, value})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="value">         the Meter Guard Band Lookup interface. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal toleranceCode As String,
                                     ByVal multimeterId As Integer, ByVal nomTypeId As Integer, ByVal value As Double) As IEnumerable(Of MeterGuardBandLookupNub)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select * from [{MeterGuardBandLookupBuilder.TableName}] ")
        queryBuilder.Append(MeterGuardBandLookupEntity.BuildWhereClause(toleranceCode, multimeterId, nomTypeId, value))
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(queryBuilder.ToString,
                                                                     New With {toleranceCode, multimeterId, nomTypeId, value})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of MeterGuardBandLookupNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Meter exists. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifies the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="value">         the Meter Guard Band Lookup interface. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal toleranceCode As String,
                                    ByVal multimeterId As Integer, ByVal nomTypeId As Integer, ByVal value As Double) As Boolean
        Return 1 = MeterGuardBandLookupEntity.CountEntities(connection, toleranceCode, multimeterId, nomTypeId, value)
    End Function

#End Region

#Region " RELATIONS "

#Region " TOLERANCE "

    ''' <summary> The tolerance entity. </summary>
    Private _ToleranceEntity As ToleranceEntity

    ''' <summary> Gets the meter model entity. </summary>
    ''' <value> The meter model entity. </value>
    Public ReadOnly Property ToleranceEntity As ToleranceEntity
        Get
            If Me._ToleranceEntity Is Nothing Then
                Me._ToleranceEntity = ToleranceEntity.EntityLookupDictionary(Me.ToleranceCode)
            End If
            Return Me._ToleranceEntity
        End Get
    End Property

    ''' <summary> Fetches the Tolerance entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchToleranceEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not ToleranceEntity.IsEnumerated Then ToleranceEntity.TryFetchAll(connection)
        Me._ToleranceEntity = ToleranceEntity.EntityLookupDictionary(Me.ToleranceCode)
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.ToleranceEntity))
        Return Me.ToleranceEntity IsNot Nothing
    End Function

#End Region

#Region " MULTIMETER "

    ''' <summary> The multimeter entity. </summary>
    Private _MultimeterEntity As MultimeterEntity

    ''' <summary> Gets the meter model entity. </summary>
    ''' <value> The meter model entity. </value>
    Public ReadOnly Property MultimeterEntity As MultimeterEntity
        Get
            If Me._MultimeterEntity Is Nothing Then
                Me._MultimeterEntity = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
            End If
            Return Me._MultimeterEntity
        End Get
    End Property

    ''' <summary> Fetches the meter. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchMultimeterEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not MultimeterEntity.IsEnumerated Then MultimeterEntity.TryFetchAll(connection)
        Me._MultimeterEntity = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.MultimeterEntity))
        Return Me._MultimeterEntity IsNot Nothing
    End Function

#End Region

#Region " METER MODEL "

    ''' <summary> The meter model entity. </summary>
    Private _MeterModelEntity As MultimeterModelEntity

    ''' <summary> Gets the meter model entity. </summary>
    ''' <value> The meter model entity. </value>
    Public ReadOnly Property MeterModelEntity As MultimeterModelEntity
        Get
            If Me._MeterModelEntity Is Nothing Then
                Me._MeterModelEntity = Me.MultimeterEntity.MultimeterModelEntity
            End If
            Return Me._MeterModelEntity
        End Get
    End Property

    ''' <summary> Fetches the meter model entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchMeterModel(ByVal connection As System.Data.IDbConnection) As Boolean
        Me.FetchMultimeterEntity(connection)
        Me.MultimeterEntity.FetchMultimeterModel(connection)
        Me._MeterModelEntity = Me.MultimeterEntity.MultimeterModelEntity
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.MeterModelEntity))
        Return Me.MeterModelEntity IsNot Nothing
    End Function

#End Region

#Region " NOMINAL TYPE "

    ''' <summary> The nom type entity. </summary>
    Private _NomTypeEntity As NomTypeEntity

    ''' <summary> Gets the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> The <see cref="Entities.NomTypeEntity"/>. </value>
    Public ReadOnly Property NomTypeEntity As NomTypeEntity
        Get
            If Me._NomTypeEntity Is Nothing Then
                Me._NomTypeEntity = NomTypeEntity.EntityLookupDictionary(Me.NomTypeId)
            End If
            Return Me._NomTypeEntity
        End Get
    End Property

    ''' <summary> Fetches the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchNomTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not NomTypeEntity.IsEnumerated Then NomTypeEntity.TryFetchAll(connection)
        Me._NomTypeEntity = NomTypeEntity.EntityLookupDictionary(Me.NomTypeId)
        Me.NotifyPropertyChanged(NameOf(MeterGuardBandLookupEntity.NomTypeEntity))
        Return Me._NomTypeEntity IsNot Nothing
    End Function

#End Region

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the GuardBandLookup. </summary>
    ''' <value> Identifies the GuardBandLookup. </value>
    Public Property Id As Integer Implements IMeterGuardBandLookup.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the tolerance code. </summary>
    ''' <value> The tolerance code. </value>
    Public Property ToleranceCode As String Implements IMeterGuardBandLookup.ToleranceCode
        Get
            Return Me.ICache.ToleranceCode
        End Get
        Set(value As String)
            If Not String.Equals(Me.ToleranceCode, value) Then
                Me.ICache.ToleranceCode = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/>.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer Implements IMeterGuardBandLookup.MultimeterId
        Get
            Return Me.ICache.MultimeterId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.MultimeterId, value) Then
                Me.ICache.MultimeterId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.NomTypeEntity"/>. </value>
    Public Property NomTypeId As Integer Implements IMeterGuardBandLookup.NomTypeId
        Get
            Return Me.ICache.NomTypeId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.NomTypeId, value) Then
                Me.ICache.NomTypeId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the minimum inclusive value. </summary>
    ''' <value> The minimum inclusive value. </value>
    Public Property MinimumInclusiveValue As Double? Implements IMeterGuardBandLookup.MinimumInclusiveValue
        Get
            Return Me.ICache.MinimumInclusiveValue
        End Get
        Set(value As Double?)
            If Not Double?.Equals(Me.MinimumInclusiveValue, value) Then
                Me.ICache.MinimumInclusiveValue = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the maximum exclusive value. </summary>
    ''' <value> The maximum exclusive value. </value>
    Public Property MaximumExclusiveValue As Double? Implements IMeterGuardBandLookup.MaximumExclusiveValue
        Get
            Return Me.ICache.MaximumExclusiveValue
        End Get
        Set(value As Double?)
            If Not Double?.Equals(Me.MaximumExclusiveValue, value) Then
                Me.ICache.MaximumExclusiveValue = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the timestamp. </summary>
    ''' <value> The timestamp. </value>
    Public Property GuardBandFactor As Double Implements IMeterGuardBandLookup.GuardBandFactor
        Get
            Return Me.ICache.GuardBandFactor
        End Get
        Set(value As Double)
            If Not Double.Equals(Me.GuardBandFactor, value) Then
                Me.ICache.GuardBandFactor = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">             The connection. </param>
    ''' <param name="meterGuardBandLookupId"> The entity unique key. </param>
    ''' <returns> A <see cref="MeterGuardBandLookupEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal meterGuardBandLookupId As Integer) As (Success As Boolean, Details As String, Entity As MeterGuardBandLookupEntity)
        Dim activity As String = String.Empty
        Dim entity As New MeterGuardBandLookupEntity
        Try
            activity = $"Fetching {NameOf(MeterGuardBandLookupEntity)} by {NameOf(MeterGuardBandLookupNub.Id)} of {meterGuardBandLookupId}"
            Return If(entity.FetchUsingKey(connection, meterGuardBandLookupId),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, details As String, Entities As IEnumerable(Of
    ''' MeterGuardBandLookupEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, details As String, Entities As IEnumerable(Of MeterGuardBandLookupEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New MeterGuardBandLookupEntity()
            activity = $"fetching all {NameOf(MeterGuardBandLookupEntity)}'s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> MeterGuardBandLookupEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(MeterGuardBandLookupEntity.EntityLookupDictionary)} count must equal {NameOf(MeterGuardBandLookupEntity.MeterGuardBandLookups)} count ")
            End If
            Return (True, String.Empty, MeterGuardBandLookupEntity.MeterGuardBandLookups)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of MeterGuardBandLookupEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

