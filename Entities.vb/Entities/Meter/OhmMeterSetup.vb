Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions

''' <summary>
''' Interface for the Ohm Meter Setup nub and entity. Includes the fields as kept in the data
''' table. Allows tracking of property changes.
''' </summary>
''' <remarks>
''' Test settings are stored in this entity for each test session. The test settings come from
''' <see cref="OhmMeterSettingEntity"/> records. These records are selected from
''' <see cref="OhmMeterSettingLookupEntity"/> records based on the part resistance and tolerance
''' and meter model and number (meter identity.)  <para>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IOhmMeterSetup

    ''' <summary> Gets Identifies the Ohm Meter Setup. </summary>
    ''' <value> Identifies the Ohm Meter Setup. </value>
    <Key>
    Property AutoId As Integer

    ''' <summary> Gets the Nplc. </summary>
    ''' <value> The Nplc. </value>
    Property Nplc As Double

    ''' <summary> Gets the number of averaging filters. </summary>
    ''' <value> The number of averaging filters. </value>
    Property AveragingFilterCount As Integer

    ''' <summary> Gets the averaging filter window. </summary>
    ''' <value> The averaging filter window. </value>
    Property AveragingFilterWindow As Double

    ''' <summary> Gets the start delay. </summary>
    ''' <value> The start delay. </value>
    Property StartDelay As Double

    ''' <summary> Gets the end delay. </summary>
    ''' <value> The end delay. </value>
    Property EndDelay As Double

End Interface

''' <summary> An ohm meter setup builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class OhmMeterSetupBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(OhmMeterSetupBuilder._TableName) Then
                OhmMeterSetupBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(OhmMeterSetupNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{OhmMeterSetupBuilder.TableName}] (
[{NameOf(OhmMeterSetupNub.AutoId)}]  integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
[{NameOf(OhmMeterSetupNub.Nplc)}] float NOT NULL, 
[{NameOf(OhmMeterSetupNub.AveragingFilterCount)}] integer NOT NULL, 
[{NameOf(OhmMeterSetupNub.AveragingFilterWindow)}] float NOT NULL, 
[{NameOf(OhmMeterSetupNub.StartDelay)}] float NOT NULL, 
[{NameOf(OhmMeterSetupNub.EndDelay)}] float NOT NULL); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmMeterSetupBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].{OhmMeterSetupBuilder.TableName}') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{OhmMeterSetupBuilder.TableName}](
	[{NameOf(OhmMeterSetupNub.AutoId)}]  [int] IDENTITY(1,1) NOT NULL,
	[{NameOf(OhmMeterSetupNub.Nplc)}] [float] NOT NULL,
	[{NameOf(OhmMeterSetupNub.AveragingFilterCount)}] [int] NOT NULL,
	[{NameOf(OhmMeterSetupNub.AveragingFilterWindow)}] [float] NOT NULL,
	[{NameOf(OhmMeterSetupNub.StartDelay)}] [float] NOT NULL,
	[{NameOf(OhmMeterSetupNub.EndDelay)}] [float] NOT NULL,
 CONSTRAINT [PK_{OhmMeterSetupBuilder.TableName}] PRIMARY KEY CLUSTERED 
([{NameOf(OhmMeterSetupNub.AutoId)}] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmMeterSetupBuilder.TableName
    End Function

End Class

''' <summary>
''' Implements the Ohm Meter Setup table <see cref="IOhmMeterSetup">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2019 </para>
''' </remarks>
<Table("OhmMeterSetup")>
Public Class OhmMeterSetupNub
    Inherits EntityNubBase(Of IOhmMeterSetup)
    Implements IOhmMeterSetup

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmMeterSetup
        Return New OhmMeterSetupNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmMeterSetup
        Dim destination As IOhmMeterSetup = Me.CreateNew
        OhmMeterSetupNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmMeterSetup)
        OhmMeterSetupNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IOhmMeterSetup, ByVal destination As IOhmMeterSetup)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.Nplc = source.Nplc
        destination.AveragingFilterCount = source.AveragingFilterCount
        destination.AveragingFilterWindow = source.AveragingFilterWindow
        destination.EndDelay = source.EndDelay
        destination.AutoId = source.AutoId
        destination.StartDelay = source.StartDelay
    End Sub

#End Region

#Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IOhmMeterSetup))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IOhmMeterSetup) As Boolean
        Return other IsNot Nothing AndAlso OhmMeterSetupNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IOhmMeterSetup, ByVal right As IOhmMeterSetup) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Double.Equals(left.Nplc, right.Nplc)
            result = result AndAlso Integer.Equals(left.AveragingFilterCount, right.AveragingFilterCount)
            result = result AndAlso Double.Equals(left.AveragingFilterWindow, right.AveragingFilterWindow)
            result = result AndAlso Double.Equals(left.EndDelay, right.EndDelay)
            result = result AndAlso Integer.Equals(left.AutoId, right.AutoId)
            result = result AndAlso Double.Equals(left.StartDelay, right.StartDelay)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.Nplc.GetHashCode() Xor Me.AveragingFilterCount Xor Me.AveragingFilterWindow.GetHashCode Xor Me.AutoId Xor Me.StartDelay.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets Identifies the Ohm Meter Setup. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Identifies the Ohm Meter Setup. </value>
    <Key>
    Public Property AutoId As Integer Implements IOhmMeterSetup.AutoId

    ''' <summary> Gets or sets the Nplc. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Nplc. </value>
    Public Property Nplc As Double Implements IOhmMeterSetup.Nplc

    ''' <summary> Gets or sets the number of averaging filters. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The number of averaging filters. </value>
    Public Property AveragingFilterCount As Integer Implements IOhmMeterSetup.AveragingFilterCount

    ''' <summary> Gets or sets the averaging filter window. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The averaging filter window. </value>
    Public Property AveragingFilterWindow As Double Implements IOhmMeterSetup.AveragingFilterWindow

    ''' <summary> Gets or sets the start delay. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The start delay. </value>
    Public Property StartDelay As Double Implements IOhmMeterSetup.StartDelay

    ''' <summary> Gets or sets the end delay. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The end delay. </value>
    Public Property EndDelay As Double Implements IOhmMeterSetup.EndDelay

#End Region

End Class

''' <summary>
''' the Ohm Meter Setup Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2019 </para>
''' </remarks>
Public Class OhmMeterSetupEntity
    Inherits EntityBase(Of IOhmMeterSetup, OhmMeterSetupNub)
    Implements IOhmMeterSetup

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New OhmMeterSetupNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The OhmMeterSetup interface. </param>
    Public Sub New(ByVal value As IOhmMeterSetup)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOhmMeterSetup, ByVal store As IOhmMeterSetup)
        MyBase.New(New OhmMeterSetupNub, cache, store)
        Me.UsingNativeTracking = String.Equals(OhmMeterSetupBuilder.TableName, NameOf(IOhmMeterSetup).TrimStart("I"c))
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmMeterSetup
        Return New OhmMeterSetupNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmMeterSetup
        Dim destination As IOhmMeterSetup = Me.CreateNew
        OhmMeterSetupNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmMeterSetup)
        OhmMeterSetupNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The OhmMeterSetup interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOhmMeterSetup)
        ' first make the copy to notify of any property change.
        OhmMeterSetupNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The OhmMeterSetup table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IOhmMeterSetup)(key), connection.Get(Of OhmMeterSetupNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="ohmMeterSetupId"> Identifier for the ohm meter setup. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal ohmMeterSetupId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, ohmMeterSetupId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOhmMeterSetup) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If OhmMeterSetupEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.AutoId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of OhmMeterSetupNub)(New OhmMeterSetupNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Guard Band Lookup entities. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> the Guard Band Lookup entities. </value>
    Public ReadOnly Property OhmMeterSetups As IEnumerable(Of OhmMeterSetupEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of OhmMeterSetupEntity)
        Return If(usingNativeTracking, OhmMeterSetupEntity.Populate(connection.GetAll(Of IOhmMeterSetup)), OhmMeterSetupEntity.Populate(connection.GetAll(Of OhmMeterSetupNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._OhmMeterSetups = OhmMeterSetupEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(OhmMeterSetupEntity.OhmMeterSetups))
        Return If(Me.OhmMeterSetups?.Any, Me.OhmMeterSetups.Count, 0)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="ohmMeterSetupAutoId"> Identifies the <see cref="Entities.MultimeterEntity"/>
    '''                                    model. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal ohmMeterSetupAutoId As Integer) As IEnumerable(Of OhmMeterSetupEntity)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{OhmMeterSetupBuilder.TableName}] WHERE {NameOf(OhmMeterSetupNub.AutoId)} = @Id", New With {Key .Id = ohmMeterSetupAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return OhmMeterSetupEntity.Populate(connection.Query(Of OhmMeterSetupNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Populates a list of OhmMeterSetup entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The OhmMeterSetup nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of OhmMeterSetupNub)) As IEnumerable(Of OhmMeterSetupEntity)
        Dim l As New List(Of OhmMeterSetupEntity)
        If nubs?.Any Then
            For Each nub As OhmMeterSetupNub In nubs
                l.Add(New OhmMeterSetupEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of OhmMeterSetup entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The OhmMeterSetup interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOhmMeterSetup)) As IEnumerable(Of OhmMeterSetupEntity)
        Dim l As New List(Of OhmMeterSetupEntity)
        If interfaces?.Any Then
            Dim nub As New OhmMeterSetupNub
            For Each iFace As IOhmMeterSetup In interfaces
                nub.CopyFrom(iFace)
                l.Add(New OhmMeterSetupEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Count entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection.CountEntities(OhmMeterSetupBuilder.TableName)
    End Function

#End Region

#Region " SETTINGS "

    ''' <summary> Copies from the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Ohm Meter settings interface. </param>
    Public Sub Copy(ByVal value As IOhmMeterSetting)
        OhmMeterSetupEntity.Copy(value, Me)
    End Sub

    ''' <summary> Copies the meter setup from the selected meter setting. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      the Ohm Meter settings interface. </param>
    ''' <param name="destination"> The ohm meter setup. </param>
    Public Shared Sub Copy(ByVal source As IOhmMeterSetting, ByVal destination As IOhmMeterSetup)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.Nplc = source.Nplc
        destination.AveragingFilterCount = source.AveragingFilterCount
        destination.AveragingFilterWindow = source.AveragingFilterWindow
        destination.EndDelay = source.EndDelay
        destination.StartDelay = source.StartDelay
    End Sub

    ''' <summary> Determine if the meter setup is equal between these entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function IsEqualSetup(ByVal left As IOhmMeterSetup, ByVal right As IOhmMeterSetting) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Double.Equals(left.Nplc, right.Nplc)
            result = result AndAlso Integer.Equals(left.AveragingFilterCount, right.AveragingFilterCount)
            result = result AndAlso Double.Equals(left.AveragingFilterWindow, right.AveragingFilterWindow)
            result = result AndAlso Double.Equals(left.EndDelay, right.EndDelay)
            result = result AndAlso Double.Equals(left.StartDelay, right.StartDelay)
            Return result
        End If
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Ohm Meter Setup. </summary>
    ''' <value> Identifies the Ohm Meter Setup. </value>
    Public Property AutoId As Integer Implements IOhmMeterSetup.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Nplc. </summary>
    ''' <value> The Nplc. </value>
    Public Property Nplc As Double Implements IOhmMeterSetup.Nplc
        Get
            Return Me.ICache.Nplc
        End Get
        Set(value As Double)
            If Not Double.Equals(Me.Nplc, value) Then
                Me.ICache.Nplc = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the number of averaging filters. </summary>
    ''' <value> The number of averaging filters. </value>
    Public Property AveragingFilterCount As Integer Implements IOhmMeterSetup.AveragingFilterCount
        Get
            Return Me.ICache.AveragingFilterCount
        End Get
        Set(value As Integer)
            If Not Double.Equals(Me.AveragingFilterCount, value) Then
                Me.ICache.AveragingFilterCount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the averaging filter window. </summary>
    ''' <value> The averaging filter window. </value>
    Public Property AveragingFilterWindow As Double Implements IOhmMeterSetup.AveragingFilterWindow
        Get
            Return Me.ICache.AveragingFilterWindow
        End Get
        Set(value As Double)
            If Not Double.Equals(Me.AveragingFilterWindow, value) Then
                Me.ICache.AveragingFilterWindow = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the start delay. </summary>
    ''' <value> The start delay. </value>
    Public Property StartDelay As Double Implements IOhmMeterSetup.StartDelay
        Get
            Return Me.ICache.StartDelay
        End Get
        Set(value As Double)
            If Not Double.Equals(Me.StartDelay, value) Then
                Me.ICache.StartDelay = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the end delay. </summary>
    ''' <value> The end delay. </value>
    Public Property EndDelay As Double Implements IOhmMeterSetup.EndDelay
        Get
            Return Me.ICache.EndDelay
        End Get
        Set(value As Double)
            If Not Double.Equals(Me.EndDelay, value) Then
                Me.ICache.EndDelay = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

End Class

''' <summary> Collection of ohm meter setup entities. </summary>
''' <remarks> David, 6/2/2020. </remarks>
Public Class OhmMeterSetupEntityCollection
    Inherits EntityKeyedCollection(Of Integer, IOhmMeterSetup, OhmMeterSetupNub, OhmMeterSetupEntity)

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified OhmMeterSetup.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The OhmMeterSetup from which to extract the key. </param>
    ''' <returns> The key for the specified OhmMeterSetup. </returns>
    Protected Overrides Function GetKeyForItem(ByVal item As OhmMeterSetupEntity) As Integer
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.AutoId
    End Function

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As OhmMeterSetupEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all OhmMeterSetups from the
    ''' <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of OhmMeterSetupEntity))
        If entities?.Any Then
            For Each entity As OhmMeterSetupEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Throw New NotImplementedException()
    End Function

#End Region

#Region " CUSTOM ADDS "

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/2/2020. </remarks>
    ''' <param name="toleranceCode"> The tolerance code. </param>
    ''' <param name="multimeterId">  Identifies the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="nominalValue">  The nominal value. </param>
    Public Overloads Sub Add(ByVal toleranceCode As String, ByVal multimeterId As Integer, ByVal nominalValue As Double)
        ' assume the setting lookup entities were fetched
        Dim ohmMeterSettingId As Integer = OhmMeterSettingLookupEntity.SelectOhmMeterSettingLookupEntity(toleranceCode, multimeterId,
                                                                                                         nominalValue).OhmMeterSettingId
        Dim entity As New OhmMeterSetupEntity
        entity.Copy(OhmMeterSettingEntity.EntityLookupDictionary(ohmMeterSettingId))
        Me.Add(entity)
    End Sub

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 6/2/2020. </remarks>
    ''' <param name="ohmMeterSettingEntity"> The ohm meter setting entity to add. </param>
    Public Overloads Sub Add(ByVal ohmMeterSettingEntity As OhmMeterSettingEntity)
        Dim entity As New OhmMeterSetupEntity
        entity.Copy(ohmMeterSettingEntity)
        Me.Add(entity)
    End Sub


#End Region

End Class

''' <summary>
''' Collection of <see cref="OhmMeterSetupEntity"/>'s keyed by the multimeter identity.
''' </summary>
''' <remarks> David, 6/12/2020. </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class MultimeterIdOhmMeterSetupCollection
    Inherits Dictionary(Of Integer, OhmMeterSetupEntity)
End Class

''' <summary>
''' Collection of <see cref="OhmMeterSetupEntity"/>'s keyed by the multimeter number.
''' </summary>
''' <remarks> David, 6/12/2020. </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class MultimeterNumberOhmMeterSetupCollection
    Inherits Dictionary(Of Integer, OhmMeterSetupEntity)
End Class
