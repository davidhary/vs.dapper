Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A Sample Trait Type builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class SampleTraitTypeBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return SampleTraitTypeBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(SampleTraitTypeBuilder._TableName) Then
                SampleTraitTypeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(SampleTraitTypeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Inserts or ignore the records as described by the <see cref="Entities.SampleTraitType"/> enumeration type. </summary>
    ''' <param name="connection"> The connection. </param>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(SampleTraitType), New Integer() {CInt(SampleTraitType.None)})
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SampleTraitTypeBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SampleTraitTypeBuilder
        If SampleTraitTypeBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                SampleTraitTypeBuilder._Instance = New SampleTraitTypeBuilder()
            End SyncLock
        End If
        Return SampleTraitTypeBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the <see cref="SampleTraitTypeNub"/> table based on the <see cref="INominal">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("SampleTraitType")>
Public Class SampleTraitTypeNub
    Inherits NominalNub
    Implements INominal

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As INominal
        Return New SampleTraitTypeNub
    End Function

End Class


''' <summary> The <see cref="SampleTraitTypeNub"/> Entity table based on the <see cref="INominal">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class SampleTraitTypeEntity
    Inherits EntityBase(Of INominal, SampleTraitTypeNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New SampleTraitTypeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The SampleTraitType interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New SampleTraitTypeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(SampleTraitTypeBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As INominal
        Return New SampleTraitTypeNub
    End Function

    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        SampleTraitTypeNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As INominal)
        SampleTraitTypeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The SampleTraitType interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        SampleTraitTypeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The SampleTraitType table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of SampleTraitTypeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Sample Trait Type label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As SampleTraitTypeNub = SampleTraitTypeEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If SampleTraitTypeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of SampleTraitTypeNub)(New SampleTraitTypeNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the SampleTraitType entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The SampleTraitType entities. </value>
    Public Shared ReadOnly Property SampleTraitTypes As IEnumerable(Of SampleTraitTypeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of SampleTraitTypeEntity)
        Return If(usingNativeTracking, SampleTraitTypeEntity.Populate(connection.GetAll(Of INominal)), SampleTraitTypeEntity.Populate(connection.GetAll(Of SampleTraitTypeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        SampleTraitTypeEntity._SampleTraitTypes = SampleTraitTypeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(SampleTraitTypeEntity.SampleTraitTypes?.Any, SampleTraitTypeEntity.SampleTraitTypes.Count, 0)
    End Function

    ''' <summary> Populates a list of SampleTraitType entities. </summary>
    ''' <param name="nubs"> The SampleTraitType nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of SampleTraitTypeNub)) As IEnumerable(Of SampleTraitTypeEntity)
        Dim l As New List(Of SampleTraitTypeEntity)
        If nubs?.Any Then
            For Each nub As SampleTraitTypeNub In nubs
                l.Add(New SampleTraitTypeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of SampleTraitType entities. </summary>
    ''' <param name="interfaces"> The SampleTraitType interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of SampleTraitTypeEntity)
        Dim l As New List(Of SampleTraitTypeEntity)
        If interfaces?.Any Then
            Dim nub As New SampleTraitTypeNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New SampleTraitTypeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, SampleTraitTypeEntity)

    ''' <summary> The entity lookup dictionary. </summary>
    ''' <returns> A Dictionary(Of Integer, SampleTraitTypeEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, SampleTraitTypeEntity)
        If Not (SampleTraitTypeEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            SampleTraitTypeEntity._EntityLookupDictionary = New Dictionary(Of Integer, SampleTraitTypeEntity)
            For Each entity As SampleTraitTypeEntity In SampleTraitTypeEntity.SampleTraitTypes
                SampleTraitTypeEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return SampleTraitTypeEntity._EntityLookupDictionary
    End Function

    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (SampleTraitTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            SampleTraitTypeEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As SampleTraitTypeEntity In SampleTraitTypeEntity.SampleTraitTypes
                SampleTraitTypeEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return SampleTraitTypeEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (SampleTraitTypeEntity.SampleTraitTypes?.Any AndAlso
                SampleTraitTypeEntity._EntityLookupDictionary?.Any AndAlso
                SampleTraitTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Sample Trait Type label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{SampleTraitTypeNub.TableName}]
        ' WHERE {NameOf(SampleTraitTypeNub.Label)} = @label", New With {Key .Label = label})
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{SampleTraitTypeBuilder.TableName}] WHERE {NameOf(SampleTraitTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Sample Trait Type label. </param>
    ''' <returns> An ISampleTraitType . </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of SampleTraitTypeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{SampleTraitTypeBuilder.TableName}] WHERE {NameOf(SampleTraitTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SampleTraitTypeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the SampleTraitType exists. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = SampleTraitTypeEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Sample Trait Type. </summary>
    ''' <value> Identifies the Sample Trait Type. </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Sample Trait Type label. </summary>
    ''' <value> The Sample Trait Type label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the Sample Trait Type. </summary>
    ''' <value> The Sample Trait Type description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="sampleTraitTypeId"> The entity unique key. </param>
    ''' <param name="e">           Action event information. </param>
    ''' <returns> A <see cref="SampleTraitTypeEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal sampleTraitTypeId As Integer, ByVal e As isr.Core.ActionEventArgs) As SampleTraitTypeEntity
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Dim result As New SampleTraitTypeEntity
        Try
            activity = $"Fetching {NameOf(SampleTraitTypeEntity)} by {NameOf(SampleTraitTypeNub.Id)} of {sampleTraitTypeId}"
            If Not result.FetchUsingKey(connection, sampleTraitTypeId) Then
                e.RegisterFailure($"Failed {activity}")
            End If
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return result
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <param name="connection">             The connection. </param>
    ''' <param name="sampleTraitTypeLabel"> The Sample Trait Type label. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entity As SampleTraitTypeEntity)
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal sampleTraitTypeLabel As String) As (Success As Boolean, Details As String, Entity As SampleTraitTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New SampleTraitTypeEntity
        Try
            activity = $"Fetching {NameOf(SampleTraitTypeEntity)} by {NameOf(SampleTraitTypeNub.Label)} of {sampleTraitTypeLabel}"
            Return If(entity.FetchUsingUniqueIndex(connection, sampleTraitTypeLabel),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of SampleTraitTypeEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of SampleTraitTypeEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New SampleTraitTypeEntity()
            activity = $"fetching all {NameOf(SampleTraitType)}s"
            Dim SampleCount As Integer = entity.FetchAllEntities(connection)
            If SampleCount <> SampleTraitTypeEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(SampleTraitTypeEntity.EntityLookupDictionary)} count must equal {NameOf(SampleTraitTypeEntity.SampleTraitTypes)} count ")
            ElseIf SampleCount <> SampleTraitTypeEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(SampleTraitTypeEntity.KeyLookupDictionary)} count must equal {NameOf(SampleTraitTypeEntity.SampleTraitTypes)} count ")
            End If
            Return (True, String.Empty, SampleTraitTypeEntity.SampleTraitTypes)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of SampleTraitTypeEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Values that represent Sample Trait Types. </summary>
<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="<Pending>")>
Public Enum SampleTraitType
    <ComponentModel.Description("None")> None = 0
    <ComponentModel.Description("Nominal Value")> NominalValue = 1
    <ComponentModel.Description("Target Value")> TargetValue = 2
    <ComponentModel.Description("Lower Specification Limit (LSL)")> LowerSpecificationLimit = 3
    <ComponentModel.Description("Upper Specification Limit (USL)")> UpperSpecificationLimit = 4
    <ComponentModel.Description("Mean")> Mean = 5
    <ComponentModel.Description("Sigma")> Sigma = 6
    <ComponentModel.Description("SampleCount")> SampleCount = 7
    <ComponentModel.Description("First Quartile")> FirstQuartile = 8
    <ComponentModel.Description("Median")> Median = 9
    <ComponentModel.Description("Third Quartile")> ThirdQuartile = 10
    <ComponentModel.Description("Minimum")> Minimum = 11
    <ComponentModel.Description("Maximum")> Maximum = 12
    <ComponentModel.Description("Fence Factor")> FenceFactor = 13
    <ComponentModel.Description("Lower OutlierLimit")> LowerOutlierLimit = 14
    <ComponentModel.Description("Upper Outlier Limit")> UpperOutlierLimit = 15
    <ComponentModel.Description("Outlier Count")> OutlierCount = 16
    <ComponentModel.Description("Minimum Cp")> MinimumCp = 17
    <ComponentModel.Description("Minimum Cpk")> MinimumCpk = 18
    <ComponentModel.Description("Minimum Cpm")> MinimumCpm = 19
    <ComponentModel.Description("Cp")> Cp = 20
    <ComponentModel.Description("Cpk")> Cpk = 21
    <ComponentModel.Description("Cpm")> Cpm = 22
    <ComponentModel.Description("Process Target")> ProcessTarget = 23
    <ComponentModel.Description("Process Mean")> ProcessMean = 24
    <ComponentModel.Description("Initial Process Nominal")> InitialProcessNominal = 25
    <ComponentModel.Description("Process Nominal")> ProcessNominal = 26
    <ComponentModel.Description("Process Error")> ProcessError = 27
    <ComponentModel.Description("Adjusted Process Nominal")> AdjustedProcessNominal = 28
    <ComponentModel.Description("Process Bias")> ProcessBias = 29
    <ComponentModel.Description("Process Bias Mean")> ProcessBiasMean = 30
    <ComponentModel.Description("Process Bias Error")> ProcessBiasError = 31
    <ComponentModel.Description("Total Count")> TotalCount = 32
    <ComponentModel.Description("Invalid Count")> InvalidCount = 33
    <ComponentModel.Description("Good Count")> GoodCount = 34
    <ComponentModel.Description("Failed Count")> FailedCount = 35
    <ComponentModel.Description("Yield")> Yield = 36
    <ComponentModel.Description("Valid Yield")> ValidYield = 37
    <ComponentModel.Description("Yield Decimal Places ")> YieldDecimalPlaces = 38
End Enum
