Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Sample Trait builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class SampleTraitBuilder
    Inherits KeyTwoForeignRealBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return SampleTraitBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(SampleTraitBuilder._TableName) Then
                SampleTraitBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(SampleTraitNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the first foreign reference table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the first foreign reference table. </value>
    Public Overrides Property FirstForeignTableName As String = NomTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the first foreign reference table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the first foreign reference table key. </value>
    Public Overrides Property FirstForeignTableKeyName As String = NameOf(NomTypeNub.Id)

    ''' <summary> Gets or sets the name of the second foreign reference table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the first foreign reference table. </value>
    Public Overrides Property SecondForeignTableName As String = SampleTraitTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the second foreign reference table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the first foreign reference table key. </value>
    Public Overrides Property SecondForeignTableKeyName As String = NameOf(SampleTraitTypeNub.Id)

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The new table. </returns>
    Public Overloads Function CreateTable(connection As System.Data.IDbConnection) As String
        ' the same Element ID and Sample Type would repeat for the NUT associated with the next UUT.
        Return MyBase.CreateTable(connection, UniqueIndexOptions.None)
    End Function

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(SampleTraitEntity.AutoId)

    ''' <summary> Gets or sets the name of the First foreign identifier field. </summary>
    ''' <value> The name of the First foreign identifier field. </value>
    Public Overrides Property FirstForeignIdFieldName() As String = NameOf(SampleTraitEntity.NomTypeId)

    ''' <summary> Gets or sets the name of the second foreign identifier field. </summary>
    ''' <value> The name of the second foreign identifier field. </value>
    Public Overrides Property SecondForeignIdFieldName() As String = NameOf(SampleTraitEntity.SampleTraitTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName() As String = NameOf(SampleTraitEntity.Amount)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SampleTraitBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SampleTraitBuilder
        If SampleTraitBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                SampleTraitBuilder._Instance = New SampleTraitBuilder()
            End SyncLock
        End If
        Return SampleTraitBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="Entities.SampleTraitEntity"/> <see cref="IKeyTwoForeignReal">interface</see>.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("SampleTrait")>
Public Class SampleTraitNub
    Inherits KeyTwoForeignRealNub
    Implements IKeyTwoForeignReal

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IKeyTwoForeignReal
        Return New SampleTraitNub
    End Function

End Class

''' <summary>
''' The <see cref="Entities.SampleTraitEntity"/> storing Real(Double)-Valued Element Sample Traits.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
''' David, 4/22/2020 </para></remarks>
Public Class SampleTraitEntity
    Inherits EntityBase(Of IKeyTwoForeignReal, SampleTraitNub)
    Implements IKeyTwoForeignReal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New SampleTraitNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> the <see cref="Entities.ElementEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IKeyTwoForeignReal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyTwoForeignReal, ByVal store As IKeyTwoForeignReal)
        MyBase.New(New SampleTraitNub, cache, store)
        Me.UsingNativeTracking = String.Equals(SampleTraitBuilder.TableName, NameOf(IKeyTwoForeignReal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IKeyTwoForeignReal
        Return New SampleTraitNub
    End Function

    Public Overrides Function CreateCopy() As IKeyTwoForeignReal
        Dim destination As IKeyTwoForeignReal = Me.CreateNew
        SampleTraitNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="Entities.SampleTraitEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IKeyTwoForeignReal)
        SampleTraitNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> the <see cref="Entities.ElementEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyTwoForeignReal)
        ' first make the copy to notify of any property change.
        SampleTraitNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The <see cref="Entities.SampleTraitEntity"/> primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyTwoForeignReal)(key), connection.Get(Of SampleTraitNub)(key)))
    End Function

    ''' <summary> Refetch; Fetches using the primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/15/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="autoId">            Identifies the <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, autoId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyTwoForeignReal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If SampleTraitEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.AutoId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="autoId">            Identifies the <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Boolean
        Return connection.Delete(Of SampleTraitNub)(New SampleTraitNub With {.AutoId = autoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or <see cref="Entities.SampleTraitEntity"/>'s. </summary>
    ''' <value> The <see cref="Entities.SampleTraitEntity"/>'s. </value>
    Public ReadOnly Property SampleTraits As IEnumerable(Of SampleTraitEntity)

    ''' <summary> Fetches all records into <see cref="Entities.SampleTraitEntity"/>'s. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of SampleTraitEntity)
        Return If(usingNativeTracking, SampleTraitEntity.Populate(connection.GetAll(Of IKeyTwoForeignReal)),
                                       SampleTraitEntity.Populate(connection.GetAll(Of SampleTraitNub)))
    End Function

    ''' <summary> Fetches all records into <see cref="Entities.SampleTraitEntity"/>'s. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._SampleTraits = SampleTraitEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(SampleTraitEntity.SampleTraits))
        Return If(Me.SampleTraits?.Any, Me.SampleTraits.Count, 0)
    End Function

    ''' <summary> Populates a list of <see cref="Entities.SampleTraitEntity"/>'s. </summary>
    ''' <param name="nubs"> the <see cref="Entities.ElementEntity"/>Value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of SampleTraitNub)) As IEnumerable(Of SampleTraitEntity)
        Dim l As New List(Of SampleTraitEntity)
        If nubs?.Any Then
            For Each nub As SampleTraitNub In nubs
                l.Add(New SampleTraitEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of <see cref="Entities.SampleTraitEntity"/>'s. </summary>
    ''' <param name="interfaces"> the <see cref="Entities.ElementEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyTwoForeignReal)) As IEnumerable(Of SampleTraitEntity)
        Dim l As New List(Of SampleTraitEntity)
        If interfaces?.Any Then
            Dim nub As New SampleTraitNub
            For Each iFace As IKeyTwoForeignReal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New SampleTraitEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count element Sample Traits; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SampleTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SampleTraitNub.AutoId)} = @AutoId", New With {.AutoId = autoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches element Sample Traits by Element number and element Sample Trait element
    ''' Sample Trait Real Value type;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As IEnumerable(Of SampleTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SampleTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SampleTraitNub.AutoId)} = @AutoId", New With {.AutoId = autoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SampleTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches element Sample Traits by <see cref="Entities.SampleTraitEntity"/> id; expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As IEnumerable(Of SampleTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SampleTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SampleTraitEntity.AutoId)} = @AutoId", New With {.AutoId = autoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SampleTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.ElementEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="Entities.SampleTraitEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Boolean
        Return 1 = SampleTraitEntity.CountEntities(connection, autoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary>
    ''' Fetches element Sample Real(Double)-Value trait values by Element auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">          The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchSampleTraitss(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of SampleTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SampleTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SampleTraitNub.AutoId)} = @AutoId", New With {Me.AutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SampleTraitNub)(selector.RawSql, selector.Parameters)
    End Function

#End Region

#Region " RELATIONS: NOMINAL TYPE "

    ''' <summary> Gets the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> the <see cref="Entities.NomTypeEntity"/>. </value>
    Public ReadOnly Property NomTypeEntity As NomTypeEntity

    ''' <summary> Fetches a <see cref="Entities.NomTypeEntity"/> . </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchNomTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._NomTypeEntity = New NomTypeEntity()
        Return Me.NomTypeEntity.FetchUsingKey(connection, Me.NomTypeId)
    End Function

#End Region

#Region " RELATIONS: SAMPLE TRAIT TYPE "

    ''' <summary> Gets or sets the <see cref="Entities.SampleTraitTypeEntity"/> . </summary>
    ''' <value> the <see cref="Entities.SampleTraitTypeEntity"/> . </value>
    Public ReadOnly Property SampleTraitTypeEntity As SampleTraitTypeEntity

    ''' <summary> Fetches a <see cref="Entities.SampleTraitTypeEntity"/>. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchSampleTraitTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._SampleTraitTypeEntity = New SampleTraitTypeEntity()
        Return Me.SampleTraitTypeEntity.FetchUsingKey(connection, Me.SampleTraitTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public Property AutoId As Integer Implements IKeyTwoForeignReal.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the first foreign key. </summary>
    ''' <value> Identifies the first foreign key. </value>
    Public Property FirstForeignId As Integer Implements IKeyTwoForeignReal.FirstForeignId
        Get
            Return Me.ICache.FirstForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.FirstForeignId, value) Then
                Me.ICache.FirstForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SampleTraitEntity.NomTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> The identifier of the <see cref="Entities.NomTypeEntity"/>. </value>
    Public Property NomTypeId As Integer
        Get
            Return Me.FirstForeignId
        End Get
        Set(ByVal value As Integer)
            Me.FirstForeignId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Second Foreign key. </summary>
    ''' <value> Identifies the Second Foreign key. </value>
    Public Property SecondForeignId As Integer Implements IKeyTwoForeignReal.SecondForeignId
        Get
            Return Me.ICache.SecondForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondForeignId, value) Then
                Me.ICache.SecondForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SampleTraitEntity.SampleTraitTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the type of the <see cref="Entities.SampleTraitEntity"/> Real(Double)-value. </summary>
    ''' <value> The type of the <see cref="Entities.SampleTraitEntity"/> Real(Double)-value. </value>
    Public Property SampleTraitTypeId As Integer
        Get
            Return Me.SecondForeignId
        End Get
        Set(ByVal value As Integer)
            Me.SecondForeignId = value
        End Set
    End Property

    ''' <summary> Gets or sets a Real(Double)-value assigned to the <see cref="Entities.SampleTraitEntity"/> for the specific <see cref="Entities.SampleTraitTypeEntity"/>. </summary>
    ''' <value> The Real(Double)-value assigned to the <see cref="Entities.SampleTraitEntity"/> for the specific <see cref="Entities.SampleTraitTypeEntity"/>. </value>
    Public Property Amount As Double Implements IKeyTwoForeignReal.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(ByVal value As Double)
            If Not Double.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The entity selector. </value>
    Public ReadOnly Property EntitySelector As ThreeKeySelector
        Get
            Return New ThreeKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of <see cref="Entities.SampleTraitEntity"/>'s. </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class SampleTraitEntityCollection
    Inherits EntityKeyedCollection(Of ThreeKeySelector, IKeyTwoForeignReal, SampleTraitNub, SampleTraitEntity)

    Protected Overrides Function GetKeyForItem(item As SampleTraitEntity) As ThreeKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As SampleTraitEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of SampleTraitEntity))
        If entities?.Any Then
            For Each entity As SampleTraitEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return SampleTraitBuilder.Get.Upsert(connection, Me)
    End Function

End Class

Public Structure SampleTraitEntitySelector
    Implements IEquatable(Of SampleTraitEntitySelector)

    Public Sub New(ByVal sampleTraitEntity As IKeyTwoForeignReal)
        Me.New(sampleTraitEntity.AutoId, sampleTraitEntity.FirstForeignId, sampleTraitEntity.SecondForeignId)
    End Sub
    Public Sub New(ByVal sampleTraitEntity As SampleTraitEntity)
        Me.New(sampleTraitEntity.AutoId, sampleTraitEntity.NomTypeId, sampleTraitEntity.SampleTraitTypeId)
    End Sub
    Public Sub New(ByVal sampleTraitEntityAutoId As Integer, ByVal nomTypeId As Integer, ByVal sampleTrait As Integer)
        Me.SampleTraitEntityAutoId = sampleTraitEntityAutoId
        Me.NomTypeId = nomTypeId
        Me.SampleTraitTypeId = sampleTrait
    End Sub

    Public Property SampleTraitEntityAutoId As Integer

    Public Property NomTypeId As Integer

    Public Property SampleTraitTypeId As Integer

    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.Equals(CType(obj, SampleTraitEntitySelector))
    End Function

    Public Overloads Function Equals(other As SampleTraitEntitySelector) As Boolean Implements IEquatable(Of SampleTraitEntitySelector).Equals
        Return Me.SampleTraitEntityAutoId = other.SampleTraitEntityAutoId AndAlso Me.SampleTraitTypeId = other.SampleTraitTypeId AndAlso Me.NomTypeId = other.NomTypeId
    End Function

    Public Overrides Function GetHashCode() As Integer
        Return Me.SampleTraitEntityAutoId Xor Me.SampleTraitTypeId Xor Me.NomTypeId
    End Function

    Public Shared Operator =(left As SampleTraitEntitySelector, right As SampleTraitEntitySelector) As Boolean
        Return left.Equals(right)
    End Operator

    Public Shared Operator <>(left As SampleTraitEntitySelector, right As SampleTraitEntitySelector) As Boolean
        Return Not left = right
    End Operator

End Structure
