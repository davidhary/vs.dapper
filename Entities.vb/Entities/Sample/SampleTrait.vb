''' <summary> The Sample Trait class holing the values for the sample of units under test. </summary>
''' <remarks> David, 2020-05-29. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Class SampleTrait
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSetter"> The getter setter. </param>
    ''' <param name="element">      The <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifier for the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="multimeter">   The <see cref="Entities.MultimeterEntity"/>. </param>
    Public Sub New(ByVal getterSetter As isr.Core.Constructs.IGetterSetter(Of Double), ByVal element As ElementEntity, ByVal nomTypeId As Integer,
                   ByVal multimeter As MultimeterEntity, ByVal certifiedQuantity As Integer)
        MyBase.New()
        Me.GetterSetter = getterSetter
        Me.ElementAutoId = element.AutoId
        Me.ElementLabel = element.ElementLabel
        Me.ElementType = CType(element.ElementTypeId, ElementType)
        Me.ElementOrdinalNumber = element.OrdinalNumber
        Me.NomType = CType(nomTypeId, NomType)
        Me.MultimeterId = multimeter.Id
        Me.CertifiedQuantity = certifiedQuantity
        Me.NominalValue = element.NomTraitsCollection(New MeterNomTypeSelector(multimeter.Id, nomTypeId)).NomTrait.NominalValue
        Me.Selector = New MeterElementNomTypeSelector(Me)
        Me.Statistics = New isr.Core.Engineering.SampleQuartiles
    End Sub

    ''' <summary> Gets or sets the selector. </summary>
    ''' <value> The selector. </value>
    Public ReadOnly Property Selector As MeterElementNomTypeSelector

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter(Of Double)

    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.GetterSetter.Setter(value.Value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

#End Region

#Region " IDENTIFIERS "

    Private _NomType As NomType

    ''' <summary> Gets or sets the type of the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> The type of the nominal. </value>
    Public Property NomType As NomType
        Get
            Return Me._NomType
        End Get
        Set
            If Value <> Me.NomType Then
                Me._NomType = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _MultimeterId As Integer

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer
        Get
            Return Me._MultimeterId
        End Get
        Set
            If Value <> Me.MultimeterId Then
                Me._MultimeterId = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _ElementAutoId As Integer

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property ElementAutoId As Integer
        Get
            Return Me._ElementAutoId
        End Get
        Set
            If Value <> Me.ElementAutoId Then
                Me._ElementAutoId = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " STATISTICS "

    ''' <summary> Gets or sets the statistics. </summary>
    ''' <value> The statistics. </value>
    Public Property Statistics As isr.Core.Engineering.SampleQuartiles

    Public Sub Clear()
        Me.Statistics.ClearKnownState()
        Me.SampleCount = New Integer?
        Me.OutlierCount = New Integer?
        Me.Mean = New Double?
        Me.Sigma = New Double?
        Me.FirstQuartile = New Double?
        Me.Median = New Double?
        Me.ThirdQuartile = New Double?
        Me.Minimum = New Double?
        Me.Maximum = New Double?
        Me.FenceFactor = New Double?
        Me.LowerOutlierLimit = New Double?
        Me.UpperOutlierLimit = New Double?
        Me.Cp = New Double?
        Me.Cpk = New Double?
        Me.Cpm = New Double?
    End Sub


    ''' <summary> Evaluates the statistics and apply. </summary>
    ''' <remarks> David, 2020-05-29. </remarks>
    Public Sub Evaluate()
        Me.Statistics.Evaluate()
        Me.SampleCount = Me.Statistics.Sample.Count
        Me.OutlierCount = Me.Statistics.OutlierCount
        Me.Mean = Me.Statistics.Sample.Mean
        Me.Sigma = Me.Statistics.Sample.Sigma
        Me.FirstQuartile = Me.Statistics.Quartiles.First
        Me.Median = Me.Statistics.Quartiles.Median
        Me.ThirdQuartile = Me.Statistics.Quartiles.Third
        Me.Minimum = Me.Statistics.Sample.Minimum
        Me.Maximum = Me.Statistics.Sample.Maximum
        Me.FenceFactor = Me.Statistics.FenceFactor
        Me.LowerOutlierLimit = Me.Statistics.Fence.LowEndPoint
        Me.UpperOutlierLimit = Me.Statistics.Fence.HighEndPoint
        Me.Cp = Me.EstimateCp
        Me.Cpk = Me.EstimateCpk
        Me.Cpm = Me.EstimateCpm
    End Sub

    ''' <summary> Adds an item. </summary>
    ''' <remarks> David, 7/7/2020. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub AddItem(ByVal value As Double?)
        If value.HasValue Then Me.Statistics.Sample.AddValue(value.Value)
    End Sub

#End Region

#Region " ADDITIONAL PROPERTIES "

    Private _CertifiedQuantity As Integer

    ''' <summary> Gets or sets the certified quantity. </summary>
    ''' <value> The certified quantity. </value>
    Public Property CertifiedQuantity As Integer
        Get
            Return Me._CertifiedQuantity
        End Get
        Set
            If Value <> Me.CertifiedQuantity Then
                Me._CertifiedQuantity = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " ELEMENT PROPERTIES "

    Private _ElementLabel As String

    ''' <summary> Gets or sets the Label of the Element. </summary>
    ''' <value> The Label of the Element. </value>
    Public Property ElementLabel As String
        Get
            Return Me._ElementLabel
        End Get
        Set
            If Not String.Equals(Value, Me.ElementLabel) Then
                Me._ElementLabel = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _ElementType As ElementType

    ''' <summary> Gets or sets the type of the Element. </summary>
    ''' <value> The type of the Element. </value>
    Public Property ElementType As ElementType
        Get
            Return Me._ElementType
        End Get
        Set
            If Value <> Me.ElementType Then
                Me._ElementType = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _ElementOrdinalNumber As Integer

    ''' <summary> Gets or sets the Element Ordinal number. </summary>
    ''' <value> The ElementOrdinal number. </value>
    Public Property ElementOrdinalNumber As Integer
        Get
            Return Me._ElementOrdinalNumber
        End Get
        Set
            If Not Integer.Equals(Value, Me.ElementOrdinalNumber) Then
                Me._ElementOrdinalNumber = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property


#End Region

#Region " DERIVED TRAITS "

    Private _Multimeter As MultimeterEntity

    ''' <summary> Gets the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <value> The <see cref="Entities.MultimeterEntity"/>. </value>
    Public ReadOnly Property Multimeter As MultimeterEntity
        Get
            If Not (Me._Multimeter?.IsClean()).GetValueOrDefault(False) Then
                Me._Multimeter = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
            End If
            Return Me._Multimeter
        End Get
    End Property

    Private _MultimeterModel As MultimeterModelEntity

    ''' <summary> Gets the Meter Model entity. </summary>
    ''' <value> The Meter Model entity. </value>
    Public ReadOnly Property MultimeterModel As MultimeterModelEntity
        Get
            If Not (Me._MultimeterModel?.IsClean()).GetValueOrDefault(False) Then
                Me._MultimeterModel = MultimeterModelEntity.EntityLookupDictionary(Me.Multimeter.MultimeterModelId)
            End If
            Return Me._MultimeterModel
        End Get
    End Property

#End Region

#Region " PRODUCT SPECIFICATION  "

    ''' <summary> Gets or sets the nominal value. </summary>
    ''' <value> The nominal value. </value>
    Public Property NominalValue As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets target value. </summary>
    ''' <value> The target value. </value>
    Public Property TargetValue As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the lower specification limit. </summary>
    ''' <value> The lower specification limit. </value>
    Public Property LowerSpecificationLimit As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the upper specification limit. </summary>
    ''' <value> The upper specification limit. </value>
    Public Property UpperSpecificationLimit As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

#End Region

#Region " SAMPLE QUARTILE COUNTS "

    Public Property SampleCount As Integer?
        Get
            Return CInt(Me.TraitValue)
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    Public Property OutlierCount As Integer?
        Get
            Return CInt(Me.TraitValue)
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

#End Region

#Region " SAMPLE STATISTICS "

    ''' <summary> Gets or sets the mean. </summary>
    ''' <value> The mean value. </value>
    Public Property Mean As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Mean percent caption. </summary>
    ''' <remarks> David, 6/11/2020. </remarks>
    ''' <param name="numberFormatInfo"> Number of format information <see cref="NomTrait.PercentFormatInfo"/>. </param>
    ''' <returns> A String. </returns>
    Public Function MeanPercentCaption(ByVal numberFormatInfo As IFormatProvider) As String
        Return If(Me.SampleCount > 0 AndAlso Me.Mean.HasValue, Me.Mean.Value.ToString("P", numberFormatInfo), String.Empty)
    End Function

    ''' <summary> Mean percent caption. </summary>
    ''' <remarks> David, 6/11/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Function MeanPercentCaption() As String
        Return Me.MeanPercentCaption(Me.PercentFormatInfo)
    End Function

    ''' <summary> Gets or sets the sigma. </summary>
    ''' <value> The sigma. </value>
    Public Property Sigma As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Standard deviation percent caption. </summary>
    ''' <remarks> David, 6/11/2020. </remarks>
    ''' <param name="numberFormatInfo"> Number of format information
    '''                                 <see cref="NomTrait.PercentFormatInfo"/>. </param>
    ''' <returns> A String. </returns>
    Public Function SigmaPercentCaption(ByVal numberFormatInfo As IFormatProvider) As String
        Return If(Me.SampleCount >= 3 AndAlso Me.Sigma.HasValue, Me.Sigma.Value.ToString("P", numberFormatInfo), String.Empty)
    End Function

    ''' <summary> Standard deviation percent caption. </summary>
    ''' <remarks> David, 6/11/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Function SigmaPercentCaption() As String
        Return Me.SigmaPercentCaption(Me.PercentFormatInfo)
    End Function


    ''' <summary> Gets or sets the first quartile. </summary>
    ''' <value> The first quartile. </value>
    Public Property FirstQuartile As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the median. </summary>
    ''' <value> The median value. </value>
    Public Property Median As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the third quartile. </summary>
    ''' <value> The third quartile. </value>
    Public Property ThirdQuartile As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the minimum. </summary>
    ''' <value> The minimum value. </value>
    Public Property Minimum As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the maximum. </summary>
    ''' <value> The maximum value. </value>
    Public Property Maximum As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the fence factor. </summary>
    ''' <value> The fence factor. </value>
    Public Property FenceFactor As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the lower outlier limit. </summary>
    ''' <value> The lower outlier limit. </value>
    Public Property LowerOutlierLimit As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the upper outlier limit. </summary>
    ''' <value> The upper outlier limit. </value>
    Public Property UpperOutlierLimit As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

#End Region

#Region " PROCESS CAPABILITY "

    ''' <summary> Gets or sets the minimum Cp (process capability for a centered process). </summary>
    ''' <value> The minimum Cp. </value>
    Public Property MinimumCp As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the minimum Cpk (process capability for a process that is not centered).
    ''' </summary>
    ''' <value> The minimum Cpk. </value>
    Public Property MinimumCpk As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the minimum Cpm (process capability with bias consideration). </summary>
    ''' <value> The minimum Cpm. </value>
    Public Property MinimumCpm As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> The sigma level. </summary>
    Private _SigmaLevel As Double = 6

    ''' <summary> Gets or sets the sigma level. </summary>
    ''' <value> The sigma level. </value>
    Public Property SigmaLevel As Double
        Get
            Return Me._SigmaLevel
        End Get
        Set(value As Double)
            If Not value = Me.SigmaLevel Then
                Me._SigmaLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the cp. </summary>
    ''' <value> The cp. </value>
    Public Property Cp As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Estimate cp. </summary>
    ''' <remarks> David, 5/28/2020. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateCp() As Double?
        Return If(Me.Sigma.GetValueOrDefault(0) > 0, (Me.UpperSpecificationLimit - Me.LowerSpecificationLimit) / (Me.SigmaLevel * Me.Sigma), New Double?)
    End Function

    ''' <summary> Gets or sets the cpk. </summary>
    ''' <value> The cpk. </value>
    Public Property Cpk As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Estimate cpk. </summary>
    ''' <remarks> David, 5/28/2020. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateCpk() As Double?
        Return If(Me.Sigma.GetValueOrDefault(0) > 0,
                         If((Me.UpperSpecificationLimit - Me.Mean) < (Me.Mean - Me.LowerSpecificationLimit),
                                 (Me.UpperSpecificationLimit - Me.Mean),
                                 (Me.Mean - Me.LowerSpecificationLimit)) /
                         (0.5 * Me.SigmaLevel * Me.Sigma),
                    New Double?)
    End Function

    ''' <summary> Gets or sets the cpm. </summary>
    ''' <value> The cpm. </value>
    Public Property Cpm As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Estimate cpm. </summary>
    ''' <remarks> David, 5/28/2020. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateCpm() As Double?
        Return If((Me.Sigma.GetValueOrDefault(0) > 0) AndAlso Me.Mean.HasValue AndAlso Me.TargetValue.HasValue,
                      Me.Cp / Math.Sqrt(1 + Math.Pow((Me.Mean.Value - Me.TargetValue.Value) / Me.Sigma.Value, 2)),
                      New Double?)
    End Function

#End Region

#Region " PROCESS TUNE "

    ''' <summary> Gets or sets the initial process nominal. </summary>
    ''' <value> The initial process nominal. </value>
    Public Property InitialProcessNominal As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the process nominal. </summary>
    ''' <value> The process nominal. </value>
    Public Property ProcessNominal As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the process target. </summary>
    ''' <value> The process target. </value>
    Public Property ProcessTarget As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the process mean. </summary>
    ''' <value> The process mean. </value>
    Public Property ProcessMean As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the process error. </summary>
    ''' <value> The process error. </value>
    Public Property ProcessError As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Estimate process error. </summary>
    ''' <remarks> David, 2020-05-29. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateProcessError() As Double?
        Return Me.Mean - Me.ProcessTarget
    End Function

    ''' <summary> Gets or sets the adjusted process nominal. </summary>
    ''' <value> The adjusted process nominal. </value>
    Public Property AdjustedProcessNominal As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Estimate adjusted process nominal. </summary>
    ''' <remarks> David, 2020-05-29. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateAdjustedProcessNominal() As Double?
        Return Me.ProcessNominal - Me.ProcessError
    End Function

    ''' <summary> Gets or sets the Process Bias (deltum standard). </summary>
    ''' <value> The Process Bias (deltum standard). </value>
    Public Property ProcessBias As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Estimate process bias. </summary>
    ''' <remarks> David, 5/28/2020. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateProcessBias() As Double?
        Return Me.NominalValue - Me.ProcessTarget
    End Function

    ''' <summary> Gets or sets the average (actual) process bias. </summary>
    ''' <value> The average (actual) process bias. </value>
    Public Property ProcessBiasMean As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Estimate process bias mean. </summary>
    ''' <remarks> David, 2020-05-29. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateProcessBiasMean() As Double?
        Return Me.Mean - Me.ProcessMean
    End Function

    ''' <summary> Gets or sets the Process Bias (deltum) error. </summary>
    ''' <value> The deltum error. </value>
    Public Property ProcessBiasError As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Estimate process bias error. </summary>
    ''' <remarks> David, 5/28/2020. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateProcessBiasError() As Double?
        Return Me.ProcessBiasMean - Me.ProcessBias
    End Function


#End Region

#Region " CUSTOM PROPERTIES "

    Private _SpecificationLimit As Double?

    ''' <summary> Gets the specification limit. </summary>
    ''' <value> The specification limit. </value>
    Public Property SpecificationLimit As Double?
        Get
            Return Me._SpecificationLimit
        End Get
        Set
            If Not Double?.Equals(Value, Me.SpecificationLimit) Then
                Me._SpecificationLimit = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the decimal precision. </summary>
    ''' <value> The decimal precision. </value>
    Public ReadOnly Property DecimalPrecision As Integer
        Get
            Dim lowestTolerance As Double = 0.00005
            Dim precision As Double = 1 / Math.Max(lowestTolerance, Me.SpecificationLimit.GetValueOrDefault(lowestTolerance))
            Return If(precision <= 1, 0, CInt(Math.Floor(Math.Log10(precision))))
        End Get
    End Property

    Private _PercentFormatInfo As Globalization.NumberFormatInfo

    ''' <summary> Number format informations for displaying values with the set specification limit. </summary>
    Public ReadOnly Property PercentFormatInfo As Globalization.NumberFormatInfo
        Get
            If Me._PercentFormatInfo Is Nothing Then
                Me._PercentFormatInfo = New Globalization.NumberFormatInfo With {.PercentDecimalDigits = Me.DecimalPrecision}
            End If
            Return Me._PercentFormatInfo
        End Get
    End Property

    Private _NumberFormatInfo As Globalization.NumberFormatInfo

    ''' <summary> Number format informations for displaying values with the set specification limit. </summary>
    Public ReadOnly Property NumberFormatInfo As Globalization.NumberFormatInfo
        Get
            If Me._NumberFormatInfo Is Nothing Then
                Me._NumberFormatInfo = New Globalization.NumberFormatInfo With {.PercentDecimalDigits = Me.DecimalPrecision}
                Me._NumberFormatInfo.NumberDecimalDigits = Me._NumberFormatInfo.PercentDecimalDigits + 2
            End If
            Return Me._NumberFormatInfo
        End Get
    End Property

    Private _DateTimeFormatInfo As Globalization.DateTimeFormatInfo

    ''' <summary> DateTime format informations for displaying values with the set specification limit. </summary>
    Public ReadOnly Property DateTimeFormatInfo As Globalization.DateTimeFormatInfo
        Get
            If Me._DateTimeFormatInfo Is Nothing Then
                Me._DateTimeFormatInfo = New Globalization.DateTimeFormatInfo With {.ShortTimePattern = "HH:MM:ss.fff"}
            End If
            Return Me._DateTimeFormatInfo
        End Get
    End Property

#End Region

End Class

''' <summary> A multimeter nominal reading selector. </summary>
''' <remarks> David, 6/29/2020. </remarks>
Public Structure SampleTraitSelector
    Implements IEquatable(Of SampleTraitSelector)

    Public Sub New(ByVal attribute As SampleTrait)
        Me.New(attribute.MultimeterId, attribute.ElementAutoId, attribute.NomType)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/1/2020. </remarks>
    ''' <param name="multimeterId">  Identifier for the <see cref="Entities.MultimeterEntity"/>. </param>
    ''' <param name="elementAutoId"> Identifier for the <see cref="ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifier for the <see cref="NomTypeEntity"/>. </param>
    Public Sub New(ByVal multimeterId As Integer, ByVal elementAutoId As Integer, ByVal nomTypeId As Integer)
        Me.MultimeterId = multimeterId
        Me.ElementAutoId = elementAutoId
        Me.NomTypeId = nomTypeId
    End Sub

    Public Property MultimeterId As Integer

    Public Property ElementAutoId As Integer

    Public Property NomTypeId As Integer

    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.Equals(CType(obj, SampleTraitSelector))
    End Function

    Public Overloads Function Equals(other As SampleTraitSelector) As Boolean Implements IEquatable(Of SampleTraitSelector).Equals
        Return Me.MultimeterId = other.MultimeterId AndAlso Me.NomTypeId = other.NomTypeId AndAlso Me.ElementAutoId = other.ElementAutoId
    End Function

    Public Overrides Function GetHashCode() As Integer
        Return Me.MultimeterId Xor Me.NomTypeId Xor Me.ElementAutoId
    End Function

    Public Shared Operator =(left As SampleTraitSelector, right As SampleTraitSelector) As Boolean
        Return left.Equals(right)
    End Operator

    Public Shared Operator <>(left As SampleTraitSelector, right As SampleTraitSelector) As Boolean
        Return Not left = right
    End Operator

End Structure
