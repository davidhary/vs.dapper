''' <summary> A yield Real(Double)-Valued <see cref="isr.Core.Constructs.IGetterSetter(Of TValue)"/> summary values. </summary>
''' <remarks> David, 2020-05-29. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Class YieldTrait
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSestter"> The getter setter. </param>
    Public Sub New(ByVal getterSestter As isr.Core.Constructs.IGetterSetter(Of Double))
        MyBase.New()
        Me.GetterSetter = getterSestter
        Me._BucketBinCounters = New Dictionary(Of Integer, Integer)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/29/2020. </remarks>
    ''' <param name="getterSestter"> The getter setter. </param>
    ''' <param name="multimeter">    The <see cref="Entities.MultimeterEntity"/>. </param>
    Public Sub New(ByVal getterSestter As isr.Core.Constructs.IGetterSetter(Of Double), ByVal multimeter As MultimeterEntity, ByVal certifiedQuantity As Integer)
        Me.New(getterSestter)
        Me.MultimeterId = multimeter.Id
        Me.CertifiedQuantity = certifiedQuantity
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter(Of Double)

    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.GetterSetter.Setter(value.Value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

#End Region

#Region " ADD ITEM(S) "

    ''' <summary> Adds an item. </summary>
    ''' <remarks> David, 2020-05-29. </remarks>
    Public Sub AddItem(ByVal uut As UutEntity)
        If Not uut.ProductSortEntity.Traits.ProductSortTrait.BucketBin.HasValue Then
            Throw New InvalidOperationException($"{NameOf(Entities.ProductSortEntity)} {NameOf(ProductSortTrait.BucketBin)} was not set")
        End If
        Dim bucketBin As BucketBin = BucketBinEntity.ToBucketBin(uut.ProductSortEntity.Traits.ProductSortTrait.BucketBin.Value)
        If Not Me.YieldDecimalPlaces.HasValue Then Me.YieldDecimalPlaces = 1
        Me.TotalCount += 1
        If bucketBin = BucketBin.Good Then
            Me.GoodCount += 1
        ElseIf BucketBin = BucketBin.Bad Then
            Me.BadCount += 1
        Else
            Me.InvalidCount += 1
        End If
        If Me.BucketBinCounters.ContainsKey(bucketBin) Then
            Me.BucketBinCounters(bucketBin) += 1
        Else
            Me.BucketBinCounters.Add(bucketBin, 1)
        End If
        Me.Yield = Me.EstimateYield
        Me.ValidYield = Me.EstimateValidYield
    End Sub

#End Region

#Region " BUCKET BIN COUNTS "

    ''' <summary> Gets or sets the bucket bin counters. </summary>
    ''' <value> The bucket bin counters. </value>
    Public ReadOnly Property BucketBinCounters As IDictionary(Of Integer, Integer)

    ''' <summary> Hourly rate. </summary>
    ''' <remarks> David, 7/7/2020. </remarks>
    ''' <param name="elapsed"> The elapsed. </param>
    ''' <returns> A Double. </returns>
    Public Function HourlyRate(ByVal elapsed As TimeSpan) As Double
        Return If(elapsed.TotalSeconds > 0, 3600 * Me.GoodCount.GetValueOrDefault(0) / elapsed.TotalSeconds, 0)
    End Function

#End Region

#Region " ADDITIONAL PROPERTIES "

    Private _CertifiedQuantity As Integer

    ''' <summary> Gets or sets the certified quantity. </summary>
    ''' <value> The certified quantity. </value>
    Public Property CertifiedQuantity As Integer
        Get
            Return Me._CertifiedQuantity
        End Get
        Set
            If Value <> Me.CertifiedQuantity Then
                Me._CertifiedQuantity = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region


#Region " IDENTIFIERS "

    Private _MultimeterId As Integer

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer
        Get
            Return Me._MultimeterId
        End Get
        Set
            If Value <> Me.MultimeterId Then
                Me._MultimeterId = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " DERIVED TRAITS "

    Private _Multimeter As MultimeterEntity

    ''' <summary> Gets the <see cref="Entities.MultimeterEntity"/>. </summary>
    ''' <value> The <see cref="Entities.MultimeterEntity"/>. </value>
    Public ReadOnly Property Multimeter As MultimeterEntity
        Get
            If Not (Me._Multimeter?.IsClean()).GetValueOrDefault(False) Then
                Me._Multimeter = MultimeterEntity.EntityLookupDictionary(Me.MultimeterId)
            End If
            Return Me._Multimeter
        End Get
    End Property

    Private _MultimeterModel As MultimeterModelEntity

    ''' <summary> Gets the Meter Model entity. </summary>
    ''' <value> The Meter Model entity. </value>
    Public ReadOnly Property MultimeterModel As MultimeterModelEntity
        Get
            If Not (Me._MultimeterModel?.IsClean()).GetValueOrDefault(False) Then
                Me._MultimeterModel = MultimeterModelEntity.EntityLookupDictionary(Me.Multimeter.MultimeterModelId)
            End If
            Return Me._MultimeterModel
        End Get
    End Property

#End Region

#Region " YIELD COUNTS "

    Public Sub Clear()
        Me.TotalCount = New Integer?
        Me.InvalidCount = New Integer?
        Me.GoodCount = New Integer?
        Me.BadCount = New Integer?
        Me.Yield = New Integer?
        Me.ValidYield = New Integer?
    End Sub

    ''' <summary> Gets or sets the number of totals. </summary>
    ''' <value> The total number of count. </value>
    Public Property TotalCount As Integer?
        Get
            Return CInt(Me.TraitValue)
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the number of invalids. </summary>
    ''' <value> The number of invalids. </value>
    Public Property InvalidCount As Integer?
        Get
            Return CInt(Me.TraitValue)
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the number of goods. </summary>
    ''' <value> The number of goods. </value>
    Public Property GoodCount As Integer?
        Get
            Return CInt(Me.TraitValue)
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the number of failures. </summary>
    ''' <value> The number of failures. </value>
    Public Property BadCount As Integer?
        Get
            Return CInt(Me.TraitValue)
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets the number of valid readings. </summary>
    ''' <value> The number of valid readings. </value>
    Public ReadOnly Property ValidCount As Integer?
        Get
            Return Me.TotalCount - Me.InvalidCount
        End Get
    End Property

    ''' <summary> Estimate yield. </summary>
    ''' <remarks> David, 5/28/2020. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateYield() As Double?
        Return If(Me.TotalCount.GetValueOrDefault(0) > 0, Me.GoodCount / Me.TotalCount, New Double?)
    End Function

    ''' <summary> Estimate valid yield. </summary>
    ''' <remarks> David, 5/28/2020. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateValidYield() As Double?
        Return If(Me.ValidCount.GetValueOrDefault(0) > 0, Me.GoodCount / Me.ValidCount, New Double?)
    End Function

#End Region

#Region " YIELD "

    ''' <summary> Evaluates this object. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    Public Sub Evaluate()
        Me.Yield = Me.EstimateYield()
        Me.ValidYield = Me.EstimateValidYield
    End Sub

    Public Property YieldDecimalPlaces As Integer?
        Get
            Return CInt(Me.TraitValue)
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the yield. </summary>
    ''' <value> The yield. </value>
    Public Property Yield As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the valid yield. </summary>
    ''' <value> The valid yield. </value>
    Public Property ValidYield As Double?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Double?)
            Me.TraitValue = value
        End Set
    End Property

#End Region

End Class

