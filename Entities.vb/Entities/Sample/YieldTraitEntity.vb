Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Yield Trait builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class YieldTraitBuilder
    Inherits KeyForeignRealBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return YieldTraitBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(YieldTraitBuilder._TableName) Then
                YieldTraitBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(YieldTraitNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the first foreign reference table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the first foreign reference table. </value>
    Public Overrides Property ForeignTableName As String = YieldTraitTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the first foreign reference table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the first foreign reference table key. </value>
    Public Overrides Property ForeignTableKeyName As String = NameOf(YieldTraitTypeNub.Id)

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The new table. </returns>
    Public Overloads Function CreateTable(connection As System.Data.IDbConnection) As String
        ' the same Element ID and Yield Type would repeat for the NUT associated with the next UUT.
        Return MyBase.CreateTable(connection, UniqueIndexOptions.None)
    End Function

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(YieldTraitEntity.AutoId)

    ''' <summary> Gets or sets the name of the foreign identifier field. </summary>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName() As String = NameOf(YieldTraitEntity.YieldTraitTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName() As String = NameOf(YieldTraitEntity.Amount)


#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As YieldTraitBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As YieldTraitBuilder
        If YieldTraitBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                YieldTraitBuilder._Instance = New YieldTraitBuilder()
            End SyncLock
        End If
        Return YieldTraitBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="YieldTraitEntity"/> <see cref="IKeyForeignReal">interface</see>.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("YieldTrait")>
Public Class YieldTraitNub
    Inherits KeyForeignRealNub
    Implements IKeyForeignReal

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IKeyForeignReal
        Return New YieldTraitNub
    End Function

End Class

''' <summary>
''' The <see cref="YieldTraitEntity"/> storing Real(Double)-Valued Element Yield Traits.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
''' David, 4/22/2020 </para></remarks>
Public Class YieldTraitEntity
    Inherits EntityBase(Of IKeyForeignReal, YieldTraitNub)
    Implements IKeyForeignReal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New YieldTraitNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> the <see cref="Entities.ElementEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IKeyForeignReal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyForeignReal, ByVal store As IKeyForeignReal)
        MyBase.New(New YieldTraitNub, cache, store)
        Me.UsingNativeTracking = String.Equals(YieldTraitBuilder.TableName, NameOf(IKeyForeignReal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IKeyForeignReal
        Return New YieldTraitNub
    End Function

    Public Overrides Function CreateCopy() As IKeyForeignReal
        Dim destination As IKeyForeignReal = Me.CreateNew
        YieldTraitNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="YieldTraitEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IKeyForeignReal)
        YieldTraitNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> the <see cref="Entities.ElementEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyForeignReal)
        ' first make the copy to notify of any property change.
        YieldTraitNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The <see cref="YieldTraitEntity"/> primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyForeignReal)(key), connection.Get(Of YieldTraitNub)(key)))
    End Function

    ''' <summary> Refetch; Fetches using the primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/15/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="autoId">            Identifies the <see cref="YieldTraitEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, autoId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyForeignReal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If YieldTraitEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.AutoId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="autoId">            Identifies the <see cref="YieldTraitEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Boolean
        Return connection.Delete(Of YieldTraitNub)(New YieldTraitNub With {.AutoId = autoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or <see cref="YieldTraitEntity"/>'s. </summary>
    ''' <value> The <see cref="YieldTraitEntity"/>'s. </value>
    Public ReadOnly Property YieldTraits As IEnumerable(Of YieldTraitEntity)

    ''' <summary> Fetches all records into <see cref="YieldTraitEntity"/>'s. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of YieldTraitEntity)
        Return If(usingNativeTracking, YieldTraitEntity.Populate(connection.GetAll(Of IKeyForeignReal)),
                                       YieldTraitEntity.Populate(connection.GetAll(Of YieldTraitNub)))
    End Function

    ''' <summary> Fetches all records into <see cref="YieldTraitEntity"/>'s. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._YieldTraits = YieldTraitEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(YieldTraitEntity.YieldTraits))
        Return If(Me.YieldTraits?.Any, Me.YieldTraits.Count, 0)
    End Function

    ''' <summary> Populates a list of <see cref="YieldTraitEntity"/>'s. </summary>
    ''' <param name="nubs"> the <see cref="Entities.ElementEntity"/>Value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of YieldTraitNub)) As IEnumerable(Of YieldTraitEntity)
        Dim l As New List(Of YieldTraitEntity)
        If nubs?.Any Then
            For Each nub As YieldTraitNub In nubs
                l.Add(New YieldTraitEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of <see cref="YieldTraitEntity"/>'s. </summary>
    ''' <param name="interfaces"> the <see cref="Entities.ElementEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyForeignReal)) As IEnumerable(Of YieldTraitEntity)
        Dim l As New List(Of YieldTraitEntity)
        If interfaces?.Any Then
            Dim nub As New YieldTraitNub
            For Each iFace As IKeyForeignReal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New YieldTraitEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count element Yield Traits; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="YieldTraitEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{YieldTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(YieldTraitNub.AutoId)} = @AutoId", New With {.AutoId = autoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches element Yield Traits by Element number and element Yield Trait element
    ''' Yield Trait Real Value type;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="YieldTraitEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As IEnumerable(Of YieldTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{YieldTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(YieldTraitNub.AutoId)} = @AutoId", New With {.AutoId = autoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of YieldTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches element Yield Traits by <see cref="YieldTraitEntity"/> id; expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="YieldTraitEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As IEnumerable(Of YieldTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{YieldTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(YieldTraitEntity.AutoId)} = @AutoId", New With {.AutoId = autoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of YieldTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.ElementEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="YieldTraitEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Boolean
        Return 1 = YieldTraitEntity.CountEntities(connection, autoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary>
    ''' Fetches element Yield Real(Double)-Value trait values by Element auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">          The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchYieldTraits(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of YieldTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{YieldTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(YieldTraitNub.AutoId)} = @AutoId", New With {Me.AutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of YieldTraitNub)(selector.RawSql, selector.Parameters)
    End Function

#End Region

#Region " RELATIONS: YIELD TRAIT TYPE "

    ''' <summary> Gets or sets the <see cref="Entities.YieldTraitTypeEntity"/> . </summary>
    ''' <value> the <see cref="Entities.YieldTraitTypeEntity"/> . </value>
    Public ReadOnly Property YieldTraitTypeEntity As YieldTraitTypeEntity

    ''' <summary> Fetches a <see cref="Entities.YieldTraitTypeEntity"/>. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchYieldTraitTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._YieldTraitTypeEntity = New YieldTraitTypeEntity()
        Return Me.YieldTraitTypeEntity.FetchUsingKey(connection, Me.YieldTraitTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public Property AutoId As Integer Implements IKeyForeignReal.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the first foreign key. </summary>
    ''' <value> Identifies the first foreign key. </value>
    Public Property ForeignId As Integer Implements IKeyForeignReal.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(YieldTraitEntity.YieldTraitTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the type of the <see cref="YieldTraitEntity"/> Real(Double)-value. </summary>
    ''' <value> The type of the <see cref="YieldTraitEntity"/> Real(Double)-value. </value>
    Public Property YieldTraitTypeId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(ByVal value As Integer)
            Me.ForeignId = value
        End Set
    End Property

    ''' <summary> Gets or sets a Real(Double)-value assigned to the <see cref="YieldTraitEntity"/> for the specific <see cref="Entities.YieldTraitTypeEntity"/>. </summary>
    ''' <value> The Real(Double)-value assigned to the <see cref="YieldTraitEntity"/> for the specific <see cref="Entities.YieldTraitTypeEntity"/>. </value>
    Public Property Amount As Double Implements IKeyForeignReal.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(ByVal value As Double)
            If Not Double.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The entity selector. </value>
    Public ReadOnly Property EntitySelector As DualKeySelector
        Get
            Return New DualKeySelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary> Collection of <see cref="YieldTraitEntity"/>'s. </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class YieldTraitEntityCollection
    Inherits EntityKeyedCollection(Of DualKeySelector, IKeyForeignReal, YieldTraitNub, YieldTraitEntity)

    Protected Overrides Function GetKeyForItem(item As YieldTraitEntity) As DualKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As YieldTraitEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of YieldTraitEntity))
        If entities?.Any Then
            For Each entity As YieldTraitEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return YieldTraitBuilder.Get.Upsert(connection, Me)
    End Function

End Class

