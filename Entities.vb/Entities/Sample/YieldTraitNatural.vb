''' <summary> Yield Natural(Integer)-Value summary. </summary>
''' <remarks> David, 2020-05-29. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Class YieldTraitNatural
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSestter"> The getter setter. </param>
    Public Sub New(ByVal getterSestter As isr.Core.Constructs.IGetterSetter(Of Integer))
        MyBase.New()
        Me.GetterSetter = getterSestter
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter(Of Integer)

    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As Integer?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.GetterSetter.Setter(value.Value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property

#End Region

#Region " ADD ITEM(S) "

    ''' <summary> Adds an item. </summary>
    ''' <remarks> David, 2020-05-29. </remarks>
    ''' <param name="isGood">         True if is good, false if not. </param>
    ''' <param name="isValidReading"> True if is valid reading, false if not. </param>
    Public Sub AddItem(ByVal isGood As Boolean, ByVal isValidReading As Boolean)
        If Not Me.YieldDecimalPlaces.HasValue Then Me.YieldDecimalPlaces = 1
        Me.TotalCount += 1
        If Not isValidReading Then
            Me.InvalidCount += 1
        ElseIf isGood Then
            Me.GoodCount += 1
        Else
            Me.FailureCount += 1
        End If
        Me.Yield = Me.EstimateYield
        Me.ValidYield = Me.EstimateValidYield
    End Sub

#End Region

#Region " YIELD COUNTS "

    ''' <summary> Gets or sets the number of totals. </summary>
    ''' <value> The total number of count. </value>
    Public Property TotalCount As Integer?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the number of invalids. </summary>
    ''' <value> The number of invalids. </value>
    Public Property InvalidCount As Integer?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the number of goods. </summary>
    ''' <value> The number of goods. </value>
    Public Property GoodCount As Integer?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the number of failures. </summary>
    ''' <value> The number of failures. </value>
    Public Property FailureCount As Integer?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets the number of valid readings. </summary>
    ''' <value> The number of valid readings. </value>
    Public ReadOnly Property ValidCount As Integer?
        Get
            Return Me.TotalCount - Me.InvalidCount
        End Get
    End Property

    ''' <summary> Estimate yield. </summary>
    ''' <remarks> David, 5/28/2020. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateYield() As Double?
        Return If(Me.TotalCount.GetValueOrDefault(0) > 0, Me.GoodCount / Me.TotalCount, New Double?)
    End Function

    ''' <summary> Estimate valid yield. </summary>
    ''' <remarks> David, 5/28/2020. </remarks>
    ''' <returns> A Double? </returns>
    Public Function EstimateValidYield() As Double?
        Return If(Me.ValidCount.GetValueOrDefault(0) > 0, Me.GoodCount / Me.ValidCount, New Double?)
    End Function

#End Region

#Region " YIELD "

    Public Property YieldDecimalPlaces As Integer?
        Get
            Return Me.TraitValue
        End Get
        Set(value As Integer?)
            Me.TraitValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the yield. </summary>
    ''' <value> The yield. </value>
    Public Property Yield As Double?
        Get
            Return Me.TraitValue / Math.Pow(10, Me.YieldDecimalPlaces.GetValueOrDefault(3))
        End Get
        Set(value As Double?)
            Me.TraitValue = CInt(value * Math.Pow(10, Me.YieldDecimalPlaces.GetValueOrDefault(3)))
        End Set
    End Property

    ''' <summary> Gets or sets the valid yield. </summary>
    ''' <value> The valid yield. </value>
    Public Property ValidYield As Double?
        Get
            Return Me.TraitValue / Math.Pow(10, Me.YieldDecimalPlaces.GetValueOrDefault(3))
        End Get
        Set(value As Double?)
            Me.TraitValue = CInt(value * Math.Pow(10, Me.YieldDecimalPlaces.GetValueOrDefault(3)))
        End Set
    End Property

#End Region

End Class
