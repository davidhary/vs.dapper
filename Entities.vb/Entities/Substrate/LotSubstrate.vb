Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Lot-Substrate builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class LotSubstrateBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return LotSubstrateBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(LotSubstrateBuilder._TableName) Then
                LotSubstrateBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(LotSubstrateNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = LotBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(LotNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = SubstrateBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(SubstrateNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(LotSubstrateEntity.LotAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(LotSubstrateEntity.SubstrateAutoId)


#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As LotSubstrateBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As LotSubstrateBuilder
        If LotSubstrateBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                LotSubstrateBuilder._Instance = New LotSubstrateBuilder()
            End SyncLock
        End If
        Return LotSubstrateBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Lot Substrate Nub based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("LotSubstrate")>
Public Class LotSubstrateNub
    Inherits OneToManyNub
    Implements IOneToMany

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToMany
        Return New LotSubstrateNub
    End Function

End Class

''' <summary>
''' The Lot-Substrate Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class LotSubstrateEntity
    Inherits EntityBase(Of IOneToMany, LotSubstrateNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New LotSubstrateNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Lot-Substrate interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New LotSubstrateNub, cache, store)
        Me.UsingNativeTracking = String.Equals(LotSubstrateBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToMany
        Return New LotSubstrateNub
    End Function

    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        LotSubstrateNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        LotSubstrateNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Lot-Substrate interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        LotSubstrateNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="substrateAutoId">      Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal substrateAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As LotSubstrateNub = LotSubstrateEntity.FetchNubs(connection, lotAutoId, substrateAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.LotAutoId, Me.SubstrateAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.LotAutoId, Me.SubstrateAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="lotAutoId">       Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal substrateAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, lotAutoId, substrateAutoId)
    End Function

    ''' <summary>
    ''' Attempts to insert or retrieve a <see cref="LotSubstrateEntity"/> from the given data.
    ''' Specifying new <paramref name="lotNumber"/> add this lot; Specifying a new
    ''' <paramref name="substrateNumber"/> adds a new substrate with the next substrate number for
    ''' this lot. Updates the <see cref="LotSubstrateEntity.SubstrateEntity"/> and
    ''' <see cref="LotSubstrateEntity.LotEntity"/>
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="lotNumber">       The lot number. </param>
    ''' <param name="substrateNumber"> The substrate number. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String, ByVal substrateNumber As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._LotEntity = New LotEntity With {.LotNumber = lotNumber}
        Me._SubstrateEntity = New SubstrateEntity With {.SubstrateNumber = substrateNumber}
        If Me.LotEntity.Obtain(connection) Then
            If 0 < substrateNumber Then
                If Not Me.SubstrateEntity.FetchUsingUniqueIndex(connection) Then
                    result = (False, $"Failed fetching existing {NameOf(Entities.SubstrateEntity)} with {NameOf(Entities.SubstrateEntity.SubstrateNumber)} of {substrateNumber}")
                End If
            Else
                Me._SubstrateEntity = LotSubstrateEntity.FetchLastSubstrate(connection, Me.LotEntity.AutoId)
                substrateNumber = If(Me.SubstrateEntity.IsClean, Me.SubstrateEntity.SubstrateNumber + 1, 1)
                Me._SubstrateEntity = New SubstrateEntity() With {.SubstrateNumber = substrateNumber}
                If Not Me.SubstrateEntity.Obtain(connection) Then
                    result = (False, $"Failed obtaining {NameOf(Entities.SubstrateEntity)} with {NameOf(Entities.SubstrateEntity.SubstrateNumber)} of {substrateNumber}")
                End If
            End If
        Else
            result = (False, $"Failed obtaining {NameOf(Entities.LotEntity)} with {NameOf(Entities.LotEntity.LotNumber)} of {lotNumber}")
        End If
        If result.Success Then
            Me.LotAutoId = Me.LotEntity.AutoId
            Me.SubstrateAutoId = Me.SubstrateEntity.AutoId
            If Not Me.Obtain(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.LotSubstrateEntity)} for [{NameOf(Entities.LotEntity.LotNumber)}, {NameOf(Entities.SubstrateEntity.SubstrateNumber)}] of [{lotNumber},{substrateNumber}]")
            End If
        End If
        Me.NotifyPropertyChanged(NameOf(LotSubstrateEntity.SubstrateEntity))
        Me.NotifyPropertyChanged(NameOf(LotSubstrateEntity.LotEntity))
        Return result
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If LotSubstrateEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId">     Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal substrateAutoId As Integer) As Boolean
        Return connection.Delete(Of LotSubstrateNub)(New LotSubstrateNub With {.PrimaryId = lotAutoId, .SecondaryId = substrateAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Lot-Substrate entities. </summary>
    ''' <value> The Lot-Substrate entities. </value>
    Public ReadOnly Property LotSubstrates As IEnumerable(Of LotSubstrateEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of LotSubstrateEntity)
        Return If(usingNativeTracking, LotSubstrateEntity.Populate(connection.GetAll(Of IOneToMany)), LotSubstrateEntity.Populate(connection.GetAll(Of LotSubstrateNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._LotSubstrates = LotSubstrateEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(LotSubstrateEntity.LotSubstrates))
        Return If(Me.LotSubstrates?.Any, Me.LotSubstrates.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of LotSubstrateNub)) As IEnumerable(Of LotSubstrateEntity)
        Dim l As New List(Of LotSubstrateEntity)
        If nubs?.Any Then
            For Each nub As LotSubstrateNub In nubs
                l.Add(New LotSubstrateEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of LotSubstrateEntity)
        Dim l As New List(Of LotSubstrateEntity)
        If interfaces?.Any Then
            Dim nub As New LotSubstrateNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New LotSubstrateEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to Substrate entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotSubstrateBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSubstrateNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of LotSubstrateEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSubstrateBuilder.TableName}] WHERE {NameOf(LotSubstrateNub.PrimaryId)} = @Id", New With {Key .Id = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotSubstrateEntity.Populate(connection.Query(Of LotSubstrateNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Substrate. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns> The total number of entities by Substrate. </returns>
    Public Shared Function CountEntitiesBySubstrate(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotSubstrateBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSubstrateNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = substrateAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Substrates in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Substrates in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesBySubstrate(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As IEnumerable(Of LotSubstrateEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSubstrateBuilder.TableName}] WHERE {NameOf(LotSubstrateNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = substrateAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return LotSubstrateEntity.Populate(connection.Query(Of LotSubstrateNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="substrateAutoId">       Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal substrateAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{LotSubstrateBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSubstrateNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotSubstrateNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = substrateAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="substrateAutoId">       Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal substrateAutoId As Integer) As IEnumerable(Of LotSubstrateNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{LotSubstrateBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(LotSubstrateNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = lotAutoId})
        sqlBuilder.Where($"{NameOf(LotSubstrateNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = substrateAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of LotSubstrateNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Lot Substrate exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <param name="substrateAutoId">       Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer, ByVal substrateAutoId As Integer) As Boolean
        Return 1 = LotSubstrateEntity.CountEntities(connection, lotAutoId, substrateAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Lot entity. </summary>
    ''' <value> The Lot entity. </value>
    Public ReadOnly Property LotEntity As LotEntity

    ''' <summary> Fetches Lot Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Lot Entity. </returns>
    Public Function FetchLotEntity(ByVal connection As System.Data.IDbConnection) As LotEntity
        Dim entity As New LotEntity()
        entity.FetchUsingKey(connection, Me.LotAutoId)
        Me._LotEntity = entity
        Return entity
    End Function

    ''' <summary> Count lots associated with the specified <paramref name="substrateAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns> The total number of lots. </returns>
    Public Shared Function CountLots(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{LotSubstrateBuilder.TableName}] WHERE {NameOf(LotSubstrateNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = substrateAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Lots associated with the specified <paramref name="substrateAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Lots in this collection.
    ''' </returns>
    Public Shared Function FetchLots(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As IEnumerable(Of SubstrateEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSubstrateBuilder.TableName}] WHERE {NameOf(LotSubstrateNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = substrateAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of SubstrateEntity)
        For Each nub As LotSubstrateNub In connection.Query(Of LotSubstrateNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotSubstrateEntity(nub)
            l.Add(entity.FetchSubstrateEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all lots associated with the specified <paramref name="substrateAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteLots(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotSubstrateBuilder.TableName}] WHERE {NameOf(LotSubstrateNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = substrateAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the Substrate entity. </summary>
    ''' <value> The Substrate entity. </value>
    Public ReadOnly Property SubstrateEntity As SubstrateEntity

    ''' <summary> Fetches a Substrate Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Substrate Entity. </returns>
    Public Function FetchSubstrateEntity(ByVal connection As System.Data.IDbConnection) As SubstrateEntity
        Dim entity As New SubstrateEntity()
        entity.FetchUsingKey(connection, Me.SubstrateAutoId)
        Me._SubstrateEntity = entity
        Return entity
    End Function

    Public Shared Function CountSubstrates(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{LotSubstrateBuilder.TableName}] WHERE {NameOf(LotSubstrateNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Substrates in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Substrates in this collection.
    ''' </returns>
    Public Shared Function FetchSubstrates(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of SubstrateEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{LotSubstrateBuilder.TableName}] WHERE {NameOf(LotSubstrateNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of SubstrateEntity)
        For Each nub As LotSubstrateNub In connection.Query(Of LotSubstrateNub)(selector.RawSql, selector.Parameters)
            Dim entity As New LotSubstrateEntity(nub)
            l.Add(entity.FetchSubstrateEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Substrate related to the specified Lot. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotAutoId">  Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteSubstrates(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{LotSubstrateBuilder.TableName}] WHERE {NameOf(LotSubstrateNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered substrates in this collection. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered substrates in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedSubstrates(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer) As IEnumerable(Of SubstrateEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .Id = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return SubstrateEntity.Populate(connection.Query(Of SubstrateNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered substrates in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered substrates in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedSubstrates(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As IEnumerable(Of SubstrateEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotSubstrate] on [LotSubstrate].SecondaryId = [Substrate].AutoId where [LotSubstrate].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{SubstrateBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{SubstrateBuilder.TableName}] Inner Join [{LotSubstrateBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotSubstrateBuilder.TableName}].{NameOf(LotSubstrateNub.SecondaryId)} = [{SubstrateBuilder.TableName}].{NameOf(SubstrateNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{LotSubstrateBuilder.TableName}].{NameOf(LotSubstrateNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{SubstrateBuilder.TableName}].{NameOf(SubstrateNub.Amount)} ASC; ")
        Return LotSubstrateEntity.FetchOrderedSubstrates(connection, queryBuilder.ToString, lotAutoId)
    End Function

    ''' <summary> Fetches the last Substrate in this collection. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="lotAutoId">   Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Substrates in this collection.
    ''' </returns>
    Public Shared Function FetchFirstSubstrate(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal lotAutoId As Integer) As SubstrateEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery, New With {Key .Id = lotAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As SubstrateNub = connection.Query(Of SubstrateNub)(template.RawSql, template.Parameters).FirstOrDefault
        Return If(nub Is Nothing, New SubstrateEntity, New SubstrateEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches the last Substrate in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="lotAutoId"> Identifies the <see cref="Entities.LotEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Substrates in this collection.
    ''' </returns>
    Public Shared Function FetchLastSubstrate(ByVal connection As System.Data.IDbConnection, ByVal lotAutoId As Integer) As SubstrateEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [LotSubstrate] on [LotSubstrate].SecondaryId = [Substrate].AutoId where [LotSubstrate].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{SubstrateBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{SubstrateBuilder.TableName}] Inner Join [{LotSubstrateBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{LotSubstrateBuilder.TableName}].{NameOf(LotSubstrateNub.SecondaryId)} = [{SubstrateBuilder.TableName}].{NameOf(SubstrateNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{LotSubstrateBuilder.TableName}].{NameOf(LotSubstrateNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{SubstrateBuilder.TableName}].{NameOf(SubstrateNub.Amount)} DESC; ")
        Return LotSubstrateEntity.FetchFirstSubstrate(connection, queryBuilder.ToString, lotAutoId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotSubstrateEntity.LotAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.LotEntity"/> </summary>
    ''' <value> Identifies the <see cref="Entities.LotEntity"/> </value>
    Public Property LotAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(LotSubstrateEntity.SubstrateAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="SubstrateEntity"/>. </summary>
    ''' <value> Identifies the <see cref="SubstrateEntity"/>. </value>
    Public Property SubstrateAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
