Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Dapper.Entities.ConnectionExtensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entity.EntityExtensions

''' <summary> A builder for a natural Substrate based on the <see cref="IKeyForeignNaturalTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class SubstrateBuilder
    Inherits KeyForeignNaturalTimeBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return SubstrateBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(SubstrateBuilder._TableName) Then
                SubstrateBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(SubstrateNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the foreign table. </summary>
    ''' <value> The name of the foreign table. </value>
    Public Overrides Property ForeignTableName As String = SubstrateTypeBuilder.TableName

    ''' <summary> Gets the name of the foreign table key. </summary>
    ''' <value> The name of the foreign table key. </value>
    Public Overrides Property ForeignTableKeyName As String = NameOf(SubstrateTypeNub.Id)

    ''' <summary>
    ''' Inserts or ignores the records described by the <see cref="StructureType"/> enumeration type.
    ''' </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of inserted or total existing records. </returns>
    Public Overloads Shared Function InsertIgnoreStrucutreTypeValues(ByVal connection As System.Data.IDbConnection) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return SubstrateBuilder.InsertIgnoreTypeValues(connection, GetType(SubstrateType), New Integer() {CInt(SubstrateType.None)})
    End Function

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="type">       The enumeration type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of inserted records or total number of records. </returns>
    Public Overloads Shared Function InsertIgnoreTypeValues(ByVal connection As System.Data.IDbConnection, ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection.InsertIgnoreNameDescriptionRecords(SubstrateTypeBuilder.TableName, GetType(IKeyForeignNaturalTime).EnumerateEntityFieldNames(), type, excluded)
    End Function

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(SubstrateEntity.AutoId)

    ''' <summary> Gets or sets the name of the foreign identifier field. </summary>
    ''' <value> The name of the foreign identifier field. </value>
    Public Overrides Property ForeignIdFieldName() As String = NameOf(SubstrateEntity.SubstrateTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName() As String = NameOf(SubstrateEntity.SubstrateNumber)

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <value> The name of the timestamp field. </value>
    Public Overrides Property TimestampFieldName() As String = NameOf(SubstrateEntity.Timestamp)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SubstrateBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SubstrateBuilder
        If SubstrateBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                SubstrateBuilder._Instance = New SubstrateBuilder()
            End SyncLock
        End If
        Return SubstrateBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the natural Substrate table based on the <see cref="IKeyForeignNaturalTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
<Table("Substrate")>
Public Class SubstrateNub
    Inherits KeyForeignNaturalTimeNub
    Implements IKeyForeignNaturalTime

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IKeyForeignNaturalTime
        Return New SubstrateNub
    End Function

End Class


''' <summary> The natural Substrate Entity based on the <see cref="IKeyForeignNaturalTime">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class SubstrateEntity
    Inherits EntityBase(Of IKeyForeignNaturalTime, SubstrateNub)
    Implements IKeyForeignNaturalTime

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New SubstrateNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Substrate interface. </param>
    Public Sub New(ByVal value As IKeyForeignNaturalTime)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyForeignNaturalTime, ByVal store As IKeyForeignNaturalTime)
        MyBase.New(New SubstrateNub, cache, store)
        Me.UsingNativeTracking = String.Equals(SubstrateBuilder.TableName, NameOf(IKeyForeignNaturalTime).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IKeyForeignNaturalTime
        Return New SubstrateNub
    End Function


    Public Overrides Function CreateCopy() As IKeyForeignNaturalTime
        Dim destination As IKeyForeignNaturalTime = Me.CreateNew
        SubstrateNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IKeyForeignNaturalTime)
        SubstrateNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Substrate interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyForeignNaturalTime)
        ' first make the copy to notify of any property change.
        SubstrateNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Substrate table primary key. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyForeignNaturalTime)(key), connection.Get(Of SubstrateNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.SubstrateNumber)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="substrateNumber"> The substrate number. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal substrateNumber As Integer) As Boolean
        Me.ClearStore()
        Dim nub As SubstrateNub = SubstrateEntity.FetchNubs(connection, substrateNumber).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.FetchUsingKey(connection, nub.AutoId)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyForeignNaturalTime) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If SubstrateEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(UniqueIndexOptions.Amount = (SubstrateBuilder.Get.QueryUniqueIndexOptions(connection) And UniqueIndexOptions.Amount),
                                        Me.FetchUsingUniqueIndex(connection, entity.Amount),
                                        Me.FetchUsingKey(connection, entity.AutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            entity.AutoId = Me.AutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of SubstrateNub)(New SubstrateNub With {.AutoId = key})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Substrate entities. </summary>
    ''' <value> The Substrate entities. </value>
    Public ReadOnly Property Substrates As IEnumerable(Of SubstrateEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of SubstrateEntity)
        Return If(usingNativeTracking, SubstrateEntity.Populate(connection.GetAll(Of IKeyForeignNaturalTime)), SubstrateEntity.Populate(connection.GetAll(Of SubstrateNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Substrates = SubstrateEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(SubstrateEntity.Substrates))
        Return If(Me.Substrates?.Any, Me.Substrates.Count, 0)
    End Function

    ''' <summary> Populates a list of Substrate entities. </summary>
    ''' <param name="nubs"> The Substrate nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of SubstrateNub)) As IEnumerable(Of SubstrateEntity)
        Dim l As New List(Of SubstrateEntity)
        If nubs?.Any Then
            For Each nub As SubstrateNub In nubs
                l.Add(New SubstrateEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Substrate entities. </summary>
    ''' <param name="interfaces"> The Substrate interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyForeignNaturalTime)) As IEnumerable(Of SubstrateEntity)
        Dim l As New List(Of SubstrateEntity)
        If interfaces?.Any Then
            Dim nub As New SubstrateNub
            For Each iFace As IKeyForeignNaturalTime In interfaces
                nub.CopyFrom(iFace)
                l.Add(New SubstrateEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="substrateNumber">  The substrate number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal substrateNumber As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SubstrateBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SubstrateNub.Amount)} = @SubstrateNumber", New With {substrateNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="substrateNumber">  The substrate number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal substrateNumber As Integer) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{SubstrateBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(SubstrateNub.Amount)} IS NULL OR {NameOf(SubstrateNub.Amount)} = @SubstrateNumber)")
        selectSql.Append("; ")
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {substrateNumber})
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="substrateNumber">  The substrate number. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal substrateNumber As Integer) As IEnumerable(Of SubstrateNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SubstrateBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SubstrateNub.Amount)} = @SubstrateNumber", New With {substrateNumber})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SubstrateNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the substrate exists. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="substrateNumber">  The substrate number. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal substrateNumber As Integer) As Boolean
        Return 1 = SubstrateEntity.CountEntities(connection, substrateNumber)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="SubstrateEntity"/>. </summary>
    ''' <value> Identifies the <see cref="SubstrateEntity"/>. </value>
    Public Property AutoId As Integer Implements IKeyForeignNaturalTime.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="SubstrateEntity"/> type. </summary>
    ''' <value> Identifies the <see cref="SubstrateEntity"/> type. </value>
    Public Property ForeignId As Integer Implements IKeyForeignNaturalTime.ForeignId
        Get
            Return Me.ICache.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.ForeignId, value) Then
                Me.ICache.ForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SubstrateEntity.SubstrateTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="SubstrateEntity"/> type. </summary>
    ''' <value> Identifies the <see cref="SubstrateEntity"/> type. </value>
    Public Property SubstrateTypeId As Integer
        Get
            Return Me.ForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SubstrateTypeId, value) Then
                Me.ForeignId = value
            End If
        End Set
    End Property


    ''' <summary> Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value. </summary>
    ''' <value> The natural amount. </value>
    Public Property Amount As Integer Implements IKeyForeignNaturalTime.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(value As Integer)
            If Me.Amount <> value Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SubstrateEntity.SubstrateNumber))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the structure number. </summary>
    ''' <value> The structure number. </value>
    Public Property SubstrateNumber As Integer
        Get
            Return Me.Amount
        End Get
        Set(value As Integer)
            Me.Amount = value
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks> Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to convert to local time. </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements IKeyForeignNaturalTime.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " FIELDS: CUSTOM "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

#End Region

End Class

