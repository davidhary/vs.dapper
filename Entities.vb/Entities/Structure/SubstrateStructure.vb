Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Substrate-Structure builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class SubstrateStructureBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return SubstrateStructureBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(SubstrateStructureBuilder._TableName) Then
                SubstrateStructureBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(SubstrateStructureNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = SubstrateBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(SubstrateNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = StructureBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(StructureNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(SubstrateStructureEntity.SubstrateAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(SubstrateStructureEntity.StructureAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SubstrateStructureBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SubstrateStructureBuilder
        If SubstrateStructureBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                SubstrateStructureBuilder._Instance = New SubstrateStructureBuilder()
            End SyncLock
        End If
        Return SubstrateStructureBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Substrate Structure Nub based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("SubstrateStructure")>
Public Class SubstrateStructureNub
    Inherits OneToManyNub
    Implements IOneToMany

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToMany
        Return New SubstrateStructureNub
    End Function

End Class

''' <summary>
''' The Substrate-Structure Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class SubstrateStructureEntity
    Inherits EntityBase(Of IOneToMany, SubstrateStructureNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New SubstrateStructureNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Substrate-Structure interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New SubstrateStructureNub, cache, store)
        Me.UsingNativeTracking = String.Equals(SubstrateStructureBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "


    Public Overrides Function CreateNew() As IOneToMany
        Return New SubstrateStructureNub
    End Function

    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        SubstrateStructureNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        SubstrateStructureNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Substrate-Structure interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        SubstrateStructureNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer, ByVal structureAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As SubstrateStructureNub = SubstrateStructureEntity.FetchNubs(connection, substrateAutoId, structureAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.SubstrateAutoId, Me.StructureAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.SubstrateAutoId, Me.StructureAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer, ByVal structureAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, substrateAutoId, structureAutoId)
    End Function

    ''' <summary>
    ''' Attempts to retrieve an existing or insert a new <see cref="SubstrateStructureEntity"/> from
    ''' the given data. Specifying new <paramref name="substrateNumber"/> adds this substrate;
    ''' Specifying a negative
    ''' <paramref name="structureNumber"/> adds a new structure with the next structure number for
    ''' this substrate. Updates the <see cref="SubstrateStructureEntity.SubstrateEntity"/> and
    ''' <see cref="SubstrateStructureEntity.StructureEntity"/>
    ''' </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="substrateNumber"> The substrate number. </param>
    ''' <param name="structureNumber"> The structure number. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal substrateNumber As Integer, ByVal structureNumber As Integer) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me._SubstrateEntity = New SubstrateEntity With {.SubstrateNumber = substrateNumber}
        Me._StructureEntity = New StructureEntity With {.StructureNumber = structureNumber}
        If Me.SubstrateEntity.Obtain(connection) Then
            If 0 < structureNumber Then
                If Not Me.StructureEntity.FetchUsingUniqueIndex(connection) Then
                    result = (False, $"Failed fetching existing {NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.StructureNumber)} of {structureNumber}")
                End If
            Else
                Me._StructureEntity = SubstrateStructureEntity.FetchLastStructure(connection, Me.SubstrateEntity.AutoId)
                structureNumber = If(Me.StructureEntity.IsClean, Me.StructureEntity.StructureNumber + 1, 1)
                Me._StructureEntity = New StructureEntity() With {.StructureNumber = structureNumber}
                If Not Me.StructureEntity.Obtain(connection) Then
                    result = (False, $"Failed obtaining {NameOf(Entities.StructureEntity)} with {NameOf(Entities.StructureEntity.StructureNumber)} of {structureNumber}")
                End If
            End If
        Else
            result = (False, $"Failed obtaining {NameOf(Entities.SubstrateEntity)} with {NameOf(Entities.SubstrateEntity.SubstrateNumber)} of {substrateNumber}")
        End If
        If result.Success Then
            Me.SubstrateAutoId = Me.SubstrateEntity.AutoId
            Me.StructureAutoId = Me.StructureEntity.AutoId
            If Not Me.Obtain(connection) Then
                result = (False, $"Failed obtaining {NameOf(Entities.SubstrateStructureEntity)} for [{NameOf(Entities.SubstrateEntity.SubstrateNumber)}, {NameOf(Entities.StructureEntity.StructureNumber)}] of [{substrateNumber},{structureNumber}]")
            End If
        End If
        Me.NotifyPropertyChanged(NameOf(SubstrateStructureEntity.StructureEntity))
        Me.NotifyPropertyChanged(NameOf(SubstrateStructureEntity.SubstrateEntity))
        Return result
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If SubstrateStructureEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substrateAutoId">     Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer, ByVal structureAutoId As Integer) As Boolean
        Return connection.Delete(Of SubstrateStructureNub)(New SubstrateStructureNub With {.PrimaryId = substrateAutoId, .SecondaryId = structureAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Substrate-Structure entities. </summary>
    ''' <value> The Substrate-Structure entities. </value>
    Public ReadOnly Property SubstrateStructures As IEnumerable(Of SubstrateStructureEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of SubstrateStructureEntity)
        Return If(usingNativeTracking, SubstrateStructureEntity.Populate(connection.GetAll(Of IOneToMany)), SubstrateStructureEntity.Populate(connection.GetAll(Of SubstrateStructureNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._SubstrateStructures = SubstrateStructureEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(SubstrateStructureEntity.SubstrateStructures))
        Return If(Me.SubstrateStructures?.Any, Me.SubstrateStructures.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of SubstrateStructureNub)) As IEnumerable(Of SubstrateStructureEntity)
        Dim l As New List(Of SubstrateStructureEntity)
        If nubs?.Any Then
            For Each nub As SubstrateStructureNub In nubs
                l.Add(New SubstrateStructureEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of SubstrateStructureEntity)
        Dim l As New List(Of SubstrateStructureEntity)
        If interfaces?.Any Then
            Dim nub As New SubstrateStructureNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New SubstrateStructureEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to Structure entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SubstrateStructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SubstrateStructureNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = substrateAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As IEnumerable(Of SubstrateStructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{SubstrateStructureBuilder.TableName}] WHERE {NameOf(SubstrateStructureNub.PrimaryId)} = @Id", New With {Key .Id = substrateAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return SubstrateStructureEntity.Populate(connection.Query(Of SubstrateStructureNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Structure. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> The total number of entities by Structure. </returns>
    Public Shared Function CountEntitiesByStructure(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SubstrateStructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SubstrateStructureNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Structures in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Structures in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByStructure(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of SubstrateStructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{SubstrateStructureBuilder.TableName}] WHERE {NameOf(SubstrateStructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return SubstrateStructureEntity.Populate(connection.Query(Of SubstrateStructureNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <param name="structureAutoId">       Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SubstrateStructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SubstrateStructureNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = substrateAutoId})
        sqlBuilder.Where($"{NameOf(SubstrateStructureNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <param name="structureAutoId">       Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer, ByVal structureAutoId As Integer) As IEnumerable(Of SubstrateStructureNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SubstrateStructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SubstrateStructureNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = substrateAutoId})
        sqlBuilder.Where($"{NameOf(SubstrateStructureNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SubstrateStructureNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Substrate Structure exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <param name="structureAutoId">       Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer, ByVal structureAutoId As Integer) As Boolean
        Return 1 = SubstrateStructureEntity.CountEntities(connection, substrateAutoId, structureAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Substrate entity. </summary>
    ''' <value> The Substrate entity. </value>
    Public ReadOnly Property SubstrateEntity As SubstrateEntity

    ''' <summary> Fetches Substrate Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Substrate Entity. </returns>
    Public Function FetchSubstrateEntity(ByVal connection As System.Data.IDbConnection) As SubstrateEntity
        Dim entity As New SubstrateEntity()
        entity.FetchUsingKey(connection, Me.SubstrateAutoId)
        Me._SubstrateEntity = entity
        Return entity
    End Function

    ''' <summary> Count Substrates associated with the specified <paramref name="structureAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> The total number of Substrates. </returns>
    Public Shared Function CountSubstrates(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{SubstrateStructureBuilder.TableName}] WHERE {NameOf(SubstrateStructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Substrates associated with the specified <paramref name="structureAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Substrates in this collection.
    ''' </returns>
    Public Shared Function FetchSubstrates(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of StructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{SubstrateStructureBuilder.TableName}] WHERE {NameOf(SubstrateStructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of StructureEntity)
        For Each nub As SubstrateStructureNub In connection.Query(Of SubstrateStructureNub)(selector.RawSql, selector.Parameters)
            Dim entity As New SubstrateStructureEntity(nub)
            l.Add(entity.FetchStructureEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Substrates associated with the specified <paramref name="structureAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteSubstrates(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{SubstrateStructureBuilder.TableName}] WHERE {NameOf(SubstrateStructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = structureAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the Structure entity. </summary>
    ''' <value> The Structure entity. </value>
    Public ReadOnly Property StructureEntity As StructureEntity

    ''' <summary> Fetches a Structure Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Structure Entity. </returns>
    Public Function FetchStructureEntity(ByVal connection As System.Data.IDbConnection) As StructureEntity
        Dim entity As New StructureEntity()
        entity.FetchUsingKey(connection, Me.StructureAutoId)
        Me._StructureEntity = entity
        Return entity
    End Function

    ''' <summary> Count structures. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns> The total number of structures. </returns>
    Public Shared Function CountStructures(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{SubstrateStructureBuilder.TableName}] WHERE {NameOf(SubstrateStructureNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = substrateAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Structures in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Structures in this collection.
    ''' </returns>
    Public Shared Function FetchStructures(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As IEnumerable(Of StructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{SubstrateStructureBuilder.TableName}] WHERE {NameOf(SubstrateStructureNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = substrateAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of StructureEntity)
        For Each nub As SubstrateStructureNub In connection.Query(Of SubstrateStructureNub)(selector.RawSql, selector.Parameters)
            Dim entity As New SubstrateStructureEntity(nub)
            l.Add(entity.FetchStructureEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Structure related to the specified Substrate. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteStructures(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{SubstrateStructureBuilder.TableName}] WHERE {NameOf(SubstrateStructureNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = substrateAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Fetches the ordered structures in this collection. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="substrateAutoId">   Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered structures in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedStructures(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal substrateAutoId As Integer) As IEnumerable(Of StructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, New With {Key .Id = substrateAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return StructureEntity.Populate(connection.Query(Of StructureNub)(template.RawSql, template.Parameters))
    End Function

    ''' <summary> Fetches the ordered structures in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ordered structures in this collection.
    ''' </returns>
    Public Shared Function FetchOrderedStructures(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As IEnumerable(Of StructureEntity)
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [SubstrateStructure] on [SubstrateStructure].SecondaryId = [Structure].AutoId where [SubstrateStructure].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{StructureBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{StructureBuilder.TableName}] Inner Join [{SubstrateStructureBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{SubstrateStructureBuilder.TableName}].{NameOf(SubstrateStructureNub.SecondaryId)} = [{StructureBuilder.TableName}].{NameOf(StructureNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{SubstrateStructureBuilder.TableName}].{NameOf(SubstrateStructureNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{StructureBuilder.TableName}].{NameOf(StructureNub.Amount)} ASC; ")
        Return SubstrateStructureEntity.FetchOrderedStructures(connection, queryBuilder.ToString, substrateAutoId)
    End Function

    ''' <summary> Fetches the last Structure in this collection. </summary>
    ''' <remarks> David, 5/18/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="substrateAutoId">   Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Structures in this collection.
    ''' </returns>
    Public Shared Function FetchFirstStructure(ByVal connection As System.Data.IDbConnection, ByVal selectQuery As String, ByVal substrateAutoId As Integer) As StructureEntity
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery, New With {Key .Id = substrateAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Dim nub As StructureNub = connection.Query(Of StructureNub)(template.RawSql, template.Parameters).FirstOrDefault
        Return If(nub Is Nothing, New StructureEntity, New StructureEntity(nub, nub.CreateCopy))
    End Function

    ''' <summary> Fetches the last Structure in this collection. </summary>
    ''' <remarks> David, 5/9/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="substrateAutoId"> Identifies the <see cref="SubstrateEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the last Structures in this collection.
    ''' </returns>
    Public Shared Function FetchLastStructure(ByVal connection As System.Data.IDbConnection, ByVal substrateAutoId As Integer) As StructureEntity
        Dim queryBuilder As New System.Text.StringBuilder
        ' Select [UUT].* From [UUT] Inner Join [SubstrateStructure] on [SubstrateStructure].SecondaryId = [Structure].AutoId where [SubstrateStructure].PrimaryId = 2
        queryBuilder.AppendLine($"SELECT [{StructureBuilder.TableName}].*")
        queryBuilder.AppendLine($"FROM [{StructureBuilder.TableName}] Inner Join [{SubstrateStructureBuilder.TableName}]")
        queryBuilder.AppendLine($"ON [{SubstrateStructureBuilder.TableName}].{NameOf(SubstrateStructureNub.SecondaryId)} = [{StructureBuilder.TableName}].{NameOf(StructureNub.AutoId)}")
        queryBuilder.AppendLine($"WHERE [{SubstrateStructureBuilder.TableName}].{NameOf(SubstrateStructureNub.PrimaryId)} = @Id")
        queryBuilder.AppendLine($"ORDER BY [{StructureBuilder.TableName}].{NameOf(StructureNub.Amount)} DESC; ")
        Return SubstrateStructureEntity.FetchFirstStructure(connection, queryBuilder.ToString, substrateAutoId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SubstrateStructureEntity.SubstrateAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SubstrateStructureEntity.StructureAutoId))
            End If
        End Set
    End Property

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="SubstrateEntity"/>. </summary>
    ''' <value> Identifies the <see cref="SubstrateEntity"/>. </value>
    Public Property SubstrateAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="StructureEntity"/>. </summary>
    ''' <value> Identifies the <see cref="StructureEntity"/>. </value>
    Public Property StructureAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
