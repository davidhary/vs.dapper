Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Session-Structure builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class SessionStructureBuilder
    Inherits OneToManyBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return SessionStructureBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(SessionStructureBuilder._TableName) Then
                SessionStructureBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(SessionStructureNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = SessionBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(SessionNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = StructureBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(StructureNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(SessionStructureEntity.SessionAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(SessionStructureEntity.StructureAutoId)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SessionStructureBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SessionStructureBuilder
        If SessionStructureBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                SessionStructureBuilder._Instance = New SessionStructureBuilder()
            End SyncLock
        End If
        Return SessionStructureBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Session Structure Nub based on the <see cref="IOneToMany">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("SessionStructure")>
Public Class SessionStructureNub
    Inherits OneToManyNub
    Implements IOneToMany

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToMany
        Return New SessionStructureNub
    End Function

End Class

''' <summary>
''' The Session-Structure Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class SessionStructureEntity
    Inherits EntityBase(Of IOneToMany, SessionStructureNub)
    Implements IOneToMany

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New SessionStructureNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Session-Structure interface. </param>
    Public Sub New(ByVal value As IOneToMany)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToMany, ByVal store As IOneToMany)
        MyBase.New(New SessionStructureNub, cache, store)
        Me.UsingNativeTracking = String.Equals(SessionStructureBuilder.TableName, NameOf(IOneToMany).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToMany
        Return New SessionStructureNub
    End Function

    Public Overrides Function CreateCopy() As IOneToMany
        Dim destination As IOneToMany = Me.CreateNew
        SessionStructureNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToMany)
        SessionStructureNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Session-Structure interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToMany)
        ' first make the copy to notify of any property change.
        SessionStructureNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal structureAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As SessionStructureNub = SessionStructureEntity.FetchNubs(connection, sessionAutoId, structureAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.SessionAutoId, Me.StructureAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.SessionAutoId, Me.StructureAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal structureAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, sessionAutoId, structureAutoId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToMany) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If SessionStructureEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId">     Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal structureAutoId As Integer) As Boolean
        Return connection.Delete(Of SessionStructureNub)(New SessionStructureNub With {.PrimaryId = sessionAutoId, .SecondaryId = structureAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Session-Structure entities. </summary>
    ''' <value> The Session-Structure entities. </value>
    Public ReadOnly Property SessionStructures As IEnumerable(Of SessionStructureEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of SessionStructureEntity)
        Return If(usingNativeTracking, SessionStructureEntity.Populate(connection.GetAll(Of IOneToMany)), SessionStructureEntity.Populate(connection.GetAll(Of SessionStructureNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._SessionStructures = SessionStructureEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(SessionStructureEntity.SessionStructures))
        Return If(Me.SessionStructures?.Any, Me.SessionStructures.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of SessionStructureNub)) As IEnumerable(Of SessionStructureEntity)
        Dim l As New List(Of SessionStructureEntity)
        If nubs?.Any Then
            For Each nub As SessionStructureNub In nubs
                l.Add(New SessionStructureEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToMany)) As IEnumerable(Of SessionStructureEntity)
        Dim l As New List(Of SessionStructureEntity)
        If interfaces?.Any Then
            Dim nub As New SessionStructureNub
            For Each iFace As IOneToMany In interfaces
                nub.CopyFrom(iFace)
                l.Add(New SessionStructureEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to Structure entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SessionStructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionStructureNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="sessionAutoId">  Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As IEnumerable(Of SessionStructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{SessionStructureBuilder.TableName}] WHERE {NameOf(SessionStructureNub.PrimaryId)} = @Id", New With {Key .Id = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return SessionStructureEntity.Populate(connection.Query(Of SessionStructureNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by Structure. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> The total number of entities by Structure. </returns>
    Public Shared Function CountEntitiesByStructure(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SessionStructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionStructureNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by Structures in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by Structures in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByStructure(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of SessionStructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{SessionStructureBuilder.TableName}] WHERE {NameOf(SessionStructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return SessionStructureEntity.Populate(connection.Query(Of SessionStructureNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="structureAutoId">       Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{SessionStructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionStructureNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = sessionAutoId})
        sqlBuilder.Where($"{NameOf(SessionStructureNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="structureAutoId">       Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal structureAutoId As Integer) As IEnumerable(Of SessionStructureNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{SessionStructureBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(SessionStructureNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = sessionAutoId})
        sqlBuilder.Where($"{NameOf(SessionStructureNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of SessionStructureNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Session Structure exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <param name="structureAutoId">       Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer, ByVal structureAutoId As Integer) As Boolean
        Return 1 = SessionStructureEntity.CountEntities(connection, sessionAutoId, structureAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Session entity. </summary>
    ''' <value> The Session entity. </value>
    Public ReadOnly Property SessionEntity As SessionEntity

    ''' <summary> Fetches Session Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Session Entity. </returns>
    Public Function FetchSessionEntity(ByVal connection As System.Data.IDbConnection) As SessionEntity
        Dim entity As New SessionEntity()
        entity.FetchUsingKey(connection, Me.SessionAutoId)
        Me._SessionEntity = entity
        Return entity
    End Function

    ''' <summary> Count Sessions associated with the specified <paramref name="structureAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> The total number of Sessions. </returns>
    Public Shared Function CountSessions(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{SessionStructureBuilder.TableName}] WHERE {NameOf(SessionStructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Sessions associated with the specified <paramref name="structureAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Sessions in this collection.
    ''' </returns>
    Public Shared Function FetchSessions(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of StructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{SessionStructureBuilder.TableName}] WHERE {NameOf(SessionStructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of StructureEntity)
        For Each nub As SessionStructureNub In connection.Query(Of SessionStructureNub)(selector.RawSql, selector.Parameters)
            Dim entity As New SessionStructureEntity(nub)
            l.Add(entity.FetchStructureEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Sessions associated with the specified <paramref name="structureAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteSessions(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{SessionStructureBuilder.TableName}] WHERE {NameOf(SessionStructureNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = structureAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the Structure entity. </summary>
    ''' <value> The Structure entity. </value>
    Public ReadOnly Property StructureEntity As StructureEntity

    ''' <summary> Fetches a Structure Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Structure Entity. </returns>
    Public Function FetchStructureEntity(ByVal connection As System.Data.IDbConnection) As StructureEntity
        Dim entity As New StructureEntity()
        entity.FetchUsingKey(connection, Me.StructureAutoId)
        Me._StructureEntity = entity
        Return entity
    End Function

    Public Shared Function CountStructures(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{SessionStructureBuilder.TableName}] WHERE {NameOf(SessionStructureNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the Structures in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="sessionAutoId"> Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Structures in this collection.
    ''' </returns>
    Public Shared Function FetchStructures(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As IEnumerable(Of StructureEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{SessionStructureBuilder.TableName}] WHERE {NameOf(SessionStructureNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = sessionAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of StructureEntity)
        For Each nub As SessionStructureNub In connection.Query(Of SessionStructureNub)(selector.RawSql, selector.Parameters)
            Dim entity As New SessionStructureEntity(nub)
            l.Add(entity.FetchStructureEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all Structure related to the specified Session. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="sessionAutoId">  Identifies the <see cref="SessionEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteStructures(ByVal connection As System.Data.IDbConnection, ByVal sessionAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{SessionStructureBuilder.TableName}] WHERE {NameOf(SessionStructureNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = sessionAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToMany.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SessionStructureEntity.SessionAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToMany.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(SessionStructureEntity.StructureAutoId))
            End If
        End Set
    End Property

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Session. </summary>
    ''' <value> Identifies the Session. </value>
    Public Property SessionAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the <see cref="StructureEntity"/>. </summary>
    ''' <value> Identifies the <see cref="StructureEntity"/>. </value>
    Public Property StructureAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
