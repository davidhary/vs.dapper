Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Structure-ProductSort builder. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public NotInheritable Class StructureProductSortBuilder
    Inherits OneToOneBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return StructureProductSortBuilder.TableName
        End Get
    End Property

    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(StructureProductSortBuilder._TableName) Then
                StructureProductSortBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(StructureProductSortNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public Overrides Property PrimaryTableName As String = StructureBuilder.TableName

    ''' <summary> Gets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public Overrides Property PrimaryTableKeyName As String = NameOf(StructureNub.AutoId)

    ''' <summary> Gets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public Overrides Property SecondaryTableName As String = ProductSortBuilder.TableName

    ''' <summary> Gets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public Overrides Property SecondaryTableKeyName As String = NameOf(ProductSortNub.AutoId)

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public Overrides Property PrimaryIdFieldName As String = NameOf(StructureProductSortEntity.StructureAutoId)

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public Overrides Property SecondaryIdFieldName As String = NameOf(StructureProductSortEntity.ProductSortAutoId)


#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As StructureProductSortBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As StructureProductSortBuilder
        If StructureProductSortBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                StructureProductSortBuilder._Instance = New StructureProductSortBuilder()
            End SyncLock
        End If
        Return StructureProductSortBuilder._Instance
    End Function

#End Region

End Class

''' <summary> Implements the Structure ProductSort Nub based on the <see cref="IOneToOne">interface</see>. </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
<Table("StructureProductSort")>
Public Class StructureProductSortNub
    Inherits OneToOneNub
    Implements IOneToOne

    Public Sub New()
        MyBase.New
    End Sub

    Public Overrides Function CreateNew() As IOneToOne
        Return New StructureProductSortNub
    End Function

End Class

''' <summary>
''' The Structure-ProductSort Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para></remarks>
Public Class StructureProductSortEntity
    Inherits EntityBase(Of IOneToOne, StructureProductSortNub)
    Implements IOneToOne

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(New StructureProductSortNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <param name="value"> The Structure-ProductSort interface. </param>
    Public Sub New(ByVal value As IOneToOne)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOneToOne, ByVal store As IOneToOne)
        MyBase.New(New StructureProductSortNub, cache, store)
        Me.UsingNativeTracking = String.Equals(StructureProductSortBuilder.TableName, NameOf(IOneToOne).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    Public Overrides Function CreateNew() As IOneToOne
        Return New StructureProductSortNub
    End Function

    Public Overrides Function CreateCopy() As IOneToOne
        Dim destination As IOneToOne = Me.CreateNew
        StructureProductSortNub.Copy(Me, destination)
        Return destination
    End Function

    Public Overrides Sub CopyFrom(ByVal value As IOneToOne)
        StructureProductSortNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary> Update the cached value, which also notifies of the entity property changes. </summary>
    ''' <param name="value"> The Structure-ProductSort interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOneToOne)
        ' first make the copy to notify of any property change.
        StructureProductSortNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="productSortAutoId">      Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal productSortAutoId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As StructureProductSortNub = StructureProductSortEntity.FetchNubs(connection, structureAutoId, productSortAutoId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.StructureAutoId, Me.ProductSortAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.StructureAutoId, Me.ProductSortAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="productSortAutoId">      Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal productSortAutoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, structureAutoId, productSortAutoId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns> <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>); otherwise <c>false</c> </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOneToOne) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If StructureProductSortEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.PrimaryId, entity.SecondaryId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId">     Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="productSortAutoId"> Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal productSortAutoId As Integer) As Boolean
        Return connection.Delete(Of StructureProductSortNub)(New StructureProductSortNub With {.PrimaryId = structureAutoId, .SecondaryId = productSortAutoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets the Structure-ProductSort entities. </summary>
    ''' <value> The Structure-ProductSort entities. </value>
    Public ReadOnly Property StructureProductSorts As IEnumerable(Of StructureProductSortEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of StructureProductSortEntity)
        Return If(usingNativeTracking, StructureProductSortEntity.Populate(connection.GetAll(Of IOneToOne)), StructureProductSortEntity.Populate(connection.GetAll(Of StructureProductSortNub)))
    End Function

    ''' <summary> Fetches all records. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._StructureProductSorts = StructureProductSortEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(StructureProductSortEntity.StructureProductSorts))
        Return If(Me.StructureProductSorts?.Any, Me.StructureProductSorts.Count, 0)
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="nubs"> The nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of StructureProductSortNub)) As IEnumerable(Of StructureProductSortEntity)
        Dim l As New List(Of StructureProductSortEntity)
        If nubs?.Any Then
            For Each nub As StructureProductSortNub In nubs
                l.Add(New StructureProductSortEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Enumerates populate in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="interfaces"> The interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOneToOne)) As IEnumerable(Of StructureProductSortEntity)
        Dim l As New List(Of StructureProductSortEntity)
        If interfaces?.Any Then
            Dim nub As New StructureProductSortNub
            For Each iFace As IOneToOne In interfaces
                nub.CopyFrom(iFace)
                l.Add(New StructureProductSortEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary>   Count entities; returns up to ProductSort entities count. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StructureProductSortBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureProductSortNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of StructureProductSortEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureProductSortBuilder.TableName}] WHERE {NameOf(StructureProductSortNub.PrimaryId)} = @Id", New With {Key .Id = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return StructureProductSortEntity.Populate(connection.Query(Of StructureProductSortNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Count entities by ProductSort. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productSortAutoId"> Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns> The total number of entities by ProductSort. </returns>
    Public Shared Function CountEntitiesByProductSort(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StructureProductSortBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureProductSortNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productSortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the entities by ProductSorts in this collection. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productSortAutoId"> Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities by ProductSorts in this
    ''' collection.
    ''' </returns>
    Public Shared Function FetchEntitiesByProductSort(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer) As IEnumerable(Of StructureProductSortEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureProductSortBuilder.TableName}] WHERE {NameOf(StructureProductSortNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = productSortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return StructureProductSortEntity.Populate(connection.Query(Of StructureProductSortNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary>   Count entities; returns 1 or 0. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="productSortAutoId">       Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns>   The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal productSortAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{StructureProductSortBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureProductSortNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = structureAutoId})
        sqlBuilder.Where($"{NameOf(StructureProductSortNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productSortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Fetches nubs; expects single entity or none. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="productSortAutoId">       Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal productSortAutoId As Integer) As IEnumerable(Of StructureProductSortNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{StructureProductSortBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(StructureProductSortNub.PrimaryId)} = @PrimaryId", New With {.PrimaryId = structureAutoId})
        sqlBuilder.Where($"{NameOf(StructureProductSortNub.SecondaryId)} = @SecondaryId", New With {.SecondaryId = productSortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of StructureProductSortNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>   Determine if the Structure ProductSort exists. </summary>
    ''' <remarks>   David, 3/10/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <param name="productSortAutoId">       Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer, ByVal productSortAutoId As Integer) As Boolean
        Return 1 = StructureProductSortEntity.CountEntities(connection, structureAutoId, productSortAutoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary> Gets or sets the Structure entity. </summary>
    ''' <value> The Structure entity. </value>
    Public ReadOnly Property StructureEntity As StructureEntity

    ''' <summary> Fetches Structure Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Structure Entity. </returns>
    Public Function FetchStructureEntity(ByVal connection As System.Data.IDbConnection) As StructureEntity
        Dim entity As New StructureEntity()
        entity.FetchUsingKey(connection, Me.StructureAutoId)
        Me._StructureEntity = entity
        Return entity
    End Function

    ''' <summary> Count Structures associated with the specified <paramref name="productSortAutoId"/>; expected 1. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productSortAutoId"> Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns> The total number of Structures. </returns>
    Public Shared Function CountStructures(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{StructureProductSortBuilder.TableName}] WHERE {NameOf(StructureProductSortNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = productSortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches the Structures associated with the specified <paramref name="productSortAutoId"/>; expected a
    ''' single entity.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productSortAutoId"> Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the Structures in this collection.
    ''' </returns>
    Public Shared Function FetchStructures(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer) As IEnumerable(Of ProductSortEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureProductSortBuilder.TableName}] WHERE {NameOf(StructureProductSortNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = productSortAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ProductSortEntity)
        For Each nub As StructureProductSortNub In connection.Query(Of StructureProductSortNub)(selector.RawSql, selector.Parameters)
            Dim entity As New StructureProductSortEntity(nub)
            l.Add(entity.FetchProductSortEntity(connection))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Deletes all Structures associated with the specified <paramref name="productSortAutoId"/>.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="productSortAutoId"> Identifies the <see cref="Entities.ProductEntity"/>Sort. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteStructures(ByVal connection As System.Data.IDbConnection, ByVal productSortAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{StructureProductSortBuilder.TableName}] WHERE {NameOf(StructureProductSortNub.SecondaryId)} = @SecondaryId", New With {Key .SecondaryId = productSortAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

    ''' <summary> Gets or sets the ProductSort entity. </summary>
    ''' <value> The ProductSort entity. </value>
    Public ReadOnly Property ProductSortEntity As ProductSortEntity

    ''' <summary> Fetches a ProductSort Entity. </summary>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The ProductSort Entity. </returns>
    Public Function FetchProductSortEntity(ByVal connection As System.Data.IDbConnection) As ProductSortEntity
        Dim entity As New ProductSortEntity()
        entity.FetchUsingKey(connection, Me.ProductSortAutoId)
        Me._ProductSortEntity = entity
        Return entity
    End Function

    Public Shared Function CountProductSorts(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*)  FROM [{StructureProductSortBuilder.TableName}] WHERE {NameOf(StructureProductSortNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches the ProductSorts in this collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="structureAutoId"> Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ProductSorts in this collection.
    ''' </returns>
    Public Shared Function FetchProductSorts(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As IEnumerable(Of ProductSortEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{StructureProductSortBuilder.TableName}] WHERE {NameOf(StructureProductSortNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = structureAutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Dim l As New List(Of ProductSortEntity)
        For Each nub As StructureProductSortNub In connection.Query(Of StructureProductSortNub)(selector.RawSql, selector.Parameters)
            Dim entity As New StructureProductSortEntity(nub)
            l.Add(entity.FetchProductSortEntity(connection))
        Next
        Return l
    End Function

    ''' <summary> Deletes all ProductSort related to the specified Structure. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeleteProductSorts(ByVal connection As System.Data.IDbConnection, ByVal structureAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"DELETE FROM [{StructureProductSortBuilder.TableName}] WHERE {NameOf(StructureProductSortNub.PrimaryId)} = @PrimaryId", New With {Key .PrimaryId = structureAutoId})
        If template.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.RawSql)} null")
        ElseIf template.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(template.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(template.RawSql, template.Parameters)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    Public Property PrimaryId As Integer Implements IOneToOne.PrimaryId
        Get
            Return Me.ICache.PrimaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PrimaryId, value) Then
                Me.ICache.PrimaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(StructureProductSortEntity.StructureAutoId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    Public Property SecondaryId As Integer Implements IOneToOne.SecondaryId
        Get
            Return Me.ICache.SecondaryId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondaryId, value) Then
                Me.ICache.SecondaryId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(StructureProductSortEntity.ProductSortAutoId))
            End If
        End Set
    End Property

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="StructureEntity"/>. </summary>
    ''' <value> Identifies the <see cref="StructureEntity"/>. </value>
    Public Property StructureAutoId As Integer
        Get
            Return Me.PrimaryId
        End Get
        Set(value As Integer)
            Me.PrimaryId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the ProductSort. </summary>
    ''' <value> Identifies the ProductSort. </value>
    Public Property ProductSortAutoId As Integer
        Get
            Return Me.SecondaryId
        End Get
        Set(value As Integer)
            Me.SecondaryId = value
        End Set
    End Property

#End Region

End Class
