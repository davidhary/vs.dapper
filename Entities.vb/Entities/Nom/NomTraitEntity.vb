Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary> A Nominal Trait builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class NomTraitBuilder
    Inherits KeyTwoForeignRealBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return NomTraitBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(NomTraitBuilder._TableName) Then
                NomTraitBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(NomTraitNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the first foreign reference table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the first foreign reference table. </value>
    Public Overrides Property FirstForeignTableName As String = NomTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the first foreign reference table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the first foreign reference table key. </value>
    Public Overrides Property FirstForeignTableKeyName As String = NameOf(NomTypeNub.Id)

    ''' <summary> Gets or sets the name of the Second foreign reference table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Second foreign reference table. </value>
    Public Overrides Property SecondForeignTableName As String = NomTraitTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the Second foreign reference table key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the Second foreign reference table key. </value>
    Public Overrides Property SecondForeignTableKeyName As String = NameOf(NomTraitTypeNub.Id)

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The new table. </returns>
    Public Overloads Function CreateTable(connection As System.Data.IDbConnection) As String
        ' the same Element ID and Nominal Type would repeat for the NUT associated with the next UUT.
        Return MyBase.CreateTable(connection, UniqueIndexOptions.None)
    End Function

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the automatic identifier field. </value>
    Public Overrides Property AutoIdFieldName() As String = NameOf(NomTraitEntity.AutoId)

    ''' <summary> Gets or sets the name of the First foreign identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the First foreign identifier field. </value>
    Public Overrides Property FirstForeignIdFieldName() As String = NameOf(NomTraitEntity.NomTypeId)

    ''' <summary> Gets or sets the name of the second foreign identifier field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the second foreign identifier field. </value>
    Public Overrides Property SecondForeignIdFieldName() As String = NameOf(NomTraitEntity.NomTraitTypeId)

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the amount field. </value>
    Public Overrides Property AmountFieldName() As String = NameOf(NomTraitEntity.Amount)

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As NomTraitBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As NomTraitBuilder
        If NomTraitBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                NomTraitBuilder._Instance = New NomTraitBuilder()
            End SyncLock
        End If
        Return NomTraitBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="NomTraitEntity"/> <see cref="IKeyTwoForeignReal">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
'''     Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("NomTrait")>
Public Class NomTraitNub
    Inherits KeyTwoForeignRealNub
    Implements IKeyTwoForeignReal

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IKeyTwoForeignReal
        Return New NomTraitNub
    End Function

End Class

''' <summary>
''' The <see cref="NomTraitEntity"/> storing Real(Double)-Valued Element Nominal Traits.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
'''     Licensed under The MIT License.</para><para>
''' David, 4/22/2020 </para>
''' </remarks>
Public Class NomTraitEntity
    Inherits EntityBase(Of IKeyTwoForeignReal, NomTraitNub)
    Implements IKeyTwoForeignReal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New NomTraitNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.ElementEntity"/>Value interface. </param>
    Public Sub New(ByVal value As IKeyTwoForeignReal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IKeyTwoForeignReal, ByVal store As IKeyTwoForeignReal)
        MyBase.New(New NomTraitNub, cache, store)
        Me.UsingNativeTracking = String.Equals(NomTraitBuilder.TableName, NameOf(IKeyTwoForeignReal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IKeyTwoForeignReal
        Return New NomTraitNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IKeyTwoForeignReal
        Dim destination As IKeyTwoForeignReal = Me.CreateNew
        NomTraitNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies from given entity. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="value"> The <see cref="NomTraitEntity"/> interface value. </param>
    Public Overrides Sub CopyFrom(ByVal value As IKeyTwoForeignReal)
        NomTraitNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the <see cref="Entities.ElementEntity"/>Value interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IKeyTwoForeignReal)
        ' first make the copy to notify of any property change.
        NomTraitNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The <see cref="NomTraitEntity"/> primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IKeyTwoForeignReal)(key), connection.Get(Of NomTraitNub)(key)))
    End Function

    ''' <summary> Refetch; Fetches using the primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.AutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/15/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="NomTraitEntity"/>. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Boolean
        Return Me.FetchUsingKey(connection, autoId)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IKeyTwoForeignReal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If NomTraitEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.AutoId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="NomTraitEntity"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Boolean
        Return connection.Delete(Of NomTraitNub)(New NomTraitNub With {.AutoId = autoId})
    End Function

#End Region

#Region " ENTITIES "

    ''' <summary> Gets or sets or <see cref="NomTraitEntity"/>'s. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="NomTraitEntity"/>'s. </value>
    Public ReadOnly Property NomTraits As IEnumerable(Of NomTraitEntity)

    ''' <summary> Fetches all records into <see cref="NomTraitEntity"/>'s. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of NomTraitEntity)
        Return If(usingNativeTracking, NomTraitEntity.Populate(connection.GetAll(Of IKeyTwoForeignReal)),
                                       NomTraitEntity.Populate(connection.GetAll(Of NomTraitNub)))
    End Function

    ''' <summary> Fetches all records into <see cref="NomTraitEntity"/>'s. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._NomTraits = NomTraitEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(NomTraitEntity.NomTraits))
        Return If(Me.NomTraits?.Any, Me.NomTraits.Count, 0)
    End Function

    ''' <summary> Populates a list of <see cref="NomTraitEntity"/>'s. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> the <see cref="Entities.ElementEntity"/>Value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of NomTraitNub)) As IEnumerable(Of NomTraitEntity)
        Dim l As New List(Of NomTraitEntity)
        If nubs?.Any Then
            For Each nub As NomTraitNub In nubs
                l.Add(New NomTraitEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of <see cref="NomTraitEntity"/>'s. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> the <see cref="Entities.ElementEntity"/>Value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IKeyTwoForeignReal)) As IEnumerable(Of NomTraitEntity)
        Dim l As New List(Of NomTraitEntity)
        If interfaces?.Any Then
            Dim nub As New NomTraitNub
            For Each iFace As IKeyTwoForeignReal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New NomTraitEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count element Nominal Traits; Returns 1 or 0. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="NomTraitEntity"/>. </param>
    ''' <returns> The total number of entities. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{NomTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NomTraitNub.AutoId)} = @AutoId", New With {.AutoId = autoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches element Nominal Traits by Element number and element Nominal Trait element Nominal
    ''' Trait Real Value type;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="NomTraitEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As IEnumerable(Of NomTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NomTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NomTraitNub.AutoId)} = @AutoId", New With {.AutoId = autoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NomTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches element Nominal Traits by <see cref="NomTraitEntity"/> id; expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="NomTraitEntity"/>. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0037:Use inferred member name", Justification:="<Pending>")>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As IEnumerable(Of NomTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NomTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NomTraitEntity.AutoId)} = @AutoId", New With {.AutoId = autoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NomTraitNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="Entities.ElementEntity"/> Value exists. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="autoId">     Identifies the <see cref="NomTraitEntity"/>. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal autoId As Integer) As Boolean
        Return 1 = NomTraitEntity.CountEntities(connection, autoId)
    End Function

#End Region

#Region " RELATIONS "

    ''' <summary>
    ''' Fetches element nominal Real(Double)-Value trait values by Element auto id;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Overridable Function FetchNomTraits(ByVal connection As System.Data.IDbConnection) As IEnumerable(Of NomTraitNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{NomTraitBuilder.TableName}] /**where**/")
        sqlBuilder.Where($"{NameOf(NomTraitNub.AutoId)} = @AutoId", New With {Me.AutoId})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NomTraitNub)(selector.RawSql, selector.Parameters)
    End Function

#End Region

#Region " RELATIONS: NOMINAL TYPE "

    ''' <summary> Gets or sets the <see cref="Entities.NomTypeEntity"/>. </summary>
    ''' <value> the <see cref="Entities.NomTypeEntity"/>. </value>
    Public ReadOnly Property NomypeEntity As NomTypeEntity

    ''' <summary> Fetches a <see cref="Entities.NomTypeEntity"/> . </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchNomTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._NomypeEntity = New NomTypeEntity()
        Return Me.NomypeEntity.FetchUsingKey(connection, Me.NomTypeId)
    End Function

#End Region

#Region " RELATIONS: NOMINAL TRAIT TYPE "

    ''' <summary> Gets or sets the <see cref="Entities.NomTraitTypeEntity"/> . </summary>
    ''' <value> the <see cref="Entities.NomTraitTypeEntity"/> . </value>
    Public ReadOnly Property NomTraitTypeEntity As NomTraitTypeEntity

    ''' <summary> Fetches a <see cref="Entities.NomTraitTypeEntity"/>. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchNomTraitTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._NomTraitTypeEntity = New NomTraitTypeEntity()
        Return Me.NomTraitTypeEntity.FetchUsingKey(connection, Me.NomTraitTypeId)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    Public Property AutoId As Integer Implements IKeyTwoForeignReal.AutoId
        Get
            Return Me.ICache.AutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AutoId, value) Then
                Me.ICache.AutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the first foreign key. </summary>
    ''' <value> Identifies the first foreign key. </value>
    Public Property FirstForeignId As Integer Implements IKeyTwoForeignReal.FirstForeignId
        Get
            Return Me.ICache.FirstForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.FirstForeignId, value) Then
                Me.ICache.FirstForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(NomTraitEntity.NomTypeId))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets nominal type identity key. </summary>
    ''' <value> The nominal type identity key. </value>
    Public Property NomTypeId As Integer
        Get
            Return Me.FirstForeignId
        End Get
        Set(ByVal value As Integer)
            Me.FirstForeignId = value
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the second foreign key. </summary>
    ''' <value> Identifies the Second foreign key. </value>
    Public Property SecondForeignId As Integer Implements IKeyTwoForeignReal.SecondForeignId
        Get
            Return Me.ICache.SecondForeignId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SecondForeignId, value) Then
                Me.ICache.SecondForeignId = value
                Me.NotifyFieldChanged()
                Me.NotifyPropertyChanged(NameOf(NomTraitEntity.NomTraitTypeId))
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the type of the <see cref="NomTraitEntity"/> Real(Double)-value.
    ''' </summary>
    ''' <value> The type of the <see cref="NomTraitEntity"/> Real(Double)-value. </value>
    Public Property NomTraitTypeId As Integer
        Get
            Return Me.SecondForeignId
        End Get
        Set(ByVal value As Integer)
            Me.SecondForeignId = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets a Real(Double)-value assigned to the <see cref="NomTraitEntity"/> for the
    ''' specific <see cref="Entities.NomTraitTypeEntity"/>.
    ''' </summary>
    ''' <value>
    ''' The Real(Double)-value assigned to the <see cref="NomTraitEntity"/> for the specific
    ''' <see cref="Entities.NomTraitTypeEntity"/>.
    ''' </value>
    Public Property Amount As Double Implements IKeyTwoForeignReal.Amount
        Get
            Return Me.ICache.Amount
        End Get
        Set(ByVal value As Double)
            If Not Double.Equals(Me.Amount, value) Then
                Me.ICache.Amount = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the entity unique key selector. </summary>
    ''' <value> The entity selector. </value>
    Public ReadOnly Property EntitySelector As ThreeKeySelector
        Get
            Return New ThreeKeySelector(Me)
        End Get
    End Property

    ''' <summary> Gets the attribute selector. </summary>
    ''' <value> The attribute selector. </value>
    Public ReadOnly Property TraitSelector As NomTraitSelector
        Get
            Return New NomTraitSelector(Me)
        End Get
    End Property

#End Region

End Class

''' <summary>
''' A Nominal Trait selector using the Nominal Trait <see cref="NomType"/> +
''' <see cref="NomTraitType"/>, which are unique for each <see cref="Entities.ElementEntity"/>.
''' </summary>
''' <remarks> David, 6/15/2020. </remarks>
Public Structure NomTraitSelector
    Implements IEquatable(Of NomTraitSelector)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nomTrait"> The nom trait. </param>
    Public Sub New(ByVal nomTrait As IKeyTwoForeignReal)
        Me.New(nomTrait.FirstForeignId, nomTrait.SecondForeignId)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nomTrait"> The nom trait. </param>
    Public Sub New(ByVal nomTrait As NomTraitEntity)
        Me.New(nomTrait.NomTypeId, nomTrait.NomTraitTypeId)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nomTypeId">      The identifier of the nom type. </param>
    ''' <param name="nomTraitTypeId"> The identifier of the nom trait type. </param>
    Public Sub New(ByVal nomTypeId As Integer, ByVal nomTraitTypeId As Integer)
        Me.NomTypeId = nomTypeId
        Me.NomTraitTypeId = nomTraitTypeId
    End Sub

    ''' <summary> Gets or sets the identifier of the nom trait type. </summary>
    ''' <value> The identifier of the nom trait type. </value>
    Public Property NomTraitTypeId As Integer

    ''' <summary> Gets or sets the identifier of the nom type. </summary>
    ''' <value> The identifier of the nom type. </value>
    Public Property NomTypeId As Integer

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="obj"> The object to compare with the current instance. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
    ''' represent the same value; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.Equals(CType(obj, NomTraitSelector))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Function Equals(other As NomTraitSelector) As Boolean Implements IEquatable(Of NomTraitSelector).Equals
        Return Me.NomTraitTypeId = other.NomTraitTypeId AndAlso Me.NomTypeId = other.NomTypeId
    End Function

    ''' <summary> Returns the hash code for this instance. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A 32-bit signed integer that is the hash code for this instance. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.NomTraitTypeId Xor Me.NomTypeId
    End Function

    ''' <summary> Cast that converts the given NomTraitSelector to a =. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(left As NomTraitSelector, right As NomTraitSelector) As Boolean
        Return left.Equals(right)
    End Operator

    ''' <summary> Cast that converts the given NomTraitSelector to a &lt;&gt; </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(left As NomTraitSelector, right As NomTraitSelector) As Boolean
        Return Not left = right
    End Operator

End Structure

''' <summary> Collection of <see cref="NomTraitEntity"/>'s. </summary>
''' <remarks> David, 2020-05-05. </remarks>
Public Class NomTraitEntityCollection
    Inherits EntityKeyedCollection(Of ThreeKeySelector, IKeyTwoForeignReal, NomTraitNub, NomTraitEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As NomTraitEntity) As ThreeKeySelector
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.EntitySelector
    End Function

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overridable Overloads Sub Add(ByVal item As NomTraitEntity)
        MyBase.Add(item)
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 5/19/2020. </remarks>
    Public Overridable Overloads Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of NomTraitEntity))
        If entities?.Any Then
            For Each entity As NomTraitEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Inserts or updates all entities using the given connection and the . </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of affected records or the total records if none was affected. </returns>
    Protected Overrides Function BulkUpsertThis(connection As Data.IDbConnection) As Integer
        Return NomTraitBuilder.Get.Upsert(connection, Me)
    End Function

End Class

