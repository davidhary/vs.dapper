Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ExceptionExtensions

''' <summary> A Nominal Trait Type builder. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class NomTraitTypeBuilder
    Inherits NominalBuilder

    ''' <summary> Gets the name of the table. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Protected Overrides ReadOnly Property TableNameThis As String
        Get
            Return NomTraitTypeBuilder.TableName
        End Get
    End Property

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(NomTraitTypeBuilder._TableName) Then
                NomTraitTypeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(NomTraitTypeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary>
    ''' Inserts or ignore the records as described by the <see cref="NomTraitType"/> enumeration type.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Overrides Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer
        Return MyBase.InsertIgnore(connection, GetType(NomTraitType), New Integer() {CInt(NomTraitType.None)})
    End Function

#Region " SINGLETON "

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As NomTraitTypeBuilder

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As NomTraitTypeBuilder
        If NomTraitTypeBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                NomTraitTypeBuilder._Instance = New NomTraitTypeBuilder()
            End SyncLock
        End If
        Return NomTraitTypeBuilder._Instance
    End Function

#End Region

End Class

''' <summary>
''' Implements the Nominal Trait Type table based on the <see cref="INominal">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
'''     Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("NomTraitType")>
Public Class NomTraitTypeNub
    Inherits NominalNub
    Implements INominal

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New NomTraitTypeNub
    End Function

End Class

''' <summary>
''' The <see cref="NomTraitTypeEntity"/> table based on the <see cref="INominal">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
'''     Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
Public Class NomTraitTypeEntity
    Inherits EntityBase(Of INominal, NomTraitTypeNub)
    Implements INominal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New NomTraitTypeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Nominal Trait Type interface. </param>
    Public Sub New(ByVal value As INominal)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As INominal, ByVal store As INominal)
        MyBase.New(New NomTraitTypeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(NomTraitTypeBuilder.TableName, NameOf(INominal).TrimStart("I"c))
    End Sub

#End Region

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As INominal
        Return New NomTraitTypeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        NomTraitTypeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As INominal)
        NomTraitTypeNub.Copy(value, Me)
    End Sub

#End Region

#Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Nominal Trait Type interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As INominal)
        ' first make the copy to notify of any property change.
        NomTraitTypeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

#End Region

#Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The <see cref="NomTraitTypeEntity"/> primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of INominal)(key), connection.Get(Of NomTraitTypeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetches using the primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.Id)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Label)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Nominal Trait Type label. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Me.ClearStore()
        Dim nub As NomTraitTypeNub = NomTraitTypeEntity.FetchNubs(connection, label).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As INominal) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If NomTraitTypeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Label) Then
            ' update the existing record from the specified entity.
            entity.Id = Me.Id
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of NomTraitTypeNub)(New NomTraitTypeNub With {.Id = key})
    End Function

#End Region

#Region " SHARED ENTITIES "

    ''' <summary> Gets or sets the Nominal Trait Type entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The <see cref="Entities.NomTypeEntity"/> entities. </value>
    Public Shared ReadOnly Property NomTraitTypes As IEnumerable(Of NomTraitTypeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of NomTraitTypeEntity)
        Return If(usingNativeTracking, NomTraitTypeEntity.Populate(connection.GetAll(Of INominal)), NomTraitTypeEntity.Populate(connection.GetAll(Of NomTraitTypeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        NomTraitTypeEntity._NomTraitTypes = NomTraitTypeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Return If(NomTraitTypeEntity.NomTraitTypes?.Any, NomTraitTypeEntity.NomTraitTypes.Count, 0)
    End Function

    ''' <summary> Populates a list of <see cref="NomTraitTypeEntity"/> entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The <see cref="NomTraitTypeNub"/>'s. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of NomTraitTypeNub)) As IEnumerable(Of NomTraitTypeEntity)
        Dim l As New List(Of NomTraitTypeEntity)
        If nubs?.Any Then
            For Each nub As NomTraitTypeNub In nubs
                l.Add(New NomTraitTypeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Nominal Trait Type entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The Nominal Trait Type interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of INominal)) As IEnumerable(Of NomTraitTypeEntity)
        Dim l As New List(Of NomTraitTypeEntity)
        If interfaces?.Any Then
            Dim nub As New NomTraitTypeNub
            For Each iFace As INominal In interfaces
                nub.CopyFrom(iFace)
                l.Add(New NomTraitTypeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Dictionary of entity lookups. </summary>
    Private Shared _EntityLookupDictionary As IDictionary(Of Integer, NomTraitTypeEntity)

    ''' <summary> The entity lookup dictionary. </summary>
    ''' <remarks> David, 6/12/2020. </remarks>
    ''' <returns> A Dictionary(Of Integer, NomTraitTypeEntity) </returns>
    Public Shared Function EntityLookupDictionary() As IDictionary(Of Integer, NomTraitTypeEntity)
        If Not (NomTraitTypeEntity._EntityLookupDictionary?.Any).GetValueOrDefault(False) Then
            NomTraitTypeEntity._EntityLookupDictionary = New Dictionary(Of Integer, NomTraitTypeEntity)
            For Each entity As NomTraitTypeEntity In NomTraitTypeEntity.NomTraitTypes
                NomTraitTypeEntity._EntityLookupDictionary.Add(entity.Id, entity)
            Next
        End If
        Return NomTraitTypeEntity._EntityLookupDictionary
    End Function

    ''' <summary> Dictionary of key lookups. </summary>
    Private Shared _KeyLookupDictionary As IDictionary(Of String, Integer)

    ''' <summary> The key lookup dictionary. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A Dictionary(Of String, Integer) </returns>
    Public Shared Function KeyLookupDictionary() As IDictionary(Of String, Integer)
        If Not (NomTraitTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False) Then
            NomTraitTypeEntity._KeyLookupDictionary = New Dictionary(Of String, Integer)
            For Each entity As NomTraitTypeEntity In NomTraitTypeEntity.NomTraitTypes
                NomTraitTypeEntity._KeyLookupDictionary.Add(entity.Label, entity.Id)
            Next
        End If
        Return NomTraitTypeEntity._KeyLookupDictionary
    End Function

    ''' <summary> Checks if entities and related dictionaries are populated. </summary>
    ''' <remarks> David, 5/25/2020. </remarks>
    ''' <returns> True if enumerated, false if not. </returns>
    Public Shared Function IsEnumerated() As Boolean
        Return (NomTraitTypeEntity.NomTraitTypes?.Any AndAlso
                NomTraitTypeEntity._EntityLookupDictionary?.Any AndAlso
                NomTraitTypeEntity._KeyLookupDictionary?.Any).GetValueOrDefault(False)
    End Function

#End Region

#Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Nominal Trait Type label. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT COUNT(*) FROM [{NomTraitTypeBuilder.TableName}] WHERE {NameOf(NomTraitTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 5/1/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Nominal Trait Type label. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the nubs in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal label As String) As IEnumerable(Of NomTraitTypeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate(
            $"SELECT * FROM [{NomTraitTypeBuilder.TableName}] WHERE {NameOf(NomTraitTypeNub.Label)} = @label", New With {label})
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of NomTraitTypeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the <see cref="NomTraitTypeEntity"/> exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="label">      The Nominal Trait Type label. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal label As String) As Boolean
        Return 1 = NomTraitTypeEntity.CountEntities(connection, label)
    End Function

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Nominal Trait Type . </summary>
    ''' <value> Identifies the Nominal Trait Type . </value>
    Public Property Id As Integer Implements INominal.Id
        Get
            Return Me.ICache.Id
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.Id, value) Then
                Me.ICache.Id = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Nominal Trait Type label. </summary>
    ''' <value> The Nominal Trait Type label. </value>
    Public Property Label As String Implements INominal.Label
        Get
            Return Me.ICache.Label
        End Get
        Set(value As String)
            If Not String.Equals(Me.Label, value) Then
                Me.ICache.Label = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description of the Nominal Trait Type . </summary>
    ''' <value> The Nominal Trait Type description. </value>
    Public Property Description As String Implements INominal.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

#End Region

#Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="nomTraitTypeId"> The entity unique key. </param>
    ''' <returns> A <see cref="NomTraitTypeEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal nomTraitTypeId As Integer) As (Success As Boolean, Details As String, Entity As NomTraitTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New NomTraitTypeEntity
        Try
            activity = $"Fetching {NameOf(NomTraitTypeEntity)} by {NameOf(NomTraitTypeNub.Id)} of {nomTraitTypeId}"
            Return If(entity.FetchUsingKey(connection, nomTraitTypeId),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <param name="connection">        The connection. </param>
    ''' <param name="nomTraitTypeLabel"> The Nominal Trait Type label. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As NomTraitTypeEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal nomTraitTypeLabel As String) As (Success As Boolean, Details As String, Entity As NomTraitTypeEntity)
        Dim activity As String = String.Empty
        Dim entity As New NomTraitTypeEntity
        Try
            activity = $"Fetching {NameOf(NomTraitTypeEntity)} by {NameOf(NomTraitTypeNub.Label)} of {nomTraitTypeLabel}"
            Return If(entity.FetchUsingUniqueIndex(connection, nomTraitTypeLabel),
                (True, String.Empty, entity),
                (False, $"Failed {activity}", entity))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", entity)
        End Try
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/8/2020. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of NomTraitTypeEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of NomTraitTypeEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New NomTraitTypeEntity()
            activity = $"fetching all {NameOf(NomTraitType)}s"
            Dim elementCount As Integer = entity.FetchAllEntities(connection)
            If elementCount <> NomTraitTypeEntity.EntityLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(NomTraitTypeEntity.EntityLookupDictionary)} count must equal {NameOf(NomTraitTypeEntity.NomTraitTypes)} count ")
            ElseIf elementCount <> NomTraitTypeEntity.KeyLookupDictionary.Count Then
                Throw New OperationFailedException($"{NameOf(NomTraitTypeEntity.KeyLookupDictionary)} count must equal {NameOf(NomTraitTypeEntity.NomTraitTypes)} count ")
            End If
            Return (True, String.Empty, NomTraitTypeEntity.NomTraitTypes)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", Array.Empty(Of NomTraitTypeEntity))
        End Try
    End Function

    ''' <summary> Fetches all. </summary>
    ''' <remarks> David, 7/11/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub FetchAll(ByVal connection As System.Data.IDbConnection)
        If Not IsEnumerated() Then TryFetchAll(connection)
    End Sub

#End Region

End Class

''' <summary> Values that represent Nominal Trait Type s. </summary>
''' <remarks> David, 2020-10-02. </remarks>
<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="<Pending>")>
Public Enum NomTraitType

    ''' <summary> . </summary>
    <ComponentModel.Description("None")> None = 0

    ''' <summary> . </summary>
    <ComponentModel.Description("Nominal Value")> NominalValue = 1

    ''' <summary> . </summary>
    <ComponentModel.Description("Tolerance")> Tolerance = 2

    ''' <summary> . </summary>
    <ComponentModel.Description("Tracking Tolerance")> TrackingTolerance = 3

    ''' <summary> . </summary>
    <ComponentModel.Description("Tcr")> Tcr = 4

    ''' <summary> . </summary>
    <ComponentModel.Description("Tracking Tcr")> TrackingTcr = 5

    ''' <summary> . </summary>
    <ComponentModel.Description("Guard Band Factor")> GuardBandFactor = 6

    ''' <summary> . </summary>
    <ComponentModel.Description("Sigma Performance Level")> SigmaPerformanceLevel = 7

    ''' <summary> . </summary>
    <ComponentModel.Description("Lower Specification Limit")> LowerSpecificationLimit = 8

    ''' <summary> . </summary>
    <ComponentModel.Description("Upper Specification Limit")> UpperSpecificationLimit = 9

    ''' <summary> . </summary>
    <ComponentModel.Description("Dispersion")> Dispersion = 10

    ''' <summary> . </summary>
    <ComponentModel.Description("Guard Band ")> GuardBand = 11

    ''' <summary> . </summary>
    <ComponentModel.Description("Guarded Specification Limit")> GuardedSpecificationLimit = 12

    ''' <summary> . </summary>
    <ComponentModel.Description("Lower Guarded Specification Limit")> LowerGuardedSpecificationLimit = 13

    ''' <summary> . </summary>
    <ComponentModel.Description("Upper Guarded Specification Limit")> UpperGuardedSpecificationLimit = 14

    ''' <summary> . </summary>
    <ComponentModel.Description("Lower TaN Value")> LowerTanValue = 15

    ''' <summary> . </summary>
    <ComponentModel.Description("Upper TaN Value")> UpperTanValue = 16

    ''' <summary> . </summary>
    <ComponentModel.Description("Lower Annealing Shift")> LowerAnnealingShift = 17

    ''' <summary> . </summary>
    <ComponentModel.Description("Upper Annealing Shift")> UpperAnnealingShift = 18

    ''' <summary> . </summary>
    <ComponentModel.Description("Lower Overflow Limit")> LowerOverflowLimit = 19

    ''' <summary> . </summary>
    <ComponentModel.Description("Upper Overflow Limit")> UpperOverflowLimit = 20

    ''' <summary> If TCR has dual values, such as TCR Core 00, 
    '''           the span represents the average around the TCR value. </summary>
    <ComponentModel.Description("Tcr Span")> TcrSpan = 21

    ''' <summary> If Tracking has dual values, such as with TCR Core 00, 
    '''           the span represents the average around the Tracking TCR value. </summary>
    <ComponentModel.Description("Tracking Tcr Span")> TrackingTcrSpan = 22
End Enum
