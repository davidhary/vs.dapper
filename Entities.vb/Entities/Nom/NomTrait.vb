''' <summary> The Nominal Trait class holing the Nominal Trait values. </summary>
''' <remarks> David, 2020-05-29. </remarks>
Public Class NomTrait
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSestter"> The getter setter. </param>
    ''' <param name="element">       The <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">     Identifier for the <see cref="Entities.NomTypeEntity"/>. </param>
    Private Sub New(ByVal getterSestter As isr.Core.Constructs.IGetterSetter(Of Double), ByVal element As ElementEntity, ByVal nomTypeId As Integer)
        MyBase.New()
        Me.GetterSetter = getterSestter
        Me.ElementLabel = element.ElementLabel
        Me.ElementType = CType(element.ElementTypeId, ElementType)
        Me.ElementOrdinalNumber = element.OrdinalNumber
        Me.NomType = CType(nomTypeId, NomType)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 6/28/2020. </remarks>
    ''' <param name="getterSetter"> The getter setter. </param>
    ''' <param name="element">      The <see cref="Entities.ElementEntity"/>. </param>
    ''' <param name="nomTypeId">    Identifier for the <see cref="Entities.NomTypeEntity"/>. </param>
    ''' <param name="multimeter">   The <see cref="Entities.MultimeterEntity"/>. </param>
    Public Sub New(ByVal getterSetter As isr.Core.Constructs.IGetterSetter(Of Double), ByVal element As ElementEntity, ByVal nomTypeId As Integer,
                   ByVal multimeter As MultimeterEntity)
        Me.New(getterSetter, element, nomTypeId)
        Me.MultimeterId = multimeter.Id
        Me.MultimeterNumber = multimeter.MultimeterNumber
    End Sub

#End Region

#Region " GETTER SETTER "

    ''' <summary> Gets or sets the getter setter. </summary>
    ''' <value> The getter setter. </value>
    Public Property GetterSetter As isr.Core.Constructs.IGetterSetter(Of Double)

    ''' <summary> Gets the trait value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Double. </returns>
    Public Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Return Me.GetterSetter.Getter(name)
    End Function

    ''' <summary> Sets the trait value. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value"> value. </param>
    ''' <param name="name">  (Optional) Name of the runtime caller member. </param>
    ''' <returns> A nullable Double. </returns>
    Public Function Setter(ByVal value As Double?, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        If value.HasValue AndAlso Not Nullable.Equals(value, Me.Getter(name)) Then
            Me.GetterSetter.Setter(value.Value, name)
            Me.NotifyPropertyChanged(name)
        End If
        Return value
    End Function

#If False Then
    ''' <summary> Gets or sets the trait value. </summary>
    ''' <value> The trait value. </value>
    Protected Property TraitValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return Me.GetterSetter.Getter(name)
        End Get
        Set(value As Double?)
            If value.HasValue AndAlso Not Nullable.Equals(value, Me.TraitValue(name)) Then
                Me.GetterSetter.Setter(value.Value, name)
                Me.NotifyPropertyChanged(name)
            End If
        End Set
    End Property
#End If

#End Region

#Region " ELEMENT PROPERTIES "

    ''' <summary> The element label. </summary>
    Private _ElementLabel As String

    ''' <summary> Gets or sets the Label of the Element. </summary>
    ''' <value> The Label of the Element. </value>
    Public Property ElementLabel As String
        Get
            Return Me._ElementLabel
        End Get
        Set
            If Not String.Equals(Value, Me.ElementLabel) Then
                Me._ElementLabel = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Type of the element. </summary>
    Private _ElementType As ElementType

    ''' <summary> Gets or sets the type of the Element. </summary>
    ''' <value> The type of the Element. </value>
    Public Property ElementType As ElementType
        Get
            Return Me._ElementType
        End Get
        Set
            If Value <> Me.ElementType Then
                Me._ElementType = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The element ordinal number. </summary>
    Private _ElementOrdinalNumber As Integer

    ''' <summary> Gets or sets the Element Ordinal number. </summary>
    ''' <value> The ElementOrdinal number. </value>
    Public Property ElementOrdinalNumber As Integer
        Get
            Return Me._ElementOrdinalNumber
        End Get
        Set
            If Not Integer.Equals(Value, Me.ElementOrdinalNumber) Then
                Me._ElementOrdinalNumber = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Type of the nom. </summary>
    Private _NomType As NomType

    ''' <summary> Gets or sets the type of the nominal. </summary>
    ''' <value> The type of the nominal. </value>
    Public Property NomType As NomType
        Get
            Return Me._NomType
        End Get
        Set
            If Value <> Me.NomType Then
                Me._NomType = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Identifier for the multimeter. </summary>
    Private _MultimeterId As Integer

    ''' <summary>
    ''' Gets or sets the identifier of the <see cref="Entities.MultimeterEntity"/>.
    ''' </summary>
    ''' <value> Identifies the <see cref="Entities.MultimeterEntity"/>. </value>
    Public Property MultimeterId As Integer
        Get
            Return Me._MultimeterId
        End Get
        Set
            If Value <> Me.MultimeterId Then
                Me._MultimeterId = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The multimeter number. </summary>
    Private _MultimeterNumber As Integer

    ''' <summary> Gets or sets the multimeter number. </summary>
    ''' <value> The multimeter number. </value>
    Public Property MultimeterNumber As Integer
        Get
            Return Me._MultimeterNumber
        End Get
        Set
            If Value <> Me.MultimeterNumber Then
                Me._MultimeterNumber = Value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " NOMINAL VALUES "

    ''' <summary> Gets or sets the nominal value. </summary>
    ''' <value> The nominal value. </value>
    Public Property NominalValue As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the tolerance. </summary>
    ''' <value> The tolerance. </value>
    Public Property Tolerance As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the tracking tolerance. </summary>
    ''' <value> The tracking tolerance. </value>
    Public Property TrackingTolerance As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the tcr. </summary>
    ''' <value> The tcr. </value>
    Public Property Tcr As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Tcr Span. </summary>
    ''' <value> The Tcr Span. </value>
    Public Property TcrSpan As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the tracking tcr. </summary>
    ''' <value> The tracking tcr. </value>
    Public Property TrackingTcr As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the tracking Tcr Span. </summary>
    ''' <value> The tracking Tcr Span. </value>
    Public Property TrackingTcrSpan As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the lower overflow limit. </summary>
    ''' <value> The lower overflow limit. </value>
    Public Property LowerOverflowLimit As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the upper overflow limit. </summary>
    ''' <value> The upper overflow limit. </value>
    Public Property UpperOverflowLimit As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the overflow range. </summary>
    ''' <value> The overflow range. </value>
    Public Property OverflowRange As isr.Core.Constructs.RangeR
        Get
            Return If(Me.LowerOverflowLimit.HasValue AndAlso Me.UpperOverflowLimit.HasValue,
                New Core.Constructs.RangeR(Me.LowerOverflowLimit.Value, Me.UpperOverflowLimit.Value), Core.Constructs.RangeR.Empty)
        End Get
        Set(value As isr.Core.Constructs.RangeR)
            Me.LowerOverflowLimit = value.Min
            Me.UpperOverflowLimit = value.Max
        End Set
    End Property

#End Region

#Region " SPECIFICATION RANGE "

    ''' <summary> Gets or sets the lower specification limit. </summary>
    ''' <value> The lower specification limit. </value>
    Public Property LowerSpecificationLimit As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the upper specification limit. </summary>
    ''' <value> The upper specification limit. </value>
    Public Property UpperSpecificationLimit As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the specification range. </summary>
    ''' <value> The specification range. </value>
    Public Property SpecificationRange As isr.Core.Constructs.RangeR
        Get
            Return If(Me.LowerSpecificationLimit.HasValue AndAlso Me.UpperSpecificationLimit.HasValue,
                New Core.Constructs.RangeR(Me.LowerSpecificationLimit.Value, Me.UpperSpecificationLimit.Value),
                Core.Constructs.RangeR.Empty)
        End Get
        Set(value As isr.Core.Constructs.RangeR)
            Me.LowerSpecificationLimit = value.Min
            Me.UpperSpecificationLimit = value.Max
        End Set
    End Property

#End Region

#Region " GUARD "

    ''' <summary> Gets or sets the guard band factor. </summary>
    ''' <value> The guard band factor. </value>
    Public Property GuardBandFactor As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the sigma performance level. </summary>
    ''' <value> The sigma performance level. </value>
    Public Property SigmaPerformanceLevel As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets the estimated dispersion. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The estimated dispersion. </value>
    Public ReadOnly Property EstimatedDispersion As Double?
        Get
            If Not Me.SigmaPerformanceLevel.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.SigmaPerformanceLevel)} must be set before getting the {NameOf(NomTrait.EstimatedDispersion)}")
            If Not Me.Tolerance.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.Tolerance)} must be set before getting the {NameOf(NomTrait.EstimatedDispersion)}")
            Return Me.Tolerance / Me.SigmaPerformanceLevel
        End Get
    End Property

    ''' <summary> Gets or sets the dispersion. </summary>
    ''' <value> The dispersion. </value>
    Public Property Dispersion As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets the estimated guarded specification limit. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The estimated guarded specification limit. </value>
    Public ReadOnly Property EstimatedGuardedSpecificationLimit As Double
        Get
            If Not Me.GuardBandFactor.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.GuardBandFactor)} must be set before getting the {NameOf(NomTrait.EstimatedGuardedSpecificationRange)}")
            If Not Me.Tolerance.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.Tolerance)} must be set before getting the {NameOf(NomTrait.EstimatedGuardedSpecificationRange)}")
            If Not Me.SigmaPerformanceLevel.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.SigmaPerformanceLevel)} must be set before getting the {NameOf(NomTrait.EstimatedGuardedSpecificationRange)}")
            Return Me.Tolerance.Value * (1 - Me.GuardBandFactor.Value / Me.SigmaPerformanceLevel.Value)
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the guarded specification limit--this is the tolerance reduced by the guard band
    ''' factor.
    ''' </summary>
    ''' <value> The guarded specification limit. </value>
    Public Property GuardedSpecificationLimit As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the lower guarded specification limit. </summary>
    ''' <value> The lower guarded specification limit. </value>
    Public Property LowerGuardedSpecificationLimit As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the upper guarded specification limit. </summary>
    ''' <value> The upper guarded specification limit. </value>
    Public Property UpperGuardedSpecificationLimit As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the guarded specification range. </summary>
    ''' <value> The guarded specification range. </value>
    Public Property GuardedSpecificationRange As isr.Core.Constructs.RangeR
        Get
            Return If(Me.LowerGuardedSpecificationLimit.HasValue AndAlso Me.UpperGuardedSpecificationLimit.HasValue,
                New Core.Constructs.RangeR(Me.LowerGuardedSpecificationLimit.Value, Me.UpperGuardedSpecificationLimit.Value),
                Core.Constructs.RangeR.Empty)
        End Get
        Set(value As isr.Core.Constructs.RangeR)
            Me.LowerGuardedSpecificationLimit = value.Min
            Me.UpperGuardedSpecificationLimit = value.Max
        End Set
    End Property

    ''' <summary> Gets the estimated guard band. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The estimated guard band. </value>
    Public ReadOnly Property EstimatedGuardBand As Double?
        Get
            If Not Me.GuardBandFactor.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.GuardBandFactor)} must be set before getting the {NameOf(NomTrait.EstimatedGuardBand)}")
            If Not Me.Tolerance.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.Tolerance)} must be set before getting the {NameOf(NomTrait.EstimatedGuardBand)}")
            If Not Me.SigmaPerformanceLevel.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.SigmaPerformanceLevel)} must be set before getting the {NameOf(NomTrait.EstimatedGuardBand)}")
            Return Me.GuardBandFactor * Me.Tolerance / Me.SigmaPerformanceLevel
        End Get
    End Property

    ''' <summary> Gets or sets the guard band. </summary>
    ''' <value> The guard band. </value>
    Public Property GuardBand As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets the estimated specification range. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The specification range. </value>
    Public ReadOnly Property EstimatedSpecificationRange As isr.Core.Constructs.RangeR
        Get
            If Not Me.NominalValue.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.NominalValue)} must be set before getting the {NameOf(NomTrait.SpecificationRange)}")
            If Not Me.Tolerance.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.Tolerance)} must be set before getting the {NameOf(NomTrait.SpecificationRange)}")
            Return New Core.Constructs.RangeR(Me.NominalValue.Value * (1 - Me.Tolerance.Value), Me.NominalValue.Value * (1 + Me.Tolerance.Value))
        End Get
    End Property

    ''' <summary> Gets the estimated guarded specification range. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The estimated guarded specification range. </value>
    Public ReadOnly Property EstimatedGuardedSpecificationRange As isr.Core.Constructs.RangeR
        Get
            If Not Me.NominalValue.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.NominalValue)} must be set before getting the {NameOf(NomTrait.EstimatedGuardedSpecificationRange)}")
            If Not Me.GuardBandFactor.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.GuardBandFactor)} must be set before getting the {NameOf(NomTrait.EstimatedGuardedSpecificationRange)}")
            If Not Me.Tolerance.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.Tolerance)} must be set before getting the {NameOf(NomTrait.EstimatedGuardedSpecificationRange)}")
            If Not Me.SigmaPerformanceLevel.HasValue Then Throw New InvalidOperationException($"{NameOf(NomTrait.SigmaPerformanceLevel)} must be set before getting the {NameOf(NomTrait.EstimatedGuardedSpecificationRange)}")
            Dim guardedTolerance As Double = Me.Tolerance.Value * (1 - Me.GuardBandFactor.Value / Me.SigmaPerformanceLevel.Value)
            Return New Core.Constructs.RangeR(Me.NominalValue.Value * (1 - guardedTolerance), Me.NominalValue.Value * (1 + guardedTolerance))
        End Get
    End Property

#End Region

#Region " ANNEALING "

    ''' <summary> Gets or sets the lower Tan value. </summary>
    ''' <value> The lower Tan value. </value>
    Public Property LowerTanValue As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the upper Tan value. </summary>
    ''' <value> The upper Tan value. </value>
    Public Property UpperTanValue As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Tan value range. </summary>
    ''' <value> The Tan value range. </value>
    Public Property TanValueRange As isr.Core.Constructs.RangeR
        Get
            Return If(Me.LowerTanValue.HasValue AndAlso Me.UpperTanValue.HasValue,
                New Core.Constructs.RangeR(Me.LowerTanValue.Value, Me.UpperTanValue.Value),
                Core.Constructs.RangeR.Empty)
        End Get
        Set(value As isr.Core.Constructs.RangeR)
            Me.LowerTanValue = value.Min
            Me.UpperTanValue = value.Max
        End Set
    End Property

    ''' <summary> Gets or sets the lower annealing shift. </summary>
    ''' <value> The lower annealing shift. </value>
    Public Property LowerAnnealingShift As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the upper annealing. </summary>
    ''' <value> The upper annealing shift. </value>
    Public Property UpperAnnealingShift As Double?
        Get
            Return Me.Getter()
        End Get
        Set(value As Double?)
            Me.Setter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the annealing shift range. </summary>
    ''' <value> The annealing shift. </value>
    Public Property AnnealingShiftRange As isr.Core.Constructs.RangeR
        Get
            Return If(Me.LowerAnnealingShift.HasValue AndAlso Me.UpperAnnealingShift.HasValue,
                New Core.Constructs.RangeR(Me.LowerAnnealingShift.Value, Me.UpperAnnealingShift.Value),
                Core.Constructs.RangeR.Empty)
        End Get
        Set(value As isr.Core.Constructs.RangeR)
            Me.LowerAnnealingShift = value.Min
            Me.UpperAnnealingShift = value.Max
        End Set
    End Property

#End Region

#Region " CUSTOM PROPERTIES "

    ''' <summary> Gets the specification limit. </summary>
    ''' <value> The specification limit. </value>
    Public ReadOnly Property SpecificationLimit As Double?
        Get
            Return Me.Tolerance
        End Get
    End Property

    ''' <summary> Gets the decimal precision. </summary>
    ''' <value> The decimal precision. </value>
    Public ReadOnly Property DecimalPrecision As Integer
        Get
            Dim lowestTolerance As Double = 0.00005
            Dim precision As Double = 1 / Math.Max(lowestTolerance, Me.SpecificationLimit.GetValueOrDefault(lowestTolerance))
            Return If(precision <= 1, 0, CInt(Math.Floor(Math.Log10(precision))))
        End Get
    End Property

    ''' <summary> Information describing the percent format. </summary>
    Private _PercentFormatInfo As Globalization.NumberFormatInfo

    ''' <summary>
    ''' Number format informations for displaying values with the set specification limit.
    ''' </summary>
    ''' <value> Information describing the percent format. </value>
    Public ReadOnly Property PercentFormatInfo As Globalization.NumberFormatInfo
        Get
            If Me._PercentFormatInfo Is Nothing Then
                Me._PercentFormatInfo = New Globalization.NumberFormatInfo With {.PercentDecimalDigits = Me.DecimalPrecision}
            End If
            Return Me._PercentFormatInfo
        End Get
    End Property

    ''' <summary> Number of format informations. </summary>
    Private _NumberFormatInfo As Globalization.NumberFormatInfo

    ''' <summary>
    ''' Number format informations for displaying values with the set specification limit.
    ''' </summary>
    ''' <value> The total number of format information. </value>
    Public ReadOnly Property NumberFormatInfo As Globalization.NumberFormatInfo
        Get
            If Me._NumberFormatInfo Is Nothing Then
                Me._NumberFormatInfo = New Globalization.NumberFormatInfo With {.PercentDecimalDigits = Me.DecimalPrecision}
                Me._NumberFormatInfo.NumberDecimalDigits = Me._NumberFormatInfo.PercentDecimalDigits + 2
            End If
            Return Me._NumberFormatInfo
        End Get
    End Property

    ''' <summary> Information describing the date time format. </summary>
    Private _DateTimeFormatInfo As Globalization.DateTimeFormatInfo

    ''' <summary>
    ''' DateTime format informations for displaying values with the set specification limit.
    ''' </summary>
    ''' <value> Information describing the date time format. </value>
    Public ReadOnly Property DateTimeFormatInfo As Globalization.DateTimeFormatInfo
        Get
            If Me._DateTimeFormatInfo Is Nothing Then
                Me._DateTimeFormatInfo = New Globalization.DateTimeFormatInfo With {.ShortTimePattern = "HH:MM:ss.fff"}
            End If
            Return Me._DateTimeFormatInfo
        End Get
    End Property

#End Region

#Region " SHARED "

    ''' <summary> Select default nominal type. </summary>
    ''' <remarks> David, 6/10/2020. </remarks>
    ''' <param name="elementType"> Type of the element. </param>
    ''' <returns> A <see cref="NomType"/>. </returns>
    Public Shared Function SelectDefaultNomType(ByVal elementType As ElementType) As NomType
        Select Case elementType
            Case ElementType.CompoundResistor
                Return NomType.Resistance
            Case ElementType.Equation
                Return NomType.Equation
            Case ElementType.OpenCircuit
                Return NomType.Resistance
            Case ElementType.Resistor
                Return NomType.Resistance
            Case ElementType.ShortCircuit
                Return NomType.Resistance
            Case Else
                Return NomType.Resistance
        End Select
    End Function

#End Region

#Region " BIN "

    ''' <summary> Executes the bin operation. </summary>
    ''' <remarks> David, 6/13/2020. </remarks>
    ''' <param name="amount"> The amount. </param>
    ''' <returns> A ReadingBin. </returns>
    Public Function DoBin(ByVal amount As Double?) As ReadingBin
        If Not amount.HasValue Then
            Return ReadingBin.Invalid
        ElseIf Me.GuardedSpecificationRange.Contains(amount.Value) Then
            Return ReadingBin.Good
        ElseIf Not Me.OverflowRange.Contains(amount.Value) Then
            Return ReadingBin.Overflow
        ElseIf Me.GuardedSpecificationRange.Min > amount.Value Then
            Return ReadingBin.Low
        ElseIf Me.GuardedSpecificationRange.Max < amount.Value Then
            Return ReadingBin.High
        Else
            Return ReadingBin.Nameless
        End If
    End Function

#End Region

End Class
