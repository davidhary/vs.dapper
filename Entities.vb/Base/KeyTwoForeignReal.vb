Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions

''' <summary>
''' Interface for entities which have an identity key, two foreign keys and a real(Double)-Value
''' amount.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IKeyTwoForeignReal

    ''' <summary> Gets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    <Key>
    Property AutoId As Integer

    ''' <summary> Gets the identifier of the first foreign key. </summary>
    ''' <value> Identifies the first foreign key. </value>
    Property FirstForeignId As Integer

    ''' <summary> Gets the identifier of the second foreign key. </summary>
    ''' <value> Identifies the Second foreign key. </value>
    Property SecondForeignId As Integer

    ''' <summary> Gets the Real(Double)-value amount. </summary>
    ''' <value> The Real(Double)-value amount. </value>
    Property Amount As Double

End Interface

''' <summary>
''' A builder for entities which have an identity key, two foreign keys and real(Double)-Value
''' amount.
''' </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public MustInherit Class KeyTwoForeignRealBuilder

#Region " TABLE BUILDER "

    ''' <summary> Gets the name of the table. </summary>
    ''' <value> The table name this. </value>
    Protected MustOverride ReadOnly Property TableNameThis() As String

    ''' <summary> Gets the name of the first foreign reference table. </summary>
    ''' <value> The name of the first foreign reference table. </value>
    Public MustOverride Property FirstForeignTableName() As String

    ''' <summary> Gets the name of the first foreign reference table key. </summary>
    ''' <value> The name of the first foreign reference table key. </value>
    Public MustOverride Property FirstForeignTableKeyName() As String

    ''' <summary> Gets the name of the Second foreign reference table. </summary>
    ''' <value> The name of the Second foreign reference table. </value>
    Public MustOverride Property SecondForeignTableName() As String

    ''' <summary> Gets the name of the Second foreign reference table key. </summary>
    ''' <value> The name of the Second foreign reference table key. </value>
    Public MustOverride Property SecondForeignTableKeyName() As String

    ''' <summary> Gets the name of the First + Second Foreign Id index. </summary>
    ''' <value> The name of the First + Second Foreign Id index. </value>
    Private ReadOnly Property FistSecondForeignIdIndexName As String
        Get
            Return $"UQ_{Me.TableNameThis}_{NameOf(KeyTwoForeignRealNub.FirstForeignId)}_{NameOf(KeyTwoForeignRealNub.SecondForeignId)}"
        End Get
    End Property

    ''' <summary> Options for controlling the unique index. </summary>
    Private _UniqueIndexOptions As UniqueIndexOptions?

    ''' <summary> Queries unique index options. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The UniqueIndexOptions. </returns>
    Public Function QueryUniqueIndexOptions(ByVal connection As System.Data.IDbConnection) As UniqueIndexOptions
        If Not Me._UniqueIndexOptions.HasValue Then
            Me._UniqueIndexOptions = UniqueIndexOptions.None
            Me._UniqueIndexOptions = Me._UniqueIndexOptions Or If(connection.IndexExists(Me.FistSecondForeignIdIndexName),
                                                                                     UniqueIndexOptions.FirstSecondForeignId, UniqueIndexOptions.None)
        End If
        Return Me._UniqueIndexOptions.Value
    End Function

    ''' <summary> Gets or sets the date time format. </summary>
    ''' <value> The date time format. </value>
    Public Property DateTimeFormat As String = "yyyy-MM-dd HH:mm:ss.fff"

    ''' <summary> Updates or inserts records. </summary>
    ''' <remarks> David, 6/15/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entities">   The entities. </param>
    ''' <returns> An Integer. </returns>
    Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entities As IEnumerable(Of IKeyTwoForeignReal)) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection.Upsert(Me.TableNameThis, entities)
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="uniqueIndexOptions"> Options for controlling the unique index. </param>
    ''' <returns> The table name or empty. </returns>
    Public Function CreateTable(ByVal connection As System.Data.IDbConnection, ByVal uniqueIndexOptions As UniqueIndexOptions) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return Me.CreateTable(sql, uniqueIndexOptions)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return Me.CreateTable(sqlite, uniqueIndexOptions)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="uniqueIndexOptions"> Options for controlling the unique index. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection, ByVal uniqueIndexOptions As UniqueIndexOptions) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{Me.TableNameThis}] (
[{NameOf(KeyTwoForeignRealNub.AutoId)}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
[{NameOf(KeyTwoForeignRealNub.FirstForeignId)}] integer NOT NULL, 
[{NameOf(KeyTwoForeignRealNub.SecondForeignId)}] integer NOT NULL, 
[{NameOf(KeyTwoForeignRealNub.Amount)}] float NOT NULL, 
FOREIGN KEY ([{NameOf(KeyTwoForeignRealNub.FirstForeignId)}]) REFERENCES [{Me.FirstForeignTableName}] ([{Me.FirstForeignTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE
FOREIGN KEY ([{NameOf(KeyTwoForeignRealNub.SecondForeignId)}]) REFERENCES [{Me.SecondForeignTableName}] ([{Me.SecondForeignTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); ")

        If 0 <> (uniqueIndexOptions And UniqueIndexOptions.FirstSecondForeignId) Then
            queryBuilder.AppendLine($"CREATE UNIQUE INDEX IF NOT EXISTS [{Me.FistSecondForeignIdIndexName}] ON [{Me.TableNameThis}] ([{NameOf(KeyTwoForeignRealNub.FirstForeignId)}],[{NameOf(KeyTwoForeignRealNub.SecondForeignId)}]); ")
        End If

        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="uniqueIndexOptions"> Options for controlling the unique index. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection, ByVal uniqueIndexOptions As UniqueIndexOptions) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{Me.TableNameThis}](
	[{NameOf(KeyTwoForeignRealNub.AutoId)}] [int] IDENTITY(1,1) NOT NULL,
	[{NameOf(KeyTwoForeignRealNub.FirstForeignId)}] [int] NOT NULL,
	[{NameOf(KeyTwoForeignRealNub.SecondForeignId)}] [int] NOT NULL,
	[{NameOf(KeyTwoForeignRealNub.Amount)}] [float] NOT NULL,
 CONSTRAINT [PK_{Me.TableNameThis}] PRIMARY KEY CLUSTERED 
([{NameOf(KeyTwoForeignRealNub.AutoId)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END; 

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.FirstForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{Me.TableNameThis}'))
    ALTER TABLE [dbo].[{Me.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{Me.TableNameThis}_{Me.FirstForeignTableName}] FOREIGN KEY([{NameOf(KeyTwoForeignRealNub.FirstForeignId)}])
    REFERENCES [dbo].[{Me.FirstForeignTableName}] ([{Me.FirstForeignTableKeyName}])
    ON UPDATE CASCADE ON DELETE CASCADE; 

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.FirstForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{Me.TableNameThis}'))
    ALTER TABLE [dbo].[{Me.TableNameThis}] CHECK CONSTRAINT [FK_{Me.TableNameThis}_{Me.FirstForeignTableName}];

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.SecondForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{Me.TableNameThis}'))
    ALTER TABLE [dbo].[{Me.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{Me.TableNameThis}_{Me.SecondForeignTableName}] FOREIGN KEY([{NameOf(KeyTwoForeignRealNub.SecondForeignId)}])
    REFERENCES [dbo].[{Me.SecondForeignTableName}] ([{Me.SecondForeignTableKeyName}])
    ON UPDATE CASCADE ON DELETE CASCADE; 

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.SecondForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{Me.TableNameThis}'))
    ALTER TABLE [dbo].[{Me.TableNameThis}] CHECK CONSTRAINT [FK_{Me.TableNameThis}_{Me.SecondForeignTableName}];")

        If 0 <> (uniqueIndexOptions And UniqueIndexOptions.FirstSecondForeignId) Then
            queryBuilder.AppendLine($"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND name = N'{Me.FistSecondForeignIdIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{Me.FistSecondForeignIdIndexName}] ON [dbo].[{Me.TableNameThis}] ([{NameOf(KeyTwoForeignRealNub.FirstForeignId)}] ASC, [{NameOf(KeyTwoForeignRealNub.SecondForeignId)}] ASC) 
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; ")
        End If

        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

#End Region

#Region " CREATE VIEW "

    ''' <summary> Gets or sets the name of the view. </summary>
    ''' <value> The name of the view. </value>
    Public ReadOnly Property ViewName As String = $"{Me.TableNameThis}View"

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public MustOverride Property AutoIdFieldName() As String

    ''' <summary> Gets or sets the name of the First foreign identifier field. </summary>
    ''' <value> The name of the First foreign identifier field. </value>
    Public MustOverride Property FirstForeignIdFieldName() As String

    ''' <summary> Gets or sets the name of the second foreign identifier field. </summary>
    ''' <value> The name of the second foreign identifier field. </value>
    Public MustOverride Property SecondForeignIdFieldName() As String

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public MustOverride Property AmountFieldName() As String

    ''' <summary> Creates a View. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="dropFirst">  True to drop first. </param>
    ''' <returns> The view name. </returns>
    Public Function CreateView(ByVal connection As System.Data.IDbConnection, ByVal dropFirst As Boolean) As String
        connection.CreateView(Me.ViewName, Me.BuildViewSelectQuery, dropFirst)
        Return Me.ViewName
    End Function

    ''' <summary> Builds view select query. </summary>
    ''' <remarks> David, 8/11/2020. </remarks>
    ''' <returns> The View Select Query. </returns>
    Protected Overridable Function BuildViewSelectQuery() As String
        Return $"
SELECT 
    [{NameOf(KeyTwoForeignRealNub.AutoId)}] AS [{Me.AutoIdFieldName}], 
    [{NameOf(KeyTwoForeignRealNub.FirstForeignId)}] AS [{Me.FirstForeignIdFieldName}], 
    [{NameOf(KeyTwoForeignRealNub.SecondForeignId)}] AS [{Me.SecondForeignIdFieldName}], 
    [{NameOf(KeyTwoForeignRealNub.Amount)}] AS [{Me.AmountFieldName}]
FROM [{Me.TableNameThis}]"
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="IKeyTwoForeignReal">interface</see>
''' of entities which have an identity key, two foreign keys and real(Double)-Value amount.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2020-04-27 </para>
''' </remarks>
Public MustInherit Class KeyTwoForeignRealNub
    Inherits EntityNubBase(Of IKeyTwoForeignReal)
    Implements IKeyTwoForeignReal

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IKeyTwoForeignReal
        Dim destination As IKeyTwoForeignReal = Me.CreateNew
        KeyTwoForeignRealNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IKeyTwoForeignReal)
        KeyTwoForeignRealNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IKeyTwoForeignReal, ByVal destination As IKeyTwoForeignReal)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.AutoId = source.AutoId
        destination.FirstForeignId = source.FirstForeignId
        destination.SecondForeignId = source.SecondForeignId
        destination.Amount = source.Amount
    End Sub

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    <Key>
    Public Property AutoId As Integer Implements IKeyTwoForeignReal.AutoId

    ''' <summary> Gets or sets Identifies the first foreign reference table. </summary>
    ''' <value> Identifies the first foreign reference table. </value>
    Public Property FirstForeignId As Integer Implements IKeyTwoForeignReal.FirstForeignId

    ''' <summary> Gets or sets Identifies the Second foreign reference table. </summary>
    ''' <value> Identifies the Second foreign reference table. </value>
    Public Property SecondForeignId As Integer Implements IKeyTwoForeignReal.SecondForeignId

    ''' <summary> Gets or sets the Real(Double)-value amount. </summary>
    ''' <value> The Real(Double)-value amount. </value>
    Public Property Amount As Double Implements IKeyTwoForeignReal.Amount

#End Region

#Region " EQUALS "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IKeyTwoForeignReal))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IKeyTwoForeignReal) As Boolean
        Return other IsNot Nothing AndAlso KeyTwoForeignRealNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IKeyTwoForeignReal, ByVal right As IKeyTwoForeignReal) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.AutoId, right.AutoId)
            result = result AndAlso Integer.Equals(left.FirstForeignId, right.FirstForeignId)
            result = result AndAlso Integer.Equals(left.SecondForeignId, right.SecondForeignId)
            result = result AndAlso Double.Equals(left.Amount, right.Amount)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.AutoId Xor Me.FirstForeignId Xor Me.SecondForeignId Xor Me.Amount.GetHashCode()
    End Function

    #End Region

End Class

