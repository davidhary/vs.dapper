Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions

Imports isr.Dapper.Entities.ConnectionExtensions

''' <summary>
''' Interface for the Two-to-Many+Real-Value nub and Entity. Includes the fields as kept in the
''' data table. Allows tracking of property changes.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface ITwoToManyReal

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    <ExplicitKey>
    Property PrimaryId As Integer

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    <ExplicitKey>
    Property SecondaryId As Integer

    ''' <summary> Gets or sets the Real(Double)-value amount. </summary>
    ''' <value> The Real(Double)-value amount. </value>
    Property Amount As Double

End Interface

''' <summary>
''' A builder for entities which have Two-to-Many relationship and a Real(Double)-value.
''' </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public MustInherit Class TwoToManyRealBuilder

#Region " TABLE BUILDER "

    ''' <summary> Gets or sets the name of the table. </summary>
    ''' <value> The table name this. </value>
    Protected MustOverride ReadOnly Property TableNameThis() As String

    ''' <summary> Gets or sets the name of the primary table. </summary>
    ''' <value> The name of the primary table. </value>
    Public MustOverride Property PrimaryTableName() As String

    ''' <summary> Gets or sets the name of the primary table key. </summary>
    ''' <value> The name of the primary table key. </value>
    Public MustOverride Property PrimaryTableKeyName() As String

    ''' <summary> Gets or sets the name of the secondary table. </summary>
    ''' <value> The name of the secondary table. </value>
    Public MustOverride Property SecondaryTableName() As String

    ''' <summary> Gets or sets the name of the secondary table key. </summary>
    ''' <value> The name of the secondary table key. </value>
    Public MustOverride Property SecondaryTableKeyName() As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entities">   The entities. </param>
    ''' <returns> An Integer. </returns>
    Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entities As IEnumerable(Of ITwoToManyReal)) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection.Upsert(Me.TableNameThis, entities)
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return Me.CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return Me.CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{Me.TableNameThis}] (
	[{NameOf(TwoToManyRealNub.PrimaryId)}] integer NOT NULL, 
	[{NameOf(TwoToManyRealNub.SecondaryId)}] integer NOT NULL,
[{NameOf(TwoToManyRealNub.Amount)}] float NOT NULL, 
CONSTRAINT [sqlite_autoindex_{Me.TableNameThis}_1] PRIMARY KEY ([{NameOf(TwoToManyRealNub.PrimaryId)}], [{NameOf(TwoToManyRealNub.SecondaryId)}]),
FOREIGN KEY ([{NameOf(TwoToManyRealNub.PrimaryId)}]) REFERENCES [{Me.PrimaryTableName}] ([{Me.PrimaryTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY ([{NameOf(TwoToManyRealNub.SecondaryId)}]) REFERENCES [{Me.SecondaryTableName}] ([{Me.SecondaryTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{Me.TableNameThis}](
	[{NameOf(TwoToManyRealNub.PrimaryId)}] [int] NOT NULL,
	[{NameOf(TwoToManyRealNub.SecondaryId)}] [int] NOT NULL,
	[{NameOf(TwoToManyRealNub.Amount)}] [float] NOT NULL,
 CONSTRAINT [PK_{Me.TableNameThis}] PRIMARY KEY CLUSTERED
([{NameOf(TwoToManyRealNub.PrimaryId)}] ASC, [{NameOf(TwoToManyRealNub.SecondaryId)}] ASC] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END;

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.SecondaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]'))
ALTER TABLE [dbo].[{Me.TableNameThis}] WITH CHECK ADD  CONSTRAINT [FK_{Me.TableNameThis}_{Me.SecondaryTableName}] FOREIGN KEY([{NameOf(TwoToManyRealNub.SecondaryId)}])
REFERENCES [dbo].[{Me.SecondaryTableName}] ([{Me.SecondaryTableKeyName}])
ON UPDATE CASCADE ON DELETE CASCADE; 
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.SecondaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]'))
ALTER TABLE [dbo].[{Me.TableNameThis}] CHECK CONSTRAINT [FK_{Me.TableNameThis}_{Me.SecondaryTableName}];

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.PrimaryTableName}') AND parent_object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]'))
ALTER TABLE [dbo].[{Me.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{Me.TableNameThis}_{Me.PrimaryTableName}] FOREIGN KEY([{NameOf(TwoToManyRealNub.PrimaryId)}])
REFERENCES [dbo].[{Me.PrimaryTableName}] ([{Me.PrimaryTableKeyName}])
ON UPDATE CASCADE ON DELETE CASCADE; 
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.PrimaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]'))
ALTER TABLE [dbo].[{Me.TableNameThis}] CHECK CONSTRAINT [FK_{Me.TableNameThis}_{Me.PrimaryTableName}]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

#End Region

#Region " CREATE VIEW "

    ''' <summary> Gets or sets the name of the view. </summary>
    ''' <value> The name of the view. </value>
    Public ReadOnly Property ViewName As String = $"{Me.TableNameThis}View"

    ''' <summary> Gets or sets the name of the primary identifier field. </summary>
    ''' <value> The name of the primary identifier field. </value>
    Public MustOverride Property PrimaryIdFieldName() As String

    ''' <summary> Gets or sets the name of the secondary identifier field. </summary>
    ''' <value> The name of the secondary identifier field. </value>
    Public MustOverride Property SecondaryIdFieldName() As String

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public MustOverride Property AmountFieldName() As String

    ''' <summary> Creates a View. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="dropFirst">  True to drop first. </param>
    ''' <returns> The view name. </returns>
    Public Function CreateView(ByVal connection As System.Data.IDbConnection, ByVal dropFirst As Boolean) As String
        connection.CreateView(Me.ViewName, Me.BuildViewSelectQuery, dropFirst)
        Return Me.ViewName
    End Function

    ''' <summary> Builds view select query. </summary>
    ''' <remarks> David, 8/11/2020. </remarks>
    ''' <returns> The View Select Query. </returns>
    Protected Overridable Function BuildViewSelectQuery() As String
        Return $"
SELECT 
    [{NameOf(TwoToManyRealNub.PrimaryId)}] AS [{Me.PrimaryIdFieldName}], 
    [{NameOf(TwoToManyRealNub.SecondaryId)}] AS [{Me.SecondaryIdFieldName}],
    [{NameOf(TwoToManyRealNub.Amount)}] AS [{Me.AmountFieldName}]
FROM [{Me.TableNameThis}]"
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="ITwoToManyReal">interface</see>
''' of entities which are indexed and identified by Two keys for selecting a Real(Double)-value
''' amount.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2020-04-27 </para>
''' </remarks>
Public MustInherit Class TwoToManyRealNub
    Inherits Entity.EntityNubBase(Of ITwoToManyReal)
    Implements ITwoToManyReal

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ITwoToManyReal
        Dim destination As ITwoToManyReal = Me.CreateNew
        TwoToManyRealNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ITwoToManyReal)
        TwoToManyRealNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As ITwoToManyReal, ByVal destination As ITwoToManyReal)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.PrimaryId = source.PrimaryId
        destination.SecondaryId = source.SecondaryId
        destination.Amount = source.Amount
    End Sub

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the primary reference. </summary>
    ''' <value> Identifies the primary reference. </value>
    <ExplicitKey>
    Public Property PrimaryId As Integer Implements ITwoToManyReal.PrimaryId

    ''' <summary> Gets or sets the identifier of the Secondary reference. </summary>
    ''' <value> The identifier of Secondary reference. </value>
    <ExplicitKey>
    Public Property SecondaryId As Integer Implements ITwoToManyReal.SecondaryId

    ''' <summary> Gets or sets the Real(Double)-value amount. </summary>
    ''' <value> The Real(Double)-value amount. </value>
    Public Property Amount As Double Implements ITwoToManyReal.Amount

#End Region

#Region " EQUALS "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, ITwoToManyReal))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As ITwoToManyReal) As Boolean
        Return other IsNot Nothing AndAlso TwoToManyRealNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As ITwoToManyReal, ByVal right As ITwoToManyReal) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.PrimaryId, right.PrimaryId)
            result = result AndAlso Integer.Equals(left.SecondaryId, right.SecondaryId)
            result = result AndAlso Double.Equals(left.Amount, right.Amount)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.PrimaryId Xor Me.SecondaryId Xor Me.Amount.GetHashCode()
    End Function

    #End Region

End Class

''' <summary> A ternary key selector. </summary>
''' <remarks> David, 2020-04-27. </remarks>
Public Structure TwoKeySelector

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="primaryId">   Identifies the primary entity. </param>
    ''' <param name="secondaryId"> Identifies the secondary entity. </param>
    Public Sub New(ByVal primaryId As Integer, ByVal secondaryId As Integer)
        Me.PrimaryId = primaryId
        Me.SecondaryId = secondaryId
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As ITwoToManyReal)
        Me.PrimaryId = value.PrimaryId
        Me.SecondaryId = value.SecondaryId
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 7/6/2020. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As ITwoToManyLabel)
        Me.PrimaryId = value.PrimaryId
        Me.SecondaryId = value.SecondaryId
    End Sub

    ''' <summary> Gets or sets the identifier of the primary entity. </summary>
    ''' <value> Identifies the primary entity. </value>
    Public Property PrimaryId As Integer

    ''' <summary> Gets or sets the identifier of the secondary entity. </summary>
    ''' <value> Identifies the secondary entity. </value>
    Public Property SecondaryId As Integer

    ''' <summary> Tests if this TernaryKeySelector is considered equal to another. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="other"> The ternary key selector to compare to this. </param>
    ''' <returns> True if the objects are considered equal, false if they are not. </returns>
    Public Overloads Function Equals(ByVal other As TwoKeySelector) As Boolean
        Return Me.PrimaryId = other.PrimaryId AndAlso Me.SecondaryId = other.SecondaryId
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="obj"> The object to compare with the current instance. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
    ''' represent the same value; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.Equals(CType(obj, TwoKeySelector))
    End Function

    ''' <summary> Returns the hash code for this instance. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <returns> A 32-bit signed integer that is the hash code for this instance. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.PrimaryId Xor Me.SecondaryId
    End Function

    ''' <summary> Cast that converts the given TernaryKeySelector to a =. </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(left As TwoKeySelector, right As TwoKeySelector) As Boolean
        Return left.Equals(right)
    End Operator

    ''' <summary> Cast that converts the given TernaryKeySelector to a &lt;&gt; </summary>
    ''' <remarks> David, 5/23/2020. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(left As TwoKeySelector, right As TwoKeySelector) As Boolean
        Return Not left = right
    End Operator
End Structure

