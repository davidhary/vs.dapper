Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions
Imports isr.Dapper.Entity.EntityExtensions

''' <summary>
''' Interface for the nominal nub and entity. Includes the fields as kept in the data table.
''' Allows tracking of property changes.
''' </summary>
''' <remarks>
''' Nominal scales are used for labeling variables, without any quantitative value.  “Nominal”
''' scales could simply be called “labels.”  Here are some examples, below.  Notice that all of
''' these scales are mutually exclusive (no overlap) and none of them have any numerical
''' significance.  A good way to remember all of this is that “nominal” sounds a lot like “name”
''' and nominal scales are kind of like “names” or labels. Such values are typically represented
''' as Enum structures. <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface INominal

    ''' <summary> Gets the identifier of the nominal value. </summary>
    ''' <value> Identifies the nominal value. </value>
    <ExplicitKey>
    Property Id As Integer

    ''' <summary> Gets the label of the nominal value . </summary>
    ''' <value> The nominal value label. </value>
    Property Label As String

    ''' <summary> Gets the description of the nominal value . </summary>
    ''' <value> The nominal value description. </value>
    Property Description As String

End Interface

''' <summary> A builder for table or nominal scale values. </summary>
''' <remarks> David, 4/23/2020. </remarks>
Public MustInherit Class NominalBuilder

#Region " TABLE BUILDER "

    ''' <summary> Gets the name of the table. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The table name this. </value>
    Protected MustOverride ReadOnly Property TableNameThis() As String

    ''' <summary> Inserts or ignores the records described by the default enumeration type. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public MustOverride Function InsertIgnoreDefaultRecords(ByVal connection As System.Data.IDbConnection) As Integer

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entities">   The entities. </param>
    ''' <returns> An Integer. </returns>
    Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entities As IEnumerable(Of INominal)) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection.Upsert(Me.TableNameThis, entities)
    End Function

    ''' <summary> Inserts or ignores the entity records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entities">   The entities. </param>
    ''' <returns> An Integer. </returns>
    Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal entities As IEnumerable(Of INominal)) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection.InsertIgnore(Me.TableNameThis, entities)
    End Function

    ''' <summary> Inserts or ignores the records described by the enumeration type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="type">       The enumeration type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> An Integer. </returns>
    Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection.InsertIgnoreNameDescriptionRecords(Me.TableNameThis, GetType(INominal).EnumerateEntityFieldNames(), type, excluded)
    End Function

    ''' <summary> Gets the name of the label index. </summary>
    ''' <value> The name of the label index. </value>
    Private ReadOnly Property LabelIndexName As String
        Get
            Return $"UQ_{Me.TableNameThis}_{NameOf(KeyForeignLabelTimeNub.Label)}"
        End Get
    End Property

    ''' <summary> The using unique label. </summary>
    Private _UsingUniqueLabel As Boolean?

    ''' <summary> Indicates if the entity uses a unique Label. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function UsingUniqueLabel(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not Me._UsingUniqueLabel.HasValue Then
            Me._UsingUniqueLabel = connection.IndexExists(Me.LabelIndexName)
        End If
        Return Me._UsingUniqueLabel.Value
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return Me.CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return Me.CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{Me.TableNameThis}] (
	[{NameOf(NominalNub.Id)}] integer NOT NULL PRIMARY KEY, 
	[{NameOf(NominalNub.Label)}] nvarchar(50) NOT NULL, 
	[{NameOf(NominalNub.Description)}] nvarchar(250) NOT NULL); 
CREATE UNIQUE INDEX IF NOT EXISTS [{Me.LabelIndexName}] ON [{Me.TableNameThis}] ([{NameOf(NominalNub.Label)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{Me.TableNameThis}](
	[{NameOf(NominalNub.Id)}] [int] NOT NULL,
	[{NameOf(NominalNub.Label)}] [nvarchar](50) NOT NULL,
	[{NameOf(NominalNub.Description)}] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_{Me.TableNameThis}] PRIMARY KEY CLUSTERED 
([{NameOf(NominalNub.Id)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END; 
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND name = N'{Me.LabelIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{Me.LabelIndexName}] ON [dbo].[{Me.TableNameThis}] ([{NameOf(NominalNub.Label)}] ASC) 
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

#End Region

End Class

''' <summary>
''' Implements the nominal scale value table defined by <see cref="INominal">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/1/2020 </para>
''' </remarks>
<Table("Nominal")>
Public MustInherit Class NominalNub
    Inherits EntityNubBase(Of INominal)
    Implements INominal

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As INominal
        Dim destination As INominal = Me.CreateNew
        NominalNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As INominal)
        NominalNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As INominal, ByVal destination As INominal)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.Id = source.Id
        destination.Label = source.Label
        destination.Description = source.Description
    End Sub

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the nominal value. </summary>
    ''' <value> Identifies the nominal scale value. </value>
    <ExplicitKey>
    Public Property Id As Integer Implements INominal.Id

    ''' <summary> Gets or sets the nominal value label. </summary>
    ''' <value> The nominal value label. </value>
    Public Property Label As String Implements INominal.Label

    ''' <summary> Gets or sets the description of the nominal value . </summary>
    ''' <value> The nominal value description. </value>
    Public Property Description As String Implements INominal.Description

#End Region

#Region " EQUALS "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, INominal))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As INominal) As Boolean
        Return other IsNot Nothing AndAlso NominalNub.AreEqual(other, Me)
    End Function

    ''' <summary> Compares two entities based on their implemented interfaces. </summary>
    ''' <remarks> David, 4/25/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  Specifies the entity to compare to. </param>
    ''' <param name="right"> Specifies the entity to compare. </param>
    ''' <returns> <c>True</c> if the entities are equal. </returns>
    Public Overloads Shared Function AreEqual(ByVal left As INominal, ByVal right As INominal) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.Id, right.Id)
            result = result AndAlso String.Equals(left.Label, right.Label)
            result = result AndAlso String.Equals(left.Description, right.Description)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.Id Xor Me.Label.GetHashCode() Xor Me.Description.GetHashCode()
    End Function


    #End Region

End Class

