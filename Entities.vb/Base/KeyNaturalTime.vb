Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions

''' <summary>
''' Interface for entities which have an identity key, Natural(Integer) value and timestamp.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IKeyNaturalTime

    ''' <summary> Gets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    <Key>
    Property AutoId As Integer

    ''' <summary>
    ''' Gets the natural (whole) number representing an arbitrary or ordinal numeric value.
    ''' </summary>
    ''' <value> The natural amount. </value>
    Property Amount As Integer

    ''' <summary> Gets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
    ''' convert to local time. <para>
    ''' Computed attribute ignores the property on either insert or update.  
    ''' https://dapper-tutorial.net/knowledge-base/57673107/what-s-the-difference-between--computed--and--write-false---attributes-
    ''' The <see cref="KeyNaturalTimeBuilder.UpdateTimestamp(Data.IDbConnection, Integer, Date)"/> is
    ''' provided should the timestamp needs to be updated.         </para> <para>
    ''' Note the SQLite stores time in the database in the time zone of the server. For example, if
    ''' the server is located in LA, with a time zone of 7 hours relative to GMT (daylight saving
    ''' time) storing 11:00:00AM using universal time value of 18:00:00Z, will store the time on the
    ''' LA server as 11:00:00. To resolve these issues for both SQL Server the SQLite, time is
    ''' converted to UTC and then to a date time format with no time zone specification (no Z).
    ''' </para>
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Property Timestamp As DateTime

End Interface

''' <summary>
''' A builder for entities which have an identity key, Natural(Integer) value and timestamp.
''' </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public MustInherit Class KeyNaturalTimeBuilder

#Region " TABLE BUILDER "

    ''' <summary> Gets the name of the table. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The table name this. </value>
    Protected MustOverride ReadOnly Property TableNameThis() As String

    ''' <summary> Gets the date time format. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The date time format. </value>
    Public Property DateTimeFormat As String = "yyyy-MM-dd HH:mm:ss.fff"

    ''' <summary> Converts a value to a timestamp format. </summary>
    ''' <remarks> David, 6/21/2020. </remarks>
    ''' <param name="value"> The value Date/Time. </param>
    ''' <returns> Time value in the <see cref="DateTimeFormat"/> format. </returns>
    Public Function ToTimestampFormat(ByVal value As DateTime) As String
        Return value.ToString(Me.DateTimeFormat)
    End Function

    ''' <summary> Converts a value to an universal timestamp format. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="value"> The value Date/Time. </param>
    ''' <returns> Time value in the <see cref="DateTimeFormat"/> format. </returns>
    Public Function ToUniversalTimestampFormat(ByVal value As DateTime) As String
        Return value.ToUniversalTime.ToString(Me.DateTimeFormat)
    End Function

    ''' <summary> Updates the timestamp. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="key">                The key. </param>
    ''' <param name="universalTimestamp"> The universal (UTC) timestamp. </param>
    Public Sub UpdateTimestamp(ByVal connection As System.Data.IDbConnection, ByVal key As Integer, ByVal universalTimestamp As Date)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"UPDATE [{Me.TableNameThis}] ")
        queryBuilder.Append($"SET [{NameOf(KeyNaturalTimeNub.Timestamp)}] = '{Me.ToTimestampFormat(universalTimestamp)}' ")
        queryBuilder.Append($"WHERE [{NameOf(KeyNaturalTimeNub.AutoId)}] = {key}; ")
        connection.Execute(queryBuilder.ToString().Clean())
    End Sub

    ''' <summary> Gets the name of the ordinal number index. </summary>
    ''' <value> The name of the ordinal number index. </value>
    Private ReadOnly Property OrdinalNumberIndexName As String
        Get
            Return $"UQ_{Me.TableNameThis}_{NameOf(KeyNaturalTimeNub.Amount)}"
        End Get
    End Property

    ''' <summary> The using unique ordinal number. </summary>
    Private _UsingUniqueOrdinalNumber As Boolean?

    ''' <summary> Indicates if the entity uses a unique OrdinalNumber. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function UsingUniqueOrdinalNumber(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not Me._UsingUniqueOrdinalNumber.HasValue Then
            Me._UsingUniqueOrdinalNumber = connection.IndexExists($"{Me.OrdinalNumberIndexName}")
        End If
        Return Me._UsingUniqueOrdinalNumber.Value
    End Function

    ''' <summary> Creates a table using a non-unique <see cref="KeyNaturalTimeNub.Amount"/>. </summary>
    ''' <remarks>
    ''' This is the preferred selection for UUT, which have a unique number within a the lot or a
    ''' unique structure within a substrate.
    ''' </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table construction command. </returns>
    Public Function CreateTableNonUniqueAmount(ByVal connection As System.Data.IDbConnection) As String
        Return Me.CreateTable(connection, False)
    End Function

    ''' <summary> Creates a table using a unique <see cref="KeyNaturalTimeNub.Amount"/>. </summary>
    ''' <remarks> David, 5/4/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table construction command. </returns>
    Public Function CreateTableUniqueAmount(ByVal connection As System.Data.IDbConnection) As String
        Return Me.CreateTable(connection, True)
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="uniqueOrdinalNumber"> True to unique ordinal number. </param>
    ''' <returns> The table name or empty. </returns>
    Public Function CreateTable(ByVal connection As System.Data.IDbConnection, ByVal uniqueOrdinalNumber As Boolean) As String
        Me._UsingUniqueOrdinalNumber = uniqueOrdinalNumber
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return Me.CreateTable(sql, uniqueOrdinalNumber)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return Me.CreateTable(sqlite, uniqueOrdinalNumber)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="uniqueOrdinalNumber"> True to unique ordinal number. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection, ByVal uniqueOrdinalNumber As Boolean) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{Me.TableNameThis}] (
[{NameOf(KeyNaturalTimeNub.AutoId)}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
[{NameOf(KeyNaturalTimeNub.Amount)}] integer NOT NULL, 
[{NameOf(KeyNaturalTimeNub.Timestamp)}] datetime NOT NULL DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')));")
        If uniqueOrdinalNumber Then
            queryBuilder.AppendLine($"CREATE UNIQUE INDEX IF NOT EXISTS [{Me.OrdinalNumberIndexName}] ON [{Me.TableNameThis}] ([{NameOf(KeyNaturalTimeNub.Amount)}]); ")
        End If
        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="uniqueOrdinalNumber"> True to unique ordinal number. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection, ByVal uniqueOrdinalNumber As Boolean) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{Me.TableNameThis}](
	[{NameOf(KeyNaturalTimeNub.AutoId)}] [int] IDENTITY(1,1) NOT NULL,
	[{NameOf(KeyNaturalTimeNub.Amount)}] [int] NOT NULL,
	[{NameOf(KeyNaturalTimeNub.Timestamp)}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
 CONSTRAINT [PK_{Me.TableNameThis}] PRIMARY KEY CLUSTERED 
([{NameOf(KeyNaturalTimeNub.AutoId)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END; ")
        If uniqueOrdinalNumber Then
            queryBuilder.AppendLine($"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND name = N'{Me.OrdinalNumberIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{Me.OrdinalNumberIndexName}] ON [dbo].[{Me.TableNameThis}] ([{NameOf(KeyNaturalTimeNub.Amount)}] ASC) 
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; ")
        End If
        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

#End Region

#Region " CREATE VIEW "

    ''' <summary> Gets or sets the name of the view. </summary>
    ''' <value> The name of the view. </value>
    Public ReadOnly Property ViewName As String = $"{Me.TableNameThis}View"

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public MustOverride Property AutoIdFieldName() As String

    ''' <summary> Gets or sets the name of the amount field. </summary>
    ''' <value> The name of the amount field. </value>
    Public MustOverride Property AmountFieldName() As String

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <value> The name of the timestamp field. </value>
    Public MustOverride Property TimestampFieldName() As String

    ''' <summary> Creates a View. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="dropFirst">  True to drop first. </param>
    ''' <returns> The view name. </returns>
    Public Function CreateView(ByVal connection As System.Data.IDbConnection, ByVal dropFirst As Boolean) As String
        connection.CreateView(Me.ViewName, Me.BuildViewSelectQuery, dropFirst)
        Return Me.ViewName
    End Function

    ''' <summary> Builds view select query. </summary>
    ''' <remarks> David, 8/11/2020. </remarks>
    ''' <returns> The View Select Query. </returns>
    Protected Overridable Function BuildViewSelectQuery() As String
        Return $"
SELECT 
    [{NameOf(KeyNaturalTimeNub.AutoId)}] AS [{Me.AutoIdFieldName}], 
    [{NameOf(KeyNaturalTimeNub.Amount)}] AS [{Me.AmountFieldName}], 
    [{NameOf(KeyNaturalTimeNub.Timestamp)}] AS [{Me.TimestampFieldName}]  
FROM [{Me.TableNameThis}]"
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="IKeyNaturalTime">interface</see>
''' of entities which have an identity key, Natural(Integer) value and timestamp.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2020-04-27 </para>
''' </remarks>
Public MustInherit Class KeyNaturalTimeNub
    Inherits EntityNubBase(Of IKeyNaturalTime)
    Implements IKeyNaturalTime

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IKeyNaturalTime
        Dim destination As IKeyNaturalTime = Me.CreateNew
        KeyNaturalTimeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IKeyNaturalTime)
        KeyNaturalTimeNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IKeyNaturalTime, ByVal destination As IKeyNaturalTime)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.AutoId = source.AutoId
        destination.Amount = source.Amount
        destination.Timestamp = source.Timestamp
    End Sub

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    <Key>
    Public Property AutoId As Integer Implements IKeyNaturalTime.AutoId

    ''' <summary>
    ''' Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
    ''' </summary>
    ''' <value> The natural amount. </value>
    Public Property Amount As Integer Implements IKeyNaturalTime.Amount

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Public Property Timestamp As DateTime Implements IKeyNaturalTime.Timestamp

#End Region

#Region " EQUALS "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IKeyNaturalTime))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IKeyNaturalTime) As Boolean
        Return other IsNot Nothing AndAlso KeyNaturalTimeNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IKeyNaturalTime, ByVal right As IKeyNaturalTime) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.AutoId, right.AutoId)
            result = result AndAlso Integer.Equals(left.Amount, right.Amount)
            result = result AndAlso Date.Equals(left.Timestamp, right.Timestamp)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.AutoId Xor Me.Amount.GetHashCode() Xor Me.Timestamp.GetHashCode()
    End Function

    #End Region

End Class

