Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Dapper.Entities.ConnectionExtensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary>
''' Interface for entities which have an identity key, a foreign key and timestamp.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IKeyForeignTime

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    <Key>
    Property AutoId As Integer

    ''' <summary> Gets or sets the identifier of the foreign key. </summary>
    ''' <value> Identifies the foreign key. </value>
    Property ForeignId As Integer

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
    ''' convert to local time. <para>
    ''' Computed attribute ignores the property on either insert or update.  
    ''' https://dapper-tutorial.net/knowledge-base/57673107/what-s-the-difference-between--computed--and--write-false---attributes-
    ''' The <see cref="KeyForeignTimeBuilder.UpdateTimestamp(Data.IDbConnection, Integer, Date)"/> is
    ''' provided should the timestamp needs to be updated.         </para> <para>
    ''' Note the SQLite stores time in the database in the time zone of the server. For example, if
    ''' the server is located in LA, with a time zone of 7 hours relative to GMT (daylight saving
    ''' time) storing 11:00:00AM using universal time value of 18:00:00Z, will store the time on the
    ''' LA server as 11:00:00. To resolve these issues for both SQL Server the SQLite, time is
    ''' converted to UTC and then to a date time format with no time zone specification (no Z).
    ''' </para>
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Property Timestamp As DateTime

End Interface

''' <summary>
''' A builder for entities which have an identity key, a foreign key and timestamp.
''' </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public MustInherit Class KeyForeignTimeBuilder

#Region " TABLE BUILDER "

    ''' <summary> Gets or sets the name of the table. </summary>
    ''' <value> The table name this. </value>
    Protected MustOverride ReadOnly Property TableNameThis() As String

    ''' <summary> Gets or sets the name of the foreign table. </summary>
    ''' <value> The name of the foreign table. </value>
    Public MustOverride Property ForeignTableName() As String

    ''' <summary> Gets or sets the name of the foreign table key. </summary>
    ''' <value> The name of the foreign table key. </value>
    Public MustOverride Property ForeignTableKeyName() As String

    ''' <summary> Gets or sets the date time format. </summary>
    ''' <value> The date time format. </value>
    Public Property DateTimeFormat As String = "yyyy-MM-dd HH:mm:ss.fff"

    ''' <summary> Converts a value to a timestamp format. </summary>
    ''' <remarks> David, 6/21/2020. </remarks>
    ''' <param name="value"> The value Date/Time. </param>
    ''' <returns> Time value in the <see cref="DateTimeFormat"/> format. </returns>
    Public Function ToTimestampFormat(ByVal value As DateTime) As String
        Return value.ToString(Me.DateTimeFormat)
    End Function

    ''' <summary> Converts a value to an universal timestamp format. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="value"> The value Date/Time. </param>
    ''' <returns> Time value in the <see cref="DateTimeFormat"/> format. </returns>
    Public Function ToUniversalTimestampFormat(ByVal value As DateTime) As String
        Return value.ToUniversalTime.ToString(Me.DateTimeFormat)
    End Function

    ''' <summary> Updates the timestamp. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="key">                The key. </param>
    ''' <param name="universalTimestamp"> The universal (UTC) timestamp. </param>
    Public Sub UpdateTimestamp(ByVal connection As System.Data.IDbConnection, ByVal key As Integer, ByVal universalTimestamp As Date)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"UPDATE [{Me.TableNameThis}] ")
        queryBuilder.Append($"SET [{NameOf(KeyForeignTimeNub.Timestamp)}] = '{Me.ToTimestampFormat(universalTimestamp)}' ")
        queryBuilder.Append($"WHERE [{NameOf(KeyForeignTimeNub.AutoId)}] = {key}; ")
        connection.Execute(queryBuilder.ToString().Clean())
    End Sub

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return Me.CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return Me.CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{Me.TableNameThis}] (
[{NameOf(KeyForeignTimeNub.AutoId)}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
[{NameOf(KeyForeignTimeNub.ForeignId)}] integer NOT NULL, 
[{NameOf(KeyForeignTimeNub.Timestamp)}] datetime NOT NULL DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')),
FOREIGN KEY ([{NameOf(KeyForeignTimeNub.ForeignId)}]) REFERENCES [{Me.ForeignTableName}] ([{Me.ForeignTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{Me.TableNameThis}](
	[{NameOf(KeyForeignTimeNub.AutoId)}] [int] IDENTITY(1,1) NOT NULL,
	[{NameOf(KeyForeignTimeNub.ForeignId)}] [int] NOT NULL,
	[{NameOf(KeyForeignTimeNub.Timestamp)}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
 CONSTRAINT [PK_{Me.TableNameThis}] PRIMARY KEY CLUSTERED 
([{NameOf(KeyForeignTimeNub.AutoId)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END; 

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{Me.TableNameThis}'))
    ALTER TABLE [dbo].[{Me.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{Me.TableNameThis}_{Me.ForeignTableName}] FOREIGN KEY([{NameOf(KeyForeignTimeNub.ForeignId)}])
    REFERENCES [dbo].[{Me.ForeignTableName}] ([{Me.ForeignTableKeyName}])
    ON UPDATE CASCADE ON DELETE CASCADE; 

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{Me.TableNameThis}'))
    ALTER TABLE [dbo].[{Me.TableNameThis}] CHECK CONSTRAINT [FK_{Me.TableNameThis}_{Me.ForeignTableName}]; ")

        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

#End Region

#Region " CREATE VIEW "

    ''' <summary> Gets or sets the name of the view. </summary>
    ''' <value> The name of the view. </value>
    Public ReadOnly Property ViewName As String = $"{Me.TableNameThis}View"

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public MustOverride Property AutoIdFieldName() As String

    ''' <summary> Gets or sets the name of the foreign identifier field. </summary>
    ''' <value> The name of the foreign identifier field. </value>
    Public MustOverride Property ForeignIdFieldName() As String

    ''' <summary> Gets or sets the name of the timestamp field. </summary>
    ''' <value> The name of the timestamp field. </value>
    Public MustOverride Property TimestampFieldName() As String

    ''' <summary> Creates a View. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="dropFirst">  True to drop first. </param>
    ''' <returns> The view name. </returns>
    Public Function CreateView(ByVal connection As System.Data.IDbConnection, ByVal dropFirst As Boolean) As String
        connection.CreateView(Me.ViewName, Me.BuildViewSelectQuery, dropFirst)
        Return Me.ViewName
    End Function

    ''' <summary> Builds view select query. </summary>
    ''' <remarks> David, 8/11/2020. </remarks>
    ''' <returns> The View Select Query. </returns>
    Protected Overridable Function BuildViewSelectQuery() As String
        Return $"
SELECT 
    [{NameOf(KeyForeignTimeNub.AutoId)}] AS [{Me.AutoIdFieldName}], 
    [{NameOf(KeyForeignTimeNub.ForeignId)}] AS [{Me.ForeignIdFieldName}], 
    [{NameOf(KeyForeignNaturalTimeNub.Timestamp)}] AS [{Me.TimestampFieldName}]  
FROM [{Me.TableNameThis}]"
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="IKeyForeignTime">interface</see>
''' of entities which have an identity key, a foreign key and timestamp.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2020-04-27 </para>
''' </remarks>
Public MustInherit Class KeyForeignTimeNub
    Inherits EntityNubBase(Of IKeyForeignTime)
    Implements IKeyForeignTime

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IKeyForeignTime
        Dim destination As IKeyForeignTime = Me.CreateNew
        KeyForeignTimeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IKeyForeignTime)
        KeyForeignTimeNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IKeyForeignTime, ByVal destination As IKeyForeignTime)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.AutoId = source.AutoId
        destination.ForeignId = source.ForeignId
        destination.Timestamp = source.Timestamp
    End Sub

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    <Key>
    Public Property AutoId As Integer Implements IKeyForeignTime.AutoId

    ''' <summary> Gets or sets Identifies the foreign reference table. </summary>
    ''' <value> Identifies the foreign reference table. </value>
    Public Property ForeignId As Integer Implements IKeyForeignTime.ForeignId

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Public Property Timestamp As DateTime Implements IKeyForeignTime.Timestamp

#End Region

#Region " EQUALS "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IKeyForeignTime))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IKeyForeignTime) As Boolean
        Return other IsNot Nothing AndAlso KeyForeignTimeNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IKeyForeignTime, ByVal right As IKeyForeignTime) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.AutoId, right.AutoId)
            result = result AndAlso Integer.Equals(left.ForeignId, right.ForeignId)
            result = result AndAlso Date.Equals(left.Timestamp, right.Timestamp)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.AutoId Xor Me.ForeignId Xor Me.Timestamp.GetHashCode()
    End Function

    #End Region

End Class

