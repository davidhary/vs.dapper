Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions

''' <summary> Interface for entities which have an identity key, foreign key and Label. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IKeyForeignLabel

    ''' <summary> Gets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    <Key>
    Property AutoId As Integer

    ''' <summary> Gets Identifies the foreign reference table. </summary>
    ''' <value> Identifies the foreign reference table. </value>
    Property ForeignId As Integer

    ''' <summary> Gets the element Label. </summary>
    ''' <value> The Label. </value>
    Property Label As String

End Interface

''' <summary> A builder for entities which have an identity key, foreign key and Label. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public MustInherit Class KeyForeignLabelBuilder

#Region " CRATE TABLE "

    ''' <summary> Gets the name of the table. </summary>
    ''' <value> The table name this. </value>
    Protected MustOverride ReadOnly Property TableNameThis() As String

    ''' <summary> Gets the name of the foreign table. </summary>
    ''' <value> The name of the foreign table. </value>
    Public MustOverride Property ForeignTableName() As String

    ''' <summary> Gets the name of the foreign table key. </summary>
    ''' <value> The name of the foreign table key. </value>
    Public MustOverride Property ForeignTableKeyName() As String

    ''' <summary> Gets the size of the label field. </summary>
    ''' <value> The size of the label field. </value>
    Public MustOverride Property LabelFieldSize() As Integer

    ''' <summary> Gets the name of the ForeignId label index. </summary>
    ''' <value> The name of the ForeignId label index. </value>
    Private ReadOnly Property ForeignIdLabelIndexName As String
        Get
            Return $"UQ_{Me.TableNameThis}_{NameOf(KeyForeignLabelNub.ForeignId)}_{NameOf(KeyForeignLabelNub.Label)}"
        End Get
    End Property

    ''' <summary> Gets the name of the label index. </summary>
    ''' <value> The name of the label index. </value>
    Private ReadOnly Property LabelIndexName As String
        Get
            Return $"UQ_{Me.TableNameThis}_{NameOf(KeyForeignLabelNub.Label)}"
        End Get
    End Property

    ''' <summary> Gets the name of the ForeignId index. </summary>
    ''' <value> The name of the ForeignId index. </value>
    Private ReadOnly Property ForeignIdIndexName As String
        Get
            Return $"UQ_{Me.TableNameThis}_{NameOf(KeyForeignLabelNub.ForeignId)}"
        End Get
    End Property

    ''' <summary> Options for controlling the unique index. </summary>
    Private _UniqueIndexOptions As UniqueIndexOptions?

    ''' <summary> Queries unique index options. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The UniqueIndexOptions. </returns>
    Public Function QueryUniqueIndexOptions(ByVal connection As System.Data.IDbConnection) As UniqueIndexOptions
        If Not Me._UniqueIndexOptions.HasValue Then
            Me._UniqueIndexOptions = UniqueIndexOptions.None
            Me._UniqueIndexOptions = Me._UniqueIndexOptions Or If(connection.IndexExists(Me.ForeignIdLabelIndexName),
                                                                                     UniqueIndexOptions.ForeignIdLabel, UniqueIndexOptions.None)
            Me._UniqueIndexOptions = Me._UniqueIndexOptions Or If(connection.IndexExists(Me.LabelIndexName),
                                                                                     UniqueIndexOptions.Label, UniqueIndexOptions.None)
            Me._UniqueIndexOptions = Me._UniqueIndexOptions Or If(connection.IndexExists(Me.ForeignIdIndexName),
                                                                                     UniqueIndexOptions.ForeignId, UniqueIndexOptions.None)
        End If
        Return Me._UniqueIndexOptions.Value
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 5/31/2020. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="uniqueIndexOptions"> Options for controlling the unique index. </param>
    ''' <returns> The table name or empty. </returns>
    Public Function CreateTable(ByVal connection As System.Data.IDbConnection, ByVal uniqueIndexOptions As UniqueIndexOptions) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return Me.CreateTable(sql, uniqueIndexOptions)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return Me.CreateTable(sqlite, uniqueIndexOptions)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="uniqueIndexOptions"> Options for controlling the unique index. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection, ByVal uniqueIndexOptions As UniqueIndexOptions) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{Me.TableNameThis}] (
[{NameOf(KeyForeignLabelNub.AutoId)}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
[{NameOf(KeyForeignLabelNub.ForeignId)}] integer NOT NULL, 
[{NameOf(KeyForeignLabelNub.Label)}] nvarchar({Me.LabelFieldSize}) NOT NULL, 
FOREIGN KEY ([{NameOf(KeyForeignLabelNub.ForeignId)}]) REFERENCES [{Me.ForeignTableName}] ([{Me.ForeignTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); ")

        If 0 <> (uniqueIndexOptions And UniqueIndexOptions.Label) Then
            queryBuilder.AppendLine($"CREATE UNIQUE INDEX IF NOT EXISTS [{Me.LabelIndexName}] ON [{Me.TableNameThis}] ([{NameOf(KeyForeignLabelNub.Label)}]); ")
        End If
        If 0 <> (uniqueIndexOptions And UniqueIndexOptions.ForeignId) Then
            queryBuilder.AppendLine($"CREATE UNIQUE INDEX IF NOT EXISTS [{Me.ForeignIdIndexName}] ON [{Me.TableNameThis}] ([{NameOf(KeyForeignLabelNub.ForeignId)}]); ")
        End If
        If 0 <> (uniqueIndexOptions And UniqueIndexOptions.ForeignIdLabel) Then
            queryBuilder.AppendLine($"CREATE UNIQUE INDEX IF NOT EXISTS [{Me.ForeignIdLabelIndexName}] ON [{Me.TableNameThis}] ([{NameOf(KeyForeignLabelNub.ForeignId)}], [{NameOf(KeyForeignLabelNub.Label)}]); ")
        End If
        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="uniqueIndexOptions"> Options for controlling the unique index. </param>
    ''' <returns> The table name or empty. </returns>
    Private Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection, ByVal uniqueIndexOptions As UniqueIndexOptions) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{Me.TableNameThis}](
	[{NameOf(KeyForeignLabelNub.AutoId)}] [int] IDENTITY(1,1) NOT NULL,
	[{NameOf(KeyForeignLabelNub.ForeignId)}] [int] NOT NULL,
	[{NameOf(KeyForeignLabelNub.Label)}] [nvarchar]({Me.LabelFieldSize}) NOT NULL,
 CONSTRAINT [PK_{Me.TableNameThis}] PRIMARY KEY CLUSTERED 
([{NameOf(KeyForeignLabelNub.AutoId)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END; 

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{Me.TableNameThis}'))
    ALTER TABLE [dbo].[{Me.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{Me.TableNameThis}_{Me.ForeignTableName}] FOREIGN KEY([{NameOf(KeyForeignLabelNub.ForeignId)}])
    REFERENCES [dbo].[{Me.ForeignTableName}] ([{Me.ForeignTableKeyName}])
    ON UPDATE CASCADE ON DELETE CASCADE; 

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{Me.TableNameThis}_{Me.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{Me.TableNameThis}'))
    ALTER TABLE [dbo].[{Me.TableNameThis}] CHECK CONSTRAINT [FK_{Me.TableNameThis}_{Me.ForeignTableName}]; ")

        If 0 <> (uniqueIndexOptions And UniqueIndexOptions.Label) Then
            queryBuilder.AppendLine($"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND name = N'{Me.LabelIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{Me.LabelIndexName}] ON [dbo].[{Me.TableNameThis}] ([{NameOf(KeyForeignLabelNub.Label)}] ASC) 
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; ")
        End If

        If 0 <> (uniqueIndexOptions And UniqueIndexOptions.ForeignId) Then
            queryBuilder.AppendLine($"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND name = N'{Me.ForeignIdIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{Me.ForeignIdIndexName}] ON [dbo].[{Me.TableNameThis}] ([{NameOf(KeyForeignLabelNub.ForeignId)}] ASC) 
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; ")
        End If

        If 0 <> (uniqueIndexOptions And UniqueIndexOptions.ForeignIdLabel) Then
            queryBuilder.AppendLine($"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{Me.TableNameThis}]') AND name = N'{Me.ForeignIdLabelIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{Me.ForeignIdLabelIndexName}] ON [dbo].[{Me.TableNameThis}] ([{NameOf(KeyForeignLabelNub.ForeignId)}] ASC, [{NameOf(KeyForeignLabelNub.Label)}] ASC) 
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; ")
        End If

        connection.Execute(queryBuilder.ToString().Clean())
        Return Me.TableNameThis
    End Function

#End Region

#Region " CREATE VIEW "

    ''' <summary> Gets or sets the name of the view. </summary>
    ''' <value> The name of the view. </value>
    Public ReadOnly Property ViewName As String = $"{Me.TableNameThis}View"

    ''' <summary> Gets or sets the name of the automatic identifier field. </summary>
    ''' <value> The name of the automatic identifier field. </value>
    Public MustOverride Property AutoIdFieldName() As String

    ''' <summary> Gets or sets the name of the foreign identifier field. </summary>
    ''' <value> The name of the foreign identifier field. </value>
    Public MustOverride Property ForeignIdFieldName() As String

    ''' <summary> Gets or sets the name of the label field. </summary>
    ''' <value> The name of the label field. </value>
    Public MustOverride Property LabelFieldName() As String

    ''' <summary> Creates a View. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="dropFirst">  True to drop first. </param>
    ''' <returns> The view name. </returns>
    Public Function CreateView(ByVal connection As System.Data.IDbConnection, ByVal dropFirst As Boolean) As String
        connection.CreateView(Me.ViewName, Me.BuildViewSelectQuery, dropFirst)
        Return Me.ViewName
    End Function

    ''' <summary> Builds view select query. </summary>
    ''' <remarks> David, 8/11/2020. </remarks>
    ''' <returns> The View Select Query. </returns>
    Protected Overridable Function BuildViewSelectQuery() As String
        Return $"
SELECT 
    [{NameOf(KeyForeignLabelNub.AutoId)}] AS [{Me.AutoIdFieldName}], 
    [{NameOf(KeyForeignLabelNub.ForeignId)}] AS [{Me.ForeignIdFieldName}], 
    [{NameOf(KeyForeignLabelNub.Label)}] AS [{Me.LabelFieldName}] 
FROM [{Me.TableNameThis}]"
    End Function

#End Region

End Class

''' <summary>
''' Implements the <see cref="IKeyForeignLabel">interface</see>
''' of entities which have an identity key, foreign key and Label.
''' </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2020-04-27 </para>
''' </remarks>
Public MustInherit Class KeyForeignLabelNub
    Inherits EntityNubBase(Of IKeyForeignLabel)
    Implements IKeyForeignLabel

#Region " I TYPED CLONABLE "

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IKeyForeignLabel
        Dim destination As IKeyForeignLabel = Me.CreateNew
        KeyForeignLabelNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IKeyForeignLabel)
        KeyForeignLabelNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IKeyForeignLabel, ByVal destination As IKeyForeignLabel)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.AutoId = source.AutoId
        destination.ForeignId = source.ForeignId
        destination.Label = source.Label
    End Sub

#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the <see cref="Entities.ElementEntity"/>. </summary>
    ''' <value> Identifies the <see cref="Entities.ElementEntity"/>. </value>
    <Key>
    Public Property AutoId As Integer Implements IKeyForeignLabel.AutoId

    ''' <summary> Gets or sets Identifies the foreign reference table. </summary>
    ''' <value> Identifies the foreign reference table. </value>
    Public Property ForeignId As Integer Implements IKeyForeignLabel.ForeignId

    ''' <summary> Gets or sets the Ordinal Label. </summary>
    ''' <value> The Ordinal Label. </value>
    Public Property Label As String Implements IKeyForeignLabel.Label

#End Region

#Region " EQUALS "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IKeyForeignLabel))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IKeyForeignLabel) As Boolean
        Return other IsNot Nothing AndAlso KeyForeignLabelNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IKeyForeignLabel, ByVal right As IKeyForeignLabel) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.AutoId, right.AutoId)
            result = result AndAlso Integer.Equals(left.ForeignId, right.ForeignId)
            result = result AndAlso String.Equals(left.Label, right.Label)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.AutoId Xor Me.ForeignId Xor Me.Label.GetHashCode()
    End Function


    #End Region

End Class

