Imports isr.Core.EnumExtensions
Imports isr.Dapper.Entity.EntityExtensions

''' <summary> A SQL server SQL builder. </summary>
''' <remarks>
''' David, 5/26/2020. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class SqlServerSqlBuilder
    Inherits SqlBuilderBase

    ''' <summary>
    ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As SqlBuilderBase

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    <System.Runtime.InteropServices.ComVisible(False)>
    Public Shared Function [Get]() As SqlBuilderBase
        If SqlServerSqlBuilder._Instance Is Nothing Then
            SyncLock SyncLocker
                SqlServerSqlBuilder._Instance = New SqlServerSqlBuilder()
            End SyncLock
        End If
        Return SqlServerSqlBuilder._Instance
    End Function

    ''' <summary> Build a create view query. </summary>
    ''' <remarks> David, 8/11/2020. </remarks>
    ''' <param name="viewName">    Name of the view. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="dropFirst">   True to drop first. </param>
    ''' <returns> The built query. </returns>
    Public Overrides Function CreateView(ByVal viewName As String, ByVal selectQuery As String, ByVal dropFirst As Boolean) As String
        Dim queryBuilder As New System.Text.StringBuilder
        If dropFirst Then
            queryBuilder.AppendLine($"DROP VIEW IF EXISTS [{viewName}]; ")
        End If
        queryBuilder.AppendLine($"IF NOT EXISTS (SELECT * FROM sys.views WHERE name = '{viewName}')
BEGIN
    EXECUTE('CREATE VIEW [{viewName}] AS")
        queryBuilder.AppendLine(selectQuery)
        queryBuilder.Append("')
END; ")
        Return queryBuilder.ToString
    End Function

    ''' <summary> Inserts or updates <see cref="IFiveToManyNatural"/> records. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFiveToManyNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFiveToManyNatural).EnumerateEntityFieldNames()
        For Each entity As IFiveToManyNatural In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId} AND [{fieldNames(4)}] = {entity.QuinaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(5)} = {entity.Amount} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(4)}] = {entity.QuaternaryId} AND [{fieldNames(3)}] = {entity.QuinaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IFiveToManyNatural"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFiveToManyNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFiveToManyNatural).EnumerateEntityFieldNames()
        For Each entity As IFiveToManyNatural In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId} AND [{fieldNames(4)}] = {entity.QuinaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates <see cref="IFiveToManyReal"/> records. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFiveToManyReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFiveToManyReal).EnumerateEntityFieldNames()
        For Each entity As IFiveToManyReal In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId} AND [{fieldNames(4)}] = {entity.QuinaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(5)} = {entity.Amount} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(4)}] = {entity.QuaternaryId} AND [{fieldNames(3)}] = {entity.QuinaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IFiveToManyReal"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFiveToManyReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFiveToManyReal).EnumerateEntityFieldNames()
        For Each entity As IFiveToManyReal In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId} AND [{fieldNames(4)}] = {entity.QuinaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToMany)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IThreeToMany).EnumerateEntityFieldNames()
        For Each entity As IThreeToMany In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId}))  ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or ignore records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToMany)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IThreeToMany).EnumerateEntityFieldNames()
        For Each entity As IThreeToMany In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId}))  ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates <see cref="IFourToMany"/> records. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToMany)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFourToMany).EnumerateEntityFieldNames()
        For Each entity As IFourToMany In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId} AND [{fieldNames(4)}] = {entity.QuinaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IFourToMany"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToMany)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFourToMany).EnumerateEntityFieldNames()
        For Each entity As IFourToMany In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId} AND [{fieldNames(4)}] = {entity.QuinaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates <see cref="IFourToManyId"/> records. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyId)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFourToManyId).EnumerateEntityFieldNames()
        For Each entity As IFourToManyId In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(4)} = {entity.ForeignId} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId}  AND [{fieldNames(3)}] = {entity.QuaternaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.ForeignId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IFourToManyId"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyId)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFourToManyId).EnumerateEntityFieldNames()
        For Each entity As IFourToManyId In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.ForeignId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates <see cref="IFourToManyNatural"/> records. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFourToManyNatural).EnumerateEntityFieldNames()
        For Each entity As IFourToManyNatural In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(4)} = {entity.Amount} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId}  AND [{fieldNames(3)}] = {entity.QuaternaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IFourToManyNatural"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFourToManyNatural).EnumerateEntityFieldNames()
        For Each entity As IFourToManyNatural In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates <see cref="IFourToManyReal"/> records. </summary>
    ''' <remarks> David, 5/20/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFourToManyReal).EnumerateEntityFieldNames()
        For Each entity As IFourToManyReal In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(4)} = {entity.Amount} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId}  AND [{fieldNames(3)}] = {entity.QuaternaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IFourToManyReal"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IFourToManyReal).EnumerateEntityFieldNames()
        For Each entity As IFourToManyReal In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId} AND [{fieldNames(3)}] = {entity.QuaternaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToMany)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(ITwoToMany).EnumerateEntityFieldNames()
        For Each entity As ITwoToMany In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId}))  ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or ignore records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToMany)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(ITwoToMany).EnumerateEntityFieldNames()
        For Each entity As ITwoToMany In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId}))  ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToManyLabel)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(ITwoToManyLabel).EnumerateEntityFieldNames()
        For Each entity As ITwoToManyLabel In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = '{entity.Label}' ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},'{entity.Label}'); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToManyLabel)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(ITwoToManyLabel).EnumerateEntityFieldNames()
        For Each entity As ITwoToManyLabel In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},'{entity.Label}'); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToManyReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(ITwoToManyReal).EnumerateEntityFieldNames()
        For Each entity As ITwoToManyReal In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = {entity.Amount} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToManyReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(ITwoToManyReal).EnumerateEntityFieldNames()
        For Each entity As ITwoToManyReal In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates <see cref="IThreeToManyId"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyId)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IThreeToManyId).EnumerateEntityFieldNames()
        For Each entity As IThreeToManyId In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(3)} = {entity.ForeignId} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.ForeignId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IThreeToManyId"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyId)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IThreeToManyId).EnumerateEntityFieldNames()
        For Each entity As IThreeToManyId In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.ForeignId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyLabel)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IThreeToManyLabel).EnumerateEntityFieldNames()
        For Each entity As IThreeToManyLabel In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(3)} = '{entity.Label}' ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},'{entity.Label}'); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyLabel)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IThreeToManyLabel).EnumerateEntityFieldNames()
        For Each entity As IThreeToManyLabel In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},'{entity.Label}'); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IThreeToManyReal).EnumerateEntityFieldNames()
        For Each entity As IThreeToManyReal In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(3)} = {entity.Amount} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IThreeToManyReal).EnumerateEntityFieldNames()
        For Each entity As IThreeToManyReal In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} AND [{fieldNames(2)}] = {entity.TernaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyLabelNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IExplicitKeyLabelNatural).EnumerateEntityFieldNames()
        For Each entity As IExplicitKeyLabelNatural In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.Id}) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = '{entity.Label}', {fieldNames(2)} = {entity.Amount} ")
            builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.Id} ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.Id}, '{entity.Label}', {entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyLabelNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IExplicitKeyLabelNatural).EnumerateEntityFieldNames()
        For Each entity As IExplicitKeyLabelNatural In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.Id}) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.Id}, '{entity.Label}', {entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyForeignLabel)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IExplicitKeyForeignLabel).EnumerateEntityFieldNames()
        For Each entity As IExplicitKeyForeignLabel In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.Id}) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.ForeignId}, {fieldNames(2)} = '{entity.Label}' ")
            builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.Id} ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.Id}, {entity.ForeignId}, '{entity.Label}'); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyForeignLabel)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IExplicitKeyForeignLabel).EnumerateEntityFieldNames()
        For Each entity As IExplicitKeyForeignLabel In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.Id}) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.Id}, {entity.ForeignId}, '{entity.Label}'); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyForeignNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IExplicitKeyForeignNatural).EnumerateEntityFieldNames()
        For Each entity As IExplicitKeyForeignNatural In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.Id}) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.ForeignId}, {fieldNames(2)} = {entity.Amount} ")
            builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.Id} ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.Id}, {entity.ForeignId}, {entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyForeignNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IExplicitKeyForeignNatural).EnumerateEntityFieldNames()
        For Each entity As IExplicitKeyForeignNatural In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.Id}) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.Id}, {entity.ForeignId}, {entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeign)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeign).EnumerateEntityFieldNames()
        For Each entity As IKeyForeign In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.ForeignId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)}) VALUES ({entity.ForeignId}); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.ForeignId} ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId}; ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)}) VALUES ({entity.ForeignId}); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or ignore records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeign)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeign).EnumerateEntityFieldNames()
        For Each entity As IKeyForeign In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(1)}] = {entity.ForeignId})) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)}) VALUES ({entity.ForeignId}); ")
            Else
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)}) VALUES ({entity.ForeignId}); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates <see cref="IKeyForeignLabelNatural"/> records. </summary>
    ''' <remarks> David, 5/14/2020. Present implementation assumes a unique index. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyForeignLabelNatural"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabelNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignLabelNatural).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignLabelNatural In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.ForeignId} AND {fieldNames(2)} = '{entity.Label}') ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(3)} = {entity.Amount} ")
                builder.AppendLine($"WHERE ({fieldNames(1)} = {entity.ForeignId} AND {fieldNames(2)} = '{entity.Label}') ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, '{entity.Label}', {entity.Amount}); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.ForeignId}, {fieldNames(2)} = '{entity.Label}', {fieldNames(3)} = {entity.Amount} ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, '{entity.Label}', {entity.Amount}); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IKeyForeignLabelNatural"/> records. </summary>
    ''' <remarks> David, 5/14/2020. Present implementation assumes a unique index. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyForeignLabelNatural"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabelNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignLabelNatural).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignLabelNatural In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(1)}] = {entity.ForeignId} AND [{fieldNames(2)}] = '{entity.Label}')) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, '{entity.Label}', {entity.Amount}); ")
            Else
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, '{entity.Label}', {entity.Amount}); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates <see cref="IKeyForeignTime"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyForeignTime"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignTime).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignTime In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.ForeignId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE ({fieldNames(1)} = {entity.ForeignId}) ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.ForeignId}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.ForeignId}, {fieldNames(2)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.ForeignId}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IKeyForeignTime"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyForeignTime"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignTime).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignTime In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(1)}] = {entity.ForeignId})) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.ForeignId}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.ForeignId}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates <see cref="IKeyForeignReal"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyForeignReal"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignReal).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignReal In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.ForeignId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = {entity.Amount}' ")
                builder.AppendLine($"WHERE ({fieldNames(1)} = {fieldNames(1)} = {entity.ForeignId}) ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId},{entity.Amount}); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.ForeignId}, {fieldNames(2)} = {entity.Amount} ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.ForeignId},{entity.Amount}); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IKeyForeignReal"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyForeignReal"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignReal).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignReal In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.ForeignId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.ForeignId},{entity.Amount}); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.ForeignId}{entity.Amount}); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates <see cref="IKeyTwoForeignReal"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyTwoForeignReal"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTwoForeignReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyTwoForeignReal).EnumerateEntityFieldNames()
        For Each entity As IKeyTwoForeignReal In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.FirstForeignId} AND {fieldNames(2)} = {entity.SecondForeignId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(3)} = {entity.Amount}' ")
                builder.AppendLine($"WHERE ({fieldNames(1)} = {fieldNames(1)} = {entity.FirstForeignId} AND {fieldNames(2)} = {entity.SecondForeignId}) ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},{entity.Amount}); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.FirstForeignId}, {fieldNames(2)} = {entity.SecondForeignId}, {fieldNames(3)} = {entity.Amount} ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},{entity.Amount}); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IKeyTwoForeignReal"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyTwoForeignReal"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTwoForeignReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyTwoForeignReal).EnumerateEntityFieldNames()
        For Each entity As IKeyTwoForeignReal In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.FirstForeignId} AND {fieldNames(2)} = {entity.SecondForeignId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},{entity.Amount}); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},{entity.Amount}); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates <see cref="IKeyTwoForeignTime"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyTwoForeignTime"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTwoForeignTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyTwoForeignTime).EnumerateEntityFieldNames()
        For Each entity As IKeyTwoForeignTime In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.FirstForeignId} AND {fieldNames(2)} = {entity.SecondForeignId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(3)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE ({fieldNames(1)} = {fieldNames(1)} = {entity.FirstForeignId} AND {fieldNames(2)} = {entity.SecondForeignId}) ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},'{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.FirstForeignId}, {fieldNames(2)} = {entity.SecondForeignId}, {fieldNames(3)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},'{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores <see cref="IKeyTwoForeignTime"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyTwoForeignTime"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTwoForeignTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyTwoForeignTime).EnumerateEntityFieldNames()
        For Each entity As IKeyTwoForeignTime In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.FirstForeignId} AND {fieldNames(2)} = {entity.SecondForeignId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},'{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},'{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyLabelTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyLabelTime).EnumerateEntityFieldNames()
        For Each entity As IKeyLabelTime In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = '{entity.Label}') ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(3)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE ({fieldNames(1)} = '{entity.Label}') ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ('{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = '{entity.Label}', {fieldNames(2)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)})  VALUES ('{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyLabelTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyLabelTime).EnumerateEntityFieldNames()
        For Each entity As IKeyLabelTime In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(1)}] = '{entity.Label}')) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ('{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ('{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyLabelTimezone)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyLabelTimezone).EnumerateEntityFieldNames()
        For Each entity As IKeyLabelTimezone In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = '{entity.Label}') ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(3)} = '{ToTimestampFormat(entity.Timestamp)}', '{entity.TimezoneId}'")
                builder.AppendLine($"WHERE ({fieldNames(1)} = '{entity.Label}') ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ('{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}', '{entity.TimezoneId}'); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = '{entity.Label}', {fieldNames(2)} = '{ToTimestampFormat(entity.Timestamp)}', '{entity.TimezoneId}' ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ('{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}', '{entity.TimezoneId}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyLabelTimezone)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyLabelTimezone).EnumerateEntityFieldNames()
        For Each entity As IKeyLabelTimezone In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(1)}] = '{entity.Label}')) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ('{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}', '{entity.TimezoneId}'); ")
            Else
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ('{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}', '{entity.TimezoneId}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyNaturalTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyNaturalTime).EnumerateEntityFieldNames()
        For Each entity As IKeyNaturalTime In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.Amount}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE ({fieldNames(1)} = {entity.Amount}) ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.Amount}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.Amount}, {fieldNames(2)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.Amount}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyNaturalTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyNaturalTime).EnumerateEntityFieldNames()
        For Each entity As IKeyNaturalTime In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(1)}] = {entity.Amount})) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.Amount}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.Amount}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyTime).EnumerateEntityFieldNames()
        For Each entity As IKeyTime In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)}) VALUES ('{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)}) VALUES ('{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyTime).EnumerateEntityFieldNames()
        For Each entity As IKeyTime In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(1)}] = {entity.Timestamp})) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)}) VALUES ('{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)}) VALUES ('{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabel)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignLabel).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignLabel In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.ForeignId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = '{entity.Label}' ")
                builder.AppendLine($"WHERE ({fieldNames(1)} = {entity.ForeignId}) ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.ForeignId}, '{entity.Label}'); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.ForeignId}, {fieldNames(2)} = '{entity.Label}' ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.ForeignId}, '{entity.Label}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabel)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignLabel).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignLabel In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(1)}] = {entity.ForeignId})) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.ForeignId}, '{entity.Label}'; ")
            Else
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)}) VALUES ({entity.ForeignId}, '{entity.Label}'; ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabelTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignLabelTime).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignLabelTime In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.ForeignId} AND {fieldNames(2)} = '{entity.Label}') ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = '{entity.Label}', {fieldNames(3)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE ({fieldNames(1)} = {entity.ForeignId} AND {fieldNames(2)} = '{entity.Label}') ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, '{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.ForeignId}, {fieldNames(2)} = '{entity.Label}', {fieldNames(3)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, '{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabelTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignLabelTime).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignLabelTime In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(1)}] = {entity.ForeignId} AND [{fieldNames(2)}] = '{entity.Label}')) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, '{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, '{entity.Label}', '{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignNaturalTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignNaturalTime).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignNaturalTime In entities.ToArray
            If entity.AutoId = 0 Then
                ' assuming auto id begins at 1, an auto id of 0 means not assigned.
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames(1)} = {entity.ForeignId} AND {fieldNames(2)} = {entity.Amount}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.ForeignId}, {fieldNames(2)} = {entity.Amount}, {fieldNames(3)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE ({fieldNames(1)} = {entity.ForeignId} AND {fieldNames(2)} = {entity.Amount}) ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, {entity.Amount}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.ForeignId}, {fieldNames(2)} = {entity.Amount}, {fieldNames(3)} = '{ToTimestampFormat(entity.Timestamp)}' ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.AutoId} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, {entity.Amount}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignNaturalTime)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IKeyForeignNaturalTime).EnumerateEntityFieldNames()
        For Each entity As IKeyForeignNaturalTime In entities.ToArray
            If entity.AutoId = 0 Then
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(1)}] = {entity.ForeignId} AND [{fieldNames(2)}] = {entity.Amount}  )) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, {entity.Amount}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            Else
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.AutoId}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] ({fieldNames(1)},{fieldNames(2)},{fieldNames(3)}) VALUES ({entity.ForeignId}, {entity.Amount}, '{ToTimestampFormat(entity.Timestamp)}'); ")
            End If
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of INominal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(INominal).EnumerateEntityFieldNames()
        For Each entity As INominal In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.Id}) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = '{entity.Label}', {fieldNames(2)} = '{entity.Description}' ")
            builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.Id} ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.Id},'{entity.Label}','{entity.Description}'); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of INominal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(INominal).EnumerateEntityFieldNames()
        For Each entity As INominal In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.Id}) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.Id},'{entity.Label}','{entity.Description}'); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates keyed values. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyId)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToManyId).EnumerateEntityFieldNames()
        For Each entity As IOneToManyId In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = {entity.ForeignId} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.ForeignId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyId)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToManyId).EnumerateEntityFieldNames()
        For Each entity As IOneToManyId In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.ForeignId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates keyed values. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToManyNatural).EnumerateEntityFieldNames()
        For Each entity As IOneToManyNatural In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = {entity.Amount} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyNatural)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToManyNatural).EnumerateEntityFieldNames()
        For Each entity As IOneToManyNatural In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Update or insert values isolation. </summary>
    ''' <remarks>
    ''' David, 5/14/2020.
    ''' https://stackoverflow.com/questions/108403/solutions-for-insert-or-update-on-sql-server.
    ''' </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Shared Function UpsertValuesIsolation(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyRange)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToManyRange).EnumerateEntityFieldNames()
        For Each entity As IOneToManyRange In entities.ToArray
            builder.AppendLine("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE; ")
            builder.AppendLine("BEGIN TRANSACTION;")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = {entity.Min}, {fieldNames(3)} = {entity.Max} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId}) ")
            builder.AppendLine("If @@ROWCOUNT = 0 BEGIN ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Min},{entity.Max}) ")
            builder.AppendLine("END ")
            builder.AppendLine("COMMIT TRANSACTION; ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updated  keyed values. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyRange)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToManyRange).EnumerateEntityFieldNames()
        For Each entity As IOneToManyRange In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = {entity.Min}, {fieldNames(3)} = {entity.Max} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Min},{entity.Max}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyRange)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToManyRange).EnumerateEntityFieldNames()
        For Each entity As IOneToManyRange In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Min},{entity.Max}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates keyed values. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToManyReal).EnumerateEntityFieldNames()
        For Each entity As IOneToManyReal In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = {entity.Amount} ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyReal)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToManyReal).EnumerateEntityFieldNames()
        For Each entity As IOneToManyReal In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates keyed values. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyLabel)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToManyLabel).EnumerateEntityFieldNames()
        For Each entity As IOneToManyLabel In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(2)} = '{entity.Label}' ")
            builder.AppendLine($"WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId}) ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},'{entity.Label}'); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyLabel)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToManyLabel).EnumerateEntityFieldNames()
        For Each entity As IOneToManyLabel In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId})) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},'{entity.Label}'); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToMany)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToMany).EnumerateEntityFieldNames()
        For Each entity As IOneToMany In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} ))  ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or ignore records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToMany)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToMany).EnumerateEntityFieldNames()
        For Each entity As IOneToMany In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames(0)}] = {entity.PrimaryId} AND [{fieldNames(1)}] = {entity.SecondaryId} ))  ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToOne)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToOne).EnumerateEntityFieldNames()
        For Each entity As IOneToOne In entities.ToArray
            builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.PrimaryId}) ")
            builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = {entity.SecondaryId} ")
            builder.AppendLine($"WHERE [{fieldNames(0)}] = {entity.PrimaryId}; ")
            builder.AppendLine("ELSE ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or ignore records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToOne)) As String
        Dim builder As New Text.StringBuilder()
        Dim fieldNames As IEnumerable(Of String) = GetType(IOneToOne).EnumerateEntityFieldNames()
        For Each entity As IOneToOne In entities.ToArray
            builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {entity.PrimaryId}) ")
            builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId}); ")
        Next entity
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates key, name description records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function UpsertNameDescriptionRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                     ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String
        Dim builder As New Text.StringBuilder()
        For Each value As System.Enum In System.Enum.GetValues(type)
            Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
            If Not excluded.Contains(enumValue) Then
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {enumValue}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = '{System.Enum.GetName(type, value)}',{fieldNames(2)} = '{value.Description}') ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {enumValue} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}]  VALUES ({enumValue},'{System.Enum.GetName(type, value)}','{value.Description}'); ")
            End If
        Next value
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or ignore name description records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnoreNameDescriptionRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                           ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String
        Dim builder As New Text.StringBuilder()
        For Each value As System.Enum In System.Enum.GetValues(type)
            Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
            If Not excluded.Contains(enumValue) Then
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {enumValue}) ")
                builder.AppendLine($"INSERT INTO [{tableName}]  VALUES ({enumValue},'{System.Enum.GetName(type, value)}','{value.Description}'); ")
            End If
        Next value
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or replaces name description records. </summary>
    ''' <remarks>
    ''' Insert or replace failed on some tables (e.g., Measurement) due to a foreign key constraint
    ''' while not failing on other tables (e.g., Polarity and Bin). At this point (2/2020) the
    ''' difference between these tables remains unclear.
    ''' </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of inserted or replaced records. </returns>
    Public Overrides Function RepsertNameDescriptionRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                            ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String
        Dim builder As New Text.StringBuilder()
        For Each value As System.Enum In System.Enum.GetValues(type)
            Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
            If Not excluded.Contains(enumValue) Then
                builder.AppendLine($"INSERT REPLACE INTO [{tableName}] VALUES ({enumValue},'{System.Enum.GetName(type, value)}','{value.Description}'); ")
            End If
        Next value
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates key-name records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function UpsertNameRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                           ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String
        Dim builder As New Text.StringBuilder()
        For Each value As System.Enum In System.Enum.GetValues(type)
            Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
            If Not excluded.Contains(enumValue) Then
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {enumValue}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = '{System.Enum.GetName(type, value)}' ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {enumValue} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({enumValue},'{System.Enum.GetName(type, value)}'); ")
            End If
        Next value
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or ignores a name records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnoreNameRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                      ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String
        Dim builder As New Text.StringBuilder()
        For Each value As System.Enum In System.Enum.GetValues(type)
            Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
            If Not excluded.Contains(enumValue) Then
                builder.AppendLine($"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {enumValue}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({enumValue},'{System.Enum.GetName(type, value)}'); ")
            End If
        Next value
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or replaces name records. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of inserted or replaced records. </returns>
    Public Overrides Function InsertReplaceNameRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                       ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String
        Dim builder As New Text.StringBuilder()
        For Each value As System.Enum In System.Enum.GetValues(type)
            Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
            If Not excluded.Contains(enumValue) Then
                builder.AppendLine($"INSERT OR REPLACE INTO [{tableName}] VALUES ({enumValue},'{System.Enum.GetName(type, value)}'); ")
            End If
        Next value
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or updates key, name ordinal records. </summary>
    ''' <remarks> David, 5/6/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function UpsertNameOrdinalRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                 ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String
        Dim builder As New Text.StringBuilder()
        Dim ordinalValue As Integer = 0
        For Each value As System.Enum In System.Enum.GetValues(type)
            Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
            If Not excluded.Contains(enumValue) Then
                ordinalValue += 1
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {enumValue}) ")
                builder.AppendLine($"UPDATE [{tableName}] SET {fieldNames(1)} = '{System.Enum.GetName(type, value)}', {fieldNames(2)} = {ordinalValue}) ")
                builder.AppendLine($"WHERE [{fieldNames(0)}] = {enumValue} ")
                builder.AppendLine("ELSE ")
                builder.AppendLine($"INSERT INTO [{tableName}]  VALUES ({enumValue},'{System.Enum.GetName(type, value)}',{ordinalValue}); ")
            End If
        Next value
        Return builder.ToString()
    End Function

    ''' <summary> Inserts or ignore name ordinal records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of affected records. </returns>
    Public Overrides Function InsertIgnoreNameOrdinalRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                             ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String
        Dim builder As New Text.StringBuilder()
        Dim ordinalValue As Integer = 0
        For Each value As System.Enum In System.Enum.GetValues(type)
            Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
            If Not excluded.Contains(enumValue) Then
                ordinalValue += 1
                builder.AppendLine($"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames(0)}] = {enumValue}) ")
                builder.AppendLine($"INSERT INTO [{tableName}] VALUES ({enumValue},'{System.Enum.GetName(type, value)}',{ordinalValue}); ")
            End If
        Next value
        Return builder.ToString()
    End Function

End Class
