Imports System.Runtime.CompilerServices

Imports Dapper

Namespace ConnectionExtensions

    Partial Public Module Methods

        ''' <summary> Executes the command using a transaction operation. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="command">    The command. </param>
        ''' <returns> The number of affected records. </returns>
        <Extension>
        Public Function ExecuteTransaction(ByVal connection As System.Data.IDbConnection, ByVal command As String) As Integer
            Return isr.Dapper.Entity.ConnectionExtensions.Methods.ExecuteTransaction(connection, command)
        End Function

        ''' <summary> Select builder. </summary>
        ''' <remarks> David, 5/26/2020. </remarks>
        ''' <param name="connection"> The data source connection. </param>
        ''' <returns> A SqlBuilderBase. </returns>
        Private Function SelectBuilder(ByVal connection As System.Data.IDbConnection) As SqlBuilderBase
            Return SqlBuilderBase.SelectBuilder(connection)
        End Function

        ''' <summary> Creates a view. </summary>
        ''' <remarks> David, 8/11/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection">  The data source connection. </param>
        ''' <param name="viewName">    Name of the view. </param>
        ''' <param name="selectQuery"> The select query. </param>
        ''' <param name="dropFirst">   True to drop first. </param>
        ''' <returns> The number of affected lines. </returns>
        <Extension>
        Public Function CreateView(ByVal connection As System.Data.IDbConnection, ByVal viewName As String, ByVal selectQuery As String, ByVal dropFirst As Boolean) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Return connection.Execute(Methods.SelectBuilder(connection).CreateView(viewName, selectQuery, dropFirst))
        End Function

        ''' <summary> Inserts or updates <see cref="IFiveToManyNatural"/> records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The <see cref="IFiveToManyNatural"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IFiveToManyNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores <see cref="IFiveToManyNatural"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IFiveToManyNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates <see cref="IFiveToManyReal"/> records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The <see cref="IFiveToManyReal"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IFiveToManyReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores <see cref="IFiveToManyReal"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IFiveToManyReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates <see cref="IFourToMany"/> records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The <see cref="IFourToMany"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IFourToMany)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores <see cref="IFourToMany"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IFourToMany)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates <see cref="IFourToManyId"/> records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The <see cref="IFourToManyId"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IFourToManyId)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores <see cref="IFourToManyId"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IFourToManyId)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates <see cref="IFourToManyNatural"/> records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The <see cref="IFourToManyNatural"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IFourToManyNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores <see cref="IFourToManyNatural"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IFourToManyNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates <see cref="IFourToManyReal"/> records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The <see cref="IFourToManyReal"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IFourToManyReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores <see cref="IFourToManyReal"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IFourToManyReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or update records. </summary>
        ''' <remarks> David, 5/12/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of ITwoToMany)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of ITwoToMany)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of ITwoToManyLabel)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of ITwoToManyLabel)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of ITwoToManyReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of ITwoToManyReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or update records. </summary>
        ''' <remarks> David, 5/12/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IThreeToMany)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IThreeToMany)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates <see cref="IThreeToManyId"/> records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IThreeToManyId)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores <see cref="IThreeToManyId"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IThreeToManyId)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IThreeToManyLabel)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IThreeToManyLabel)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IThreeToManyReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IThreeToManyReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IExplicitKeyLabelNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IExplicitKeyLabelNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IExplicitKeyForeignLabel)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IExplicitKeyForeignLabel)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IExplicitKeyForeignNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IExplicitKeyForeignNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or update <see cref="IKeyForeign"/> records. </summary>
        ''' <remarks> David, 5/12/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The <see cref="IKeyForeign"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyForeign)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores <see cref="IKeyForeign"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The <see cref="IKeyForeign"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IKeyForeign)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates  <see cref="IKeyForeignLabelNatural"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The  <see cref="IKeyForeignLabelNatural"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyForeignLabelNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores  <see cref="IKeyForeignLabelNatural"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The  <see cref="IKeyForeignLabelNatural"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IKeyForeignLabelNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates  <see cref="IKeyForeignTime"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The  <see cref="IKeyForeignTime"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyForeignTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores  <see cref="IKeyForeignTime"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The  <see cref="IKeyForeignTime"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IKeyForeignTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates  <see cref="IKeyForeignReal"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The  <see cref="IKeyForeignReal"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyForeignReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores  <see cref="IKeyForeignReal"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The  <see cref="IKeyForeignReal"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IKeyForeignReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates  <see cref="IKeyTwoForeignReal"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The  <see cref="IKeyTwoForeignReal"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyTwoForeignReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores  <see cref="IKeyTwoForeignReal"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The  <see cref="IKeyTwoForeignReal"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IKeyTwoForeignReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates  <see cref="IKeyTwoForeignTime"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The  <see cref="IKeyTwoForeignTime"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyTwoForeignTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores  <see cref="IKeyTwoForeignTime"/> records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The  <see cref="IKeyTwoForeignTime"/> entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IKeyTwoForeignTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyLabelTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IKeyLabelTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyLabelTimezone)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IKeyLabelTimezone)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyNaturalTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IKeyNaturalTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IKeyTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyForeignLabel)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IKeyForeignLabel)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyForeignLabelTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IKeyForeignLabelTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IKeyForeignNaturalTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                    ByVal entities As IEnumerable(Of IKeyForeignNaturalTime)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of INominal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of INominal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or ignore records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IOneToManyId)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IOneToManyId)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or ignore records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IOneToManyNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IOneToManyNatural)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IOneToManyRange)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                          ByVal entities As IEnumerable(Of IOneToManyRange)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IOneToManyReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IOneToManyReal)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates records. </summary>
        ''' <remarks> David, 5/13/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IOneToManyLabel)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IOneToManyLabel)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or update records. </summary>
        ''' <remarks> David, 5/12/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IOneToMany)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IOneToMany)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or update records. </summary>
        ''' <remarks> David, 5/12/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                               ByVal entities As IEnumerable(Of IOneToOne)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).Upsert(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> inserts or ignores records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="entities">   The entities. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnore(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                     ByVal entities As IEnumerable(Of IOneToOne)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnore(tableName, entities))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates names description records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="fieldNames"> List of names of the fields. </param>
        ''' <param name="type">       The type. </param>
        ''' <param name="excluded">   The excluded. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function UpsertNameDescriptionRecords(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                                     ByVal fieldNames As IEnumerable(Of String),
                                                     ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).UpsertNameDescriptionRecords(tableName, fieldNames, type, excluded))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or ignores name description records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="fieldNames"> List of names of the fields. </param>
        ''' <param name="type">       The type. </param>
        ''' <param name="excluded">   The excluded. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnoreNameDescriptionRecords(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                                           ByVal fieldNames As IEnumerable(Of String),
                                                           ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnoreNameDescriptionRecords(tableName, fieldNames, type, excluded))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates name records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="fieldNames"> List of names of the fields. </param>
        ''' <param name="type">       The type. </param>
        ''' <param name="excluded">   The excluded. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function UpsertNameRecords(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                          ByVal fieldNames As IEnumerable(Of String),
                                          ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).UpsertNameRecords(tableName, fieldNames, type, excluded))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or ignores a name records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="fieldNames"> List of names of the fields. </param>
        ''' <param name="type">       The type. </param>
        ''' <param name="excluded">   The excluded. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnoreNameRecords(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                                ByVal fieldNames As IEnumerable(Of String),
                                                ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnoreNameRecords(tableName, fieldNames, type, excluded))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or updates name ordinal records. </summary>
        ''' <remarks> David, 5/6/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="fieldNames"> List of names of the fields. </param>
        ''' <param name="type">       The type. </param>
        ''' <param name="excluded">   The excluded. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function UpsertNameOrdinalRecords(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                                 ByVal fieldNames As IEnumerable(Of String),
                                                 ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).UpsertNameOrdinalRecords(tableName, fieldNames, type, excluded))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Inserts or ignore name ordinal records. </summary>
        ''' <remarks> David, 5/14/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <param name="fieldNames"> List of names of the fields. </param>
        ''' <param name="type">       The type. </param>
        ''' <param name="excluded">   The excluded. </param>
        ''' <returns>
        ''' The number of affected records or the total number of records in the table if none were
        ''' affected.
        ''' </returns>
        <Extension>
        Public Function InsertIgnoreNameOrdinalRecords(ByVal connection As System.Data.IDbConnection, ByVal tableName As String,
                                                       ByVal fieldNames As IEnumerable(Of String),
                                                       ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim count As Integer = connection.Execute(Methods.SelectBuilder(connection).InsertIgnoreNameOrdinalRecords(tableName, fieldNames, type, excluded))
            If count = 0 Then count = Methods.CountEntities(connection, tableName)
            Return count
        End Function

        ''' <summary> Count entities. </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <returns> The total number of entities. </returns>
        <Extension>
        Public Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal tableName As String) As Integer
            Return isr.Dapper.Entity.ConnectionExtensions.Methods.CountEntities(connection, tableName)
        End Function

        ''' <summary> Queries if a given table exists. </summary>
        ''' <remarks> David, 5/16/2020. </remarks>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        <Extension>
        Public Function TableExists(connection As System.Data.IDbConnection, tableName As String) As Boolean
            Return isr.Dapper.Entity.ProviderBase.SelectProvider(connection).TableExists(connection, tableName)
        End Function

        ''' <summary> Queries if a given index exists. </summary>
        ''' <remarks> David, 5/16/2020. </remarks>
        ''' <param name="connection"> The data source connection. </param>
        ''' <param name="indexName">  Name of the index. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        <Extension>
        Public Function IndexExists(connection As System.Data.IDbConnection, indexName As String) As Boolean
            Return isr.Dapper.Entity.ProviderBase.SelectProvider(connection).IndexExists(connection, indexName)
        End Function

    End Module

End Namespace

