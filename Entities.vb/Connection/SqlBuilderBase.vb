Imports Dapper

''' <summary> A SQL builder base. </summary>
''' <remarks>
''' David, 5/26/2020. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public MustInherit Class SqlBuilderBase

    ''' <summary> Selects a builder. </summary>
    ''' <remarks> David, 5/26/2020. </remarks>
    ''' <param name="connection"> The data source connection. </param>
    ''' <returns> A SqlBuilderBase. </returns>
    Public Shared Function SelectBuilder(ByVal connection As System.Data.IDbConnection) As SqlBuilderBase
        Dim transactedConnection As TransactedConnection = TryCast(connection, TransactedConnection)
        If transactedConnection Is Nothing Then
            If TryCast(connection, System.Data.SQLite.SQLiteConnection) IsNot Nothing Then
                Return SqliteSqlBuilder.Get
            Else
                ' System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
                Return SqlServerSqlBuilder.Get
            End If
        Else
            Return SqlBuilderBase.SelectBuilder(transactedConnection.Connection)
        End If
    End Function

    ''' <summary> Converts a value to an universal time format. </summary>
    ''' <remarks> David, 5/7/2020. </remarks>
    ''' <param name="universalTimestamp"> The Universal (UTC) timestamp. </param>
    ''' <returns>
    ''' Time value in the <see cref="isr.Dapper.Entity.ProviderBase.DefaultDateTimeFormat"/> format.
    ''' </returns>
    Public Shared Function ToTimestampFormat(ByVal universalTimestamp As DateTime) As String
        Return isr.Dapper.Entity.ProviderBase.ToDefaultTimestampFormat(universalTimestamp)
    End Function

    ''' <summary> Build a create view query. </summary>
    ''' <remarks> David, 8/11/2020. </remarks>
    ''' <param name="viewName">    Name of the view. </param>
    ''' <param name="selectQuery"> The select query. </param>
    ''' <param name="dropFirst">   True to drop first. </param>
    ''' <returns> The built query. </returns>
    Public MustOverride Function CreateView(ByVal viewName As String, ByVal selectQuery As String, ByVal dropFirst As Boolean) As String

    ''' <summary> Inserts or updates <see cref="IFiveToManyNatural"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFiveToManyNatural"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFiveToManyNatural)) As String

    ''' <summary> inserts or ignores <see cref="IFiveToManyNatural"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFiveToManyNatural"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFiveToManyNatural)) As String

    ''' <summary> Inserts or updates <see cref="IFiveToManyReal"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFiveToManyReal"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFiveToManyReal)) As String

    ''' <summary> inserts or ignores <see cref="IFiveToManyReal"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFiveToManyReal"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFiveToManyReal)) As String

    ''' <summary> Inserts or updates <see cref="IFourToMany"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFourToMany"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToMany)) As String

    ''' <summary> inserts or ignores <see cref="IFourToMany"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFourToMany"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToMany)) As String

    ''' <summary> Inserts or updates <see cref="IFourToManyId"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFourToManyId"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyId)) As String

    ''' <summary> inserts or ignores <see cref="IFourToManyId"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFourToManyId"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyId)) As String

    ''' <summary> Inserts or updates <see cref="IFourToManyNatural"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFourToManyNatural"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyNatural)) As String

    ''' <summary> inserts or ignores <see cref="IFourToManyNatural"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFourToManyNatural"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyNatural)) As String

    ''' <summary> Inserts or updates <see cref="IFourToManyReal"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFourToManyReal"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyReal)) As String

    ''' <summary> inserts or ignores <see cref="IFourToManyReal"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IFourToManyReal"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IFourToManyReal)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToMany)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToMany)) As String

    ''' <summary> Inserts or updates <see cref="ITwoToManyLabel"/> records. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="ITwoToManyLabel"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToManyLabel)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToManyLabel)) As String

    ''' <summary> Inserts or updates <see cref="ITwoToManyReal"/> records. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="ITwoToManyReal"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToManyReal)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of ITwoToManyReal)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToMany)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToMany)) As String

    ''' <summary> Inserts or updates <see cref="IThreeToManyId"/> records. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IThreeToManyId"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyId)) As String

    ''' <summary> inserts or ignores <see cref="IThreeToManyId"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IThreeToManyReal"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyId)) As String

    ''' <summary> Inserts or updates <see cref="IThreeToManyLabel"/> records. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IThreeToManyLabel"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyLabel)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyLabel)) As String

    ''' <summary> Inserts or updates <see cref="IThreeToManyReal"/> records. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IThreeToManyReal"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyReal)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IThreeToManyReal)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyLabelNatural)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyLabelNatural)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyForeignLabel)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyForeignLabel)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyForeignNatural)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IExplicitKeyForeignNatural)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeign)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeign)) As String

    ''' <summary> Inserts or updates <see cref="IKeyForeignLabelNatural"/> records. </summary>
    ''' <remarks> David, 5/14/2020. Present implementation assumes a unique index. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyForeignLabelNatural"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabelNatural)) As String

    ''' <summary> inserts or ignores <see cref="IKeyForeignLabelNatural"/> records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The <see cref="IKeyForeignLabelNatural"/> entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabelNatural)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignTime)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignTime)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignReal)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignReal)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTwoForeignReal)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTwoForeignReal)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTwoForeignTime)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTwoForeignTime)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyLabelTime)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyLabelTime)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyLabelTimezone)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyLabelTimezone)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyNaturalTime)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyNaturalTime)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTime)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyTime)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabel)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabel)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabelTime)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignLabelTime)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignNaturalTime)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IKeyForeignNaturalTime)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of INominal)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of INominal)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyId)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyId)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyNatural)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyNatural)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyRange)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyRange)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyReal)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyReal)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/13/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyLabel)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToManyLabel)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToMany)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToMany)) As String

    ''' <summary> Inserts or updates records. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function Upsert(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToOne)) As String

    ''' <summary> inserts or ignores records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName"> Name of the table. </param>
    ''' <param name="entities">  The entities. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnore(ByVal tableName As String, ByVal entities As IEnumerable(Of IOneToOne)) As String

    ''' <summary> Inserts or updates a name description records. </summary>
    ''' <remarks> David, 5/11/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function UpsertNameDescriptionRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                              ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String

    ''' <summary> Inserts or ignores name description records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnoreNameDescriptionRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                                    ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String

    ''' <summary> Inserts or replaces name description records. </summary>
    ''' <remarks>
    ''' Insert or replace failed on some tables (e.g., Measurement) due to a foreign key constraint
    ''' while not failing on other tables (e.g., Polarity and Bin). At this point (2/2020) the
    ''' difference between these tables remains unclear.
    ''' </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of inserted or replaced records. </returns>
    Public MustOverride Function RepsertNameDescriptionRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                               ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String

    ''' <summary> Inserts or updates name ordinal records. </summary>
    ''' <remarks> David, 5/6/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function UpsertNameOrdinalRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                          ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String

    ''' <summary> Inserts or ignores a name ordinal records. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnoreNameOrdinalRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                                ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String

    ''' <summary> Inserts or updates name records. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of inserted or updated records. </returns>
    Public MustOverride Function UpsertNameRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String), ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String

    ''' <summary> Inserts or ignores a name records. </summary>
    ''' <remarks> David, 5/16/2020. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of affected records. </returns>
    Public MustOverride Function InsertIgnoreNameRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                         ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String

    ''' <summary> Inserts or replaces name records. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <param name="fieldNames"> List of names of the fields. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of inserted or replaced records. </returns>
    Public MustOverride Function InsertReplaceNameRecords(ByVal tableName As String, ByVal fieldNames As IEnumerable(Of String),
                                                          ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As String

End Class
