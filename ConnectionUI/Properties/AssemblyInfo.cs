﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Dapper.ConnectionUI.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Dapper.ConnectionUI.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Dapper.ConnectionUI.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
