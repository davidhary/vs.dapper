# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2.2.7827] - 2021-06-05
### Entities
* Add session UUT and Session meter settings. 

## [2.2.7824] - 2021-06-03
### Entities
* Rename multimeter to meter. Nom Readings to Reading and Meta Reading. 

## [2.2.7607] - 2020-10-29
Converted to C#

## [2.2.7345] - 2019-02-10
Uses readme.md version (2.0.204) as the current version.
 

## [2.2.7209] - 2019-09-27
Uses NUGET version (2.2.203) as the current version.

## [1.60.7175] - 2019-08-24
Updated to version 1.60

## [1.50.6792] - 2018-08-06
Created

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
