using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Entity.Tests
{

    /// <summary> An auditor: Owner of the actual test code. </summary>
    /// <remarks> David, 2020-03-26. </remarks>
    public sealed partial class Auditor
    {

        #region " PROVIDER TESTS "

        /// <summary> Asserts connection exists. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="provider"> The provider. </param>
        internal static void AssertConnectionExists( ProviderBase provider )
        {
            using var connection = provider.GetConnection();
            Assert.IsTrue( provider.ConnectionExists(), $"connection {connection.ConnectionString} should exist" );
        }

        /// <summary> Asserts tables exist. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="provider">   The provider. </param>
        /// <param name="tableNames"> List of names of the tables. </param>
        internal static void AssertTablesExist( ProviderBase provider, IEnumerable<string> tableNames )
        {
            using var connection = provider.GetConnection();
            foreach ( string name in tableNames )
                Assert.IsTrue( provider.TableExists( connection, name ), $"table {name} should exist" );
        }

        #endregion

    }
}
