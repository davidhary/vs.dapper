Imports Dapper.Contrib.Extensions

Imports isr.Dapper.Entity
Imports isr.Dapper.Entity.ConnectionExtensions

''' <summary> An auditor: Owner of the actual test code. </summary>
''' <remarks> David, 3/26/2020. </remarks>
Partial Public NotInheritable Class Auditor

    #Region " PROVIDER TESTS "

    ''' <summary> Asserts connection exists. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="provider"> The provider. </param>
    Friend Shared Sub AssertConnectionExists(ByVal provider As ProviderBase)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Assert.IsTrue(provider.ConnectionExists(), $"connection {connection.ConnectionString} should exist")
        End Using
    End Sub

    ''' <summary> Asserts tables exist. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="provider">   The provider. </param>
    ''' <param name="tableNames"> List of names of the tables. </param>
    Friend Shared Sub AssertTablesExist(ByVal provider As ProviderBase, ByVal tableNames As IEnumerable(Of String))
        Using connection As System.Data.IDbConnection = provider.GetConnection
            For Each name As String In tableNames
                Assert.IsTrue(provider.TableExists(connection, name), $"table {name} should exist")
            Next
        End Using
    End Sub

    #End Region

End Class
