Imports System.ComponentModel

Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-10-02. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Dapper.Entity.My.ProjectTraceEventId.DapperConnectionUI

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Dapper Connection UI Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Dapper Connection UI Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Dapper.Connection.UI"

        ''' <summary> The test assembly strong name. </summary>
        Public Const TestAssemblyStrongName As String = "isr.Dapper.Tests,PublicKey=" & SolutionInfo.PublicKey

    End Class

End Namespace
