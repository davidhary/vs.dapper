Imports Dapper
Imports isr.Dapper
Imports isr.Dapper.Entity
Imports isr.Dapper.ConnectionUI.ExceptionExtensions
Imports System.Runtime.CompilerServices

Namespace SqlProviderExtensions

    Public Module Methods

        ''' <summary> Establish master provider. </summary>
        ''' <remarks> David, 4/1/2020. </remarks>
        ''' <param name="provider">              The provider. </param>
        ''' <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
        ''' <returns>
        ''' The (Success As Boolean, Details As String, MasterProvider As SqlServerProvider)
        ''' </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
        <Extension>
        Public Function EstablishMasterProvider(ByVal provider As SqlServerProvider,
                                                ByVal includeInitialCatalog As Boolean) As (Success As Boolean, Details As String, MasterProvider As SqlServerProvider)
            Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
            Dim activity As String = $"invoking {NameOf(SqlServerProvider.BuildMasterConnectionString)}"
            Dim masterConnectionString As String
            Dim masterProvider As SqlServerProvider = Nothing
            ' check if user has access to the credentials tables
            Try
                masterConnectionString = provider.BuildMasterConnectionString(includeInitialCatalog)
                activity = $"invoking {NameOf(SqlServerProvider.Create)}({masterConnectionString})"
                masterProvider = SqlServerProvider.Create(masterConnectionString)
                activity = $"1st invoking {NameOf(SqlServerProvider.ServerPrincipalExists)} for {provider.Credentials.UserId}"
                result = If(SqlServerProvider.ServerPrincipalExists(masterProvider.GetConnection, provider.Credentials.UserId),
                    (True, String.Empty),
                    (False, $"{activity} for '{provider.Credentials.UserId}' returned false"))
            Catch ex As System.Data.SqlClient.SqlException When ex.ErrorCode = -2146232060
                result = (False, $"Exception {activity}'. {ex.ToFullBlownString}")
            Catch ex As Exception
                result = (False, $"Exception {activity}'. {ex.ToFullBlownString}")
            End Try
            If Not result.Success Then
                activity = $"invoking {NameOf(Global.Microsoft.Data.ConnectionUI.Dialog.My.Dialogs.ShowSqlDataSourceConnectionDialog)}({provider.ConnectionString})"
                Try
                    ' get a new connection using SA credentials
                    Dim r As (Success As Boolean, ConnectionString As String)
                    r = Global.Microsoft.Data.ConnectionUI.Dialog.My.Dialogs.ShowSqlDataSourceConnectionDialog(provider.ConnectionString)
                    If r.Success Then
                        activity = $"invoking {NameOf(SqlServerProvider.Create)}"
                        masterProvider = SqlServerProvider.Create(r.ConnectionString)
                        activity = $"2nd invoking {NameOf(SqlServerProvider.ServerPrincipalExists)}({provider.Credentials.UserId})"
                        result = If(SqlServerProvider.ServerPrincipalExists(masterProvider.GetConnection, provider.Credentials.UserId),
                                (True, String.Empty),
                                (False, $"{activity} for '{provider.Credentials.UserId}' returned false"))
                    Else
                        masterProvider = Nothing
                    End If
                Catch ex As Exception
                    result = (False, $"Exception {activity}'. {ex.ToFullBlownString}")
                End Try
            End If
            Return (result.Success, result.Details, masterProvider)
        End Function

        ''' <summary> Establishes login credentials. </summary>
        ''' <remarks> David, 3/21/2020. </remarks>
        ''' <param name="provider">              The provider. </param>
        ''' <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
        ''' <returns> The (Success As Boolean, Details As String) </returns>
        <Extension>
        Public Function EstablishLoginCredentials(ByVal provider As SqlServerProvider, ByVal includeInitialCatalog As Boolean) As (Success As Boolean, Details As String)
            Dim result As (Success As Boolean, Details As String)
            Dim r As (Success As Boolean, Details As String, MasterProvider As SqlServerProvider)
            r = Methods.EstablishMasterProvider(provider, includeInitialCatalog)
            result = (r.Success, r.Details)
            If result.Success Then
                result = SqlServerProvider.EstablishLoginCredentials(r.MasterProvider, provider.DatabaseName,
                                                                     provider.Credentials.UserId, provider.Credentials.Password)
            End If
            Return result
        End Function

        ''' <summary> Query if database exists. </summary>
        ''' <remarks> David, 3/24/2020. </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown when failed to establish master
        '''                                              credentials. </exception>
        ''' <param name="provider"> The provider. </param>
        ''' <returns> True if database exists, false if not. </returns>
        <Extension>
        Public Function IsDatabaseExists(ByVal provider As SqlServerProvider) As Boolean
            Dim r As (Success As Boolean, Details As String, MasterProvider As SqlServerProvider)
            r = Methods.EstablishMasterProvider(provider, False)
            If r.Success Then
                Return provider.DatabaseExists(r.MasterProvider.GetConnection, provider.DatabaseName)
            Else
                Throw New InvalidOperationException($"Failed establishing master provider credentials;. {r.Details}")
            End If
        End Function

        ''' <summary>
        ''' Establish login credentials for this connection and determine if connection can open.
        ''' </summary>
        ''' <remarks> David, 3/21/2020. </remarks>
        ''' <param name="provider"> The provider. </param>
        ''' <returns> The (Success As Boolean, Details As String) </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
        <Extension>
        Public Function EstablishConnectionCredentials(ByVal provider As SqlServerProvider) As (Success As Boolean, Details As String)
            Dim connString As String = provider.ConnectionString
            Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
            Dim establishLoginResult As (Success As Boolean, Details As String) = (False, String.Empty)
            Try
                result = provider.GetConnection.CanOpen()
            Catch ex As System.Data.SqlClient.SqlException When ex.ErrorCode = -2146232060
                ' checks if we have a user login issue
                establishLoginResult = Methods.EstablishLoginCredentials(provider, False)
            Catch ex As Exception
                ' checks if we have a user login issue
                establishLoginResult = Methods.EstablishLoginCredentials(provider, False)
            End Try
            If Not result.Success AndAlso establishLoginResult.Success Then
                result = provider.GetConnection.CanOpen()
            End If
            Return result
        End Function

    End Module

End Namespace

