using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
[assembly: AssemblyCompany( "Stack Exchange" )]
[assembly: AssemblyCopyright( "(c) 2017 Stack Exchange, Inc." )]
[assembly: AssemblyTrademark( "Licensed under The MIT License." )]
[assembly: AssemblyCulture( "" )]
[assembly: ComVisible( false )]
[assembly: NeutralResourcesLanguage( "en-US", UltimateResourceFallbackLocation.MainAssembly )]
