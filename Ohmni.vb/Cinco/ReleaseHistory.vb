Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions
Imports isr.Dapper.Ohmni.ExceptionExtensions

''' <summary>
''' Interface for the ReleaseHistory nub and entity. Includes the fields as kept in the data
''' table. Allows tracking of property changes.
''' </summary>
''' <remarks>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value. <para>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface ICincoReleaseHistory

    ''' <summary> Gets the version number. </summary>
    ''' <value> The version number. </value>
    <ExplicitKey>
    Property VersionNumber As String

    ''' <summary> Gets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time. <para>
    ''' Computed attribute ignores the property on either insert or update.  
    ''' https://dapper-tutorial.net/knowledge-base/57673107/what-s-the-difference-between--computed--and--write-false---attributes-
    ''' The <see cref="CincoReleaseHistoryEntity.UpdateLocalTimeStamp(Data.IDbConnection, Date)"/> is
    ''' provided should the timestamp needs to be updated.         </para> <para>
    ''' Note the SQLite stores time in the database in the time zone of the server. For example, if
    ''' the server is located in LA, with a time zone of 7 hours relative to GMT (daylight saving
    ''' time) storing 11:00:00AM using universal time value of 18:00:00Z, will store the time on the
    ''' LA server as 11:00:00. To resolve these issues for both SQL Server the SQLite, time is
    ''' converted to UTC and then to a date time format with no time zone specification (no Z).
    ''' </para>
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Property Timestamp As DateTime

    ''' <summary> Gets the is active. </summary>
    ''' <value> The is active. </value>
    Property IsActive As Boolean

    ''' <summary> Gets the description. </summary>
    ''' <value> The description. </value>
    Property Description As String

End Interface

''' <summary> A release history builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class CincoReleaseHistoryBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(CincoReleaseHistoryBuilder._TableName) Then
                CincoReleaseHistoryBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(CincoReleaseHistoryNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return CincoReleaseHistoryBuilder._TableName
        End Get
    End Property

    ''' <summary> Upsert revision. </summary>
    ''' <remarks> David, 3/25/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="recordData"> Comma separated values for the revision and the description. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function UpsertRevision(ByVal connection As System.Data.IDbConnection, ByVal recordData As String) As Boolean
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        If String.IsNullOrEmpty(recordData) Then Throw New ArgumentNullException(NameOf(recordData))
        Dim values As String() = recordData.Split()
        If values.Count = 1 Then values = New String() {values(0), "Unspecified release info"}
        Dim releaseHistory As ICincoReleaseHistory = New CincoReleaseHistoryNub With {.VersionNumber = values(0), .Description = values(1), .IsActive = True}
        Dim entity As New CincoReleaseHistoryEntity
        Return entity.Upsert(connection, releaseHistory)
    End Function

    ''' <summary> Clears the active bit of all records. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    Public Shared Sub ClearActiveBit(ByVal connection As System.Data.IDbConnection)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"UPDATE [{CincoReleaseHistoryBuilder.TableName}] SET [{NameOf(CincoReleaseHistoryNub.IsActive)}] = 0; ")
        connection.Execute(queryBuilder.ToString().Clean())
    End Sub

    ''' <summary> Gets or sets the date time format. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="NotImplementedException">   Thrown when the requested operation is
    '''                                              unimplemented. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The date time format. </value>
    Public Shared Property DateTimeFormat As String = "yyyy-MM-dd HH:mm:ss.fff"

    ''' <summary> Converts a value to a timestamp format. </summary>
    ''' <remarks> David, 6/22/2020. </remarks>
    ''' <param name="value"> The value Date/Time. </param>
    ''' <returns> Time value in the <see cref="DateTimeFormat"/> format. </returns>
    Public Shared Function ToTimestampFormat(ByVal value As DateTime) As String
        Return value.ToString(CincoReleaseHistoryBuilder.DateTimeFormat)
    End Function

    ''' <summary> Updates the timestamp. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="key">                The key. </param>
    ''' <param name="universalTimestamp"> The universal (UTC) timestamp. </param>
    Public Shared Sub UpdateTimestamp(ByVal connection As System.Data.IDbConnection, ByVal key As String, ByVal universalTimestamp As Date)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"UPDATE [{CincoReleaseHistoryBuilder.TableName}] ")
        queryBuilder.Append($"SET [{NameOf(CincoReleaseHistoryNub.Timestamp)}] = '{CincoReleaseHistoryBuilder.ToTimestampFormat(universalTimestamp)}' ")
        queryBuilder.Append($"WHERE [{NameOf(CincoReleaseHistoryNub.VersionNumber)}] = '{key}'; ")
        connection.Execute(queryBuilder.ToString().Clean())
    End Sub

    ''' <summary> Updates the release history. </summary>
    ''' <remarks> David, 3/25/2020. </remarks>
    ''' <param name="connection">     The connection. </param>
    ''' <param name="revisionRecord"> The revision record. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function UpdateReleaseHistory(ByVal connection As System.Data.IDbConnection, ByVal revisionRecord As String) As Boolean
        Dim values As String() = revisionRecord.Split(","c)
        Return 0 <= CincoReleaseHistoryBuilder.UpdateReleaseHistory(connection, values(0), DateTime.Now, values(1))
    End Function

    ''' <summary> Updates the release history. </summary>
    ''' <remarks> David, 5/2/2020. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="versionNumber">      The version number. </param>
    ''' <param name="universalTimestamp"> The universal (UTC) timestamp. </param>
    ''' <param name="description">        The description. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function UpdateReleaseHistory(ByVal connection As System.Data.IDbConnection, ByVal versionNumber As String,
                                                ByVal universalTimestamp As DateTime, ByVal description As String) As Integer
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CincoReleaseHistoryBuilder.UpdateReleaseHistory(sql, versionNumber, universalTimestamp, description)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CincoReleaseHistoryBuilder.UpdateReleaseHistory(sqlite, versionNumber, universalTimestamp, description)
        Return -1
    End Function

    ''' <summary> Updates the release history. </summary>
    ''' <remarks> David, 5/2/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="versionNumber">      The version number. </param>
    ''' <param name="universalTimestamp"> The universal (UTC) timestamp. </param>
    ''' <param name="description">        The description. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function UpdateReleaseHistory(ByVal connection As System.Data.SQLite.SQLiteConnection, ByVal versionNumber As String,
                                                ByVal universalTimestamp As DateTime, ByVal description As String) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"UPDATE [{CincoReleaseHistoryBuilder.TableName}] SET [{NameOf(CincoReleaseHistoryNub.IsActive)}] = 0; ")
        Dim dateTimeValue As String = CincoReleaseHistoryBuilder.ToTimestampFormat(universalTimestamp)
        queryBuilder.Append($"INSERT OR IGNORE INTO [{CincoReleaseHistoryBuilder.TableName}] VALUES ('{versionNumber}','{dateTimeValue}','1','{description}'); ")
        Return connection.ExecuteTransaction(queryBuilder.ToString)
    End Function

    ''' <summary> Updates the release history. </summary>
    ''' <remarks> David, 5/2/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="versionNumber">      The version number. </param>
    ''' <param name="universalTimestamp"> The universal (UTC) timestamp. </param>
    ''' <param name="description">        The description. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function UpdateReleaseHistory(ByVal connection As System.Data.SqlClient.SqlConnection, ByVal versionNumber As String,
                                                ByVal universalTimestamp As DateTime, ByVal description As String) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"UPDATE [{CincoReleaseHistoryBuilder.TableName}] SET [{NameOf(CincoReleaseHistoryNub.IsActive)}] = 0;
begin
    declare @rev as nvarchar(max) = '{versionNumber}'
    IF NOT EXISTS (SELECT * FROM [{CincoReleaseHistoryBuilder.TableName}] WHERE [{NameOf(CincoReleaseHistoryNub.VersionNumber)}]=@rev)
        begin
	        INSERT INTO [{CincoReleaseHistoryBuilder.TableName}] ([{NameOf(CincoReleaseHistoryNub.VersionNumber)}],[{NameOf(CincoReleaseHistoryNub.Timestamp)}],[{NameOf(CincoReleaseHistoryNub.IsActive)}],[{NameOf(CincoReleaseHistoryNub.Description)}])
		        VALUES (@rev,'{CincoReleaseHistoryBuilder.ToTimestampFormat(universalTimestamp)}',1,'{description}')
	    end
    IF EXISTS (SELECT * FROM [{CincoReleaseHistoryBuilder.TableName}] WHERE [{NameOf(CincoReleaseHistoryNub.VersionNumber)}]=@rev)
        begin
	        UPDATE [dbo].[{CincoReleaseHistoryBuilder.TableName}] SET [{NameOf(CincoReleaseHistoryNub.IsActive)}] = 1 WHERE [{NameOf(CincoReleaseHistoryNub.VersionNumber)}] = @rev
        end 
end;")
        Return connection.ExecuteTransaction(queryBuilder.ToString)
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String

        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)

        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)

        Return String.Empty

    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{CincoReleaseHistoryBuilder.TableName}] (
	[{NameOf(CincoReleaseHistoryNub.VersionNumber)}] nvarchar(16) NOT NULL PRIMARY KEY, 
	[{NameOf(CincoReleaseHistoryNub.Timestamp)}] datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP), 
	[{NameOf(CincoReleaseHistoryNub.IsActive)}] bit NOT NULL DEFAULT True, 
	[{NameOf(CincoReleaseHistoryNub.Description)}] nvarchar(250) NOT NULL); 
CREATE UNIQUE INDEX [UQ_{CincoReleaseHistoryBuilder.TableName}_{NameOf(CincoReleaseHistoryNub.VersionNumber)}] ON [{CincoReleaseHistoryBuilder.TableName}] ([{NameOf(CincoReleaseHistoryNub.VersionNumber)}]); 
CREATE UNIQUE INDEX [UQ_{CincoReleaseHistoryBuilder.TableName}_{NameOf(CincoReleaseHistoryNub.Timestamp)}] ON [{CincoReleaseHistoryBuilder.TableName}] ([{NameOf(CincoReleaseHistoryNub.Timestamp)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CincoReleaseHistoryBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{CincoReleaseHistoryBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{CincoReleaseHistoryBuilder.TableName}] (
	[{NameOf(CincoReleaseHistoryNub.VersionNumber)}] nvarchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL PRIMARY KEY CLUSTERED, 
	[{NameOf(CincoReleaseHistoryNub.Timestamp)}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()), 
	[{NameOf(CincoReleaseHistoryNub.IsActive)}] bit NOT NULL DEFAULT ((1)), 
	[{NameOf(CincoReleaseHistoryNub.Description)}] nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]; 

CREATE UNIQUE NONCLUSTERED INDEX [UQ_{CincoReleaseHistoryBuilder.TableName}_{NameOf(CincoReleaseHistoryNub.VersionNumber)}]
	ON [dbo].[{CincoReleaseHistoryBuilder.TableName}] ([{NameOf(CincoReleaseHistoryNub.VersionNumber)}])
	WITH (PAD_INDEX=OFF
	,STATISTICS_NORECOMPUTE=OFF
	,IGNORE_DUP_KEY=OFF
	,ALLOW_ROW_LOCKS=ON
	,ALLOW_PAGE_LOCKS=ON) ON [PRIMARY];

CREATE UNIQUE NONCLUSTERED INDEX [UQ_{CincoReleaseHistoryBuilder.TableName}_{NameOf(CincoReleaseHistoryNub.Timestamp)}]
	ON [dbo].[{CincoReleaseHistoryBuilder.TableName}] ([{NameOf(CincoReleaseHistoryNub.Timestamp)}])
	WITH (PAD_INDEX=OFF
	,STATISTICS_NORECOMPUTE=OFF
	,IGNORE_DUP_KEY=OFF
	,ALLOW_ROW_LOCKS=ON
	,ALLOW_PAGE_LOCKS=ON) ON [PRIMARY]
END; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CincoReleaseHistoryBuilder.TableName
    End Function

End Class

''' <summary>
''' Implements the ReleaseHistory table <see cref="ICincoReleaseHistory">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2018 </para>
''' </remarks>
<Table("ReleaseHistory")>
Public Class CincoReleaseHistoryNub
    Inherits EntityNubBase(Of ICincoReleaseHistory)
    Implements ICincoReleaseHistory

    #Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICincoReleaseHistory
        Return New CincoReleaseHistoryNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICincoReleaseHistory
        Dim destination As ICincoReleaseHistory = Me.CreateNew
        CincoReleaseHistoryNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICincoReleaseHistory)
        CincoReleaseHistoryNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As ICincoReleaseHistory, ByVal destination As ICincoReleaseHistory)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.Description = source.Description
        destination.IsActive = source.IsActive
        destination.VersionNumber = source.VersionNumber
        destination.Timestamp = source.Timestamp
    End Sub

    #End Region

    #Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, ICincoReleaseHistory))
        Throw New NotImplementedException()
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As ICincoReleaseHistory) As Boolean
        Return other IsNot Nothing AndAlso CincoReleaseHistoryNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As ICincoReleaseHistory, ByVal right As ICincoReleaseHistory) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso String.Equals(left.VersionNumber, right.VersionNumber)
            result = result AndAlso DateTime.Equals(left.Timestamp, right.Timestamp)
            result = result AndAlso String.Equals(left.Description, right.Description)
            result = result AndAlso Boolean.Equals(left.IsActive, right.IsActive)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.VersionNumber.GetHashCode() Xor Me.Timestamp.GetHashCode() Xor Me.Description.GetHashCode() Xor Me.IsActive.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the version number. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The version number. </value>
    <ExplicitKey>
    Public Property VersionNumber As String Implements ICincoReleaseHistory.VersionNumber

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time. <para>
    ''' Computed attribute ignores the property on either insert or update.  
    ''' https://dapper-tutorial.net/knowledge-base/57673107/what-s-the-difference-between--computed--and--write-false---attributes-
    ''' The <see cref="CincoReleaseHistoryEntity.UpdateLocalTimeStamp(Data.IDbConnection, Date)"/> is
    ''' provided should the timestamp needs to be updated.         </para>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Public Property Timestamp As DateTime Implements ICincoReleaseHistory.Timestamp

    ''' <summary> Gets or sets the is active. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The is active. </value>
    Public Property IsActive As Boolean Implements ICincoReleaseHistory.IsActive

    ''' <summary> Gets or sets the description. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The description. </value>
    Public Property Description As String Implements ICincoReleaseHistory.Description

    #End Region

End Class

''' <summary>
''' The Release History Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2018 </para>
''' </remarks>
Public Class CincoReleaseHistoryEntity
    Inherits EntityBase(Of ICincoReleaseHistory, CincoReleaseHistoryNub)
    Implements ICincoReleaseHistory

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New CincoReleaseHistoryNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As ICincoReleaseHistory)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ICincoReleaseHistory, ByVal store As ICincoReleaseHistory)
        MyBase.New(New CincoReleaseHistoryNub, cache, store)
        Me.UsingNativeTracking = String.Equals(CincoReleaseHistoryBuilder.TableName, NameOf(ICincoReleaseHistory).TrimStart("I"c))
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICincoReleaseHistory
        Return New CincoReleaseHistoryNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICincoReleaseHistory
        Dim destination As ICincoReleaseHistory = Me.CreateNew
        CincoReleaseHistoryNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICincoReleaseHistory)
        CincoReleaseHistoryNub.Copy(value, Me)
    End Sub

    #End Region

    #Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ICincoReleaseHistory)
        ' first make the copy to notify of any property change.
        CincoReleaseHistoryNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

    #End Region

    #Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Release History table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As String) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of ICincoReleaseHistory)(key), connection.Get(Of CincoReleaseHistoryNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.VersionNumber)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.VersionNumber)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="versionNumber"> The version number. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal versionNumber As String) As Boolean
        Return Me.FetchUsingKey(connection, versionNumber)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ICincoReleaseHistory) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If CincoReleaseHistoryEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingKey(connection, entity.VersionNumber) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As String) As Boolean
        Return connection.Delete(Of CincoReleaseHistoryNub)(New CincoReleaseHistoryNub With {.VersionNumber = key})
    End Function

    ''' <summary> Updates the time stamp. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="timestamp">  The Local timestamp. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function UpdateLocalTimeStamp(ByVal connection As System.Data.IDbConnection, ByVal timestamp As Date) As Boolean
        CincoReleaseHistoryBuilder.UpdateTimestamp(connection, Me.VersionNumber, timestamp.ToUniversalTime)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates the time stamp. </summary>
    ''' <remarks> David, 6/22/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="timestamp">  The UTC timestamp. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function UpdateTimeStamp(ByVal connection As System.Data.IDbConnection, ByVal timestamp As Date) As Boolean
        CincoReleaseHistoryBuilder.UpdateTimestamp(connection, Me.VersionNumber, timestamp)
        Return Me.FetchUsingKey(connection)
    End Function

    #End Region

    #Region " ENTITIES "

    ''' <summary> Gets or sets the Attribute Type entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The Attribute Type entities. </value>
    Public ReadOnly Property ReleaseHistories As IEnumerable(Of CincoReleaseHistoryEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of CincoReleaseHistoryEntity)
        Return If(usingNativeTracking, CincoReleaseHistoryEntity.Populate(connection.GetAll(Of ICincoReleaseHistory)), CincoReleaseHistoryEntity.Populate(connection.GetAll(Of CincoReleaseHistoryNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._ReleaseHistories = CincoReleaseHistoryEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(CincoReleaseHistoryEntity.ReleaseHistories))
        Return If(Me.ReleaseHistories?.Any, Me.ReleaseHistories.Count, 0)
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The entity nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of CincoReleaseHistoryNub)) As IEnumerable(Of CincoReleaseHistoryEntity)
        Dim l As New List(Of CincoReleaseHistoryEntity)
        If nubs?.Any Then
            For Each nub As CincoReleaseHistoryNub In nubs
                l.Add(New CincoReleaseHistoryEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The entity interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of ICincoReleaseHistory)) As IEnumerable(Of CincoReleaseHistoryEntity)
        Dim l As New List(Of CincoReleaseHistoryEntity)
        If interfaces?.Any Then
            Dim nub As New CincoReleaseHistoryNub
            For Each iFace As ICincoReleaseHistory In interfaces
                nub.CopyFrom(iFace)
                l.Add(New CincoReleaseHistoryEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    #End Region

    #Region " FIND "

    ''' <summary> Count Release Histories by <see cref="IsActive"/>. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="isActive">   The is active. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal isActive As Boolean) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{CincoReleaseHistoryBuilder.TableName}]
WHERE {NameOf(CincoReleaseHistoryNub.IsActive)} = @IsActive", New CincoReleaseHistoryEntity With {.IsActive = isActive})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="isActive">   The is active. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal isActive As Boolean) As IEnumerable(Of CincoReleaseHistoryNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{CincoReleaseHistoryBuilder.TableName}]
WHERE {NameOf(CincoReleaseHistoryNub.IsActive)} = @IsActive", New CincoReleaseHistoryEntity With {.IsActive = isActive})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of CincoReleaseHistoryNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the ReleaseHistory exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="isActive">   The <see cref="CincoReleaseHistoryEntity"/> active state. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal isActive As Boolean) As Boolean
        Return 1 = CincoReleaseHistoryEntity.CountEntities(connection, isActive)
    End Function

    #End Region

    #Region " ACTIVE RELEASE "

    ''' <summary> Fetches the active entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchActive(ByVal connection As System.Data.IDbConnection) As Boolean
        Dim activeEntities As IEnumerable(Of CincoReleaseHistoryNub) = CincoReleaseHistoryEntity.FetchEntities(connection, True)
        Dim entities As IEnumerable(Of CincoReleaseHistoryNub) = CincoReleaseHistoryEntity.FetchEntities(connection, True)
        If entities.Any Then
            Me.UpdateCache(entities.Single)
            Return Me.FetchUsingKey(connection)
        Else
            Return False
        End If
    End Function

    ''' <summary> Query if 'expectedVersion' is expected current release. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="expectedVersion"> The expected version. </param>
    ''' <returns> <c>true</c> if expected current release; otherwise <c>false</c> </returns>
    Public Function IsExpectedCurrentRelease(ByVal expectedVersion As String) As Boolean
        Return String.Equals(Me.VersionNumber, expectedVersion, StringComparison.OrdinalIgnoreCase)
    End Function

    ''' <summary> Returns True if the database version verifies. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="expectedVersion"> The expected version. </param>
    ''' <param name="e">               Cancel details event information. </param>
    ''' <returns> <c>True</c> if current release; <c>False</c> otherwise. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryVerifyDatabaseVersion(ByVal connection As System.Data.IDbConnection, ByVal expectedVersion As String, ByVal e As isr.Core.ActionEventArgs) As Boolean
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Try
            Dim entity As New CincoReleaseHistoryEntity
            If entity.FetchActive(connection) Then
                If Not entity.IsExpectedCurrentRelease(expectedVersion) Then
                    e.RegisterFailure($"Active database version {entity.VersionNumber} does not match the program data version {expectedVersion}")
                End If
            Else
                e.RegisterFailure($"Release history has no active records")
            End If
        Catch ex As Exception
            e.RegisterError($"Exception fetching active release;. {ex.ToFullBlownString}")
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> Returns True if the database version verifies. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="expectedVersion"> The expected version. </param>
    ''' <returns> <c>True</c> if current release; <c>False</c> otherwise. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryVerifyDatabaseVersion(ByVal connection As System.Data.IDbConnection, ByVal expectedVersion As String) As (Success As Boolean, Details As String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim outcome As (Success As Boolean, Details As String) = (True, String.Empty)
        Try
            Dim entity As New CincoReleaseHistoryEntity
            If entity.FetchActive(connection) Then
                If Not entity.IsExpectedCurrentRelease(expectedVersion) Then
                    outcome = (False, $"Active database version {entity.VersionNumber} does not match the program data version {expectedVersion}")
                End If
            Else
                outcome = (False, "Release history has no active records")
            End If
        Catch ex As Exception
            outcome = (False, $"Exception fetching active release;. {ex.ToFullBlownString}")
        End Try
        Return outcome
    End Function

    ''' <summary> Fetches active release history version. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The active release history version. </returns>
    Public Shared Function FetchActiveReleaseHistoryVersion(ByVal connection As System.Data.IDbConnection) As Version
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return New Version(CincoReleaseHistoryEntity.FetchEntities(connection, True).Single.VersionNumber)
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the version number. </summary>
    ''' <value> The version number. </value>
    Public Property VersionNumber As String Implements ICincoReleaseHistory.VersionNumber
        Get
            Return Me.ICache.VersionNumber
        End Get
        Set(value As String)
            If Not String.Equals(Me.VersionNumber, value) Then
                Me.ICache.VersionNumber = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp. </summary>
    ''' <remarks> Stored in universal time. </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements ICincoReleaseHistory.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not Date.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the is active. </summary>
    ''' <value> The is active. </value>
    Public Property IsActive As Boolean Implements ICincoReleaseHistory.IsActive
        Get
            Return Me.ICache.IsActive
        End Get
        Set(value As Boolean)
            If Not Boolean.Equals(Me.IsActive, value) Then
                Me.ICache.IsActive = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description. </summary>
    ''' <value> The description. </value>
    Public Property Description As String Implements ICincoReleaseHistory.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    #End Region

    #Region " FIELDS: CUSTOM "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    ''' <summary> The timestamp caption format. </summary>
    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

    #End Region

End Class

