Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Ohmni.ExceptionExtensions

''' <summary>
''' Interface for the Cinco session nub and entity. Includes the fields as kept in the data
''' table. Allows tracking of property changes.
''' </summary>
''' <remarks>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value. <para>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface ICincoSession

    ''' <summary> Gets the identifier of the session. </summary>
    ''' <value> The identifier of the session. </value>
    <Key>
    Property SessionAutoId As Integer

    ''' <summary> Gets the identifier of the station. </summary>
    ''' <value> The identifier of the station. </value>
    Property StationAutoId As Integer

    ''' <summary> Gets the identifier of the test level. </summary>
    ''' <value> The identifier of the test level. </value>
    Property TestLevelId As Integer

    ''' <summary> Gets the session number within the station. </summary>
    ''' <value> The session  number. </value>
    Property SessionNumber As Integer

    ''' <summary> Gets the station name. </summary>
    ''' <value> The station name. </value>
    Property StationName As String

    ''' <summary> Gets the description. </summary>
    ''' <value> The description. </value>
    Property Description As String

    ''' <summary> Gets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Property Timestamp As DateTime

End Interface

''' <summary> The Cinco session builder. </summary>
''' <remarks> David, 4/23/2020. </remarks>
Public NotInheritable Class CincoSessionBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(CincoSessionBuilder._TableName) Then
                CincoSessionBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(CincoSessionNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return CincoSessionBuilder._TableName
        End Get
    End Property

    ''' <summary> Gets or sets the indication of using a unique session number. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="NotImplementedException">   Thrown when the requested operation is
    '''                                              unimplemented. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The using unique station session number. </value>
    Public Shared ReadOnly Property UsingUniqueStationSessionNumber As Boolean = True

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{CincoSessionBuilder.TableName}] (
	[{NameOf(CincoSessionNub.SessionAutoId)}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
	[{NameOf(CincoSessionNub.StationAutoId)}] integer NOT NULL, 
	[{NameOf(CincoSessionNub.TestLevelId)}] integer NOT NULL, 
	[{NameOf(CincoSessionNub.SessionNumber)}] integer NOT NULL, 
	[{NameOf(CincoSessionNub.StationName)}] nvarchar(50) NOT NULL, 
    [{NameOf(CincoSessionNub.Description)}] nvarchar(250) NOT NULL,
	[{NameOf(CincoSessionNub.Timestamp)}] datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP),
FOREIGN KEY ([{NameOf(CincoSessionNub.StationAutoId)}]) REFERENCES [{CincoStationBuilder.TableName}] ([{NameOf(CincoSessionNub.StationAutoId)}])	ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY ([{NameOf(CincoSessionNub.TestLevelId)}])	REFERENCES [{OhmniTestLevelBuilder.TableName}] ([{NameOf(CincoSessionNub.TestLevelId)}]) ON UPDATE CASCADE ON DELETE CASCADE); 
CREATE INDEX [UQ_{CincoSessionBuilder.TableName}] ON [{CincoSessionBuilder.TableName}] ([{NameOf(CincoSessionNub.StationAutoId)}], [{NameOf(CincoSessionNub.SessionNumber)}]); 
CREATE INDEX [UQ_{CincoStationBuilder.TableName}{CincoSessionBuilder.TableName}] ON [{CincoSessionBuilder.TableName}] ([{NameOf(CincoSessionNub.StationName)}], [{NameOf(CincoSessionNub.SessionNumber)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CincoSessionBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{CincoSessionBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{CincoSessionBuilder.TableName}](
	[{NameOf(CincoSessionNub.SessionAutoId)}] [int] IDENTITY(1,1) NOT NULL,
	[{NameOf(CincoSessionNub.StationAutoId)}] [int] NOT NULL,
	[{NameOf(CincoSessionNub.TestLevelId)}] [int] NOT NULL,
	[{NameOf(CincoSessionNub.SessionNumber)}] [int] NOT NULL,
	[{NameOf(CincoSessionNub.StationName)}] [nvarchar](50) NOT NULL,
	[{NameOf(CincoSessionNub.Description)}] [nvarchar](250) NOT NULL,
	[{NameOf(CincoSessionNub.Timestamp)}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
 CONSTRAINT [PK_{CincoSessionBuilder.TableName}] PRIMARY KEY CLUSTERED 
([{NameOf(CincoSessionNub.SessionAutoId)}] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END; 
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{CincoSessionBuilder.TableName}]') AND name = N'UQ_{CincoSessionBuilder.TableName}')
CREATE NONCLUSTERED INDEX [UQ_{CincoSessionBuilder.TableName}] ON [dbo].[{CincoSessionBuilder.TableName}] ([{NameOf(CincoSessionNub.StationAutoId)}] ASC, [{NameOf(CincoSessionNub.SessionNumber)}] ASC) 
 WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; 

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{CincoSessionBuilder.TableName}]') AND name = N'UQ_{CincoStationBuilder.TableName}{CincoSessionBuilder.TableName}')
CREATE NONCLUSTERED INDEX [UQ_Stationession] ON [dbo].[{CincoSessionBuilder.TableName}] ([{NameOf(CincoSessionNub.StationName)}] ASC, [{NameOf(CincoSessionNub.SessionNumber)}] ASC) 
 WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; 

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CincoSessionBuilder.TableName}_{CincoStationBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{CincoSessionBuilder.TableName}]'))
ALTER TABLE [dbo].[{CincoSessionBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{CincoSessionBuilder.TableName}_{CincoStationBuilder.TableName}] FOREIGN KEY([{NameOf(CincoSessionNub.StationAutoId)}])
REFERENCES [dbo].[{CincoStationBuilder.TableName}] ([{NameOf(CincoSessionNub.StationAutoId)}])
ON UPDATE CASCADE ON DELETE CASCADE; 

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CincoSessionBuilder.TableName}_{CincoStationBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{CincoSessionBuilder.TableName}]'))
ALTER TABLE [dbo].[{CincoSessionBuilder.TableName}] CHECK CONSTRAINT [FK_{CincoSessionBuilder.TableName}_{CincoStationBuilder.TableName}]; 

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CincoSessionBuilder.TableName}_{OhmniTestLevelBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{CincoSessionBuilder.TableName}]'))
ALTER TABLE [dbo].[{CincoSessionBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{CincoSessionBuilder.TableName}_{OhmniTestLevelBuilder.TableName}] FOREIGN KEY([{NameOf(CincoSessionNub.TestLevelId)}])
REFERENCES [dbo].[{OhmniTestLevelBuilder.TableName}] ([{NameOf(CincoSessionNub.TestLevelId)}])
ON UPDATE CASCADE ON DELETE CASCADE; 

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CincoSessionBuilder.TableName}_{OhmniTestLevelBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{CincoSessionBuilder.TableName}]'))
ALTER TABLE [dbo].[{CincoSessionBuilder.TableName}] CHECK CONSTRAINT [FK_{CincoSessionBuilder.TableName}_{OhmniTestLevelBuilder.TableName}]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CincoSessionBuilder.TableName
    End Function

End Class

''' <summary>
''' Implements the Cinco session table <see cref="ICincoSession">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2018 </para>
''' </remarks>
<Table("Session")>
Public Class CincoSessionNub
    Inherits EntityNubBase(Of ICincoSession)
    Implements ICincoSession

    #Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICincoSession
        Return New CincoSessionNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICincoSession
        Dim destination As ICincoSession = Me.CreateNew
        CincoSessionNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICincoSession)
        CincoSessionNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As ICincoSession, ByVal destination As ICincoSession)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.Description = source.Description
        destination.SessionAutoId = source.SessionAutoId
        destination.SessionNumber = source.SessionNumber
        destination.StationAutoId = source.StationAutoId
        destination.StationName = source.StationName
        destination.TestLevelId = source.TestLevelId
        destination.Timestamp = source.Timestamp
    End Sub

    #End Region

    #Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, ICincoSession))
        Throw New NotImplementedException()
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As ICincoSession) As Boolean
        Return other IsNot Nothing AndAlso CincoSessionNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As ICincoSession, ByVal right As ICincoSession) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso String.Equals(left.Description, right.Description)
            result = result AndAlso Integer.Equals(left.SessionAutoId, right.SessionAutoId)
            result = result AndAlso Integer.Equals(left.SessionNumber, right.SessionNumber)
            result = result AndAlso Integer.Equals(left.StationAutoId, right.StationAutoId)
            result = result AndAlso String.Equals(left.StationName, right.StationName)
            result = result AndAlso Integer.Equals(left.TestLevelId, right.TestLevelId)
            result = result AndAlso DateTime.Equals(left.Timestamp, right.Timestamp)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.Description.GetHashCode() Xor Me.SessionAutoId Xor Me.SessionNumber.GetHashCode() Xor Me.StationAutoId Xor Me.StationName.GetHashCode() Xor Me.TestLevelId Xor Me.Timestamp.GetHashCode()
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the session. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The identifier of the session. </value>
    <Key>
    Public Property SessionAutoId As Integer Implements ICincoSession.SessionAutoId

    ''' <summary> Gets or sets the identifier of the station. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The identifier of the station. </value>
    Public Property StationAutoId As Integer Implements ICincoSession.StationAutoId

    ''' <summary> Gets or sets the identifier of the test level. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The identifier of the test level. </value>
    Public Property TestLevelId As Integer Implements ICincoSession.TestLevelId

    ''' <summary> Gets or sets the session number within the station. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The session number. </value>
    Public Property SessionNumber As Integer Implements ICincoSession.SessionNumber

    ''' <summary> Gets or sets the station name. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The station name. </value>
    Public Property StationName As String Implements ICincoSession.StationName

    ''' <summary> Gets or sets the description. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The description. </value>
    Public Property Description As String Implements ICincoSession.Description

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Public Property Timestamp As DateTime Implements ICincoSession.Timestamp

    #End Region

End Class

''' <summary> The Session Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2018 </para>
''' </remarks>
Public Class CincoSessionEntity
    Inherits EntityBase(Of ICincoSession, CincoSessionNub)
    Implements ICincoSession

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New CincoSessionNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As ICincoSession)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ICincoSession, ByVal store As ICincoSession)
        MyBase.New(New CincoSessionNub, cache, store)
        Me.UsingNativeTracking = String.Equals(CincoSessionBuilder.TableName, NameOf(ICincoSession).TrimStart("I"c))
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICincoSession
        Return New CincoSessionNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICincoSession
        Dim destination As ICincoSession = Me.CreateNew
        CincoSessionNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICincoSession)
        CincoSessionNub.Copy(value, Me)
    End Sub

    #End Region

    #Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ICincoSession)
        ' first make the copy to notify of any property change.
        CincoSessionNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

    #End Region

    #Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Session table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of ICincoSession)(key), connection.Get(Of CincoSessionNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.SessionAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.StationAutoId, Me.SessionNumber)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> The Station auto id. </param>
    ''' <param name="sessionNumber"> The session number. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer, ByVal sessionNumber As Integer) As Boolean
        Me.ClearStore()
        Dim nub As CincoSessionNub = CincoSessionEntity.FetchNubs(connection, stationAutoId, sessionNumber).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ICincoSession) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If CincoSessionEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(CincoSessionBuilder.UsingUniqueStationSessionNumber,
                                        Me.FetchUsingUniqueIndex(connection, entity.StationAutoId, entity.SessionNumber),
                                        Me.FetchUsingKey(connection, entity.SessionAutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            entity.SessionAutoId = Me.SessionAutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of CincoSessionNub)(New CincoSessionNub With {.SessionAutoId = key})
    End Function

    #End Region

    #Region " ENTITIES "

    ''' <summary> Gets or sets the Session entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Session entities. </value>
    Public ReadOnly Property Sessions As IEnumerable(Of CincoSessionEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of CincoSessionEntity)
        Return If(usingNativeTracking, CincoSessionEntity.Populate(connection.GetAll(Of ICincoSession)), CincoSessionEntity.Populate(connection.GetAll(Of CincoSessionNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Sessions = CincoSessionEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(CincoSessionEntity.Sessions))
        Return If(Me.Sessions?.Any, Me.Sessions.Count, 0)
    End Function

    ''' <summary> Count Sessions. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifier for the Station. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{CincoSessionBuilder.TableName}]
WHERE {NameOf(CincoSessionNub.StationAutoId)} = @Id", New With {Key .Id = stationAutoId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> Identifier for the Station. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As IEnumerable(Of CincoSessionEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{CincoSessionBuilder.TableName}]
WHERE {NameOf(CincoSessionNub.StationAutoId)} = @Id
ORDER BY {NameOf(CincoSessionNub.SessionNumber)}", New With {Key .Id = stationAutoId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return CincoSessionEntity.Populate(connection.Query(Of CincoSessionNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Populates a list of Session entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The entity nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of CincoSessionNub)) As IEnumerable(Of CincoSessionEntity)
        Dim l As New List(Of CincoSessionEntity)
        If nubs?.Any Then
            For Each nub As CincoSessionNub In nubs
                l.Add(New CincoSessionEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Session entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The entity interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of ICincoSession)) As IEnumerable(Of CincoSessionEntity)
        Dim l As New List(Of CincoSessionEntity)
        If interfaces?.Any Then
            Dim nub As New CincoSessionNub
            For Each iFace As ICincoSession In interfaces
                nub.CopyFrom(iFace)
                l.Add(New CincoSessionEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    #End Region

    #Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> The Station auto id. </param>
    ''' <param name="sessionNumber"> The Session number within this station. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer, ByVal sessionNumber As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{CincoSessionBuilder.TableName}] /**where**/")
        
        sqlBuilder.Where($"{NameOf(CincoSessionNub.StationAutoId)} = @Id", New With {.Id = stationAutoId})
        sqlBuilder.Where($"{NameOf(CincoSessionNub.SessionNumber)} = @SessionNumber", New With {.SessionNumber = sessionNumber})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> The Station auto id. </param>
    ''' <param name="sessionNumber"> The Session number within this station. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer, ByVal sessionNumber As Integer) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{CincoSessionBuilder.TableName}]")
        selectSql.Append($" WHERE (@{NameOf(CincoSessionNub.StationAutoId)} IS NULL OR {NameOf(CincoSessionNub.StationAutoId)} = @Id)")
        selectSql.Append($" AND (@{NameOf(CincoSessionNub.SessionNumber)} IS NULL OR {NameOf(CincoSessionNub.SessionNumber)} =  @SessionNumber)")
        selectSql.Append("; ")
        
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {.Id = stationAutoId, .SessionNumber = sessionNumber})
        
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> The Station auto id. </param>
    ''' <param name="sessionNumber"> The Session number within this station. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer, ByVal sessionNumber As Integer) As IEnumerable(Of CincoSessionNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{CincoSessionBuilder.TableName}] /**where**/")
        
        sqlBuilder.Where($"{NameOf(CincoSessionNub.StationAutoId)} = @Id", New With {.Id = stationAutoId})
        sqlBuilder.Where($"{NameOf(CincoSessionNub.SessionNumber)} = @SessionNumber", New With {.SessionNumber = sessionNumber})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of CincoSessionNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Session exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> The Station auto id. </param>
    ''' <param name="sessionNumber"> The Session number within this station. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer, ByVal sessionNumber As Integer) As Boolean
        Return 1 = CincoSessionEntity.CountEntities(connection, stationAutoId, sessionNumber)
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the session. </summary>
    ''' <value> The identifier of the session. </value>
    Public Property SessionAutoId As Integer Implements ICincoSession.SessionAutoId
        Get
            Return Me.ICache.SessionAutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SessionAutoId, value) Then
                Me.ICache.SessionAutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the station. </summary>
    ''' <value> The identifier of the station. </value>
    Public Property StationAutoId As Integer Implements ICincoSession.StationAutoId
        Get
            Return Me.ICache.StationAutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.StationAutoId, value) Then
                Me.ICache.StationAutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the test level. </summary>
    ''' <value> The identifier of the test level. </value>
    Public Property TestLevelId As Integer Implements ICincoSession.TestLevelId
        Get
            Return Me.ICache.TestLevelId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.TestLevelId, value) Then
                Me.ICache.TestLevelId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the session number within the station. </summary>
    ''' <value> The session number. </value>
    Public Property SessionNumber As Integer Implements ICincoSession.SessionNumber
        Get
            Return Me.ICache.SessionNumber
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SessionNumber, value) Then
                Me.ICache.SessionNumber = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the station name. </summary>
    ''' <value> The station name. </value>
    Public Property StationName As String Implements ICincoSession.StationName
        Get
            Return Me.ICache.StationName
        End Get
        Set(value As String)
            If Not String.Equals(Me.StationName, value) Then
                Me.ICache.StationName = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description. </summary>
    ''' <value> The description. </value>
    Public Property Description As String Implements ICincoSession.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements ICincoSession.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    #End Region

    #Region " FIELDS: CUSTOM "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    ''' <summary> The timestamp caption format. </summary>
    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

    ''' <summary> Gets the session identity. </summary>
    ''' <value> The session identity. </value>
    Public ReadOnly Property Identity As String
        Get
            Return $"{Me.StationName}:{Me.SessionNumber}"
        End Get
    End Property

    #End Region

    #Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="sessionAutoId"> The entity unique key. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As TestSessionEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection,
                                            ByVal sessionAutoId As Integer) As (Success As Boolean, Details As String, Entity As CincoSessionEntity)
        Dim activity As String = String.Empty
        Dim entity As New CincoSessionEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching session by id {sessionAutoId}"
            If Not entity.FetchUsingKey(connection, sessionAutoId) Then
                success = False
                details = $"Failed {activity}"
            End If
        Catch ex As Exception
            success = False
            details = $"Exception {activity};. {ex.ToFullBlownString}"
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> The Station auto id. </param>
    ''' <param name="sessionNumber"> The session number. </param>
    ''' <returns> The (Success As Boolean, Details As String, Entity As TestSessionEntity) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection,
                                                    ByVal stationAutoId As Integer, ByVal sessionNumber As Integer) As (Success As Boolean, Details As String, Entity As CincoSessionEntity)
        Dim activity As String = String.Empty
        Dim entity As New CincoSessionEntity
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            activity = $"Fetching session by station id {stationAutoId} and session number {sessionNumber}"
            If Not entity.FetchUsingUniqueIndex(connection, stationAutoId, sessionNumber) Then
                success = False
                details = $"Failed {activity}"
            End If
        Catch ex As Exception
            success = False
            details = $"Exception {activity};. {ex.ToFullBlownString}"
        End Try
        Return (success, details, entity)
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of TestSessionEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of CincoSessionEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New CincoSessionEntity()
            activity = "fetching all Sessions"
            ' fetch all Sessions
            entity.FetchAllEntities(connection)
            Return (True, String.Empty, entity.Sessions)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", New List(Of CincoSessionEntity))
        End Try
    End Function

    #End Region

End Class

