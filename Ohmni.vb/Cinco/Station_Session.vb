Imports Dapper

Partial Public Class CincoStationEntity


    #Region " RELATIONS "

    ''' <summary> Adds a session. </summary>
    ''' <remarks> Assumes that sessions are. </remarks>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="description"> The description. </param>
    ''' <param name="testLevel">   The test level. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function AddSession(ByVal connection As System.Data.IDbConnection, ByVal description As String, ByVal testLevel As Integer) As CincoSessionEntity
        Dim sessionNumber As Integer = 1
        If Me.Sessions?.Any Then
        Else
            Me.FetchSessions(connection)
        End If
        If Me.Sessions.Any Then sessionNumber = Me.Sessions.Last.SessionNumber + 1
        Dim session As New CincoSessionEntity With {.StationAutoId = Me.StationAutoId, .SessionNumber = sessionNumber,
                                                         .StationName = Me.StationName, .Description = description, .TestLevelId = testLevel}
        session.Insert(connection)
        Return session
    End Function

    ''' <summary> Gets or sets the Session entities. </summary>
    ''' <value> The Session entities. </value>
    Public ReadOnly Property Sessions As IEnumerable(Of CincoSessionEntity)

    ''' <summary> Fetches the Sessions. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Sessions. </returns>
    Public Function FetchSessions(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Sessions = CincoSessionEntity.FetchEntities(connection, Me.StationAutoId)
        Me.NotifyPropertyChanged(NameOf(CincoStationEntity.Sessions))
        Return If(Me.Sessions?.Any, Me.Sessions.Count, 0)
    End Function

    ''' <summary> Fetches the Sessions. </summary>
    ''' <remarks>
    ''' https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
    ''' https://dapper-tutorial.net/querymultiple.
    ''' </remarks>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="stationAutoId"> The identifier of the Station. </param>
    ''' <returns> The Sessions. </returns>
    Public Function FetchSessions(ByVal connection As System.Data.IDbConnection, ByVal stationAutoId As Integer) As Integer
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select * from [{CincoStationBuilder.TableName}] where {NameOf(CincoStationNub.StationAutoId)} = @{NameOf(CincoStationNub.StationAutoId)}; ")
        queryBuilder.AppendLine($"select * from [{CincoSessionBuilder.TableName}] where {NameOf(CincoSessionNub.StationAutoId)} = @{NameOf(CincoSessionNub.StationAutoId)} ORDER BY {NameOf(CincoSessionNub.SessionNumber)}; ")
        Dim gridReader As SqlMapper.GridReader = connection.QueryMultiple(queryBuilder.ToString, New CincoStationNub With {.StationAutoId = stationAutoId})
        Dim entity As ICincoStation = gridReader.ReadSingle(Of CincoStationNub)()
        If entity Is Nothing Then
            Me._Sessions = New List(Of CincoSessionEntity)
        Else
            If Not CincoStationNub.AreEqual(entity, Me.ICache) Then
                Me.UpdateCache(entity)
                Me.UpdateStore()
            End If
            Me._Sessions = CincoSessionEntity.Populate(gridReader.Read(Of CincoSessionNub))
        End If
        Return If(Me.Sessions?.Any, Me.Sessions.Count, 0)
    End Function

    #End Region


End Class
