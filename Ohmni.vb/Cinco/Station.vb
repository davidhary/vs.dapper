Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary>
''' Interface for the Station nub and entity. Includes the fields as kept in the data table.
''' Allows tracking of property changes.
''' </summary>
''' <remarks>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value. <para>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface ICincoStation

    ''' <summary> Gets the identifier of the station. </summary>
    ''' <value> The identifier of the station. </value>
    <Key>
    Property StationAutoId As Integer

    ''' <summary> Gets the station name. </summary>
    ''' <value> The station name. </value>
    Property StationName As String

    ''' <summary> Gets the name of the computer. </summary>
    ''' <value> The name of the computer. </value>
    Property ComputerName As String

    ''' <summary> Gets the timezone identity. </summary>
    ''' <remarks>
    ''' The entity <see cref="TimeZoneId"/> is derived from the
    ''' <see cref="System.TimeZone.CurrentTimeZone"/> Standard Name, which, in-fact, is the timezone
    ''' identity. The Standard Name is used by the
    ''' <see cref="System.TimeZoneInfo.FindSystemTimeZoneById(String)"/> to select the
    ''' <see cref="System.TimeZoneInfo">time zone info</see> for the specified identity (Standard
    ''' Name).
    ''' </remarks>
    ''' <value> The timezone identity. </value>
    Property TimeZoneId As String

    ''' <summary> Gets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Property Timestamp As DateTime

End Interface

''' <summary> A station builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class CincoStationBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(CincoStationBuilder._TableName) Then
                CincoStationBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(CincoStationNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return CincoStationBuilder._TableName
        End Get
    End Property

    ''' <summary> Gets or sets the indication of using unique station. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="NotImplementedException">   Thrown when the requested operation is
    '''                                              unimplemented. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the using unique station. </value>
    Public Shared ReadOnly Property UsingUniqueStationName As Boolean = True

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{CincoStationBuilder.TableName}] (
	[{NameOf(CincoStationNub.StationAutoId)}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
	[{NameOf(CincoStationNub.StationName)}] nvarchar(50) NOT NULL, 
	[{NameOf(CincoStationNub.ComputerName)}] nvarchar(50) NOT NULL, 
	[{NameOf(CincoStationNub.TimeZoneId)}] nvarchar(50) NOT NULL, 
	[{NameOf(CincoStationNub.Timestamp)}] datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP));
CREATE UNIQUE INDEX [UQ_{CincoStationBuilder.TableName}_{NameOf(CincoStationNub.ComputerName)}] ON [{CincoStationBuilder.TableName}] ([{NameOf(CincoStationNub.ComputerName)}]); 
CREATE UNIQUE INDEX [UQ_{CincoStationBuilder.TableName}_{NameOf(CincoStationNub.StationName)}] ON [{CincoStationBuilder.TableName}] ([{NameOf(CincoStationNub.StationName)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CincoStationBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{CincoStationBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{CincoStationBuilder.TableName}](
	[{NameOf(CincoStationNub.StationAutoId)}] [int] IDENTITY(1,1) NOT NULL,
	[{NameOf(CincoStationNub.StationName)}] [nvarchar](50) NOT NULL,
	[{NameOf(CincoStationNub.ComputerName)}] [nvarchar](50) NOT NULL,
	[{NameOf(CincoStationNub.TimeZoneId)}] [nvarchar](50) NOT NULL,
	[{NameOf(CincoStationNub.Timestamp)}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
 CONSTRAINT [PK_{CincoStationBuilder.TableName}] PRIMARY KEY CLUSTERED 
([{NameOf(CincoStationNub.StationAutoId)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END;
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{CincoStationBuilder.TableName}]') AND name = N'UQ_{CincoStationBuilder.TableName}_{NameOf(CincoStationNub.ComputerName)}')
CREATE UNIQUE NONCLUSTERED INDEX [UQ_{CincoStationBuilder.TableName}_{NameOf(CincoStationNub.ComputerName)}] ON [dbo].[{CincoStationBuilder.TableName}] ([{NameOf(CincoStationNub.ComputerName)}] ASC) 
 WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
 ON [PRIMARY]; 
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{CincoStationBuilder.TableName}]') AND name = N'UQ_{CincoStationBuilder.TableName}_{NameOf(CincoStationNub.StationName)}')
CREATE UNIQUE NONCLUSTERED INDEX [UQ_{CincoStationBuilder.TableName}_{NameOf(CincoStationNub.StationName)}] ON [dbo].[{CincoStationBuilder.TableName}] ([{NameOf(CincoStationNub.StationName)}] ASC) 
 WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
 ON [PRIMARY]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CincoStationBuilder.TableName
    End Function

End Class

''' <summary> Implements the Station table <see cref="ICincoStation">interface</see>. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2018 </para>
''' </remarks>
<Table("Station")>
Public Class CincoStationNub
    Inherits EntityNubBase(Of ICincoStation)
    Implements ICincoStation

    #Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICincoStation
        Return New CincoStationNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICincoStation
        Dim destination As ICincoStation = Me.CreateNew
        CincoStationNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICincoStation)
        CincoStationNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As ICincoStation, ByVal destination As ICincoStation)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.ComputerName = source.ComputerName
        destination.StationAutoId = source.StationAutoId
        destination.StationName = source.StationName
        destination.Timestamp = source.Timestamp
        destination.TimeZoneId = source.TimeZoneId
    End Sub

    #End Region

    #Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, ICincoStation))
        Throw New NotImplementedException()
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As ICincoStation) As Boolean
        Return other IsNot Nothing AndAlso CincoStationNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As ICincoStation, ByVal right As ICincoStation) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso String.Equals(left.ComputerName, right.ComputerName)
            result = result AndAlso Integer.Equals(left.StationAutoId, right.StationAutoId)
            result = result AndAlso String.Equals(left.StationName, right.StationName)
            result = result AndAlso String.Equals(left.TimeZoneId, right.TimeZoneId)
            result = result AndAlso Date.Equals(left.Timestamp, right.Timestamp)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.ComputerName.GetHashCode Xor Me.StationAutoId Xor Me.StationName.GetHashCode() Xor Me.TimeZoneId.GetHashCode() Xor Me.Timestamp.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the station. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The identifier of the station. </value>
    <Key>
    Public Property StationAutoId As Integer Implements ICincoStation.StationAutoId

    ''' <summary> Gets or sets the station name. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The station name. </value>
    Public Property StationName As String Implements ICincoStation.StationName

    ''' <summary> Gets or sets the name of the computer. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the computer. </value>
    Public Property ComputerName As String Implements ICincoStation.ComputerName

    ''' <summary> Gets or sets the timezone identity. </summary>
    ''' <remarks>
    ''' The entity <see cref="TimezoneId"/> is derived from the
    ''' <see cref="System.TimeZone.CurrentTimeZone"/> Standard Name, which, in-fact, is the timezone
    ''' identity. The Standard Name is used by the
    ''' <see cref="System.TimeZoneInfo.FindSystemTimeZoneById(String)"/> to select the
    ''' <see cref="System.TimeZoneInfo">time zone info</see> for the specified identity (Standard
    ''' Name).
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The timezone identity. </value>
    Public Property TimeZoneId As String Implements ICincoStation.TimeZoneId

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Public Property Timestamp As DateTime Implements ICincoStation.Timestamp

    #End Region

    #Region " ENTITY LOCAL TIMES "

    ''' <summary> Information describing the locale time zone. </summary>
    Private _LocaleTimeZoneInfo As TimeZoneInfo

    ''' <summary> Returns the information describing the time zone in the entity locale. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A TimeZoneInfo. </returns>
    Public Function LocaleTimeZoneInfo() As TimeZoneInfo
        If Me._LocaleTimeZoneInfo Is Nothing Then
            If String.IsNullOrWhiteSpace(Me.TimeZoneId) Then Me.TimeZoneId = TimeZoneInfo.Local.Id
            Me._LocaleTimeZoneInfo = System.TimeZoneInfo.FindSystemTimeZoneById(Me.TimeZoneId)
        End If
        Return Me._LocaleTimeZoneInfo
    End Function

    ''' <summary>
    ''' Returns the time now in the entity locale based on the Locale
    ''' <see cref="System.TimeZoneInfo"/>.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The time now in the entity locale. </returns>
    Public Function LocaleTimeNow() As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTime(DateTimeOffset.Now, Me.LocaleTimeZoneInfo)
    End Function

    ''' <summary> Returns the <see cref="Timestamp"/> in the entity local. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The <see cref="Timestamp"/> entity locale. </returns>
    Public Function LocaleTimestamp() As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTimeFromUtc(Me.Timestamp, Me.LocaleTimeZoneInfo)
    End Function

    ''' <summary> Converts an universalTime to a locale timestamp. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="universalTime"> The universal time. </param>
    ''' <returns> UniversalTime as a DateTimeOffset. </returns>
    Public Function ToLocaleTimestamp(ByVal universalTime As DateTime) As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTimeFromUtc(universalTime, Me.LocaleTimeZoneInfo)
    End Function

    #End Region

End Class

''' <summary> The Station Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2018 </para>
''' </remarks>
Public Class CincoStationEntity
    Inherits EntityBase(Of ICincoStation, CincoStationNub)
    Implements ICincoStation

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New CincoStationNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Station interface. </param>
    Public Sub New(ByVal value As ICincoStation)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ICincoStation, ByVal store As ICincoStation)
        MyBase.New(New CincoStationNub, cache, store)
        Me.UsingNativeTracking = String.Equals(CincoStationBuilder.TableName, NameOf(ICincoStation).TrimStart("I"c))
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICincoStation
        Return New CincoStationNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICincoStation
        Dim destination As ICincoStation = Me.CreateNew
        CincoStationNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICincoStation)
        CincoStationNub.Copy(value, Me)
    End Sub

    #End Region

    #Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The Station interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ICincoStation)
        ' first make the copy to notify of any property change.
        CincoStationNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

    #End Region

    #Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The Station table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of ICincoStation)(key), connection.Get(Of CincoStationNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.StationAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.ComputerName)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="computerName"> Name of the computer. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal computerName As String) As Boolean
        Me.ClearStore()
        Dim nub As CincoStationNub = CincoStationEntity.FetchNubs(connection, computerName).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Obtains a default entity using the given connection. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function ObtainDefault(ByVal connection As System.Data.IDbConnection) As Boolean
        If String.IsNullOrEmpty(Me.ComputerName) Then Me.ComputerName = My.Computer.Name
        If String.IsNullOrEmpty(Me.StationName) Then Me.StationName = Me.ComputerName
        If String.IsNullOrWhiteSpace(Me.TimeZoneId) Then Me.TimeZoneId = TimeZone.CurrentTimeZone.StandardName
        Return Me.Obtain(connection)
    End Function

    ''' <summary> Attempts to obtain from the given data. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="computerName">  Name of the computer. </param>
    ''' <param name="promptEnabled"> True to enable, false to disable the prompt. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryObtain(ByVal connection As System.Data.IDbConnection, ByVal computerName As String, ByVal promptEnabled As Boolean) As Boolean
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        If String.IsNullOrWhiteSpace(computerName) Then Throw New ArgumentNullException(NameOf(computerName))
        Me.ComputerName = computerName
        If Me.FetchUsingUniqueIndex(connection) Then
            Return Me.IsClean
        ElseIf promptEnabled Then
            Return isr.Core.MyDialogResult.Yes = isr.Core.WindowsForms.ShowDialogNoYes($"Station not found for computer '{computerName}'",
                                                                                                             "Add Station to the Database?") AndAlso Me.ObtainDefault(connection)
        Else
            Return Me.ObtainDefault(connection)
        End If
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ICincoStation) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If CincoStationEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        Dim fetched As Boolean = If(CincoStationBuilder.UsingUniqueStationName,
                                        Me.FetchUsingUniqueIndex(connection, entity.StationName),
                                        Me.FetchUsingKey(connection, entity.StationAutoId))
        If fetched Then
            ' update the existing record from the specified entity.
            ' insert a new record based on the specified entity values.
            entity.StationAutoId = Me.StationAutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of CincoStationNub)(New CincoStationNub With {.StationAutoId = key})
    End Function

    #End Region

    #Region " ENTITIES "

    ''' <summary> Gets or sets the Station entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Station entities. </value>
    Public ReadOnly Property Stations As IEnumerable(Of CincoStationEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of CincoStationEntity)
        Return If(usingNativeTracking, CincoStationEntity.Populate(connection.GetAll(Of ICincoStation)), CincoStationEntity.Populate(connection.GetAll(Of CincoStationNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Stations = CincoStationEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(CincoStationEntity.Stations))
        Return If(Me.Stations?.Any, Me.Stations.Count, 0)
    End Function

    ''' <summary> Populates a list of Station entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The Station nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of CincoStationNub)) As IEnumerable(Of CincoStationEntity)
        Dim l As New List(Of CincoStationEntity)
        If nubs?.Any Then
            For Each nub As CincoStationNub In nubs
                l.Add(New CincoStationEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Station entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The Station interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of ICincoStation)) As IEnumerable(Of CincoStationEntity)
        Dim l As New List(Of CincoStationEntity)
        If interfaces?.Any Then
            Dim nub As New CincoStationNub
            For Each iFace As ICincoStation In interfaces
                nub.CopyFrom(iFace)
                l.Add(New CincoStationEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    #End Region

    #Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="computerName"> Name of the computer. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal computerName As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        ' Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{StationNub.TableName}]
        ' WHERE {NameOf(StationNub.ComputerName)} = @ComputerName", New With {Key .ComputerName = ComputerName})
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{CincoStationBuilder.TableName}]
WHERE {NameOf(CincoStationNub.ComputerName)} = @ComputerName", New CincoStationEntity With {.ComputerName = computerName})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="computerName"> The computer name. </param>
    ''' <returns> An IStation . </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal computerName As String) As IEnumerable(Of CincoStationNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{CincoStationBuilder.TableName}]
WHERE {NameOf(CincoStationNub.ComputerName)} = @ComputerName", New CincoStationEntity With {.ComputerName = computerName})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of CincoStationNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the Station exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="computerName"> The computer name. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal computerName As String) As Boolean
        Return 1 = CincoStationEntity.CountEntities(connection, computerName)
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the station. </summary>
    ''' <value> The identifier of the station. </value>
    Public Property StationAutoId As Integer Implements ICincoStation.StationAutoId
        Get
            Return Me.ICache.StationAutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.StationAutoId, value) Then
                Me.ICache.StationAutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the name. </summary>
    ''' <value> The name. </value>
    Public Property StationName As String Implements ICincoStation.StationName
        Get
            Return Me.ICache.StationName
        End Get
        Set(value As String)
            If Not String.Equals(Me.StationName, value) Then
                Me.ICache.StationName = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the name of the computer. </summary>
    ''' <value> The name of the computer. </value>
    Public Property ComputerName As String Implements ICincoStation.ComputerName
        Get
            Return Me.ICache.ComputerName
        End Get
        Set(value As String)
            If Not String.Equals(Me.ComputerName, value) Then
                Me.ICache.ComputerName = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the timezone identity. </summary>
    ''' <remarks>
    ''' The entity <see cref="TimezoneId"/> is derived from the
    ''' <see cref="System.TimeZone.CurrentTimeZone"/> Standard Name, which, in-fact, is the timezone
    ''' identity. The Standard Name is used by the
    ''' <see cref="System.TimeZoneInfo.FindSystemTimeZoneById(String)"/> to select the
    ''' <see cref="System.TimeZoneInfo">time zone info</see> for the specified identity (Standard
    ''' Name).
    ''' </remarks>
    ''' <value> The timezone identity. </value>
    Public Property TimeZoneId As String Implements ICincoStation.TimeZoneId
        Get
            Return Me.ICache.TimeZoneId
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimeZoneId, value) Then
                Me.ICache.TimeZoneId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp. </summary>
    ''' <remarks> Stored in universal time. </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements ICincoStation.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    #End Region

    #Region " ENTITY LOCAL TIMES "

    ''' <summary> Information describing the locale time zone. </summary>
    Private _LocaleTimeZoneInfo As TimeZoneInfo

    ''' <summary> Returns the information describing the time zone in the entity locale. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A TimeZoneInfo. </returns>
    Public Function LocaleTimeZoneInfo() As TimeZoneInfo
        If Me._LocaleTimeZoneInfo Is Nothing Then
            If String.IsNullOrWhiteSpace(Me.TimeZoneId) Then Me.TimeZoneId = TimeZoneInfo.Local.Id
            Me._LocaleTimeZoneInfo = System.TimeZoneInfo.FindSystemTimeZoneById(Me.TimeZoneId)
        End If
        Return Me._LocaleTimeZoneInfo
    End Function

    ''' <summary>
    ''' Returns the time now in the entity locale based on the Locale
    ''' <see cref="System.TimeZoneInfo"/>.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The time now in the entity locale. </returns>
    Public Function LocaleTimeNow() As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTime(DateTimeOffset.Now, Me.LocaleTimeZoneInfo)
    End Function

    ''' <summary> Returns the <see cref="Timestamp"/> in the entity local. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The <see cref="Timestamp"/> entity locale. </returns>
    Public Function LocaleTimestamp() As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTimeFromUtc(Me.Timestamp, Me.LocaleTimeZoneInfo)
    End Function

    ''' <summary> Converts an universalTime to a locale timestamp. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="universalTime"> The universal time. </param>
    ''' <returns> UniversalTime as a DateTimeOffset. </returns>
    Public Function ToLocaleTimestamp(ByVal universalTime As DateTime) As DateTimeOffset
        Return System.TimeZoneInfo.ConvertTimeFromUtc(universalTime, Me.LocaleTimeZoneInfo)
    End Function

    #End Region

    #Region " TIMESTAMP "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    ''' <summary> The timestamp caption format. </summary>
    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

    #End Region

End Class

