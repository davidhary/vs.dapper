Imports System.ComponentModel
Imports Dapper

Imports isr.Dapper.Entities

Partial Public Class CincoPartEntity

    #Region " BUILD "

    ''' <summary> Inserts a test level values described by connection. </summary>
    ''' <remarks> David, 3/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertAttributeTypeValues(ByVal connection As System.Data.IDbConnection) As Integer
        Return OhmniAttributeTypeBuilder.InsertIgnoreValues(connection, GetType(AnnealingAttributeType), New Integer() {AnnealingAttributeType.None})
    End Function

    ''' <summary> Inserts a specification values described by connection. </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertSpecificationValues(ByVal connection As System.Data.IDbConnection) As Integer
        Return Entities.PartNamingTypeBuilder.Get.InsertIgnore(connection, GetType(Entities.PartNamingType), Array.Empty(Of Integer))
    End Function

    #End Region

    #Region " ATTRIBUTE RANGES "

    ''' <summary> Gets or sets the part attribute ranges. </summary>
    ''' <value> The part attribute ranges. </value>
    Public ReadOnly Property AttributeRangeEntities As IEnumerable(Of OhmniPartAttributeRangeEntity)

    ''' <summary> Fetches the PartAttributeRanges. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The PartAttributeRanges. </returns>
    Public Function FetchPartAttributeRanges(ByVal connection As System.Data.IDbConnection) As Integer
        Return Me.Populate(OhmniPartAttributeRangeEntity.FetchEntities(connection, Me.PartAutoId))
    End Function

    ''' <summary> Fetches the PartAttributeRanges. </summary>
    ''' <remarks>
    ''' https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
    ''' https://dapper-tutorial.net/querymultiple.
    ''' </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> The identifier of the part. </param>
    ''' <returns> The PartAttributeRanges. </returns>
    Public Function FetchPartAttributeRanges(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select * from [{CincoPartBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(CincoPartNub.PartAutoId)} = @Id; ")
        queryBuilder.AppendLine($"select * from [{OhmniPartAttributeRangeBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(OhmniPartAttributeRangeNub.PartAutoId)} = @Id; ")
        Dim gridReader As SqlMapper.GridReader = connection.QueryMultiple(queryBuilder.ToString, New With {.Id = partAutoId})
        Dim entity As ICincoPart = gridReader.ReadSingle(Of CincoPartNub)()
        If entity Is Nothing Then
            Return Me.Populate(New List(Of OhmniPartAttributeRangeEntity))
        Else
            If Not CincoPartNub.AreEqual(entity, Me.ICache) Then
                Me.UpdateCache(entity)
                Me.UpdateStore()
            End If
            Return Me.Populate(OhmniPartAttributeRangeEntity.Populate(gridReader.Read(Of OhmniPartAttributeRangeNub)))
        End If
        Return If(Me.AttributeRangeEntities?.Any, Me.AttributeRangeEntities.Count, 0)
    End Function

    ''' <summary> Gets or sets the Part Attribute Ranges. </summary>
    ''' <value> The Part Range attribute values. </value>
    Public ReadOnly Property AttributeRanges As CincoPartAttributeRangeCollection

    ''' <summary> Fetches attribute ranges. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Ranges. </returns>
    Public Function FetchRanges(ByVal connection As System.Data.IDbConnection) As Integer
        Return Me.Populate(OhmniPartAttributeRangeEntity.FetchEntities(connection, Me.PartAutoId))
    End Function

    ''' <summary> Fetches attribute values. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifier for the part. </param>
    ''' <returns> The Ranges. </returns>
    Public Shared Function FetchRanges(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As CincoPartAttributeRangeCollection
        Dim Ranges As New CincoPartAttributeRangeCollection(partAutoId)
        Ranges.Populate(OhmniPartAttributeRangeEntity.FetchEntities(connection, partAutoId))
        Return Ranges
    End Function

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    ''' <returns> The number of entities. </returns>
    Private Overloads Function Populate(ByVal entities As IEnumerable(Of OhmniPartAttributeRangeEntity)) As Integer
        Me._AttributeRangeEntities = entities
        Me._AttributeRanges = New CincoPartAttributeRangeCollection(Me.PartAutoId)
        Me._AttributeRanges.Populate(entities)
        Me.NotifyPropertyChanged(NameOf(CincoPartEntity.AttributeRangeEntities))
        Me.NotifyPropertyChanged(NameOf(CincoPartEntity.AttributeRanges))
        Return If(entities?.Any, entities.Count, 0)
    End Function

    #End Region

    #Region " ATTRIBUTE VALUES "

    ''' <summary> Gets or sets the part attribute values. </summary>
    ''' <value> The part attribute values. </value>
    Public ReadOnly Property AttributeValueEntities As IEnumerable(Of OhmniPartAttributeValueEntity)

    ''' <summary> Fetches the PartAttributeValues. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The PartAttributeValues. </returns>
    Public Function FetchPartAttributeValues(ByVal connection As System.Data.IDbConnection) As Integer
        Return Me.Populate(OhmniPartAttributeValueEntity.FetchEntities(connection, Me.PartAutoId))
    End Function

    ''' <summary> Fetches the PartAttributeValues. </summary>
    ''' <remarks>
    ''' https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
    ''' https://dapper-tutorial.net/querymultiple.
    ''' </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> The identifier of the part. </param>
    ''' <returns> The PartAttributeValues. </returns>
    Public Function FetchPartAttributeValues(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select * from [{CincoPartBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(CincoPartNub.PartAutoId)} = @Id; ")
        queryBuilder.AppendLine($"select * from [{OhmniPartAttributeValueBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(OhmniPartAttributeValueNub.PartAutoId)} = @Id; ")
        Dim gridReader As SqlMapper.GridReader = connection.QueryMultiple(queryBuilder.ToString, New With {.Id = partAutoId})
        Dim entity As ICincoPart = gridReader.ReadSingle(Of CincoPartNub)()
        If entity Is Nothing Then
            Return Me.Populate(New List(Of OhmniPartAttributeValueEntity))
        Else
            If Not CincoPartNub.AreEqual(entity, Me.ICache) Then
                Me.UpdateCache(entity)
                Me.UpdateStore()
            End If
            Return Me.Populate(OhmniPartAttributeValueEntity.Populate(gridReader.Read(Of OhmniPartAttributeValueNub)))
        End If
    End Function

    ''' <summary> Gets or sets the Part Attribute Values. </summary>
    ''' <value> The Part Value attribute values. </value>
    Public ReadOnly Property AttributeValues As CincoPartAttributeValueCollection

    ''' <summary> Fetches attribute Values. </summary>
    ''' <remarks> David, 3/31/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The Values. </returns>
    Public Function FetchValues(ByVal connection As System.Data.IDbConnection) As Integer
        Return Me.Populate(OhmniPartAttributeValueEntity.FetchEntities(connection, Me.PartAutoId))
    End Function

    ''' <summary> Fetches attribute values. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifier for the part. </param>
    ''' <returns> The Values. </returns>
    Public Shared Function FetchValues(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As CincoPartAttributeValueCollection
        Dim Values As New CincoPartAttributeValueCollection(partAutoId)
        Values.Populate(OhmniPartAttributeValueEntity.FetchEntities(connection, partAutoId))
        Return Values
    End Function

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 5/21/2020. </remarks>
    ''' <param name="entities"> The entities. </param>
    ''' <returns> The number of entities. </returns>
    Private Overloads Function Populate(ByVal entities As IEnumerable(Of OhmniPartAttributeValueEntity)) As Integer
        Me._AttributeValueEntities = entities
        Me._AttributeValues = New CincoPartAttributeValueCollection(Me.PartAutoId)
        Me._AttributeValues.Populate(entities)
        Me.NotifyPropertyChanged(NameOf(CincoPartEntity.AttributeValueEntities))
        Me.NotifyPropertyChanged(NameOf(CincoPartEntity.AttributeValues))
        Return If(entities?.Any, entities.Count, 0)
    End Function

    #End Region

End Class

''' <summary>
''' Values that represent annealing attribute types for the attribute type table.
''' </summary>
''' <remarks> David, 2020-10-02. </remarks>
Public Enum AnnealingAttributeType

    ''' <summary> . </summary>
    <Description("Attribute Type")> None = 0

    ''' <summary> . </summary>
    <Description("Nominal Value")> NominalValue = 1

    ''' <summary> . </summary>
    <Description("Tolerance")> Tolerance = 2

    ''' <summary> . </summary>
    <Description("Tracking Tolerance")> TrackingTolerance = 3

    ''' <summary> . </summary>
    <Description("TCR")> Tcr = 4

    ''' <summary> . </summary>
    <Description("Tracking TCR")> TrackingTcr = 5

    ''' <summary> . </summary>
    <Description("TaN Value")> TanValue = 6

    ''' <summary> . </summary>
    <Description("Annealing Shift")> AnnealingShift = TanValue + 1
End Enum

''' <summary> Collection of ohmni cinco part attribute values. </summary>
''' <remarks> David, 4/30/2020. </remarks>
Public Class CincoPartAttributeValueCollection
    Inherits OhmniPartAttributeValueCollection

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="partAutoId"> Identifier for the part. </param>
    Public Sub New(ByVal partAutoId As Integer)
        MyBase.New(partAutoId)
    End Sub

    ''' <summary> Converts a name to a key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Private Function ToKey(ByVal name As String) As Integer
        Return CType([Enum].Parse(GetType(AnnealingAttributeType), name), Integer)
    End Function

    ''' <summary> Gets or sets the attribute value. </summary>
    ''' <value> The attribute value. Returns <see cref="Double.NaN"/> if value does not exist. </value>
    Protected Property AttributeValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double
        Get
            Dim key As Integer = Me.ToKey(name)
            Return If(Me.Contains(key), Me(key).Value, Double.NaN)
        End Get
        Set(value As Double)
            Dim key As Integer = Me.ToKey(name)
            If Me.Contains(key) Then
                Me(key).Value = value
            Else
                Me.Add(New OhmniPartAttributeValueEntity With {.PartAutoId = Me.PartAutoId, .AttributeTypeId = key, .Value = value})
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the nominal value. </summary>
    ''' <value> The nominal value. </value>
    Public Property NominalValue As Double
        Get
            Return Me.AttributeValue
        End Get
        Set(value As Double)
            Me.AttributeValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the tolerance. </summary>
    ''' <value> The tolerance. </value>
    Public Property Tolerance As Double
        Get
            Return Me.AttributeValue
        End Get
        Set(value As Double)
            Me.AttributeValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the tracking tolerance. </summary>
    ''' <value> The tracking tolerance. </value>
    Public Property TrackingTolerance As Double
        Get
            Return Me.AttributeValue
        End Get
        Set(value As Double)
            Me.AttributeValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the tcr. </summary>
    ''' <value> The tcr. </value>
    Public Property Tcr As Double
        Get
            Return Me.AttributeValue
        End Get
        Set(value As Double)
            Me.AttributeValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the tracking tcr. </summary>
    ''' <value> The tracking tcr. </value>
    Public Property TrackingTcr As Double
        Get
            Return Me.AttributeValue
        End Get
        Set(value As Double)
            Me.AttributeValue = value
        End Set
    End Property

End Class

''' <summary> Collection of ohmni cinco part attribute ranges. </summary>
''' <remarks> David, 4/30/2020. </remarks>
Public Class CincoPartAttributeRangeCollection
    Inherits OhmniPartAttributeRangeCollection

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="partAutoId"> Identifier for the part. </param>
    Public Sub New(ByVal partAutoId As Integer)
        MyBase.New(partAutoId)
    End Sub

    ''' <summary> Converts a name to a key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> Name as an Integer. </returns>
    Private Function ToKey(ByVal name As String) As Integer
        Return CType([Enum].Parse(GetType(AnnealingAttributeType), name), Integer)
    End Function

    ''' <summary> Gets or sets the attribute range. </summary>
    ''' <value>
    ''' The attribute value. Returns <see cref="isr.Core.Constructs.RangeR.Empty"/> if value does not
    ''' exist.
    ''' </value>
    Protected Property AttributeRange(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As isr.Core.Constructs.RangeR
        Get
            Dim key As Integer = Me.ToKey(name)
            Return If(Me.Contains(key), Me(key).Range, isr.Core.Constructs.RangeR.Empty)
        End Get
        Set(value As isr.Core.Constructs.RangeR)
            Dim key As Integer = Me.ToKey(name)
            If Me.Contains(key) Then
                Me(key).Range = value
            Else
                Me.Add(New OhmniPartAttributeRangeEntity With {.PartAutoId = Me.PartAutoId, .AttributeTypeId = key, .Range = value})
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the tangent value. </summary>
    ''' <value> The tangent value. </value>
    Public Property TanValue As isr.Core.Constructs.RangeR
        Get
            Return Me.AttributeRange
        End Get
        Set(value As isr.Core.Constructs.RangeR)
            Me.AttributeRange = value
        End Set
    End Property

    ''' <summary> Gets or sets the annealing shift. </summary>
    ''' <value> The annealing shift. </value>
    Public Property AnnealingShift As isr.Core.Constructs.RangeR
        Get
            Return Me.AttributeRange
        End Get
        Set(value As isr.Core.Constructs.RangeR)
            Me.AttributeRange = value
        End Set
    End Property

End Class
