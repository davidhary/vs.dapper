Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions
Imports isr.Dapper.Ohmni.ExceptionExtensions

''' <summary>
''' Interface for the part nub and entity. Includes the part fields as kept in the part table.
''' Allows tracking of property change.
''' </summary>
''' <remarks>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value. <para>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface ICincoPart

    ''' <summary> Gets the identifier of the part. </summary>
    ''' <value> The identifier of the part. </value>
    <Key>
    Property PartAutoId As Integer

    ''' <summary> Gets the part number. </summary>
    ''' <value> The part number. </value>
    Property PartNumber As String

    ''' <summary> Gets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Property Timestamp As DateTime

End Interface

''' <summary> A part builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class CincoPartBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(CincoPartBuilder._TableName) Then
                CincoPartBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(CincoPartNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return CincoPartBuilder._TableName
        End Get
    End Property

    ''' <summary> Gets the name of the label index. </summary>
    ''' <value> The name of the label index. </value>
    Private Shared ReadOnly Property LabelIndexName As String
        Get
            Return $"UQ_{CincoPartBuilder.TableName}"
        End Get
    End Property

    ''' <summary> Gets the name of the sq lite label index. </summary>
    ''' <value> The name of the sq lite label index. </value>
    Private Shared ReadOnly Property SQLiteLabelIndexName As String
        Get
            Return $"UQ_{CincoPartBuilder.TableName}_{NameOf(CincoPartNub.PartNumber)}"
        End Get
    End Property

    ''' <summary> The using unique label. </summary>
    Private Shared _UsingUniqueLabel As Boolean?

    ''' <summary> Indicates if the entity uses a unique Label. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Shared Function UsingUniqueLabel(ByVal connection As System.Data.IDbConnection) As Boolean
        If Not CincoPartBuilder._UsingUniqueLabel.HasValue Then
            CincoPartBuilder._UsingUniqueLabel = If(TryCast(connection, System.Data.SQLite.SQLiteConnection) IsNot Nothing,
                connection.IndexExists(CincoPartBuilder.SQLiteLabelIndexName),
                connection.IndexExists(CincoPartBuilder.LabelIndexName))
        End If
        Return CincoPartBuilder._UsingUniqueLabel.Value
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{CincoPartBuilder.TableName}] (
	[{NameOf(CincoPartNub.PartAutoId)}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
	[{NameOf(CincoPartNub.PartNumber)}] nvarchar(50) NOT NULL, 
	[{NameOf(CincoPartNub.Timestamp)}] datetime NOT NULL DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW'))); 
CREATE UNIQUE INDEX IF NOT EXISTS [{CincoPartBuilder.SQLiteLabelIndexName}] ON [{CincoPartBuilder.TableName}] ([{NameOf(CincoPartNub.PartNumber)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CincoPartBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].{CincoPartBuilder.TableName}') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{CincoPartBuilder.TableName}](
	[{NameOf(CincoPartNub.PartAutoId)}] [int] IDENTITY(1,1) NOT NULL,
	[{NameOf(CincoPartNub.PartNumber)}] [nvarchar](50) NOT NULL,
	[{NameOf(CincoPartNub.Timestamp)}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
 CONSTRAINT [PK_Part] PRIMARY KEY CLUSTERED 
([{NameOf(CincoPartNub.PartAutoId)}] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]
END; 
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].{CincoPartBuilder.TableName}') AND name = N'{CincoPartBuilder.LabelIndexName}')
CREATE UNIQUE NONCLUSTERED INDEX [{CincoPartBuilder.LabelIndexName}] ON [dbo].[{CincoPartBuilder.TableName}] ([{NameOf(CincoPartNub.PartNumber)}] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
ON [PRIMARY]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CincoPartBuilder.TableName
    End Function

End Class

''' <summary>
''' The part table implementation based on the <see cref="ICincoPart">part interface</see>.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/18/2018 </para><para>
''' David, 9/17/2018 </para>
''' </remarks>
<Table("Part")>
Public Class CincoPartNub
    Inherits EntityNubBase(Of ICincoPart)
    Implements ICincoPart

    #Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICincoPart
        Return New CincoPartNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICincoPart
        Dim destination As ICincoPart = Me.CreateNew
        CincoPartNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICincoPart)
        CincoPartNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As ICincoPart, ByVal destination As ICincoPart)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.PartAutoId = source.PartAutoId
        destination.PartNumber = source.PartNumber
        destination.Timestamp = source.Timestamp
    End Sub

    #End Region

    #Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, ICincoPart))
        Throw New NotImplementedException()
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As ICincoPart) As Boolean
        Return other IsNot Nothing AndAlso CincoPartNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As ICincoPart, ByVal right As ICincoPart) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.PartAutoId, right.PartAutoId)
            result = result AndAlso String.Equals(left.PartNumber, right.PartNumber)
            result = result AndAlso Date.Equals(left.Timestamp, right.Timestamp)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.PartAutoId Xor Me.PartNumber.GetHashCode() Xor Me.Timestamp.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the part. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The identifier of the part. </value>
    <Key>
    Public Property PartAutoId As Integer Implements ICincoPart.PartAutoId

    ''' <summary> Gets or sets the part number. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The part number. </value>
    Public Property PartNumber As String Implements ICincoPart.PartNumber

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Public Property Timestamp As DateTime Implements ICincoPart.Timestamp

    #End Region

End Class

''' <summary> The part entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/14/2018 </para>
''' </remarks>
Public Class CincoPartEntity
    Inherits EntityBase(Of ICincoPart, CincoPartNub)
    Implements ICincoPart

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New CincoPartNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As ICincoPart)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ICincoPart, ByVal store As ICincoPart)
        MyBase.New(New CincoPartNub, cache, store)
        Me.UsingNativeTracking = String.Equals(CincoPartBuilder.TableName, NameOf(ICincoPart).TrimStart("I"c))
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICincoPart
        Return New CincoPartNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICincoPart
        Dim destination As ICincoPart = Me.CreateNew
        CincoPartNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICincoPart)
        CincoPartNub.Copy(value, Me)
    End Sub

    #End Region

    #Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ICincoPart)
        ' first make the copy to notify of any property change.
        CincoPartNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

    #End Region

    #Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        the Meter Model table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of ICincoPart)(key), connection.Get(Of CincoPartNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PartAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PartNumber)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 4/28/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal partNumber As String) As Boolean
        Me.ClearStore()
        Dim nub As CincoPartNub = CincoPartEntity.FetchNubs(connection, partNumber).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 5/16/2020. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ICincoPart) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If CincoPartEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.PartNumber) Then
            ' update the existing record from the specified entity.
            entity.PartAutoId = Me.PartAutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of CincoPartNub)(New CincoPartNub With {.PartAutoId = key})
    End Function

    #End Region

    #Region " ENTITIES "

    ''' <summary> Gets or sets the Part entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Part entities. </value>
    Public ReadOnly Property Parts As IEnumerable(Of CincoPartEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of CincoPartEntity)
        Return If(usingNativeTracking, CincoPartEntity.Populate(connection.GetAll(Of ICincoPart)), CincoPartEntity.Populate(connection.GetAll(Of CincoPartNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Parts = CincoPartEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(CincoPartEntity.Parts))
        Return If(Me.Parts?.Any, Me.Parts.Count, 0)
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 5/2/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Entities As IEnumerable(Of PartEntity))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection) As (Success As Boolean, Details As String, Entities As IEnumerable(Of CincoPartEntity))
        Dim activity As String = String.Empty
        Try
            Dim entity As New CincoPartEntity()
            activity = "fetching all Parts"
            ' fetch all Parts
            entity.FetchAllEntities(connection)
            Return (True, String.Empty, entity.Parts)
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}", New List(Of CincoPartEntity))
        End Try
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The entity nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal nubs As IEnumerable(Of CincoPartNub)) As IEnumerable(Of CincoPartEntity)
        Dim l As New List(Of CincoPartEntity)
        If nubs?.Any Then
            For Each nub As CincoPartNub In nubs
                l.Add(New CincoPartEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The entity interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal interfaces As IEnumerable(Of ICincoPart)) As IEnumerable(Of CincoPartEntity)
        Dim l As New List(Of CincoPartEntity)
        If interfaces?.Any Then
            Dim nub As New CincoPartNub
            For Each iFace As ICincoPart In interfaces
                nub.CopyFrom(iFace)
                l.Add(New CincoPartEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    #End Region

    #Region " FIND  "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partNumber As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{CincoPartBuilder.TableName}]
WHERE {NameOf(CincoPartNub.PartNumber)} = @partNumber", New With {partNumber})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal partNumber As String) As IEnumerable(Of CincoPartNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{CincoPartBuilder.TableName}]
WHERE {NameOf(CincoPartNub.PartNumber)} = @partNumber", New With {partNumber})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of CincoPartNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the part exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal partNumber As String) As Boolean
        Return 1 = CincoPartEntity.CountEntities(connection, partNumber)
    End Function

    #End Region

    #Region " RELATIONS: LOTS "

    ''' <summary> Gets or sets the lot entities. </summary>
    ''' <value> The lot entities. </value>
    Public ReadOnly Property Lots As IEnumerable(Of CincoLotEntity)

    ''' <summary> Fetches the lots. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The lots. </returns>
    Public Function FetchLots(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Lots = CincoLotEntity.FetchEntities(connection, Me.PartAutoId)
        Me.NotifyPropertyChanged(NameOf(CincoPartEntity.Lots))
        Return If(Me.Lots?.Any, Me.Lots.Count, 0)
    End Function

    ''' <summary> Fetches the lots. </summary>
    ''' <remarks>
    ''' https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
    ''' https://dapper-tutorial.net/querymultiple.
    ''' </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> The identifier of the part. </param>
    ''' <returns> The lots. </returns>
    Public Function FetchLots(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.AppendLine($"select * from [{CincoPartBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(CincoPartNub.PartAutoId)} = @Id; ")
        queryBuilder.AppendLine($"select * from [{CincoLotBuilder.TableName}]")
        queryBuilder.AppendLine($" where {NameOf(CincoLotNub.PartAutoId)} = @Id; ")
        Dim gridReader As SqlMapper.GridReader = connection.QueryMultiple(queryBuilder.ToString, New With {.Id = partAutoId})
        Dim entity As ICincoPart = gridReader.ReadSingle(Of CincoPartNub)()
        If entity Is Nothing Then
            Me._Lots = New List(Of CincoLotEntity)
        Else
            If Not CincoPartNub.AreEqual(entity, Me.ICache) Then
                Me.UpdateCache(entity)
                Me.UpdateStore()
            End If
            Me._Lots = CincoLotEntity.Populate(gridReader.Read(Of CincoLotNub))
        End If
        Return If(Me.Lots?.Any, Me.Lots.Count, 0)
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the part. </summary>
    ''' <value> The identifier of the part. </value>
    Public Property PartAutoId As Integer Implements ICincoPart.PartAutoId
        Get
            Return Me.ICache.PartAutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PartAutoId, value) Then
                Me.ICache.PartAutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the part number. </summary>
    ''' <value> The part number. </value>
    Public Property PartNumber As String Implements ICincoPart.PartNumber
        Get
            Return Me.ICache.PartNumber
        End Get
        Set(value As String)
            If Not String.Equals(Me.PartNumber, value) Then
                Me.ICache.PartNumber = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements ICincoPart.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTime.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    #End Region

    #Region " FIELDS: CUSTOM "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    ''' <summary> The timestamp caption format. </summary>
    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

    #End Region

End Class

