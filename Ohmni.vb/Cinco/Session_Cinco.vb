
Partial Public Class CincoSessionEntity

    #Region " BUILD "

    ''' <summary> Inserts test level values described for <see cref="CincoTestLevel"/>. </summary>
    ''' <remarks> David, 3/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function InsertTestLevelValues(ByVal connection As System.Data.IDbConnection) As Integer
        Return OhmniTestLevelBuilder.InsertIgnoreValues(connection, GetType(CincoTestLevel), Array.Empty(Of Integer)())
    End Function

    #End Region

    #Region " SHARED "

    ''' <summary> Parse test level. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value">     True to show or False to hide the control. </param>
    ''' <param name="[default]"> The default value. </param>
    ''' <returns> A TestLevel. </returns>
    Public Shared Function ParseTestLevel(value As String, ByVal [default] As CincoTestLevel) As CincoTestLevel
        Dim tl As CincoTestLevel = [default]
        If Not [Enum].TryParse(value, tl) Then tl = [default]
        Return tl
    End Function

    #End Region

    #Region " FIELDS: CUSTOM "

    ''' <summary> Gets or sets the test level. </summary>
    ''' <value> The test level. </value>
    Public Property TestLevel As CincoTestLevel
        Get
            Return CType(Me.TestLevelId, CincoTestLevel)
        End Get
        Set(value As CincoTestLevel)
            If Not Integer.Equals(Me.TestLevel, value) Then
                Me.TestLevelId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    #End Region

End Class

''' <summary> Values that represent Ohmni Cinco test levels. </summary>
''' <remarks> David, 2020-10-02. </remarks>
<Flags>
Public Enum CincoTestLevel

    ''' <summary> . </summary>
    <ComponentModel.Description("Normal")> Normal = 0

    ''' <summary> . </summary>
    <ComponentModel.Description("Four Point Probe")> FourPointProbe = 1

    ''' <summary> . </summary>
    <ComponentModel.Description("TaN Voltage")> Voltage = 2
End Enum

