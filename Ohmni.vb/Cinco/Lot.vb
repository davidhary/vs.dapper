Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary>
''' Interface for the Lot nub and entity. Includes the fields as kept in the data table. Allows
''' tracking of property changes.
''' </summary>
''' <remarks>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value. <para>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface ICincoLot

    ''' <summary> Gets the identifier of the lot. </summary>
    ''' <value> The identifier of the lot. </value>
    <Key>
    Property LotAutoId As Integer

    ''' <summary> Gets the identifier of the part. </summary>
    ''' <value> The identifier of the part. </value>
    Property PartAutoId As Integer

    ''' <summary> Gets the lot number. </summary>
    ''' <value> The lot number. </value>
    Property LotNumber As String

    ''' <summary> Gets the part number. </summary>
    ''' <value> The part number. </value>
    Property PartNumber As String

    ''' <summary> Gets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Property Timestamp As DateTime

End Interface

''' <summary> A lot builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class CincoLotBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(CincoLotBuilder._TableName) Then
                CincoLotBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(CincoLotNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks>
    ''' Uses the compact UNIX epoch time storage, which counts milliseconds relative to 1/1/1970.
    ''' </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{CincoLotBuilder.TableName}] (
	[{NameOf(CincoLotNub.LotAutoId)}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
	[{NameOf(CincoLotNub.PartAutoId)}] integer NOT NULL, 
	[{NameOf(CincoLotNub.LotNumber)}] nvarchar(50) NOT NULL, 
	[{NameOf(CincoLotNub.PartNumber)}] nvarchar(50) NOT NULL, 
	[{NameOf(CincoLotNub.Timestamp)}] datetime NOT NULL DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')),
FOREIGN KEY ([{NameOf(CincoLotNub.PartAutoId)}]) REFERENCES [{CincoPartBuilder.TableName}] ([{NameOf(CincoLotNub.PartAutoId)}]) ON UPDATE CASCADE ON DELETE CASCADE); 
CREATE UNIQUE INDEX IF NOT EXISTS [UQ_{CincoLotBuilder.TableName}] ON [{CincoLotBuilder.TableName}] ([{NameOf(CincoLotNub.LotNumber)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CincoLotBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].{CincoLotBuilder.TableName}') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{CincoLotBuilder.TableName}](
	[{NameOf(CincoLotNub.LotAutoId)}] [int] IDENTITY(1,1) NOT NULL,
	[{NameOf(CincoLotNub.PartAutoId)}] [int] NOT NULL,
	[{NameOf(CincoLotNub.LotNumber)}] [nvarchar](50) NOT NULL,
	[{NameOf(CincoLotNub.PartNumber)}] [nvarchar](50) NOT NULL,
	[{NameOf(CincoLotNub.Timestamp)}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
 CONSTRAINT [PK_{CincoLotBuilder.TableName}] PRIMARY KEY CLUSTERED 
([{NameOf(CincoLotNub.LotAutoId)}] ASC) 
 WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END; 

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].{CincoLotBuilder.TableName}') AND name = N'UQ_{CincoLotBuilder.TableName}')
CREATE UNIQUE NONCLUSTERED INDEX [UQ_{CincoLotBuilder.TableName}] ON [dbo].[{CincoLotBuilder.TableName}] ([{NameOf(CincoLotNub.LotNumber)}] ASC) 
 WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; 

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CincoLotBuilder.TableName}_{CincoPartBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{CincoLotBuilder.TableName}'))
ALTER TABLE [dbo].[{CincoLotBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{CincoLotBuilder.TableName}_{CincoPartBuilder.TableName}] FOREIGN KEY([{NameOf(CincoLotNub.PartAutoId)}])
REFERENCES [dbo].[{CincoPartBuilder.TableName}] ([{NameOf(CincoLotNub.PartAutoId)}])
ON UPDATE CASCADE ON DELETE CASCADE; 

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{CincoLotBuilder.TableName}_{CincoPartBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{CincoLotBuilder.TableName}'))
ALTER TABLE [dbo].[{CincoLotBuilder.TableName}] CHECK CONSTRAINT [FK_{CincoLotBuilder.TableName}_{CincoPartBuilder.TableName}]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return CincoLotBuilder.TableName
    End Function

End Class

''' <summary> Implements the Lot table <see cref="ICincoLot">interface</see>. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/18/2018 </para>
''' </remarks>
<Table("Lot")>
Public Class CincoLotNub
    Inherits EntityNubBase(Of ICincoLot)
    Implements ICincoLot

    #Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICincoLot
        Return New CincoLotNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICincoLot
        Dim destination As ICincoLot = Me.CreateNew
        CincoLotNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICincoLot)
        CincoLotNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As ICincoLot, ByVal destination As ICincoLot)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.LotAutoId = source.LotAutoId
        destination.LotNumber = source.LotNumber
        destination.PartAutoId = source.PartAutoId
        destination.PartNumber = source.PartNumber
        destination.Timestamp = source.Timestamp
    End Sub

    #End Region

    #Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, ICincoLot))
        Throw New NotImplementedException()
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As ICincoLot) As Boolean
        Return other IsNot Nothing AndAlso CincoLotNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As ICincoLot, ByVal right As ICincoLot) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.LotAutoId, right.LotAutoId)
            result = result AndAlso String.Equals(left.LotNumber, right.LotNumber)
            result = result AndAlso Integer.Equals(left.PartAutoId, right.PartAutoId)
            result = result AndAlso String.Equals(left.PartNumber, right.PartNumber)
            result = result AndAlso Date.Equals(left.Timestamp, right.Timestamp)
            Return result
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.LotAutoId Xor Me.LotNumber.GetHashCode() Xor Me.PartAutoId Xor Me.PartNumber.GetHashCode() Xor Me.Timestamp.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the lot. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The identifier of the lot. </value>
    <Key>
    Public Property LotAutoId As Integer Implements ICincoLot.LotAutoId

    ''' <summary> Gets or sets the identifier of the part. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The identifier of the part. </value>
    Public Property PartAutoId As Integer Implements ICincoLot.PartAutoId

    ''' <summary> Gets or sets the lot number. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The lot number. </value>
    Public Property LotNumber As String Implements ICincoLot.LotNumber

    ''' <summary> Gets or sets the part number. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The part number. </value>
    Public Property PartNumber As String Implements ICincoLot.PartNumber

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The UTC timestamp. </value>
    <Computed()>
    Public Property Timestamp As DateTime Implements ICincoLot.Timestamp

    #End Region

End Class

''' <summary> The Lot Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/18/2018 </para>
''' </remarks>
Public Class CincoLotEntity
    Inherits EntityBase(Of ICincoLot, CincoLotNub)
    Implements ICincoLot

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New CincoLotNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As ICincoLot)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As ICincoLot, ByVal store As ICincoLot)
        MyBase.New(New CincoLotNub, cache, store)
        Me.UsingNativeTracking = String.Equals(CincoLotBuilder.TableName, NameOf(ICincoLot).TrimStart("I"c))
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As ICincoLot
        Return New CincoLotNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As ICincoLot
        Dim destination As ICincoLot = Me.CreateNew
        CincoLotNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As ICincoLot)
        CincoLotNub.Copy(value, Me)
    End Sub

    #End Region

    #Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As ICincoLot)
        ' first make the copy to notify of any property change.
        CincoLotNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

    #End Region

    #Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        the Meter Model table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of ICincoLot)(key), connection.Get(Of CincoLotNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.LotAutoId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.LotNumber)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 4/28/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The Lot number. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As Boolean
        Me.ClearStore()
        Dim nub As CincoLotNub = CincoLotEntity.FetchNubs(connection, lotNumber).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary>
    ''' Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
    ''' using given connection thus preserving tracking. Fetches the stored entity to update the
    ''' computed value.
    ''' </summary>
    ''' <remarks> David, 4/30/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Insert(connection As System.Data.IDbConnection) As Boolean
        MyBase.Insert(connection)
        Return Me.FetchUsingKey(connection)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As ICincoLot) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If CincoLotEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.LotNumber) Then
            ' update the existing record from the specified entity.
            entity.LotAutoId = Me.LotAutoId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of CincoLotNub)(New CincoLotNub With {.LotAutoId = key})
    End Function

    #End Region

    #Region " ENTITIES "

    ''' <summary> Gets or sets the Lot entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Lot entities. </value>
    Public ReadOnly Property Lots As IEnumerable(Of CincoLotEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of CincoLotEntity)
        Return If(usingNativeTracking, CincoLotEntity.Populate(connection.GetAll(Of ICincoLot)), CincoLotEntity.Populate(connection.GetAll(Of CincoLotNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._Lots = CincoLotEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(CincoLotEntity.Lots))
        Return If(Me.Lots?.Any, Me.Lots.Count, 0)
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The entity nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal nubs As IEnumerable(Of CincoLotNub)) As IEnumerable(Of CincoLotEntity)
        Dim l As New List(Of CincoLotEntity)
        If nubs?.Any Then
            For Each nub As CincoLotNub In nubs
                l.Add(New CincoLotEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The entity interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Overloads Shared Function Populate(ByVal interfaces As IEnumerable(Of ICincoLot)) As IEnumerable(Of CincoLotEntity)
        Dim l As New List(Of CincoLotEntity)
        If interfaces?.Any Then
            Dim nub As New CincoLotNub
            For Each iFace As ICincoLot In interfaces
                nub.CopyFrom(iFace)
                l.Add(New CincoLotEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    ''' <summary> Count lots. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifier for the part. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{CincoLotBuilder.TableName}]
WHERE {NameOf(CincoLotNub.PartAutoId)} = @Id", New With {Key .Id = partAutoId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count lots. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountLots(ByVal connection As System.Data.IDbConnection, ByVal partNumber As String) As Integer
        #pragma warning enable BC30269 ' 'Public Shared Function CountEntities(connection As System.Data.IDbConnection, partNumber As String) As Integer' has multiple definitions with identical signatures.
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{CincoLotBuilder.TableName}]
WHERE {NameOf(CincoLotNub.PartNumber)} = @partNumber", New With {partNumber})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> Identifier for the part. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of CincoLotEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{CincoLotBuilder.TableName}]
WHERE {NameOf(CincoLotNub.PartAutoId)} = @Id", New With {Key .Id = partAutoId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return CincoLotEntity.Populate(connection.Query(Of CincoLotNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Fetches the entities for the specified part number. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partNumber"> The part number. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partNumber As String) As IEnumerable(Of CincoLotEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{CincoLotBuilder.TableName}]
WHERE {NameOf(CincoLotNub.PartNumber)} = @partNumber", New With {partNumber})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return CincoLotEntity.Populate(connection.Query(Of CincoLotNub)(selector.RawSql, selector.Parameters))
    End Function

    #End Region

    #Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{CincoLotBuilder.TableName}] /**where**/")
        
        sqlBuilder.Where($"{NameOf(CincoLotNub.LotNumber)} = @lotNumber", New With {lotNumber})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntitiesAlternative(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As Integer
        Dim selectSql As New System.Text.StringBuilder($"SELECT * FROM [{CincoLotBuilder.TableName}]")
        selectSql.Append($"WHERE (@{NameOf(CincoLotNub.LotNumber)} IS NULL OR {NameOf(CincoLotNub.LotNumber)} = @LotNumber)")
        selectSql.Append("; ")
        
        Return connection.ExecuteScalar(Of Integer)(selectSql.ToString, New With {lotNumber})
        
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As IEnumerable(Of CincoLotNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{CincoLotBuilder.TableName}] /**where**/")
        
        sqlBuilder.Where($"{NameOf(CincoLotNub.LotNumber)} = @lotNumber", New With {lotNumber})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of CincoLotNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the lot exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="lotNumber">  The lot number. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal lotNumber As String) As Boolean
        Return 1 = CincoLotEntity.CountEntities(connection, lotNumber)
    End Function

    #End Region

    #Region " RELATIONS "

    ''' <summary> Gets or sets the part entity. </summary>
    ''' <value> The part entity. </value>
    Public ReadOnly Property PartEntity As CincoPartEntity

    ''' <summary> Fetches a part Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The part. </returns>
    Public Function FetchPartEntity(ByVal connection As System.Data.IDbConnection) As CincoPartEntity
        Dim entity As New CincoPartEntity()
        entity.FetchUsingKey(connection, Me.PartAutoId)
        Me._PartEntity = entity
        Return entity
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the lot. </summary>
    ''' <value> The identifier of the lot. </value>
    Public Property LotAutoId As Integer Implements ICincoLot.LotAutoId
        Get
            Return Me.ICache.LotAutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.LotAutoId, value) Then
                Me.ICache.LotAutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the part. </summary>
    ''' <value> The identifier of the part. </value>
    Public Property PartAutoId As Integer Implements ICincoLot.PartAutoId
        Get
            Return Me.ICache.PartAutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PartAutoId, value) Then
                Me.ICache.PartAutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the lot number. </summary>
    ''' <value> The lot number. </value>
    Public Property LotNumber As String Implements ICincoLot.LotNumber
        Get
            Return Me.ICache.LotNumber
        End Get
        Set(value As String)
            If Not String.Equals(Me.LotNumber, value) Then
                Me.ICache.LotNumber = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the part number. </summary>
    ''' <value> The part number. </value>
    Public Property PartNumber As String Implements ICincoLot.PartNumber
        Get
            Return Me.ICache.PartNumber
        End Get
        Set(value As String)
            If Not String.Equals(Me.PartNumber, value) Then
                Me.ICache.PartNumber = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
    ''' <remarks>
    ''' Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
    ''' convert to local time.
    ''' </remarks>
    ''' <value> The UTC timestamp. </value>
    Public Property Timestamp As DateTime Implements ICincoLot.Timestamp
        Get
            Return Me.ICache.Timestamp
        End Get
        Set(value As DateTime)
            If Not DateTimeOffset.Equals(Me.Timestamp, value) Then
                Me.ICache.Timestamp = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    #End Region

    #Region " TIMESTAMP "

    ''' <summary> Gets or sets the default timestamp caption format. </summary>
    ''' <value> The default timestamp caption format. </value>
    Public Shared Property DefaultTimestampCaptionFormat As String = "YYYYMMDD.HHmmss.f"

    ''' <summary> The timestamp caption format. </summary>
    Private _TimestampCaptionFormat As String

    ''' <summary> Gets or sets the timestamp caption format. </summary>
    ''' <value> The timestamp caption format. </value>
    Public Property TimestampCaptionFormat As String
        Get
            Return Me._TimestampCaptionFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.TimestampCaptionFormat, value) Then
                Me._TimestampCaptionFormat = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the timestamp caption. </summary>
    ''' <value> The timestamp caption. </value>
    Public ReadOnly Property TimestampCaption As String
        Get
            Return Me.Timestamp.ToString(Me.TimestampCaptionFormat)
        End Get
    End Property

    #End Region

End Class

