Imports System.ComponentModel
Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions
Imports isr.Dapper.Entity.EntityExtensions

''' <summary>
''' Interface for the Ohmni Attribute Type nub and entity. Includes the fields as kept in the
''' data table. Allows tracking of property changes.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IOhmniAttributeType

    ''' <summary> Gets the identifier of the Attribute type. </summary>
    ''' <value> The identifier of the Attribute type. </value>
    <ExplicitKey>
    Property AttributeTypeId As Integer

    ''' <summary> Gets the name. </summary>
    ''' <value> The name. </value>
    Property Name As String

    ''' <summary> Gets the description. </summary>
    ''' <value> The description. </value>
    Property Description As String

End Interface

''' <summary> The Ohmni Attribute Type builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class OhmniAttributeTypeBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(OhmniAttributeTypeBuilder._TableName) Then
                OhmniAttributeTypeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(OhmniAttributeTypeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return OhmniAttributeTypeBuilder._TableName
        End Get
    End Property

    ''' <summary>
    ''' Inserts or ignores the values described by the <see cref="AttributeType"/> enumeration type.
    ''' </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of inserted records or total number of records. </returns>
    Public Shared Function InsertIgnoreValues(ByVal connection As System.Data.IDbConnection) As Integer
        Return OhmniAttributeTypeBuilder.InsertIgnoreValues(connection, GetType(AttributeType), New Integer() {AttributeType.None})
    End Function

    ''' <summary> Inserts or ignores the values described by the enumeration type. </summary>
    ''' <remarks> David, 5/14/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="type">       The enumeration type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of inserted records or total number of records. </returns>
    Public Shared Function InsertIgnoreValues(ByVal connection As System.Data.IDbConnection, ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As Integer
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection.InsertIgnoreNameDescriptionRecords(OhmniAttributeTypeBuilder.TableName, GetType(IOhmniAttributeType).EnumerateEntityFieldNames(), type, excluded)
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{OhmniAttributeTypeBuilder.TableName}] (
	[{NameOf(OhmniAttributeTypeNub.AttributeTypeId)}] integer NOT NULL PRIMARY KEY, 
	[{NameOf(OhmniAttributeTypeNub.Name)}] nvarchar(50) NOT NULL, 
	[{NameOf(OhmniAttributeTypeNub.Description)}] nvarchar(250) NOT NULL); 
CREATE UNIQUE INDEX IF NOT EXISTS [UQ_{OhmniAttributeTypeBuilder.TableName}] ON [{OhmniAttributeTypeBuilder.TableName}] ([{NameOf(OhmniAttributeTypeNub.Name)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmniAttributeTypeBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{OhmniAttributeTypeBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{OhmniAttributeTypeBuilder.TableName}](
	[{NameOf(OhmniAttributeTypeNub.AttributeTypeId)}] [int] NOT NULL,
	[{NameOf(OhmniAttributeTypeNub.Name)}] [nvarchar](50) NOT NULL,
	[{NameOf(OhmniAttributeTypeNub.Description)}] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_{OhmniAttributeTypeBuilder.TableName}] PRIMARY KEY CLUSTERED 
([{NameOf(OhmniAttributeTypeNub.AttributeTypeId)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END; 
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{OhmniAttributeTypeBuilder.TableName}]') AND name = N'UQ_{OhmniAttributeTypeBuilder.TableName}')
CREATE UNIQUE NONCLUSTERED INDEX [UQ_{OhmniAttributeTypeBuilder.TableName}] ON [dbo].[{OhmniAttributeTypeBuilder.TableName}] ([{NameOf(OhmniAttributeTypeNub.Name)}] ASC) 
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmniAttributeTypeBuilder.TableName
    End Function

End Class

''' <summary>
''' Implements the AttributeType table <see cref="IOhmniAttributeType">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2018 </para>
''' </remarks>
<Table("AttributeType")>
Public Class OhmniAttributeTypeNub
    Inherits EntityNubBase(Of IOhmniAttributeType)
    Implements IOhmniAttributeType

    #Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmniAttributeType
        Return New OhmniAttributeTypeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmniAttributeType
        Dim destination As IOhmniAttributeType = Me.CreateNew
        OhmniAttributeTypeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmniAttributeType)
        OhmniAttributeTypeNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IOhmniAttributeType, ByVal destination As IOhmniAttributeType)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.AttributeTypeId = source.AttributeTypeId
        destination.Name = source.Name
        destination.Description = source.Description
    End Sub

    #End Region

    #Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IOhmniAttributeType))
        Throw New NotImplementedException()
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IOhmniAttributeType) As Boolean
        Return other IsNot Nothing AndAlso OhmniAttributeTypeNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IOhmniAttributeType, ByVal right As IOhmniAttributeType) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.AttributeTypeId, right.AttributeTypeId)
            result = result AndAlso String.Equals(left.Name, right.Name)
            result = result AndAlso String.Equals(left.Description, right.Description)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.AttributeTypeId Xor Me.Name.GetHashCode() Xor Me.Description.GetHashCode.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Attribute type. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The identifier of the Attribute type. </value>
    <ExplicitKey>
    Public Property AttributeTypeId As Integer Implements IOhmniAttributeType.AttributeTypeId

    ''' <summary> Gets or sets the name. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name. </value>
    Public Property Name As String Implements IOhmniAttributeType.Name

    ''' <summary> Gets or sets the description. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The description. </value>
    Public Property Description As String Implements IOhmniAttributeType.Description

    #End Region

End Class

''' <summary> The AttributeType Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2018 </para>
''' </remarks>
Public Class OhmniAttributeTypeEntity
    Inherits EntityBase(Of IOhmniAttributeType, OhmniAttributeTypeNub)
    Implements IOhmniAttributeType

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New OhmniAttributeTypeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As IOhmniAttributeType)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOhmniAttributeType, ByVal store As IOhmniAttributeType)
        MyBase.New(New OhmniAttributeTypeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(OhmniAttributeTypeBuilder.TableName, NameOf(IOhmniAttributeType).TrimStart("I"c))
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmniAttributeType
        Return New OhmniAttributeTypeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmniAttributeType
        Dim destination As IOhmniAttributeType = Me.CreateNew
        OhmniAttributeTypeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmniAttributeType)
        OhmniAttributeTypeNub.Copy(value, Me)
    End Sub

    #End Region

    #Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOhmniAttributeType)
        ' first make the copy to notify of any property change.
        OhmniAttributeTypeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

    #End Region

    #Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        the Meter Model table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IOhmniAttributeType)(key), connection.Get(Of OhmniAttributeTypeNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.AttributeTypeId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Name)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 4/28/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="name">       Name of the Attribute Type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal name As String) As Boolean
        Me.ClearStore()
        Dim nub As OhmniAttributeTypeNub = OhmniAttributeTypeEntity.FetchNubs(connection, name).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOhmniAttributeType) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If OhmniAttributeTypeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Name) Then
            ' update the existing record from the specified entity.
            entity.AttributeTypeId = Me.AttributeTypeId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of OhmniAttributeTypeNub)(New OhmniAttributeTypeNub With {.AttributeTypeId = key})
    End Function

    #End Region

    #Region " ENTITIES "

    ''' <summary> Gets or sets the Attribute Type entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Attribute Type entities. </value>
    Public ReadOnly Property AttributeTypes As IEnumerable(Of OhmniAttributeTypeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of OhmniAttributeTypeEntity)
        Return If(usingNativeTracking, OhmniAttributeTypeEntity.Populate(connection.GetAll(Of IOhmniAttributeType)), OhmniAttributeTypeEntity.Populate(connection.GetAll(Of OhmniAttributeTypeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._AttributeTypes = OhmniAttributeTypeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(OhmniAttributeTypeEntity.AttributeTypes))
        Return If(Me.AttributeTypes?.Any, Me.AttributeTypes.Count, 0)
    End Function

    ''' <summary> Populates a list of Attribute Type entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The entity nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of OhmniAttributeTypeNub)) As IEnumerable(Of OhmniAttributeTypeEntity)
        Dim l As New List(Of OhmniAttributeTypeEntity)
        If nubs?.Any Then
            For Each nub As OhmniAttributeTypeNub In nubs
                l.Add(New OhmniAttributeTypeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Attribute type entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The entity interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOhmniAttributeType)) As IEnumerable(Of OhmniAttributeTypeEntity)
        Dim l As New List(Of OhmniAttributeTypeEntity)
        If interfaces?.Any Then
            Dim nub As New OhmniAttributeTypeNub
            For Each iFace As IOhmniAttributeType In interfaces
                nub.CopyFrom(iFace)
                l.Add(New OhmniAttributeTypeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    #End Region

    #Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="name">       The <see cref="IOhmniAttributeType"/> name. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal name As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{OhmniAttributeTypeBuilder.TableName}]
WHERE {NameOf(OhmniAttributeTypeNub.Name)} = @name", New With {name})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="name">       The <see cref="IOhmniAttributeType"/> name. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal name As String) As IEnumerable(Of OhmniAttributeTypeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{OhmniAttributeTypeBuilder.TableName}]
WHERE {NameOf(OhmniAttributeTypeNub.Name)} = @name", New With {name})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of OhmniAttributeTypeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the AttributeType exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="name">       The <see cref="IOhmniAttributeType"/> name. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal name As String) As Boolean
        Return 1 = OhmniAttributeTypeEntity.CountEntities(connection, name)
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Attribute type. </summary>
    ''' <value> The identifier of the Attribute type. </value>
    Public Property AttributeTypeId As Integer Implements IOhmniAttributeType.AttributeTypeId
        Get
            Return Me.ICache.AttributeTypeId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AttributeTypeId, value) Then
                Me.ICache.AttributeTypeId = value
                Me.NotifyFieldChanged()
                Me.NotifyFieldChanged(NameOf(OhmniAttributeTypeEntity.AttributeType))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the AttributeType. </summary>
    ''' <value> The AttributeType. </value>
    Public Property AttributeType As AttributeType
        Get
            Return CType(Me.AttributeTypeId, AttributeType)
        End Get
        Set(value As AttributeType)
            If Not Integer.Equals(Me.AttributeType, value) Then
                Me.AttributeTypeId = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the name. </summary>
    ''' <value> The name. </value>
    Public Property Name As String Implements IOhmniAttributeType.Name
        Get
            Return Me.ICache.Name
        End Get
        Set(value As String)
            If Not String.Equals(Me.Name, value) Then
                Me.ICache.Name = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Description. </summary>
    ''' <value> The Description. </value>
    Public Property Description As String Implements IOhmniAttributeType.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    #End Region

End Class

''' <summary> Values that represent attribute types. </summary>
''' <remarks> David, 2020-10-02. </remarks>
Public Enum AttributeType

    ''' <summary> . </summary>
    <Description("Attribute Type")> None = 0

    ''' <summary> . </summary>
    <Description("Nominal Value")> NominalValue = 1

    ''' <summary> . </summary>
    <Description("Tolerance")> Tolerance = 2

    ''' <summary> . </summary>
    <Description("Tracking Tolerance")> TrackingTolerance = 3

    ''' <summary> . </summary>
    <Description("TCR")> Tcr = 4

    ''' <summary> . </summary>
    <Description("Tracking TCR")> TrackingTcr = 5
End Enum
