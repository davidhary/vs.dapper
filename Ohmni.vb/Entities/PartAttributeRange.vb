Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary>
''' Interface for the <see cref="OhmniPartAttributeRangeNub">Part Attribute Range nub</see> and
''' <see cref="OhmniPartAttributeRangeEntity">entity</see>. Includes the fields as kept in the
''' data table. Allows tracking of property changes.
''' </summary>
''' <remarks>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value. <para>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IOhmniPartAttributeRange

    ''' <summary> Gets the identifier of the Part. </summary>
    ''' <value> The identifier of the Part. </value>
    <ExplicitKey>
    Property PartAutoId As Integer

    ''' <summary> Gets the identifier of the Attribute type. </summary>
    ''' <value> The identifier of the structure type. </value>
    <ExplicitKey>
    Property AttributeTypeId As Integer

    ''' <summary> Gets the minimum Value. </summary>
    ''' <value> The minimum Value. </value>
    Property Min As Double

    ''' <summary> Gets the maximum Value. </summary>
    ''' <value> The minimum Value. </value>
    Property Max As Double

End Interface

''' <summary> A part attribute range builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class OhmniPartAttributeRangeBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(OhmniPartAttributeRangeBuilder._TableName) Then
                OhmniPartAttributeRangeBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(OhmniPartAttributeRangeNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return OhmniPartAttributeRangeBuilder._TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the Attribute Type table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="NotImplementedException">   Thrown when the requested operation is
    '''                                              unimplemented. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the AttributeType table. </value>
    Public Shared Property AttributeTypeTableName As String = OhmniAttributeTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the Attribute Type table foreign key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="NotImplementedException">   Thrown when the requested operation is
    '''                                              unimplemented. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the AttributeType table foreign key. </value>
    Public Shared Property AttributeTypeForeignKeyName As String = NameOf(IOhmniAttributeType.AttributeTypeId)

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{OhmniPartAttributeRangeBuilder.TableName}] (
[{NameOf(OhmniPartAttributeRangeNub.PartAutoId)}] integer NOT NULL, 
[{NameOf(OhmniPartAttributeRangeNub.AttributeTypeId)}] integer NOT NULL, 
[{NameOf(OhmniPartAttributeRangeNub.Min)}] float NOT NULL, 
[{NameOf(OhmniPartAttributeRangeNub.Max)}] float NOT NULL, 
CONSTRAINT [sqlite_autoindex_{OhmniPartAttributeRangeBuilder.TableName}_1] PRIMARY KEY ([{NameOf(OhmniPartAttributeRangeNub.PartAutoId)}], [{NameOf(OhmniPartAttributeRangeNub.AttributeTypeId)}]),
FOREIGN KEY ([{NameOf(OhmniPartAttributeRangeNub.PartAutoId)}]) REFERENCES [{CincoPartBuilder.TableName}] ([{NameOf(CincoPartNub.PartAutoId)}]) ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY ([{NameOf(OhmniPartAttributeRangeNub.AttributeTypeId)}]) REFERENCES [{OhmniPartAttributeRangeBuilder.AttributeTypeTableName}] ([{OhmniPartAttributeRangeBuilder.AttributeTypeForeignKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE);
")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmniPartAttributeRangeBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{OhmniPartAttributeRangeBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{OhmniPartAttributeRangeBuilder.TableName}](
	[{NameOf(OhmniPartAttributeRangeNub.PartAutoId)}] [int] NOT NULL,
	[{NameOf(OhmniPartAttributeRangeNub.AttributeTypeId)}] [int] NOT NULL,
	[{NameOf(OhmniPartAttributeRangeNub.Min)}] [float] NOT NULL,
	[{NameOf(OhmniPartAttributeRangeNub.Max)}] [float] NOT NULL,
 CONSTRAINT [PK_{OhmniPartAttributeRangeBuilder.TableName}] PRIMARY KEY CLUSTERED 
([{NameOf(OhmniPartAttributeRangeNub.PartAutoId)}] ASC, [{NameOf(OhmniPartAttributeRangeNub.AttributeTypeId)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmniPartAttributeRangeBuilder.TableName}_{OhmniPartAttributeRangeBuilder.AttributeTypeTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmniPartAttributeRangeBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmniPartAttributeRangeBuilder.TableName}] WITH CHECK ADD CONSTRAINT [FK_{OhmniPartAttributeRangeBuilder.TableName}_{OhmniPartAttributeRangeBuilder.AttributeTypeTableName}] FOREIGN KEY([{NameOf(OhmniPartAttributeRangeNub.AttributeTypeId)}])
REFERENCES [dbo].[{OhmniPartAttributeRangeBuilder.AttributeTypeTableName}] ([{OhmniPartAttributeRangeBuilder.AttributeTypeForeignKeyName}])
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmniPartAttributeRangeBuilder.TableName}_{OhmniPartAttributeRangeBuilder.AttributeTypeTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmniPartAttributeRangeBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmniPartAttributeRangeBuilder.TableName}] CHECK CONSTRAINT [FK_{OhmniPartAttributeRangeBuilder.TableName}_{OhmniPartAttributeRangeBuilder.AttributeTypeTableName}]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmniPartAttributeRangeBuilder.TableName}_{CincoPartBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmniPartAttributeRangeBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmniPartAttributeRangeBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{OhmniPartAttributeRangeBuilder.TableName}_{CincoPartBuilder.TableName}] FOREIGN KEY([{NameOf(CincoPartNub.PartAutoId)}])
REFERENCES [dbo].[{CincoPartBuilder.TableName}] ([{NameOf(CincoPartNub.PartAutoId)}])
ON UPDATE CASCADE ON DELETE CASCADE 
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmniPartAttributeRangeBuilder.TableName}_{CincoPartBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmniPartAttributeRangeBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmniPartAttributeRangeBuilder.TableName}] CHECK CONSTRAINT [FK_{OhmniPartAttributeRangeBuilder.TableName}_{CincoPartBuilder.TableName}]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmniPartAttributeRangeBuilder.TableName
    End Function

End Class

''' <summary>
''' Implements the PartAttributeRange table <see cref="IOhmniPartAttributeRange">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2018 </para>
''' </remarks>
<Table("PartAttributeRange")>
Public Class OhmniPartAttributeRangeNub
    Inherits EntityNubBase(Of IOhmniPartAttributeRange)
    Implements IOhmniPartAttributeRange

    #Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmniPartAttributeRange
        Return New OhmniPartAttributeRangeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmniPartAttributeRange
        Dim destination As IOhmniPartAttributeRange = Me.CreateNew
        OhmniPartAttributeRangeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmniPartAttributeRange)
        OhmniPartAttributeRangeNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IOhmniPartAttributeRange, ByVal destination As IOhmniPartAttributeRange)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.AttributeTypeId = source.AttributeTypeId
        destination.Max = source.Max
        destination.Min = source.Min
        destination.PartAutoId = source.PartAutoId
    End Sub

    #End Region

    #Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IOhmniPartAttributeRange))
        Throw New NotImplementedException()
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IOhmniPartAttributeRange) As Boolean
        Return other IsNot Nothing AndAlso OhmniPartAttributeRangeNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IOhmniPartAttributeRange, ByVal right As IOhmniPartAttributeRange) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.AttributeTypeId, right.AttributeTypeId)
            result = result AndAlso Double.Equals(left.Max, right.Max)
            result = result AndAlso Double.Equals(left.Min, right.Min)
            result = result AndAlso Integer.Equals(left.PartAutoId, right.PartAutoId)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.AttributeTypeId Xor Me.Max.GetHashCode() Xor Me.Min.GetHashCode() Xor Me.PartAutoId
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Part. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The identifier of the Part. </value>
    <ExplicitKey>
    Public Property PartAutoId As Integer Implements IOhmniPartAttributeRange.PartAutoId

    ''' <summary> Gets or sets the identifier of the Part Attribute type. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The identifier of the Part Attribute type. </value>
    <ExplicitKey>
    Public Property AttributeTypeId As Integer Implements IOhmniPartAttributeRange.AttributeTypeId

    ''' <summary> Gets or sets the Part Attribute minimum value. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The Part Attribute minimum value. </value>
    Public Property Min As Double Implements IOhmniPartAttributeRange.Min

    ''' <summary> Gets or sets the Part Attribute Maximum value. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The Part Attribute Maximum value. </value>
    Public Property Max As Double Implements IOhmniPartAttributeRange.Max

    #End Region

End Class

''' <summary>
''' The PartAttributeRange Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2018 </para>
''' </remarks>
Public Class OhmniPartAttributeRangeEntity
    Inherits EntityBase(Of IOhmniPartAttributeRange, OhmniPartAttributeRangeNub)
    Implements IOhmniPartAttributeRange

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New OhmniPartAttributeRangeNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As IOhmniPartAttributeRange)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOhmniPartAttributeRange, ByVal store As IOhmniPartAttributeRange)
        MyBase.New(New OhmniPartAttributeRangeNub, cache, store)
        Me.UsingNativeTracking = String.Equals(OhmniPartAttributeRangeBuilder.TableName, NameOf(IOhmniPartAttributeRange).TrimStart("I"c))
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmniPartAttributeRange
        Return New OhmniPartAttributeRangeNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmniPartAttributeRange
        Dim destination As IOhmniPartAttributeRange = Me.CreateNew
        OhmniPartAttributeRangeNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmniPartAttributeRange)
        OhmniPartAttributeRangeNub.Copy(value, Me)
    End Sub

    #End Region

    #Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOhmniPartAttributeRange)
        ' first make the copy to notify of any property change.
        OhmniPartAttributeRangeNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

    #End Region

    #Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="partAutoId">      The identifier of the Part. </param>
    ''' <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal attributeTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{OhmniPartAttributeRangeBuilder.TableName}] /**where**/")
        
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeRangeNub.PartAutoId)} = @Id", New With {.Id = partAutoId})
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeRangeNub.AttributeTypeId)} = @TypeId", New With {.TypeId = attributeTypeId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of OhmniPartAttributeRangeNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PartAutoId, Me.AttributeTypeId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PartAutoId, Me.AttributeTypeId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="partAutoId">      The identifier of the Part. </param>
    ''' <param name="attributeTypeId"> The Part Attribute value. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal attributeTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As OhmniPartAttributeRangeNub = OhmniPartAttributeRangeEntity.FetchEntities(connection, partAutoId, attributeTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOhmniPartAttributeRange) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If OhmniPartAttributeRangeEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.PartAutoId, entity.AttributeTypeId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="partAutoId">      The identifier of the Part. </param>
    ''' <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal attributeTypeId As Integer) As Boolean
        Return connection.Delete(Of OhmniPartAttributeRangeNub)(New OhmniPartAttributeRangeNub With {.PartAutoId = partAutoId, .AttributeTypeId = attributeTypeId})
    End Function

    #End Region

    #Region " ENTITIES "

    ''' <summary> Gets or sets the PartAttributeRange entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The PartAttributeRange entities. </value>
    Public ReadOnly Property PartAttributeRanges As IEnumerable(Of OhmniPartAttributeRangeEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of OhmniPartAttributeRangeEntity)
        Return If(usingNativeTracking, OhmniPartAttributeRangeEntity.Populate(connection.GetAll(Of IOhmniPartAttributeRange)), OhmniPartAttributeRangeEntity.Populate(connection.GetAll(Of OhmniPartAttributeRangeNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._PartAttributeRanges = OhmniPartAttributeRangeEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(OhmniPartAttributeRangeEntity.PartAttributeRanges))
        Return If(Me.PartAttributeRanges?.Any, Me.PartAttributeRanges.Count, 0)
    End Function

    ''' <summary> Count PartAttributeRanges. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> The identifier of the Part. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{OhmniPartAttributeRangeBuilder.TableName}]
WHERE {NameOf(OhmniPartAttributeRangeNub.PartAutoId)} = @Id", New With {Key .Id = partAutoId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> The identifier of the Part. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of OhmniPartAttributeRangeEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{OhmniPartAttributeRangeBuilder.TableName}]
WHERE {NameOf(OhmniPartAttributeRangeNub.PartAutoId)} = @Id", New With {Key .Id = partAutoId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return OhmniPartAttributeRangeEntity.Populate(connection.Query(Of OhmniPartAttributeRangeNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Populates a list of Part Attribute Range entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The Part Attribute Range nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of OhmniPartAttributeRangeNub)) As IEnumerable(Of OhmniPartAttributeRangeEntity)
        Dim l As New List(Of OhmniPartAttributeRangeEntity)
        If nubs?.Any Then
            For Each nub As OhmniPartAttributeRangeNub In nubs
                l.Add(New OhmniPartAttributeRangeEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Part Attribute Range entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The Part Attribute Range interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOhmniPartAttributeRange)) As IEnumerable(Of OhmniPartAttributeRangeEntity)
        Dim l As New List(Of OhmniPartAttributeRangeEntity)
        If interfaces?.Any Then
            Dim nub As New OhmniPartAttributeRangeNub
            For Each iFace As IOhmniPartAttributeRange In interfaces
                nub.CopyFrom(iFace)
                l.Add(New OhmniPartAttributeRangeEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    #End Region

    #Region " FIND "

    ''' <summary> Count Part Attributes; Returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="partAutoId">      The identifier of the Part. </param>
    ''' <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal attributeTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{OhmniPartAttributeRangeBuilder.TableName}] /**where**/")
        
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeRangeEntity.PartAutoId)} = @Id", New With {.Id = partAutoId})
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeRangeEntity.AttributeTypeId)} = @TypeId", New With {.TypeId = attributeTypeId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Part Attributes by Part number and Part Attribute Part Attribute value;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="partAutoId">      The identifier of the Part. </param>
    ''' <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal attributeTypeId As Integer) As IEnumerable(Of OhmniPartAttributeRangeNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{OhmniPartAttributeRangeBuilder.TableName}] /**where**/")
        
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeRangeEntity.PartAutoId)} = @Id", New With {.Id = partAutoId})
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeRangeEntity.AttributeTypeId)} = @TypeId", New With {.TypeId = attributeTypeId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of OhmniPartAttributeRangeNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the PartAttributeRange exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="partAutoId">         The identifier of the Part. </param>
    ''' <param name="partAttributeRange"> The part attribute range. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal partAttributeRange As Integer) As Boolean
        Return 1 = OhmniPartAttributeRangeEntity.CountEntities(connection, partAutoId, partAttributeRange)
    End Function

    #End Region

    #Region " RELATIONS "

    ''' <summary> Gets or sets the Part entity. </summary>
    ''' <value> The Part entity. </value>
    Public ReadOnly Property PartEntity As CincoPartEntity

    ''' <summary> Fetches a Part Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchPartEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._PartEntity = New CincoPartEntity()
        Return Me.PartEntity.FetchUsingKey(connection, Me.PartAutoId)
    End Function

    ''' <summary> Gets or sets the AttributeType entity. </summary>
    ''' <value> The AttributeType entity. </value>
    Public ReadOnly Property AttributeTypeEntity As OhmniAttributeTypeEntity

    ''' <summary> Fetches a AttributeType Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchAttributeTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._AttributeTypeEntity = New OhmniAttributeTypeEntity()
        Return Me.AttributeTypeEntity.FetchUsingKey(connection, Me.AttributeTypeId)
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Part. </summary>
    ''' <value> The identifier of the Part. </value>
    Public Property PartAutoId As Integer Implements IOhmniPartAttributeRange.PartAutoId
        Get
            Return Me.ICache.PartAutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PartAutoId, value) Then
                Me.ICache.PartAutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Part Attribute type. </summary>
    ''' <value> The identifier of the Part Attribute type. </value>
    Public Property AttributeTypeId As Integer Implements IOhmniPartAttributeRange.AttributeTypeId
        Get
            Return Me.ICache.AttributeTypeId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AttributeTypeId, value) Then
                Me.ICache.AttributeTypeId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Part Attribute minimum value. </summary>
    ''' <value> The Part Attribute minimum value. </value>
    Public Property Min As Double Implements IOhmniPartAttributeRange.Min
        Get
            Return Me.ICache.Min
        End Get
        Set(value As Double)
            If Not Double.Equals(Me.Min, value) Then
                Me.ICache.Min = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Part Attribute Maximum value. </summary>
    ''' <value> The Part Attribute Maximum value. </value>
    Public Property Max As Double Implements IOhmniPartAttributeRange.Max
        Get
            Return Me.ICache.Max
        End Get
        Set(value As Double)
            If Not Double.Equals(Me.Max, value) Then
                Me.ICache.Max = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    #End Region

    #Region " FIELDS: CUSTOM "

    ''' <summary> Gets or sets the AttributeType. </summary>
    ''' <value> The AttributeType. </value>
    Public Property AttributeType As AttributeType
        Get
            Return CType(Me.AttributeTypeId, AttributeType)
        End Get
        Set(ByVal value As AttributeType)
            If Not Integer.Equals(Me.AttributeTypeId, value) Then
                Me.AttributeTypeId = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the range. </summary>
    ''' <value> The range. </value>
    Public Property Range As isr.Core.Constructs.RangeR
        Get
            Return New isr.Core.Constructs.RangeR(Me.Min, Me.Max)
        End Get
        Set(value As isr.Core.Constructs.RangeR)
            If value Is Nothing Then
                Me.Min = 0
                Me.Max = 0
            Else
                Me.Min = value.Min
                Me.Max = value.Max
            End If
        End Set
    End Property

    #End Region

End Class

''' <summary> Collection of part attribute Ranges. </summary>
''' <remarks> David, 4/30/2020. </remarks>
Public Class OhmniPartAttributeRangeCollection
    Inherits ObjectModel.KeyedCollection(Of Integer, OhmniPartAttributeRangeEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As OhmniPartAttributeRangeEntity) As Integer
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.AttributeTypeId
    End Function

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="partAutoId"> Identifier for the part. </param>
    Public Sub New(ByVal partAutoId As Integer)
        MyBase.New
        Me.PartAutoId = partAutoId
    End Sub

    ''' <summary> Gets or sets the identifier of the part automatic. </summary>
    ''' <value> The identifier of the part automatic. </value>
    Public ReadOnly Property PartAutoId As Integer

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of OhmniPartAttributeRangeEntity))
        If entities?.Any Then
            For Each entity As OhmniPartAttributeRangeEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

End Class

