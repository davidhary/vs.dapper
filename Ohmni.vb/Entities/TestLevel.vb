Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities.ConnectionExtensions
Imports isr.Dapper.Entity.EntityExtensions
Imports isr.Dapper.Ohmni.ExceptionExtensions

''' <summary>
''' Interface for the TestLevel nub and entity. Includes the fields as kept in the data table.
''' Allows tracking of property changes.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IOhmniTestLevel

    ''' <summary> Gets the identifier of the test level. </summary>
    ''' <value> The identifier of the test level. </value>
    <ExplicitKey>
    Property TestLevelId As Integer

    ''' <summary> Gets the name. </summary>
    ''' <value> The name. </value>
    Property Name As String

    ''' <summary> Gets the description. </summary>
    ''' <value> The description. </value>
    Property Description As String

End Interface

''' <summary> A test level builder. </summary>
''' <remarks> David, 4/24/2020. </remarks>
Public NotInheritable Class OhmniTestLevelBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(OhmniTestLevelBuilder._TableName) Then
                OhmniTestLevelBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(OhmniTestLevelNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return _TableName
        End Get
    End Property

    ''' <summary>
    ''' Inserts the values described by the <see cref="TestLevel"/> enumeration type.
    ''' </summary>
    ''' <remarks> David, 4/21/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The number of inserted or total existing records. </returns>
    Public Shared Function InsertIgnoreTestLevelValues(ByVal connection As System.Data.IDbConnection) As Integer
        Return OhmniTestLevelBuilder.InsertIgnoreValues(connection, GetType(TestLevel), New Integer() {0})
    End Function

    ''' <summary> Inserts the values described by type. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="type">       The type. </param>
    ''' <param name="excluded">   The excluded. </param>
    ''' <returns> The number of inserted or total existing records. </returns>
    Public Shared Function InsertIgnoreValues(ByVal connection As System.Data.IDbConnection, ByVal type As Type, ByVal excluded As IEnumerable(Of Integer)) As Integer
        Return connection.InsertIgnoreNameDescriptionRecords(OhmniTestLevelBuilder.TableName, GetType(IOhmniTestLevel).EnumerateEntityFieldNames(), type, excluded)
    End Function

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{OhmniTestLevelBuilder.TableName}] (
	[{NameOf(OhmniTestLevelNub.TestLevelId)}] integer NOT NULL PRIMARY KEY, 
	[{NameOf(OhmniTestLevelNub.Name)}] nvarchar(50) NOT NULL, 
	[{NameOf(OhmniTestLevelNub.Description)}] nvarchar(250) NOT NULL); 
CREATE UNIQUE INDEX [UQ_{OhmniTestLevelBuilder.TableName}] ON [{OhmniTestLevelBuilder.TableName}] ([{NameOf(OhmniTestLevelNub.Name)}]); ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmniTestLevelBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{OhmniTestLevelBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{OhmniTestLevelBuilder.TableName}](
	[{NameOf(OhmniTestLevelNub.TestLevelId)}] [int] NOT NULL,
	[{NameOf(OhmniTestLevelNub.Name)}] [nvarchar](50) NOT NULL,
	[{NameOf(OhmniTestLevelNub.Description)}] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_{OhmniTestLevelBuilder.TableName}] PRIMARY KEY CLUSTERED 
([{NameOf(OhmniTestLevelNub.TestLevelId)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END; 
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{OhmniTestLevelBuilder.TableName}]') AND name = N'UQ_{OhmniTestLevelBuilder.TableName}')
CREATE UNIQUE NONCLUSTERED INDEX [UQ_{OhmniTestLevelBuilder.TableName}] ON [dbo].[{OhmniTestLevelBuilder.TableName}] ([{NameOf(OhmniTestLevelNub.Name)}] ASC) 
 WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmniTestLevelBuilder.TableName
    End Function

End Class

''' <summary>
''' Implements the TestLevel table <see cref="IOhmniTestLevel">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2018 </para>
''' </remarks>
<Table("TestLevel")>
Public Class OhmniTestLevelNub
    Inherits EntityNubBase(Of IOhmniTestLevel)
    Implements IOhmniTestLevel

    #Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmniTestLevel
        Return New OhmniTestLevelNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmniTestLevel
        Dim destination As IOhmniTestLevel = Me.CreateNew
        OhmniTestLevelNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmniTestLevel)
        OhmniTestLevelNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IOhmniTestLevel, ByVal destination As IOhmniTestLevel)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.TestLevelId = source.TestLevelId
        destination.Name = source.Name
        destination.Description = source.Description
    End Sub

    #End Region

    #Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IOhmniTestLevel))
        Throw New NotImplementedException()
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IOhmniTestLevel) As Boolean
        Return other IsNot Nothing AndAlso OhmniTestLevelNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IOhmniTestLevel, ByVal right As IOhmniTestLevel) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.TestLevelId, right.TestLevelId)
            result = result AndAlso String.Equals(left.Name, right.Name)
            result = result AndAlso String.Equals(left.Description, right.Description)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.TestLevelId Xor Me.Name.GetHashCode() Xor Me.Description.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the test level. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The identifier of the test level. </value>
    <ExplicitKey>
    Public Property TestLevelId As Integer Implements IOhmniTestLevel.TestLevelId

    ''' <summary> Gets or sets the name. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name. </value>
    Public Property Name As String Implements IOhmniTestLevel.Name

    ''' <summary> Gets or sets the description. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The description. </value>
    Public Property Description As String Implements IOhmniTestLevel.Description

    #End Region

End Class

''' <summary> The TestLevel Entity. Implements access to the database using Dapper. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2018 </para>
''' </remarks>
Public Class OhmniTestLevelEntity
    Inherits EntityBase(Of IOhmniTestLevel, OhmniTestLevelNub)
    Implements IOhmniTestLevel

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New OhmniTestLevelNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As IOhmniTestLevel)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOhmniTestLevel, ByVal store As IOhmniTestLevel)
        MyBase.New(New OhmniTestLevelNub, cache, store)
        Me.UsingNativeTracking = String.Equals(OhmniTestLevelBuilder.TableName, NameOf(IOhmniTestLevel).TrimStart("I"c))
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmniTestLevel
        Return New OhmniTestLevelNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmniTestLevel
        Dim destination As IOhmniTestLevel = Me.CreateNew
        OhmniTestLevelNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmniTestLevel)
        OhmniTestLevelNub.Copy(value, Me)
    End Sub

    #End Region

    #Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOhmniTestLevel)
        ' first make the copy to notify of any property change.
        OhmniTestLevelNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

    #End Region

    #Region " DATABASE ACCESS "

    ''' <summary> Fetches using key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        the Meter Model table primary key. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Me.ClearStore()
        Return Me.Enstore(If(Me.UsingNativeTracking, connection.Get(Of IOhmniTestLevel)(key), connection.Get(Of OhmniTestLevelNub)(key)))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.TestLevelId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.Name)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 4/28/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="name">       Name of the Attribute Type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal name As String) As Boolean
        Me.ClearStore()
        Dim nub As OhmniTestLevelNub = OhmniTestLevelEntity.FetchNubs(connection, name).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOhmniTestLevel) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If OhmniTestLevelEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.Name) Then
            ' update the existing record from the specified entity.
            entity.TestLevelId = Me.TestLevelId
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="key">        The primary key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal key As Integer) As Boolean
        Return connection.Delete(Of OhmniTestLevelNub)(New OhmniTestLevelNub With {.TestLevelId = key})
    End Function

    #End Region

    #Region " ENTITIES "

    ''' <summary> Gets or sets the Attribute Type entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Attribute Type entities. </value>
    Public ReadOnly Property TestLevels As IEnumerable(Of OhmniTestLevelEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of OhmniTestLevelEntity)
        Return If(usingNativeTracking, OhmniTestLevelEntity.Populate(connection.GetAll(Of IOhmniTestLevel)), OhmniTestLevelEntity.Populate(connection.GetAll(Of OhmniTestLevelNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._TestLevels = OhmniTestLevelEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(OhmniTestLevelEntity.TestLevels))
        Return If(Me.TestLevels?.Any, Me.TestLevels.Count, 0)
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The entity nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of OhmniTestLevelNub)) As IEnumerable(Of OhmniTestLevelEntity)
        Dim l As New List(Of OhmniTestLevelEntity)
        If nubs?.Any Then
            For Each nub As OhmniTestLevelNub In nubs
                l.Add(New OhmniTestLevelEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The entity interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOhmniTestLevel)) As IEnumerable(Of OhmniTestLevelEntity)
        Dim l As New List(Of OhmniTestLevelEntity)
        If interfaces?.Any Then
            Dim nub As New OhmniTestLevelNub
            For Each iFace As IOhmniTestLevel In interfaces
                nub.CopyFrom(iFace)
                l.Add(New OhmniTestLevelEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    #End Region

    #Region " FIND "

    ''' <summary> Count entities; returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="name">       The test level name. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal name As String) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{OhmniTestLevelBuilder.TableName}]
WHERE {NameOf(OhmniTestLevelNub.Name)} = @Name", New With {.Name = name})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches nubs; expects single entity or none. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="name">       The test level name. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchNubs(ByVal connection As System.Data.IDbConnection, ByVal name As String) As IEnumerable(Of OhmniTestLevelNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{OhmniTestLevelBuilder.TableName}]
WHERE {NameOf(OhmniTestLevelNub.Name)} = @Name", New With {.Name = name})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of OhmniTestLevelNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the TestLevel exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="name">       The test level name. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal name As String) As Boolean
        Return 1 = OhmniTestLevelEntity.CountEntities(connection, name)
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the test level. </summary>
    ''' <value> The identifier of the test level. </value>
    Public Property TestLevelId As Integer Implements IOhmniTestLevel.TestLevelId
        Get
            Return Me.ICache.TestLevelId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.TestLevelId, value) Then
                Me.ICache.TestLevelId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the name. </summary>
    ''' <value> The name. </value>
    Public Property Name As String Implements IOhmniTestLevel.Name
        Get
            Return Me.ICache.Name
        End Get
        Set(value As String)
            If Not String.Equals(Me.Name, value) Then
                Me.ICache.Name = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the description. </summary>
    ''' <value> The description. </value>
    Public Property Description As String Implements IOhmniTestLevel.Description
        Get
            Return Me.ICache.Description
        End Get
        Set(value As String)
            If Not String.Equals(Me.Description, value) Then
                Me.ICache.Description = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    #End Region

    #Region " SHARED FUNCTIONS "

    ''' <summary> Attempts to fetch the entity using the entity unique key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">  The connection. </param>
    ''' <param name="testLevelId"> The entity unique key. </param>
    ''' <param name="e">           Action event information. </param>
    ''' <returns> A <see cref="OhmniTestLevelEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal testLevelId As Integer, ByVal e As isr.Core.ActionEventArgs) As OhmniTestLevelEntity
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Dim result As New OhmniTestLevelEntity
        Try
            activity = $"Fetching test level by id {testLevelId}"
            If Not result.FetchUsingKey(connection, testLevelId) Then
                e.RegisterFailure($"Failed {activity}")
            End If
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return result
    End Function

    ''' <summary> Try fetch using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">    The connection. </param>
    ''' <param name="testLevelName"> Name of the test level. </param>
    ''' <param name="e">             Action event information. </param>
    ''' <returns> A <see cref="OhmniTestLevelEntity"/>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal testLevelName As String, ByVal e As isr.Core.ActionEventArgs) As OhmniTestLevelEntity
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Dim result As New OhmniTestLevelEntity
        Try
            activity = $"Fetching test level by index {testLevelName}"
            If Not result.FetchUsingUniqueIndex(connection, testLevelName) Then
                e.RegisterFailure($"Failed {activity}")
            End If
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return result
    End Function

    ''' <summary> Attempts to fetch all entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="e">          Action event information. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process try fetch all
    ''' <see cref="OhmniTestLevelEntity"/> items in this collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryFetchAll(ByVal connection As System.Data.IDbConnection, ByVal e As isr.Core.ActionEventArgs) As IEnumerable(Of OhmniTestLevelEntity)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Try
            Dim entity As New OhmniTestLevelEntity()
            activity = "fetching all TestLevels"
            ' fetch all TestLevels
            entity.FetchAllEntities(connection)
            Return entity.TestLevels
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
            Return New List(Of OhmniTestLevelEntity)
        End Try
    End Function

    #End Region

End Class

''' <summary> Values that represent test levels. </summary>
''' <remarks> David, 2020-10-02. </remarks>
Public Enum TestLevel

    ''' <summary> . </summary>
    <ComponentModel.Description("None")> None = 0

    ''' <summary> . </summary>
    <ComponentModel.Description("Trim")> Trim = 1

    ''' <summary> . </summary>
    <ComponentModel.Description("Final Test")> Final = 2

    ''' <summary> . </summary>
    <ComponentModel.Description("Empty")> Empty = 3
End Enum
