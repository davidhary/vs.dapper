Imports Dapper
Imports Dapper.Contrib.Extensions

Imports isr.Core.TrimExtensions
Imports isr.Dapper.Entity

''' <summary>
''' Interface for the Part Attribute Value nub and entity. Includes the fields as kept in the
''' data table. Allows tracking of property changes.
''' </summary>
''' <remarks>
''' Update tracking of table with default values requires fetching the inserted record if a
''' default value is set to a non-default value. <para>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IOhmniPartAttributeValue

    ''' <summary> Gets the identifier of the Part. </summary>
    ''' <value> The identifier of the Part. </value>
    <ExplicitKey>
    Property PartAutoId As Integer

    ''' <summary> Gets the identifier of the Attribute type. </summary>
    ''' <value> The identifier of the structure type. </value>
    <ExplicitKey>
    Property AttributeTypeId As Integer

    ''' <summary> Gets the Value. </summary>
    ''' <value> The Part Value. </value>
    Property Value As Double

End Interface

''' <summary> A part attribute value builder. </summary>
''' <remarks> David, 4/30/2020. </remarks>
Public NotInheritable Class OhmniPartAttributeValueBuilder

    ''' <summary> Name of the table. </summary>
    Private Shared _TableName As String

    ''' <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <value> A String. </value>
    Public Shared ReadOnly Property TableName() As String
        Get
            If String.IsNullOrWhiteSpace(OhmniPartAttributeValueBuilder._TableName) Then
                OhmniPartAttributeValueBuilder._TableName = CType(System.Attribute.GetCustomAttribute(GetType(OhmniPartAttributeValueNub), GetType(TableAttribute)), TableAttribute).Name
            End If
            Return OhmniPartAttributeValueBuilder._TableName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the Attribute Type table. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="NotImplementedException">   Thrown when the requested operation is
    '''                                              unimplemented. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the AttributeType table. </value>
    Public Shared Property AttributeTypeTableName As String = OhmniAttributeTypeBuilder.TableName

    ''' <summary> Gets or sets the name of the Attribute Type table foreign key. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="NotImplementedException">   Thrown when the requested operation is
    '''                                              unimplemented. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The name of the AttributeType table foreign key. </value>
    Public Shared Property AttributeTypeForeignKeyName As String = NameOf(OhmniAttributeTypeNub.AttributeTypeId)

    ''' <summary> Creates a table. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Public Shared Function CreateTable(ByVal connection As System.Data.IDbConnection) As String
        Dim sql As System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
        If sql IsNot Nothing Then Return CreateTable(sql)
        Dim sqlite As System.Data.SQLite.SQLiteConnection = TryCast(connection, System.Data.SQLite.SQLiteConnection)
        If sqlite IsNot Nothing Then Return CreateTable(sqlite)
        Return String.Empty
    End Function

    ''' <summary> Creates table for SQLite database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SQLite.SQLiteConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"CREATE TABLE IF NOT EXISTS [{OhmniPartAttributeValueBuilder.TableName}] (
	[{NameOf(OhmniPartAttributeValueNub.PartAutoId)}] integer NOT NULL, 
	[{NameOf(OhmniPartAttributeValueNub.AttributeTypeId)}] integer NOT NULL, 
	[{NameOf(OhmniPartAttributeValueNub.Value)}] float NOT NULL, 
CONSTRAINT [sqlite_autoindex_{OhmniPartAttributeValueBuilder.TableName}_1] PRIMARY KEY ([{NameOf(OhmniPartAttributeValueNub.PartAutoId)}], [{NameOf(OhmniPartAttributeValueNub.AttributeTypeId)}]),
FOREIGN KEY ([{NameOf(OhmniPartAttributeValueNub.PartAutoId)}]) REFERENCES [{CincoPartBuilder.TableName}] ([{NameOf(CincoPartNub.PartAutoId)}]) ON UPDATE CASCADE ON DELETE CASCADE, 
FOREIGN KEY ([{NameOf(OhmniPartAttributeValueNub.AttributeTypeId)}]) REFERENCES [{OhmniPartAttributeValueBuilder.AttributeTypeTableName}] ([{OhmniPartAttributeValueBuilder.AttributeTypeForeignKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); 
")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmniPartAttributeValueBuilder.TableName
    End Function

    ''' <summary> Creates table for SQL Server database. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> The table name or empty. </returns>
    Private Shared Function CreateTable(ByVal connection As System.Data.SqlClient.SqlConnection) As String
        Dim queryBuilder As New System.Text.StringBuilder
        queryBuilder.Append($"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{OhmniPartAttributeValueBuilder.TableName}]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[{OhmniPartAttributeValueBuilder.TableName}](
	[{NameOf(OhmniPartAttributeValueNub.PartAutoId)}] [int] NOT NULL,
	[{NameOf(OhmniPartAttributeValueNub.AttributeTypeId)}] [int] NOT NULL,
	[{NameOf(OhmniPartAttributeValueNub.Value)}] [float] NOT NULL,
 CONSTRAINT [PK_{OhmniPartAttributeValueBuilder.TableName}] PRIMARY KEY CLUSTERED 
([{NameOf(OhmniPartAttributeValueNub.PartAutoId)}] ASC, [{NameOf(OhmniPartAttributeValueNub.AttributeTypeId)}] ASC) 
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
  ON [PRIMARY]
END

 IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmniPartAttributeValueBuilder.TableName}_{OhmniPartAttributeValueBuilder.AttributeTypeTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmniPartAttributeValueBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmniPartAttributeValueBuilder.TableName}] WITH CHECK ADD CONSTRAINT [FK_{OhmniPartAttributeValueBuilder.TableName}_{OhmniPartAttributeValueBuilder.AttributeTypeTableName}] FOREIGN KEY([{NameOf(OhmniPartAttributeValueNub.AttributeTypeId)}])
REFERENCES [dbo].[{OhmniPartAttributeValueBuilder.AttributeTypeTableName}] ([{OhmniPartAttributeValueBuilder.AttributeTypeForeignKeyName}])
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmniPartAttributeValueBuilder.TableName}_{OhmniPartAttributeValueBuilder.AttributeTypeTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmniPartAttributeValueBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmniPartAttributeValueBuilder.TableName}] CHECK CONSTRAINT [FK_{OhmniPartAttributeValueBuilder.TableName}_{OhmniPartAttributeValueBuilder.AttributeTypeTableName}]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmniPartAttributeValueBuilder.TableName}_{CincoPartBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmniPartAttributeValueBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmniPartAttributeValueBuilder.TableName}] WITH CHECK ADD CONSTRAINT [FK_{OhmniPartAttributeValueBuilder.TableName}_{CincoPartBuilder.TableName}] FOREIGN KEY([{NameOf(CincoPartNub.PartAutoId)}])
REFERENCES [dbo].[{CincoPartBuilder.TableName}] ([{NameOf(CincoPartNub.PartAutoId)}])
ON UPDATE CASCADE ON DELETE CASCADE 
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{OhmniPartAttributeValueBuilder.TableName}_{CincoPartBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{OhmniPartAttributeValueBuilder.TableName}]'))
ALTER TABLE [dbo].[{OhmniPartAttributeValueBuilder.TableName}] CHECK CONSTRAINT [FK_{OhmniPartAttributeValueBuilder.TableName}_{CincoPartBuilder.TableName}]; ")
        connection.Execute(queryBuilder.ToString().Clean())
        Return OhmniPartAttributeValueBuilder.TableName
    End Function

End Class

''' <summary>
''' Implements the Part Attribute Value table
''' <see cref="IOhmniPartAttributeValue">interface</see>.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2018 </para>
''' </remarks>
<Table("PartAttributeValue")>
Public Class OhmniPartAttributeValueNub
    Inherits EntityNubBase(Of IOhmniPartAttributeValue)
    Implements IOhmniPartAttributeValue

    #Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmniPartAttributeValue
        Return New OhmniPartAttributeValueNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmniPartAttributeValue
        Dim destination As IOhmniPartAttributeValue = Me.CreateNew
        OhmniPartAttributeValueNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmniPartAttributeValue)
        OhmniPartAttributeValueNub.Copy(value, Me)
    End Sub

    ''' <summary> Copies the given value. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source">      Another instance to copy. </param>
    ''' <param name="destination"> Destination for the. </param>
    Public Overloads Shared Sub Copy(ByVal source As IOhmniPartAttributeValue, ByVal destination As IOhmniPartAttributeValue)
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
        destination.AttributeTypeId = source.AttributeTypeId
        destination.PartAutoId = source.PartAutoId
        destination.Value = source.Value
    End Sub

    #End Region

    #Region " I EQUATABLE "

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="other"> The object to compare with the current object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the specified object  is equal to the current object; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(other As Object) As Boolean
        Return Me.Equals(TryCast(other, IOhmniPartAttributeValue))
        Throw New NotImplementedException()
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-04-27. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Overrides Function Equals(other As IOhmniPartAttributeValue) As Boolean ' Implements IEquatable(Of IOhmniPartAttributeValue).Equals
        Return other IsNot Nothing AndAlso OhmniPartAttributeValueNub.AreEqual(other, Me)
    End Function

    ''' <summary> Determines if entities are equal. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal left As IOhmniPartAttributeValue, ByVal right As IOhmniPartAttributeValue) As Boolean
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        Dim result As Boolean = right IsNot Nothing
        If right Is Nothing Then
            Return False
        Else
            result = result AndAlso Integer.Equals(left.AttributeTypeId, right.AttributeTypeId)
            result = result AndAlso Integer.Equals(left.PartAutoId, right.PartAutoId)
            result = result AndAlso Double.Equals(left.Value, right.Value)
            Return result
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.AttributeTypeId Xor Me.PartAutoId Xor Me.Value.GetHashCode()
    End Function


    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Part. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The identifier of the Part. </value>
    <ExplicitKey>
    Public Property PartAutoId As Integer Implements IOhmniPartAttributeValue.PartAutoId

    ''' <summary> Gets or sets the identifier of the Part Attribute type. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The identifier of the Part Attribute type. </value>
    <ExplicitKey>
    Public Property AttributeTypeId As Integer Implements IOhmniPartAttributeValue.AttributeTypeId

    ''' <summary> Gets or sets the Part Attribute value. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The Part Attribute value. </value>
    Public Property Value As Double Implements IOhmniPartAttributeValue.Value

    #End Region

End Class

''' <summary>
''' The PartAttributeValue Entity. Implements access to the database using Dapper.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2018 </para>
''' </remarks>
Public Class OhmniPartAttributeValueEntity
    Inherits EntityBase(Of IOhmniPartAttributeValue, OhmniPartAttributeValueNub)
    Implements IOhmniPartAttributeValue ' , IEquatable(Of IOhmniPartAttributeValue), ITypedCloneable(Of IOhmniPartAttributeValue)

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    Public Sub New()
        Me.New(New OhmniPartAttributeValueNub)
    End Sub

    ''' <summary> Constructs an entity that was not yet stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Sub New(ByVal value As IOhmniPartAttributeValue)
        ' this tags the entity is new
        Me.New(value, Nothing)
    End Sub

    ''' <summary> Constructs an entity that is already stored. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="cache"> The cache. </param>
    ''' <param name="store"> The store. </param>
    Public Sub New(ByVal cache As IOhmniPartAttributeValue, ByVal store As IOhmniPartAttributeValue)
        MyBase.New(New OhmniPartAttributeValueNub, cache, store)
        Me.UsingNativeTracking = String.Equals(OhmniPartAttributeValueBuilder.TableName, NameOf(IOhmniPartAttributeValue).TrimStart("I"c))
    End Sub

    #End Region

    #Region " I TYPED CLONABLE "

    ''' <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The new instance the entity 'Nub'. </returns>
    Public Overrides Function CreateNew() As IOhmniPartAttributeValue
        Return New OhmniPartAttributeValueNub
    End Function

    ''' <summary> Creates a copy of the class using the entity 'Nub'. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <returns> The copy of the entity 'Nub'. </returns>
    Public Overrides Function CreateCopy() As IOhmniPartAttributeValue
        Dim destination As IOhmniPartAttributeValue = Me.CreateNew
        OhmniPartAttributeValueNub.Copy(Me, destination)
        Return destination
    End Function

    ''' <summary> Copies the given entity into this class. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> The instance from which to copy. </param>
    Public Overrides Sub CopyFrom(ByVal value As IOhmniPartAttributeValue)
        OhmniPartAttributeValueNub.Copy(value, Me)
    End Sub

    #End Region

    #Region " OVERRIDES "

    ''' <summary>
    ''' Update the cached value, which also notifies of the entity property changes.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="value"> the Meter Model interface. </param>
    Public Overrides Sub UpdateCache(ByVal value As IOhmniPartAttributeValue)
        ' first make the copy to notify of any property change.
        OhmniPartAttributeValueNub.Copy(value, Me)
        ' this is required to restore the cache interface for tracking
        MyBase.UpdateCache(value)
    End Sub

    #End Region

    #Region " DATABASE ACCESS "

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="partAutoId">      The identifier of the Part. </param>
    ''' <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingKey(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal attributeTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{OhmniPartAttributeValueBuilder.TableName}] /**where**/")
        
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeRangeNub.PartAutoId)} = @Id", New With {.Id = partAutoId})
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeRangeNub.AttributeTypeId)} = @TypeId", New With {.TypeId = attributeTypeId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return Me.Enstore(connection.QueryFirstOrDefault(Of OhmniPartAttributeValueNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Refetch; Fetch using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function FetchUsingKey(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingKey(connection, Me.PartAutoId, Me.AttributeTypeId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection) As Boolean
        Return Me.FetchUsingUniqueIndex(connection, Me.PartAutoId, Me.AttributeTypeId)
    End Function

    ''' <summary> Fetches an existing entity using unique index. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="partAutoId">      The identifier of the Part. </param>
    ''' <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overloads Function FetchUsingUniqueIndex(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal attributeTypeId As Integer) As Boolean
        Me.ClearStore()
        Dim nub As OhmniPartAttributeValueNub = OhmniPartAttributeValueEntity.FetchEntities(connection, partAutoId, attributeTypeId).SingleOrDefault
        Return nub IsNot Nothing AndAlso Me.Enstore(nub)
    End Function

    ''' <summary> Updates or inserts the entity using the specified entity information. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="entity">     The entity. </param>
    ''' <returns>
    ''' <c>true</c> if it succeeds (<see cref="EntityBase(Of TIEntity, TEntityNub).IsClean()"/>);
    ''' otherwise <c>false</c>
    ''' </returns>
    Public Overrides Function Upsert(ByVal connection As System.Data.IDbConnection, ByVal entity As IOhmniPartAttributeValue) As Boolean
        If entity Is Nothing Then Throw New ArgumentNullException(NameOf(entity))
        If OhmniPartAttributeValueEntity.ReferenceEquals(Me, entity) Then Throw New InvalidOperationException($"{NameOf(entity)} must not be identical to the specified entity")
        ' try fetching an existing record
        If Me.FetchUsingUniqueIndex(connection, entity.PartAutoId, entity.AttributeTypeId) Then
            ' update the existing record from the specified entity.
            Me.UpdateCache(entity)
            Me.Update(connection)
        Else
            ' insert a new record based on the specified entity values.
            Me.UpdateCache(entity)
            Me.Insert(connection)
        End If
        Return Me.IsClean
    End Function

    ''' <summary> Deletes a record using the given primary key. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="partAutoId">      The identifier of the Part. </param>
    ''' <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Shared Function Delete(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal attributeTypeId As Integer) As Boolean
        Return connection.Delete(Of OhmniPartAttributeValueNub)(New OhmniPartAttributeValueNub With {.PartAutoId = partAutoId, .AttributeTypeId = attributeTypeId})
    End Function

    #End Region

    #Region " ENTITIES "

    ''' <summary> Gets or sets the PartAttributeValue entities. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The PartAttributeValue entities. </value>
    Public ReadOnly Property PartAttributeValues As IEnumerable(Of OhmniPartAttributeValueEntity)

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">          The connection. </param>
    ''' <param name="usingNativeTracking"> True to using native tracking. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Overloads Shared Function FetchAllEntities(ByVal connection As System.Data.IDbConnection, ByVal usingNativeTracking As Boolean) As IEnumerable(Of OhmniPartAttributeValueEntity)
        Return If(usingNativeTracking, OhmniPartAttributeValueEntity.Populate(connection.GetAll(Of IOhmniPartAttributeValue)), OhmniPartAttributeValueEntity.Populate(connection.GetAll(Of OhmniPartAttributeValueNub)))
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> all. </returns>
    Public Overrides Function FetchAllEntities(ByVal connection As System.Data.IDbConnection) As Integer
        Me._PartAttributeValues = OhmniPartAttributeValueEntity.FetchAllEntities(connection, Me.UsingNativeTracking)
        Me.NotifyPropertyChanged(NameOf(OhmniPartAttributeValueEntity.PartAttributeValues))
        Return If(Me.PartAttributeValues?.Any, Me.PartAttributeValues.Count, 0)
    End Function

    ''' <summary> Count PartAttributeValues. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> The identifier of the Part. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT COUNT(*) FROM [{OhmniPartAttributeValueBuilder.TableName}]
WHERE {NameOf(OhmniPartAttributeValueNub.PartAutoId)} = @Id", New With {Key .Id = partAutoId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Fetches all records into entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="partAutoId"> The identifier of the Part. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the entities in this collection.
    ''' </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer) As IEnumerable(Of OhmniPartAttributeValueEntity)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"SELECT * FROM [{OhmniPartAttributeValueBuilder.TableName}]
WHERE {NameOf(OhmniPartAttributeValueNub.PartAutoId)} = @Id", New With {Key .Id = partAutoId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return OhmniPartAttributeValueEntity.Populate(connection.Query(Of OhmniPartAttributeValueNub)(selector.RawSql, selector.Parameters))
    End Function

    ''' <summary> Populates a list of Part Attribute value entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="nubs"> The Part Attribute value nubs. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal nubs As IEnumerable(Of OhmniPartAttributeValueNub)) As IEnumerable(Of OhmniPartAttributeValueEntity)
        Dim l As New List(Of OhmniPartAttributeValueEntity)
        If nubs?.Any Then
            For Each nub As OhmniPartAttributeValueNub In nubs
                l.Add(New OhmniPartAttributeValueEntity(nub, nub.CreateCopy))
            Next
        End If
        Return l
    End Function

    ''' <summary> Populates a list of Part Attribute value entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="interfaces"> The Part Attribute value interfaces. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process populate in this collection.
    ''' </returns>
    Public Shared Function Populate(ByVal interfaces As IEnumerable(Of IOhmniPartAttributeValue)) As IEnumerable(Of OhmniPartAttributeValueEntity)
        Dim l As New List(Of OhmniPartAttributeValueEntity)
        If interfaces?.Any Then
            Dim nub As New OhmniPartAttributeValueNub
            For Each iFace As IOhmniPartAttributeValue In interfaces
                nub.CopyFrom(iFace)
                l.Add(New OhmniPartAttributeValueEntity(iFace, nub.CreateCopy()))
            Next
        End If
        Return l
    End Function

    #End Region

    #Region " FIND "

    ''' <summary> Count Part Attributes; Returns 1 or 0. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="partAutoId">      The identifier of the Part. </param>
    ''' <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
    ''' <returns> The total number of entities. </returns>
    Public Shared Function CountEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal attributeTypeId As Integer) As Integer
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select count(*) from [{OhmniPartAttributeValueBuilder.TableName}] /**where**/")
        
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeValueEntity.PartAutoId)} = @Id", New With {.Id = partAutoId})
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeValueEntity.AttributeTypeId)} = @TypeId", New With {.TypeId = attributeTypeId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.ExecuteScalar(Of Integer)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary>
    ''' Fetches Part Attributes by Part number and Part Attribute Part Attribute value;
    ''' expected single or none.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">      The connection. </param>
    ''' <param name="partAutoId">      The identifier of the Part. </param>
    ''' <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Shared Function FetchEntities(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal attributeTypeId As Integer) As IEnumerable(Of OhmniPartAttributeValueNub)
        Dim sqlBuilder As SqlBuilder = New SqlBuilder()
        Dim selector As SqlBuilder.Template = sqlBuilder.AddTemplate($"select * from [{OhmniPartAttributeValueBuilder.TableName}] /**where**/")
        
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeValueEntity.PartAutoId)} = @Id", New With {.Id = partAutoId})
        sqlBuilder.Where($"{NameOf(OhmniPartAttributeValueEntity.AttributeTypeId)} = @TypeId", New With {.TypeId = attributeTypeId})
        
        If selector.RawSql Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.RawSql)} null")
        ElseIf selector.Parameters Is Nothing Then
            Throw New InvalidOperationException($"{NameOf(selector.Parameters)} null")
        End If
        Return connection.Query(Of OhmniPartAttributeValueNub)(selector.RawSql, selector.Parameters)
    End Function

    ''' <summary> Determine if the PartAttributeValue exists. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection">         The connection. </param>
    ''' <param name="partAutoId">         The identifier of the Part. </param>
    ''' <param name="partAttributeValue"> The part attribute value. </param>
    ''' <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
    Public Shared Function IsExists(ByVal connection As System.Data.IDbConnection, ByVal partAutoId As Integer, ByVal partAttributeValue As Integer) As Boolean
        Return 1 = OhmniPartAttributeValueEntity.CountEntities(connection, partAutoId, partAttributeValue)
    End Function

    #End Region

    #Region " RELATIONS "

    ''' <summary> Gets or sets the Part entity. </summary>
    ''' <value> The Part entity. </value>
    Public ReadOnly Property PartEntity As CincoPartEntity

    ''' <summary> Fetches a Part Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchPartEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._PartEntity = New CincoPartEntity()
        Return Me.PartEntity.FetchUsingKey(connection, Me.PartAutoId)
    End Function

    ''' <summary> Gets or sets the AttributeType entity. </summary>
    ''' <value> The AttributeType entity. </value>
    Public ReadOnly Property AttributeTypeEntity As OhmniAttributeTypeEntity

    ''' <summary> Fetches a AttributeType Entity. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FetchAttributeTypeEntity(ByVal connection As System.Data.IDbConnection) As Boolean
        Me._AttributeTypeEntity = New OhmniAttributeTypeEntity()
        Return Me.AttributeTypeEntity.FetchUsingKey(connection, Me.AttributeTypeId)
    End Function

    #End Region

    #Region " FIELDS "

    ''' <summary> Gets or sets the identifier of the Part. </summary>
    ''' <value> The identifier of the Part. </value>
    Public Property PartAutoId As Integer Implements IOhmniPartAttributeValue.PartAutoId
        Get
            Return Me.ICache.PartAutoId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.PartAutoId, value) Then
                Me.ICache.PartAutoId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the Part Attribute type. </summary>
    ''' <value> The identifier of the Part Attribute type. </value>
    Public Property AttributeTypeId As Integer Implements IOhmniPartAttributeValue.AttributeTypeId
        Get
            Return Me.ICache.AttributeTypeId
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.AttributeTypeId, value) Then
                Me.ICache.AttributeTypeId = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Part Attribute value. </summary>
    ''' <value> The Part Attribute value. </value>
    Public Property Value As Double Implements IOhmniPartAttributeValue.Value
        Get
            Return Me.ICache.Value
        End Get
        Set(ByVal value As Double)
            If Not Double.Equals(Me.Value, value) Then
                Me.ICache.Value = value
                Me.NotifyFieldChanged()
            End If
        End Set
    End Property

    #End Region

    #Region " FIELDS: CUSTOM "

    ''' <summary> Gets or sets the AttributeType. </summary>
    ''' <value> The AttributeType. </value>
    Public Property AttributeType As AttributeType
        Get
            Return CType(Me.AttributeTypeId, AttributeType)
        End Get
        Set(ByVal value As AttributeType)
            If Not Integer.Equals(Me.AttributeTypeId, value) Then
                Me.AttributeTypeId = value
            End If
        End Set
    End Property

    #End Region

End Class

''' <summary> Collection of part attribute values. </summary>
''' <remarks> David, 4/30/2020. </remarks>
Public Class OhmniPartAttributeValueCollection
    Inherits ObjectModel.KeyedCollection(Of Integer, OhmniPartAttributeValueEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As OhmniPartAttributeValueEntity) As Integer
        If item Is Nothing Then Throw New ArgumentNullException
        Return item.AttributeTypeId
    End Function

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 4/23/2020. </remarks>
    ''' <param name="partAutoId"> Identifier for the part. </param>
    Public Sub New(ByVal partAutoId As Integer)
        MyBase.New
        Me.PartAutoId = partAutoId
    End Sub

    ''' <summary> Gets or sets the identifier of the part automatic. </summary>
    ''' <value> The identifier of the part automatic. </value>
    Public ReadOnly Property PartAutoId As Integer

    ''' <summary> Populates the given entities. </summary>
    ''' <remarks> David, 2020-10-02. </remarks>
    ''' <param name="entities"> The entities. </param>
    Public Sub Populate(ByVal entities As IEnumerable(Of OhmniPartAttributeValueEntity))
        If entities?.Any Then
            For Each entity As OhmniPartAttributeValueEntity In entities
                Me.Add(entity)
            Next
        End If
    End Sub

End Class

