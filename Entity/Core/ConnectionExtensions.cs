using System;

namespace isr.Dapper.Entity.ConnectionExtensions
{

    /// <summary> The Connection Extensions methods. </summary>
    /// <remarks> David, 2020-05-26. </remarks>
    public static partial class Methods
    {

        /// <summary> Executes the command using a transaction operation. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The data source connection. </param>
        /// <param name="command">    The command. </param>
        /// <returns> The number of affected records. </returns>
        public static int ExecuteTransaction( this System.Data.IDbConnection connection, string command )
        {
            return ProviderBase.ExecuteTransaction( connection, command );
        }

        /// <summary> Counts the number of records in the specified table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The data source connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( this System.Data.IDbConnection connection, string tableName )
        {
            return connection is null ? throw new ArgumentNullException( nameof( connection ) ) : ProviderBase.CountEntities( connection, tableName );
        }

        /// <summary> Queries if a given table exists. </summary>
        /// <remarks> David, 2020-05-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The data source connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool TableExists( this System.Data.IDbConnection connection, string tableName )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : ProviderBase.SelectProvider( connection ).TableExists( connection, tableName );
        }

        /// <summary> Queries if a given index exists. </summary>
        /// <remarks> David, 2020-05-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The data source connection. </param>
        /// <param name="indexName">  Name of the index. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool IndexExists( this System.Data.IDbConnection connection, string indexName )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : ProviderBase.SelectProvider( connection ).IndexExists( connection, indexName );
        }

        /// <summary> Reseed table identity. </summary>
        /// <remarks> David, 2020-06-01. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The data source connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> An Integer. </returns>
        public static int ReseedTableIdentity( this System.Data.IDbConnection connection, string tableName )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : ProviderBase.SelectProvider( connection ).ReseedIdentity( connection, tableName );
        }

        /// <summary> Queries if a given view exists. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The data source connection. </param>
        /// <param name="viewName">   Name of the view. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool ViewExists( this System.Data.IDbConnection connection, string viewName )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : ProviderBase.SelectProvider( connection ).ViewExists( connection, viewName );
        }
    }
}
