using System.ComponentModel;

namespace isr.Dapper.Entity.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) ProjectTraceEventId.DapperEntity;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Dapper Entity Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Dapper Entity Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Dapper.Entity";

        /// <summary> The test assembly strong name. </summary>
        public const string TestAssemblyStrongName = "isr.Dapper.Entity.Tests,PublicKey=" + global::Dapper.My.SolutionInfo.PublicKey;
    }

    /// <summary> Values that represent project trace event identifiers. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public enum ProjectTraceEventId
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not specified" )]
        None,

        /// <summary> . </summary>
        [Description( "Dapper Entity" )]
        DapperEntity = Core.ProjectTraceEventId.Dapper,

        /// <summary> . </summary>
        [Description( "Dapper Entities" )]
        DapperEntities = Core.ProjectTraceEventId.Dapper + 0x1,

        /// <summary> . </summary>
        [Description( "Dapper Ohmni" )]
        DapperOhmni = Core.ProjectTraceEventId.Dapper + 0x2,

        /// <summary> . </summary>
        [Description( "Dapper Connection UI" )]
        DapperConnectionUI = Core.ProjectTraceEventId.Dapper + 0x3,

        /// <summary> . </summary>
        [Description( "Dapper Entity UI" )]
        DapperEntityUI = Core.ProjectTraceEventId.Dapper + 0x4
    }
}
