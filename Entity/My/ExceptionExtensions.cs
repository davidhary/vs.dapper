using System;

namespace isr.Dapper.Entity.ExceptionExtensions
{

    /// <summary> Adds exception data for building the exception full blown report. </summary>
    /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class Methods
    {

        /// <summary> Adds an exception data to 'exception'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionData( Exception value, System.Data.SQLite.SQLiteException exception )
        {
            if ( exception is object )
            {
                int count = value.Data.Count;
                value.Data.Add( $"{count}-ErrorCode", exception.ErrorCode );
                value.Data.Add( $"{count}-ResultCode", exception.ResultCode );
            }

            return exception is object;
        }

        /// <summary> Adds an exception data to 'exception'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionData( Exception value, System.Data.SqlClient.SqlException exception )
        {
            if ( exception is object )
            {
                int count = value.Data.Count;
                value.Data.Add( $"{count}-ErrorCode", exception.ErrorCode );
                value.Data.Add( $"{count}-LineNumber", exception.LineNumber );
                value.Data.Add( $"{count}-State", exception.State );
                if ( (exception.Errors?.Count) > 0 == true )
                {
                    int errorId = 0;
                    foreach ( System.Data.SqlClient.SqlError err in exception.Errors )
                    {
                        errorId += 1;
                        // error number could repeat a few times. 
                        value.Data.Add( $"{count}.{errorId}-Error{err.Number}", err.ToString() );
                    }
                }
            }

            return exception is object;
        }

        /// <summary> Adds exception data from the specified exception. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( this Exception exception )
        {
            return AddExceptionData( exception, exception as System.Data.SqlClient.SqlException ) || AddExceptionData( exception, exception as System.Data.SQLite.SQLiteException );
        }

        /// <summary> Adds an exception data. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionDataThis( Exception exception )
        {
            return exception.AddExceptionData() || Core.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( exception );
        }

        /// <summary> Converts a value to a full blown string. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a String. </returns>
        internal static string ToFullBlownString( this Exception value )
        {
            return value.ToFullBlownString( int.MaxValue );
        }

        /// <summary> Converts this object to a full blown string. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="level"> The level. </param>
        /// <returns> The given data converted to a String. </returns>
        internal static string ToFullBlownString( this Exception value, int level )
        {
            return Core.ExceptionExtensions.ExceptionExtensionMethods.ToFullBlownString( value, level, AddExceptionDataThis );
        }
    }
}
