using System;
using System.ComponentModel;

using isr.Core;
using isr.Core.Primitives;

namespace isr.Dapper.Entity
{
    public partial class EntityKeyedCollection<TKey, TIEntity, TEntityNub, TEntity> : INotifyPropertyChanged
        where TKey : struct
        where TIEntity : class
        where TEntityNub : class, TIEntity, IEquatable<TIEntity>, ITypedCloneable<TIEntity>
        where TEntity : EntityBase<TIEntity, TEntityNub>
    {

        /// <summary> Removes the property changed event handlers. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            this._PropertyChangedHandlers.RemoveAll();
        }

        /// <summary> The property changed handlers. </summary>
        private readonly PropertyChangeEventContextCollection _PropertyChangedHandlers = new ();

        /// <summary> Event queue for all listeners interested in property changed events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event PropertyChangedEventHandler PropertyChanged
        {
            add {
                this._PropertyChangedHandlers.Add( new PropertyChangedEventContext( value ) );
            }

            remove {
                this._PropertyChangedHandlers.RemoveValue( value );
            }
        }

        private void OnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            // defaults to Send to make this thread safe.
            this._PropertyChangedHandlers.Send( sender, e );
        }
    }
}
