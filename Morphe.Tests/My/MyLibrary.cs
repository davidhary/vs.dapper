
namespace isr.Dapper.Entities.Morphe.Tests.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    internal sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Dapper Morphe Entities Tests";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Unit Tests for the Dapper Morphe Entities";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Dapper.Morphe.Tests";
    }
}
