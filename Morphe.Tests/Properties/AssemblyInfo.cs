using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Dapper.Entities.Morphe.Tests.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Dapper.Entities.Morphe.Tests.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Dapper.Entities.Morphe.Tests.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
