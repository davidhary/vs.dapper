# Dapper Libraries

A simple object mapper for .Net Data Connection

* [Entity](#Entity)
* [Source Code](#Source-Code)
* [Database Servers, Drivers and Packages](#Packages)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Entity"></a>
## Entity

Each Dapper entity is build using the following classes:

### Table Builder

Builds the data table. Presently build functions are defined for SQLite and SQL server.

### Interface

Defines the interface for the class support the Dapper core cache management.

### NUB

A POCO Class implementing the Entity interface.

### Entity

The sub class implementing and extending the POCO class.

### View

A view to facilitate access to the class fields.

<a name="Entities"></a>
## Entities

<a name="ProductEntity"></a>
### Product

The Product entity defines a set of parts, all belonging to the same [Product](#Product)

<a name="Part"></a>
### Part

The Part entity names a specific part of a product family.

<a name="Element"></a>
### Element

Each product consists of a set of elements such as resistors, compound resistors,
open or short circuits and equations. For example, a divider product may consist of two
resistors, R1 and R2, a compound resistor D1 = R1+R2 and an equation M1 = (R1/R2).

<a name="Nominal"></a>
### Adaptations

Nominal, thus termed Nom in this context, stands for any measured or derived quantity of a product 
[Element](#Element). Quantities include Resistance, Equation and Third Harmonics Index (THI).
Nominal Types would be extended to include capacitance, inductance, current, voltage or
whatever other quantities could be measured or derived for the specific element.

<a name="Nominal-Trait"></a>
### Nominal Traits

Each part include a set of [Product Elements](#Element), each described further by a set of
traits, called Nominal Traits (Nom Trait). For example, the R1 of a divider has, at a minimum,
a nominal value and tolerance. Similarly, an equation also has an expected nominal value and
tolerance. A resistor element with a nominal type of Third Harmonics Index (THI), has a nominal
resistance for nominal value, does not have a tolerance but only upper and lower specification
limits. The part nominal traits are specified with respect to each meter thus allowing the
tolerance to map to a specification limit that matches the meter specifications as defined in
the Ohm Meter Setup table. For example, the specifications for measurements with the 7510
multimeter might differ from those done with the 2002 multimeter.

Note that Nominal here stands for any generic Aspect of a part element. Presently defined are
Resistance, equation, or Third Harmonics Index (THI). But Nominals could be extended to include
capacitance, inductance, current, voltage or whatever other nominal value could be evaluated for
the specific element.

With each such nominal aspect, is associated a Nominal Under Test, or [NUT](#NUT), 
which identifies the specific measurement or evaluation (in case of an equation) that takes
place during data collection.

<a name="NUT"></a>
### NUT

NUT stands for the Nominal Under Test, which is the measured or derived quantity to be saved
in the database for the an element, e.g., R1, and a Nominal Type, such as Resistance, Equation
or Third Harmonics Index.

<a name="UUT"></a>
### UUT

UUT stands for the Unit Under Test. Each UUT includes a set of [NUTs](#NUT) describing the values 
assigned to each product element nominal value during data collection. 

<a name="Meter"></a>
### Meter

Meter is the instrument that takes measurements, which are saved for each [NUT](#NUT) as 
[Nut Readings](#NUT-Reading). Each [UUT](#UUT) and [NUT](#NUT) are associated with a 
[Meter](#Meter). 

<a name="Platform-Meter"></a>
### Platform Meter

The platform meter encapsulates a meter entity as well as the [Element](#Element)s associated
with this meter. 

<a name="NUT-Reading"></a>
### NUT Reading

Each [Nominal Under Test](#NUT) is saved in the database along with a set of values such as the
reading amount, reading status and bin number. The values, which are optionally recorded 
in the NUT Reading table, are aggregated in the Meta Reading.

<a name="Meta-Reading"></a>
### Meta Reading

Each [part](#Part) consists of a set of [elements](#Element), each with a set of 
measured or derived quantities (e.g., resistance or equation) called [Nominals](#Nominal).
Each nominal quantity has [nominal traits](#Nominal-Trait) defining quantities such as
nominal value, tolerance, and thermal stability.

During data collection, each [NUT](#NUT) gets a set of values, each called a
Reading. These readings are assembled into a collection of [NUT Reading](#NUT-Reading) 
Entities, which include a measured or evaluated value (Amount), a measurement status word, 
and a bin number assigned to the Reading based on the [nominal traits](#Nominal-Trait) of 
the NUT [element](#Element).

The Meta Reading class assembles the Amounts of the Nut Reading Entities (e.g., Amount, Status,
and Bin Number) for each NUT as well as the part specifications of the nominal type, which are
used for binning the NUT.

<a name="Preformance-Statistics"></a>
### Performance Statistics

The performance statistics includes the session and historical statistics of all sessions past
the part reference date.

<a name="reading-Bin"></a>
### Reading Bin

The reading bin defines the bin numbers, such as good, high, low or overflow) into which 
a NUT (measurement or derived quantity) is binned.

<a name="Product-Sort"></a>
### Product Sort

The product sort collection maps from the bin number of the UUT elements to a sort
and a sort bucket. This allows to combine bins into a single sort bucket. For example, both the
high and low bins could be dropped into the guard sort bucket. 

<a name="Bucket-Bin"></a>
### Bucket Bin

The bucket bin describes the physical qualities of the bucket into which parts are actually 
sorted. These could include the digital output that sorts the part into the bucket or 
the color to display on screen when a part is sorted to the specific bucket. 

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [Core](https://www.bitbucket.org/davidhary/vs.core) - Core Libraries
* [Dapper](https://www.bitbucket.org/davidhary/vs.dapper) - Dapper Libraries
```
git clone git@bitbucket.org:davidhary/vs.core.git
git clone git@bitbucket.org:davidhary/vs.dapper.git
```

Clone the repositories into the following relative path(s) (parents of the .git folder):
```
.\Libraries\VS\Core\Core
.\Libraries\VS\Data\Dapper
```

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository]((https://www.bitbucket.org/davidhary/vs.ide)).

Restoring Editor Configuration assuming c:\My is the root folder of the .NET solutions):
```
xcopy /Y c:\My\.editorconfig c:\My\.editorconfig.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.editorconfig c:\My\.editorconfig
```

Restoring Run Settings assuming c:\user\<me> is the root user folder:
```
xcopy /Y c:\user\<me>\.runsettings c:\user\<me>\.runsettings.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.runsettings c:\user\<me>\.runsettings
```

<a name="Packages"></a>
## Database Servers, Drivers and Packages
The following database servers, drivers and packages are associated with this repository:
* [SQL Server Express](https://www.microsoft.com/en-us/sql-server/sql-server-downloads)
* [SQL Server Compact](http://www.microsoft.com/sqlserver/en/us/editions/2012-editions/compact.aspx)
* [SQLite](https://system.data.sqlite.org/index.html/doc/trunk/www/downloads.wiki)

### SQLite
* The  SQLite dynamic link libraries are linked to C:\Apps\SQLite. 
This was down due to update conflicts between the SQLite NUGET packages and the SQLite installation package.
It seems that the SQLite packages are more up to date. 
* The Interop library is marked to be copied to the output folder, which  might have been resolved with version 113.

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio](https://www.visualstudio.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - WiX Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [Code Converter](https://github.com/icsharpcode/CodeConverter) - Code Converter
* [Search and Replace](http://www.funduc.com/search_replace.htm) - Funduc Search and Replace for Windows

<a name="Authors"></a>
## Authors
* [Sam Saffron, Marc Gravell, Nick Craver](https://stackexchange.github.io/Dapper/)
* [ATE Coder](https://www.IntegratedScientificResources.com)

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix](https://www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow](https://www.stackoveflow.com) - Joel Spolsky

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Dapper](https://stackexchange.github.io/Dapper/)

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)

